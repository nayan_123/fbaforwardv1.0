<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class TelexRelease extends Eloquent
{

    protected $primaryKey = 'id';
    protected $table = 'telex_releases';
    protected $fillable = ['order_id', 'file'];

}