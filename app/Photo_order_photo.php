<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo_order_photo extends Model
{
    protected $primaryKey = 'id';
    protected $table ='photo_order_photos';
    protected $fillable = ['photo_order_id','photo'];

}