<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerClearance extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table ='customer_clearances';
    protected $fillable = ['file_3461','file_7501','custom_duties','FDA_clearance','lacey_act','OGA','terminal_fees','order_id'];

}