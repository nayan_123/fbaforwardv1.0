<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse_hold extends Model
{
    protected $primaryKey = 'id';
    protected $table ='warehouse_holds';
    protected $fillable = ['name'];

}