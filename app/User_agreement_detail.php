<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_agreement_detail extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table = 'user_agreement_details';
    protected $fillable = ['user_id', 'signature_request_id','response', 'status'];

}