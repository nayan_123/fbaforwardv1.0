<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class PreShipment extends Eloquent
{

    protected $table = 'pre_shipment';
    protected $fillable = ['name', 'customer_requirement_id','rate'];

}