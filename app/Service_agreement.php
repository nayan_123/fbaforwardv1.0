<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Service_agreement extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['user_id','signature_request_id','service_date','response','status'];
}