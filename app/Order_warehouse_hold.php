<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Order_warehouse_hold extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['order_id','hold_type_id','hold_type_text','detail_id','detail_text','cost','measure','no_of_unit','notes'];

}