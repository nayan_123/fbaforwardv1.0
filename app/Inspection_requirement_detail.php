<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inspection_requirement_detail extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table ='inspection_requirement_details';
    protected $fillable = ['customer_requirement_id','inspection_id','qty'];

    public function customer_requirement()
    {
        return $this->belongsTo(Customer_requirement::class,'id');
    }

}