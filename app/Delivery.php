<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table ='delivery';
    protected $fillable = ['delivery_required','location_of_goods','delivery_company','bill_of_lading','container','file_3461','delivery_order','ETA_to_warehouse','order_id','warehouse_process_fee','payment_method','terminal_fees'];

}