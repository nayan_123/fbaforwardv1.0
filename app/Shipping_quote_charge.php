<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping_quote_charge extends Model
{
    //
    protected $primaryKey = 'id';

    protected $table ='shipping_quote_charges';
    protected $fillable = ['shipping_quote_id', 'charge_id','charge'];

    public function shipping_quote()
    {
        return $this->belongsTo(Shipping_quote::class);
    }
    public function charges()
    {
        return $this->belongsTo(Charges::class);
    }
}