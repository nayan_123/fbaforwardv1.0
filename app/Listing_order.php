<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Listing_order extends Model
{
    protected $primaryKey = 'id';
    protected $table ='listing_orders';
    protected $fillable = ['order_id','amazon_order_id','user_id','frontend_qty','backend_qty','product_detail','links','links_to_competitors','lead_time','is_activated'];

}