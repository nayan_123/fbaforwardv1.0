<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Custom_requirement_detail extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table ='custom_requirement_details';
    protected $fillable = ['customer_requirement_id','custom_id'];

    public function customer_requirement()
    {
        return $this->belongsTo(Customer_requirement::class,'id');
    }

}