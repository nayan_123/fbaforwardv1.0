<?php

namespace App;
use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;
class Amazon_inventory extends Eloquent
{
    protected $guarded = ['id'];
    public function users()
    {
        return $this->belongsTo(Users::class);
    }

}