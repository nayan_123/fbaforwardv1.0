<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipment_contain extends Model
{
    //
    protected $primaryKey = 'id';
    protected $fillable = ['name'];

}
