<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class OrderHistory extends Eloquent
{

    protected $table = 'order_history';
    protected $fillable = ['order_history_status','order_id'];

}