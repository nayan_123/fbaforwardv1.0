<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class ZohoDetail extends Eloquent
{

    protected $table = 'zoho_details';
    protected $fillable = ['zoho_detail_id', 'auth_token','is_active'];

}