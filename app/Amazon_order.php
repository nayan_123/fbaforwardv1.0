<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Amazon_order extends Model
{
    protected $primaryKey = 'id';
    protected $table ='amazon_orders';
    protected $fillable = ['order_id','is_activated'];
}