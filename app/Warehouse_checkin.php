<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse_checkin extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table ='warehouse_checkins';
    protected $fillable = ['order_id', 'label', 'deliver_by','air_way','total_cartons','total_units','total_volume','shipment_condition','condition_notes','notified','notified_note'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function Warehouse_checkin_image()
    {
        return $this->hasMany(Warehouse_checkin_image::class);
    }
}