<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse_lineitem extends Model
{
    protected $primaryKey = 'id';
    protected $table ='warehouse_lineitems';
    protected $fillable = ['warehouse_checkin_id', 'length', 'width', 'height','weight','unit_per_carton','unit_subtotal','cartons_subtotal','volume_subtotal'];

    public function warehouse_checkin()
    {
        return $this->belongsTo(Warehouse_checkin::class);
    }
}