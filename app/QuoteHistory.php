<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class QuoteHistory extends Eloquent
{

    protected $table = 'quote_history';
    protected $fillable = ['quote_history_status','customer_requirement_id','units','cartons','cbm','weight','chargable_weight','order_id'];

}