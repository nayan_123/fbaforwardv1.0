<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Question extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['name','price','measurement_id'];

}