<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer_requirement extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['order_id','user_id','goods_ready_date','incoterms','supplier','factory_audit','pre_inspection','port_of_origin', 'shipment_size_type','units','shipment_weight','shipment_volume','shipment_notes','prep_note','inspection_note','outbound_method_id','outbound_method_note','is_activated','expire_date','shipper_destination','added_by','order_type','date_of_arrival_at_fbaforward','pre_shipment_note'];

    public function customer_requirement_detail()
    {
        return $this->hasmany(Customer_requirement_detail::class);
    }
    public function shipping_quote()
    {
    	return $this->hasmany(Shipping_quote::class);
    }
}
