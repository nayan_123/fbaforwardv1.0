<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charge_category extends Model
{
    //
    protected $primaryKey = 'id';

    protected $table ='charge_categories';
    protected $fillable = ['category_name'];

    public function charge()
    {
        return $this->hasMany(Charges::class);
    }
}