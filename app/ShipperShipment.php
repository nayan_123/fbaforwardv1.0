<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class ShipperShipment extends Eloquent
{

    protected $table = 'shipper_shipment';
    protected $fillable = ['cargo_ready_date', 'carrier','containers','container_height','container_width', 'notes','order_id','closing_date'];

}