<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Amazon_marketplace extends Eloquent
{

    protected $guarded = ['id'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function customer_amazon_detail()
    {
        return $this->hasOne(Customer_amazon_detail::class);
    }
    public function dev_account()
    {
        return $this->hasOne(Dev_account::class);
    }

}
