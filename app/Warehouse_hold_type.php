<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse_hold_type extends Model
{
    protected $primaryKey = 'id';
    protected $table ='warehouse_hold_types';
    protected $fillable = ['name','hold_id'];

}