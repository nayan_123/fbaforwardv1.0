<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incoterm extends Model
{
    //
    protected $primaryKey = 'id';
    protected $fillable = ['name'];

}