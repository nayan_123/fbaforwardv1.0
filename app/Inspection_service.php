<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inspection_service extends Model
{

    protected $primaryKey = 'id';
    protected $table ='inspection_services';
    protected $fillable = ['name'];

}