<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Hello_sign_detail extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['sign_key','client_id'];

}