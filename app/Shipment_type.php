<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipment_type extends Model
{
    //
    protected $primaryKey = 'id';
    protected $fillable = ['name','rate','trucking_rate'];

}