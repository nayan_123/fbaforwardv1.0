<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class ZohoAccountManager extends Eloquent
{

    protected $table = 'zoho_account_managers';
    protected $fillable = ['zoho_user_account_id', 'email','role','name','mobile','phone','image','status'];

}