<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itin_notice extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table ='itin_notices';
    protected $fillable = ['user_id','file'];

}