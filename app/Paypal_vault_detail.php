<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Paypal_vault_detail extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['client_id','secret_key'];
}