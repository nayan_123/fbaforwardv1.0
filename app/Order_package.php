<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Order_package extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['order_id','file','status'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}