<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class SubHold extends Eloquent
{

    protected $table = 'sub_holds';
    protected $fillable = ['hold_id','sub_hold_name'];

}