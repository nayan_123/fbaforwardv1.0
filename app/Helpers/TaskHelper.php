<?php
use App\User_info;
use App\Customer_amazon_detail;
function getprofile()
{
   $profile = User_info::where('user_id',\Auth::user()->id)->get();
   if(count($profile)>0)
       return "Complete";
   else
       return "Pending";
}
function getamazon()
{
    $customer_amazon_detail = Customer_amazon_detail::where('user_id', '=', \Auth::user()->id)->get();
    if(count($customer_amazon_detail)>0)
    {
        if($customer_amazon_detail[0]->mws_seller_id == '')
        return "Pending";
    else
        return "Complete";
    }
    else
    {
        return "Pending";
    }
}
function getcreditcard()
{
    $credit_card_info = DB::table('user_credit_cardinfos')->where('user_id',\Auth::user()->id)->get();
    if(empty($credit_card_info))
        return "Pending";
    else
        return "Complete";
}
function getagreement()
{
    $agreement_info = DB::table('user_agreement_details')->where('user_id',\Auth::user()->id)->where('status','1')->get();
    if(empty($agreement_info))
        return "Pending";
    else
        return "Complete";
}
function getvideo()
{
    if(\Auth::user()->complete=='0')
        return "Pending";
    else
        return "Complete";
}