<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse_checkin_label extends Model
{
    protected $primaryKey = 'id';
    protected $table ='warehouse_checkin_labels';
    protected $fillable = ['warehouse_checkin_id', 'label_number'];
    public function Warehouse_checkin()
    {
        return $this->belongsTo(Warehouse_checkin::class);
    }
}