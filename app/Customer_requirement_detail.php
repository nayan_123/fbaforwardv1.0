<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer_requirement_detail extends Model
{
    //
    protected $primaryKey = 'id';
    protected $fillable = ['customer_requirement_id','product_id','fnsku','qty_per_box', 'no_boxs', 'total','product_name','length','width','height','unit'];

    /*public function amazon_inventory()
    {
        return $this->belongsTo(Amazon_inventory::class);
    }*/
    public function customer_requirement()
    {
        return $this->belongsTo(Customer_requirement::class);
    }

}
