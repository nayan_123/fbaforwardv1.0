<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Amazon_report_log extends Model
{
    protected $primaryKey = 'id';
    protected $table ='amazon_report_logs';
    protected $fillable = ['user_id','report_type','report_id','status'];
}