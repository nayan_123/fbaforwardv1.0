<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Laravel\Cashier\Billable;

class User_info extends Authenticatable
{
    use Billable;
    protected $guarded = ['id'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function users()
    {
        return $this->belongsTo(Users::class);
    }

}
