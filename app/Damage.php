<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Damage extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['name','price','measurement_id'];

}