<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class User_document extends Eloquent
{

    protected $primaryKey = 'id';
    protected $table = 'user_documents';
    protected $fillable = ['user_id', 'document'];

}