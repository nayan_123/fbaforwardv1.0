<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Power_of_attorny extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table ='power_of_attornies';
    protected $fillable = ['user_id','name','title','email','not_agree'];

}