<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping_charges extends Model
{
    //
    protected $primaryKey = 'id';

    protected $table ='shipping_charges';
    protected $fillable = ['shipping_method_id','charge_id'];

    public function shipping_method()
    {
        return $this->belongsTo(Shipping_method::class);
    }
    public function charges()
    {
        return $this->belongsTo(Charges::class);
    }
}