<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing_order_photo extends Model
{
    protected $primaryKey = 'id';
    protected $table ='listing_order_photos';
    protected $fillable = ['listing_order_id','photo'];

}