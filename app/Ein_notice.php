<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ein_notice extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table ='ein_notices';
    protected $fillable = ['user_id','file'];

}