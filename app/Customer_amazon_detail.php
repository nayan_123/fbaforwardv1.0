<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Customer_amazon_detail extends Eloquent
{


    protected $guarded = ['id'];
    public function users()
    {
        return $this->belongsTo(Users::class);
    }
    public function amazon_marketplace()
    {
        return $this->belongsTo(Amazon_marketplace::class);
    }
}
