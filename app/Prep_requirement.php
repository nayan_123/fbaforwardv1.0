<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prep_requirement extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table ='prep_requirements';
    protected $fillable = ['customer_requirement_id','shipment_id', 'prep_id', 'is_activated'];

    public function customer_requirement()
    {
        return $this->belongsTo(Customer_requirement::class,'id');
    }

}