<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo_order extends Model
{
    protected $primaryKey = 'id';
    protected $table ='photo_orders';
    protected $fillable = ['order_id','amazon_order_id','user_id','product','standard_photo_qty','prop_photo_qty','lifestyle_photo_qty','standard_file_format','prop_file_format','lifestyle_file_format','description_photo','links','after_photo','is_activated'];

}