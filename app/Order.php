<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $primaryKey = 'order_id';
    protected $fillable = ['order_no','user_id','is_activated','tracking_number','note_id','note','sb_number','zoho_sales_order_id','zoho_order_id'];


}
