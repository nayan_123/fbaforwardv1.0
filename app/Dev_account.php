<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Dev_account extends Eloquent
{
    protected $guarded = ['id'];
    public function amazon_marketplace()
    {
        return $this->belongsTo(Amazon_marketplace::class);
    }
}
