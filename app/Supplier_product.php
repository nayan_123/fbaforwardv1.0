<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier_product extends Model
{
    protected $primaryKey = 'id';
    protected $table ='supplier_product_details';
    protected $fillable = ['supplier_id', 'product_id'];
    public function supplier()
    {
        return $this->belongsTo(Supplier::class,'supplier_id');
    }
    public function product()
    {
        return $this->belongsTo(Amazon_inventory::class,'id');
    }
}