<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Document extends Eloquent
{

    protected $table = 'documents';
    protected $fillable = ['document'];

}