<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //
    protected $primaryKey = 'supplier_id';
    protected $fillable = ['user_id', 'company_name', 'contact_name', 'email', 'phone_number', 'second_contact_name','second_email','second_phone_number','nick_name','city','is_activated'];
    protected $table ='suppliers';

}
