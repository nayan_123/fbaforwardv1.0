<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer_requirement_method extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['customer_requirement_id','shipping_method_id','is_activated'];

}
