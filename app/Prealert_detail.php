<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prealert_detail extends Model
{
    //
    protected $primaryKey = 'id';

    protected $table ='prealert_details';
    protected $fillable = ['order_id','ISF','HBL','MBL','cargo_manifest','commercial_invoice','packing_list','ETD_china','ETA_US','status'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}