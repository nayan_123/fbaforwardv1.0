<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Hold extends Eloquent
{

    protected $table = 'holds';
    protected $fillable = ['hold_name','shipping_method_id'];

}