<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing_service_detail extends Model
{
    //
    protected $primaryKey = 'listing_service_detail_id';
    protected $table ='listing_service_details';
    protected $fillable = ['listing_service_id', 'shipping_method_id', 'customer_requirement_id'];

    public function listing_service()
    {
        return $this->belongsTo(Listing_service::class,'listing_service_id');
    }
    public function photo_list_detail()
    {
        return $this->hasMany(Photo_list_detail::class,'listing_service_detail_id');
    }

}