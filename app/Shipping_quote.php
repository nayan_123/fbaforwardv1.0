<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping_quote extends Model
{
    //
    protected $primaryKey = 'id';

    protected $table ='shipping_quotes';
    protected $fillable = ['customer_requirement_id','shipping_method_id','shipping_port','freight_fee','additional_note','chargeable_weight','total_freight_price','is_activated'];

    public function shipping_charge()
    {
        return $this->hasMany(Shipping_charge::class);
    }
    public function customer_requirement()
    {
    	return $this->belongsTo('customer_requirement_id',Customer_requirement::id);
    }
}