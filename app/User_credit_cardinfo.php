<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class User_credit_cardinfo extends Eloquent
{

    protected $guarded = ['id'];
    public function users()
    {
        return $this->belongsTo(Users::class);
    }
    public function payment_detail()
    {
        return $this->hasMany(Payment_detail::class);
    }

}
