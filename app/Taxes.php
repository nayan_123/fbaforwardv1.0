<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxes extends Model
{
    //
    protected $primaryKey = 'id';
    protected $table ='taxes';
    protected $fillable = ['tax_name','short_name'];

   public function user_infos()
   {
       $this->hasMany(User_info::class);
   }
}

