<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Order_inventory_history extends Model
{
    protected $primaryKey = 'id';
    protected $table ='order_inventory_histories';
    protected $fillable = ['order_id','product_id','total','status'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}