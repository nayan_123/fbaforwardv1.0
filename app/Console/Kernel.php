<?php

namespace App\Console;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
class Kernel extends ConsoleKernel
{
    protected $commands = [
        // Commands\Inspire::class,
        Commands\InventoryCommand::class,
        Commands\InventorynameCommand::class,
        Commands\ZohomanagerCommand::class,
        Commands\ZohoorderCommand::class,
        Commands\statusesCommand::class,
    ];
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inventory:command')->everyMinute();
        $schedule->command('inventoryname:command')->everyMinute();
        $schedule->command('zohomanager:command')->daily();
        $schedule->command('Zohoorder:command')->daily();
        $schedule->command('statuses:command')->daily();
    }
}
