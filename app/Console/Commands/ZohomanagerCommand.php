<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\ZohoAccountManager;
use App\ZohoDetail;
class ZohomanagerCommand extends Command{
    protected $signature = 'zohomanager:command';
    protected $description = 'get zoho CRM manager';
    public function __construct(){
        parent::__construct();
    }
    public function handle(){
        $token = $this->check_token();
        $users= $this->api("https://crm.zoho.com/crm/private/json/Users/getUsers?authtoken=".$token."&scope=crmapi&type=ActiveUsers");
        $data = json_decode($users);
        foreach ($data as $key => $user) {
            foreach ($user as $value) {
                foreach ($value as $val) {
                    if($val->role == 'Consultant'){
                        $manager = ZohoAccountManager::where('email',$val->email)->first();
                        if(!isset($manager)){
                            ZohoAccountManager::create([
                                'zoho_user_account_id' => $val->id,
                                'email' => $val->email,
                                'role' => $val->role,
                                'phone' => $val->phone,
                                'name' => $val->content,
                                'mobile' => $val->mobile,
                                'status' => 0
                            ]);
                        }
                    }
                }
            }
        }
        $deactive_users = $this->api("https://crm.zoho.com/crm/private/json/Users/getUsers?authtoken=".$token."&scope=crmapi&type=DeactiveUsers");
        $data = json_decode($deactive_users);
        foreach ($data as $key => $user) {
            foreach ($user as $value) {
                if(isset($value->code)){
                    if($value->code == '4422'){
                        dd('exit');
                    }
                }
                foreach ($value as $val) {
                    if($val->role == 'Manager'){
                        ZohoAccountManager::where('zoho_user_account_id',$val->id)
                            ->update(['status' => 1]);
                    }
                }
            }
        }
    }
    public function check_token(){
        $zoho_detail = ZohoDetail::first();
        if(!$zoho_detail){
            $this->authGenerate();
            $zoho_detail = ZohoDetail::first();
        }
        return $zoho_detail->auth_token;
    }
    public function api($url){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}
