<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Customer_requirement;
use App\QuoteHistory;
use Carbon\Carbon;
use App\Prealert_detail;
use App\Order;
class statusesCommand extends Command{
    protected $signature = 'statuses:command';
   protected $description = 'Manage quote and order statuses';
    public function __construct(){
        parent::__construct();
    }
    public function handle(){
        $customers = Customer_requirement::where('customer_requirements.expire_date','=',Carbon::now()->toDateString())->get();
        foreach ($customers as $key => $customer) {
            if(!QuoteHistory::where('customer_requirement_id',$customer->id)->first()){
                QuoteHistory::create([
                    'quote_history_status' => 5,
                    'customer_requirement_id' => $customer->id
                ]);
            }
        }
        $current_date = date('Y-m-d');
        $etd_orders = Prealert_detail::where('ETD_china',$current_date)->where('status','0')->get();
        if(count($etd_orders)>0){
            foreach ($etd_orders as $etd_order) {
                Order::where('order_id',$etd_order->order_id)->update(array('is_activated'=>'5'));
                Prealert_detail::where('order_id',$etd_order->order_id)->update(array('status'=>'1'));
            }
        }
        $eta_orders = Prealert_detail::where('ETA_US',$current_date)->where('status','1')->get();
        if(count($eta_orders)>0) {
            foreach ($eta_orders as $eta_order) {
                Order::where('order_id', $eta_order->order_id)->update(array('is_activated' => '6'));
                Prealert_detail::where('order_id', $eta_order->order_id)->update(array('status' => '2'));
            }
        }
    }
}
