<?php

namespace App\Console\Commands;
use App\Amazon_inventory;
use App\Amazon_report_log;
use App\Dev_account;
use Illuminate\Console\Command;
use App\Amazon_marketplace;
use Carbon\Carbon;
class InventorynameCommand extends Command
{
    protected $signature = 'inventoryname:command';
    protected $description = 'get inventory name';
    public function __construct(){
        parent::__construct();
    }
    public function handle(){
        $results = Amazon_marketplace::selectRaw("customer_amazon_details.mws_seller_id, customer_amazon_details.user_id, customer_amazon_details.mws_authtoken, amazon_marketplaces.market_place_id")
            ->join('customer_amazon_details', 'customer_amazon_details.mws_market_place_id', '=', 'amazon_marketplaces.id')
            ->get();
        foreach ($results as $seller_detail) {
            $asinarray=array();
            $productasins = Amazon_inventory::where('user_id',$seller_detail->user_id)->where('product_name','')->get();
            foreach ($productasins as  $productasin) {
                $asinarray[] = $productasin->ASIN;
            }
            $UserCredentials['mws_authtoken'] = !empty($seller_detail->mws_authtoken) ? decrypt($seller_detail->mws_authtoken) : '';
            $UserCredentials['mws_seller_id'] = !empty($seller_detail->mws_seller_id) ? decrypt($seller_detail->mws_seller_id) : '';
            $UserCredentials['market_place_id'] =$seller_detail->market_place_id;
            if(!empty($asinarray)) {
                $this->getProductInfo($asinarray, $seller_detail, $UserCredentials);
            }
        }
    }
    public function getProductInfo($asinarray,$account,$usercredential){
        $user_id = $account->user_id;
        $reports = Amazon_report_log::where('user_id',$user_id)->where('status','0')->get()->toArray();
        $service = $this->getReportsClient_report();
            if(empty($reports)) {
                $this->report_type = '_GET_MERCHANT_LISTINGS_ALL_DATA_';
                $this->from_date_time = Carbon::parse('1 days ago');
                $this->to_date_time = Carbon::now();
                $request = $this->getRequest_report($usercredential);
                $response = $service->requestReport($request);
                $report_status=$response->RequestReportResult->ReportRequestInfo->ReportProcessingStatus;
                $report_id=$response->RequestReportResult->ReportRequestInfo->ReportRequestId;
                $data =array('user_id'=>$user_id,
                             'report_type'=>$this->report_type,
                             'report_id'=>$report_id,
                             'status'=>'0'
                             );
                $amz_report= Amazon_report_log::create($data);
                if($report_status=='_SUBMITTED_' || $report_status=='_DONE_') {
                    $request_new= $this->getreportrequest($usercredential,$report_id);
                    $response_new=$service->getReportRequestList($request_new);
                    $status=''; $generated_id='';
                    foreach ($response_new->GetReportRequestListResult->getReportRequestInfoList() as $reportinfo) {
                        $status=$reportinfo->ReportProcessingStatus;
                        $generated_id=$reportinfo->GeneratedReportId;
                    }
                    if($status=='_DONE_' || $status=='_DONE_NO_DATA_' && !empty($generated_id)) {
                        $last_request = $this->getlastreportrequest($usercredential,$generated_id);
                        $last_response =$service->getReport($last_request);
                        $last_response_final =$last_request->getReport();
                        while ( ! feof( $last_response_final ) ) {
                            $row = fgetcsv($last_response_final, 10000, "\t");
                            if($row[16]=='asin1' && $row[17]=='asin2' && $row[18]=='asin3')
                                continue;
                            else{
                                foreach ($asinarray as $new_array) {
                                    if($new_array==$row[16] || $new_array==$row[17] || $new_array==$row[18]) {
                                        Amazon_inventory::where('user_id',$user_id)->where('ASIN',$new_array)->update(array('product_name'=>$row[0]));
                                    }
                                }
                            }
                        }
                        Amazon_report_log::where('id',$amz_report->id)->update(array('status'=>'2'));
                    }
                    elseif ($status=='_CANCELLED_' && empty($generated_id)) {
                        Amazon_report_log::where('id',$amz_report->id)->update(array('status'=>'2'));
                    }
                }
            }
            else {
                foreach ($reports as $new_reports) {
                    $request_new = $this->getreportrequest($usercredential, $new_reports['report_id']);
                    $response_new = $service->getReportRequestList($request_new);
                    $status=''; $generated_id='';
                    foreach ($response_new->GetReportRequestListResult->getReportRequestInfoList() as $reportinfo) {
                        $status=$reportinfo->ReportProcessingStatus;
                        $generated_id=$reportinfo->GeneratedReportId;
                    }
                    if($status=='_DONE_' || $status=='_DONE_NO_DATA_' && !empty($generated_id)) {
                        $last_request = $this->getlastreportrequest($usercredential,$generated_id);
                        $last_response =$service->getReport($last_request);
                        $last_response_final =$last_request->getReport();
                        while ( ! feof( $last_response_final ) ) {
                            $row = fgetcsv($last_response_final, 10000, "\t");
                            if($row[16]=='asin1' && $row[17]=='asin2' && $row[18]=='asin3')
                                continue;
                            else{
                                foreach ($asinarray as $new_array) {
                                    if($new_array==$row[16]) {
                                        Amazon_inventory::where('user_id',$user_id)->where('ASIN',$new_array)->update(array('product_name'=>$row[0]));
                                    }
                                }
                            }
                        }
                        Amazon_report_log::where('id',$new_reports['id'])->update(array('status'=>'2'));
                    }
                    elseif ($status=='_CANCELLED_' && empty($generated_id)) {
                        Amazon_report_log::where('id',$new_reports['id'])->update(array('status'=>'2'));
                    }
                }
            }
    }
    protected function getReportsClient_report(){
        list($access_key, $secret_key, $config) = $this->getKeys_report();
        return new \MarketplaceWebService_Client(
            $access_key,
            $secret_key,
            $config,
            env('APPLICATION_NAME'),
            env('APPLICATION_VERSION')
        );
    }
    private function getKeys_report($uri = ''){
        add_to_path('Libraries');
        $devAccount = Dev_account::first();
        return [
            $devAccount->access_key,
            $devAccount->secret_key,
            self::getMWSConfig_report()
        ];
    }
    public static function getMWSConfig_report(){
        return [
            'ServiceURL' => "https://mws.amazonservices.com",
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 10,
        ];
    }
    private function getRequest_report($UserCredentials){
        $request = new \MarketplaceWebService_Model_RequestReportRequest();
        $request->setReportType($this->report_type);
        if ($this->from_date_time) {
            $request->setStartDate($this->from_date_time);
        }
        if ($this->to_date_time) {
            $request->setEndDate($this->to_date_time);
        }
        $this->initRequest($UserCredentials, $request, 'setMerchant');
        return $request;
    }
    protected function initRequest($UserCredentials, $request, $merchantMethod = 'setSellerId', $setMarketPlace = null){
        $request->setMWSAuthToken($UserCredentials['mws_authtoken']);
        $request->$merchantMethod($UserCredentials['mws_seller_id']);
        if (!is_null($setMarketPlace)) {
            $request->$setMarketPlace($UserCredentials['market_place_id']);
        }
    }
    private function getreportrequest($UserCredentials,$report_id){
        $request= new \MarketplaceWebService_Model_GetReportRequestListRequest();
        $request->setMerchant($UserCredentials['mws_seller_id']);
        $request->setMarketplace($UserCredentials['market_place_id']);
        $request->setMWSAuthToken($UserCredentials['mws_authtoken']);
        $newid = new \MarketplaceWebService_Model_IdList();
        $newid->setId($report_id);
        $request->setReportRequestIdList($newid);
        return $request;
    }
    private function getlastreportrequest($UserCredentials,$generated_id){
        $request= new \MarketplaceWebService_Model_GetReportRequest();
        $request->setMerchant($UserCredentials['mws_seller_id']);
        $request->setMarketplace($UserCredentials['market_place_id']);
        $request->setMWSAuthToken($UserCredentials['mws_authtoken']);
        $request->setReport(@fopen('php://memory', 'rw+'));
        $request->setReportId($generated_id);
        return $request;
    }
}
