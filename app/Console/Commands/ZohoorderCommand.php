<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Order;
use App\ZohoDetail;
class ZohoorderCommand extends Command{
    protected $signature = 'Zohoorder:command';
    protected $description = 'Get orders from custom module';
    public function __construct(){
        parent::__construct();
    }
    public function handle()
    {
        $token = $this->check_token();
        $orders = $this->api("https://crm.zoho.com/crm/private/json/CustomModule4/getRecords?newFormat=1&authtoken=" . $token . "&scope=crmapi");
        $order_response = json_decode($orders);
        if (isset($order_response->response->result)) {
            $order_data = Order::where('zoho_order_id', null)->pluck('order_no')->toArray();
            foreach ($order_response->response->result->CustomModule4->row as $key => $response) {
                foreach ($response->FL as $value) {
                    if ($value->val == 'CUSTOMMODULE4_ID') {
                        $zoho_order_id = $value->content;
                    }
                    if ($value->val == 'Order Name') {
                        if (in_array($value->content, $order_data)) {
                            Order::where('order_no', $value->content)->update(['zoho_order_id' => $zoho_order_id]);
                        }
                    }
                }
            }
        }
    }
    public function check_token(){
        $zoho_detail = ZohoDetail::first();
        if(!$zoho_detail){
            $this->authGenerate();
            $zoho_detail = ZohoDetail::first();
        }
        return $zoho_detail->auth_token;
    }
    public function api($url){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}
