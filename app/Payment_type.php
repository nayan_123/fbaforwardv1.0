<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment_type extends Model
{
    //
    protected $primaryKey = 'id';

    protected $table ='payment_types';
    protected $fillable = ['type_name'];


}