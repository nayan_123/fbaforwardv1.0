<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Location extends Eloquent
{

    protected $table = 'locations';
    protected $fillable = ['name'];

}