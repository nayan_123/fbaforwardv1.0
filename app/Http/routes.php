<?php
use App\User;
use App\Role;
use App\Order;
use App\Amazon_inventory;
use App\ZohoAccountManager;

Route::model('users', User::class);
Route::model('roles', Role::class);
Route::model('customers', User::class);
Route::model('orders', Order::class);
Route::model('activequotes', Order::class);
Route::model('expirequotes', Order::class);
Route::model('pastquotes', Order::class);
Route::model('inventories', Amazon_inventory::class);
Route::model('zohoaccountmanagers', ZohoAccountManager::class);

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'FrontendController@index');
    Route::get('/contact-us', 'FrontendController@contactUs');
    Route::post('/contact-us', 'FrontendController@contactUsSubmit');
    Route::get('/notify', 'EmailController@getnotify');
    Route::post('/notify', 'EmailController@notify');
    Route::resource('amazoninventory','AmazoninventoryController');
    Route::get('/amazon_inventory', ['as' => 'amazon_inventory', 'uses' => 'AmazoninventoryController@index']);
    Route::get('/getinvoices','QuickbooksController@getinvoices');
    Route::get('/getcustomers','QuickbooksController@getcustomers');
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::post('selectState', 'Auth\AuthController@selectState');
    Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
        Route::controllers(['datatables' => 'Admin\DatatablesController']);
        Route::get('/dashboard', 'Admin\DashboardController@index');
        Route::resource('users', 'Admin\UsersController');
        Route::post('users/selectState', 'Admin\UsersController@selectState');
        Route::get('settings/create/{type}', ['as' => 'admin.settings.create.type', 'uses' => 'Admin\SettingsController@createForm']);
        Route::get('settings/download/{settings}', ['as' => 'admin.settings.download', 'uses' => 'Admin\SettingsController@fileDownload']);
        Route::resource('roles', 'Admin\RolesController');
        Route::resource('customers', 'Admin\CustomerController');
        Route::resource('orders', 'Admin\OrderController');
        Route::resource('quotes', 'Admin\QuoteController');
        Route::resource('inventories', 'Admin\InventoryController');
        Route::resource('zohoaccountmanagers', 'Admin\ZohoController');
        
        Route::get('order-export','Admin\OrderController@export');

        Route::get('active-quote-export','Admin\QuoteController@activeQuoteExport');
        Route::get('expire-quote-export','Admin\QuoteController@expireQuoteExport');
        Route::get('past-quote-export','Admin\QuoteController@pastQuoteExport');

        Route::get('inventory-export','Admin\InventoryController@export');

        Route::get('order_report','Admin\OrderController@orderReport');

        Route::get('inventory_report','Admin\InventoryController@inventoryReport');

        Route::get('active_quote_report','Admin\QuoteController@activeQuoteReport');

        Route::get('/order/detail/{method_id}/{customer_requirement_id}','Admin\OrderController@viewOrderDetail');

        Route::get('quote/edit/{quote_id}/{quote}','Admin\QuoteController@editshippingquote');

    });
    Route::resource('telexrelease','TelexReleaseController');
    Route::resource('billoflading','BillofladingController');
    Route::resource('prealert','PrealertController');
    Route::resource('neworders','NeworderController');
    Route::resource('powerofatterney','PowerOfAtterneyController');
    Route::resource('supplychain', 'SupplierController');
    Route::post('supplychain/getproduct','SupplierController@getproduct');
    Route::resource('useragreement', 'User_agreement');
    Route::resource('order','OrderController');
    Route::resource('shipment','ShipmentController');
    Route::resource('amazonservices','AmazonServicesController');
    Route::resource('listingdocument','ListingDocumentController');
    Route::resource('photographyphoto','PhotographyDocumentController');
    Route::resource('photos','PhotoOrderController');
    Route::resource('listingoptimization','ListOrderController');
    Route::resource('serviceagreement','ServiceagreementController');
    Route::group(['prefix' => 'member'], function () {
        Route::get('/home', ['as' => 'member.home', 'uses' => 'MemberController@index']);
        Route::get('/profile', ['as' => 'member.profile', 'uses' => 'MemberController@profile']);
        Route::get('/profile/edit', ['as' => 'member.profile.edit', 'uses' => 'MemberController@editProfile']);
        Route::put('/profile/edit', ['as' => 'member.profile.update', 'uses' => 'MemberController@updateProfile']);
        Route::post('/checkread', 'MemberController@checkread');
        Route::get('/switchuser','MemberController@switchuser');
        Route::post('/storeuser','MemberController@storeuser');
        Route::get('app-request','MemberController@approveAppRequest');
        Route::get('skip-app-request','MemberController@skipAppRequest');
    });
    Route::resource('quote','QuoteController');
    Route::group(['prefix' => 'quote'], function () {
        Route::post('/rejectshippingquote', 'QuoteController@rejectshippingquote');
        Route::post('/getshippingmethod', 'QuoteController@getshippingmethod');
        Route::post('/getshippingquote', 'QuoteController@getshippingquote');
        Route::post('/revisionshippingquote', 'QuoteController@revisionshippingquote');
        Route::post('/approveshippingquote', 'QuoteController@approveshippingquote');
        Route::get('/downloadquote/{id}/{method_id}','QuoteController@viewshippingquote');
        Route::get('/edit/{quote_id}/{quote}','QuoteController@editshippingquote');
        Route::post('/get-supplier-location','QuoteController@getSupplierLocation');
        Route::post('/resubmit-quote','QuoteController@resubmitQuote');
        Route::post('get-product','QuoteController@getProduct');
        Route::post('get-supplier','QuoteController@getSupplier');
        Route::post('add-dimensions','QuoteController@addDimensions');
        Route::post('get-all-supplier','QuoteController@getAllSupplier');
        Route::post('/update-prep-note','QuoteController@updatePrepNote');
        Route::post('/update-inspection-note','QuoteController@updateInspectionNote');
        Route::post('/update-pre_shipment-note','QuoteController@updatePreshipmentNote');
        Route::get('/print-quote/{method_id}/{customer_requirement_id}/{file}','QuoteController@printQuote');
        Route::get('/quote-print/{method_id}/{customer_requirement_id}/{print}','QuoteController@quotePrint');
    });
    Route::group(['prefix' => 'shipper'], function () {
        Route::get('/viewrejectquote', 'ShipperController@viewrejectquote');
        Route::post('/viewquote', 'ShipperController@viewquote');
        Route::get('/quotelist', 'ShipperController@quotelist');
        Route::get('/shippingquoteform/{id}', 'ShipperController@shippingquoteform');
        Route::put('/shippingquoteform', 'ShipperController@addshippingquoteform');
        Route::post('/getshippingrequest', 'ShipperController@getshippingrequest');
        Route::post('/getnewmethod','ShipperController@getnewmethod');
        Route::post('/getshipmentdetail','ShipperController@getshipmentdetail');
        Route::post('/getquotedetail','ShipperController@getquotedetail');
        Route::post('/shipper_status','ShipperController@shipper_status');
        Route::post('/updatedetail','ShipperController@updatedetail');
        Route::get('/shipment','ShipperController@viewShipmentPage');
        Route::get('/view-shipment/{order_id}','ShipperController@viewShipment');
        Route::post('/create-shipment','ShipperController@createShipment');
        Route::get('/invoicing','ShipperController@invoiceDashboard');
        Route::get('/view_invoice/{order_id}','ShipperController@viewInvoice');
        Route::post('/update-invoice','ShipperController@updateInvoice');
    });
    Route::post('/test_connection','AmazonController@testconnection');
    Route::post('/amazon_credential',['as' => 'amazon_credential','uses' =>'AmazonController@addamazoncredential']);
    Route::resource('customer','CustomerController');
    Route::post('customer/selectState','CustomerController@selectState');
    Route::resource('creditcard','CreditCardController');
    Route::post('creditcard/getcarddetail','CreditCardController@getcarddetail');
    Route::resource('settings','SettingController');
    Route::post('settings/getuser', 'SettingController@getuserdetail');
    Route::post('settings/complete','SettingController@completeSetting');
    Route::resource('inventory','InventoryController');
    Route::post('inventory/inventoryhistory','InventoryController@inventoryhistory');
    Route::post('inventory/addnickname','InventoryController@addnickname');

    Route::resource('billing','BillingController');

Route::post('member/order/update_order','OrderController@updateOrder');
Route::get('member/order/place_order','OrderController@placeOrder')->name('place_order');

Route::get('bill_of_lading','LogisticsController@index');
Route::get('bill_of_lading/details','LogisticsController@billOfLadingDetails');
Route::get('bill_of_lading/approve','LogisticsController@approveBillOfLading');
Route::post('bill_of_lading/revision','LogisticsController@revisionBillOfLading');
Route::post('bill_of_lading/viewfile','LogisticsController@viewfile');

Route::resource('delivery','DeliveryController');
Route::post('delivery/create','DeliveryController@store');

Route::get('order/detail/{method_id}/{customer_requirement_id}','OrderController@viewOrderDetail');
Route::post('order/create','OrderController@store');
Route::post('order/document','OrderController@uploadDocument');
Route::post('order/upload_tracking_number','OrderController@uploadTrackingNumber');
Route::post('order/add_note','OrderController@addNote');
Route::get('order/approvewarehouse/{order_id}','OrderController@approvewarehouse');
Route::get('order/print-order/{method_id}/{customer_requirement_id}/{file}','OrderController@printOrder');
Route::get('order/edit-order/{method_id}/{customer_requirement_id}','OrderController@edit');
Route::post('order/update','OrderController@update');
Route::get('orderstatus','OrderController@orderstatus');

Route::get('auth_generate','ZohosampleController@authGenerate');
Route::get('get_lead','ZohosampleController@getLead');
Route::get('insert_note','ZohosampleController@insertNote');
Route::get('get_users','ZohosampleController@getUsers');
Route::get('get_modules','ZohosampleController@getModules');
Route::get('insert_account','ZohosampleController@insertAccount');
Route::get('get-orders-from-zoho','ZohosampleController@getOrdersFromZoho');

    /** warehouse routes
     *
     */
    Route::group(['prefix' => 'warehouse'], function(){
        Route::get('/checkin','WarehouseController@warehousecheckin');
        Route::get('/warehousecheckinform/{order_id}', 'WarehouseController@warehousecheckinform');
        Route::put('/warehousecheckinform', 'WarehouseController@addwarehousecheckinform');
        //Route::get('/createshipments/{order_id}', 'WarehouseController@createshipments');
        Route::get('/reviewcheckin','WarehouseController@warehousereviewcheckin');
        Route::get('/warehousecheckinreview/{order_id}','WarehouseController@warehousecheckinreview');
        Route::post('/addrequirement','warehouseController@addorderrequirement');
        Route::get('/holdcustomer/{order_id}','warehouseController@holdcustomer');
        Route::get('/submitreview/{order_id}','warehouseController@submitreview');
        Route::get('/downloadwarehouseimages/{id}','WarehouseController@downloadwarehouseimages');
        Route::get('/prep','WarehouseController@warehouseprep');
        Route::get('/viewchecklist/{order_id}/{status}','WarehouseController@viewchecklist');
        Route::post('/getlabel','WarehouseController@getlabel');
        Route::post('/getotherlabel','WarehouseController@getotherlabel');
        Route::post('/prepcomplete','WarehouseController@prepcomplete');
        Route::get('/submitchecklist/{order_id}','WarehouseController@submitchecklist');
        Route::get('/reviewprep','WarehouseController@warehousereviewprep');
        Route::get('/reviewwork/{order_id}','WarehouseController@reviewwork');
        Route::post('/sendquantity','WarehouseController@sendquantity');
        Route::post('/submitreviewwork','WarehouseController@submitreviewwork');
        Route::get('/shippinglabels','WarehouseController@warehouseshippinglabels');
        Route::get('/shippinglabel/{order_id}','WarehouseController@shippinglabel');
        Route::get('/printshippinglabel/{amazon_destination_id}','WarehouseController@printshippinglabel');
        Route::post('/verifylabel','WarehouseController@verifylabel');
        Route::get('/submitshipment/{order_id}','WarehouseController@submitshipment');
        Route::get('/reviewshipment','WarehouseController@warehousereviewshipment');
        Route::get('/shipmentreview/{order_id}','WarehouseController@shipmentreview');
        Route::post('/verifystatus','WarehouseController@verifystatus');
        Route::get('/complete/{order_id}','WarehouseController@complete');
        Route::post('/checkinrevision','WarehouseController@checkinrevision');
        Route::post('/approvereview','WarehouseController@approvereview');
        Route::post('/getestimateshippingcost','WarehouseController@getestimateshippingcost');
        Route::get('/packaginglist/{order_id}','WarehouseController@packaginglist');
        Route::post('/getdetails','WarehouseController@getdetails');
        Route::post('/getcostmesure','WarehouseController@getcostmesure');
        Route::post('/getorder','WarehouseController@getorder');
        Route::post('/warehouseholdstatus','WarehouseController@warehouseholdstatus');
    });

Route::post('add-card','CreditCardController@store');
Route::get('customs-clearance','CustomerClearanceController@index');
Route::get('customs-clearance/create','CustomerClearanceController@create');
Route::post('customs-clearance/create','CustomerClearanceController@store');
Route::post('add-power_of_attorney','SettingController@addPowerOfAttorney');

Route::get('get-user','ZohosampleController@getUsers');
Route::get('view-logistics-shipment/{order_id}','LogisticsController@viewShipment');
Route::get('logistics/shipment','LogisticsController@showShipmentList');
Route::post('logistics/get-shipment','LogisticsController@getShipment');
Route::post('submit-hold','LogisticsController@submitHold');
Route::post('release-hold','LogisticsController@releaseHold');
Route::post('logistics/update-document','LogisticsController@updateDocument');
Route::post('logistics/delete-document','LogisticsController@deleteDocument');
Route::post('logistics/update-logistics-details','LogisticsController@updateLogisticsDetails');
Route::post('logistics/updatelocation','LogisticsController@updatelocation');
Route::get('member/request-app','RequestAppController@index')->name('member/request-app');
Route::post('add-location','LogisticsController@addLocation');
Route::get('logistics/approve-document/{id}','LogisticsController@approveDocument');
Route::get('logistics/revision-document/{id}','LogisticsController@revisionDocument');

Route::post('zohoaccountmanager/update','Admin\ZohoController@update');

Route::post('orders/generate_report','Admin\OrderController@generateReport');
Route::post('quotes/generate_report','Admin\QuoteController@generateReport');

Route::post('inventory/generate_report','Admin\InventoryController@generateReport');

Route::post('orders/get-chart','Admin\OrderController@getChart');
Route::post('payment/get-chart','Admin\DashboardController@getPaymentChart');

Route::post('add-order-note','OrderController@addOrderNote');

Route::get('billing/invoice-download/{id}','BillingController@downloadInvoice');

});