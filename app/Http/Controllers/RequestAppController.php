<?php

namespace App\Http\Controllers;
use Auth;
class RequestAppController extends Controller{
	public function index(){
		$user=\Auth::user();
		if($user->role->name=='Customer') {
            if($user->is_requested_for_app == 0){
                return view('member.app_request');
            }
        }
        return redirect('member/home');
	}
}