<?php

namespace App\Http\Controllers;
use App\Order_inventory_history;
use Illuminate\Http\Request;
use App\Amazon_inventory;
use App\Order;
use App\Supplier;
use App\Supplier_product;
class InventoryController extends Controller{
    public function index(){
        $user = \Auth::user();
        if ($user->role_id == '3') {
            $active_order = Amazon_inventory::where('user_id', $user->id)
                                            ->where('infbaforward','>','0')
                                            ->Orwhere('processfbaforward','>','0')
                                            ->Orwhere('atfbaforward','>','0')
                                            ->Orwhere('inamazon','>','0')
                                            ->Orwhere('atamazon','>','0')
                                            ->get();
            $past_order = Amazon_inventory::where('user_id', $user->id)
                                          ->where('inamazon','>','0')
                                          ->Orwhere('atamazon','>','0')
                                          ->get();
            return view('inventory.index')->with(compact('user', 'active_order', 'past_order'));
        }
        else if($user->role_id == '4' || $user->role_id == '7') {
            $active_order = Amazon_inventory::selectRaw('amazon_inventories.*, sum(customer_requirement_details.no_boxs) as carton, sum(customer_requirement_details.total) as units')
                ->join('customer_requirement_details','customer_requirement_details.product_id','=','amazon_inventories.id')
                ->join('customer_requirements','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
                ->join('orders','customer_requirements.order_id','=','orders.order_id')
                ->whereBetween('orders.is_activated',array('0','22'))
                ->groupby('customer_requirement_details.product_id')
                ->get();
            return view('inventory.index')->with(compact('user','active_order'));
        }
        else if($user->role_id=='8' || $user->role_id=='10') {
            $active_order = Amazon_inventory::selectRaw('amazon_inventories.*, sum(customer_requirement_details.no_boxs) as carton, sum(customer_requirement_details.total) as units')
                                            ->join('customer_requirement_details','customer_requirement_details.product_id','=','amazon_inventories.id')
                                            ->join('customer_requirements','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
                                            ->join('orders','customer_requirements.order_id','=','orders.order_id')
                                            ->whereBetween('orders.is_activated',array('0','11'))
                                            ->groupby('customer_requirement_details.product_id')
                                            ->get();
            return view('inventory.index')->with(compact('user','active_order'));
        }
    }
    public function create(){
        Amazon_inventory::where('hide_show','1')->update(array('hide_show'=>'0'));
        return redirect('supplychain');
    }
    public function store(Request $request){}
    public function show($id){
        $order_detail = Order::selectRaw('orders.*, amazon_inventories.product_name, amazon_inventories.sellerSKU, amazon_inventories.id as inventory_id, customer_requirement_details.no_boxs, customer_requirement_details.total, delivery.ETA_to_warehouse, customer_requirements.date_of_arrival_at_fbaforward')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->leftjoin('delivery','delivery.order_id','=','orders.order_id')
            ->where('orders.is_activated','>=','5')
            ->orWhere('orders.is_activated','0')
            ->where('customer_requirements.order_type','<>','1')
            ->where('customer_requirement_details.product_id',$id)
            ->get();
        return view('inventory.warehouse_show')->with(compact('order_detail'));
    }
    public function edit($id){
        $user= \Auth::user();
        $product = Amazon_inventory::where('id',$id)->first();
        $supplier = Supplier::where('user_id',$user->id)->get();
        $product_supplier = Supplier_product::where('product_id',$id)->get();
        return view('inventory.create_edit')->with(compact('supplier','product','product_supplier'));
    }
    public function update(Request $request, $id){
        $details = array('product_nick_name'=>$request->input('product_nick_name'));
        Amazon_inventory::where('id',$id)->update($details);
        Supplier_product::where('product_id',$id)->delete();
        foreach ($request->input('supplier') as $supplier) {
            $data = array('product_id'=>$id,
                'supplier_id'=>$supplier
            );
            Supplier_product::create($data);
        }
        return redirect('supplychain')->with('success', 'Supplier updated Successfully');
    }
    public function destroy($id, Request $request){
        $post = $request->all();
        $hide_show= $post['cnt'];
        if($hide_show=='1') {
            Amazon_inventory::where('id', $id)->update(array('hide_show' => '1'));
            return response()->json(['success' => 'Product has been hidden successfully']);
        }
        else {
            Amazon_inventory::where('id', $id)->update(array('hide_show' => '0'));
            return response()->json(['success' => 'Product has been unhidden successfully']);
        }
    }
    public function addnickname(Request $request){
        $id=$request->input('id');
        $nickname=$request->input('nickname');
        $list =Amazon_inventory::where('id','<>',$id)->where('product_nick_name',$nickname)->get();
        $role_id = \Auth::user()->role_id;

        if(count($list)>0) {
            if($role_id == '1'){
                return redirect('admin/inventories')->with('warning','Product Nick Name Already Assign To Another Product');
            }
            return redirect('inventory')->with('warning','Product Nick Name Already Assign To Another Product');
        }
        else {
            $data=array('product_nick_name'=>$nickname);
            Amazon_inventory::where('id',$id)->update($data);
            if($role_id == '1'){
                return redirect('admin/inventories')->with('success','Product Nick Name successfully changed');
            }
            return redirect('inventory')->with('success','Product Nick Name successfully changed');
        }
    }
    public function inventoryhistory(Request $request){
        $post=$request->all();
        $id= $post['id'];
        //$details =Order_inventory_history::whereRaw('id IN (select MAX(id) FROM order_inventory_histories GROUP BY status)')->where('product_id',$id)->where('status','<>','5')->Orderby('created_at','desc')->get();
        $details =Order_inventory_history::where('product_id',$id)->where('status','<>','5')->Orderby('created_at','desc')->get();
        echo json_encode(array('details'=>$details));
    }
}