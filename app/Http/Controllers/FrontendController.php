<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
class FrontendController extends Controller{
    public function index(){
        if(\Auth::user())
            return redirect('member/home');
        else
        return view('auth.login');
    }
    public function contactUsSubmit(Request $request){
        if($request->ajax()) {
            $post=$request->all();
            $name = $post['name'];
            $email = $post['email'];
            $subject = $post['subject'];
            $form_message = $post['message'];
            $to_email = \Config::get('app.contact_email');
            $data = array("name" => $name,
                "email" => $email,
                "subject" => $subject,
                "form_message" => $form_message,
                "to_email" => $to_email
            );
            \Mail::send('emails.contact', $data, function ($message) use ($data) {
                $message->to(\Config::get('app.contact_email'), $data['name'])
                    ->subject('Contact Form Message');
            });
            if(count(\Mail::failures()) > 0) {
                echo 'Mail could not send';
            }
            else {
                echo 'Thanks for contacting us!';
            }
        }
    }
}