<?php

namespace App\Http\Controllers;
use App\Service_agreement;
use App\User;
use Illuminate\Http\Request;
use CountryState;
use HelloSign;
use App\Hello_sign_detail;
class ServiceagreementController extends Controller
{
    public function index(){}
    public function store(Request $request){
        $user=\Auth::user();
        $signer='';
        if(empty($request->electronic_signature))
            $signer = $user->email;
        else
            $signer = $request->electronic_signature;
        $hellosign = Hello_sign_detail::first();
        $client = new HelloSign\Client($hellosign->sign_key);
        $sign_request = new HelloSign\SignatureRequest;
        $sign_request->enableTestMode();
        $sign_request->addSigner($signer, $signer);
        $sign_request->addFile(public_path() . '/uploads/test.pdf');
        $client_id = $hellosign->client_id;
        $embedded_request = new HelloSign\EmbeddedSignatureRequest($sign_request, $client_id);
        $response = $client->createEmbeddedSignatureRequest($embedded_request);
        $agreement_data = array('user_id' => $user->id,
            'signature_request_id' => $response->signature_request_id,
            'service_date' => date('Y-m-d',strtotime($request->service_date)),
            'response' => "Service Agreement",
            'status' => '0'
        );
        Service_agreement::create($agreement_data);
        User::where('id',$user->id)->update(array('service_complete'=>'1'));
    }
}