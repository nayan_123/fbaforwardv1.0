<?php
namespace App\Http\Controllers;
use App\Amazon_inventory;
use App\Order;
use App\Order_inventory_history;
use App\Outbound_method;
use Illuminate\Http\Request;
use App\Libraries;
class AmazoninventoryController extends Controller
{
    public function __construct(){}
    public function index(){}
    public function create(Request $request){
        $product_id=0;
        $user = \Auth::user();
        $user_role = $user->role_id;
        if($user->role_id=='3') {
            if (isset($request->product_id))
                $product_id = $request->product_id;
            $details = Amazon_inventory::where('id', $product_id)->first();
            $outbound_method = Outbound_method::get();
            return view('inventory.amazoninventory_create')->with(compact('product_id', 'details', 'outbound_method','user_role'));
        }
        else if($user->role_id=='1') {
            if (isset($request->product_id))
                $product_id = $request->product_id;
            $details = Amazon_inventory::where('id', $product_id)->first();
            $outbound_method = Outbound_method::get();
            return view('admin.inventories.amazoninventory_create')->with(compact('product_id', 'details', 'outbound_method','user_role'));
        }
        else if($user->role_id=='10') {
            if (isset($request->product_id))
                $product_id = $request->product_id;
            $details = Amazon_inventory::where('id', $product_id)->first();
            $order_detail = Order::selectRaw('orders.*, amazon_inventories.product_name, amazon_inventories.sellerSKU, amazon_inventories.id as inventory_id, order_inventory_histories.total, amazon_inventories.infbaforward, amazon_inventories.processfbaforward')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('order_inventory_histories','order_inventory_histories.order_id','=','orders.order_id')
                ->where('order_inventory_histories.status','1')
                ->where('customer_requirement_details.product_id',$product_id)
                ->where('orders.complete_order','0')
                ->where('orders.is_activated','>=','6')
                ->get();
            $outbound_method = Outbound_method::get();
            return view('inventory.amazoninventory_create')->with(compact('product_id', 'details', 'outbound_method','user_role','order_detail'));
        }
    }
    public function store(Request $request){
        $user = \Auth::user();
        if($user->role_id=='3') {
            $outbound = $request->input('outbound');
            $units = $request->input('units');
            $shipping_note = $request->input('shipping_note');
            $product_id = $request->input('product_id');
            $sku = $request->input('sku');
            $inbound_amazon = Amazon_inventory::where('id', $product_id)->first();
            $new_inbound = $inbound_amazon->inamazon + $units;
            $new_unit_storage = $inbound_amazon->atfbaforward - $units;
            Amazon_inventory::where('id', $product_id)->update(array('atfbaforward' => $new_unit_storage, 'inamazon' => $new_inbound));
        }
        else if($user->role_id=='10') {
            $cnt = $request->input('count');
            for($count=1;$count<$cnt;$count++) {
                $order_id=$request->input('order_id'.$count);
                $outbound = $request->input('outbound'.$order_id);
                $units = $request->input('units'.$order_id);
                $shipping_note = $request->input('shipping_note'.$order_id);
                $product_id = $request->input('product_id');
                $sku = $request->input('sku');
                $inbound_amazon = Amazon_inventory::where('id', $product_id)->first();
                $new_process = $inbound_amazon->processfbaforward+$units;
                $new_inbound_fba = $inbound_amazon->infbaforward-$units;
                Amazon_inventory::where('id', $product_id)->update(array('infbaforward' => $new_inbound_fba, 'processfbaforward' => $new_process));
                Order_inventory_history::create(array('order_id'=>$order_id,'product_id'=>$product_id,'total'=>$new_process,'status'=>'2'));
            }
        }
        return redirect('inventory')->with('success', 'Amazon inventory send successfully');
    }
}