<?php

namespace App\Http\Controllers;
use App\Listing_order;
use App\Listing_service;
use App\Listing_order_photo;
use App\Photo_order;
use App\Photo_service;
use Illuminate\Http\Request;
class ListOrderController extends Controller{
    public function index(){
        $listing_order_list = Listing_order::selectRaw('listing_orders.*, user_infos.company_name')
            ->join('user_infos','user_infos.user_id','=','listing_orders.user_id')
            ->get();
        $listing_order_status = array('Order Received','Order Processing','Details Requested','Order Pending Approval','Revision Requested','Listing Approved, Order Complete');
        return view('amazonservices.listlistingorder')->with(compact('listing_order_list','listing_order_status'));
    }
    public function create(){}
    public function store(Request $request){
        $id=$request->input('require_id');
        $lead_time = date('Y-m-d',strtotime($request->input('lead_time')));
        Listing_order::where('id',$id)->update(array('lead_time'=>$lead_time, 'is_activated'=>'1'));
        return redirect('listingoptimization/'.$id);
    }
    public function show($id){
        $title="Amazon Listing Order Details";
        $listing_order = Listing_order::selectRaw('listing_orders.*, user_infos.company_name,amazon_orders.order_id as amazon_order')
            ->join('user_infos','user_infos.user_id','=','listing_orders.user_id')
            ->join('amazon_orders','amazon_orders.id','=','listing_orders.amazon_order_id')
            ->where('listing_orders.id',$id)->first()->toArray();
        $photo_service = Photo_service::get();
        $photo_order = Photo_order::where('amazon_order_id',$listing_order['amazon_order_id'])->first()->toArray();
        $listing_service = Listing_service::get();
        $listing_order_photos = Listing_order_photo::where('listing_order_id',$id)->get();
        $listing_order_status = array('Order Received','Order Processing','Details Requested','Order Pending Approval','Revision Requested','Listing Approved, Order Complete');
        return view('amazonservices.detail_listing_order')->with(compact('title','listing_order','photo_order','photo_service','listing_order_status','listing_service','listing_order_photos'));
    }
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
}