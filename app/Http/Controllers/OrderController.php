<?php
namespace App\Http\Controllers;
use App\Damage;
use App\Order_warehouse_hold;
use App\Outbound_method;
use App\Paypal_vault_detail;
use App\Prealert_detail;
use App\Question;
use Illuminate\Http\Request;
use App\Order;
use App\Shipping_method;
use App\Customer_requirement;
use App\Photo_service;
use App\Listing_service;
use App\Photo_list_detail;
use App\Listing_service_detail;
use App\User_credit_cardinfo;
use App\Customer_requirement_method;
use App\Prep_requirement;
use App\Shipping_quote_charge;
use Auth;
use Redirect;
use App\Shipping_quote;
use App\Order_document;
use App\ZohoDetail;
use App\QuoteHistory;
use App\OrderHistory;
use PayPal\Api\CreditCard;
use PayPal\Api\Amount;
use PayPal\Api\CreditCardToken;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use App\Payment_info;
use App\Amazon_inventory;
use App\Customer_requirement_detail;
use App\Supplier;
use PDF;
use App\Prep_service;
use App\PreShipment;
use App\Customer_amazon_detail;
use App\Document;
use App\Order_note;
use App\Warehouse_hold_type;
use App\Customer_quickbook_detail;
use App\User;

class OrderController extends Controller{
    private $IntuitAnywhere;
    private $context;
    private $realm;
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request){
        $title= "Orders";
        $user=\Auth::user();
        $user_role = $user->role_id;
        if($user->role_id=='5') {
            $order = Order::selectRaw('orders.*,user_infos.company_name,suppliers.contact_name as supplier_company_name,amazon_inventories.product_name,customer_requirements.port_of_origin,customer_requirements.goods_ready_date,shipment_types.name as shipment_type_name,shipment_types.short_name as shipment_type_short_name,customer_requirements.id,customer_requirements.port_of_origin,customer_requirements.shipper_destination,prealert_details.ETD_china,prealert_details.created_at as prealert_details_ctreated_at')
                ->join('user_infos','user_infos.user_id','=','orders.user_id')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('suppliers','suppliers.supplier_id','=','customer_requirements.supplier')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type')
                ->join('prealert_details','prealert_details.order_id','=','orders.order_id','left')
                ->orderBy('orders.order_id','desc')
                ->get()->toArray();
            $shipping_method = Shipping_method::selectRaw('shipping_methods.*, customer_requirements.id')
                ->join('customer_requirement_methods','customer_requirement_methods.shipping_method_id','=','shipping_methods.shipping_method_id')
                ->join('customer_requirements','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                ->join('orders','customer_requirements.order_id','=','orders.order_id')
                ->get()->toArray();
           $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
            return view('order.shipperorder')->with(compact('orderStatus','title','order'));
        }
        else if($user->role_id=='6'){
            $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated,orders.is_activated as order_is_activate,customer_requirement_methods.shipping_method_id')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->where('customer_requirement_methods.is_activated','1')
                ->whereBetween('orders.is_activated',array('3','4','5'))
                ->orderBy('orders.order_id','desc')
                ->get()->toArray();
            $past_order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated,orders.is_activated as order_is_activate,customer_requirement_methods.shipping_method_id')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                ->leftjoin('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->whereBetween('customer_requirement_methods.is_activated',array('1','6'))
                ->orderBy('orders.order_id','desc')
                ->get()->toArray();
            $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
            return view('order.index')->with(compact('title','order','orderStatus','user_role','past_order'));
        }
        else if($user->role_id=='3'){
            $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,orders.is_activated,customer_requirement_methods.shipping_method_id')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                ->leftjoin('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->where('customer_requirement_methods.is_activated','1')
                ->where('orders.user_id',$user->id)
                ->orderBy('orders.order_id','desc')
                ->get()->toArray();
            $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
            return view('order.index')->with(compact('title','order','orderStatus','user_role'));
        }
        else if($user->role_id=='8'){
            $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated as method_activated,customer_requirement_methods.shipping_method_id, user_infos.company_name')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('user_infos','user_infos.user_id','=','orders.user_id')
                ->where('customer_requirement_methods.is_activated','1')
                ->where('orders.is_activated','>=','16')
                ->orderBy('orders.order_id','desc')
                ->get()->toArray();
            $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
            return view('order.warehouseorder')->with(compact('title','order','orderStatus','user_role'));
        }
        else if($user->role_id=='10'){
            $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated as method_activated,customer_requirement_methods.shipping_method_id, user_infos.company_name')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('user_infos','user_infos.user_id','=','orders.user_id')
                ->where('customer_requirement_methods.is_activated','1')
                ->where('orders.is_activated','>=','15')
                ->orWhere('orders.is_activated','0')
                ->where('customer_requirements.order_type','2')
                ->where('customer_requirement_methods.is_activated','1')
                ->orderBy('orders.order_id','desc')
                ->get()->toArray();
            $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
            return view('order.index')->with(compact('title','order','orderStatus','user_role'));
        }
        else{
            $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated,customer_requirement_methods.shipping_method_id')
                      ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                      ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                      ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                      ->leftjoin('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.customer_requirement_id')
                      ->whereBetween('customer_requirement_methods.is_activated',array('0','11'))
                      ->orderBy('orders.order_id','desc')
                      ->get()->toArray();
            $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
            return view('order.index')->with(compact('title','order','orderStatus','user_role'));
        }
    }
    public function create(Request $request){
        $data['place_order'] = Customer_requirement::select('customer_requirements.*','amazon_inventories.product_name as product_name','amazon_inventories.product_nick_name as product_nick_name','customer_requirement_details.total as units','customer_requirement_details.no_boxs as cartoons','customer_requirement_details.product_id','customer_requirements.shipment_weight as weight','customer_requirements.shipment_volume as volume','shipment_types.name as type','shipment_types.short_name as shipment_short_name', 'incoterms.name as incoterms','incoterms.short_name as incoterm_short_name','customer_requirements.port_of_origin','customer_requirements.goods_ready_date as apporx_goods_ready_date','suppliers.contact_name as supplier_name','outbound_methods.outbound_name as outbound_shipping_method','outbound_methods.short_name as outbound_short_name','customer_requirement_details.product_name as customer_requirement_details_product_name','customer_requirement_details.id as customer_requirement_details_id','customer_requirement_details.length','customer_requirement_details.width','customer_requirement_details.height','customer_requirement_details.unit','customer_requirements.units as customer_requirement_units')
                                ->leftjoin('customer_requirement_details', 'customer_requirements.id', '=', 'customer_requirement_details.customer_requirement_id')
                                ->leftjoin('shipment_types','customer_requirements.shipment_size_type', '=','shipment_types.id')
                                ->leftjoin('incoterms','customer_requirements.incoterms', '=','incoterms.id')
                                ->leftjoin('outbound_methods','customer_requirements.outbound_method_id', '=','outbound_methods.outbound_method_id')
                                ->leftjoin('suppliers','customer_requirements.supplier','=','suppliers.supplier_id')
                                ->leftjoin('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id','left')
                                ->where('customer_requirements.id',$request->id)
                                ->first();
        $data['shipping_method'] = Shipping_method::where('shipping_method_id',$request->method_id)->first();
        $data['photo_services'] = Photo_service::all();
        $data['listing_services'] = Listing_service::all();
        $data['customer_requirement_id'] = $request->id;
        $data['outbound_method'] =Outbound_method::all();
        //$user = \Auth::user();
        $user_id = $data['place_order']->user_id;
        $data['suppliers'] = Supplier::where('user_id', $user_id)->where('is_activated','0')->get();
        $data['product'] = Amazon_inventory::where('hide_show','0')->where('user_id', $user_id)->get();
        $data['customer_amazon_detail'] = Customer_amazon_detail::where('user_id',$user_id)->first();
        if(Auth::user()->role_id == 1){
            return view('admin.orders.create',$data);
        }

        return view('order.create',$data);
    }
    public function updateOrder(Request $request){
        if($request->update_products){
            Customer_requirement::where('id',$request->customer_requirement_id)
                        ->update([
                            'outbound_method_id' => $request->outbound,
                            'supplier' => $request->supplier,
                            'goods_ready_date' => date('Y-m-d', strtotime($request->good_ready_date)),
                            'shipment_notes' => $request->shipment_notes
                        ]);
            $product = explode(" ", $request->input('product'));
            Customer_requirement_detail::where('id',$request->customer_requirement_id)
                    ->update([
                        'product_id' => $product[0],
                        'fnsku' => $product[1],
                        'product_name' => $request->product_name
                    ]);
        }
        if($request->customer_requirement_details_id){
            $product = explode(" ", $request->input('product'));
            Customer_requirement_detail::where('id',$request->customer_requirement_details_id)
                        ->update([
                            'product_id' => $product[0],
                            'fnsku' => $product[1]
                        ]);
        }
        $date_of_arrival = \Carbon\Carbon::parse($request->date_of_arrival)->format('Y-m-d h:i:s');
        if($request->photo_service_id){
            foreach ($request->photo_service_id as $key => $value) {
                if($request->quantity[$key] != null){
                    Photo_list_detail::create([
                        'customer_requirement_id' => $request->customer_requirement_id,
                        'shipping_method_id' => $request->shipping_method,
                        'photo_service_id' => $request->photo_service_id[$key],
                        'quantity' => $request->quantity[$key]
                    ]);
                }
            }
        }
        if($request->listing_service_checkbox){
            foreach ($request->listing_service_checkbox as $key => $value) {
                Listing_service_detail::create([
                    'customer_requirement_id' => $request->customer_requirement_id,
                    'shipping_method_id' => $request->shipping_method,
                    'listing_service_id' => $request->listing_service_id[$key],
                ]);
            }
        }
        Customer_requirement::where('id',$request->customer_requirement_id)->update(['date_of_arrival_at_fbaforward' => $date_of_arrival]);
        QuoteHistory::create([
                'quote_history_status' => 3,
                'customer_requirement_id' => $request->customer_requirement_id
            ]);
        $data['id'] = $request->customer_requirement_id;
        $data['method'] = $request->shipping_method;
        return Redirect::route('place_order',$data);
    }
    public function placeOrder(){
        $customer_requirement_id= $_GET['id'];
        $shipping_method= $_GET['method'];
        $data['place_order'] = Customer_requirement::select('customer_requirements.*','amazon_inventories.product_name as product_name','amazon_inventories.product_nick_name as product_nick_name','customer_requirement_details.total as units','customer_requirement_details.no_boxs as cartoons','customer_requirement_details.total', 'customer_requirements.shipment_weight as weight','customer_requirements.shipment_volume as volume','shipment_types.name as type', 'incoterms.name as incoterms','incoterms.short_name as incoterm_short_name','customer_requirements.port_of_origin','customer_requirements.goods_ready_date as apporx_goods_ready_date','suppliers.contact_name as supplier_name','outbound_methods.outbound_name as outbound_shipping_method','outbound_methods.short_name as outbound_short_name','shipment_types.rate as shipment_type_rate','shipment_types.trucking_rate','customer_requirement_details.qty_per_box', 'shipping_methods.shipping_name','shipping_methods.shipping_method_id','customer_requirement_details.product_name as customer_requirement_details_product_name','customer_requirement_details.length','customer_requirement_details.width','customer_requirement_details.height','customer_requirement_details.unit', 'shipment_types.short_name as shipment_short_name')
                                ->leftjoin('customer_requirement_details', 'customer_requirements.id', '=', 'customer_requirement_details.customer_requirement_id')
                                ->leftjoin('shipment_types','customer_requirements.shipment_size_type', '=','shipment_types.id')
                                ->leftjoin('incoterms','customer_requirements.incoterms', '=','incoterms.id')
                                ->leftjoin('outbound_methods','customer_requirements.outbound_method_id', '=','outbound_methods.outbound_method_id')
                                ->leftjoin('suppliers','customer_requirements.supplier','=','suppliers.supplier_id')
                                ->join('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id','left')
                                ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                                ->where('customer_requirements.id',$customer_requirement_id)
                                ->where('customer_requirement_methods.shipping_method_id',$shipping_method)
                                ->first();
        $data['detail_charges'] = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
            ->join('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id')
            ->join('charges','charges.id','=','shipping_quote_charges.charge_id')
            ->where('shipping_quotes.customer_requirement_id',$customer_requirement_id)
            ->where('shipping_quotes.shipping_method_id',$shipping_method)
            ->where('shipping_quotes.is_activated','0')
            ->get();
        $data['detail_prep'] = Prep_requirement:: selectRaw('prep_services.service_name, prep_services.price, prep_requirements.*')
            ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
            ->where('customer_requirement_id',$customer_requirement_id)->get();
        $data['shipping_method'] = Shipping_method::where('shipping_method_id',$shipping_method)->first();
        $data['listing_services'] = Listing_service_detail::where('customer_requirement_id',$customer_requirement_id)
                                ->where('shipping_method_id',$shipping_method)
                                ->join('listing_services','listing_service_details.listing_service_id','=','listing_services.listing_service_id')
                                ->get();
        $data['photo_services'] = Photo_list_detail::where('customer_requirement_id',$customer_requirement_id)
                                ->where('shipping_method_id',$shipping_method)
                                ->join('photo_services','photo_list_details.photo_service_id','=','photo_services.id')
                                ->get();
        $data['cards'] = User_credit_cardinfo::where('user_id',$data['place_order']->user_id)->get();
        
        if(Auth::user()->role_id == '1'){
            return view('admin.orders.place_order',$data);
        }

        return view('order.place_order',$data);
    }
    public function store(Request $request)
    {
        if(!$request->credit_card){
            return back()->withErrors('Please Select card first.');
        }
        $post = $request->all();
        $id = $post['customer_requirement_id'];
        $method_id= $post['method_id'];
        $user = \Auth::user();
        $user_id=$user->id;
        $credit_card = $request->input('credit_card');
        $credit_card_detail = User_credit_cardinfo::where('id',$credit_card)->first();
        $paypal_detail = Paypal_vault_detail::first();
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $paypal_detail->client_id,
                $paypal_detail->secret_key
            )
        );
        $creditCardToken = new CreditCardToken();
        $creditCardToken->setCreditCardId($credit_card_detail->credit_card_id);
        $fi = new FundingInstrument();
        $fi->setCreditCardToken($creditCardToken);
        $payer = new Payer();
        $payer->setPaymentMethod("credit_card")
            ->setFundingInstruments(array($fi));
        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($request->input('order_total'));
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setDescription("Order Deposite")
            ->setInvoiceNumber(uniqid());
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions(array($transaction));
        try {
            $payment->create($apiContext);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            return Redirect::back()->withErrors('Something was wrong, Payment not completed');
            /*echo $ex->getCode(); // Prints the Error Code
            echo $ex->getData();
            die($ex);*/
        } catch (Exception $ex) {
            ResultPrinter::printError("Create Payment using Saved Card", "Payment", null, $request, $ex);
            exit(1);
        }
        if($post['order_type']=='3')
        {
            $order_detail = array(
                'user_id' => $user_id,
                'is_activated' => '9'
            );
        }
        else {
            $order_detail = array(
                'user_id' => $user_id,
                'is_activated' => '0'
            );
        }
        $order = new Order($order_detail);
        $order->save();
        Order::where('order_id',$order->order_id)->update(['order_no' => (24000 + $order->order_id)]);
        $order_id = $order->order_id;
        $payment_info = array('order_id' => $order_id,
            'transaction' => $payment,
            'transaction_type'=>'1',
            'amount'=>$request->input('order_total'),
        );
        Payment_info::create($payment_info);
        $customer_requirement = array('order_id'=>$order_id,'is_activated'=>'1','expire_date'=>null);
        Customer_requirement::where('id',$id)->update($customer_requirement);
        $method = array('is_activated'=>'1');
        Customer_requirement_method::where('customer_requirement_id',$id)->where('shipping_method_id',$method_id)->update($method);
        Shipping_quote::where('customer_requirement_id',$id)->where('shipping_method_id',$method_id)->update($method);
        //$this->createCustomer($order_id);
        $user->newNotification()
            ->withType('New Order')
            ->withSubject('You are placing new order')
            ->withBody('You are placing new order')
            ->regarding($order_detail)
            ->deliver();
        QuoteHistory::create([
            'quote_history_status' => 3,
            'customer_requirement_id' => $id
        ]);
        return redirect('order')->with('success', 'Your new order placed successfully');
    }
    public function show($id){}
    public function edit($method_id,$customer_requirement_id){
        $data['prep_service']  = Prep_service::get();
        $data['prep_detail'] = Prep_requirement::selectRaw('prep_id')->where('customer_requirement_id',$customer_requirement_id)->get()->toArray();
        $data['prep_detail'] = array_map('current',$data['prep_detail']);
        $data['pre_shipment'] = PreShipment::where('customer_requirement_id',$customer_requirement_id)->get();
        $data['method_id'] = $method_id;
        $data['customer_requirement_id'] = $customer_requirement_id;
        return view('order.edit_order',$data);
    }
    public function update(Request $request){
        if($request->Factory_Audit){
            PreShipment::where('customer_requirement_id',$request->customer_requirement_id)
                        ->where('name' ,'Factory Audit')
                            ->update([
                                'rate' => $request->Factory_Audit
                            ]);
        }
        if($request->Pre_Inspection){
            PreShipment::where('customer_requirement_id',$request->customer_requirement_id)
                    ->where('name' ,'Pre Inspection')
                        ->update([
                            'rate' => $request->Pre_Inspection
                        ]);
        }
        if($request->prep_service){
            Prep_requirement::where('customer_requirement_id',$request->customer_requirement_id)->delete();
            foreach ($request->prep_service as $prep_services) {
                Prep_requirement::create([
                    'customer_requirement_id' => $request->customer_requirement_id,
                    'prep_id' => $prep_services,
                    'is_activated' => 0
                ]);
            }
        }
        return back();
    }
    public function destroy($id){}
    public function viewOrderDetail($method_id,$customer_requirement_id){
        $details = Customer_requirement::selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, customer_requirement_details.total, outbound_methods.outbound_name, outbound_methods.short_name as outbound_method_short_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.id as incoterm_id, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id,suppliers.contact_name,prealert_details.*,user_infos.zoho_id,user_infos.user_id,customer_requirement_details.product_name as customer_requirement_details_product_name, prealert_details.id as prealert_id, orders.hold, customer_requirement_methods.shipping_method_id, customer_clearances.created_at as custom_release_date, delivery.ETA_to_warehouse, delivery.created_at as cargo_release, delivery.id as delivery_id, warehouse_checkins.created_at as checkin_date, warehouse_checkins.id as checkin_id,orders.order_id,orders.order_no,orders.is_activated as order_status, amazon_destinations.destination_name')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id','left')
            ->leftjoin('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
            ->leftjoin('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type','left')
            ->leftjoin('incoterms','incoterms.id','=','customer_requirements.incoterms','left')
            ->leftjoin('suppliers','customer_requirements.supplier','=','suppliers.supplier_id','left')
            ->leftjoin('prealert_details','prealert_details.order_id','=','customer_requirements.order_id','left')
            ->leftjoin('customer_clearances','customer_clearances.order_id','=','customer_requirements.order_id','left')
            ->leftjoin('delivery','delivery.order_id','=','customer_requirements.order_id','left')
            ->leftjoin('warehouse_checkins','warehouse_checkins.order_id','=','customer_requirements.order_id','left')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id','left')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id','left')
            ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
            ->join('orders','orders.order_id','=','customer_requirements.order_id')
            ->leftjoin('amazon_destinations','amazon_destinations.order_id','=','orders.order_id')
            ->where('customer_requirements.id',$customer_requirement_id)
            ->where('customer_requirement_methods.shipping_method_id',$method_id)
            ->groupby('customer_requirement_methods.customer_requirement_id');
        if(!empty($method_id)) {
            $details->selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, shipping_methods.shipping_name, shipping_methods.shipping_method_id, shipping_quotes.chargeable_weight,shipping_quotes.shipping_port');
            $details->leftjoin('shipping_quotes',function($join) {
                $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
            });
        }
        $data['order']=$details->first();
        $data['shipping_method'] = Shipping_method::where('shipping_method_id',$method_id)->first();
        $data['detail_charge'] = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
            ->join('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id','left')
            ->join('charges','charges.id','=','shipping_quote_charges.charge_id','left')
            ->where('shipping_quotes.customer_requirement_id',$customer_requirement_id)
            ->where('shipping_quotes.shipping_method_id',$method_id)
            ->where('shipping_quotes.is_activated','0')
            ->get();
        $data['detail_prep'] = Prep_requirement:: selectRaw('prep_services.service_name, prep_services.price, prep_requirements.*')
            ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
            ->where('customer_requirement_id',$customer_requirement_id)->get();
        $data['order_documents'] = Order_document::where('order_id',$data['order']['order_id'])->get();
        $data['user_role'] = Auth::user()->role_id;
        $data['order_histories'] = OrderHistory::selectRaw('order_history.*,prealert_details.ETD_china,prealert_details.created_at as prealert_details_created_at,customer_requirements.port_of_origin, customer_requirements.shipper_destination,customer_requirements.id as customer_requirements_id')
                                ->join('prealert_details','prealert_details.order_id','=','order_history.order_id','left')
                                ->join('customer_requirements','customer_requirements.order_id','=','order_history.order_id','left')
                                ->where('order_history.order_id',$data['order']['id'])
                                ->get();
        $data['quote_histories'] = QuoteHistory::where('customer_requirement_id',$customer_requirement_id)->whereIN('quote_history_status',['3','5'])->get();
        $data['listing_services'] = Listing_service_detail::where('customer_requirement_id',$customer_requirement_id)
            ->where('shipping_method_id',$method_id)
            ->join('listing_services','listing_service_details.listing_service_id','=','listing_services.listing_service_id')
            ->get();
        $data['photo_services'] = Photo_list_detail::where('customer_requirement_id',$customer_requirement_id)
            ->where('shipping_method_id',$method_id)
            ->join('photo_services','photo_list_details.photo_service_id','=','photo_services.id')
            ->get();
        $data['pre_shipment'] = PreShipment::where('customer_requirement_id',$customer_requirement_id)->get();
        $data['method_id'] = $method_id;
        $data['customer_requirement_id'] = $customer_requirement_id;
        $order_id = $data['order']['order_id'];
        $data['documents'] = Document::selectRaw('documents.*,documents.document as document_name, order_documents.order_id,order_documents.status,order_documents.document_id,order_documents.created_at as order_document_created_at,order_documents.id as order_document_id,order_documents.due_date');
        $data['documents']->leftjoin('order_documents',function($join) use ($order_id){
            $join->on('order_documents.document_id','=','documents.id');
            $join->where('order_documents.order_id','=', $order_id);
        });
        $data['documents'] = $data['documents']->get();
        $warehouse_hold = Order_warehouse_hold::selectRaw('order_warehouse_holds.*, warehouse_hold_types.name as hold_type')
                                            ->leftjoin('warehouse_hold_types','warehouse_hold_types.id','=','order_warehouse_holds.hold_type_id')
                                            ->where('order_warehouse_holds.order_id',$order_id)
                                            ->get();
        $warehouse_hold_detail =array();
        foreach ($warehouse_hold as $key=>$warehouse_holds) {
            if($warehouse_holds->hold_type_id=='0'){
                $warehouse_hold_detail[$key]['id'] = $warehouse_holds->id;
                $warehouse_hold_detail[$key]['name'] = $warehouse_holds->detail_text;
                $warehouse_hold_detail[$key]['status'] = $warehouse_holds->is_activated;
                $warehouse_hold_detail[$key]['hold_type'] = $warehouse_holds->hold_type_text;
                $warehouse_hold_detail[$key]['cost'] = $warehouse_holds->cost;
                $warehouse_hold_detail[$key]['notes'] = $warehouse_holds->notes;
                $warehouse_hold_detail[$key]['numbers'] = $warehouse_holds->no_of_unit;
                $warehouse_hold_detail[$key]['updated_at'] = $warehouse_holds->updated_at;
            }
            elseif($warehouse_holds->hold_type_id=='1') {
                $prep = Prep_service::selectRaw('service_name as name')->where('prep_service_id',$warehouse_holds->detail_id)->get()->toArray();
                $prep = array_map('current',$prep);
                $warehouse_hold_detail[$key]['id'] = $warehouse_holds->id;
                $warehouse_hold_detail[$key]['name'] = $prep[0];
                $warehouse_hold_detail[$key]['status'] = $warehouse_holds->is_activated;
                $warehouse_hold_detail[$key]['hold_type'] = $warehouse_holds->hold_type;
                $warehouse_hold_detail[$key]['cost'] = $warehouse_holds->cost;
                $warehouse_hold_detail[$key]['notes'] = $warehouse_holds->notes;
                $warehouse_hold_detail[$key]['numbers'] = $warehouse_holds->no_of_unit;
                $warehouse_hold_detail[$key]['updated_at'] = $warehouse_holds->updated_at;
            }
            elseif ($warehouse_holds->hold_type_id=='2'){
                $damage = Damage::selectRaw('name as name')->where('id',$warehouse_holds->detail_id)->get()->toArray();
                $damage = array_map('current',$damage);
                $warehouse_hold_detail[$key]['id'] = $warehouse_holds->id;
                $warehouse_hold_detail[$key]['name'] = $damage[0];
                $warehouse_hold_detail[$key]['status'] = $warehouse_holds->is_activated;
                $warehouse_hold_detail[$key]['hold_type'] = $warehouse_holds->hold_type;
                $warehouse_hold_detail[$key]['cost'] = $warehouse_holds->cost;
                $warehouse_hold_detail[$key]['notes'] = $warehouse_holds->notes;
                $warehouse_hold_detail[$key]['numbers'] = $warehouse_holds->no_of_unit;
                $warehouse_hold_detail[$key]['updated_at'] = $warehouse_holds->updated_at;
            }
            elseif ($warehouse_holds->hold_type_id=='3'){
                $question = Question::selectRaw('name as name')->where('id',$warehouse_holds->detail_id)->get()->toArray();
                $question = array_map('current',$question);
                $warehouse_hold_detail[$key]['id'] = $warehouse_holds->id;
                $warehouse_hold_detail[$key]['name'] = $question[0];
                $warehouse_hold_detail[$key]['status'] = $warehouse_holds->is_activated;
                $warehouse_hold_detail[$key]['hold_type'] = $warehouse_holds->hold_type;
                $warehouse_hold_detail[$key]['cost'] = $warehouse_holds->cost;
                $warehouse_hold_detail[$key]['notes'] = $warehouse_holds->notes;
                $warehouse_hold_detail[$key]['numbers'] = $warehouse_holds->no_of_unit;
                $warehouse_hold_detail[$key]['updated_at'] = $warehouse_holds->updated_at;
            }
            elseif($warehouse_holds->hold_type_id=='4'){
                $warehouse_hold_detail[$key]['id'] = $warehouse_holds->id;
                $warehouse_hold_detail[$key]['name'] = $warehouse_holds->detail_text;
                $warehouse_hold_detail[$key]['status'] = $warehouse_holds->is_activated;
                $warehouse_hold_detail[$key]['hold_type'] = $warehouse_holds->hold_type;
                $warehouse_hold_detail[$key]['cost'] = $warehouse_holds->cost;
                $warehouse_hold_detail[$key]['notes'] = $warehouse_holds->notes;
                $warehouse_hold_detail[$key]['numbers'] = $warehouse_holds->no_of_unit;
                $warehouse_hold_detail[$key]['updated_at'] = $warehouse_holds->updated_at;
            }
        }
        $data['warehouse_hold_detail']= $warehouse_hold_detail;
        $data['warehouse_hold_type'] = Warehouse_hold_type::all();
        $data['orderStatus'] = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('order.view_order',$data);
    }
    public function uploadDocument(Request $request){
        for($i=1; $i < $request->count + 1 ; $i++) {
            $document_id = $request->input('document_id_'.$i);
            $document = Document::where('id',$document_id)->first();
            $document_name = str_replace(' ', '_',$document->document);
            $newname = $request->order_id.'_'.$document_name.'.'.$request->file('document_'.$i)->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/documents';
            $request->file('document_'.$i)->move($destinationPath, $newname);
            Order_document::create([
                'order_id' => $request->order_id,
                'status' => '0',
                'document' => $newname,
                'document_id' => $document_id
            ]);
        }
        return back();
    }
    public function uploadTrackingNumber(Request $request){
        if($request->order_id){
            Order::where('order_id',$request->order_id)->update(['tracking_number' => $request->tracking_number]);
        }
        return back();
    }
    public function addNote(Request $request){
        $token = $this->check_token();
        $note = str_replace(' ', '%20', $request->note);
        if($request->zoho_id){
            $response = $this->api("https://crm.zoho.com/crm/private/xml/Notes/insertRecords?newFormat=1&authtoken=".$token."&scope=crmapi&xmlData=%3CNotes%3E%3Crow%20no%3D%22".$request->order_id."%22%3E%3CFL%20val%3D%22entityId%22%3E".$request->zoho_id."%3C%2FFL%3E%3CFL%20val%3D%22Note%20Title%22%3EZoho%20CRM%20Sample%20Note%3C%2FFL%3E%3CFL%20val%3D%22Note%20Content%22%3E".$note."%3C%2FFL%3E%3C%2Frow%3E%3C%2FNotes%3E");
            $data = simplexml_load_string($response);
            Order::where('order_id',$request->order_id)->update(['note_id' => $data->result->recorddetail->FL['0'],'note' => $request->note]);
        }
        return back();
    }
    public function approvewarehouse(Request $request){
        $order_id= $request->order_id;
        Order::where('order_id',$order_id)->update(array('hold'=>'1','is_activated'=>'7'));
        return redirect('order')->with('success', 'Order Approve for Warehouse Successfully');
    }
    public function authGenerate(){
        $auth = $this->api('https://accounts.zoho.com/apiauthtoken/nb/create?SCOPE=ZohoCRM%2Fcrmapi&EMAIL_ID=webdimensionsindia%40gmail.com&PASSWORD=webdimensions%40123&DISPLAY_NAME=webdimensionsindia');
        $arr = explode("\n", $auth);
        $str = strstr($arr[2], '=');
        $token = ltrim($str, '=');
        if($arr[3] == "RESULT=TRUE"){
            $zoho_detail = ZohoDetail::create([
                'auth_token' => $token
            ]);
        }
        else{
            dd('Auth already generated.');
        }
       return back();
    }
    public function check_token(){
        $zoho_detail = ZohoDetail::first();
        if(!$zoho_detail){
            $this->authGenerate();
            $zoho_detail = ZohoDetail::first();
        }
        return $zoho_detail->auth_token;
    }
    public function api($url){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $response;
        }
    }
    public function printOrder($method_id,$customer_requirement_id,$file){
        $details = Customer_requirement::selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, customer_requirement_details.total, outbound_methods.outbound_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.id as incoterm_id, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id,suppliers.contact_name,prealert_details.*,user_infos.zoho_id,user_infos.user_id,customer_requirement_details.product_name as customer_requirement_details_product_name, prealert_details.id as prealert_id, orders.hold, customer_requirement_methods.shipping_method_id, customer_clearances.created_at as custom_release_date, delivery.ETA_to_warehouse, delivery.created_at as cargo_release, delivery.id as delivery_id, warehouse_checkins.created_at as checkin_date, warehouse_checkins.id as checkin_id,user_infos.company_name')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id','left')
            ->leftjoin('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
            ->leftjoin('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type','left')
            ->leftjoin('incoterms','incoterms.id','=','customer_requirements.incoterms','left')
            ->leftjoin('suppliers','customer_requirements.supplier','=','suppliers.supplier_id','left')
            ->leftjoin('prealert_details','prealert_details.order_id','=','customer_requirements.order_id','left')
            ->leftjoin('customer_clearances','customer_clearances.order_id','=','customer_requirements.order_id','left')
            ->leftjoin('delivery','delivery.order_id','=','customer_requirements.order_id','left')
            ->leftjoin('warehouse_checkins','warehouse_checkins.order_id','=','customer_requirements.order_id','left')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id','left')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id','left')
            ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
            ->join('orders','orders.order_id','=','customer_requirements.order_id')
            ->where('customer_requirements.id',$customer_requirement_id)
            ->where('customer_requirement_methods.shipping_method_id',$method_id)
            ->groupby('customer_requirement_methods.customer_requirement_id');
        if(!empty($method_id)) {
            $details->selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, shipping_methods.shipping_name, shipping_methods.shipping_method_id, shipping_quotes.chargeable_weight,shipping_quotes.shipping_port');
            $details->leftjoin('shipping_quotes',function($join) {
                $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
            });
        }
        $data['order']=$details->first();
        $data['detail_charge'] = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
            ->join('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id','left')
            ->join('charges','charges.id','=','shipping_quote_charges.charge_id','left')
            ->where('shipping_quotes.customer_requirement_id',$customer_requirement_id)
            ->where('shipping_quotes.shipping_method_id',$method_id)
            ->where('shipping_quotes.is_activated','0')
            ->get();
        $data['detail_prep'] = Prep_requirement:: selectRaw('prep_services.service_name, prep_services.price, prep_requirements.*')
            ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
            ->where('customer_requirement_id',$customer_requirement_id)->get();
        $data['listing_services'] = Listing_service_detail::where('customer_requirement_id',$customer_requirement_id)
            ->where('shipping_method_id',$method_id)
            ->join('listing_services','listing_service_details.listing_service_id','=','listing_services.listing_service_id')
            ->get();
        $data['photo_services'] = Photo_list_detail::where('customer_requirement_id',$customer_requirement_id)
            ->where('shipping_method_id',$method_id)
            ->join('photo_services','photo_list_details.photo_service_id','=','photo_services.id')
            ->get();
        $data['charges'] = Shipping_quote_charge::selectRaw('shipping_quote_charges.*,shipping_quotes.id as shipping_quote_id,shipping_quotes.customer_requirement_id,charges.name')
                        ->leftjoin('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.id')
                        ->leftjoin('charges','charges.id','=','shipping_quote_charges.charge_id')
                        ->where('shipping_quotes.customer_requirement_id',$customer_requirement_id)
                        ->get();
        $data['shipping_quote'] = Shipping_quote::select('shipping_port')->where(['customer_requirement_id' => $customer_requirement_id,'shipping_method_id' => $method_id,'is_activated' => 1])->first();
        $data['sum_custom_total'] = 0;
        $charge_qty = 1;
        if($data['order']->units==1){
            $trucking_qty=round($data['order']->shipment_volume / env('PALLET_CBM'));
        }
        else{
            $trucking_qty=round($data['order']->shipment_volume / env('PALLET_CFT'));
        }
        $data['sum_shipping_total'] = 0;
        $data['sum_receive_forward_total'] = 0;
        $pallet_charge= env('pallet_charge');
        $data['sum_prep_total'] = 0;
        $entry_rate= env('ENTRY_RATE');
        $bond_rate =  env('BOND_RATE');
        $isf_rate =  env('ISF_RATE');
        $data['sum_pre_shipment'] = 0;
        if($trucking_qty > env('TRUCKING_QTY')){
            $trucking_price = ($trucking_qty -  env('TRUCKING_QTY')) * env('TRUCKING_ADDITIONAL_PRICE') + env('TRUCKING_PRICE');
        }
        else{
            $trucking_price =  env('TRUCKING_PRICE');
        }

        //Sum custom
        if(($data['order']->order_type == 1) || ($data['order']->order_type == 3)){
            if($data['order']->shipping_method_id == 1){
                $data['sum_custom_total'] += ($isf_rate * $charge_qty);
            }
            $data['sum_custom_total'] += ($entry_rate*$charge_qty);
            $data['sum_custom_total'] += ($bond_rate*$charge_qty);
        }
        //End Sum custom
        //Sum shipping
        if($data['order']->order_type == 1){

            if($data['order']->shipment_type_id==1){
                if($data['order']->units==1){
                    $trucking_qty=round($data['order']->shipment_volume / env('PALLET_CBM'));
                }
                else{
                    $trucking_qty=round($data['order']->shipment_volume / env('PALLET_CFT'));
                }
                $trucking_price = $trucking_qty*env('TRUCKING_PRICE');
            }    
            else{
                $trucking_qty=1;
                $trucking_price = $data['order']->trucking_rate;
            }

            $data['sum_shipping_total'] += ($data['order']->shipment_type_rate * $data['order']->shipment_volume);
            foreach($data['detail_charge'] as  $detail_charges){
                $data['sum_shipping_total'] += $detail_charges->charge * $charge_qty;
            }
            if($data['order']->shipping_method_id  == 2){
                $port_fee= env('PORT_FEE');
            }
            elseif($data['order']->shipping_method_id  == 1){
                if($data['order']->shipment_type_id==1){
                    $port_fee= env('OCEAN_LCL_PORT_FEE');
                }
                else{
                    $port_fee= env('OCEAN_PORT_FEE');
                }
                $trucking_price = ($data['order']->trucking_rate*$trucking_qty);
            }
            else{
                $port_fee= (int)env('PORT_FEE');
                $trucking_price = 0;
            }
            $data['sum_shipping_total'] += ($port_fee*$charge_qty);
            $data['sum_shipping_total'] += $trucking_price;
            $data['sum_shipping_total'] += env('WIRE_TRANS_FEE');
        }
        //End shipping
        //Sub receive and forwarding
            if($data['order']->shipment_type_id >1){
                $data['sum_receive_forward_total'] += ($data['order']->container_unload_rate*$charge_qty);
            }
            if($data['order']->outbound_method_id >1){
                $data['sum_receive_forward_total'] += $pallet_charge*$trucking_qty;
                $data['sum_receive_forward_total'] += $pallet_charge*$trucking_qty;
            }
        //End Sub receive and forwarding
        //Prep inspection
            foreach($data['detail_prep'] as $prep_key=>$prep_value ){
                $prep_price =  $prep_value->price * $data['order']->qty_per_box;
                 $data['sum_prep_total']+=$prep_price;
            }
        //End Prep inspection

        //Pre-shipment
            $data['pre_shipment'] = PreShipment::where('customer_requirement_id',$customer_requirement_id)->get();

            foreach ($data['pre_shipment'] as $key => $value) {
                $data['sum_pre_shipment'] += ($data['order']->no_boxs * $value->rate);
            }
        //End Pre-shipment

        $data['email'] = \Auth::user()->email;
        
        if($file == 'print'){
            $view = view('order.print_order',$data);
            return $view;
        }

        // return view('order.print_order',$data);
        //End prep inspection
        $pdf = PDF::loadView('order.print_order',$data);
        return $pdf->stream();
    }
    public function updateOrederNote(Request $request){
        $order = Order::where('order_id',$request->order_id)->first();
        $token = $this->check_token();
        $note = str_replace(' ', '%20', $request->note);
        $response = $this->api('https://crm.zoho.com/crm/private/xml/SalesOrders/updateRecords?authtoken='.$token.'&scope=crmapi&newFormat=1&id='.$order->zoho_sales_order_id.'&xmlData=%3CSalesOrders%3E%3Crow%20no%3D%221%22%3E%3CFL%20val%3D%22Note%22%3E'.$note.'%3C%2FFL%3E%3C%2Frow%3E%3C%2FSalesOrders%3E');
        $data = simplexml_load_string($response);
        $zoho_sales_order_id = $data->result->recorddetail->FL['0'];
    }
    public function addOrderNote(Request $request){
        if($request->zoho_order_id == null){
            return back()->withErrors('Please first add order in zoho.');
        }
        $token = $this->check_token();
        $add_note = $this->api('https://crm.zoho.com/crm/private/xml/Notes/insertRecords?newFormat=1&authtoken='.$token.'&scope=crmapi&xmlData=%3CNotes%3E%3Crow%20no%3D%226%22%3E%3CFL%20val%3D%22entityId%22%3E'+$request->zoho_order_id+'%3C%2FFL%3E%3CFL%20val%3D%22Note%20Title%22%3ENote%3C%2FFL%3E%3CFL%20val%3D%22Note%20Content%22%3E'+$request->note+'%3C%2FFL%3E%3C%2Frow%3E%3C%2FNotes%3E');
        $response = simplexml_load_string($add_note);
        Order_note::create([
            'note' => $request->note,
            'order_id' => $request->order_id,
            'zoho_order_note_id' => $response->result->recorddetail->FL[0]
        ]);
        return back();
    }
    public function orderstatus(Request $request){
        $current_date = date('Y-m-d');
        $etd_orders = Prealert_detail::where('ETD_china',$current_date)->where('status','0')->get();
        if(count($etd_orders)>0){
            foreach ($etd_orders as $etd_order) {
                Order::where('order_id',$etd_order->order_id)->update(array('is_activated'=>'5'));
                Prealert_detail::where('order_id',$etd_order->order_id)->update(array('status'=>'1'));
            }
        }
        $eta_orders = Prealert_detail::where('ETA_US',$current_date)->where('status','1')->get();
        if(count($eta_orders)>0) {
            foreach ($eta_orders as $eta_order) {
                Order::where('order_id', $eta_order->order_id)->update(array('is_activated' => '6'));
                Prealert_detail::where('order_id', $eta_order->order_id)->update(array('status' => '2'));
            }
        }
    }
    public function qboConnect(){
        $this->IntuitAnywhere = new \QuickBooks_IPP_IntuitAnywhere(env('QBO_DSN'), env('QBO_ENCRYPTION_KEY'), env('QBO_OAUTH_CONSUMER_KEY'), env('QBO_CONSUMER_SECRET'), env('QBO_OAUTH_URL'), env('QBO_SUCCESS_URL'));
        if ($this->IntuitAnywhere->check(env('QBO_USERNAME'), env('QBO_TENANT'))){// && $this->IntuitAnywhere->test(env('QBO_USERNAME'), env('QBO_TENANT'))) {
            $IPP = new \QuickBooks_IPP(env('QBO_DSN'));
            $creds = $this->IntuitAnywhere->load(env('QBO_USERNAME'), env('QBO_TENANT'));
            $IPP->authMode(
                \QuickBooks_IPP::AUTHMODE_OAUTH,
                env('QBO_USERNAME'),
                $creds);
            if (env('QBO_SANDBOX')) {
                $IPP->sandbox(true);
            }
            $this->realm = $creds['qb_realm'];
            $this->context = $IPP->context();
            return true;
        } else {
            return false;
        }
    }
    public function createCustomer($order_id){
        $user__detail = User::selectRaw('user_infos.*')
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->join('user_infos', 'user_infos.user_id', '=', 'users.id')
            ->where('orders.order_id', $order_id)
            ->get();
        $exist_user_detail = Customer_quickbook_detail::selectRaw('customer_quickbook_details.*')
            ->join('orders', 'customer_quickbook_details.user_id', '=', 'orders.user_id')
            ->where('orders.order_id', $order_id)
            ->get();
        $this->qboConnect();
        $CustomerService = new \QuickBooks_IPP_Service_Customer();
        $Customer = new \QuickBooks_IPP_Object_Customer();
        if (count($exist_user_detail) == 0) {
            $Customer->setDisplayName($user__detail[0]->company_name . mt_rand(0, 1000));
            $PrimaryPhone = new \QuickBooks_IPP_Object_PrimaryPhone();
            $PrimaryPhone->setFreeFormNumber($user__detail[0]->contact_phone);
            $Customer->setPrimaryPhone($PrimaryPhone);
            $Mobile = new \QuickBooks_IPP_Object_Mobile();
            $Mobile->setFreeFormNumber($user__detail[0]->contact_phone);
            $Customer->setMobile($Mobile);
            $Fax = new \QuickBooks_IPP_Object_Fax();
            $Fax->setFreeFormNumber($user__detail[0]->contact_phone);
            $Customer->setFax($Fax);
            $BillAddr = new \QuickBooks_IPP_Object_BillAddr();
            $BillAddr->setLine1($user__detail[0]->company_address);
            $BillAddr->setLine2($user__detail[0]->company_address2);
            $BillAddr->setCity($user__detail[0]->company_city);
            $BillAddr->setCountrySubDivisionCode($user__detail[0]->company_country);
            $BillAddr->setPostalCode($user__detail[0]->company_zipcode);
            $Customer->setBillAddr($BillAddr);
            $PrimaryEmailAddr = new \QuickBooks_IPP_Object_PrimaryEmailAddr();
            $PrimaryEmailAddr->setAddress($user__detail[0]->contact_email);
            $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);
            if ($resp = $CustomerService->add($this->context, $this->realm, $Customer)) {
                $resp = $this->getId($resp);
                $user_data = array('user_id' => $user__detail[0]->user_id,
                    'customer_id' => $resp
                );
                Customer_quickbook_detail::create($user_data);
                $this->addInvoice($resp, $order_id);
            } else {
                print($CustomerService->lastError($this->context));
            }
        } else {
            $resp = $exist_user_detail[0]->customer_id;
            $this->addInvoice($resp, $order_id);
        }
    }
    public function addInvoice($cust_resp, $order_id){
        $details = Payment_info::where('order_id', $order_id)->get();
        $InvoiceService = new \QuickBooks_IPP_Service_Invoice();
        $Invoice = new \QuickBooks_IPP_Object_Invoice();
        $Invoice->setDocNumber('WEB' . mt_rand(0, 10000));
        $Invoice->setTxnDate('2013-10-11');
        $service = array('Additional Port Fees', 'Customs Duties');
        if (isset($service)) {
            $ItemService = new \QuickBooks_IPP_Service_Item();
            foreach ($service as $services) {
                $Line = new \QuickBooks_IPP_Object_Line();
                $Line->setDetailType('SalesItemLineDetail');
                $Line->setAmount($details[0]->amount);
                $Line->setDescription('');
                $items = $ItemService->query($this->context, $this->realm, "SELECT * FROM Item WHERE Name = '$services'  ORDER BY Metadata.LastUpdatedTime ");
                if (!empty($items)) {
                    $resp = $this->getId($items[0]->getId());
                } else {
                    $Item = new \QuickBooks_IPP_Object_Item();
                    $Item->setName($services);
                    $Item->setType('Service');
                    $Item->setIncomeAccountRef('10');
                    if ($resp = $ItemService->add($this->context, $this->realm, $Item)) {
                        $resp = $this->getId($resp);
                    } else {
                        print($ItemService->lastError($this->context));
                    }
                }
                $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
                $SalesItemLineDetail->setUnitPrice();
                $SalesItemLineDetail->setQty(2);
                $SalesItemLineDetail->setItemRef($resp);
                $Line->addSalesItemLineDetail($SalesItemLineDetail);
                $Invoice->addLine($Line);
            }
            $Invoice->setCustomerRef($cust_resp);
        } else {
            exit;
        }
        if ($resp = $InvoiceService->add($this->context, $this->realm, $Invoice)) {
            $resp = $this->getId($resp);
            $invoice = array('invoice_id' => $resp);
            Order::where('order_id', $order_id)->update($invoice);
            $this->invoice_pdf($order_id);
        } else {
            print($InvoiceService->lastError());
        }
    }
    public function invoice_pdf($order_id)
    {
        $Context = $this->context;
        $realm = $this->realm;
        $InvoiceService = new \QuickBooks_IPP_Service_Invoice();
        $invoices = $InvoiceService->query($Context, $realm, "SELECT * FROM Invoice STARTPOSITION 1 MAXRESULTS 1");
        $invoice = reset($invoices);
        $id = substr($invoice->getId(), 2, -1);
        header("Content-Disposition: attachment; filename=" . $order_id . "_invoice.pdf");
        header("Content-type: application/x-pdf");
        $dir = public_path() . "/uploads/bills/";
        file_put_contents($dir . $order_id . "_invoice.pdf", $InvoiceService->pdf($Context, $realm, $id));
    }
    public function getId($resp)
    {
        $resp = str_replace('{', '', $resp);
        $resp = str_replace('}', '', $resp);
        $resp = abs($resp);
        return $resp;
    }
}