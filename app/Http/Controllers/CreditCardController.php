<?php

namespace App\Http\Controllers;
use App\Addresses;
use App\User;
use PayPal\Api\CreditCard;
use App\User_credit_cardinfo;
use Illuminate\Http\Request;
use App\Paypal_vault_detail;
class CreditCardController extends Controller
{
    public function index(){}
    public function create(){}
    public function store(Request $request){
        $default=0;
        $default = $request->has('default');
        $post = $request->all();
        $user = \Auth::user();
        $paypal_detail = Paypal_vault_detail::first();
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $paypal_detail->client_id,
                $paypal_detail->secret_key
            )
        );
        if(!empty($post['caddress2'])) {
            $billing_address = array(
                'line1' => $post['caddress1'],
                'line2' => $post['caddress2'],
                'city' => $post['ccity'],
                'country_code' => $post['ccountry'],
                'postal_code' => $post['cpostal_code'],
                'phone' => $post['cphone']
            );
        }
        else {
            $billing_address = array(
                'line1' => $post['caddress1'],
                'city' => $post['ccity'],
                'country_code' => $post['ccountry'],
                'postal_code' => $post['cpostal_code'],
                'phone' => $post['cphone']
            );
        }
        $card_name = explode(' ',$post['card_name']);
        $fname = $card_name[0];
        if(isset($card_name[1]))
            $lname = $card_name[1];
        else
            $lname='';
        $card = new CreditCard();
        $card->setType($post['card_type'])
            ->setNumber($post['card_number'])
            ->setExpireMonth($post['exp_month'])
            ->setExpireYear($post['exp_year'])
            ->setCvv2($post['security_code'])
            ->setFirstName($fname)
            ->setLastName($lname)
            ->setBillingAddress($billing_address);
        try {
            $card->create($apiContext);
        }
        catch (\PayPal\Exception\PayPalConnectionException $ex) {
            //echo $ex->getCode();
            //echo $ex->getData();
            $response = json_decode($ex->getData());

            return \Redirect::back()->withErrors(['Card details are invalid.']);
        }
        catch (Exception $ex) {
            die($ex);
        }
        if($default=='1')
            User_credit_cardinfo::where('user_id',$user->id)->update(array('make_deafult'=>'0'));
        $card_detail=array('user_id'=>$user->id,
            'credit_card_type'=>$card->type,
            'credit_card_number'=>$card->number,
            'credit_card_id' =>$card->id,
            'cvv'=>$post['security_code'],
            'expire_month'=>$post['exp_month'],
            'expire_year'=>$post['exp_year'],
            'first_name'=>$card_name[0],
            'last_name'=>$card_name[1],
            'nick_name'=>$post['nick_name'],
            'make_deafult'=>$default
        );
        User_credit_cardinfo::create($card_detail);
        if(!empty($post['card_name1'])) {
            $default1=0;
            $default1 = $request->has('default1');
            $card_name1 = explode(' ',$post['card_name1']);
            $fname1 = $card_name1[0];
            if(isset($card_name1[1]))
                $lname1 = $card_name1[1];
            else
                $lname1='';
            $card = new CreditCard();
            $card->setType($post['card_type1'])
                ->setNumber($post['card_number1'])
                ->setExpireMonth($post['exp_month1'])
                ->setExpireYear($post['exp_year1'])
                ->setCvv2($post['security_code1'])
                ->setFirstName($fname1)
                ->setLastName($lname1)
                ->setBillingAddress($billing_address);
            try {
                $card->create($apiContext);
            }
            catch (\PayPal\Exception\PayPalConnectionException $ex) {
                echo $ex->getCode();
                echo $ex->getData();
                die($ex);
            }
            catch (Exception $ex) {
                die($ex);
            }
            if($default1=='1')
                User_credit_cardinfo::where('user_id',$user->id)->update(array('make_default'=>'0'));
            $card_detail1=array('user_id'=>$user->id,
                'credit_card_type'=>$card->type,
                'credit_card_number'=>$card->number,
                'credit_card_id' =>$card->id,
                'cvv'=>$post['security_code1'],
                'expire_month'=>$post['exp_month1'],
                'expire_year'=>$post['exp_year1'],
                'first_name'=>$card_name1[0],
                'last_name'=>$card_name1[1],
                'nick_name'=>$post['nick_name1'],
                'make_default'=>$default1
            );
            User_credit_cardinfo::create($card_detail1);
        }
        $addresses = array('user_id'=>$user->id,
            'type'=>'B',
            'address_1'=>$post['caddress1'],
            'address_2'=>$post['caddress2'],
            'city'=>$post['ccity'],
            'postal_code'=>$post['cpostal_code'],
            'country'=>$post['ccountry'],
            'country_code'=>$post['com_ccode'],
            'phone_number'=>$post['cphone']
        );
        Addresses::create($addresses);
        User::where('id',$user->id)->update(array('billing_complete'=>'1'));
        if($request->add_card){
            return back();
        }
    }
    public function show($id){}
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
    public function getcarddetail(Request $request){
        $post=$request->all();
        $id=$post['id'];
        $card_detail = User_credit_cardinfo::where('id',$id)->first()->toArray();
        return json_encode($card_detail);
    }
}