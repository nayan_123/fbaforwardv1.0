<?php

namespace App\Http\Controllers;
use App\Customer_requirement;
use App\Http\Requests\ProfileRequest;
use App\Amazon_inventory;
use App\Notification;
use App\Order;
use App\Shipping_quote;
use App\Supplier_inspection;
use App\User;
use App\Invoice_detail;
use Illuminate\Support\Facades\DB;
use App\User_info;
use Illuminate\Http\Request;
use App\Order_shipment_quantity;
use CountryState;
use Auth;
class MemberController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(){
        $user=\Auth::user();
        if (\Auth::user()->role->name == 'Admin') {
            return redirect('admin/dashboard');
        }
        $total_customer = User::where('role_id','3')->count();
        if($user->role->name=='Customer') {
            if($user->first_login=="0") {
                return redirect('settings');
            }
            $inventory = Amazon_inventory::where('user_id',$user->id)
                                         ->where('infbaforward','>','0')
                                         ->Orwhere('processfbaforward','>','0')
                                         ->Orwhere('atfbaforward','>','0')
                                         ->Orwhere('inamazon','>','0')
                                         ->Orwhere('atamazon','>','0')
                                         ->limit(2)->get()->toArray();
            $user_details = User_info::selectRaw('user_infos.*,zoho_account_managers.email,zoho_account_managers.phone,zoho_account_managers.mobile,zoho_account_managers.image')
                                    ->leftjoin('zoho_account_managers','zoho_account_managers.id','=','user_infos.zoho_account_manager_id')
                                    ->where('user_infos.user_id',$user->id)->first();
            $quote = Customer_requirement::selectRaw('amazon_inventories.product_name, amazon_inventories.product_nick_name,customer_requirements.created_at, customer_requirements.id')
                     ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                     ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                     ->where('customer_requirements.user_id',$user->id)
                     ->whereIn('customer_requirements.is_activated',array('0','3'))
                     ->limit(3)
                     ->get()->toArray();
            $quote_freight=array();
            foreach ($quote as $quotes) {
                $getquotes = Shipping_quote::selectRaw('shipping_quotes.customer_requirement_id, shipping_quotes.total_freight_price, shipping_methods.shipping_name')
                             ->join('shipping_methods','shipping_methods.shipping_method_id','=','shipping_quotes.shipping_method_id')
                             ->where('customer_requirement_id',$quotes['id'])->get();
                $quote_freight=$getquotes;
            }
            $order = Order::selectRaw('amazon_inventories.product_name, amazon_inventories.product_nick_name, orders.created_at, orders.order_no')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->where('orders.user_id',$user->id)
                ->limit(3)
                ->get()->toArray();
            $complete=0;
            if(isset($user))
                $complete = $user->company_complete + $user->billing_complete + $user->amazon_complete + $user->authorize_complete + $user->service_complete;
                $complete_percentage = ($complete * 100) / 5;
            return view('member.home')->with(compact('user_details','inventory','quote','order','user','complete_percentage','quote_freight'));
        }
        elseif ($user->role->name=='Shipper') {
            return view('member.home')->with(compact('total_customer','shipping_quote_count','bill_lading_count','pre_alert_count','user'));
        }
        elseif ($user->role->name=='Sales') {
            $total_order = Order::count();
            $total_in_order= Order::where('is_activated','0')->count();
            $total_place_order= Order::where('is_activated','1')->count();
            $total_inspect_order= Order::where('is_activated','2')->count();
            $total_shipping_order= Order::where('is_activated','4')->count();
            return view('member.home')->with(compact('total_order','total_customer','total_in_order','total_place_order','total_inspect_order','total_shipping_order','user'));
        }
        elseif ($user->role->name=='Logistics') {
            return view('member.home')->with(compact('user'));
        }
        elseif ($user->role->name=='Warehouse Admin') {
            return view('member.home')->with(compact('user'));
        }
        elseif ($user->role->name=='Warehouse Lead') {
            return view('member.home')->with(compact('user'));
        }
        elseif ($user->role->name=='Billing Service') {
            $total_invoice = Invoice_detail::count();
            $total_amount = Invoice_detail::selectRaw('sum(total_amt) as amount_count')
                            ->get();
            return view('member.home')->with(compact('total_invoice','total_amount','user'));
        }
        elseif ($user->role->name=='Amazon Listing Service') {
            return view('member.home')->with(compact('user'));
        }
        return view('member.home')->with(compact('user'));
    }
    public function profile(){
        $user = \Auth::user();
        if (\Auth::user()->role->name == 'Admin') {
            return redirect('admin/users/'.$user->id);
        }
        $user_info = DB::table('user_infos')->where('user_id', $user->id)->get();
        return view('member.profile')->with(compact('user','user_info'));
    }
    public function editProfile(){
        $user = \Auth::user();
        $user_info = DB::table('user_infos')->where('user_id', $user->id)->get();
        return view('member.edit_profile')->with(compact('user', 'user_info'));
    }
    public function updateProfile(ProfileRequest $request){
        $user = \Auth::user();
        $user->name = $request->input('name');
        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        if ($request->hasFile('avatar')) {
            $destinationPath = public_path() . '/uploads/avatars';
            if ($user->avatar != "uploads/avatars/avatar.png") {
                @unlink($user->avatar);
            }
            $avatar = hash('sha256', mt_rand()) . '.' . $request->file('avatar')->getClientOriginalExtension();
            $request->file('avatar')->move($destinationPath, $avatar);
            $user->avatar = $avatar;
        }
        $user->save();
        $user_info= array(
            'company_name' => $request->input('company_name'),
            'company_phone' => $request->input('company_phone'),
            'company_address' => $request->input('company_address'),
            'company_address2' => $request->input('company_address2'),
            'primary_bussiness_type' => $request->input('business_type'),
            'contact_fname' => $request->input('contact_fname'),
            'contact_lname' => $request->input('contact_lname'),
        );
        User_info::where("user_id", "=", $user->id)->update($user_info);
        return redirect('member/profile')->with('success', 'Your Profile Updated Successfully');
    }
    public function checkread(Request $request){
        if($request->ajax())
            $post=$request->all();
            $id=$post['id'];
             $data=array('is_read'=>'1');
            Notification::where('id',$id)->update($data);
    }
    public function switchuser(Request $request){
        $user = \Auth::user();
        if($request->session()->get('new_user')) {
            $request->session()->put('old_user', $user->id);
            \Auth::loginUsingId($request->session()->get('new_user'));
            return redirect('member/home');
        }
        else {
            \Auth::loginUsingId($request->session()->get('old_user'));
            $request->session()->forget('old_user');
            $request->session()->forget('new_user');
            return redirect('member/home');
        }
    }
    public function storeuser(Request $request){
        if($request->ajax()) {
            $post=$request->all();
            $user_id=$post['user_id'];
            $request->session()->put('new_user',$user_id);
        }
    }
    public function getuserdetail(Request $request){
        $user = \Auth::user();
        $user_info = User_info::where('user_id',$user->id)->get()->toArray();
        return json_encode($user_info);
    }
    public function approveAppRequest(){
        User::where('id',Auth::user()->id)->update(['is_requested_for_app' => 1]);
        return redirect('member/home');
    }
}