<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use App\Delivery;
use App\Order;
class DeliveryController extends Controller{
    public function index(){
        $user = \Auth::user();
        $user_role_id=$user->role_id;
        if($user->role_id=='6') {
            $data['orders'] = $data['order'] = Order::selectRaw('orders.order_id,orders.created_at,orders.order_no,amazon_inventories.product_name,amazon_inventories.product_nick_name, user_infos.company_name')
                ->join('customer_requirements', 'customer_requirements.order_id', '=', 'orders.order_id')
                ->join('customer_requirement_details', 'customer_requirements.id', '=', 'customer_requirement_details.customer_requirement_id')
                ->join('amazon_inventories', 'amazon_inventories.id', '=', 'customer_requirement_details.product_id')
                ->join('user_infos', 'user_infos.user_id', '=', 'customer_requirements.user_id')
                ->where('orders.is_activated', '14')
                //->where('customer_requirements.order_type','1')
                //->OrwhereIn('orders.is_activated',array('12','13'))
                //->where('customer_requirements.order_type','3')
                ->get();
            $data['user_role_id']=$user_role_id;
            return view('delivery.index', $data);
        }
        else if($user->role_id=='8') {
            $schedule_shipment = Order::selectRaw('orders.order_id, orders.order_no,user_infos.company_name, amazon_inventories.product_name, delivery.ETA_to_warehouse, delivery.created_at')
                                      ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                                      ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                                      ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                                      ->join('user_infos','user_infos.user_id','=','orders.user_id')
                                      ->join('delivery','delivery.order_id','=','orders.order_id')
                                      ->where('orders.is_activated','6')
                                      ->where('customer_requirements.order_type','1')
                                      ->get();
            $incoming_shipment = Order::selectRaw('orders.order_id, orders.order_no,user_infos.company_name, amazon_inventories.product_name, prealert_details.ETA_US, prealert_details.ETD_china')
                                      ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                                      ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                                      ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                                      ->join('user_infos','user_infos.user_id','=','orders.user_id')
                                      ->leftjoin('delivery','delivery.order_id','=','orders.order_id')
                                      ->leftjoin('prealert_details','prealert_details.order_id','=','orders.order_id')
                                      ->where('orders.is_activated','4')
                                      ->where('customer_requirements.order_type','1')
                                      ->get();
            $non_turkey_shipment = Order::selectRaw('orders.order_id, orders.order_no,user_infos.company_name, amazon_inventories.product_name')
                                        ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                                        ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                                        ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                                        ->join('user_infos','user_infos.user_id','=','orders.user_id')
                                        ->where('orders.is_activated','0')
                                        ->whereIn('order_type',array('2','3'))
                                        ->get();
            return view('delivery.index')->with(compact('user_role_id','schedule_shipment','incoming_shipment','non_turkey_shipment'));
        }
    }
    public function create(Request $request){
        $data['order_id'] = $request->id;
        return view('delivery.book_delivery',$data);
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'order_id' => 'required',
            'delivery_required' => 'required',
            'location_of_goods' => 'required',
            'delivery_company' => 'required',
            'bill_of_lading' => 'required',
            'container' => 'required',
            'delivery_order' => 'required',
            'ETA_to_warehouse' => 'required',
            'terminal_fees' => 'required'
        ]);
        if($validator->fails()){
            return back()->withInput()->withErrors($validator);
        }
        $data = [
            'delivery_required' => $request->delivery_required,
            'location_of_goods' => $request->location_of_goods,
            'delivery_company' => $request->delivery_company,
            'bill_of_lading' => $request->bill_of_lading,
            'container' => $request->container,
            'ETA_to_warehouse' => \Carbon\Carbon::parse($request->ETA_to_warehouse)->format('Y-m-d h:i:s'),
            'order_id' => $request->order_id,
            'warehouse_process_fee'=>$request->warehouse_process_fee,
            'payment_method'=>$request->payment_method,
            'terminal_fees' => $request->terminal_fees
        ];
        if($request->delivery_order){
            $newname = $request->order_id."_delivery_order".'.'.$request->delivery_order->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/delivery';
            $request->file('delivery_order')->move($destinationPath, $newname);
            $data = array_merge($data, ['delivery_order' => $newname]);
        }
        Delivery::create($data);
        Order::where('order_id',$request->order_id)->update(array('is_activated'=>'15'));
        return redirect('delivery')->with('success','Delivery created successfully');
    }
}