<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Order;
use App\Bill_of_lading;
class BillofladingController extends Controller
{
    public function __construct(){
        $this->middleware(['auth']);
    }
    
    public function index(){
        $order = Order::selectRaw('orders.order_id,orders.order_no,amazon_inventories.product_name,amazon_inventories.product_nick_name, user_infos.company_name, shipping_methods.shipping_name, bill_of_ladings.id as bill_lading_id')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->leftjoin('user_infos','user_infos.user_id','=','customer_requirements.user_id')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->where('customer_requirement_methods.is_Activated','1')
            ->where('orders.is_activated','1')
            ->where('customer_requirements.order_type','1');
            $order->leftjoin('bill_of_ladings',function($join){
                $join->on('bill_of_ladings.order_id', '=', 'orders.order_id');
            });
            $order= $order->get();
        return view('billoflading.index')->with(compact('order'));
    }
    public function create(Request $request){
        $order_id= $request->id;
        $order = Order::selectRaw('orders.order_id,orders.order_no,orders.sb_number,amazon_inventories.product_name,amazon_inventories.product_nick_name, shipping_methods.shipping_name, bill_of_ladings.id as bill_lading_id, bill_of_ladings.revision_note')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->where('customer_requirement_methods.is_Activated','1')
            ->where('orders.order_id',$order_id);
        $order->leftjoin('bill_of_ladings',function($join){
            $join->on('bill_of_ladings.order_id', '=', 'orders.order_id');
        });
           $order=$order->get();
        return view('billoflading.create')->with(compact('order'));
    }
    public function store(Request $request){
        $order_id=$request->input('order_id');
        if ($request->hasFile('bill')) {
            $destinationPath = public_path() . '/uploads/bills';
            $image = $order_id . '_' . 'billoflading' . '.' . $request->file('bill')->getClientOriginalExtension();
            $request->file('bill')->move($destinationPath, $image);
            $bill_data = array('order_id' => $order_id,
                'bill' => $image
            );
            Bill_of_lading::create($bill_data);
            Order::where('order_id',$order_id)->update(array('is_activated'=>'2'));
            return redirect('billoflading')->with('success','Bill Of Lading Uploaded successfully');
        }
        else {
            return redirect('billoflading')->with('success', 'Bill Of Lading Uploaded successfully');
        }
    }
    public function show($id){}
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
}