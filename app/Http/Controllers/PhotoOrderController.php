<?php

namespace App\Http\Controllers;
use App\Photo_order;
use App\Photo_order_photo;
use App\Photo_service;
use App\Listing_service;
use App\Listing_order;
use Illuminate\Http\Request;
class PhotoOrderController extends Controller{
    public function index(){
       $photo_order_list = Photo_order::selectRaw('photo_orders.*, user_infos.company_name')
                            ->join('user_infos','user_infos.user_id','=','photo_orders.user_id')
                            ->where('photo_orders.is_activated','0')->get();
       $photo_order_status = array('Awaiting Product Arrival','Product Received, Photos Processing','Photos Submitted','Revision Requested','Photos Approved, Order Complete');
       return view('amazonservices.listphotoorder')->with(compact('photo_order_list','photo_order_status'));
    }
    public function create(){}
    public function store(Request $request){}
    public function show($id){
        $title="Photo Order Details";
        $photo_order = Photo_order::selectRaw('photo_orders.*, user_infos.company_name,amazon_orders.order_id as amazon_order')
            ->join('user_infos','user_infos.user_id','=','photo_orders.user_id')
            ->join('amazon_orders','amazon_orders.id','=','photo_orders.amazon_order_id')
            ->where('photo_orders.id',$id)->first()->toArray();
        $listing_service = Listing_service::get();
        $listing_order = Listing_order::where('amazon_order_id',$photo_order['amazon_order_id'])->first()->toArray();
        $photo_service = Photo_service::get();
        $photo_order_photos = Photo_order_photo::where('photo_order_id',$id)->get();
        $photo_order_status = array('Awaiting Product Arrival','Product Received, Photos Processing','Photos Submitted','Revision Requested','Photos Approved, Order Complete');
       return view('amazonservices.detail_photo_order')->with(compact('title','photo_order','photo_order_status','photo_service','photo_order_photos','listing_service','listing_order'));
    }
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
}