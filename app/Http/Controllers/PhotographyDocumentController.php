<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Photo_order_photo;
use ZipArchive;
use App\Photo_order;
class PhotographyDocumentController extends Controller
{
    public function index(){}
    public function create(Request $request){
        $id= $request->id;
        $photo_document= Photo_order_photo::where('photo_order_id',$id)->get();
        return view('amazonservices.photo')->with(compact('photo_document','id'));
    }
    public function store(Request $request){
        $id=$request->input('id');
        $approve=$request->input('approve');
        $detail = $request->input('detail');
        Photo_order::where('id',$id)->update(array('revision'=>$approve,'revision_note'=>$detail));
        return redirect('amazonservices');
    }
    public function show($id){}
    public function edit($id){
        $files=Photo_order_photo::where('photo_order_id',$id)->get()->toArray();
        $zip = new ZipArchive();
        $zip_name = "order#P".$id.".zip";
        $zip->open($zip_name,  ZipArchive::CREATE);
        foreach ($files as $file) {
            $path = public_path("uploads/photography_photo/".$file['photo']);
            if(file_exists($path)){
                $zip->addFromString(basename($path),  file_get_contents($path));
            }
            else{
                echo"file does not exist";
            }
        }
        $zip->close();
        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename='.$zip_name);
        header('Content-Length: ' . filesize($zip_name));
        readfile($zip_name);
    }
    public function update(Request $request, $id){}
    public function destroy($id){}
}