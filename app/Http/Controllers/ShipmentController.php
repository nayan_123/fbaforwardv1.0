<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
class ShipmentController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request){
        $title="Shipment";
        return view('shipment.index')->with(compact('title'));
    }
    public function create(Request $request){}
    public function store(Request $request){}
    public function show($id){}
    public function edit(Request $request, $id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
}