<?php

namespace App\Http\Controllers;
use App\Amazon_order;
use App\Listing_order;
use App\Listing_order_photo;
use App\Listing_service;
use App\Photo_order;
use App\Photo_order_photo;
use App\Photo_service;
use Illuminate\Http\Request;
class AmazonServicesController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    public function index(){
        $user = \Auth::user();
        $list_order=Listing_order::where('user_id',$user->id)->get();
        $photo_order=Photo_order::where('user_id',$user->id)->get();
        $list_service=Listing_service::get();
        $photo_service=Photo_service::get();
        $list_order_status = array('Order Received','Processing','Complete','Revision Requested');
        $photo_order_status = array('Order Received','Awaiting Product Arrival','Product Received, Photos Processing','Photos Complete','Revision Requested');
        return view('amazonservices.index')->with(compact('list_service','photo_service','list_order','photo_order','list_order_status','photo_order_status'));
    }
    public function create(){
        return view('amazonservices.create');
    }
    public function store(Request $request){
       $user = \Auth::user();
       $service=$request->input('like');
       $amazon_order=Amazon_order::create(array('is_activated'=>'0'));
       Amazon_order::where('id',$amazon_order->id)->update(array('order_id'=>'A'.$amazon_order->id));
       foreach ($service as $services) {
           if($services=="list") {
               $list_array = array('amazon_order_id'=>$amazon_order->id,
                                    'frontend_qty'=>$request->input('frontend_qty'),
                                   'backend_qty'=>$request->input('backend_qty'),
                                   'product_detail'=>$request->input('product_details'),
                                   'links'=>$request->input('listing_links'),
                                   'links_to_competitors'=>$request->input('link_competitor'),
                                    'user_id'=>$user->id,
                                   'is_activated'=>'0',
               );
               $list_detail=Listing_order::create($list_array);
               $list_last_id = $list_detail->id;
               Listing_order::where('id',$list_last_id)->update(array('order_id'=>'L'.$list_last_id));
               if($request->hasFile('listing_photos')) {
                   $destinationPath = public_path() . '/uploads/listing_photo';
                   $list_files = $request->file('listing_photos');
                   foreach ($list_files as $key=>$list_file) {
                       $file = 'order_'.'L'.$list_last_id.'_'.$key.'.' .$list_file->getClientOriginalExtension();
                       $list_file->move($destinationPath, $file);
                       $list_photo_array=array('listing_order_id'=>$list_last_id,
                                               'photo'=>$file
                       );
                       Listing_order_photo::create($list_photo_array);
                   }
               }
           }
           else if($services=="photo") {
               $photo_array = array('amazon_order_id'=>$amazon_order->id,
                   'product'=>$request->input('photo_product'),
                   'standard_photo_qty'=>$request->input('standard_photo'),
                   'prop_photo_qty'=>$request->input('prop_photo'),
                   'lifestyle_photo_qty'=>$request->input('style_photo'),
                   'standard_file_format'=>$request->input('standard_file'),
                   'prop_file_format'=>$request->input('prop_file'),
                   'lifestyle_file_format'=>$request->input('style_file'),
                   'description_photo'=>$request->input('photo_desc'),
                   'links'=>$request->input('photography_links'),
                   'after_photo'=>$request->input('after_photo'),
                   'user_id'=>$user->id,
                   'is_activated'=>'0'
               );
               $photo_detail=Photo_order::create($photo_array);
               $photo_last_id = $photo_detail->id;
               Photo_order::where('id',$photo_last_id)->update(array('order_id'=>'P'.$photo_last_id));
               if($request->hasFile('photography_photos')) {
                   $destinationPath = public_path() . '/uploads/photography_photo';
                   $photo_files = $request->file('photography_photos');
                   foreach ($photo_files as $key=>$photo_file) {
                       $file = 'order_'.'P'.$photo_last_id.'_'.$key.'.' .$photo_file->getClientOriginalExtension();
                       $photo_file->move($destinationPath, $file);
                       $photo_photo_array=array('photo_order_id'=>$photo_last_id,
                           'photo'=>$file
                       );
                       Photo_order_photo::create($photo_photo_array);
                   }
               }
           }
       }
       return redirect('amazonservices')->with('success', 'Amazon Service order created successfully');
    }
    public function show($id){}
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
}
