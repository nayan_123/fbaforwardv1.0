<?php

namespace App\Http\Controllers;
use App\Ein_notice;
use App\Itin_notice;
use App\Order;
use App\User;
use Illuminate\Http\Request;
use CountryState;
use App\User_info;
use File;

class CustomerController extends Controller
{
    public function index(){
        $users = User_info::get();
       return view('customer.index')->with(compact('users'));
    }
    public function create(Request $request){}
    public function store(Request $request){
        $user = \Auth::user();
        $post=$request->all();
        $user_exist = User_info::where('user_id',$user->id)->get();
        $data = array(
            'user_id' => $user->id,
            'company_name' => $post['lcname'],
            'company_address' => $post['address1'],
            'company_address2' => $post['address2'],
            'company_city' => $post['city'],
            'company_zipcode' => $post['postal_code'],
            'company_country' => $post['country'],
            'business_state' => $post['business_state'],
            'business_country' => $post['business_country'],
            'tax_id' => $post['taxid'],
            'primary_bussiness_type' => $post['btype'],
            'business_as' => $post['business_as'],
            'contact_fname' => $post['fname'],
            'contact_lname' => $post['lname'],
            'contact_email' => $post['email'],
            'contact_country_code' => $post['ccode'],
            'contact_phone' => $post['phone'],
            'secondary_contact_fname' => $post['fname1'],
            'secondary_contact_lname' => $post['lname1'],
            'secondary_contact_email' => $post['email1'],
            'secondary_contact_country_code' => $post['ccode1'],
            'secondary_contact_phone' => $post['phone1'],
            'third_contact_fname' => $post['fname2'],
            'third_contact_lname' => $post['lname2'],
            'third_contact_email' => $post['email2'],
            'third_contact_country_code' => $post['ccode2'],
            'third_contact_phone' => $post['phone2'],
            'ssn'=>$post['ssn']
        );
        if(count($user_exist) > 0) {
            User_info::where('user_id', $user->id)->update($data);
        }
        else {
            User_info::create($data);
            User::where('id',$user->id)->update(array('company_complete'=>'1'));
        }
        if ($request->hasFile('ein')) {
            $path = public_path() . '/uploads/ein_notice';
            if(!File::exists($path)) {
                //File::makeDirectory($path, $mode = 0777, true, true);
                File::makeDirectory($path);
            }
            $getein= Ein_notice::where('user_id',$user->id)->get()->toArray();
            if(empty($getein)) {
                $path = public_path() . '/uploads/ein_notice';
                if(!File::exists($path)) {
                    File::makeDirectory($path);
                }
                $ein_file = $post['ein'];
                $ein_destinationPath = public_path() . '/uploads/ein_notice';
                $ein_notice = $user->id . "_" . 'ein_notice.' . $request->file('ein')->getClientOriginalExtension();
                $ein_file->move($ein_destinationPath, $ein_notice);
                Ein_notice::create(array('user_id'=>$user->id,'file'=>$ein_notice));
            }
            else {
                @unlink($getein['file']);
                $ein_file = $request->file('ein');
                $ein_destinationPath = public_path() . '/uploads/ein_notice';
                $ein_notice = $user->id . "_" . 'ein_notice.' . $request->file('ein')->getClientOriginalExtension();
                $ein_file->move($ein_destinationPath, $ein_notice);
                Ein_notice::where('user_id',$user->id)->update(array('file'=>$ein_notice));
            }
        }
        if ($request->hasFile('itin')) {
            $path = public_path() . '/uploads/itin_notice';
            if(!File::exists($path)) {
                File::makeDirectory($path);
            }
            $getitin= Itin_notice::where('user_id',$user->id)->get()->toArray();
            if(empty($getitin)) {
                $itin_file = $request->file('itin');
                $itin_destinationPath = public_path() . '/uploads/itin_notice';
                $itin_notice = $user->id . "_" . 'itin_notice.' . $request->file('itin')->getClientOriginalExtension();
                $itin_file->move($itin_destinationPath, $itin_notice);
                Itin_notice::create(array('user_id'=>$user->id,'file'=>$itin_notice));
            }
            else {
                @unlink($getitin['file']);
                $itin_file = $request->file('itin');
                $itin_destinationPath = public_path() . '/uploads/itin_notice';
                $itin_notice = $user->id . "_" . 'itin_notice.' . $request->file('itin')->getClientOriginalExtension();
                $itin_file->move($itin_destinationPath, $itin_notice);
                Itin_notice::where('user_id',$user->id)->update(array('file'=>$itin_notice));
            }
        }
    }
    public function show($id){
        $title= "Customer Account";
        $customer = User_info::where('user_id',$id)->first();
        $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated,customer_requirement_methods.shipping_method_id')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.customer_requirement_id')
            ->where('orders.user_id',$id)
            ->get()->toArray();
        $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('customer.show')->with(compact('title','customer','order','orderStatus'));
    }
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
    public function selectState(Request $request){
        if ($request->ajax()) {
            $post = $request->all();
            $states = CountryState::getStates($post['country_code']);
            return view('customer.change_state')->with(compact('states'));
        }
    }
}