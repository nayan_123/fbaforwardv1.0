<?php

namespace App\Http\Controllers;
use App\Order;
use App\TelexRelease;
use Illuminate\Http\Request;
class TelexReleaseController extends Controller{
    public function index(){
        $user = \Auth::user();
        if($user->role_id=='6') {
            $order = Order::selectRaw('orders.order_id,orders.order_no, orders.created_at, amazon_inventories.product_name,amazon_inventories.product_nick_name, user_infos.company_name')
                ->join('customer_requirements', 'customer_requirements.order_id', '=', 'orders.order_id')
                ->join('customer_requirement_details', 'customer_requirements.id', '=', 'customer_requirement_details.customer_requirement_id')
                ->join('amazon_inventories', 'amazon_inventories.id', '=', 'customer_requirement_details.product_id')
                ->leftjoin('user_infos', 'user_infos.user_id', '=', 'customer_requirements.user_id')
                ->join('customer_requirement_methods', 'customer_requirement_methods.customer_requirement_id', '=', 'customer_requirements.id')
                ->where('customer_requirement_methods.shipping_method_id', '1')
                ->where('customer_requirement_methods.is_activated', '1')
                ->whereBetween('orders.is_activated', array('7', '9'))
                ->get();
        }
        else if($user->role_id=='5') {
            $order = Order::selectRaw('orders.order_id,orders.order_no, orders.created_at, amazon_inventories.product_name,amazon_inventories.product_nick_name, user_infos.company_name')
                ->join('customer_requirements', 'customer_requirements.order_id', '=', 'orders.order_id')
                ->join('customer_requirement_details', 'customer_requirements.id', '=', 'customer_requirement_details.customer_requirement_id')
                ->join('amazon_inventories', 'amazon_inventories.id', '=', 'customer_requirement_details.product_id')
                ->leftjoin('user_infos', 'user_infos.user_id', '=', 'customer_requirements.user_id')
                ->join('customer_requirement_methods', 'customer_requirement_methods.customer_requirement_id', '=', 'customer_requirements.id')
                ->where('customer_requirement_methods.shipping_method_id', '1')
                ->where('customer_requirement_methods.is_activated', '1')
                ->where('orders.is_activated', '6')
                ->get();
        }
        return view('telexrelease.index')->with(compact('order','user'));
    }
    public function create(Request $request){
        $order_id= $request->id;
        $order = Order::selectRaw('orders.order_id,orders.order_no,orders.sb_number,amazon_inventories.product_name,amazon_inventories.product_nick_name, user_infos.company_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id')
            ->where('orders.order_id',$order_id)
            ->get();
        return view('telexrelease.create')->with(compact('order'));
    }
    public function store(Request $request){
        $order_id=$request->input('order_id');
        Order::where('order_id',$order_id)->update(array('is_activated'=>'7'));
        if ($request->hasFile('telex')) {
            $destinationPath = public_path() . '/uploads/telexrelease';
            $image = $order_id . '_' . 'telexrelease' . '.' . $request->file('telex')->getClientOriginalExtension();
            $request->file('telex')->move($destinationPath, $image);
            $telex_data = array('order_id' => $order_id,
                'file' => $image
            );
            TelexRelease::create($telex_data);
            return redirect('telexrelease')->with('success','Telex Uploaded successfully');
        }
        else {
            return redirect('telexrelease')->with('success','Telex Uploaded successfully');
        }
    }
    public function show($id){
        $telex = TelexRelease::selectRaw('orders.order_no, user_infos.company_name, telex_releases.*')
                            ->join('orders','orders.order_id','=','telex_releases.order_id')
                            ->join('user_infos','user_infos.user_id','=','orders.user_id')
                            ->where('orders.order_id',$id)->first();
        return view('telexrelease.show')->with(compact('telex'));
    }
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
}