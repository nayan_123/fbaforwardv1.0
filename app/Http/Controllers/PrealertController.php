<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Order;
use App\Prealert_detail;
class PrealertController extends Controller{
    public function __construct() {
        $this->middleware('auth');
    }
    public function index(){
        $order = Order::selectRaw('orders.order_id,orders.order_no,amazon_inventories.product_name,amazon_inventories.product_nick_name, user_infos.company_name, shipping_methods.shipping_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->leftjoin('user_infos','user_infos.user_id','=','customer_requirements.user_id')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->where('customer_requirement_methods.is_Activated','1')
            ->where('orders.is_activated','3')
            ->where('customer_requirements.order_type','1')
            ->get();
        return view('prealert.index')->with(compact('order'));
    }
    public function create(Request $request){
        $order_id= $request->id;
        $order = Order::selectRaw('orders.order_id,orders.order_no,orders.sb_number,amazon_inventories.product_name,amazon_inventories.product_nick_name, shipping_methods.shipping_name, customer_requirement_methods.shipping_method_id')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->where('customer_requirement_methods.is_Activated','1')
            ->where('orders.order_id',$order_id)
            ->get();
        return view('prealert.create')->with(compact('order'));
    }
    public function store(Request $request){
        $order_id=$request->input('order_id');
        $shipping_method_id = $request->input('shipping_method_id');
        $isf = $hbl = $mbl = $cargo = $commercial = $pack = '';
        if ($request->hasFile('isf')) {
            $destinationPath = public_path() . '/uploads/bills';
            $isf = $order_id . '_' . 'isf' . '.' . $request->file('isf')->getClientOriginalExtension();
            $request->file('isf')->move($destinationPath, $isf);
        }
        if ($request->hasFile('hbl')) {
            $destinationPath = public_path() . '/uploads/bills';
            $hbl = $order_id . '_' . 'hbl' . '.' . $request->file('hbl')->getClientOriginalExtension();
            $request->file('hbl')->move($destinationPath, $hbl);
        }
        if ($request->hasFile('mbl')) {
            $destinationPath = public_path() . '/uploads/bills';
            $mbl = $order_id . '_' . 'mbl' . '.' . $request->file('mbl')->getClientOriginalExtension();
            $request->file('mbl')->move($destinationPath, $mbl);
        }
        if ($request->hasFile('cargo')) {
            $destinationPath = public_path() . '/uploads/bills';
            $cargo = $order_id . '_' . 'cargomenifest' . '.' . $request->file('cargo')->getClientOriginalExtension();
            $request->file('cargo')->move($destinationPath, $cargo);
        }
        if ($request->hasFile('commercial')) {
            $destinationPath = public_path() . '/uploads/bills';
            $commercial = $order_id . '_' . 'commercialinvoice' . '.' . $request->file('commercial')->getClientOriginalExtension();
            $request->file('commercial')->move($destinationPath, $commercial);
        }
        if ($request->hasFile('packing')) {
            $destinationPath = public_path() . '/uploads/bills';
            $pack = $order_id . '_' . 'packinglist' . '.' . $request->file('packing')->getClientOriginalExtension();
            $request->file('packing')->move($destinationPath, $pack);
        }
        $prealert_data = array('order_id' => $order_id,
                'ISF' => $isf,
                'HBL'=> $hbl,
                'MBL' => $mbl,
                'cargo_manifest'=>$cargo,
                'commercial_invoice'=>$commercial,
                'packing_list'=>$pack,
                'ETD_china'=>date('Y-m-d',strtotime($request->input('etd_original'))),
                'ETA_US'=>date('Y-m-d',strtotime($request->input('etd_us'))),
                'status'=>'0'
        );
        Prealert_detail::create($prealert_data);
        Order::where('order_id',$order_id)->update(array('is_activated'=>'4'));
        /*if($shipping_method_id=='1')
            Order::where('order_id',$order_id)->update(array('is_activated'=>'4'));
        else
            Order::where('order_id',$order_id)->update(array('is_activated'=>'5'));*/
        return redirect('prealert')->with('Success','Prealert Uploaded successfully');
    }
    public function show($id){}
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
}