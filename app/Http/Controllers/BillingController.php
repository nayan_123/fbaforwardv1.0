<?php
namespace App\Http\Controllers;
use App\Order;
use App\Payment_info;
use App\OrderHold;
use PDF;

class BillingController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(){
        $title = "Billing";
        $user_id = \Auth::user()->id;
        $active_order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.shipment_size_type')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->leftjoin('payment_infos','payment_infos.order_id','=','orders.order_id')
                ->where('orders.is_activated','>=','19')
                ->whereNotIn('payment_infos.transaction_type',['3'])
                ->where('orders.user_id',$user_id)
                ->get();
        $past_order = Order::selectRaw('orders.*, amazon_inventories.product_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->leftjoin('payment_infos','payment_infos.order_id','=','orders.order_id')
            ->where('orders.is_activated','>=','19')
            ->where('payment_infos.transaction_type','3')
            ->where('orders.user_id',$user_id)
            ->get();

        return view('billing.index')->with(compact('title','active_order','past_order'));
    }
    public function create(){}
    public function show($id){
        $order = Order::selectRaw('orders.*, amazon_inventories.product_name, customer_requirement_details.total, customer_requirement_details.no_boxs,customer_requirements.shipment_size_type')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->where('orders.order_id',$id)
            ->where('orders.user_id',\Auth::user()->id)
            ->first();

        $custom_fee = OrderHold::where('order_id',$id)->whereIn('sub_hold_id',['7','13'])->sum('fee');

        $order_paid_history = Payment_info::where('order_id',$id)->get();
        $title = "Invoice Details";
        return view('billing.invoice')->with(compact('title','order','order_paid_history','custom_fee'));
    }


    public function downloadInvoice($id){

        $data['order'] = Order::selectRaw('orders.*, amazon_inventories.product_name, customer_requirement_details.total, customer_requirement_details.no_boxs,customer_requirements.shipment_size_type,user_infos.company_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->leftjoin('user_infos','user_infos.user_id','=','customer_requirements.user_id')
            ->where('orders.order_id',$id)
            ->where('orders.user_id',\Auth::user()->id)
            ->first();

        $data['custom_fee'] = OrderHold::where('order_id',$id)->whereIn('sub_hold_id',['7','13'])->sum('fee');
        $data['order_paid_history'] = Payment_info::where('order_id',$id)->get();

        $data['email'] = \Auth::user()->email;

        $pdf = PDF::loadView('billing.download_invoice',$data);
        return $pdf->download();
    }

}