<?php

namespace App\Http\Controllers\Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use CountryState;
use App\User_info;
use App\ZohoDetail;
use App\ZohoAccountManager;

class AuthController extends Controller {
use AuthenticatesAndRegistersUsers,
    ThrottlesLogins;
    // protected $redirectTo = 'member/home';
    protected $redirectTo = 'member/request-app';

    public function __construct() {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }
    protected function validator(array $data) {
        $validator =  Validator::make($data, [
                    'fname' => 'required|max:255',
                    'lname' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'cname' => 'required|max:255',
                    'password' => 'required|min:6',
        ]);
        
        return $validator;
    }
    public function getRegister(){
        return $this->showRegistrationForm();
    }
    public function showRegistrationForm(){
        $country = CountryState::getCountries();
        $states = CountryState::getStates('US');
        if (property_exists($this, 'registerView')) {
            return view($this->registerView)->with(compact('country'));
        }
        return view('auth.register')->with(compact('country','states'));
    }
    public function selectState(Request $request){
        if ($request->ajax()) {
            $post = $request->all();
            $states = CountryState::getStates($post['country_code']);
            return view('auth.change_state')->with(compact('states'));
        }
    }
    protected function create(array $data) {
        $user = User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'company_name' => $data['cname'],
                    'role_id' => '3',
                    'avatar' => 'avatar.png',
                    'password' => bcrypt($data['password']),
                    'first_login' => '0'
        ]);

        $zoho_account_manager = ZohoAccountManager::first();
        if(isset($zoho_account_manager)) {
            $zoho_account_manager_id = $zoho_account_manager->zoho_user_account_id;
            $zoho_account_manager = $zoho_account_manager->id;
        }
        else {
            $zoho_account_manager_id = 0;
            $zoho_account_manager = 0;
        }
        $token = $this->check_token();
        $company_name = str_replace(' ', '%20', $user->company_name);
        $response = $this->api('https://crm.zoho.com/crm/private/xml/Accounts/insertRecords?authtoken='.$token.'&scope=crmapi&newFormat=1&xmlData=%3CAccounts%3E%3Crow%20no%3D%22'.$user->id.'%22%3E%3CFL%20val%3D%22Account%20Name%22%3E'.$company_name.'%3C%2FFL%3E%3CFL%20val%3D%22SMOWNERID%22%3E'.$zoho_account_manager_id.'%3C%2FFL%3E%3C%2Frow%3E%3C%2FAccounts%3E');
            $data = simplexml_load_string($response);
           if ($data->result)
               $zoho_id = $data->result->recorddetail->FL['0'];
           else
              $zoho_id = 0;
        User_info::create(['user_id' => $user->id,'zoho_id' =>$zoho_id, 'zoho_account_manager_id' =>$zoho_account_manager ]);
        return $user;
    }

    public function getLogout(){
        return $this->logout();
    }
    public function logout(){
        \Session::flush();
        \Auth::guard($this->getGuard())->logout();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }
    public function authGenerate(){
        $auth = $this->api('https://accounts.zoho.com/apiauthtoken/nb/create?SCOPE=ZohoCRM%2Fcrmapi&EMAIL_ID=webdimensionsindia@gmail.com&PASSWORD=webdimensions@123&DISPLAY_NAME=webdimensionsindia');
        $arr = explode("\n", $auth);
        $str = strstr($arr[2], '=');
        $token = ltrim($str, '=');
        if($arr[3] == "RESULT=TRUE"){
            $zoho_detail = ZohoDetail::create([
                'auth_token' => $token
            ]);
        }
        else{
           // dd('Auth already generated.');
        }
       return back();
    }
    public function check_token(){
        $zoho_detail = ZohoDetail::first();
        if(!$zoho_detail){
            $this->authGenerate();
            $zoho_detail = ZohoDetail::first();
        }
        if($zoho_detail)
        return $zoho_detail->auth_token;
    }
    public function api($url){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: 0d06fbca-2115-25c0-aecb-f2d105e90c76"
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $response;
        }
    }
}