<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Customer_amazon_detail;
use App\Http\Requests\AmazoncredentialRequest;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Libraries;
use Carbon\Carbon;
class AmazonController extends Controller
{
    protected $service;
    public function __construct(){
       $this->middleware('auth');
    }
    public function index(){}
    public function addamazoncredential(Request $request){
        if(!empty($request->input('user_id'))) {
            $user_id = $request->input('user_id');
            $account = Customer_amazon_detail::where('user_id', '=', $user_id)->get()->first();
            $devAccount = DB::table('dev_accounts')->first();
            $account->mws_seller_id = $request->input('seller_id');
            $account->mws_authtoken = $request->input('auth_token');
            $account->dev_account_id = $devAccount->developer_accno;
            $validamazonMWS = $this->amazonAccountValidation($account);
            $flag = $request->input('status');
            if ($validamazonMWS == 1) {
                $credentail = array(
                    'user_id' => $user_id,
                    'mws_seller_id' => encrypt($request->input('seller_id')),
                    'mws_authtoken' => encrypt($request->input('auth_token')),
                );
                Customer_amazon_detail::where("user_id", "=", $user_id)->update($credentail);
            } else {
                echo "Invalid credential";
            }
            return redirect('settings');
        }
        else {
            $post = $request->all();
            $user = \Auth::user();
            $account = Customer_amazon_detail::where('user_id', '=', $user->id)->get()->first();
            $devAccount = DB::table('dev_accounts')->first();
            $account->mws_seller_id = $post['seller_id'];
            $account->mws_authtoken = $post['auth_token'];
            $account->dev_account_id = $devAccount->developer_accno;
            $validamazonMWS = $this->amazonAccountValidation($account);
            $flag = $request->input('status');
            if ($validamazonMWS == 1) {
                $credentail = array(
                    'user_id' => $user->id,
                    'mws_seller_id' => encrypt($post['seller_id']),
                    'mws_authtoken' => encrypt($post['auth_token']),
                );
                Customer_amazon_detail::where("user_id", "=", $user->id)->update($credentail);
                User::where('id', $user->id)->update(array('amazon_complete' => '1'));
            } else {
                echo "Invalid credential";
            }
            return 1;
        }
    }
    public function amazonAccountValidation($account){
        $UserCredentials['mws_authtoken'] = $account->mws_authtoken;
        $UserCredentials['mws_seller_id'] = $account->mws_seller_id;
        $this->report_type = '_GET_MERCHANT_LISTINGS_DATA_';
        $this->from_date_time = Carbon::parse('3 days ago');
        $this->to_date_time = null;
        $service = $this->getReportsClient();
        $request = $this->getRequest($UserCredentials);
        $response = $service->requestReport($request);
           if(is_array($response)) { return $response; }
           else { return 1; }
    }
    private function getKeys($uri = ''){
        add_to_path('Libraries');
        $devAccount = DB::table('dev_accounts')->first();
        return [
            $devAccount->access_key,
            $devAccount->secret_key,
            self::getMWSConfig()
        ];
    }
    protected function getReportsClient(){
        list($access_key, $secret_key, $config) = $this->getKeys();
        return new \MarketplaceWebService_Client(
            $access_key,
            $secret_key,
            $config,
            env('APPLICATION_NAME'),
            env('APPLICATION_VERSION')
        );
    }
    public static function getMWSConfig(){
        return [
            'ServiceURL' => "https://mws.amazonservices.com",
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 10,
        ];
    }
    private function getRequest($UserCredentials){
        $request = new \MarketplaceWebService_Model_RequestReportRequest();
        $request->setReportType($this->report_type);
        if ($this->from_date_time) {
            $request->setStartDate($this->from_date_time);
        }
        if ($this->to_date_time) {
            $request->setEndDate($this->to_date_time);
        }
        $this->initRequest($UserCredentials, $request, 'setMerchant');
         return $request;
    }
    protected function initRequest($UserCredentials, $request, $merchantMethod = 'setSellerId', $setMarketPlace = null){
        $request->setMWSAuthToken($UserCredentials['mws_authtoken']);
        $request->$merchantMethod($UserCredentials['mws_seller_id']);
        if (!is_null($setMarketPlace)) {
            $request->$setMarketPlace($this->account->marketplace->mws_market_place_id);
        }
    }
    public function testconnection(AmazoncredentialRequest $request){
        if ($request->ajax()) {
            $user = \Auth::user();
            $account = Customer_amazon_detail::where('user_id', '=', $user->id)->get()->first();
            if (empty($account)) {
                Customer_amazon_detail::create([
                    'user_id' => $user->id,
                    'mws_seller_id' => '',
                    'mws_market_place_id' => '',
                    'mws_authtoken' => '',
                ]);
                $account = Customer_amazon_detail::where('user_id', '=', $user->id)->get()->first();
            }
            $account->mws_seller_id = $_POST['seller_id'];
            $account->mws_authtoken = $_POST['auth_token'];
            $validamazonMWS = $this->amazonAccountValidation($account);
            if ($validamazonMWS == 1) {
                echo "Your Amazon Credential is valid";
            } else {
               echo "Your Amazon Credentail is invalid";
            }
        }
    }
}