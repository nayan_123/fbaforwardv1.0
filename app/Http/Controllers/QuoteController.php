<?php

namespace App\Http\Controllers;
use App\Customer_requirement;
use App\Customer_requirement_detail;
use App\Customer_requirement_method;
use App\Incoterm;
use App\Inspection_requirement_detail;
use App\Inspection_service;
use App\Outbound_method;
use App\Prep_requirement;
use App\Prep_service;
use App\Shipment_contain;
use App\Shipment_type;
use App\Shipping_quote;
use App\Shipping_quote_charge;
use App\User_info;
use Illuminate\Http\Request;
use App\Amazon_inventory;
use App\Prep_detail;
use App\Shipping_method;
use App\Order;
use App\Role;
use App\Supplier;
use App\Custom_requirement_detail;
use phpDocumentor\Reflection\Types\Null_;
use Webpatser\Uuid\Uuid;
use App\Libraries;
use PDF;
use DNS1D;
use Carbon\Carbon;
use Auth;
use App\Photo_list_detail;
use App\Listing_service_detail;
use App\QuoteHistory;
use App\PreShipment;

class QuoteController extends Controller{
    public function index(){
        $user = \Auth::user();
        $user_role_id = $user->role_id;
        if($user->role_id=='3') {
            $title="Quote";
            $user = \Auth::user();
            $active_quote = Customer_requirement::selectRaw("customer_requirements.*, amazon_inventories.product_name, customer_requirement_methods.shipping_method_id, shipping_quotes.id as shipping_id,customer_requirement_details.product_name as customer_requirement_details_product_name, customer_requirement_details.total, customer_requirement_details.no_boxs,shipping_quotes.is_activated as shipping_quote_is_activated")
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id','left')
                ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id','left')
                ->where('customer_requirements.user_id',$user->id)
                ->whereIn('customer_requirements.is_activated',['0','6','4'])
                ->where('customer_requirements.expire_date',null)
                ->Orwhere('customer_requirements.expire_date','>=',Carbon::now()->toDateString())
                ->whereIn('customer_requirements.is_activated',['0','6','4'])
                ->where('customer_requirements.user_id',$user->id)
                ->orderby('customer_requirements.id','desc')
                ->groupby('customer_requirement_methods.customer_requirement_id');
                $active_quote->leftjoin('shipping_quotes',function ($join){
                    $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id')
                         ->where('shipping_quotes.is_activated','=',0);
                });
            $active_quote = $active_quote->get();
            $expire_quote= Customer_requirement::selectRaw("customer_requirements.*, amazon_inventories.product_name,shipping_quotes.id as shipping_id,customer_requirement_details.product_name as customer_requirement_details_product_name, customer_requirement_details.total, customer_requirement_details.no_boxs")
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id','left')
                ->leftjoin('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id')
                ->where('customer_requirements.user_id',$user->id)
                ->whereIn('customer_requirements.is_activated',['0','6','4'])
                ->where('customer_requirements.expire_date','<',Carbon::now()->toDateString())
                ->groupby('shipping_quotes.customer_requirement_id')
                ->orderby('customer_requirements.id','desc')
                ->get();
            $past_quote= Customer_requirement::selectRaw("customer_requirements.*, amazon_inventories.product_name,shipping_quotes.id as shipping_id, shipping_methods.shipping_name,customer_requirement_details.product_name as customer_requirement_details_product_name, customer_requirement_details.total, customer_requirement_details.no_boxs")
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id','left')
                ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id','left')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                ->where('customer_requirements.user_id',$user->id)
                ->where('customer_requirement_methods.is_activated','1')
                ->where('customer_requirements.is_activated','1')
                ->Orwhere('customer_requirements.is_activated','2')
                ->groupby('customer_requirement_methods.customer_requirement_id')
                ->orderby('customer_requirements.id','desc');
            $past_quote = $past_quote->leftjoin('shipping_quotes',function($join) {
                 $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                 $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
                 $join->where('shipping_quotes.is_activated','<>','3');
            })->get();
           
            return view('quote.index')->with(compact('title', 'user_role_id','active_quote','expire_quote','past_quote'));
        }
        else if($user->role_id=='4') {
            $title="Shipping Quote";
            $active_quote = Customer_requirement::selectRaw("user_infos.company_name, customer_requirements.*, amazon_inventories.product_name, customer_requirement_methods.shipping_method_id, shipping_quotes.id as shipping_id")
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id','left')
                ->join('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id','left')
                ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
                ->where('customer_requirements.is_activated','0')
                ->orwhere('customer_requirements.is_activated','4')
                ->where('customer_requirements.expire_date','>=',Carbon::now()->toDateString())
                ->orderby('customer_requirements.id','desc')
                ->groupby('customer_requirement_methods.customer_requirement_id')
                ->groupby('shipping_quotes.customer_requirement_id')
                ->get();
            $expire_quote= Customer_requirement::selectRaw("user_infos.company_name, customer_requirements.*, amazon_inventories.product_name,shipping_quotes.id as shipping_id")
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id')
                ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
                ->where('customer_requirements.is_activated','0')
                ->where('customer_requirements.expire_date','<',Carbon::now()->toDateString())
                ->groupby('shipping_quotes.customer_requirement_id')
                ->orderby('customer_requirements.id','desc')
                ->get();
            $past_quote= Customer_requirement::selectRaw("user_infos.company_name, customer_requirements.*, amazon_inventories.product_name,shipping_quotes.id as shipping_id, shipping_methods.shipping_name")
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id','left')
                ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id','left')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
                ->where('customer_requirement_methods.is_activated','1')
                ->where('customer_requirements.is_activated','1')
                ->orWhere('customer_requirements.is_activated','2')
                ->orderby('customer_requirements.id','desc')
                ->groupby('shipping_quotes.customer_requirement_id')
                ->get();
            return view('quote.index')->with(compact('title', 'user_role_id','active_quote','expire_quote','past_quote'));
        }
        else if($user->role_id=='8') {
            $title="Quotes";
            $quote = Order::selectRaw('orders.*, amazon_inventories.product_name, user_infos.company_name')
                            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                            ->join('user_infos','user_infos.user_id','=','orders.user_id')
                            ->get();
            return view('quote.index')->with(compact('title', 'user_role_id','quote'));
        }
    }
    public function create(Request $request){
        $user_role=0;
        $customer=array();
        if(isset($request->user_role)) {
            $user_role = $request->user_role;
            $product = Amazon_inventory::where('hide_show','0')->get();
            $supplier = Supplier::get();
            $customer = User_info::get();
        }
        else {
            $user = \Auth::user();
            $product = Amazon_inventory::where('hide_show','0')->where('user_id', $user->id)->get();
            $supplier = Supplier::where('user_id', $user->id)->get();
        }
        $product_id=0;
        if(isset($request->product_id))
        $product_id=$request->product_id;
        $id = $request->id;
        $title= "Shipping Quote";
        $request->session()->forget('order_id');
        $shipping_method = Shipping_method::get();
        $prep_service  =Prep_service::get();
        $customer_requirement= Customer_requirement::selectRaw('customer_requirements.*, customer_requirement_details.*,customer_requirement_details.product_name as customer_requirement_details_product_name')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->where('customer_requirements.id',$id)
            ->get()
            ->toArray();

        $customer_requirement_method = Customer_requirement_method::selectRaw("shipping_method_id")->where('customer_requirement_id',$id)->get()->toArray();
        $customer_requirement_method = array_map('current',$customer_requirement_method);
        $inspection_detail = Inspection_requirement_detail::selectRaw('qty')->where('customer_requirement_id',$id)->get()->toArray();
        $inspection_detail = array_map('current',$inspection_detail);
        $prep_detail = Prep_requirement::selectRaw('prep_id')->where('customer_requirement_id',$id)->get()->toArray();
        $prep_detail = array_map('current',$prep_detail);
        $custom_detail = Custom_requirement_detail::selectRaw('custom_id')->where('customer_requirement_id',$id)->get()->toArray();
        $custom_detail = array_map('current',$custom_detail);
        $outbound_method = Outbound_method::get();
        $shipment_contain = Shipment_contain::get();
        $shipment_type = Shipment_type::get();
        $incoterm = Incoterm::get();
        $inspection_service = Inspection_service::get();
        $inspection_detail_data = Inspection_requirement_detail::where('customer_requirement_id',$id)->pluck('qty','inspection_id')->toArray();
        $resubmit_quote = null;

        if(\Auth::user()->role_id == '1'){
            return view('admin.quotes.create')->with(compact('product','title','supplier','customer_requirement','inspection_detail','prep_detail','custom_detail','shipping_method','customer_requirement_method','prep_service','outbound_method','shipment_contain','shipment_type','incoterm','inspection_service','product_id','user_role','customer','resubmit_quote','inspection_detail_data'));
        }

        return view('quote.create')->with(compact('product','title','supplier','customer_requirement','inspection_detail','prep_detail','custom_detail','shipping_method','customer_requirement_method','prep_service','outbound_method','shipment_contain','shipment_type','incoterm','inspection_service','product_id','user_role','customer','resubmit_quote','inspection_detail_data'));
    }
    public function store(Request $request){
        if($request->input('good_ready_date'))
            $goods_ready_date = \Carbon\Carbon::parse($request->good_ready_date)->format('Y-m-d h:m:s');
        else
            $goods_ready_date=NULL;
        if(!empty($request->input('shipment_details')) && !empty($request->input('custom_details'))) {
            $order_type=2;
        }
        else if (!empty($request->input('shipment_details')) && empty($request->input('custom_details'))) {
            $order_type=3;
        }
        else {
            $order_type=1;
        }
        $user = \Auth::user();
        if(!empty($request->input('customer'))) {
            $user_id =$request->input('customer');
        }
        else {
            $user_id = $user->id;
        }
        $factory_audit=0;
        $pre_inspection=0;
        if(!empty($request->input('factory_audit'))) {
            $factory_audit = $request->input('factory_audit');
        }
        if(!empty($request->input('pre_inspection'))) {
            $pre_inspection = $request->input('pre_inspection');
        }

        if(empty($request->input('customer_requirement_id'))) {
            if($request->input('select_supplier') == 0){
                $supplier_id=$request->input('supplier');
            }
            else{
                $supplier=Supplier::create(array('user_id'=>$user_id,'contact_name'=>$request->input('supplier_name'),'city'=>$request->input('port_of_origin')));
                $supplier_id=$supplier->supplier_id;
            }
            $shipment = array('user_id' => $user_id,
                'goods_ready_date' => $goods_ready_date,
                'incoterms' => $request->input('incoterm'),
                'supplier' => $supplier_id,
                'port_of_origin' => $request->input('port_of_origin'),
                'shipment_size_type' => $request->input('shipment_type'),
                'units' => $request->input('unit'),
                'shipment_weight' => $request->input('total_weight'),
                'shipment_volume' => $request->input('total_volume'),
                'shipment_notes' => $request->input('shipment_note'),
                'prep_note' => $request->input('prep_note'),
                'inspection_note' => $request->input('inspect_note'),
                'outbound_method_id' => $request->input('outbound'),
                'outbound_method_note' => $request->input('outbound_note'),
                'factory_audit'=>$factory_audit,
                'pre_inspection'=>$pre_inspection,
                'is_activated' => 0,
                'added_by'=>$user->id,
                'order_type'=>$order_type,
                'pre_shipment_note'=>$request->input('pre_ship_note')
            );
            $customer_requirement = new Customer_requirement($shipment);
            $customer_requirement->save();
            $last_id = $customer_requirement->id;
            $product = explode(" ", $request->input('product'));
            if($request->select_product == 0){
                $shipment_detail = array('customer_requirement_id' => $last_id,
                    'product_id' => $product[0],
                    'fnsku' => $product[1],
                    'qty_per_box' => $request->input('qty_per_carton'),
                    'no_boxs' => $request->input('no_of_carton'),
                    'total' => $request->input('total_unit')
                );
            }
            else{
                $shipment_detail = array('customer_requirement_id' => $last_id,
                    'product_name' => $request->product_name,
                    'qty_per_box' => $request->input('qty_per_carton'),
                    'no_boxs' => $request->input('no_of_carton'),
                    'total' => $request->input('total_unit')
                );
            }
            if(!empty($request->input('factory_audit'))) {
                PreShipment::create([
                    'name' => 'Factory Audit',
                    'customer_requirement_id' => $customer_requirement->id,
                    'rate' => 100
                ]);
            }
            if(!empty($request->input('pre_inspection'))) {
                PreShipment::create([
                    'name' => 'Pre Inspection',
                    'customer_requirement_id' => $customer_requirement->id,
                    'rate' => 100
                ]);
            }
            $customer_requirement_detail = new Customer_requirement_detail($shipment_detail);
            $customer_requirement_detail->save();
            $shipping_methods = $request->input('shipping_method');
            if(!empty($shipping_methods)) {
                foreach ($shipping_methods as $shipping_method) {
                    $customer_requirement_methods = array('customer_requirement_id' => $last_id,
                        'shipping_method_id' => $shipping_method
                    );
                    $customer_requirement_method = new Customer_requirement_method($customer_requirement_methods);
                    $customer_requirement_method->save();
                }
            }
            $prep_service = $request->input('prep_service');
            if (!empty($prep_service)) {
                foreach ($prep_service as $prep_services) {
                    $prep_requirement = array('customer_requirement_id' => $last_id,
                        'prep_id' => $prep_services,
                        'is_activated' => 0
                    );
                    $prep_requirements = new Prep_requirement($prep_requirement);
                    $prep_requirements->save();
                }
            }
            $inspection_requirement1 = array('customer_requirement_id' => $last_id,
                'inspection_id' => '1',
                'qty' => $request->input('inspect_service1'),
            );
            $inspection_requirement = new Inspection_requirement_detail($inspection_requirement1);
            $inspection_requirement->save();
            $inspection_requirement2 = array('customer_requirement_id' => $last_id,
                'inspection_id' => '2',
                'qty' => $request->input('inspect_service2'),
            );
            $inspection_requirement = new Inspection_requirement_detail($inspection_requirement2);
            $inspection_requirement->save();
            $shipment_container = $request->input('shipment_contain');
            if (!empty($shipment_container)) {
                foreach ($shipment_container as $shipment_containers) {
                    $custom_requirement = array('customer_requirement_id' => $last_id,
                        'custom_id' => $shipment_containers,
                    );
                    $custom_requirements = new Custom_requirement_detail($custom_requirement);
                    $custom_requirements->save();
                }
            }
            $role = Role::find(5);
            $role->newNotification()
                ->withType('shipping quote')
                ->withSubject('You have shipping quote for upload')
                ->withBody('You have shipping quote for upload')
                ->regarding($shipment)
                ->deliver();
            $role = Role::find(8);
            $role->newNotification()
                ->withType('Prep service quote')
                ->withSubject('You have prep service quote for upload')
                ->withBody('You have prep service quote for upload')
                ->regarding($shipment)
                ->deliver();
            QuoteHistory::create([
                'quote_history_status' => 1,
                'customer_requirement_id' => $last_id
            ]);
        }
        else
        {
            $last_id = $request->input('customer_requirement_id');
            $supplier_id=0;
            if($request->input('select_supplier') == 0){
                $supplier_id=$request->input('supplier');
            }
            else{
                $supplier=Supplier::create(array('user_id'=>$user_id,'contact_name'=>$request->input('supplier_name'),'city'=>$request->input('port_of_origin')));
                $supplier_id=$supplier->supplier_id;
            }
            $shipment = array(
                'user_id' => $user_id,
                'goods_ready_date' => date('Y-m-d', strtotime($request->input('good_ready_date'))),
                'incoterms' => $request->input('incoterm'),
                'supplier' => $supplier_id,
                'port_of_origin' => $request->input('port_of_origin'),
                'shipment_size_type' => $request->input('shipment_type'),
                'units' => $request->input('unit'),
                'shipment_weight' => $request->input('total_weight'),
                'shipment_volume' => $request->input('total_volume'),
                'shipment_notes' => $request->input('shipment_note'),
                'prep_note' => $request->input('prep_note'),
                'inspection_note' => $request->input('inspect_note'),
                'outbound_method_id' => $request->input('outbound'),
                'outbound_method_note' => $request->input('outbound_note'),
                'factory_audit'=>$factory_audit,
                'pre_inspection'=>$pre_inspection,
                'is_activated' => 5,
                'expire_date' => null
            );
            Customer_requirement::where('id',$last_id)->update($shipment);
            Shipping_quote::where('customer_requirement_id',$last_id)->update(array('is_activated'=>'5'));
            $product = explode(" ", $request->input('product'));
            if($request->product){
                $shipment_detail = array('product_id' => $product[0],
                    'fnsku' => $product[1],
                    'qty_per_box' => $request->input('qty_per_carton'),
                    'no_boxs' => $request->input('no_of_carton'),
                    'total' => $request->input('total_unit')
                );
            }
            else{
                $shipment_detail = array('customer_requirement_id' => $last_id,
                    'product_name' => $request->product_name,
                    'qty_per_box' => $request->input('qty_per_carton'),
                    'no_boxs' => $request->input('no_of_carton'),
                    'total' => $request->input('total_unit')
                );
            }
            Customer_requirement_detail::where('customer_requirement_id',$last_id)->update($shipment_detail);
            $prep_service = $request->input('prep_service');
            if (!empty($prep_service)) {
                Prep_requirement::where('customer_requirement_id',$last_id)->delete();
                foreach ($prep_service as $prep_services) {
                    $prep_requirement = array('customer_requirement_id' => $last_id,
                        'prep_id' => $prep_services,
                        'is_activated' => 0
                    );
                    $prep_requirements = new Prep_requirement($prep_requirement);
                    $prep_requirements->save();
                }
            }
            Inspection_requirement_detail::where('customer_requirement_id',$last_id)->delete();
            $inspection_requirement1 = array('customer_requirement_id' => $last_id,
                'inspection_id' => '1',
                'qty' => $request->input('inspect_service1'),
            );
            $inspection_requirement = new Inspection_requirement_detail($inspection_requirement1);
            $inspection_requirement->save();
            $inspection_requirement2 = array('customer_requirement_id' => $last_id,
                'inspection_id' => '2',
                'qty' => $request->input('inspect_service2'),
            );
            $inspection_requirement = new Inspection_requirement_detail($inspection_requirement2);
            $inspection_requirement->save();
            $shipment_container = $request->input('shipment_contain');
            if (!empty($shipment_container)) {
                Custom_requirement_detail::where('customer_requirement_id',$last_id)->delete();
                foreach ($shipment_container as $shipment_containers) {
                    $custom_requirement = array('customer_requirement_id' => $last_id,
                        'custom_id' => $shipment_containers,
                    );
                    $custom_requirements = new Custom_requirement_detail($custom_requirement);
                    $custom_requirements->save();
                }
            }
            $shipping_methods = $request->input('shipping_method');
            if(!empty($shipping_methods)) {
                Customer_requirement_method::where('customer_requirement_id',$last_id)->delete();
                foreach ($shipping_methods as $shipping_method) {
                    $customer_requirement_methods = array('customer_requirement_id' => $last_id,
                        'shipping_method_id' => $shipping_method
                    );
                    $customer_requirement_method = new Customer_requirement_method($customer_requirement_methods);
                    $customer_requirement_method->save();
                }
            }
        }
        if(\Auth::user()->role_id == '1'){
            return redirect('admin/quotes')->with('success', 'Shipping Quote Information Send Successfully');
        }

        return redirect('quote')->with('success', 'Shipping Quote Information Send Successfully');
    }
    public function show($id){
        $title="Quotes Detail";
        $user = \Auth::user();
        $user_role_id = $user->role_id;
        $quote = Order::selectRaw('orders.*, amazon_inventories.product_name, user_infos.company_name, customer_requirements.*, customer_requirement_details.*, shipping_methods.shipping_name, shipment_types.name as shipment_type_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.receive_forward, outbound_methods.outbound_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('user_infos','user_infos.user_id','=','orders.user_id')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type')
            ->join('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
            ->where('orders.order_id',$id)
            ->where('customer_requirement_methods.is_activated','1')
            ->first();
        $prep_details = Order::selectRaw('orders.*, prep_services.*')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('prep_requirements','customer_requirements.id','=','prep_requirements.customer_requirement_id')
            ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
            ->where('orders.order_id',$id)
            ->get();
        return view('quote.show')->with(compact('title', 'user_role_id','quote','prep_details'));
    }
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
    public function rejectshippingquote(Request $request){
        $id= $_POST['require_id'];
        $reason = $_POST['reason'];
        Customer_requirement::where('id',$id)->update(array('is_activated'=>'2','reject_reason'=>$reason));
        QuoteHistory::create([
            'quote_history_status' => 4,
            'customer_requirement_id' => $id
        ]);

        if(Auth::user()->role_id == '1'){
            return redirect('admin/quotes');
        }
        return redirect('quote');
    }
    public function getshippingmethod(){
        $id = $_POST['id'];
        $detail_method = Customer_requirement_method::selectRaw('shipping_methods.shipping_method_id,shipping_methods.shipping_name')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->where('customer_requirement_methods.customer_requirement_id',$id)
            ->where('customer_requirement_methods.is_activated','=','1')
            ->get()
            ->toArray();
        $method= array();
        foreach($detail_method as $array) {
            foreach ($array as $key => $value) {
                $newmethod[$key] = $value;
            }
            $method[] = $newmethod;
        }
        echo json_encode(array("detail_method"=>$method));
    }
    public function getshippingquote(){
        $id = $_POST['id'];
        $method_id = $_POST['method_id'];
        $order_type = $_POST['order_type'];
        $details = Customer_requirement::selectRaw('customer_requirements.*, amazon_inventories.product_name, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, customer_requirement_details.total, outbound_methods.outbound_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id, shipping_methods.shipping_name, shipping_methods.shipping_method_id,customer_requirement_details.product_name as customer_requirement_details_product_name')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id','left')
            ->join('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
            ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type','left')
            ->join('incoterms','incoterms.id','=','customer_requirements.incoterms','left')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->where('customer_requirements.id',$id)
            ->where('customer_requirement_methods.shipping_method_id',$method_id)
            ->groupby('customer_requirement_methods.customer_requirement_id');
        if(!empty($method_id) && $order_type==1) {
            $details->selectRaw('customer_requirements.*, amazon_inventories.product_name, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, customer_requirement_details.total, outbound_methods.outbound_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id, shipping_methods.shipping_name, shipping_methods.shipping_method_id, shipping_quotes.chargeable_weight');
            $details->join('shipping_quotes',function($join) {
                $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
            });
        }
        $details=$details->get();
        $detail_method = Customer_requirement_method::where('customer_requirement_id',$id)->where('shipping_method_id',$method_id)->get();
        $detail_charge = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
            ->join('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id')
            ->join('charges','charges.id','=','shipping_quote_charges.charge_id')
            ->where('shipping_quotes.customer_requirement_id',$id)
            ->where('shipping_quotes.shipping_method_id',$method_id)
            ->where('shipping_quotes.is_activated','0')
            ->get();
        $detail_prep = Prep_requirement:: selectRaw('prep_services.service_name, prep_services.price, prep_requirements.*')
            ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
            ->where('customer_requirement_id',$id)->get();
        echo json_encode(array("details"=>$details,"detail_method"=>$detail_method,"detail_charge"=>$detail_charge,'detail_prep'=>$detail_prep,'method_id'=>$method_id));
    }
    public function revisionshippingquote(Request $request){
        if ($request->ajax()) {
            $post = $request->all();
            $id = $post['id'];
            Customer_requirement::where('id',$id)->update(array('is_activated'=>'3','expire_date'=>null));
            QuoteHistory::create([
                'quote_history_status' => 6,
                'customer_requirement_id' => $id
            ]);
        }
    }
    public function approveshippingquote(Request $request){
        if ($request->ajax()) {
            $post = $request->all();
            $id = $post['id'];
            $method_id= $post['method_id'];
            $user = \Auth::user();
            $user_id=$user->id;
            $order_detail = array('order_no' => Uuid::generate(1, time())->string,
                'user_id' => $user_id,
                'is_activated' => '0'
            );
            $order = new Order($order_detail);
            $order->save();
            $order_id = $order->order_id;
            $customer_requirement = array('order_id'=>$order_id,'is_activated'=>'1');
            Customer_requirement::where('id',$id)->update($customer_requirement);
            $method = array('is_activated'=>'1');
            Customer_requirement_method::where('customer_requirement_id',$id)->where('shipping_method_id',$method_id)->update($method);
            Shipping_quote::where('customer_requirement_id',$id)->where('shipping_method_id',$method_id)->update($method);
            $user->newNotification()
                ->withType('New Order')
                ->withSubject('You are placing new order')
                ->withBody('You are placing new order')
                ->regarding($order_detail)
                ->deliver();
            return 1;
        }
    }
    //to download shipping quote
    public function viewshippingquote(Request $request){
        $id = $request->id;
        $method_id = $request->method_id;
        $details = Customer_requirement::selectRaw('customer_requirements.*, amazon_inventories.product_name, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, outbound_methods.outbound_name')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id')
            ->join('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
            ->where('customer_requirements.id',$id);
        if(!empty($method_id)) {
            $details->selectRaw('customer_requirements.*, amazon_inventories.product_name, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, shipping_methods.shipping_name, shipping_quotes.chargeable_weight');
            $details->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id');
            $details->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
            $details->join('shipping_quotes',function($join) {
                $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
            });
            $details->where('customer_requirement_methods.shipping_method_id',$method_id);
            $details->groupby('customer_requirement_methods.customer_requirement_id');
        }
        $details=$details->get();
        $detail_method = Customer_requirement_method::where('customer_requirement_id',$id)->where('shipping_method_id',$method_id)->get();
        $detail_charge = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
            ->join('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id')
            ->join('charges','charges.id','=','shipping_quote_charges.charge_id')
            ->where('shipping_quotes.customer_requirement_id',$id)
            ->where('shipping_quotes.shipping_method_id',$method_id)
            ->get();
        $detail_prep = Prep_requirement:: selectRaw('prep_services.service_name, prep_services.price, prep_requirements.*')
            ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
            ->where('customer_requirement_id',$id)->get();
        view()->share('details',$details);
        view()->share('detail_method',$detail_method);
        view()->share('detail_charge',$detail_charge);
        view()->share('detail_prep',$detail_prep);
        $pdf = PDF::loadView('quote/viewshippingquote');
        return $pdf->download('viewquote.pdf');
    }
    public function editshippingquote($quote_id,$quote){
        $user = \Auth::user();
        $user_role = $user->role_id;
        $company='';
        $order_type='';
        if($quote == 'past_quotes'){
            $customer_requirement = Customer_requirement::where('id',$quote_id)->first();
            $detail_method = Customer_requirement_method::selectRaw('shipping_methods.shipping_method_id,shipping_methods.shipping_name, customer_requirements.order_type')
            ->leftjoin('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->join('customer_requirements','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->where('customer_requirement_methods.customer_requirement_id',$quote_id);
            if($customer_requirement->is_activated == 1){
                $detail_method->where('customer_requirement_methods.is_activated','=','1');
            }
            $detail_method = $detail_method->get()->toArray();
            $method= array();
            foreach($detail_method as $array) {
                foreach ($array as $key => $value) {
                    $newmethod[$key] = $value;
                }
                $method[] = $newmethod;
                $order_type= $array['order_type'];
            }
            if(!empty($method)) {
               $method_id= $method[0]['shipping_method_id'];
            }
        }
        else{
            $detail_method = Customer_requirement_method::selectRaw('shipping_methods.shipping_method_id,shipping_methods.shipping_name, customer_requirements.order_type')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                ->join('customer_requirements','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                ->where('customer_requirement_methods.customer_requirement_id',$quote_id)
                ->get()
                ->toArray();
            $method= array();
            foreach($detail_method as $array) {
                foreach ($array as $key => $value) {
                    $newmethod[$key] = $value;
                }
                $method[] = $newmethod;
                $order_type= $array['order_type'];
            }
            if(!empty($method)) {
               $method_id= $method[0]['shipping_method_id'];
            }

        }
        $details = Customer_requirement::selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name,amazon_inventories.FNSKU,amazon_inventories.id as amazon_inventory_product_id, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, customer_requirement_details.total, outbound_methods.outbound_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id,suppliers.supplier_id, shipping_methods.shipping_name, shipping_methods.shipping_method_id,customer_requirement_details.product_name as customer_requirement_details_product_name')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->leftjoin('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id')
            ->join('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
            ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type','left')
            ->join('incoterms','incoterms.id','=','customer_requirements.incoterms','left')
            ->leftjoin('suppliers','customer_requirements.supplier','=','suppliers.supplier_id')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->where('customer_requirements.id',$quote_id)
            ->where('customer_requirement_methods.shipping_method_id',$method_id)
            ->groupby('customer_requirement_methods.customer_requirement_id');
        if(!empty($method_id) && $order_type==1) {
            $details->selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name,amazon_inventories.FNSKU,amazon_inventories.id as amazon_inventory_product_id, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, customer_requirement_details.total, outbound_methods.outbound_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id,suppliers.supplier_id, shipping_methods.shipping_name, shipping_methods.shipping_method_id, shipping_quotes.chargeable_weight');
            $details->leftjoin('shipping_quotes',function($join) {
                $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
            });
        }
        $details=$details->first();
        if($user->role_id=='4') {
            $com = User_info::where('user_id',$details->user_id)->first();
            $company = $com->company_name;
        }
        $detail_method = Customer_requirement_method::where('customer_requirement_id',$quote_id)->where('shipping_method_id',$method_id)->get();
        $detail_charge = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
            ->join('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id')
            ->join('charges','charges.id','=','shipping_quote_charges.charge_id')
            ->where('shipping_quotes.customer_requirement_id',$quote_id)
            ->where('shipping_quotes.shipping_method_id',$method_id)
            ->where('shipping_quotes.is_activated','0')
            ->get();
        $detail_prep = Prep_requirement:: selectRaw('prep_services.service_name, prep_services.price, prep_requirements.*')
            ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
            ->where('customer_requirement_id',$quote_id)->get();
        $inspection_requirement_details = Inspection_requirement_detail::where('customer_requirement_id',$quote_id)->get();
        $photo_list_detail = Photo_list_detail::where('customer_requirement_id',$quote_id)->get();
        $listing_service_details = Listing_service_detail::where('customer_requirement_id',$quote_id)->get();
        $histories = QuoteHistory::where('customer_requirement_id',$quote_id)->get();

        return view('quote.edit')->with(compact('details','method','detail_method','detail_charge','detail_prep','quote','inspection_requirement_details','photo_list_detail','listing_service_details','company','user_role','quote_id','method_id','histories'));
    }
    public function getSupplierLocation(Request $request){
        $supplier = Supplier::where('supplier_id',$request->supplier_id)->first();
        return $supplier->city;
    }
    public function resubmitQuote(Request $request){
        $user = Auth::user();
        $user_role=0;
        $customer=array();
        if(isset($request->user_role)) {
            $user_role = $request->user_role;
            $product = Amazon_inventory::get();
            $supplier = Supplier::get();
            $customer = User_info::get();
        }
        else {
            $user = \Auth::user();
            $product = Amazon_inventory::where('user_id', $user->id)->get();
            $supplier = Supplier::where('user_id', $user->id)->get();
        }
        $product_id=0;
        if(isset($request->product_id))
        $product_id=$request->product_id;
        $id = $request->customer_requirement_id;
        $title= "Shipping Quote";
        $shipping_method = Shipping_method::get();
        $prep_service  =Prep_service::get();
        $customer_requirement= Customer_requirement::selectRaw('customer_requirements.*, customer_requirement_details.*, customer_requirement_details.product_name as customer_requirement_details_product_name')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->where('customer_requirements.id',$id)
            ->get();
        $customer_requirement_method = Customer_requirement_method::selectRaw("shipping_method_id")->where('customer_requirement_id',$id)->get()->toArray();
        $customer_requirement_method = array_map('current',$customer_requirement_method);
        $inspection_detail = Inspection_requirement_detail::selectRaw('qty')->where('customer_requirement_id',$id)->get()->toArray();
        $inspection_detail = array_map('current',$inspection_detail);
        $prep_detail = Prep_requirement::selectRaw('prep_id')->where('customer_requirement_id',$id)->get()->toArray();
        $prep_detail = array_map('current',$prep_detail);
        $custom_detail = Custom_requirement_detail::selectRaw('custom_id')->where('customer_requirement_id',$id)->get()->toArray();
        $custom_detail = array_map('current',$custom_detail);
        $outbound_method = Outbound_method::get();
        $shipment_contain = Shipment_contain::get();
        $shipment_type = Shipment_type::get();
        $incoterm = Incoterm::get();
        $inspection_service = Inspection_service::get();
        $selected_shipment_contain = Customer_requirement_detail::where('customer_requirement_id',$request->customer_requirement_id)->get();
        $prep_service_data = Prep_requirement::where('customer_requirement_id',$request->customer_requirement_id)->pluck('prep_id')->toArray();
        $inspection_detail_data = Inspection_requirement_detail::where('customer_requirement_id',$request->customer_requirement_id)->pluck('qty','inspection_id')->toArray();
        $custom_detail = Custom_requirement_detail::where('customer_requirement_id',$request->customer_requirement_id)->pluck('custom_id')->toArray();
        $custom_detail = Custom_requirement_detail::selectRaw('custom_id')->where('customer_requirement_id',$id)->get()->toArray();
        $custom_detail = array_map('current',$custom_detail);
        $resubmit_quote = 'resubmit_quote';
        $customer_requirement_id = $customer_requirement[0]->id;
        $role_id = \Auth::user()->role_id;
        if($role_id == '1'){
            return view('admin.quotes.create')->with(compact('customer_requirement','shipment_contain','prep_service','inspection_requirement_detail','product','title','supplier','customer_requirement','inspection_detail','prep_detail','custom_detail','shipping_method','customer_requirement_method','prep_service','outbound_method','shipment_contain','shipment_type','incoterm','inspection_service','product_id','user_role','customer','selected_shipping_method','selected_shipment_contain','inspection_detail_data','prep_service_data','resubmit_quote','customer_requirement_id','role_id'));
        }

        return view('quote.create')->with(compact('customer_requirement','shipment_contain','prep_service','inspection_requirement_detail','product','title','supplier','customer_requirement','inspection_detail','prep_detail','custom_detail','shipping_method','customer_requirement_method','prep_service','outbound_method','shipment_contain','shipment_type','incoterm','inspection_service','product_id','user_role','customer','selected_shipping_method','selected_shipment_contain','inspection_detail_data','prep_service_data','resubmit_quote','customer_requirement_id')); 
    }
    public function getProduct(Request $request){
        $data['products'] = Amazon_inventory::where('hide_show','0')->where('user_id',$request->user_id)->get();
        $data['suppliers'] = Supplier::where('user_id',$request->user_id)->get();
        return $data;
    }
    public function getSupplier(Request $request){
        $product_id=explode(" ",$request->product_id);
        $user_id = Auth::user()->id;
        if($request->user_id){
            $user_id = $request->user_id;
        }
        $supplier = Supplier::selectRaw('suppliers.*,supplier_product_details.product_id')
                    ->join('supplier_product_details','supplier_product_details.supplier_id','=','suppliers.supplier_id')
                    ->where('suppliers.user_id',$user_id)
                    ->where('supplier_product_details.product_id',$product_id[0])
                    ->get();
        return $supplier;
    }
    public function getAllSupplier(Request $request){
        $user_id = Auth::user()->id;
        if($request->user_id){
            $user_id = $request->user_id;
        }
        $supplier = Supplier::selectRaw('suppliers.*')
                    ->where('suppliers.user_id',$user_id)
                    ->get();
        return $supplier;
    }

    public function addDimensions(Request $request){
        Customer_requirement_detail::where('id',$request->customer_requirement_details_id)->update([
            'unit' => $request->unit,
            'length' => $request->length,
            'width' => $request->width,
            'height' => $request->height
        ]);
        return back();
    }
    public function updatePrepNote(Request $request){
        Customer_requirement::where('id',$request->customer_requirement_id)->update(['prep_note' => $request->prep_note]);
        return back();
    }
    public function updateInspectionNote(Request $request){
        Customer_requirement::where('id',$request->customer_requirement_id)->update(['inspection_note' => $request->inspection_note]);
        return back();
    }
    public function updatePreshipmentNote(Request $request){
        Customer_requirement::where('id',$request->customer_requirement_id)->update(['pre_shipment_note' => $request->pre_shipment_note]);
        return back();
    }
    public function printQuote($method_id,$customer_requirement_id,$file,$quote = null){
        $user = \Auth::user();
        $user_role = $user->role_id;
        $company='';
        $order_type='';
        if($quote == 'past_quotes'){
            $customer_requirement = Customer_requirement::where('id',$customer_requirement_id)->first();
            $detail_method = Customer_requirement_method::selectRaw('shipping_methods.shipping_method_id,shipping_methods.shipping_name, customer_requirements.order_type,user_infos.company_name')
            ->leftjoin('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->join('customer_requirements','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
            ->where('customer_requirement_methods.customer_requirement_id',$customer_requirement_id);
            if($customer_requirement->is_activated == 1){
                $detail_method->where('customer_requirement_methods.is_activated','=','1');
            }
            $detail_method = $detail_method->get()->toArray();
            $method= array();
            foreach($detail_method as $array) {
                foreach ($array as $key => $value) {
                    $newmethod[$key] = $value;
                }
                $method[] = $newmethod;
                $order_type= $array['order_type'];
            }
            // if(!empty($method)) {
            //    $method_id= $method[0]['shipping_method_id'];
            // }
        }
        else{
            $detail_method = Customer_requirement_method::selectRaw('shipping_methods.shipping_method_id,shipping_methods.shipping_name, customer_requirements.order_type')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                ->join('customer_requirements','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                ->where('customer_requirement_methods.customer_requirement_id',$customer_requirement_id)
                ->get()
                ->toArray();
            $method= array();
            foreach($detail_method as $array) {
                foreach ($array as $key => $value) {
                    $newmethod[$key] = $value;
                }
                $method[] = $newmethod;
                $order_type= $array['order_type'];
            }
            // if(!empty($method)) {
            //    $method_id= $method[0]['shipping_method_id'];
            // }

        }
        // else{
        //     $detail_method = Customer_requirement_method::selectRaw('shipping_methods.shipping_method_id,shipping_methods.shipping_name, customer_requirements.order_type,user_infos.company_name')
        //         ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
        //         ->join('customer_requirements','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
        //         ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
        //         ->where('customer_requirement_methods.customer_requirement_id',$customer_requirement_id)
        //         ->get()
        //         ->toArray();
        // }
        $details = Customer_requirement::selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name,amazon_inventories.FNSKU,amazon_inventories.id as amazon_inventory_product_id, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, outbound_methods.outbound_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id,suppliers.supplier_id, shipping_methods.shipping_name, shipping_methods.shipping_method_id,customer_requirement_details.product_name as customer_requirement_details_product_name,customer_requirement_details.total')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id','left')
            ->join('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
            ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type','left')
            ->join('incoterms','incoterms.id','=','customer_requirements.incoterms','left')
            ->join('suppliers','customer_requirements.supplier','=','suppliers.supplier_id')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->where('customer_requirements.id',$customer_requirement_id)
            ->where('customer_requirement_methods.shipping_method_id',$method_id)
            ->groupby('customer_requirement_methods.customer_requirement_id');
        if(!empty($method_id) && $order_type==1) {
            $details->selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name,amazon_inventories.FNSKU,amazon_inventories.id as amazon_inventory_product_id, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, outbound_methods.outbound_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id,suppliers.supplier_id, shipping_methods.shipping_name, shipping_methods.shipping_method_id, shipping_quotes.chargeable_weight');
            $details->leftjoin('shipping_quotes',function($join) {
                $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
            });
        }
        $details=$details->first();
        if($user->role_id=='4') {
            $com = User_info::where('user_id',$details->user_id)->first();
            $company = $com->company_name;
        }
        // $detail_method = Customer_requirement_method::where('customer_requirement_id',$customer_requirement_id)->where('shipping_method_id',$method_id)->get();
        $detail_method = Customer_requirement_method::selectRaw('shipping_methods.shipping_method_id,shipping_methods.shipping_name, customer_requirements.order_type,user_infos.company_name')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                ->join('customer_requirements','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
                ->where('customer_requirement_methods.customer_requirement_id',$customer_requirement_id)
                ->where('shipping_methods.shipping_method_id',$method_id)
                ->first();

        $detail_charge = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
            ->join('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id')
            ->join('charges','charges.id','=','shipping_quote_charges.charge_id')
            ->where('shipping_quotes.customer_requirement_id',$customer_requirement_id)
            ->where('shipping_quotes.shipping_method_id',$method_id)
            ->where('shipping_quotes.is_activated','0')
            ->get();
        $detail_prep = Prep_requirement:: selectRaw('prep_services.service_name, prep_services.price, prep_requirements.*')
            ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
            ->where('customer_requirement_id',$customer_requirement_id)->get();
        $inspection_requirement_details = Inspection_requirement_detail::where('customer_requirement_id',$customer_requirement_id)->get();
        $photo_list_detail = Photo_list_detail::where('customer_requirement_id',$customer_requirement_id)->get();
        $listing_service_details = Listing_service_detail::where('customer_requirement_id',$customer_requirement_id)->get();
        $charge_qty = 1;
        $sum_details_charge = 0;
        $isf_rate =  env('ISF_RATE');
        $sum_custom_total = 0;
        $entry_rate = env('ENTRY_RATE');
        $bond_rate =  env('BOND_RATE');
        $sum_receive_forward_total = 0;
        $pallet_charge= env('pallet_charge');
        $wire_trans_fee = env('WIRE_TRANS_FEE');
        $sum_prep_total = 0;
        $trucking_qty = 1;

        if($details->order_type==1){
            if($details->units==1){
                $trucking_qty=round($details->shipment_volume / env('PALLET_CBM'));
            }
            else{
                $trucking_qty=round($details->shipment_volume / env('PALLET_CFT'));
            }
            if($trucking_qty > env('TRUCKING_QTY')){
                $trucking_price = ($trucking_qty -  env('TRUCKING_QTY')) * env('TRUCKING_ADDITIONAL_PRICE') + env('TRUCKING_PRICE');
            }
            else{
                $trucking_price =  env('TRUCKING_PRICE');
            }
            //Shipping total
            $sum_details_charge+=($details->shipment_type_rate*$details->shipment_volume);
            foreach($detail_charge as  $charge_key=>$charge_value ) {
                $sum_details_charge += $charge_value->charge * $charge_qty;
            }
            if($details->shipping_method_id == 2){
                $port_fee= env('PORT_FEE');
            }
            else{
                if($details->shipment_type_id==1){
                    $port_fee= env('OCEAN_LCL_PORT_FEE');
                }
                else{
                    $port_fee= env('OCEAN_PORT_FEE');
                }
                if($details->shipping_method_id==1){
                    $trucking_price = $details->trucking_rate*$trucking_qty;
                }
                else{
                    $trucking_price = 0;
                }
            }
              
            if($details->shipment_type_id==1){
                if($details->units==1){
                    $trucking_qty=round($details->shipment_volume / env('PALLET_CBM'));
                }
                else{
                    $trucking_qty=round($details->shipment_volume / env('PALLET_CFT'));
                }
                $trucking_price = $trucking_qty* env('TRUCKING_PRICE');
            }
            elseif($details->shipping_method_id==3){  
                $trucking_qty=1;
                $trucking_price=0;
            }
            else{
                $trucking_qty=1;
                $trucking_price = $details->trucking_rate;
            }
             

            $sum_details_charge+=$port_fee*$charge_qty;

            $sum_details_charge+=$trucking_price;
            $sum_details_charge+=$wire_trans_fee;
        }
        
        //End Shipping total
        //Custom total
        if($details->order_type==1 || $details->order_type==3){
            if($details->shipping_method_id == 1){
                $isf_rate * $charge_qty;
                $sum_custom_total= ($isf_rate*$charge_qty);
            }
            $entry_rate*$charge_qty;
            $bond_rate*$charge_qty;
            $sum_custom_total+= (int)($entry_rate*$charge_qty) + ($bond_rate*$charge_qty);
        }
        //End custom total
        //Sum Receive and forwarding
        if($details->shipment_type_id >1){
            $sum_receive_forward_total+=$details->container_unload_rate*$charge_qty;
        }
        if($details->outbound_method_id >1){
            $sum_receive_forward_total += $pallet_charge*$trucking_qty;
            $sum_receive_forward_total += $pallet_charge*$trucking_qty;
        }
        //End Sum Receive and forwarding
        //Sum Prep and inspection
            foreach($detail_prep as $prep_key=>$prep_value ){
                $prep_price = $prep_value->price * $details->total;
                $sum_prep_total+=$prep_price;
            }
            $sum_prep_total = round($sum_prep_total);
        //End Sum Prep and inspection
        $shipping_quote = Shipping_quote::select('shipping_port')->where(['customer_requirement_id' => $customer_requirement_id,'shipping_method_id' => $method_id])->first();

        $email = \Auth::user()->email;

        $total_cost = $sum_details_charge + $sum_custom_total + $sum_receive_forward_total + $sum_prep_total;

        $view = view('quote.print_quote')->with(compact('details','method','detail_method','detail_charge','detail_prep','quote','inspection_requirement_details','photo_list_detail','listing_service_details','company','user_role','quote_id','method_id','sum_details_charge','sum_custom_total','sum_receive_forward_total','sum_prep_total','shipping_quote','email','total_cost'))->render();
        $pdf = PDF::loadHtml($view);

        if($file == 'print'){
            return $view;
        }

        return $pdf->stream();
        unlink(public_path().'/temperory.html');
    }

    public function quotePrint($method_id,$customer_requirement_id,$file,$quote = null){
        $user = \Auth::user();
        $user_role = $user->role_id;
        $company='';
        $order_type='';
        if($quote == 'past_quotes'){
            $customer_requirement = Customer_requirement::where('id',$quote_id)->first();
            $detail_method = Customer_requirement_method::selectRaw('shipping_methods.shipping_method_id,shipping_methods.shipping_name, customer_requirements.order_type')
            ->leftjoin('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->join('customer_requirements','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->where('customer_requirement_methods.customer_requirement_id',$quote_id);
            if($customer_requirement->is_activated == 1){
                $detail_method->where('customer_requirement_methods.is_activated','=','1');
            }
            $detail_method = $detail_method->get()->toArray();
            $method= array();
            foreach($detail_method as $array) {
                foreach ($array as $key => $value) {
                    $newmethod[$key] = $value;
                }
                $method[] = $newmethod;
                $order_type= $array['order_type'];
            }
            if(!empty($method)) {
               $method_id= $method[0]['shipping_method_id'];
            }
        }
        else{
            $detail_method = Customer_requirement_method::selectRaw('shipping_methods.shipping_method_id,shipping_methods.shipping_name, customer_requirements.order_type')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                ->join('customer_requirements','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                ->where('customer_requirement_methods.customer_requirement_id',$customer_requirement_id)
                ->get()
                ->toArray();
        }
        $details = Customer_requirement::selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name,amazon_inventories.FNSKU,amazon_inventories.id as amazon_inventory_product_id, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, outbound_methods.outbound_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id,suppliers.supplier_id, shipping_methods.shipping_name, shipping_methods.shipping_method_id,customer_requirement_details.product_name as customer_requirement_details_product_name')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id','left')
            ->join('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
            ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type','left')
            ->join('incoterms','incoterms.id','=','customer_requirements.incoterms','left')
            ->join('suppliers','customer_requirements.supplier','=','suppliers.supplier_id')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->where('customer_requirements.id',$customer_requirement_id)
            ->where('customer_requirement_methods.shipping_method_id',$method_id)
            ->groupby('customer_requirement_methods.customer_requirement_id');
        if(!empty($method_id) && $order_type==1) {
            $details->selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name,amazon_inventories.FNSKU,amazon_inventories.id as amazon_inventory_product_id, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, outbound_methods.outbound_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id,suppliers.supplier_id, shipping_methods.shipping_name, shipping_methods.shipping_method_id, shipping_quotes.chargeable_weight');
            $details->leftjoin('shipping_quotes',function($join) {
                $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
            });
        }
        $details=$details->first();
        if($user->role_id=='4') {
            $com = User_info::where('user_id',$details->user_id)->first();
            $company = $com->company_name;
        }
        $detail_method = Customer_requirement_method::where('customer_requirement_id',$customer_requirement_id)->where('shipping_method_id',$method_id)->get();
        $detail_charge = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
            ->join('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id')
            ->join('charges','charges.id','=','shipping_quote_charges.charge_id')
            ->where('shipping_quotes.customer_requirement_id',$customer_requirement_id)
            ->where('shipping_quotes.shipping_method_id',$method_id)
            ->where('shipping_quotes.is_activated','0')
            ->get();
        $detail_prep = Prep_requirement:: selectRaw('prep_services.service_name, prep_services.price, prep_requirements.*')
            ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
            ->where('customer_requirement_id',$customer_requirement_id)->get();
        $inspection_requirement_details = Inspection_requirement_detail::where('customer_requirement_id',$customer_requirement_id)->get();
        $photo_list_detail = Photo_list_detail::where('customer_requirement_id',$customer_requirement_id)->get();
        $listing_service_details = Listing_service_detail::where('customer_requirement_id',$customer_requirement_id)->get();
        $charge_qty = 1;
        $sum_details_charge = 0;
        $isf_rate =  env('ISF_RATE');
        $sum_custom_total = 0;
        $entry_rate = env('ENTRY_RATE');
        $bond_rate =  env('BOND_RATE');
        $sum_receive_forward_total = 0;
        $pallet_charge= env('pallet_charge');
        $sum_prep_total = 0;
        if($details->units==1){
            $trucking_qty=round($details->shipment_volume / env('PALLET_CBM'));
        }
        else{
            $trucking_qty=round($details->shipment_volume / env('PALLET_CFT'));
        }
        if($trucking_qty > env('TRUCKING_QTY')){
            $trucking_price = ($trucking_qty -  env('TRUCKING_QTY')) * env('TRUCKING_ADDITIONAL_PRICE') + env('TRUCKING_PRICE');
        }
        else{
            $trucking_price =  env('TRUCKING_PRICE');
        }
        //Shipping total
        $sum_details_charge+=($details->shipment_type_rate*$details->shipment_volume);
        foreach($detail_charge as  $charge_key=>$charge_value ) {
            $sum_details_charge += $charge_value->charge * $charge_qty;
        }
        if($details->shipping_method_id == 2){
            $port_fee= env('PORT_FEE');
        }
        else{
            if($details->shipment_type_id==1){
                $port_fee= env('OCEAN_LCL_PORT_FEE');
            }
            else{
                $port_fee= env('OCEAN_PORT_FEE');
            }
            if($details->shipping_method_id==1){
                $trucking_price = $details->trucking_rate*$trucking_qty;
            }
            else{
                $trucking_price = 0;
            }
        }
        $sum_details_charge+=$port_fee*$charge_qty;
        $sum_details_charge+=$trucking_price;
        //End Shipping total
        //Custom total
        if($details->shipping_method_id == 1){
            $isf_rate * $charge_qty;
            $sum_custom_total= ($isf_rate*$charge_qty);
        }
        $entry_rate*$charge_qty;
        $bond_rate*$charge_qty;
        $sum_custom_total+= (int)($entry_rate*$charge_qty) + ($bond_rate*$charge_qty);
        //End custom total
        //Sum Receive and forwarding
        if($details->shipment_type_id >1){
            $sum_receive_forward_total+=$details->container_unload_rate*$charge_qty;
        }
        if($details->outbound_method_id >1){
            $sum_receive_forward_total += $pallet_charge*$trucking_qty;
            $sum_receive_forward_total += $pallet_charge*$trucking_qty;
        }
        //End Sum Receive and forwarding
        //Sum Prep and inspection
            foreach($detail_prep as $prep_key=>$prep_value ){
                $prep_price = $prep_value->price * $details->total;
                $sum_prep_total+=$prep_price;
            }
        //End Sum Prep and inspection
        $shipping_quote = Shipping_quote::select('shipping_port')->where(['customer_requirement_id' => $customer_requirement_id,'shipping_method_id' => $method_id])->first();
       
       return view('quote.quote_print')->with(compact('details','method','detail_method','detail_charge','detail_prep','quote','inspection_requirement_details','photo_list_detail','listing_service_details','company','user_role','quote_id','method_id','sum_details_charge','sum_custom_total','sum_receive_forward_total','sum_prep_total','shipping_quote'));

    }
}