<?php

namespace App\Http\Controllers;
use App\Listing_order;
use App\Listing_order_photo;
use Illuminate\Http\Request;
use ZipArchive;
class ListingDocumentController extends Controller
{
    public function index(){}
    public function create(Request $request){
        $id= $request->id;
        $list_document= Listing_order_photo::where('listing_order_id',$id)->get();
        return view('amazonservices.document')->with(compact('list_document','id'));
    }
    public function store(Request $request){
        $id=$request->input('id');
        $approve=$request->input('approve');
        $detail = $request->input('detail');
        Listing_order::where('id',$id)->update(array('revision'=>$approve,'revision_note'=>$detail));
        return redirect('amazonservices');
    }
    public function show($id){}
    public function edit($id){
        $files=Listing_order_photo::where('listing_order_id',$id)->get()->toArray();
        $zip = new ZipArchive();
        $zip_name = "order#L".$id.".zip";
        $zip->open($zip_name,  ZipArchive::CREATE);
        foreach ($files as $file) {
            $path = public_path("uploads/listing_photo/".$file['photo']);
            if(file_exists($path)){
                $zip->addFromString(basename($path),  file_get_contents($path));
            }
            else{
                echo"file does not exist";
            }
        }
        $zip->close();
        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename='.$zip_name);
        header('Content-Length: ' . filesize($zip_name));
        readfile($zip_name);
    }
    public function update(Request $request, $id){}
    public function destroy($id){}
    public function downloadlisting(Request $request){
        $id=$request->id;
        $files=Listing_order_photo::where('listing_order_id',$id)->get()->toArray();
        $zip = new ZipArchive();
        $zip_name = "order#L".$id.".zip";
        $zip->open($zip_name,  ZipArchive::CREATE);
        foreach ($files as $file) {
            $path = public_path("uploads/listing_photo/".$file['photo']);
            if(file_exists($path)){
                $zip->addFromString(basename($path),  file_get_contents($path));
            }
            else{
                echo"file does not exist";
            }
        }
        $zip->close();
        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename='.$zip_name);
        header('Content-Length: ' . filesize($zip_name));
        readfile($zip_name);
    }
}