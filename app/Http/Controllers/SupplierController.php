<?php

namespace App\Http\Controllers;
use App\Amazon_inventory;
use App\Supplier;
use App\Supplier_product;
use Illuminate\Http\Request;
class SupplierController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request){
        $user = \Auth::user();
        $supplier = Supplier::where('is_activated','0')->where('suppliers.user_id',$user->id)->get();
        $supplier_product=array();
        foreach ($supplier as $suppliers) {
            $supplier_products = Supplier_product::selectRaw('supplier_product_details.supplier_id, amazon_inventories.product_name')
                                ->join('amazon_inventories','supplier_product_details.product_id','=','amazon_inventories.id')
                                ->where('supplier_product_details.supplier_id',$suppliers->supplier_id)
                                ->get()->toArray();
            if(!empty($supplier_products)) {
                foreach ($supplier_products as $newsupplier) {
                    $supplier_product[$newsupplier['supplier_id']][]= $newsupplier['product_name'];
                }
            }
        }
        $product = Amazon_inventory::where('user_id',$user->id)->where('hide_show','0')->get();
        $product_supplier=array();
        foreach ($product as $products) {
            $product_suppliers = Supplier_product::selectRaw('supplier_product_details.product_id, suppliers.contact_name')
                ->join('suppliers','supplier_product_details.supplier_id','=','suppliers.supplier_id')
                ->where('supplier_product_details.product_id',$products->id)
                ->get()->toArray();
            if(!empty($product_suppliers)) {
                foreach ($product_suppliers as $newproduct) {
                    $product_supplier[$newproduct['product_id']][]= $newproduct['contact_name'];
                }
            }
        }

        return view('supplier.index')->with(compact('product', 'supplier','supplier_product','product_supplier'));
    }
    public function create(){
        $user= \Auth::user();
        $product = Amazon_inventory::where('user_id',$user->id)->get();
        return view('supplier.create_edit')->with(compact('product'));
    }
    public function store(Request $request){
        $user = \Auth::user();
        $post = $request->all();
        $supplier = new Supplier();
        $supplier->user_id = $user->id;
        $supplier->contact_name = $post['contact_name'];
        $supplier->email = $post['email'];
        $supplier->phone_number = $post['phone_number'];
        $supplier->nick_name = $post['nick_name'];
        $supplier->second_contact_name = $post['second_contact_name'];
        $supplier->second_email = $post['second_email'];
        $supplier->second_phone_number = $post['second_phone_number'];
        $supplier->city=$post['city'];
        $supplier->save();
        $last_id = $supplier->supplier_id;
        foreach ($post['product'] as $product) {
            $data = array('supplier_id'=>$last_id,
                          'product_id'=>$product
                );
            Supplier_product::create($data);
        }
        return redirect('supplychain')->with('success','New Supplier added successfully');
    }
    public function show($id){}
    public function edit($id){
        $user= \Auth::user();
        $supplier = Supplier::where('supplier_id',$id)->first();
        $product = Amazon_inventory::where('user_id',$user->id)->get();
        $supplier_product = Supplier_product::where('supplier_id',$id)->get();
        return view('supplier.create_edit')->with(compact('supplier','product','supplier_product'));
    }
    public function update(Request $request, $id){
        $details = array('contact_name'=>$request->input('contact_name'),
                         'email'=>$request->input('email'),
                         'phone_number'=>$request->input('phone_number'),
                         'nick_name'=>$request->input('nick_name'),
                         'second_contact_name'=>$request->input('second_contact_name'),
                         'second_email'=>$request->input('second_email'),
                         'second_phone_number'=>$request->input('second_phone_number'),
                         'city'=>$request->input('city')
                   );
        Supplier::where('supplier_id',$id)->update($details);
        Supplier_product::where('supplier_id',$id)->delete();
        foreach ($request->input('product') as $product) {
            $data = array('supplier_id'=>$id,
                'product_id'=>$product
            );
            Supplier_product::create($data);
        }
        return redirect('supplychain')->with('success', 'Supplier Updated Successfully');
    }
    public function destroy($id){
        Supplier::where('supplier_id',$id)->update(array('is_activated'=>'1'));
        return response()->json(['success' => 'Supplier has been deleted successfully']);
    }
    public  function getproduct(Request $request){
        $post = $request->all();
        $prod = $post['prod'];
        $user = \Auth::user();
        if($prod==1)
            $product = Amazon_inventory::where('user_id',$user->id)->get();
        else
            $product = Amazon_inventory::where('user_id',$user->id)->where('hide_show','0')->get();
        $product_supplier=array();
        foreach ($product as $products) {
            $product_suppliers = Supplier_product::selectRaw('supplier_product_details.product_id, suppliers.contact_name')
                ->join('suppliers','supplier_product_details.supplier_id','=','suppliers.supplier_id')
                ->where('supplier_product_details.product_id',$products->id)
                ->get()->toArray();
            if(!empty($product_suppliers)) {
                foreach ($product_suppliers as $newproduct) {
                    $product_supplier[$newproduct['product_id']][]= $newproduct['contact_name'];
                }
            }
        }
        return json_encode(array('product'=>$product,'product_supplier'=>$product_supplier));
    }
}