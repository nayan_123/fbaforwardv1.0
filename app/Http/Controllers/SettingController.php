<?php

namespace App\Http\Controllers;
use App\User;
use App\User_agreement_detail;
use Illuminate\Http\Request;
use App\Business_type;
use App\Taxes;
use CountryState;
use App\User_info;
use Illuminate\Support\Facades\DB;
use App\Customer_amazon_detail;
use App\Addresses;
use App\User_credit_cardinfo;
use App\Power_of_attorny;
use App\Ein_notice;
use App\Itin_notice;
use App\User_document;
use App\Service_agreement;

class SettingController extends Controller{
    public function index(){
        $user = \Auth::user();
        if ($user->role->name == 'Customer' && $user->first_login==0) {
            $business_type = Business_type::get();
            $taxes = Taxes::get();
            $country = CountryState::getCountries();
            $user_details = User_info::where('user_id', $user->id)->first();
            if(!empty($user_details->business_country)) {
                $states = CountryState::getStates($user_details->business_country);
            }
            else{
                $states = CountryState::getStates('US');
            }
            $arrMarketplaces = DB::table('amazon_marketplaces')->get();
            $market_place_id = '';
            if (!empty($arrMarketplaces))
                $market_place_id = $arrMarketplaces[0]->id;
            $customer_amazon_detail = Customer_amazon_detail::selectRaw('customer_amazon_details.*, amazon_marketplaces.market_place_id ')
                ->join('amazon_marketplaces', 'amazon_marketplaces.id', '=', 'customer_amazon_details.mws_market_place_id')
                ->where('user_id', '=', $user->id)->first();
            if (empty($customer_amazon_detail)) {
                Customer_amazon_detail::create([
                    'user_id' => $user->id,
                    'mws_seller_id' => '',
                    'mws_market_place_id' => $market_place_id,
                    'mws_authtoken' => '',
                ]);
                $customer_amazon_detail = Customer_amazon_detail::selectRaw('customer_amazon_details.*, amazon_marketplaces.market_place_id ')
                    ->join('amazon_marketplaces', 'amazon_marketplaces.id', '=', 'customer_amazon_details.mws_market_place_id')
                    ->where('user_id', '=', $user->id)->first();
            }
            $address_detail = Addresses::where('user_id', $user->id)->Orderby('address_id', 'desc')->limit(1)->get()->toArray(); 
            $card_detail = User_credit_cardinfo::where('user_id', $user->id)->Orderby('id', 'desc')->get()->toArray();
            $attorney = Power_of_attorny::where('user_id',$user->id)->get()->toArray();
            return view('settings.settings')->with(compact('user', 'business_type', 'country', 'states', 'user_details', 'taxes', 'customer_amazon_detail', 'card_detail', 'address_detail','attorney'));
        }
        else if($user->role->name=='Customer') {
            $business_type = Business_type::get();
            $country = CountryState::getCountries();
            $user_details = User_info::where('user_id', $user->id)->first();
            if(isset($user_details)) {
                if ($user_details->business_country) {
                    $states = CountryState::getStates($user_details->business_country);
                }
                else{
                    $states = CountryState::getStates('AF');
                }
            }
            else{
                $states = CountryState::getStates('AF');
            }
            $customer_amazon_detail = Customer_amazon_detail::selectRaw('customer_amazon_details.*, amazon_marketplaces.market_place_id ')
                ->join('amazon_marketplaces', 'amazon_marketplaces.id', '=', 'customer_amazon_details.mws_market_place_id')
                ->where('user_id', '=', $user->id)->first();
            $taxes = Taxes::get();
            $user_agreement = User_agreement_detail::where('user_id',$user->id)->Orderby('id','desc')->first();
            $ein_notice = Ein_notice::where('user_id',$user->id)->first();
            $itin_notice = Itin_notice::where('user_id',$user->id)->first();
            $user_documents = User_document::where('user_id',$user->id)->get();
            $user_agreement_detail = User_agreement_detail::where('user_id',$user->id)->first();
            $card_details = User_credit_cardinfo::where('user_id', $user->id)->Orderby('id', 'desc')->get();
            $power_of_attorney = Power_of_attorny::where('user_id',$user->id)->first();
            $service_agreements = Service_agreement::where('user_id',$user->id)->get();

            return view('settings.settings')->with(compact('user','user_details','customer_amazon_detail','taxes','user_agreement','user_documents','user_agreement_detail','ein_notice','business_type','country','states','card_details','power_of_attorney','service_agreements'));
        }
        else {
            $user_details=array();
            return view('settings.settings')->with(compact('user','user_details'));
        }
    }
    public function create(){}
    public function store(Request $request){
        $user_id=$request->input('user_id');
        if($request->input('update_account')!='') {
            $account_data=array();
            if ($request->input('new_password') != '') {
                $user = User::where('id',\Auth::user()->id)->first();
                if(auth()->attempt(['password' => $user->password])){
                    $new_password = bcrypt($request->input('new_password'));
                    $password_array=array('password'=>$new_password);
                    $account_data= array_merge($account_data,$password_array);
                }
                else{
                    $validation = $this->validate($request, [
                        'password' => 'passwordNotSame' 
                    ]);
                }
                $validator =  \Validator::make(['password' => $request->password], [
                    'password' => 'required|min:6'
                ]);
                if($validator->fails()){
                    return back()->withInput()->withErrors($validator);
                }
            }
            if($request->input('email') != '') {
                $validator =  \Validator::make($request->all(), [
                    'email' => 'required|email|max:255|unique:users'
                ]);
                if($validator->fails()){
                    return back()->withInput()->withErrors($validator);
                }
                $new_email= $request->input('email');
                $email_array=array('email'=>$new_email);
                $account_data=array_merge($account_data,$email_array);
            }
            User::where('id', $user_id)->update($account_data);
            $user_account_data = array();
            if($request->input('action_notify')) {
                $action_array=array('action_notify'=>$request->input('action_notify'));
                $user_account_data = array_merge($user_account_data,$action_array);
            }
            else{
                $action_array=array('action_notify'=>0);
                $user_account_data = array_merge($user_account_data,$action_array);
            }
            if($request->input('notificate_notify')) {
                $notified_array=array('notificate_notify'=>$request->input('notificate_notify'));
                $user_account_data = array_merge($user_account_data,$notified_array);
            }
            else{
                $notified_array=array('notificate_notify'=>0);
                $user_account_data = array_merge($user_account_data,$notified_array);
            }
            if($request->input('email_notify')) {
                $email_array=array('email_notify'=>$request->input('email_notify'));
                $user_account_data = array_merge($user_account_data,$email_array);
            }
            else{
                $email_array=array('email_notify'=>0);
                $user_account_data = array_merge($user_account_data,$email_array);
            }
            User_info::where('user_id',$user_id)->update($user_account_data);
        }
        else if($request->input('update_company')!='') {
            if($request->input('taxid')=='2')
                $ssn=$request->input('ssn');
            else
                $ssn = '';
            $data = array(
                'company_name' => $request->input('company_name'),
                'company_address' => $request->input('address1'),
                'company_address2' => $request->input('address2'),
                'company_city' => $request->input('city'),
                'company_zipcode' => $request->input('postal_code'),
                'company_country' => $request->input('country'),
                'contact_fname' => $request->input('contact_fname'),
                'contact_lname' => $request->input('contact_lname'),
                'contact_email' => $request->input('primary_email'),
                'contact_country_code'=>$request->input('ccode'),
                'contact_phone' => $request->input('primary_phone_number'),
                'secondary_contact_fname' => $request->input('secondary_contact_fname'),
                'secondary_contact_lname' => $request->input('secondary_contact_lname'),
                'secondary_contact_email' => $request->input('secondary_email'),
                'secondary_contact_country_code'=>$request->input('secondary_contact_country_code'),
                'secondary_contact_phone' => $request->input('secondary_phone_number'),
                'third_contact_fname' => $request->input('third_contact_fname'),
                'third_contact_lname' => $request->input('third_contact_lname'),
                'third_contact_email' => $request->input('third_email'),
                'third_contact_country_code'=>$request->input('third_contact_country_code'),
                'third_contact_phone' => $request->input('third_phone_number'),
                'primary_bussiness_type' =>  $request->input('btype'),
                'business_as' =>  $request->input('business_as'),
                'business_country' =>  $request->input('business_country'),
                'business_state' => $request->input('business_state'),
                'tax_id' =>  $request->input('taxid'),
                'ssn' => $ssn,
                'business_email' => $request->input('business_email'),
                'business_phone' => $request->input('business_phone'),
            );
            $user_info =User_info::where('user_id', $user_id)->update($data);
            $user = User::where('id',$user_id)->first();
            if($user_info && $user->company_complete==0) {
                User::where('id',$user_id)->update(array('company_complete'=>'1'));
            }
            if ($request->hasFile('ein')) {
                $path = public_path() . '/uploads/ein_notice';
                if(!File::exists($path)) {
                    File::makeDirectory($path);
                }
                $getein= Ein_notice::where('user_id',$user_id)->get()->toArray();
                if(empty($getein)) {
                    $ein_file = $request->file('ein');
                    $ein_destinationPath = public_path() . '/uploads/ein_notice';
                    $ein_notice = $user_id . "_" . 'ein_notice.' . $request->file('ein')->getClientOriginalExtension();
                    $ein_file->move($ein_destinationPath, $ein_notice);
                    Ein_notice::create(array('user_id'=>$user_id,'file'=>$ein_notice));
                }
                else {
                    @unlink($getein['file']);
                    $ein_file = $request->file('ein');
                    $ein_destinationPath = public_path() . '/uploads/ein_notice';
                    $ein_notice = $user_id . "_" . 'ein_notice.' . $request->file('ein')->getClientOriginalExtension();
                    $ein_file->move($ein_destinationPath, $ein_notice);
                    Ein_notice::where('user_id',$user_id)->update(array('file'=>$ein_notice));
                }
            }
            if ($request->hasFile('itin')) {
                $path = public_path() . '/uploads/itin_notice';
                if(!File::exists($path)) {
                    File::makeDirectory($path);
                }
                $getitin= Itin_notice::where('user_id',$user_id)->get()->toArray();
                if(empty($getitin)) {
                    $itin_file = $request->file('itin');
                    $itin_destinationPath = public_path() . '/uploads/itin_notice';
                    $itin_notice = $user_id . "_" . 'itin_notice.' . $request->file('itin')->getClientOriginalExtension();
                    $itin_file->move($itin_destinationPath, $itin_notice);
                    Itin_notice::create(array('user_id'=>$user_id,'file'=>$itin_notice));
                }
                else {
                    @unlink($getitin['file']);
                    $itin_file = $request->file('itin');
                    $itin_destinationPath = public_path() . '/uploads/itin_notice';
                    $itin_notice = $user_id . "_" . 'itin_notice.' . $request->file('itin')->getClientOriginalExtension();
                    $itin_file->move($itin_destinationPath, $itin_notice);
                    Itin_notice::where('user_id',$user_id)->update(array('file'=>$itin_notice));
                }
            }
        }
        else if($request->input('update_bill')!='') {}
        else if($request->input('update_amazon')!='') {}
        else if($request->input('update_document')!='') {}
        return redirect('settings')->with('success','Details Update Successfully');
    }
    public function show($id){}
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
    public function getuserdetail(Request $request){
        $user = \Auth::user();
        $user_info = User_info::where('user_id',$user->id)->get()->toArray();
        return json_encode($user_info);
    }
    public function completeSetting(){
        User::where('id',\Auth::user()->id)->update(array('first_login'=>'1'));
        return redirect('member/home');
    }
    public function addPowerOfAttorney(Request $request){
        $user = \Auth::user();
        if(Power_of_attorny::where('user_id',$user->id)->first()){
            Power_of_attorny::where('user_id',$user->id)->update([
                'user_id' => $user->id,
                'name' => $request->name,
                'title' => $request->title,
                'email' => $request->email
            ]);
        }
        else{
            Power_of_attorny::create([
                'user_id' => $user->id,
                'name' => $request->name,
                'title' => $request->title,
                'email' => $request->email
            ]);
        }
        if($request->file('identity')[0]){
            $path = public_path() . '/uploads/customer_document';
            if(!File::exists($path)) {
                File::makeDirectory($path);
            }
            $files=$request->file('identity');
            $destinationPath = public_path() . '/uploads/customer_document';
            foreach($files as $index=>$file){
                $name = $user->id . "_document" . $index.'.'. $file->getClientOriginalExtension();
                $file->move($destinationPath, $name);
                $images[]=$name;
                User_document::create(array('user_id'=>$user->id,'document'=>$name));
            }
        }
        return back();
    }
}