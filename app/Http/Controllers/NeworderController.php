<?php

namespace App\Http\Controllers;
use App\Customer_requirement_detail;
use App\Customer_requirement;
use App\Incoterm;
use App\Shipment_type;
use App\Shipping_method;
use Illuminate\Http\Request;
use App\Order;
class NeworderController extends Controller{
    public function __construct(){
        $this->middleware(['auth']);
    }
    public function index(){
        $order = Order::selectRaw('orders.order_id,orders.order_no,amazon_inventories.product_name,amazon_inventories.product_nick_name, user_infos.company_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->leftjoin('user_infos','user_infos.user_id','=','customer_requirements.user_id')
            ->where('orders.is_activated','0')
            ->where('customer_requirements.order_type','1')
            ->get();
        return view('neworders.index')->with(compact('order'));
    }
    public function create(Request $request){
        $order_id= $request->id;
        $order = Order::selectRaw('orders.order_id,amazon_inventories.product_name,user_infos.company_name, user_infos.contact_fname, user_infos.contact_lname, user_infos.contact_phone, user_infos.company_address, user_infos.company_address2, user_infos.company_city, user_infos.business_state, user_infos.business_country, user_infos.company_zipcode,suppliers.company_name as supplier_company_name, suppliers.phone_number as supplier_phone_no, suppliers.email as supplier_email,customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, customer_requirement_details.total, incoterms.name as incoterm_name, customer_requirements.shipment_weight, customer_requirements.shipment_volume, customer_requirements.port_of_origin, customer_requirements.goods_ready_date, customer_requirements.shipment_size_type, customer_requirements.incoterms, shipment_types.name as shipment_type_name, shipping_methods.shipping_name, shipping_quotes.total_freight_price, customer_requirement_methods.shipping_method_id')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->leftjoin('user_infos','user_infos.user_id','=','customer_requirements.user_id')
            ->join('suppliers','suppliers.supplier_id','=','customer_requirements.supplier')
            ->join('incoterms','incoterms.id','=','customer_requirements.incoterms')
            ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->join('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id')
            ->where('shipping_quotes.is_activated','1')
            ->where('customer_requirement_methods.is_activated','1')
            ->where('orders.order_id',$order_id)
            ->get();
        $shipping_method = Shipping_method::get();
        $incoterm= Incoterm::get();
        $shipment_type = Shipment_type::get();
        return view('neworders.create')->with(compact('order','shipping_method','incoterm','shipment_type'));
    }
    public function store(Request $request){
        Customer_requirement::where('order_id',$request->input('order_id'))->update(array('shipper_destination'=>$request->input('destination')));
        Order::where('order_id',$request->input('order_id'))->update(array('is_activated'=>'1'));
        return redirect('neworders')->with('success', 'New order created Successfully');
    }
    public function show($id){}
    public function edit($id){}
    public function update(Request $request){
        $post = $request->all();
        $data = array('incoterms' => $post['incoterm'],
            'port_of_origin' => $post['port_of_origin'],
            'goods_ready_date' => date('Y-m-d', strtotime($post['goods_date'])),
            'shipment_weight' => $post['shipment_weight'],
            'shipment_volume' => $post['shipment_volume'],
            'shipment_size_type' => $post['shipment_type'],
            'shipper_destination' => $post['destination_port']
        );
        Customer_requirement::where('order_id', $post['order_id'])->update($data);
        $customer_requirement = Customer_requirement::where('order_id', $post['order_id'])->first();
        $detail_data = array('no_boxs' => $post['no_of_carton'],
                'qty_per_box' => $post['qty_per_box'],
                'total' => $post['total_unit']
        );
        Customer_requirement_detail::where('customer_requirement_id', $customer_requirement->id)->update($detail_data);
    }
    public function destroy($id){}
}