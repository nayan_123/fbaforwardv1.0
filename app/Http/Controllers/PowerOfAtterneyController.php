<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Power_of_attorny;
use App\User;
use App\User_document;
use File;

class PowerOfAtterneyController extends Controller{
    public function index(){}
    public function create(){}
    public function store(Request $request){
        $user = \Auth::user();
        $agree = $request->has('agree');
        $attornies = Power_of_attorny::where('user_id',$user->id)->first();
        if(!$attornies) {
            if (!isset($agree)) {
                $post = $request->all();
                $attorny = array('user_id' => $user->id,
                    'name' => $post['rep_name'],
                    'title' => $post['rep_title'],
                    'email' => $post['rep_email'],
                    'not_agree' => '0');
                Power_of_attorny::create($attorny);
            } else {
                Power_of_attorny::create(array('user_id' => $user->id, 'not_agree' => '1'));
            }
            User::where('id',$user->id)->update(array('authorize_complete'=>'1'));
        }
        else {
            if (!$request->agree) {
                $post = $request->all();
                $attorny = array('user_id' => $user->id,
                    'name' => $post['rep_name'],
                    'title' => $post['rep_title'],
                    'email' => $post['rep_email'],
                    'not_agree' => '0');
                Power_of_attorny::where('user_id',$user->id)->update($attorny);
            } else {
                Power_of_attorny::where('user_id',$user->id)->update(array('user_id' => $user->id, 'not_agree' => $agree));
            }
        }
        if($request->file('identity')[0]){
            $path = public_path() . '/uploads/customer_document';
            if(!File::exists($path)) {
                File::makeDirectory($path);
            }

            $files=$request->file('identity');

            $destinationPath = public_path() . '/uploads/customer_document';
            foreach($files as $index=>$file){
                $name = $user->id . "_document" . $index.'.'. $file->getClientOriginalExtension();
                $file->move($destinationPath, $name);
                $images[]=$name;
                User_document::create(array('user_id'=>$user->id,'document'=>$name));
            }
        }
    }
    public function show($id){}
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
}