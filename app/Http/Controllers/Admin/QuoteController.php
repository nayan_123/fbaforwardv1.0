<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer_requirement;
use Carbon\Carbon;
use Excel;
use App\Amazon_inventory;
use App\User_info;
use Yajra\Datatables\Datatables;
use App\Customer_requirement_method;
use App\Shipping_quote_charge;
use App\Prep_requirement;
use App\Inspection_requirement_detail;
use App\Photo_list_detail;
use App\Listing_service_detail;
use App\QuoteHistory;

class QuoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('admin.quotes.index');
    }
    public function create()
    {
    }
    public function store(Request $request)
    {
    }
    public function show($id)
    {
    }
    public function edit($id)
    {
    }
    public function update(Request $request, $id)
    {
    }
    public function destroy($id)
    {
    }

    public function activeQuoteExport(){

        Excel::create('Active Quotes Data', function($excel) {
            
            $excel->sheet('Active Quotes Data', function($sheet) {

                $activequotes = Customer_requirement::selectRaw("user_infos.company_name, customer_requirements.*, amazon_inventories.product_name, customer_requirement_methods.shipping_method_id, shipping_quotes.id as shipping_id")
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id','left')
                ->join('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id','left')
                ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
                ->where('customer_requirements.is_activated','0')
                ->orwhere('customer_requirements.is_activated','4')
                ->where('customer_requirements.expire_date','>=',Carbon::now()->toDateString())
                ->orderby('customer_requirements.id','desc')
                ->groupby('customer_requirement_methods.customer_requirement_id')
                ->groupby('shipping_quotes.customer_requirement_id')
                ->get();

                if(count($activequotes) == 0){
                    return back();
                }

                $sheet->loadView('admin.quotes.active_quote_export')->with(['activequotes' => $activequotes]);
            
            });

        })->export('xls');
    }


    public function expireQuoteExport(){

        Excel::create('Expire Quotes Data', function($excel) {
            
            $excel->sheet('Expire Quotes Data', function($sheet) {

                $expirequotes = Customer_requirement::selectRaw("user_infos.company_name, customer_requirements.*, amazon_inventories.product_name,shipping_quotes.id as shipping_id")
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id')
                ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
                ->where('customer_requirements.is_activated','0')
                ->where('customer_requirements.expire_date','<',Carbon::now()->toDateString())
                ->groupby('shipping_quotes.customer_requirement_id')
                ->orderby('customer_requirements.id','desc')
                ->get();

                if(count($expirequotes) == 0){
                    return back();
                }
                $sheet->loadView('admin.quotes.expire_quote_export')->with(['expirequotes' => $expirequotes]);
            
            });

        })->export('xls');
    }


    public function pastQuoteExport(){

        Excel::create('Past Quotes Data', function($excel) {
            
            $excel->sheet('Past Quotes Data', function($sheet) {

                $pastquotes = Customer_requirement::selectRaw("user_infos.company_name, customer_requirements.*, amazon_inventories.product_name,shipping_quotes.id as shipping_id, shipping_methods.shipping_name")
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id','left')
                ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id','left')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
                ->where('customer_requirement_methods.is_activated','1')
                ->where('customer_requirements.is_activated','1')
                ->orWhere('customer_requirements.is_activated','2')
                ->orderby('customer_requirements.id','desc')
                ->groupby('shipping_quotes.customer_requirement_id')
                ->get();

                if(count($pastquotes) == 0){
                    return back();
                }
                
                $sheet->loadView('admin.quotes.past_quote_export')->with(['pastquotes' => $pastquotes]);
            
            });

        })->export('xls');
    }


    public function activeQuoteReport(){

        $data['product'] = Amazon_inventory::where('hide_show','0')->get();
        $data['user_infos'] = User_info::all();
        return view('admin.reports.active_quote_report',$data);
    }


    public function generateReport(Request $request){

        if($request->quote == 'active'){
            $quotes =  Customer_requirement::selectRaw("customer_requirements.*, amazon_inventories.product_name,customer_requirement_details.product_name as customer_requirement_details_product_name,customer_requirement_details.total, customer_requirement_details.no_boxs,customer_requirement_methods.shipping_method_id,shipping_quotes.id as shipping_id")
                    ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                    ->leftjoin('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                    ->leftjoin('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                    ->leftjoin('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id')
                    ->where('customer_requirements.is_activated','0')
                    ->orwhere('customer_requirements.is_activated','4')
                    ->where('customer_requirements.expire_date','>=',Carbon::now()->toDateString())
                    ->groupby('customer_requirement_methods.customer_requirement_id')
                    ->groupby('shipping_quotes.customer_requirement_id')
                    ->orderby('customer_requirements.id','desc');

            if($request->product_name){
             
                $quotes = $quotes->where('amazon_inventories.id',$request->product_name);
            }
            

            // if($request->company_name){
            //     $quotes = $quotes->where('user_infos.company_name',$request->company_name);
            // }

            // if(($request->from) && ($request->to)){
            //     $quotes = $quotes->whereBetween('customer_requirements.created_at', [\Carbon\Carbon::parse($request->from)->format('Y-m-d h:i:s'), \Carbon\Carbon::parse($request->to)->format('Y-m-d h:i:s')]);
            // }

        }

        $quotes = $quotes->get();
        
        return Datatables::of($quotes)
            ->editColumn('product', function ($quotes) {
                return $quotes->product_name;
            })
            ->editColumn('company', function ($quotes) {
                return $quotes->company_name;
            })
            ->editColumn('date_ordered', function ($quotes) {
                return date('m/d/Y',strtotime($quotes->created_at));
            })
            ->make(true);

    }


    public function editshippingquote($quote_id,$quote){
        $user = \Auth::user();
        $user_role = $user->role_id;
        $company='';
        $order_type='';
        if($quote == 'past_quotes'){
            $customer_requirement = Customer_requirement::where('id',$quote_id)->first();
            $detail_method = Customer_requirement_method::selectRaw('shipping_methods.shipping_method_id,shipping_methods.shipping_name, customer_requirements.order_type')
            ->leftjoin('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->join('customer_requirements','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->where('customer_requirement_methods.customer_requirement_id',$quote_id);
            if($customer_requirement->is_activated == 1){
                $detail_method->where('customer_requirement_methods.is_activated','=','1');
            }
            $detail_method = $detail_method->get()->toArray();
            $method= array();
            foreach($detail_method as $array) {
                foreach ($array as $key => $value) {
                    $newmethod[$key] = $value;
                }
                $method[] = $newmethod;
                $order_type= $array['order_type'];
            }
            if(!empty($method)) {
               $method_id= $method[0]['shipping_method_id'];
            }
        }
        else{
            $detail_method = Customer_requirement_method::selectRaw('shipping_methods.shipping_method_id,shipping_methods.shipping_name, customer_requirements.order_type')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                ->join('customer_requirements','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                ->where('customer_requirement_methods.customer_requirement_id',$quote_id)
                ->get()
                ->toArray();
            $method= array();
            foreach($detail_method as $array) {
                foreach ($array as $key => $value) {
                    $newmethod[$key] = $value;
                }
                $method[] = $newmethod;
                $order_type= $array['order_type'];
            }
            if(!empty($method)) {
               $method_id= $method[0]['shipping_method_id'];
            }

        }
        $details = Customer_requirement::selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name,amazon_inventories.FNSKU,amazon_inventories.id as amazon_inventory_product_id, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, customer_requirement_details.total, outbound_methods.outbound_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id,suppliers.supplier_id, shipping_methods.shipping_name, shipping_methods.shipping_method_id,customer_requirement_details.product_name as customer_requirement_details_product_name')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->leftjoin('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id')
            ->join('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
            ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type','left')
            ->join('incoterms','incoterms.id','=','customer_requirements.incoterms','left')
            ->leftjoin('suppliers','customer_requirements.supplier','=','suppliers.supplier_id')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->where('customer_requirements.id',$quote_id)
            ->where('customer_requirement_methods.shipping_method_id',$method_id)
            ->groupby('customer_requirement_methods.customer_requirement_id');
        if(!empty($method_id) && $order_type==1) {
            $details->selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name,amazon_inventories.FNSKU,amazon_inventories.id as amazon_inventory_product_id, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, customer_requirement_details.total, outbound_methods.outbound_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id,suppliers.supplier_id, shipping_methods.shipping_name, shipping_methods.shipping_method_id, shipping_quotes.chargeable_weight');
            $details->leftjoin('shipping_quotes',function($join) {
                $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
            });
        }
        $details=$details->first();
        if($user->role_id=='4') {
            $com = User_info::where('user_id',$details->user_id)->first();
            $company = $com->company_name;
        }
        $detail_method = Customer_requirement_method::where('customer_requirement_id',$quote_id)->where('shipping_method_id',$method_id)->get();
        $detail_charge = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
            ->join('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id')
            ->join('charges','charges.id','=','shipping_quote_charges.charge_id')
            ->where('shipping_quotes.customer_requirement_id',$quote_id)
            ->where('shipping_quotes.shipping_method_id',$method_id)
            ->where('shipping_quotes.is_activated','0')
            ->get();
        $detail_prep = Prep_requirement:: selectRaw('prep_services.service_name, prep_services.price, prep_requirements.*')
            ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
            ->where('customer_requirement_id',$quote_id)->get();
        $inspection_requirement_details = Inspection_requirement_detail::where('customer_requirement_id',$quote_id)->get();
        $photo_list_detail = Photo_list_detail::where('customer_requirement_id',$quote_id)->get();
        $listing_service_details = Listing_service_detail::where('customer_requirement_id',$quote_id)->get();
        $histories = QuoteHistory::where('customer_requirement_id',$quote_id)->get();

        $role_id = \Auth::user()->role_id;

        return view('admin.quotes.edit')->with(compact('details','method','detail_method','detail_charge','detail_prep','quote','inspection_requirement_details','photo_list_detail','listing_service_details','company','user_role','quote_id','method_id','histories','role_id'));
    }

}
