<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Order;
use Yajra\Datatables\Datatables;
use App\User;
use App\Role;
use App\Customer_requirement;
use Carbon\Carbon;
use App\Amazon_inventory;
use App\ZohoAccountManager;

class DatatablesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getUsers()
    {
        $users = User::all();
        return Datatables::of($users)
            ->editColumn('name', '<a href="{{ url(\'admin/users/\'.$id) }}"><b>{{ $name }}</b></a>')
            ->editColumn('role_id', function ($user) {
                if (!is_null($user->role)) {
                    return $user->role->name;
                } else {
                    return '-';
                }
            })
            ->addColumn('avatar', function ($user) {
                return '<a href="' . url('admin/users/' . $user->id) . '"><img src="' . asset($user->avatar) . '" style="height:50px;width:50px;" class="img-circle" alt="User Avatar"></a>';
            })
            ->addColumn('actions', function ($user) {
                if (\Auth::user()->role->name == 'Admin') {
                    $editBtn = '<a style="margin-right: 0.1em;" href="' . url('admin/users/' . $user->id . '/edit') . '"  title="Edit"><i class="fa fa-2 fa-pencil"></i></a>';
                    if (!is_null($user->role) && $user->role->name != 'Admin') {
                        $deleteBtn = '&nbsp;<a href="' . url('admin/users/' . $user->id) . '" class="message_box text-danger" data-box="#message-box-delete" data-action="DELETE" title="Delete"><i class="fa fa-2 fa-remove"></i></i></a><a onclick="storeuser(' . $user->id . ')"  title="Switch User">Switch User</a>';
                    } else {
                        $deleteBtn = '';
                    }
                }
                $buttons = '' . $editBtn . $deleteBtn;
                return $buttons;
            })->make(true);
    }
    public function getRoles()
    {
        $roles = Role::all();
        return Datatables::of($roles)
            ->editColumn('routes', function ($role) {
                return htmlentities(strlen($role->getOriginal('routes')) > 50 ? substr($role->getOriginal('routes'), 0, 50) : $role->getOriginal('routes'));
            })->make(true);
    }
    public function getCustomers()
    {
        $customers = User::where('role_id','3')->get();
        return Datatables::of($customers)
            ->editColumn('name', '<a href="{{ url(\'admin/customers/\'.$id) }}"><b>{{ $name }}</b></a>')
            ->editColumn('role_id', function ($customer) {
                if (!is_null($customer->role)) {
                    return $customer->role->name;
                } else {
                    return '-';
                }
            })
            ->addColumn('avatar', function ($customer) {
                return '<a href="' . url('admin/customers/' . $customer->id) . '"><img src="' . asset($customer->avatar) . '" style="height:50px;" class="img-circle" alt="User Avatar"></a>';
            })
           ->make(true);
    }
    public function getOrders()
    {
        $orders = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,orders.is_activated,customer_requirement_methods.shipping_method_id')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->where('customer_requirement_methods.is_activated','1')
            ->get();
          
        return Datatables::of($orders)
            ->editColumn('order', function ($order) {
                return $order->order_no;
            })
            ->editColumn('product', function ($order) {
                return $order->product_name;
            })
            ->editColumn('date_ordered', function ($order) {
                return date('m/d/Y',strtotime($order->created_at));
            })
            ->editColumn('status',function($order){
                $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
                return $orderStatus[$order->is_activated];
            })
            ->editcolumn('view_details', function($order){
                return '<a href="'.url('admin/order/detail/'.$order->shipping_method_id.'/'.$order->customer_requirement_id).'" class="button col-md-12">View Details</a>';
            })
            ->make(true);
    }
    public function getActivequotes()
    {
       /* $activequotes = Customer_requirement::selectRaw("customer_requirements.*, amazon_inventories.product_name, customer_requirement_methods.shipping_method_id, shipping_quotes.id as shipping_id,customer_requirement_details.product_name as customer_requirement_details_product_name, customer_requirement_details.total, customer_requirement_details.no_boxs")
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id','left')
                ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id','left')
                ->leftjoin('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id','left')
                ->where('customer_requirements.is_activated','0')
                ->orwhere('customer_requirements.is_activated','4')
                ->where('customer_requirements.expire_date','>=',Carbon::now()->toDateString())
                ->orderby('customer_requirements.id','desc')
                ->groupby('customer_requirement_methods.customer_requirement_id')
                ->groupby('shipping_quotes.customer_requirement_id')
                ->get(); */

            $activequotes = Customer_requirement::selectRaw("customer_requirements.*, amazon_inventories.product_name,customer_requirement_details.product_name as customer_requirement_details_product_name,customer_requirement_details.total, customer_requirement_details.no_boxs,customer_requirement_methods.shipping_method_id,shipping_quotes.id as shipping_id")
                                        ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                                        ->leftjoin('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                                        ->leftjoin('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                                        ->leftjoin('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id')
                                        ->whereIn('customer_requirements.is_activated',['0','5'])
                                        ->orwhere('customer_requirements.is_activated','4')
                                        ->where('customer_requirements.expire_date','>=',Carbon::now()->toDateString())
                                        ->groupby('customer_requirement_methods.customer_requirement_id')
                                        ->groupby('shipping_quotes.customer_requirement_id')
                                         ->orderby('customer_requirements.id','desc')
                                        ->get();    

        return Datatables::of($activequotes)
            ->editColumn('product', function ($activequote) {
                return $activequote->product_name;
            })
            ->editColumn('company', function ($activequote) {
                return $activequote->company_name;
            })
            ->editColumn('date_ordered', function ($activequote) {
                return date('m/d/Y',strtotime($activequote->created_at));
            })
            ->editcolumn('view_quote', function($activequote){
                return '<a href="'.url('admin/quote/edit/'.$activequote->id.'/active_quotes').'" class="button">View & Approve Quote</a>';
            })
            ->make(true);
    }
    public function getExpirequotes()
    {
        $expirequotes = Customer_requirement::selectRaw("user_infos.company_name, customer_requirements.*, amazon_inventories.product_name,shipping_quotes.id as shipping_id")
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->leftjoin('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id')
            ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
            ->where('customer_requirements.is_activated','0')
            ->where('customer_requirements.expire_date','<',Carbon::now()->toDateString())
            ->groupby('shipping_quotes.customer_requirement_id')
            ->orderby('customer_requirements.id','desc')
            ->get();
        return Datatables::of($expirequotes)
            ->editColumn('product', function ($expirequote) {
                return $expirequote->product_name;
            })
            ->editColumn('company', function ($expirequote) {
                return $expirequote->company_name;
            })
            ->editColumn('date_ordered', function ($expirequote) {
                return date('m/d/Y',strtotime($expirequote->created_at));
            })
            ->editcolumn('view_quote', function($expirequote){
                return '<a href="'.url('admin/quote/edit/'.$expirequote->id.'/expire_quotes').'" class="button">View & Approve Quote</a>';
            })
            ->make(true);
    }
    public function getPastquotes()
    {
        $pastquotes = Customer_requirement::selectRaw("user_infos.company_name, customer_requirements.*, amazon_inventories.product_name,shipping_quotes.id as shipping_id, shipping_methods.shipping_name")
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id','left')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id','left')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
            ->where('customer_requirement_methods.is_activated','1')
            ->whereIn('customer_requirements.is_activated',['1','2'])
            ->orderby('customer_requirements.id','desc')
            ->groupby('shipping_quotes.customer_requirement_id')
            ->get();
        return Datatables::of($pastquotes)
            ->editColumn('product', function ($pastquote) {
                return $pastquote->product_name;
            })
            ->editColumn('company', function ($pastquote) {
                return $pastquote->company_name;
            })
            ->editColumn('date_ordered', function ($pastquote) {
                return date('m/d/Y',strtotime($pastquote->created_at));
            })
            ->editcolumn('view_quote', function($pastquote){
                return '<a href="'.url('admin/quote/edit/'.$pastquote->id.'/past_quotes').'" class="button">View & Approve Quote</a>';
            })
            ->make(true);
    }
    public function getInventories()
    {
        $inventories = Amazon_inventory::selectRaw('amazon_inventories.*, sum(customer_requirement_details.no_boxs) as carton, sum(customer_requirement_details.total) as units')
            ->join('customer_requirement_details','customer_requirement_details.product_id','=','amazon_inventories.id')
            ->join('customer_requirements','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('orders','customer_requirements.order_id','=','orders.order_id')
            ->whereBetween('orders.is_activated',array('0','11'))
            ->groupby('customer_requirement_details.product_id')
            ->get();
        
        return Datatables::of($inventories)
            ->editColumn('product', function ($inventory) {
                return $inventory->product_name;
            })
            ->editColumn('sku', function ($inventory) {
                return $inventory->sellerSKU;
            })
            ->editColumn('last_updated', function ($inventory) {
                return date('m/d/Y',strtotime($inventory->updated_at));
            })
            ->editColumn('infba',function($inventory){
                return $inventory->infbaforward;
            })
            ->editColumn('processfba',function($inventory){
                return $inventory->processfbaforward;
            })
            ->editColumn('storefba',function($inventory){
                return $inventory->atfbaforward;
            })
            ->editColumn('inamazon',function($inventory){
                return $inventory->inamazon;
            })
            ->editColumn('atamazon',function($inventory){
                return $inventory->atamazon;
            })
            ->editcolumn('action', function($inventory) {
                
                $role_id =  \Auth::user()->role_id;

                return '<div class="dropdown"><i onclick="myFunction('.$inventory->id.')" class="dropbtn fa fa-cog"></i><div id="myDropdown'.$inventory->id.'" class="dropdown-content"> <a href="'.url('amazoninventory/create?product_id='.$inventory->id).'">Send Inventory to Amazon</a> <a href="'.url('quote/create?product_id='.$inventory->id.'&user_role='.$role_id).'">Get New Shipping Quote</a> <a onclick="getnickname('.$inventory->id.','.$inventory->product_nick_name.')">Edit Nickname</a> </div> </div>';
            })
            ->make(true);
    }

    public function getZohoaccountmanagers()
    {
        $zohoaccountmanagers = ZohoAccountManager::all();
        return Datatables::of($zohoaccountmanagers)
            ->editColumn('name',function($zohoaccountmanager){
                return $zohoaccountmanager->name;
            })
            ->addColumn('actions', function ($zohoaccountmanager) {
                $buttons = '<a style="margin-right: 0.1em;" href="' . url('admin/zohoaccountmanagers/' . $zohoaccountmanager->id . '/edit') . '"  title="Edit"><i class="fa fa-2 fa-pencil"></i></a>';

                return $buttons;
            })->make(true);
    }


    public function getOrderreports()
    {
        $zohoaccountmanagers = ZohoAccountManager::all();
        return Datatables::of($zohoaccountmanagers)
            ->editColumn('name',function($zohoaccountmanager){
                return $zohoaccountmanager->name;
            })
            ->addColumn('actions', function ($zohoaccountmanager) {
                $buttons = '<a style="margin-right: 0.1em;" href="' . url('admin/zohoaccountmanagers/' . $zohoaccountmanager->id . '/edit') . '"  title="Edit"><i class="fa fa-2 fa-pencil"></i></a>';

                return $buttons;
            })->make(true);
    }
}
				