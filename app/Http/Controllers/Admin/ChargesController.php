<?php
namespace App\Http\Controllers\Admin;

use App\Charge_category;
use App\Charges;
use App\Http\Requests\ChargesRequest;
use App\Http\Controllers\Controller;
class ChargesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $category= Charge_category::all();
        return view('admin.Charges.index')->with(compact('category'));
    }
    public function create()
    {
        $category= Charge_category::all();
        return view('admin.Charges.create_edit')->with(compact('category'));
    }
    public function store(ChargesRequest $request,Charges $charges)
    {
        $charges->name = $request->input('name');
        $charges->category = $request->input('category');
        $charges->price = $request->input('price');
        $charges->save();
        return redirect('admin/charges')->with('success', $charges->name . ' charges Added Successfully');
    }
    public function edit(Charges $charges)
    {
        $category= Charge_category::all();
        return view('admin.Charges.create_edit')->with(compact('charges','category'));
    }
    public function update(ChargesRequest $request, Charges $charges)
    {
        $charges->name = $request->input('name');
        $charges->category = $request->input('category');
        $charges->price = $request->input('price');
        $charges->save();
        return redirect('admin/charges')->with('success', $charges->name . ' Charges Updated Successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param OutboundMethodRequest $request
     * @param Package $package
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(ChargesRequest $request, Charges $charges)
    {
        if ($request->ajax()) {

            $charges->delete();

            return response()->json(['success' => 'Charges has been deleted successfully']);
        } else {
            return 'You can\'t proceed in delete operation';
        }
    }
}