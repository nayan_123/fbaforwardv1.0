<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\ZohoAccountManager;

class ZohoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('admin.zoho_manager.index');
    }
    public function create()
    {
    }
    public function store(Request $request)
    {
    }
    public function show($id)
    {
    }
    public function edit($id)
    {
    	$data['zoho_account_manager'] = $id;
    	return view('admin.zoho_manager.edit',$data);
    }
    public function update(Request $request)
    {
    	if($request->image){
	        $destinationPath = public_path() . '/uploads/zoho_managers';
	        $request->image->move($destinationPath, $request->image->getClientOriginalName());

	        ZohoAccountManager::where('id',$request->id)->update(['image' => $request->image->getClientOriginalName()]);
	    }

        return back();
    }
    public function destroy($id)
    {
    }
}