<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;
use App\Amazon_inventory;
use Yajra\Datatables\Datatables;


class InventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('admin.inventories.index');
    }
    public function create()
    {
    }
    public function store(Request $request)
    {
    }
    public function show($id)
    {
    }
    public function edit($id)
    {
    }
    public function update(Request $request, $id)
    {
    }
    public function destroy($id)
    {
    }


    public function export(){

        Excel::create('Past Quotes Data', function($excel) {
            
            $excel->sheet('Past Quotes Data', function($sheet) {

                $inventories = Amazon_inventory::selectRaw('amazon_inventories.*, sum(customer_requirement_details.no_boxs) as carton, sum(customer_requirement_details.total) as units')
                ->join('customer_requirement_details','customer_requirement_details.product_id','=','amazon_inventories.id')
                ->join('customer_requirements','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
                ->join('orders','customer_requirements.order_id','=','orders.order_id')
                ->whereBetween('orders.is_activated',array('0','11'))
                ->groupby('customer_requirement_details.product_id')
                ->get();

                if(count($inventories) == 0){
                    return back();
                }
                
                $sheet->loadView('admin.inventories.export')->with(['inventories' => $inventories]);
            
            });

        })->export('xls');
    }


    public function inventoryReport(){

        $data['product'] = Amazon_inventory::where('hide_show','0')->get();

        return view('admin.reports.inventory_report',$data);
    }

    public function generateReport(Request $request){

        $inventories = Amazon_inventory::selectRaw('amazon_inventories.*, sum(customer_requirement_details.no_boxs) as carton, sum(customer_requirement_details.total) as units')
            ->join('customer_requirement_details','customer_requirement_details.product_id','=','amazon_inventories.id')
            ->join('customer_requirements','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('orders','customer_requirements.order_id','=','orders.order_id')
            ->whereBetween('orders.is_activated',array('0','11'))
            ->groupby('customer_requirement_details.product_id');

        if($request->product_name){
            $inventories = $inventories->where('amazon_inventories.id',$request->product_name);
        }

        if(($request->from) && ($request->to)){
            $inventories = $inventories->whereBetween('amazon_inventories.updated_at', [\Carbon\Carbon::parse($request->from)->format('Y-m-d h:i:s'), \Carbon\Carbon::parse($request->to)->format('Y-m-d h:i:s')]);
        }

        $inventories = $inventories->get();

        return Datatables::of($inventories)
                ->editColumn('product', function ($inventory) {
                    return $inventory->product_name;
                })
                ->editColumn('sku', function ($inventory) {
                    return $inventory->sellerSKU;
                })
                ->editColumn('last_updated', function ($inventory) {
                    return date('m/d/Y',strtotime($inventory->updated_at));
                })
                ->editColumn('infba',function($inventory){
                    return $inventory->infbaforward;
                })
                ->editColumn('processfba',function($inventory){
                    return $inventory->processfbaforward;
                })
                ->editColumn('storefba',function($inventory){
                    return $inventory->atfbaforward;
                })
                ->editColumn('inamazon',function($inventory){
                    return $inventory->inamazon;
                })
                ->editColumn('atamazon',function($inventory){
                    return $inventory->atamazon;
                })
            ->make(true);

    }
}
