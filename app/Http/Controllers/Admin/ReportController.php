<?php

namespace App\Http\Controllers\Admin;

use App\Addresses;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\AddressesRequest;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function orderReport(){

        return view('admin.reports.order_report');
    }

}
