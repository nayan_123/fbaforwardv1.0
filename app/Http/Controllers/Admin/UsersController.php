<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\User_info;
use Illuminate\Support\Facades\DB;
use CountryState;
use App\Business_type;
use App\Taxes;
use App\Customer_amazon_detail;
use App\Addresses;
use App\User_credit_cardinfo;
use App\Power_of_attorny;
use App\Ein_notice;
use App\Itin_notice;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('admin.users.index');
    }
    public function create()
    {
        $roles = Role::lists('name', 'id');
        $country = CountryState::getCountries();

        $business_type = Business_type::get();
        $taxes = Taxes::get();
        $country = CountryState::getCountries();
        $states = CountryState::getStates('US');

        $arrMarketplaces = DB::table('amazon_marketplaces')->get();
        $market_place_id = '';
        if (!empty($arrMarketplaces))
            $market_place_id = $arrMarketplaces[0]->id;

        return view('admin.users.create_edit')->with(compact('roles','country','states','user', 'business_type', 'country', 'states', 'user_details', 'taxes', 'customer_amazon_detail', 'card_detail', 'address_detail','attorney'));
    }
    public function store(UserRequest $request)
    {
        $account = [];
        $avatar = 'avatar.png';
        if ($request->hasFile('avatar')) {
            $destinationPath = public_path() . '/uploads/avatars';
            $avatar = hash('sha256', mt_rand()) . '.' . $request->file('avatar')->getClientOriginalExtension();
            $request->file('avatar')->move($destinationPath, $avatar);
        }
        $account['avatar'] = $avatar;
        $account['name'] = $request->input('name');
        $account['email'] = $request->input('email');
        $account['role_id'] = $request->input('role');
        $user = new User($account);
        $user->password = bcrypt($request->input('password'));
        $user->save();
        if($user->role_id == 3){
            // $user_info=array(
            //     'user_id' => $user->id,
            //     'company_name' => $request->input('company_name'),
            //     'company_phone' => $request->input('company_phone'),
            //     'company_address' => $request->input('company_address'),
            //     'company_address2' => $request->input('company_address2'),
            //     'company_city' => $request->input('company_city'),
            //     'company_state' => $request->input('company_state'),
            //     'company_zipcode' => $request->input('company_zipcode'),
            //     'company_country' => $request->input('company_country'),
            //     'tax_id_number' => $request->input('tax_id_number'),
            //     'primary_bussiness_type' => $request->input('business_type'),
            //     'estimate_annual_amazon_revenue' => $request->input('annual_amazon_revenue'),
            //     'estimate_annual_fba_order' => $request->input('annual_fba_order'),
            //     'reference_from' => $request->input('reference_from'),
            //     'contact_fname' => $request->input('contact_fname'),
            //     'contact_lname' => $request->input('contact_lname'),
            //     'contact_email' => $request->input('contact_email'),
            //     'contact_phone' => $request->input('contact_phone'),
            //     'secondary_contact_phone' => $request->input('secondary_contact_phone'),
            //     'secondary_contact_email' => $request->input('secondary_contact_email'),
            //     'account_payable' => $request->input('account_payable'),
            //     'account_email' => $request->input('account_email'),
            //     'account_phone' => $request->input('account_phone'),
            // );

            // $data = array(
            //     'company_name' => $request->input('company_name'),
            //     'company_address' => $request->input('address1'),
            //     'company_address2' => $request->input('address2'),
            //     'company_city' => $request->input('city'),
            //     'company_zipcode' => $request->input('postal_code'),
            //     'company_country' => $request->input('country'),
            //     'tax_id' => $request->input('tax_id'),
            //     'contact_fname' => $request->input('contact_fname'),
            //     'contact_lname' => $request->input('contact_lname'),
            //     'contact_email' => $request->input('primary_email'),
            //     'contact_country_code'=>$request->input('ccode'),
            //     'contact_phone' => $request->input('primary_phone_number'),
            //     'secondary_contact_fname' => $request->input('secondary_contact_fname'),
            //     'secondary_contact_lname' => $request->input('secondary_contact_lname'),
            //     'secondary_contact_email' => $request->input('secondary_email'),
            //     'secondary_contact_country_code'=>$request->input('secondary_contact_country_code'),
            //     'secondary_contact_phone' => $request->input('secondary_phone_number'),
            //     'third_contact_fname' => $request->input('third_contact_fname'),
            //     'third_contact_lname' => $request->input('third_contact_lname'),
            //     'third_contact_email' => $request->input('third_email'),
            //     'third_contact_country_code'=>$request->input('third_contact_country_code'),
            //     'third_contact_phone' => $request->input('third_phone_number'),
            //     'primary_bussiness_type' =>  $request->input('btype'),
            //     'business_as' =>  $request->input('business_as'),
            //     'business_country' =>  $request->input('business_country'),
            //     'business_state' => $request->input('business_state'),
            //     'tax_id' =>  $request->input('tax_id'),
            //     'business_email' => $request->input('business_email'),
            //     'business_phone' => $request->input('business_phone'),
            // );
            // $user_info = new User_info($user_info);
            // $user_info->save();

            $user_id = $user->id;
            
            if($request->ssn)
                $ssn=$request->input('ssn');
            else
                $ssn = '';

            $data = array(
                'company_name' => $request->input('lcname'),
                'company_address' => $request->input('address1'),
                'company_address2' => $request->input('address2'),
                'company_city' => $request->input('city'),
                'company_zipcode' => $request->input('postal_code'),
                'company_country' => $request->input('country'),
                'tax_id' => $request->input('tax_id'),
                'contact_fname' => $request->input('contact_fname'),
                'contact_lname' => $request->input('contact_lname'),
                'contact_email' => $request->input('primary_email'),
                'contact_country_code'=>$request->input('ccode'),
                'contact_phone' => $request->input('primary_phone_number'),
                'secondary_contact_fname' => $request->input('secondary_contact_fname'),
                'secondary_contact_lname' => $request->input('secondary_contact_lname'),
                'secondary_contact_email' => $request->input('secondary_email'),
                'secondary_contact_country_code'=>$request->input('secondary_contact_country_code'),
                'secondary_contact_phone' => $request->input('secondary_phone_number'),
                'third_contact_fname' => $request->input('fname2'),
                'third_contact_lname' => $request->input('lname2'),
                'third_contact_email' => $request->input('email2'),
                'third_contact_country_code'=>$request->input('ccode2'),
                'third_contact_phone' => $request->input('phone2'),
                'primary_bussiness_type' =>  $request->input('btype'),
                'business_as' =>  $request->input('business_as'),
                'business_country' =>  $request->input('business_country'),
                'business_state' => $request->input('business_state'),
                'tax_id' =>  $request->input('taxid'),
                'user_id' => $user_id,
                'ssn' => $request->ssn
                // 'business_email' => $request->input('business_email'),
                // 'business_phone' => $request->input('business_phone'),
            );
            User_info::create($data);
            if ($request->hasFile('ein')) {
                $getein= Ein_notice::where('user_id',$user_id)->get()->toArray();
                if(empty($getein)) {
                    $ein_file = $request->file('ein');
                    $ein_destinationPath = public_path() . '/uploads/ein_notice';
                    $ein_notice = $user_id . "_" . 'ein_notice.' . $request->file('ein')->getClientOriginalExtension();
                    $ein_file->move($ein_destinationPath, $ein_notice);
                    Ein_notice::create(array('user_id'=>$user_id,'file'=>$ein_notice));
                }
                else {
                    @unlink($getein['file']);
                    $ein_file = $request->file('ein');
                    $ein_destinationPath = public_path() . '/uploads/ein_notice';
                    $ein_notice = $user_id . "_" . 'ein_notice.' . $request->file('ein')->getClientOriginalExtension();
                    $ein_file->move($ein_destinationPath, $ein_notice);
                    Ein_notice::where('user_id',$user_id)->update(array('file'=>$ein_notice));
                }
            }
            if ($request->hasFile('itin')) {
                $getitin= Itin_notice::where('user_id',$user_id)->get()->toArray();
                if(empty($getitin)) {
                    $itin_file = $request->file('itin');
                    $itin_destinationPath = public_path() . '/uploads/itin_notice';
                    $itin_notice = $user_id . "_" . 'itin_notice.' . $request->file('itin')->getClientOriginalExtension();
                    $itin_file->move($itin_destinationPath, $itin_notice);
                    Itin_notice::create(array('user_id'=>$user_id,'file'=>$itin_notice));
                }
                else {
                    @unlink($getitin['file']);
                    $itin_file = $request->file('itin');
                    $itin_destinationPath = public_path() . '/uploads/itin_notice';
                    $itin_notice = $user_id . "_" . 'itin_notice.' . $request->file('itin')->getClientOriginalExtension();
                    $itin_file->move($itin_destinationPath, $itin_notice);
                    Itin_notice::where('user_id',$user_id)->update(array('file'=>$itin_notice));
                }
            }


        }
        $status_message = $user->name . ' has been added Successfully.';
        return redirect('admin/users')->with('success', $status_message);
    }
    public function show(User $user)
    {
        $user_info = DB::table('user_infos')->where('user_id', $user->id)->get();
        return view('admin.users.show')->with(compact('user','user_info'));
    }
    public function edit(User $user)
    {
        $roles = Role::lists('name', 'id');
        // $user_info = DB::table('user_infos')->where('user_id', $user->id)->get();
        // $country = CountryState::getCountries();
        // $state='US';
        // if(!empty($user_info))
        // {
        //     $state=$user_info[0]->company_country;
        // }
        // $states = CountryState::getStates($state);



        $business_type = Business_type::get();
        $taxes = Taxes::get();
        $country = CountryState::getCountries();
        $user_details = User_info::where('user_id', $user->id)->first();
        if(!empty($user_details->business_country)) {
            $states = CountryState::getStates($user_details->business_country);
        }
        else{
            $states = CountryState::getStates('US');
        }
        $arrMarketplaces = DB::table('amazon_marketplaces')->get();
        $market_place_id = '';
        if (!empty($arrMarketplaces))
            $market_place_id = $arrMarketplaces[0]->id;
        $customer_amazon_detail = Customer_amazon_detail::selectRaw('customer_amazon_details.*, amazon_marketplaces.market_place_id ')
            ->join('amazon_marketplaces', 'amazon_marketplaces.id', '=', 'customer_amazon_details.mws_market_place_id')
            ->where('user_id', '=', $user->id)->first();
        if (empty($customer_amazon_detail)) {
            Customer_amazon_detail::create([
                'user_id' => $user->id,
                'mws_seller_id' => '',
                'mws_market_place_id' => $market_place_id,
                'mws_authtoken' => '',
            ]);
            $customer_amazon_detail = Customer_amazon_detail::selectRaw('customer_amazon_details.*, amazon_marketplaces.market_place_id ')
                ->join('amazon_marketplaces', 'amazon_marketplaces.id', '=', 'customer_amazon_details.mws_market_place_id')
                ->where('user_id', '=', $user->id)->first();
        }
        $address_detail = Addresses::where('user_id', $user->id)->Orderby('address_id', 'desc')->limit(1)->get()->toArray(); 
        $card_detail = User_credit_cardinfo::where('user_id', $user->id)->Orderby('id', 'desc')->get()->toArray();


        return view('admin.users.create_edit')->with(compact('user', 'roles','user_info','country','states','business_type','taxes','user_details','arrMarketplaces','customer_amazon_detail'));
    }
    public function selectState(Request $request)
    {
        if ($request->ajax()) {
            $post = $request->all();
            $states = CountryState::getStates($post['country_code']);
            return view('admin.users.change_state')->with(compact('states'));
        }
    }
    public function update(UserRequest $request, User $user)
    {
        if ($request->hasFile('avatar')) {
            $destinationPath = public_path() . '/uploads/avatars';
            if ($user->avatar != "uploads/avatars/avatar.png") {
                @unlink($user->avatar);
            }
            $avatar = hash('sha256', mt_rand()) . '.' . $request->file('avatar')->getClientOriginalExtension();
            $request->file('avatar')->move($destinationPath, $avatar);
            $user->avatar = $avatar;
        }
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->updated_at = \Carbon::now();
        $user->role_id = $request->input('role');
        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();
        // $user_info= array(
        //     'company_name' => $request->input('company_name'),
        //     'company_phone' => $request->input('company_phone'),
        //     'company_address' => $request->input('company_address'),
        //     'company_address2' => $request->input('company_address2'),
        //     'company_city' => $request->input('company_city'),
        //     'company_state' => $request->input('company_state'),
        //     'company_zipcode' => $request->input('company_zipcode'),
        //     'company_country' => $request->input('company_country'),
        //     'tax_id_number' => $request->input('tax_id_number'),
        //     'primary_bussiness_type' => $request->input('business_type'),
        //     'estimate_annual_amazon_revenue' => $request->input('annual_amazon_revenue'),
        //     'estimate_annual_fba_order' => $request->input('annual_fba_order'),
        //     'reference_from' => $request->input('reference_from'),
        //     'contact_fname' => $request->input('contact_fname'),
        //     'contact_lname' => $request->input('contact_lname'),
        //     'contact_email' => $request->input('contact_email'),
        //     'contact_phone' => $request->input('contact_phone'),
        //     'secondary_contact_phone' => $request->input('secondary_contact_phone'),
        //     'secondary_contact_email' => $request->input('secondary_contact_email'),
        //     'account_payable' => $request->input('account_payable'),
        //     'account_email' => $request->input('account_email'),
        //     'account_phone' => $request->input('account_phone')
        // );
        // User_info::where("user_id", "=", $user->id)->update($user_info);
        
        $user_id = $user->id;

        $data = array(
            'company_name' => $request->input('lcname'),
            'company_address' => $request->input('address1'),
            'company_address2' => $request->input('address2'),
            'company_city' => $request->input('city'),
            'company_zipcode' => $request->input('postal_code'),
            'company_country' => $request->input('country'),
            'tax_id' => $request->input('tax_id'),
            'contact_fname' => $request->input('contact_fname'),
            'contact_lname' => $request->input('contact_lname'),
            'contact_email' => $request->input('primary_email'),
            'contact_country_code'=>$request->input('ccode'),
            'contact_phone' => $request->input('primary_phone_number'),
            'secondary_contact_fname' => $request->input('secondary_contact_fname'),
            'secondary_contact_lname' => $request->input('secondary_contact_lname'),
            'secondary_contact_email' => $request->input('secondary_email'),
            'secondary_contact_country_code'=>$request->input('secondary_contact_country_code'),
            'secondary_contact_phone' => $request->input('secondary_phone_number'),
            'third_contact_fname' => $request->input('fname2'),
            'third_contact_lname' => $request->input('lname2'),
            'third_contact_email' => $request->input('email2'),
            'third_contact_country_code'=>$request->input('ccode2'),
            'third_contact_phone' => $request->input('phone2'),
            'primary_bussiness_type' =>  $request->input('btype'),
            'business_as' =>  $request->input('business_as'),
            'business_country' =>  $request->input('business_country'),
            'business_state' => $request->input('business_state'),
            'tax_id' =>  $request->input('taxid'),
            // 'business_email' => $request->input('business_email'),
            // 'business_phone' => $request->input('business_phone'),
            );
            User_info::where('user_id', $user_id)->update($data);
            if ($request->hasFile('ein')) {
                $getein= Ein_notice::where('user_id',$user_id)->get()->toArray();
                if(empty($getein)) {
                    $ein_file = $request->file('ein');
                    $ein_destinationPath = public_path() . '/uploads/ein_notice';
                    $ein_notice = $user_id . "_" . 'ein_notice.' . $request->file('ein')->getClientOriginalExtension();
                    $ein_file->move($ein_destinationPath, $ein_notice);
                    Ein_notice::create(array('user_id'=>$user_id,'file'=>$ein_notice));
                }
                else {
                    @unlink($getein['file']);
                    $ein_file = $request->file('ein');
                    $ein_destinationPath = public_path() . '/uploads/ein_notice';
                    $ein_notice = $user_id . "_" . 'ein_notice.' . $request->file('ein')->getClientOriginalExtension();
                    $ein_file->move($ein_destinationPath, $ein_notice);
                    Ein_notice::where('user_id',$user_id)->update(array('file'=>$ein_notice));
                }
            }
            if ($request->hasFile('itin')) {
                $getitin= Itin_notice::where('user_id',$user_id)->get()->toArray();
                if(empty($getitin)) {
                    $itin_file = $request->file('itin');
                    $itin_destinationPath = public_path() . '/uploads/itin_notice';
                    $itin_notice = $user_id . "_" . 'itin_notice.' . $request->file('itin')->getClientOriginalExtension();
                    $itin_file->move($itin_destinationPath, $itin_notice);
                    Itin_notice::create(array('user_id'=>$user_id,'file'=>$itin_notice));
                }
                else {
                    @unlink($getitin['file']);
                    $itin_file = $request->file('itin');
                    $itin_destinationPath = public_path() . '/uploads/itin_notice';
                    $itin_notice = $user_id . "_" . 'itin_notice.' . $request->file('itin')->getClientOriginalExtension();
                    $itin_file->move($itin_destinationPath, $itin_notice);
                    Itin_notice::where('user_id',$user_id)->update(array('file'=>$itin_notice));
                }
            }
            if($request->ssn) {
                User_info::where('user_id',$user_id)->update(['ssn' => $request->ssn]);
            }

        $status_message = $user->name . ' has been updated Successfully.';
        return redirect('admin/users')->with('success', $status_message);
    }
    public function destroy(Request $request, User $user)
    {
        if ($request->ajax()) {
            User_info::where("user_id", "=", $user->id)->delete();
            $user->delete();
            return response()->json(['success' => 'User has been deleted successfully']);
        } else {
            return 'You can\'t proceed in delete operation';
        }
    }
}