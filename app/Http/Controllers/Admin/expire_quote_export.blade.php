<html>
<table>
    <tr>
        <th>Id</th>
        <th>Product Name</th>
        <th>Company</th>
        <th>Date Quated</th>
    </tr>
    @foreach($expirequotes as $expirequote)
        <tr>
            <td>{{ $expirequote->id }}</td>
            <td>{{ $expirequote->product_name }}</td>
            <td>{{ $expirequote->company_name }}</td>
            <td>{{ date('m/d/Y',strtotime($expirequote->created_at)) }}</td>
        </tr>
    @endforeach
</table>
</html>