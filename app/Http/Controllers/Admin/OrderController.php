<?php

namespace App\Http\Controllers\Admin;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;
use App\Amazon_inventory;
use Yajra\Datatables\Datatables;
use \Carbon\Carbon;
use App\Customer_requirement;
use App\Shipping_method;
use App\Shipping_quote_charge;
use App\Prep_requirement;
use App\Order_document;
use Auth;
use App\OrderHistory;
use App\QuoteHistory;
use App\Listing_service_detail;
use App\Photo_list_detail;
use App\PreShipment;
use App\Document;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('admin.orders.index');
    }
    public function create()
    {
    }
    public function store(Request $request)
    {
    }
    public function show($id)
    {
        $orders = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated,customer_requirement_methods.shipping_method_id')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.customer_requirement_id')
            ->where('orders.order_id',$id)
            ->get();
        return view('admin.orders.show')->with(compact('orders'));
    }
    public function edit($id)
    {
    }
    public function update(Request $request, $id)
    {
    }
    public function destroy($id)
    {
    }

    public function export(){

        Excel::create('Order Data', function($excel) {
            
            $excel->sheet('Order Data', function($sheet) {

                $orders = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,orders.is_activated,customer_requirement_methods.shipping_method_id')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->where('customer_requirement_methods.is_activated','1')
                ->get();

                $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Warehouse Processing Complete','Released to Amazon, Complete','Storage');

                $sheet->loadView('admin.orders.export')->with(['orders' => $orders,'orderStatus' => $orderStatus]);
            
            });

        })->export('xls');
    }


    public function orderReport(){

        $data['product'] = Amazon_inventory::where('hide_show','0')->get();

        return view('admin.reports.order_report',$data);
    }

    public function generateReport(Request $request){

        $orders = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,orders.is_activated,customer_requirement_methods.shipping_method_id')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->where('customer_requirement_methods.is_activated','1');

        if($request->product_name){
            $orders = $orders->where('amazon_inventories.id',$request->product_name);
        }

        if($request->status){
            $orders = $orders->where('orders.is_activated','=',$request->status);
        }

        if(($request->from) && ($request->to)){
            $orders = $orders->whereBetween('orders.created_at', [\Carbon\Carbon::parse($request->from)->format('Y-m-d h:i:s'), \Carbon\Carbon::parse($request->to)->format('Y-m-d h:i:s')]);
        }

        $orders = $orders->get();

        return Datatables::of($orders)
            ->editColumn('order', function ($order) {
                return $order->order_no;
            })
            ->editColumn('product', function ($order) {
                return $order->product_name;
            })
            ->editColumn('date_ordered', function ($order) {
                return date('m/d/Y',strtotime($order->created_at));
            })
            ->editColumn('status',function($order){
                $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Warehouse Processing Complete','Released to Amazon, Complete','Storage');
                return $orderStatus[$order->is_activated];
            })
            ->make(true);

    }

    public function getChart(Request $request){

        $order = Order::select('order_id', 'created_at')
                ->whereYear('created_at','=',$request->year)
                ->get()
                ->groupBy(function($date) {
                    return Carbon::parse($date->created_at)->format('m'); // grouping by months
                });

        $usermcount = [];
        $order_year_count = [];

        foreach ($order as $key => $value) {
            $usermcount[(int)$key] = count($value);
        }

        for($i = 1; $i <= 12; $i++){
            if(!empty($usermcount[$i])){
                $order_year_count[$i] = $usermcount[$i];    
            }else{
                $order_year_count[$i] = 0;    
            }
        }

        return json_encode($order_year_count);
    }


    public function viewOrderDetail($method_id,$customer_requirement_id){
        $details = Customer_requirement::selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, customer_requirement_details.total, outbound_methods.outbound_name, outbound_methods.short_name as outbound_method_short_name, shipment_types.id as shipment_type_id, shipment_types.container_unload_rate, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, shipment_types.trucking_rate, incoterms.id as incoterm_id, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name, outbound_methods.outbound_method_id,suppliers.contact_name,prealert_details.*,user_infos.zoho_id,user_infos.user_id,customer_requirement_details.product_name as customer_requirement_details_product_name, prealert_details.id as prealert_id, orders.hold, customer_requirement_methods.shipping_method_id, customer_clearances.created_at as custom_release_date, delivery.ETA_to_warehouse, delivery.created_at as cargo_release, delivery.id as delivery_id, warehouse_checkins.created_at as checkin_date, warehouse_checkins.id as checkin_id,orders.order_id,orders.order_no')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id','left')
            ->leftjoin('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
            ->leftjoin('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type','left')
            ->leftjoin('incoterms','incoterms.id','=','customer_requirements.incoterms','left')
            ->leftjoin('suppliers','customer_requirements.supplier','=','suppliers.supplier_id','left')
            ->leftjoin('prealert_details','prealert_details.order_id','=','customer_requirements.order_id','left')
            ->leftjoin('customer_clearances','customer_clearances.order_id','=','customer_requirements.order_id','left')
            ->leftjoin('delivery','delivery.order_id','=','customer_requirements.order_id','left')
            ->leftjoin('warehouse_checkins','warehouse_checkins.order_id','=','customer_requirements.order_id','left')
            ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id','left')
            ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id','left')
            ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id','left')
            ->join('orders','orders.order_id','=','customer_requirements.order_id')
            ->where('customer_requirements.id',$customer_requirement_id)
            ->where('customer_requirement_methods.shipping_method_id',$method_id)
            ->groupby('customer_requirement_methods.customer_requirement_id');
        if(!empty($method_id)) {
            $details->selectRaw('customer_requirements.*, amazon_inventories.product_name, amazon_inventories.product_nick_name, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, shipping_methods.shipping_name, shipping_methods.shipping_method_id, shipping_quotes.chargeable_weight,shipping_quotes.shipping_port');
            $details->leftjoin('shipping_quotes',function($join) {
                $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
            });
        }
        $data['order']=$details->first();
        $data['shipping_method'] = Shipping_method::where('shipping_method_id',$method_id)->first();
        $data['detail_charge'] = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
            ->join('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id','left')
            ->join('charges','charges.id','=','shipping_quote_charges.charge_id','left')
            ->where('shipping_quotes.customer_requirement_id',$customer_requirement_id)
            ->where('shipping_quotes.shipping_method_id',$method_id)
            ->where('shipping_quotes.is_activated','0')
            ->get();
        $data['detail_prep'] = Prep_requirement:: selectRaw('prep_services.service_name, prep_services.price, prep_requirements.*')
            ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
            ->where('customer_requirement_id',$customer_requirement_id)->get();
        $data['order_documents'] = Order_document::where('order_id',$data['order']['order_id'])->get();
        $data['user_role'] = Auth::user()->role_id;
        $data['order_histories'] = OrderHistory::selectRaw('order_history.*,prealert_details.ETD_china,prealert_details.created_at as prealert_details_created_at,customer_requirements.port_of_origin, customer_requirements.shipper_destination,customer_requirements.id as customer_requirements_id')
                                ->join('prealert_details','prealert_details.order_id','=','order_history.order_id','left')
                                ->join('customer_requirements','customer_requirements.order_id','=','order_history.order_id','left')
                                ->where('order_history.order_id',$data['order']['id'])
                                ->get();
        $data['quote_histories'] = QuoteHistory::where('customer_requirement_id',$customer_requirement_id)->whereIN('quote_history_status',['3','5'])->get();
        $data['listing_services'] = Listing_service_detail::where('customer_requirement_id',$customer_requirement_id)
            ->where('shipping_method_id',$method_id)
            ->join('listing_services','listing_service_details.listing_service_id','=','listing_services.listing_service_id')
            ->get();
        $data['photo_services'] = Photo_list_detail::where('customer_requirement_id',$customer_requirement_id)
            ->where('shipping_method_id',$method_id)
            ->join('photo_services','photo_list_details.photo_service_id','=','photo_services.id')
            ->get();
        $data['pre_shipment'] = PreShipment::where('customer_requirement_id',$customer_requirement_id)->get();
        $data['method_id'] = $method_id;
        $data['customer_requirement_id'] = $customer_requirement_id;
        $order_id = $data['order']['order_id'];
        $data['documents'] = Document::selectRaw('documents.*,documents.document as document_name, order_documents.order_id,order_documents.status,order_documents.document_id,order_documents.created_at as order_document_created_at,order_documents.id as order_document_id,order_documents.due_date');
            $data['documents']->leftjoin('order_documents',function($join) use ($order_id){
                $join->on('order_documents.document_id','=','documents.id');
                $join->where('order_documents.order_id','=', $order_id);
            });
            $data['documents'] = $data['documents']->get();

        return view('admin.orders.view_order',$data);
    }


}
