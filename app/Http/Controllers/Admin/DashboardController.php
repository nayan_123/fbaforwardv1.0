<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Customer_requirement;
use App\Http\Controllers\Controller;
use App\User;
use App\Order;
use App\Payment_detail;
use Carbon\Carbon;
use App\Amazon_inventory;
use App\Payment_info;


class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $users = User::all()->count();
        $customers=User::where('role_id','3')->count();
        $active_order = Order::where('is_activated','<=','11')->count();
        $active_quote = Customer_requirement::where('is_activated','0')->Orwhere('is_activated','4')->count();
        $past_quote = Customer_requirement::where('is_activated','1')->Orwhere('is_activated','2')->count();
        $expire_quote = Customer_requirement::where('is_activated','0')->where('expire_date','<',Carbon::now()->toDateString())->count();
        $active_inventory = Amazon_inventory::join('customer_requirement_details','customer_requirement_details.product_id','=','amazon_inventories.id')
            ->join('customer_requirements','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('orders','customer_requirements.order_id','=','orders.order_id')
            ->whereBetween('orders.is_activated',array('0','11'))
            ->groupby('customer_requirement_details.product_id')
            ->count();

        $order = Order::select('order_id', 'created_at')
                ->whereYear('created_at','=',Carbon::now()->format('Y'))
                ->get()
                ->groupBy(function($date) {
                    return Carbon::parse($date->created_at)->format('m'); // grouping by months
                });

        $usermcount = [];
        $order_year_count = [];

        foreach ($order as $key => $value) {
            $usermcount[(int)$key] = count($value);
        }

        for($i = 1; $i <= 12; $i++){
            if(!empty($usermcount[$i])){
                $order_year_count[$i] = $usermcount[$i];    
            }else{
                $order_year_count[$i] = 0;    
            }
        }

        $order_year_count = json_encode($order_year_count);

        $payment = Payment_info::select('amount', 'created_at')
                ->whereYear('created_at','=',Carbon::now()->format('Y'))
                ->get()
                ->groupBy(function($date) {
                    return Carbon::parse($date->created_at)->format('m'); // grouping by months
                });

        $paymentcount = [];
        $payment_chart = [];
        $sum = 0;

        foreach ($payment as $key => $value) {
            $sum = 0;
            foreach ($value as $val) {
                $sum += $val->amount;
            }
            $paymentcount[(int)$key] = $sum;
        }

        for($i = 1; $i <= 12; $i++){
            if(!empty($paymentcount[$i])){
                $payment_chart[$i] = $paymentcount[$i];    
            }else{
                $payment_chart[$i] = 0;    
            }
        }

        $payment_chart = json_encode($payment_chart);
        
        return view('admin.dashboard')->with(compact('users','customers','active_order','active_quote','past_quote','expire_quote','active_inventory','order_year_count','payment_chart'));
    }

    public function getPaymentChart(Request $request){

       $payment = Payment_info::select('amount', 'created_at')
                ->whereYear('created_at','=',$request->year)
                ->get()
                ->groupBy(function($date) {
                    return Carbon::parse($date->created_at)->format('m'); // grouping by months
                });

        $paymentcount = [];
        $payment_chart = [];
        $sum = 0;

        foreach ($payment as $key => $value) {
            $sum = 0;
            foreach ($value as $val) {
                $sum += $val->amount;
            }
            $paymentcount[(int)$key] = $sum;
        }

        for($i = 1; $i <= 12; $i++){
            if(!empty($paymentcount[$i])){
                $payment_chart[$i] = $paymentcount[$i];    
            }else{
                $payment_chart[$i] = 0;    
            }
        }

        return json_encode($payment_chart);
    }

}
