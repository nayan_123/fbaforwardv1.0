<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Order;
use App\CustomerClearance;

class CustomerClearanceController extends Controller{
    public function index(){
    	$data['orders'] = $data['order'] = Order::selectRaw('orders.order_id,orders.created_at,orders.order_no,amazon_inventories.product_name,amazon_inventories.product_nick_name, user_infos.company_name')
                ->join('customer_requirements', 'customer_requirements.order_id', '=', 'orders.order_id')
                ->join('customer_requirement_details', 'customer_requirements.id', '=', 'customer_requirement_details.customer_requirement_id')
                ->join('amazon_inventories', 'amazon_inventories.id', '=', 'customer_requirement_details.product_id')
                ->join('customer_requirement_methods', 'customer_requirement_methods.customer_requirement_id', '=', 'customer_requirements.id')
                ->leftjoin('user_infos', 'user_infos.user_id', '=', 'customer_requirements.user_id')
                ->whereIn('customer_requirements.order_type',array('1','3'))
                ->where('orders.is_activated', '10')
                ->where('customer_requirement_methods.is_activated','1')
                ->OrwhereIn('orders.is_activated',array('8','9'))
                ->where('customer_requirement_methods.shipping_method_id','<>','1')
                ->where('customer_requirement_methods.is_activated','1')
                ->get();
    	return view('custom_clearance.index',$data);
    }
    public function create(Request $request){
    	$data['order_id'] = $request->id;
        return view('custom_clearance.create',$data);
    }
    public function store(Request $request){
        $data = [
            'custom_duties' => $request->custom_duties,
            'order_id' => $request->order_id
        ];
        if($request->file_3461){
            $newname = $request->order_id."_file_3461".'.'.$request->file_3461->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/customer_clearance';
            $request->file('file_3461')->move($destinationPath, $newname);
            $data = array_merge($data, ['file_3461' => $newname]);
        }
        if($request->file_7501){
            $newname = $request->order_id."_file_7501".'.'.$request->file_7501->getClientOriginalExtension();
            $destinationPath = public_path() . '/uploads/customer_clearance';
            $request->file('file_7501')->move($destinationPath, $newname);
            $data = array_merge($data, ['file_7501' => $newname]);
        }
        foreach ($request->additional_service as $value) {
            $data = array_merge($data, [$value => '1']);
        }
        CustomerClearance::create($data);
        Order::where('order_id',$request->order_id)->update(array('is_activated'=>'11'));
        return redirect('customs-clearance')->with('success', 'Customs clearance added successfully.');
    }
}