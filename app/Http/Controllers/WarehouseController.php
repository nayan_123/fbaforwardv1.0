<?php

namespace App\Http\Controllers;
use App\Amazon_inventory;
use App\Amazon_marketplace;
use App\Amazon_destination;
use App\Customer_quickbook_detail;
use App\Customer_requirement_detail;
use App\Damage;
use App\Dev_account;
use App\Order_inventory_history;
use App\Order_package;
use App\Order_warehouse_hold;
use App\Prep_requirement;
use App\Prep_service;
use App\Order;
use App\Question;
use App\User;
use App\User_info;
use App\Warehouse_checkin;
use App\Warehouse_checkin_image;
use App\Warehouse_checkin_label;
use App\Warehouse_hold;
use App\Warehouse_hold_type;
use App\Warehouse_lineitem;
use Illuminate\Http\Request;
use App\Customer_requirement;
use App\Libraries;
use PDF;
use DNS1D;
class WarehouseController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    public function getorder(Request $request){
        $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated as method_activated,customer_requirement_methods.shipping_method_id, user_infos.company_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('user_infos','user_infos.user_id','=','orders.user_id')
            ->where('customer_requirement_methods.is_activated','1');
        if($request->tab_id == 1) {
            $order->where('orders.is_activated', '>=', '15');
        }
        else if($request->tab_id == 2){
            $order->where('orders.is_activated', '>=', '15');
            $order->where('orders.hold','0');
        }
        else if($request->tab_id == 3){
            $order->where('orders.is_activated', '>=', '15');
            $order->whereIn('orders.hold',array('1','2'));
        }
        return $order = $order->get()->toArray();
    }
    public function warehousecheckin(){
        $user=\Auth::user();
        $user_role=$user->role_id;
        $title="Check In";
        $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated as method_activated,customer_requirement_methods.shipping_method_id, user_infos.company_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('user_infos','user_infos.user_id','=','orders.user_id')
            ->where('customer_requirement_methods.is_activated','1')
            ->where('orders.is_activated','=','15')
            ->orWhere('orders.is_activated','0')
            ->where('customer_requirements.order_type','2')
            ->where('customer_requirement_methods.is_activated','1')
            ->get()->toArray();
       $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('warehouse.order')->with(compact('title','order','orderStatus','user_role'));
    }
    public function warehousecheckinform(Request $request){
        $title = "Receive Shipment";
        $order_id = $request->order_id;
        $user = User_info::selectRaw('user_infos.company_name, user_infos.contact_email, amazon_inventories.product_name, orders.order_no, customer_requirement_details.product_id, customer_requirement_details.total, customer_requirement_details.no_boxs')
            ->join('orders', 'orders.user_id', '=', 'user_infos.user_id')
            ->join('customer_requirements', 'customer_requirements.order_id', '=', 'orders.order_id')
            ->join('customer_requirement_details', 'customer_requirement_details.customer_requirement_id', '=', 'customer_requirements.id')
            ->join('amazon_inventories', 'amazon_inventories.id', '=', 'customer_requirement_details.product_id')
            ->where('orders.order_id', $order_id)
            ->first();
        $warehouse_checkin = Warehouse_checkin::where('order_id',$order_id)->where('status','2')->first();
        isset($warehouse_checkin) ? $warehouse_lineitems = Warehouse_lineitem::where('warehouse_checkin_id', $warehouse_checkin->id)->get() : $warehouse_lineitems = array();
        $warehouse_images = Warehouse_checkin_image::selectRaw('warehouse_checkin_images.*')
            ->join('warehouse_checkins', 'warehouse_checkins.id', '=', 'warehouse_checkin_images.warehouse_checkin_id')
            ->where('warehouse_checkin_images.status', '0')
            ->where('warehouse_checkins.order_id', $order_id)
            ->get();
        $warehouse_labels = Warehouse_checkin_label::selectRaw('warehouse_checkin_labels.*')
            ->join('warehouse_checkins','warehouse_checkins.id','=','warehouse_checkin_labels.warehouse_checkin_id')
            ->where("order_id",$order_id)
            ->get();
        return view('warehouse.warehouse_checkin')->with(compact('order_id', 'user', 'title','warehouse_checkin','warehouse_lineitems','warehouse_images','warehouse_labels'));
    }
    public function addwarehousecheckinform(Request $request){
        $warehouse_checkin = array('order_id' => $request->input('order_id'),
            'label' => $request->input('label'),
            'deliver_by' => $request->input('delivered_by'),
            'air_way' => $request->input('air_waybill'),
            'total_cartons' => $request->input('carton_total'),
            'total_units' => $request->input('unit_total'),
            'total_volume' => $request->input('volume_total'),
            'shipment_condition' => $request->input('shipment_condition'),
            'condition_notes' => $request->input('condition_note'),
            'notified' => $request->input('notified'),
            'notified_note'=>$request->input('notified_note')
        );
        $warehouse_checkin_detail = Warehouse_checkin::create($warehouse_checkin);
        $label_count = $request->input('label_count');
        for ($label_cnt=1; $label_cnt<=$label_count;$label_cnt++) {
            if($request->input('label_number' . $label_cnt)!='') {
                $warehouse_checkin_label = array('warehouse_checkin_id' => $warehouse_checkin_detail->id,
                    'label_number' => $request->input('label_number' . $label_cnt)
                );
                Warehouse_checkin_label::create($warehouse_checkin_label);
            }
        }
        if ($request->hasFile('photos')) {
            $destinationPath = public_path() . '/uploads/warehouse';
            $images = $request->file('photos');
            foreach ($images as $image) {
                $file = $warehouse_checkin_detail->id.'_'.$request->input('order_id') . '_' . 'warehouse' . '.' . $image->getClientOriginalExtension();
                $image->move($destinationPath, $file);
                $warehouse_checkin_image = array('warehouse_checkin_id' => $warehouse_checkin_detail->id,
                    'images' => $file
                );
                Warehouse_checkin_image::create($warehouse_checkin_image);
            }
        }
        $cnt = $request->input('cnt');
        for ($new_cnt = 1; $new_cnt <= $cnt; $new_cnt++) {
            if($request->input('length' . $new_cnt)!='') {
                $warehouse_lineitems = array('warehouse_checkin_id' => $warehouse_checkin_detail->id,
                    'length' => $request->input('length' . $new_cnt),
                    'width' => $request->input('width' . $new_cnt),
                    'height' => $request->input('height' . $new_cnt),
                    'weight' => $request->input('weight' . $new_cnt),
                    'unit_per_carton' => $request->input('units_per_carton' . $new_cnt),
                    'unit_subtotal' => $request->input('unit_subtotal' . $new_cnt),
                    'cartons_subtotal' => $request->input('cartons_subtotal' . $new_cnt),
                    'volume_subtotal' => $request->input('volume' . $new_cnt),
                );
                Warehouse_lineitem::create($warehouse_lineitems);
            }
        }
        $order = array('is_activated' => '16');
        Order::where('order_id', $request->input('order_id'))->update($order);
        Order_inventory_history::create(array('order_id' => $request->input('order_id'), 'product_id' => $request->input('product_id'), 'total' => $request->input('total'), 'status' => '1'));
        $inventory = Amazon_inventory::where('id', $request->input('product_id'))->first();
        $inboundfba = $inventory->infbaforward + $request->input('total');
        Amazon_inventory::where('id', $request->input('product_id'))->update(array('infbaforward' => $inboundfba));

        $user_detail = User::selectRaw('users.*')
            ->join('orders', 'orders.user_id', '=', 'users.id')
            ->where('orders.order_id', $request->input('order_id'))
            ->get();
        if (count($user_detail) > 0)
            $user = User::find($user_detail[0]->id);
        else
            $user = '';
        $user->newNotification()
            ->withType('Warehouse check in')
            ->withSubject('You order checkin in FBA warehouse')
            ->withBody('You order checkin  in FBA warehouse')
            ->regarding($warehouse_checkin_detail)
            ->deliver();
        return redirect('warehouse/checkin')->with('success', 'Warehouse Checkin Form Submitted Successfully');
    }
    public function warehousereviewcheckin(){
        $user=\Auth::user();
        $user_role=$user->role_id;
        $title="Review Check In";
        $order = Order::selectRaw('orders.*, user_infos.company_name, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated as method_activated,customer_requirement_methods.shipping_method_id')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('user_infos','user_infos.user_id','=','orders.user_id')
            ->where('customer_requirement_methods.is_activated','1')
            ->where('orders.is_activated','=','16')
            ->get()->toArray();
       $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('warehouse.order')->with(compact('title','order','orderStatus','user_role'));
    }
    public function warehousecheckinreview(Request $request){
        $title = "Review Check In";
        $order_id = $request->order_id;
        $order_detail = Order::selectRaw('orders.order_no, user_infos.company_name, user_infos.contact_email, warehouse_checkins.*, warehouse_checkins.id as warehouse_checkin_id, customer_requirement_details.*, amazon_inventories.product_name, shipping_methods.shipping_name, customer_requirements.shipment_plan,customer_requirements.prep_note, orders.hold')
            ->leftjoin('warehouse_checkins', 'warehouse_checkins.order_id', '=', 'orders.order_id')
            ->join('customer_requirements', 'customer_requirements.order_id', '=', 'orders.order_id')
            ->join('customer_requirement_details', 'customer_requirement_details.customer_requirement_id', '=', 'customer_requirements.id')
            ->join('amazon_inventories', 'amazon_inventories.id', '=', 'customer_requirement_details.product_id')
            ->join('customer_requirement_methods', 'customer_requirement_methods.customer_requirement_id', '=', 'customer_requirements.id')
            ->join('shipping_methods', 'shipping_methods.shipping_method_id', '=', 'customer_requirement_methods.shipping_method_id')
            ->join('user_infos', 'user_infos.user_id', '=', 'orders.user_id')
            ->where('customer_requirement_methods.is_activated', '1')
            ->where('orders.order_id', $order_id)
            ->where('warehouse_checkins.status','0')
            ->first();
        $warehouse_lineitems = Warehouse_lineitem::where('warehouse_checkin_id', $order_detail->warehouse_checkin_id)->get();
        $prep_require = Prep_requirement::selectRaw('prep_requirements.prep_id')->join('customer_requirements', 'customer_requirements.id', '=', 'prep_requirements.customer_requirement_id')
            ->join('prep_services', 'prep_services.prep_service_id', '=', 'prep_requirements.prep_id')
            ->where('customer_requirements.order_id', $order_id)
            ->get()->toArray();
        $prep_require = array_map('current', $prep_require);
        $prep_service = Prep_service::get();
        $warehouse_images = Warehouse_checkin_image::selectRaw('warehouse_checkin_images.*')
            ->join('warehouse_checkins', 'warehouse_checkins.id', '=', 'warehouse_checkin_images.warehouse_checkin_id')
            ->where('warehouse_checkin_images.status', '0')
            ->where('warehouse_checkins.order_id', $order_id)
            ->where('warehouse_checkins.status','0')
            ->get();
        $warehouse_labels = Warehouse_checkin_label::selectRaw('warehouse_checkin_labels.*')
                                        ->join('warehouse_checkins','warehouse_checkins.id','=','warehouse_checkin_labels.warehouse_checkin_id')
                                        ->where("order_id",$order_id)
                                        ->get();
        $amazonqty = Order_inventory_history::selectRaw('total')
            ->where('status', '2')
            ->where('order_id', $order_id)
            ->orderBy('id', 'desc')
            ->sum('total');
        $shippedqty = Order_inventory_history::select('total')
            ->where('status', '3')
            ->where('order_id', $order_id)
            ->groupby('order_id')
            ->sum('total');
        $warehouse_hold = Warehouse_hold::all();
        $warehouse_hold_type = Warehouse_hold_type::all();
        return view('warehouse/reviewarehousecheckin')->with(compact('warehouse_hold','warehouse_hold_type','order_detail', 'order_id', 'warehouse_images', 'title', 'prep_require', 'prep_service', 'amazonqty', 'shippedqty', 'warehouse_lineitems','warehouse_labels'));
    }
    public function getdetails(Request $request){
        if(isset($request->type_id)) {
            $type_id = $request->type_id;
            if ($type_id == 1) {
                $getprep = Prep_service::all();
                return $getprep;
            } elseif ($type_id == 2) {
                $getdamage = Damage::all();
                return $getdamage;
            } elseif ($type_id == 3) {
                $getquestion = Question::all();
                return $getquestion;
            }
        }
        elseif(isset($request->id)){
            $data['detail'] = Order_warehouse_hold::where('id',$request->id)->first()->toArray();
            if($data['detail']['hold_type_id']=='1'){
                 $data['type_details'] = Prep_service::all()->toArray();
            }
            elseif($data['detail']['hold_type_id']=='2'){
                 $data['type_details'] = Damage::all()->toArray();
            }
            elseif($data['detail']['hold_type_id']=='3'){
                 $data['type_details'] = Question::all()->toArray();
            }
            $data['warehouse_hold_type'] = Warehouse_hold_type::all();
           return $data;
        }
    }
    public function getcostmesure(Request $request){
        $type_id=$request->type_id;
        $detail_id = $request->detail_id;
        if($type_id==1) {
            $data = Prep_service::selectRaw('prep_services.price, measurements.id, measurements.name')
                                        ->join('measurements','measurements.id','=','prep_services.mesurement_id')
                                        ->where('prep_services.prep_service_id',$detail_id)
                                        ->first();
        }
        elseif ($type_id==2){
            $data = Damage::selectRaw('damages.price,measurements.id, measurements.name')
                ->join('measurements','measurements.id','=','damages.mesurement_id')
                ->where('damages.id',$detail_id)
                ->first();
        }
        elseif ($type_id==3){
            $data = Question::selectRaw('questions.price,measurements.id, measurements.name')
                ->join('measurements','measurements.id','=','questions.mesurement_id')
                ->where('questions.id',$detail_id)
                ->first();
        }
        return $data;
    }
    public function downloadwarehouseimages(Request $request){
        $id = $request->id;
        $image = Warehouse_checkin_image::where('id', $id)->get();
        $images = isset($image[0]->images) ? $image[0]->images : '';
        $file = public_path() . "/uploads/warehouse/" . $images;
        $headers = array('Content-Type: application/pdf',
        );
        return response()->download($file, $images, $headers);
    }
    public function checkinrevision(Request $request){
        $post = $request->all();
        $order_id= $post['order_id'];
        $revision_note = $post['revision_note'];
        $order = Customer_requirement::selectRaw('customer_requirements.order_type,customer_requirement_details.total, customer_requirement_details.product_id')
                        ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
                        ->where('customer_requirements.order_id',$order_id)
                        ->first();
        if($order->order_type=='2')
            $data = array('is_activated'=>'0');
        else
            $data = array('is_activated'=>'15');
        Order::where('order_id',$order_id)->update($data);
        $revision_data = array('revision_note'=>$revision_note,'status'=>'2');
        Warehouse_checkin::where('order_id',$order_id)->update($revision_data);
        $inventory = Amazon_inventory::where('id',$order->product_id)->first();
        $inboundfba = $inventory->infbaforward - $order->total;
        Amazon_inventory::where('id', $request->input('product_id'))->update(array('infbaforward' => $inboundfba));
        return redirect('warehouse/reviewcheckin')->with('sucess','Order Submitted for Revision');
    }
    public function approvereview(Request $request){
        if(isset($request->new)) {
            if ($request->status == 1){
                if($request->input('type')=='4') {
                    $data = array('order_id' => $request->order,
                        'hold_type_id' => $request->input('type'),
                        'detail_text' => $request->input('details'),
                        'cost' => $request->input('cost'),
                        'measure' => $request->input('measurement'),
                        'no_of_unit' => $request->input('number_unit'),
                        'notes' => $request->input('notes')
                    );
                }
                else {
                    $data = array('order_id' => $request->order,
                        'hold_type_id' => $request->input('type'),
                        'detail_id' => $request->input('details'),
                        'cost' => $request->input('cost'),
                        'measure' => $request->input('measurement'),
                        'no_of_unit' => $request->input('number_unit'),
                        'notes' => $request->input('notes')
                    );
                }
                Order_warehouse_hold::create($data);
            }
            elseif ($request->status == 2) {
                $data = array('order_id' => $request->order,
                    'hold_type_text' => $request->input('type'),
                    'detail_text' => $request->input('details'),
                    'notes' => $request->input('notes')
                );
                Order_warehouse_hold::create($data);
            }
            return back();
        }
        else{
               $post = $request->all();
               $detail = Customer_requirement_detail::where('customer_requirement_id',$post['customer_requirement_id'])->first();
               $inventory = Amazon_inventory::where('id', $detail->product_id)->first();
               $inboundfba = $inventory->infbaforward - ($detail->total);
               //$storagefba = $inventory->atfbaforward + $post['storage_unit'];
               $processfba = $inventory->processfbaforward + $detail->total;
               Amazon_inventory::where('id', $detail->product_id)->update(array('infbaforward' => $inboundfba,'processfbaforward'=>$processfba));

            if ($request->status == 0) {
                $count = 0;
                $data = array('warehouse_due_date' => date('Y-m-d',strtotime($request->input('due_date'))), 'is_activated' => '17');
                Order::where('order_id', $request->order)->update($data);
            } elseif ($request->status == 1) {
                $count = $request->count2;
                for ($cnt = 1; $cnt <= $count; $cnt++) {
                    if ($request->input('type' . ($request->status + 1) . '_' . $cnt) != '') {
                        if ($request->input('type' . ($request->status + 1) . '_' . $cnt) == '4') {
                            $data = array('order_id' => $request->order,
                                'hold_type_id' => $request->input('type' . ($request->status + 1) . '_' . $cnt),
                                'detail_text' => $request->input('details' . $cnt),
                                'cost' => $request->input('cost' . $cnt),
                                'measure' => $request->input('measurement' . $cnt),
                                'no_of_unit' => $request->input('number_unit' . $cnt),
                                'notes' => $request->input('notes' . ($request->status + 1) . '_' . $cnt)
                            );
                        } else {
                            $data = array('order_id' => $request->order,
                                'hold_type_id' => $request->input('type' . ($request->status + 1) . '_' . $cnt),
                                'detail_id' => $request->input('details' . $cnt),
                                'cost' => $request->input('cost' . $cnt),
                                'measure' => $request->input('measurement' . $cnt),
                                'no_of_unit' => $request->input('number_unit' . $cnt),
                                'notes' => $request->input('notes' . ($request->status + 1) . '_' . $cnt)
                            );
                        }
                        Order_warehouse_hold::create($data);
                    }
                }
                Order::where('order_id', $request->order)->update(array('hold' => $request->status, 'is_activated' => '17'));
            } elseif ($request->status == 2) {
                $count = $request->count3;
                for ($cnt = 1; $cnt <= $count; $cnt++) {
                    $data = array('order_id' => $request->order,
                        'hold_type_text' => $request->input('type' . ($request->status + 1) . '_' . $cnt),
                        'detail_text' => $request->input('details' . $cnt),
                        'notes' => $request->input('notes' . ($request->status + 1) . '_' . $cnt)
                    );
                    Order_warehouse_hold::create($data);
                }
                Order::where('order_id', $request->order)->update(array('hold' => $request->status, 'is_activated' => '17'));
            }
            $this->createshipments($request->order,$detail->total);
            return redirect('warehouse/reviewcheckin');
        }
    }
    // create shipment plan and shipments
    public function createshipments($order_id,$shipqty){
        $order = Order::selectRaw('user_infos.*, orders.order_id,customer_requirements.*, customer_requirement_details.*, amazon_inventories.sellerSKU')
            ->join('customer_requirements', 'customer_requirements.order_id', '=', 'orders.order_id')
            ->join('customer_requirement_details', 'customer_requirement_details.customer_requirement_id', '=', 'customer_requirements.id')
            ->join('amazon_inventories', 'amazon_inventories.id', '=', 'customer_requirement_details.product_id')
            ->join('user_infos', 'user_infos.user_id', '=', 'orders.user_id')
            ->where('orders.order_id', $order_id)
            ->first();
        $results = Amazon_marketplace::selectRaw("customer_amazon_details.mws_seller_id, customer_amazon_details.user_id, customer_amazon_details.mws_authtoken, amazon_marketplaces.market_place_id")
            ->join('customer_amazon_details', 'customer_amazon_details.mws_market_place_id', '=', 'amazon_marketplaces.id')
            ->where('customer_amazon_details.user_id', $order->user_id)
            ->get();
        $UserCredentials['mws_authtoken'] = !empty($results[0]->mws_authtoken) ? decrypt($results[0]->mws_authtoken) : '';
        $UserCredentials['mws_seller_id'] = !empty($results[0]->mws_seller_id) ? decrypt($results[0]->mws_seller_id) : '';
        $UserCredentials['marketplace'] = $results[0]->market_place_id ? $results[0]->market_place_id : '';
        $fromaddress = new \FBAInboundServiceMWS_Model_Address();
        $fromaddress->setName($order->company_name);
        $fromaddress->setAddressLine1($order->company_address);
        $fromaddress->setCountryCode($order->business_country);
        $fromaddress->setStateOrProvinceCode($order->business_state);
        $fromaddress->setCity($order->company_city);
        $fromaddress->setPostalCode($order->company_zipcode);
        $service = $this->getReportsClient();
        $ship_request = new \FBAInboundServiceMWS_Model_CreateInboundShipmentPlanRequest();
        $ship_request->setSellerId($UserCredentials['mws_seller_id']);
        $ship_request->setMWSAuthToken($UserCredentials['mws_authtoken']);
        $ship_request->setShipFromAddress($fromaddress);
            $item = array();
                if($order->total > 0) {
                    $data = array('SellerSKU' => $order->sellerSKU, 'Quantity' => $shipqty);
                    $item[] = new \FBAInboundServiceMWS_Model_InboundShipmentPlanItem($data);
                }
            if(!empty($item)) {
                $itemlist = new \FBAInboundServiceMWS_Model_InboundShipmentPlanRequestItemList();
                $itemlist->setmember($item);
                $ship_request->setInboundShipmentPlanRequestItems($itemlist);
                $arr_response = $this->invokeCreateInboundShipmentPlan($service, $ship_request);
                //create shipments api of particular shipmentplan
                $shipment_service = $this->getReportsClient();
                $shipment_request = new \FBAInboundServiceMWS_Model_CreateInboundShipmentRequest();
                $shipment_request->setSellerId($UserCredentials['mws_seller_id']);
                $shipment_request->setMWSAuthToken($UserCredentials['mws_authtoken']);
                $shipment_header = new \FBAInboundServiceMWS_Model_InboundShipmentHeader();
                $shipment_header->setShipmentName("SHIPMENT_NAME");
                $shipment_header->setShipFromAddress($fromaddress);
                //response of shipment plan and insert data in amazon destination
                foreach ($arr_response as $new_response) {
                    foreach ($new_response->InboundShipmentPlans as $planresult) {
                        foreach ($planresult->member as $member) {
                            $api_shipment_id = $member->ShipmentId;
                            $destination_name = $member->DestinationFulfillmentCenterId;
                            foreach ($member->ShipToAddress as $address) {
                                $address_name = $address->Name;
                                $addressline1 = $address->AddressLine1;
                                $city = $address->City;
                                $state = $address->StateOrProvinceCode;
                                $country = $address->CountryCode;
                                $postal = $address->PostalCode;
                            }
                            $preptype = $member->LabelPrepType;
                            foreach ($member->EstimatedBoxContentsFee as $fee) {
                                $total_unit = $fee->TotalUnits;
                                foreach ($fee->FeePerUnit as $unit) {
                                    $unit_currency = $unit->CurrencyCode;
                                    $unit_value = $unit->Value;
                                }
                                foreach ($fee->TotalFee as $total) {
                                    $total_currency = $total->CurrencyCode;
                                    $total_value = $total->Value;
                                }
                            }
                            $shipment_header->setDestinationFulfillmentCenterId($destination_name);
                            $shipment_request->setInboundShipmentHeader($shipment_header);
                            $shipment_request->setShipmentId($api_shipment_id);
                            $shipment_item = array();
                            foreach ($member->Items as $item) {
                                foreach ($item->member as $sub_member) {
                                    $amazon_destination = array('destination_name' => $destination_name,
                                        'order_id' => $order_id,
                                        'api_shipment_id' => $api_shipment_id,
                                        'sellerSKU' => $sub_member->SellerSKU,
                                        'fulfillment_network_SKU' => $sub_member->FulfillmentNetworkSKU,
                                        'qty' => $sub_member->Quantity,
                                        'ship_to_address_name' => $address_name,
                                        'ship_to_address_line1' => $addressline1,
                                        'ship_to_city' => $city,
                                        'ship_to_state_code' => $state,
                                        'ship_to_country_code' => $country,
                                        'ship_to_postal_code' => $postal,
                                        'label_prep_type' => $preptype,
                                        'total_units' => $total_unit,
                                        'fee_per_unit_currency_code' => $unit_currency,
                                        'fee_per_unit_value' => $unit_value,
                                        'total_fee_value' => $total_value
                                    );
                                    Amazon_destination::create($amazon_destination);
                                    $item_array = array('SellerSKU' => $sub_member->SellerSKU, 'QuantityShipped' => $sub_member->Quantity, 'FulfillmentNetworkSKU' => $sub_member->FulfillmentNetworkSKU);
                                    $shipment_item[] = new \FBAInboundServiceMWS_Model_InboundShipmentItem($item_array);
                                }
                            }
                            $api_shipment_detail = new \FBAInboundServiceMWS_Model_InboundShipmentItemList();
                            $api_shipment_detail->setmember($shipment_item);
                            $shipment_request->setInboundShipmentItems($api_shipment_detail);
                            $this->invokeCreateInboundShipment($shipment_service, $shipment_request);
                        }
                    }
                }
            }
            $shipment_ids = Amazon_destination::selectRaw('amazon_destinations.api_shipment_id, warehouse_checkins.total_cartons')
            ->join('orders', 'orders.order_id', '=', 'amazon_destinations.order_id')
            ->join('warehouse_checkins', 'warehouse_checkins.order_id', '=', 'orders.order_id')
            ->where('orders.order_id', $order_id)
            ->groupby('amazon_destinations.api_shipment_id')
            ->get();
        $product_ids = Amazon_destination::selectRaw('amazon_destinations.*, warehouse_checkins.total_cartons')
            ->join('orders', 'orders.order_id', '=', 'amazon_destinations.order_id')
            ->join('warehouse_checkins', 'warehouse_checkins.order_id', '=', 'orders.order_id')
            ->where('orders.order_id', $order_id)
            ->distinct('amazon_destinations.api_shipment_id')
            ->get();
        $cartoon_id = 1;
        $devAccount = Dev_account::first();
        $access_key = $devAccount->access_key;
        foreach ($shipment_ids as $new_shipment_ids) {
            $feed = '<?xml version="1.0" encoding="UTF-8"?>' .
                '<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">' .
                '<Header>' .
                '<DocumentVersion>1.01</DocumentVersion>' .
                '<MerchantIdentifier>' . $UserCredentials["mws_seller_id"] . '</MerchantIdentifier>' .
                '</Header>' .
                '<MessageType>CartonContentsRequest</MessageType>' .
                '<Message>' .
                '<MessageID>1</MessageID>' .
                '<CartonContentsRequest>' .
                '<ShipmentId>' . $new_shipment_ids->api_shipment_id . '</ShipmentId>' .
                '<NumCartons>' . $new_shipment_ids->no_of_cartoon . '</NumCartons>' .
                '<Carton>' .
                '<CartonId>' . $cartoon_id . '</CartonId>';
            foreach ($product_ids as $new_product_ids) {
                if ($new_shipment_ids->api_shipment_id == $new_product_ids->api_shipment_id) {
                    $feed .= '<Item>' .
                        '<SKU>' . $new_product_ids->sellerSKU . '</SKU>' .
                        '<QuantityShipped>' . $new_product_ids->qty . '</QuantityShipped>' .
                        '<QuantityInCase>' . $new_product_ids->qty . '</QuantityInCase>' .
                        '</Item>';
                }
            }
            $feed .= '</Carton>' .
                '</CartonContentsRequest>' .
                '</Message>' .
                '</AmazonEnvelope>';
            $param = array();
            $param['AWSAccessKeyId'] = $access_key;
            $param['MarketplaceId.Id.1'] = $UserCredentials['marketplace'];
            $param['MWSAuthToken'] = $UserCredentials['mws_authtoken']; //MWS Auth Token for this store
            $param['Merchant'] = $UserCredentials['mws_seller_id'];
            $param['Action'] = 'SubmitFeed';
            $param['FeedType'] = '_POST_FBA_INBOUND_CARTON_CONTENTS_';
            $param['SignatureMethod'] = 'HmacSHA256';
            $param['SignatureVersion'] = '2';
            $param['Timestamp'] = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
            $param['Version'] = '2009-01-01';
            $param['PurgeAndReplace'] = 'false';
            $strUrl = $this->arrToQueryString($param);
            $result = $this->sendQuery($strUrl, $feed, $param);
            $arr = simplexml_load_string($result);
            $feed_id=0;
            foreach ($arr as $new_arr) {
                foreach ($new_arr->FeedSubmissionInfo as $feedsubmit) {
                    $feed_id = $feedsubmit->FeedSubmissionId;
                }
            }
            $feed_list = '<?xml version="1.0" encoding="UTF-8"?>' .
                '<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">' .
                '<Header>' .
                '<DocumentVersion>1.02</DocumentVersion>' .
                '<MerchantIdentifier>' . $UserCredentials["mws_seller_id"] . '</MerchantIdentifier>' .
                '</Header>' .
                '<MessageType>ProcessingReport</MessageType>' .
                '<Message>' .
                '<MessageID>1</MessageID>' .
                '<ProcessingReport>' .
                '<DocumentTransactionID>' . $feed_id . '</DocumentTransactionID>' .
                '</ProcessingReport>' .
                '</Message>' .
                '</AmazonEnvelope>';
            $param = array();
            $param['AWSAccessKeyId'] = $access_key;
            $param['MarketplaceId.Id.1'] = $UserCredentials['marketplace'];
            $param['MWSAuthToken'] = $UserCredentials['mws_authtoken']; //MWS Auth Token for this store
            $param['Merchant'] = $UserCredentials['mws_seller_id'];
            $param['Action'] = 'GetFeedSubmissionResult';
            $param['SignatureMethod'] = 'HmacSHA256';
            $param['SignatureVersion'] = '2';
            $param['Timestamp'] = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
            $param['Version'] = '2009-01-01';
            $param['PurgeAndReplace'] = 'false';
            $param['FeedSubmissionId'] = $feed_id;
            $strUrl = $this->arrToQueryString($param);
            $feed_result = $this->sendQuery($strUrl, $feed_list, $param);
            $data = array('cartoon_id' => $cartoon_id, 'feed_submition_id' => $feed_id);
            Amazon_destination::where('api_shipment_id', $new_shipment_ids->api_shipment_id)->update($data);
            $cartoon_id++;
        }
        Customer_requirement::where('order_id',$order_id)->update(array('shipment_plan'=>'1'));
    }
    protected function getReportsClient(){
        list($access_key, $secret_key, $config) = $this->getKeys();
        return new \FBAInboundServiceMWS_Client(
            $access_key,
            $secret_key,
            env('APPLICATION_NAME'),
            env('APPLICATION_VERSION'),
            $config
        );
    }
    private function getKeys(){
        add_to_path('Libraries');
        $devAccount = Dev_account::first();
        return [
            $devAccount->access_key,
            $devAccount->secret_key,
            self::getMWSConfig()
        ];
    }
    public static function getMWSConfig(){
        return [
            'ServiceURL' => "https://mws.amazonservices.com/FulfillmentInboundShipment/2010-10-01",
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 3,
        ];
    }
    function invokeCreateInboundShipmentPlan(\FBAInboundServiceMWS_Interface $service, $request){
        try {
            $response = $service->CreateInboundShipmentPlan($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            return $arr_response = new \SimpleXMLElement($dom->saveXML());
        } catch (\FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }
    function invokeCreateInboundShipment(\FBAInboundServiceMWS_Interface $service, $request){
        try {
            $response = $service->CreateInboundShipment($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            return 1;
        } catch (\FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }
    function invokeUpdateInboundShipment(\FBAInboundServiceMWS_Interface $service, $request){
        try {
            $response = $service->UpdateInboundShipment($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            return $update_response = new \SimpleXMLElement($dom->saveXML());
        } catch (\FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }
    function invokeListInboundShipments(\FBAInboundServiceMWS_Interface $service, $request){
        try {
            $response = $service->ListInboundShipments($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            return $list_response = new \SimpleXMLElement($dom->saveXML());
        } catch (\FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }
    public function arrToQueryString($param){
        $strURL = "";
        $url = array();
        foreach ($param as $key => $val) {
            $key = str_replace("%7E", "~", rawurlencode($key));
            $val = str_replace("%7E", "~", rawurlencode($val));
            $url[] = "{$key}={$val}";
        }
        sort($url);
        $strURL = implode('&', $url);
        return $strURL;
    }
    public function sendQuery($strUrl, $amazon_feed, $param){
        $devAccount = Dev_account::first();
        $secret_key = $devAccount->secret_key;
        $strServieURL = preg_replace('#^https?://#', '', 'https://mws.amazonservices.com');
        $strServieURL = str_ireplace("/", "", $strServieURL);
        $sign = 'POST' . "\n";
        $sign .= $strServieURL . "\n";
        $sign .= '/Feeds/' . $param['Version'] . '' . "\n";
        $sign .= $strUrl;
        $signature = hash_hmac("sha256", $sign, $secret_key, true);
        $signature = urlencode(base64_encode($signature));
        $httpHeader = array();
        $httpHeader[] = 'Transfer-Encoding: chunked';
        $httpHeader[] = 'Content-Type: application/xml';
        $httpHeader[] = 'Content-MD5: ' . base64_encode(md5($amazon_feed, true));
        $httpHeader[] = 'Expect:';
        $httpHeader[] = 'Accept:';
        $link = "https://mws.amazonservices.com/Feeds/" . $param['Version'] . "?";
        $link .= $strUrl . "&Signature=" . $signature;
        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $amazon_feed);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return $response;
    }
    public function warehouseprep(){
        $user=\Auth::user();
        $user_role=$user->role_id;
        $title="Prep";
        $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated as method_activated,customer_requirement_methods.shipping_method_id, user_infos.company_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('user_infos','user_infos.user_id','=','orders.user_id')
            ->where('customer_requirement_methods.is_activated','1')
            ->where('orders.is_activated','=','17')
            ->where('customer_requirement_methods.is_activated','1')
            ->get()->toArray();
       $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('warehouse.order')->with(compact('title','order','orderStatus','user_role'));
    }
    public function viewchecklist(Request $request){
            $title = 'View Requirements';
            $user = \Auth::user();
            $user_role = $user->role_id;
            $order_id = $request->order_id;
            $status = $request->status;
            $order_detail = Order::selectRaw('orders.*, customer_requirement_details.no_boxs, customer_requirement_details.total, user_infos.company_name,user_infos.contact_email, amazon_inventories.product_name, amazon_inventories.product_nick_name, customer_requirement_details.fnsku, shipping_methods.shipping_name')
                                 ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                                 ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                                 ->join('amazon_inventories', 'amazon_inventories.id', '=', 'customer_requirement_details.product_id', 'left')
                                 ->join('customer_requirement_methods', 'customer_requirement_methods.customer_requirement_id', '=', 'customer_requirements.id', 'left')
                                 ->join('shipping_methods', 'shipping_methods.shipping_method_id', '=', 'customer_requirement_methods.shipping_method_id', 'left')
                                 ->join('user_infos', 'user_infos.user_id', '=', 'orders.user_id', 'left')
                                 ->where('orders.order_id',$order_id)
                                 ->first();
            $prep_require = Prep_requirement::selectRaw('prep_services.service_name, prep_requirements.prep_id, orders.order_id, prep_requirements.id, prep_requirements.complete')
                                        ->join('customer_requirements','customer_requirements.id','=','prep_requirements.customer_requirement_id')
                                        ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
                                        ->join('orders','orders.order_id','=','customer_requirements.order_id')
                                        ->where('orders.order_id',$order_id)
                                        ->get();
            return view('warehouse/viewchecklist')->with(compact('order_detail', 'order_id', 'amazon_destination', 'user_role','order_labor','title','prep_require','status'));
    }
    public function getlabel(Request $request){
        if ($request->ajax()) {
            $post = $request->all();
            $fnsku = $post['fnsku'];
            $order_id = $post['order_id'];
            $image = '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($fnsku, "C39+", 1, 50) . '" alt="barcode"   />';
            if(!empty($image))
                Order::where('order_id',$order_id)->update(array('label_complete'=>'1'));
            return view('warehouse/barcode')->with(compact('image'));
        }
    }
    public function getotherlabel(Request $request){
        $post = $request->all();
        $order_id = $post['order_id'];
        $image = "This is set";
        if(!empty($image))
            Order::where('order_id',$order_id)->update(array('other_label_complete'=>'1'));
        return view('warehouse/barcode')->with(compact('image'));
    }
    public function submitchecklist(Request $request){
        $order_id= $request->order_id;
        $quantity=0;
        //$this->createshipments($order_id,$quantity);
        Order::where('order_id',$order_id)->update(array('is_activated'=>'18'));
        return redirect('warehouse/prep')->with('success','Prep Submitted Successfully');
    }
    public function prepcomplete(Request $request){
        if ($request->ajax()) {
            $post = $request->all();
            $prep_require_id = $post['prep_require_id'];
            $data = array('complete' => '1');
            Prep_requirement::where('id',$prep_require_id)->update($data);
            return 1;
        }
    }
    public function warehousereviewprep(){
        $user=\Auth::user();
        $user_role=$user->role_id;
        $title="Review Prep";
        $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated as method_activated,customer_requirement_methods.shipping_method_id, user_infos.company_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('user_infos','user_infos.user_id','=','orders.user_id')
            ->where('customer_requirement_methods.is_activated','1')
            ->where('orders.is_activated','=','18')
            ->where('customer_requirement_methods.is_activated','1')
            ->get()->toArray();
       $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('warehouse.order')->with(compact('title','order','orderStatus','user_role'));
    }
    public function reviewwork(Request $request){
        $title="Review Work Complete";
        $order_id = $request->order_id;
        $amazon_destination = Amazon_destination::where('order_id',$order_id)->get();
        $order_detail = Order::selectRaw('orders.*, outbound_methods.outbound_name,  shipping_methods.shipping_name, amazon_inventories.product_name, amazon_inventories.product_nick_name, customer_requirement_details.fnsku, customer_requirement_details.total, customer_requirement_details.product_id')
            ->join('customer_requirements', 'customer_requirements.order_id', '=', 'orders.order_id', 'left')
            ->join('customer_requirement_details', 'customer_requirement_details.customer_requirement_id', '=', 'customer_requirements.id', 'left')
            ->join('customer_requirement_methods', 'customer_requirement_methods.customer_requirement_id', '=', 'customer_requirements.id', 'left')
            ->join('amazon_inventories', 'amazon_inventories.id', '=', 'customer_requirement_details.product_id', 'left')
            ->join('shipping_methods', 'shipping_methods.shipping_method_id', '=', 'customer_requirement_methods.shipping_method_id')
            ->join('outbound_methods', 'outbound_methods.outbound_method_id', '=', 'customer_requirements.outbound_method_id')
            ->where('orders.order_id', $order_id)
            ->where('customer_requirement_methods.is_activated','1')
            ->first();
        $prep_detail = Prep_requirement::selectRaw('prep_requirements.*, prep_services.service_name ')
                       ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
                       ->join('customer_requirements','customer_requirements.id','=','prep_requirements.customer_requirement_id')
                       ->join('orders','orders.order_id','=','customer_requirements.order_id')
                       ->where('orders.order_id',$order_id)
                       ->get();
        $quantity = Order_inventory_history::where('order_id',$order_id)
                                        ->whereIn('status',array('3','4'))
                                        ->get();
        return view('warehouse/review_work')->with(compact('order_detail', 'amazon_destination', 'order_id', 'title','quantity','prep_detail'));
    }
    public function sendquantity(Request $request){
        $post = $request->all();
        $order_id = $post['order_id'];
        $product_id= $post['product_id'];
        $storageqty= $post['storageqty'];
        $amazonqty = $post['amazonqty'];
        Order_inventory_history::create(array('order_id'=>$order_id,'product_id'=>$product_id,'total'=>$storageqty,'status'=>'3'));
        Order_inventory_history::create(array('order_id'=>$order_id,'product_id'=>$product_id,'total'=>$amazonqty,'status'=>'4'));
    }
    public function submitreviewwork(Request $request){
        $order_id = $request->input('order_id');
        Order::where('order_id',$order_id)->update(array('is_activated'=>'19'));
        return redirect('warehouse/reviewprep')->with('success','Submit Review Work Successfully');
    }
    public function warehouseshippinglabels(){
        $user=\Auth::user();
        $user_role=$user->role_id;
        $title="Shipping Labels";
        $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated as method_activated,customer_requirement_methods.shipping_method_id, user_infos.company_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('user_infos','user_infos.user_id','=','orders.user_id')
            ->where('customer_requirement_methods.is_activated','1')
            ->where('orders.is_activated','=','19')
            ->where('customer_requirement_methods.is_activated','1')
            ->get()->toArray();
       $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('warehouse.order')->with(compact('title','order','orderStatus','user_role'));
    }
    public function shippinglabel(Request $request){
        $title="View Requirements";
        $order_id = $request->order_id;
        $order_detail = Order::selectRaw('orders.order_no, orders.order_id, user_infos.company_name, user_infos.contact_email, customer_requirements.*,customer_requirement_details.*, amazon_inventories.product_name, amazon_inventories.product_nick_name, amazon_inventories.fnsku, shipping_methods.shipping_name')
                             ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                             ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                             ->leftjoin('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                             ->leftjoin('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                             ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                             ->join('user_infos','user_infos.user_id','=','orders.user_id')
                             ->where('orders.order_id',$order_id)
                             ->where('customer_requirement_methods.is_activated','1')
                             ->first();
        $amazon_destination = Amazon_destination::selectRaw('amazon_destinations.*')
                                                ->where('order_id',$order_id)
                                                ->get();
        return view('warehouse/shipping_label')->with(compact('title','order_detail', 'order_id','amazon_destination'));
    }
    public function printshippinglabel(Request $request){
        $amazon_destination_id = $request->amazon_destination_id;
        $shipment_ids = Amazon_destination::selectRaw('amazon_destinations.api_shipment_id, amazon_destinations.feed_submition_id, amazon_destinations.cartoon_id, amazon_destinations.order_id, warehouse_checkins.total_cartons, orders.user_id')
            ->join('orders', 'orders.order_id', '=', 'amazon_destinations.order_id')
            ->join('warehouse_checkins', 'warehouse_checkins.order_id', '=', 'orders.order_id')
            ->where('amazon_destinations.amazon_destination_id', $amazon_destination_id)
            ->groupby('amazon_destinations.api_shipment_id')
            ->get();
        $user_id = isset($shipment_ids[0]->user_id) ? $shipment_ids[0]->user_id : '';
        $results = Amazon_marketplace::selectRaw("customer_amazon_details.mws_seller_id, customer_amazon_details.user_id, customer_amazon_details.mws_authtoken, amazon_marketplaces.market_place_id")
            ->join('customer_amazon_details', 'customer_amazon_details.mws_market_place_id', '=', 'amazon_marketplaces.id')
            ->where('customer_amazon_details.user_id', $user_id)
            ->get();
        $UserCredentials['mws_authtoken'] = !empty($results[0]->mws_authtoken) ? decrypt($results[0]->mws_authtoken) : '';
        $UserCredentials['mws_seller_id'] = !empty($results[0]->mws_seller_id) ? decrypt($results[0]->mws_seller_id) : '';
        $service = $this->getReportsClient();
        $shipping_request = new \FBAInboundServiceMWS_Model_GetUniquePackageLabelsRequest();
        $shipping_request->setSellerId($UserCredentials['mws_seller_id']);
        $shipping_request->setMWSAuthToken($UserCredentials['mws_authtoken']);
        foreach ($shipment_ids as $new_shipment_ids) {
            $shipping_request->setShipmentId($new_shipment_ids->api_shipment_id);
            $shipping_request->setPageType('PackageLabel_Letter_2');
            $label_content = new \FBAInboundServiceMWS_Model_PackageIdentifiers();
            $label_content->setmember($new_shipment_ids->cartoon_id);
            $shipping_request->setPackageLabelsToPrint($label_content);
            $response = $this->invokeGetUniquePackageLabels($service, $shipping_request);
            foreach ($response->GetUniquePackageLabelsResult as $packagelabel) {
                foreach ($packagelabel->TransportDocument as $trasport_document) {
                    $pdf_file = $trasport_document->PdfDocument;
                }
            }
            $data = array("shipping_label" => "1");
            Amazon_destination::where('amazon_destination_id', $amazon_destination_id)->update($data);
            $zipStr = $pdf_file;
            header('Content-Type: application/zip');
            header('Content-disposition: filename="shipping_label.zip"');
            $out = base64_decode($pdf_file);
            print($out);
            exit;
        }
    }
    function invokeGetUniquePackageLabels(\FBAInboundServiceMWS_Interface $service, $request){
        try {
            $response = $service->GetUniquePackageLabels($request);
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->saveXML();
            return $arr_response = new \SimpleXMLElement($dom->saveXML());
        } catch (\FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }
    public function verifylabel(Request $request){
        if ($request->ajax()) {
            $post = $request->all();
            $amazon_destination_id = $post['amazon_destination_id'];
            $status = $post['status'];
            $data = array('shipping_label' => $status);
            Amazon_destination::where('amazon_destination_id', $amazon_destination_id)->update($data);
            return $status;
        }
    }
    public function submitshipment(Request $request){
        $order_id = $request->order_id;
        Order::where('order_id',$order_id)->update(array('is_activated'=>'20'));
        return redirect('warehouse/shippinglabels')->with('success','Shipment Complete Successfully');
    }
    public function warehousereviewshipment(){
        $user=\Auth::user();
        $user_role=$user->role_id;
        $title="Review Shipment";
        $order = Order::selectRaw('orders.*, amazon_inventories.product_name,customer_requirements.id as customer_requirement_id,customer_requirement_methods.is_activated as method_activated,customer_requirement_methods.shipping_method_id, user_infos.company_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.customer_requirement_id')
            ->join('user_infos','user_infos.user_id','=','orders.user_id')
            ->where('customer_requirement_methods.is_activated','1')
            ->where('orders.is_activated','=','20')
            ->where('customer_requirement_methods.is_activated','1')
            ->get()->toArray();
       $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('warehouse.order')->with(compact('title','order','orderStatus','user_role'));
    }
    public function shipmentreview(Request $request){
        $title="Review Shipment";
        $order_id = $request->order_id;
        $amazon_destination = Amazon_destination::where('order_id',$order_id)->get();
        $order_detail = Order::selectRaw('customer_requirements.*,customer_requirement_details.*, amazon_inventories.product_name, amazon_inventories.product_nick_name,amazon_inventories.fnsku, shipping_methods.shipping_name, user_infos.company_name, user_infos.contact_email, orders.order_no, orders.order_id')
                        ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                        ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                        ->leftjoin('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                        ->leftjoin('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                        ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                        ->join('user_infos','user_infos.user_id','=','orders.user_id')
                        ->where('orders.order_id',$order_id)
                        ->where('customer_requirement_methods.is_activated','1')
                        ->first();
        return view('warehouse/admin_shipment_review')->with(compact('title','order_id', 'amazon_destination', 'order_detail'));
    }
    public function verifystatus(Request $request){
        if ($request->ajax()) {
            $post = $request->all();
            $order_id = $post['order_id'];
            $shipment_id = $post['shipment_id'];
            $data = array('verify_status' => '1');
            Shipments::where('shipment_id',$shipment_id)->update($data);
        }
    }
    public function complete(Request $request){
        $order_id = $request->order_id;
        Order::where('order_id',$order_id)->update(array('is_activated'=>'21'));
        return redirect('warehouse/reviewshipment')->with('success','Shipment Review Complete Successfully');
    }
    public function getestimateshippingcost(Request $request){
        if ($request->ajax()) {
            $user = \Auth::user();
            $post = $request->all();
            $shipment_id= $post['shipment_id'];
            $order_id = $post['order_id'];
            $results = Amazon_marketplace::selectRaw("customer_amazon_details.mws_seller_id, customer_amazon_details.user_id, customer_amazon_details.mws_authtoken, amazon_marketplaces.market_place_id")
                ->join('customer_amazon_details', 'customer_amazon_details.mws_market_place_id', '=', 'amazon_marketplaces.id')
                ->join('orders','orders.user_id','=','customer_amazon_details.user_id')
                ->where('orders.order_id', $order_id)
                ->first();
            $UserCredentials['mws_authtoken'] = !empty($results->mws_authtoken) ? decrypt($results->mws_authtoken) : '';
            $UserCredentials['mws_seller_id'] = !empty($results->mws_seller_id) ? decrypt($results->mws_seller_id) : '';
            $service = $this->getReportsClient();
            $shipping_price_request = new \FBAInboundServiceMWS_Model_GetTransportContentRequest();
            $shipping_price_request->setSellerId($UserCredentials['mws_seller_id']);
            $shipping_price_request->setShipmentId($shipment_id);
            $response=$this->invokeGetTransportContent($service, $shipping_price_request);
            echo $response;
        }
    }
    function invokeGetTransportContent(\FBAInboundServiceMWS_Interface $service, $request){
        try {
            $response = $service->GetTransportContent($request);
            return $response;
            $dom = new \DOMDocument();
            $dom->loadXML($response->toXML());
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            echo $dom->saveXML();
            echo("ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");
            EXIT;
        } catch (FBAInboundServiceMWS_Exception $ex) {
            echo("Caught Exception: " . $ex->getMessage() . "\n");
            echo("Response Status Code: " . $ex->getStatusCode() . "\n");
            echo("Error Code: " . $ex->getErrorCode() . "\n");
            echo("Error Type: " . $ex->getErrorType() . "\n");
            echo("Request ID: " . $ex->getRequestId() . "\n");
            echo("XML: " . $ex->getXML() . "\n");
            echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
        }
    }
    public function packaginglist(Request $request){
        $order_id= $request->order_id;
        $packaging = Order_package::selectRaw('orders.order_no, order_packages.*')
                                ->join('orders','orders.order_id','=','order_packages.order_id')
                                ->where('orders.order_id',$order_id)
                                ->first();
        return view('warehouse.orderpackage')->with(compact('title','order_id','packaging'));
    }
    public function warehouseholdstatus(Request $request){
        Order_warehouse_hold::where('id',$request->id)->update(array('is_activated'=>$request->status));
    }
}