<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Order;
use App\Bill_of_lading;
use App\OrderHistory;
use App\Hold;
use App\SubHold;
use App\OrderHold;
use App\Order_document;
use App\Prealert_detail;
use App\TelexRelease;
use App\Delivery;
use App\CustomerClearance;
use App\Shipment_contain;
use App\Custom_requirement_detail;
use App\Location;
use App\Document;
use App\Order_note;

class LogisticsController extends Controller{

    public function index(){
    	$data['order'] = Order::selectRaw('orders.order_id,orders.created_at,orders.order_no,amazon_inventories.product_name,amazon_inventories.product_nick_name, user_infos.company_name')
            ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
            ->join('customer_requirement_details','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->leftjoin('user_infos','user_infos.user_id','=','customer_requirements.user_id')
            ->where('orders.is_activated','2')
            ->where('customer_requirements.order_type','1')
            ->get();
    	return view('logistics.index',$data);
    }


    public function billOfLadingDetails(Request $request){
    	$data['order_id'] = $request->id;
        $data['order_files'] = Bill_of_lading::select('bill_of_ladings.bill as order_file')->where('order_id',$data['order_id'])->whereIn('status',['0','2'])->first();
        $data['order'] = Order::selectRaw('orders.*,user_infos.company_name')
                                ->leftjoin('user_infos','user_infos.user_id','=','orders.user_id')
                                ->where('order_id',$request->id)
                                ->first();
        return view('logistics.view_order',$data);
    }


    public function viewfile(Request $request){
        $post = $request->all();
        $order_id = $post['order_id'];
        $order_files = Bill_of_lading::select('bill_of_ladings.bill as order_file')->where('order_id',$order_id)->where('status','0')->first();
        if($order_files){
            $file =  base_path().'/public/uploads/bills/'.$order_files->order_file;
            if (file_exists($file)){
                $ext =\File::extension($file);
                if($ext=='pdf'){
                    $content_types='application/pdf';
                }elseif ($ext=='doc') {
                    $content_types='application/msword';
                }elseif ($ext=='docx') {
                    $content_types = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                }
                return response(file_get_contents($file),200)
                    ->header('Content-Type',$content_types);
            }
        }
    }


    public function approveBillOfLading(Request $request){
        Order::where('order_id',$request->id)->update(array('is_activated'=>'3'));
        Bill_of_lading::where('order_id',$request->id)->where('status','0')->update(array('status'=>'1'));
        return redirect('bill_of_lading')->with('success', 'Bill Of Lading approved successfully');
    }


    public function revisionBillOfLading(Request $request){
        $order_id = $request->input('order');
        $note = $request->input('revision_note');
        Order::where('order_id',$order_id)->update(array('is_activated'=>'1'));
        Bill_of_lading::where('order_id',$order_id)->update(array('status'=>'2','revision_note'=>$note));
        return redirect('bill_of_lading');
    }


    public function showShipmentList(){
        $order = Order::selectRaw('orders.*,user_infos.company_name,amazon_inventories.product_name,customer_requirements.port_of_origin,customer_requirements.goods_ready_date,customer_requirements.id,customer_requirements.port_of_origin,customer_requirements.shipper_destination,prealert_details.ETD_china,prealert_details.ETA_US,prealert_details.created_at as prealert_details_ctreated_at,shipping_methods.shipping_name')
                ->leftjoin('user_infos','user_infos.user_id','=','orders.user_id')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type')
                ->join('prealert_details','prealert_details.order_id','=','orders.order_id','left')
                ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id','left')
                ->orderBy('order_id','desc')
                ->where('customer_requirement_methods.is_activated','1')
                ->where('orders.is_activated','<=','16')
                ->where('orders.is_activated','>=','7')
                ->Orwhere('orders.is_activated','=','6')
                ->where('customer_requirement_methods.shipping_method_id','<>','1')
                ->where('customer_requirement_methods.is_activated','1')
                ->get()->toArray();
       $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('logistics.shipment')->with(compact('orderStatus','order'));
    }


    public function viewShipment($order_id){
        $order = Order::selectRaw('orders.*,orders.order_id as orders_id,shipping_methods.shipping_name,shipping_methods.shipping_method_id,customer_requirement_details.product_id,amazon_inventories.product_name,customer_requirement_details.total,customer_requirement_details.no_boxs, shipment_types.short_name as shipment_type_short_name,incoterms.short_name as incoterm_short_name,incoterms.id as incoterm_id,prealert_details.*,prealert_details.id as prealert_id,outbound_methods.outbound_name, outbound_methods.short_name as outbound_method_short_name,suppliers.contact_name,delivery.ETA_to_warehouse,delivery.delivery_company,delivery.location_of_goods, delivery.created_at as cargo_release, delivery.id as delivery_id,warehouse_checkins.created_at as checkin_date, warehouse_checkins.id as checkin_id,customer_clearances.created_at as custom_release_date,shipping_quotes.chargeable_weight,customer_clearances.FDA_clearance,customer_clearances.lacey_act,customer_clearances.OGA,customer_requirements.id as customer_requirement_id,user_infos.company_name')
                ->leftjoin('user_infos','user_infos.user_id','=','orders.user_id')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','customer_requirement_details.product_id','=','amazon_inventories.id','left')
                ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                ->leftjoin('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type','left')
                ->leftjoin('incoterms','incoterms.id','=','customer_requirements.incoterms','left')
                ->leftjoin('prealert_details','prealert_details.order_id','=','customer_requirements.order_id','left')
                ->leftjoin('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
                ->leftjoin('suppliers','customer_requirements.supplier','=','suppliers.supplier_id')
                ->leftjoin('delivery','delivery.order_id','=','customer_requirements.order_id')
                ->leftjoin('warehouse_checkins','warehouse_checkins.order_id','=','customer_requirements.order_id')
                ->leftjoin('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id')
                ->leftjoin('customer_clearances','customer_clearances.order_id','=','orders.order_id')
                ->where('orders.order_id',$order_id)
                ->where('customer_requirement_methods.is_activated',1)
                ->first();
        $shipping_quote_charges = null;
        $order_histories = OrderHistory::selectRaw('order_history.*,prealert_details.ETD_china,prealert_details.created_at as prealert_details_created_at,customer_requirements.port_of_origin, customer_requirements.shipper_destination,customer_requirements.id as customer_requirements_id')
                                ->join('prealert_details','prealert_details.order_id','=','order_history.order_id','left')
                                ->join('customer_requirements','customer_requirements.order_id','=','order_history.order_id','left')
                                ->where('order_history.order_id',$order_id)
                                ->get();
        $holds = Hold::where('shipping_method_id',$order['shipping_method_id'])
                         ->get();
        $sub_holds = SubHold::selectRaw('sub_holds.*')
                        ->leftjoin('holds','sub_holds.hold_id','=','holds.id')
                        ->where('holds.shipping_method_id',$order['shipping_method_id'])
                        ->get();
        $order_holds = OrderHold::selectRaw('order_holds.*,sub_holds.sub_hold_name')
                        ->leftjoin('sub_holds','sub_holds.id','=','order_holds.sub_hold_id')
                        ->where('order_holds.order_id',$order_id)
                        ->where('order_holds.is_deleted',0)
                        ->get();
        $order_documents = Order_document::where('order_id',$order_id)->get();
        $prealert_detail = Prealert_detail::where('order_id',$order_id)->first();
        $bill_of_lading = Bill_of_lading::where('order_id',$order_id)->first();
        $telex_release = TelexRelease::where('order_id',$order_id)->first();
        $customer_clearance = CustomerClearance::where('order_id',$order_id)->first();
        $custom_exam_hold = OrderHold::where('hold_id',2)->where('sub_hold_id',7)->first();
        $shipment_contains = Shipment_contain::selectRaw('shipment_contains.*,custom_requirement_details.customer_requirement_id')
                        ->join('custom_requirement_details','custom_requirement_details.custom_id','=','shipment_contains.id')
                        ->where('custom_requirement_details.customer_requirement_id',$order['customer_requirement_id'])
                        ->get();
        $all_shipment_contains = Shipment_contain::all();
        $shipment_contain_names = Shipment_contain::join('custom_requirement_details','custom_requirement_details.custom_id','=','shipment_contains.id')
                        ->where('custom_requirement_details.customer_requirement_id',$order['customer_requirement_id'])
                        ->pluck('shipment_contains.name')->toArray();
        $locations = Location::all();
        $documents= Document::selectRaw('documents.*, order_documents.order_id,order_documents.status,order_documents.document_id,order_documents.created_at as order_document_created_at,order_documents.id as order_document_id,order_documents.due_date');
            $documents->leftjoin('order_documents',function($join) use ($order_id){
                $join->on('order_documents.document_id','=','documents.id');
                $join->where('order_documents.order_id','=', $order_id);
            });
            $documents = $documents->get();

        $order_notes = Order_note::where('order_id',$order_id)->get();
        $cfs_fee = OrderHold::where('order_id',$order_id)->sum('fee');

        return view('logistics.view_shipment')->with(compact('order','shipping_quote_charges','order_histories','holds','sub_holds','order_holds','order_documents','prealert_detail','bill_of_lading','telex_release','customer_clearance','custom_exam_hold','shipment_contains','all_shipment_contains','shipment_contain_names','locations','documents','order_notes','cfs_fee','order_id'));
    }
    public function getShipment(Request $request){
        $data['order'] = Order::selectRaw('orders.*,user_infos.company_name,amazon_inventories.product_name,customer_requirements.port_of_origin,customer_requirements.goods_ready_date,customer_requirements.id,customer_requirements.port_of_origin,customer_requirements.shipper_destination,prealert_details.ETD_china,prealert_details.ETA_US,prealert_details.created_at as prealert_details_ctreated_at,shipping_methods.shipping_name')
                    ->leftjoin('user_infos','user_infos.user_id','=','orders.user_id')
                    ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                    ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                    ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                    ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type')
                    ->join('prealert_details','prealert_details.order_id','=','orders.order_id','left')
                    ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                    ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id','left')
                    ->where('customer_requirement_methods.is_activated',1);
        if($request->tab_id == 1){
            //$data['order']->where('orders.is_activated','>=','7');
            //$data['order']->where('orders.is_activated','<=','17');
            $data['order']->where('orders.is_activated','<=','16');
            $data['order']->where('orders.is_activated','>=','7');
            $data['order']->Orwhere('orders.is_activated','=','6');
            $data['order']->where('customer_requirement_methods.shipping_method_id','<>','1');
        }
        if($request->tab_id == 2){
            $data['order']->where('orders.is_activated','<=','5');
        }
        if($request->tab_id == 3){
            $data['order']->where('orders.is_activated','<=','6');
        }
        if($request->tab_id == 4){
            $data['order']->where('orders.is_activated','>=','6');
            $data['order']->where('orders.is_activated','<=','17');
        }
        if($request->tab_id == 5){
            $data['order']->where('orders.is_activated','>','17');
        }
        $data['order'] = $data['order']->get()->toArray();
        $data['orderStatus'] = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Warehouse Processing Complete','Released to Amazon, Complete','Storage');
            return $data;
    }
    public function submitHold(Request $request){
        $data = [
            'order_id' => $request->order_id
        ];
        $hold_id = 0;
        foreach ($request->all() as $key => $value) {
            if(strpos($key, 'hold_checkbox_') !== false) {
                $holds = explode("_",$key);
                $data = array_merge($data, ['sub_hold_id' => $holds[3]]);
                $data = array_merge($data, ['hold_id' => $holds[2]]);
                $order_hold = OrderHold::create($data);
                $hold_id = $holds[2];
            }
            if(strpos($key, 'note_') !== false){
                if($value !== null){
                    $holds = explode("_",$key);
                    OrderHold::where('hold_id',$holds[1])->where('sub_hold_id',$holds[2])->update(['note' => $value]);
                }
            }
        }
        if($hold_id=='1')
            Order::where('order_id',$request->order_id)->update(array('is_activated'=>'10'));
        else if($hold_id=='2')
            Order::where('order_id',$request->order_id)->update(array('is_activated'=>'12'));
        return back();
    }
    public function releaseHold(Request $request){
        foreach ($request->all() as $key => $value) {
            if(strpos($key, 'fee_') !== false) {
                $hold = explode("_",$key);
                OrderHold::where('hold_id',$hold[1])->where('sub_hold_id',$hold[2])->update(['fee' => $value,'is_deleted' => 1]);
            }
            if(strpos($key, 'confirm_') !== false) {
                if($value != 0){
                    $hold = explode("_",$key);
                    OrderHold::where('hold_id',$hold[1])->where('sub_hold_id',$hold[2])->update(['is_deleted' => 1]);
                }
            }
        }
        return back();
    }
    public function updateDocument(Request $request){
        $data = array();
        if($request->order_document_id){
            if($request->document){
                if($request->order_document_name == 'Commercial Invoice') {
                    $newname = $request->order_id."_commercial_invoice".'.'.$request->document->getClientOriginalExtension();
                    $destinationPath = public_path() . '/uploads/documents';
                    $request->file('document')->move($destinationPath, $newname);
                    $data = array_merge($data,['document' => $newname]);
                }
                elseif($request->order_document_name == 'Packing List') {
                    $packing_resume = $request->order_id."_packing_list".'.'.$request->document->getClientOriginalExtension();
                    $destinationPath = public_path() . '/uploads/documents';
                    $request->file('document')->move($destinationPath, $packing_resume);
                    $data = array_merge($data,['document' => $packing_resume]);
                }
            }
            if($request->due_date){
                $data = array_merge($data,['due_date' => \Carbon\Carbon::parse($request->due_date)->format('Y-m-d h:m:s')]);
            }
            Order_document::where('id',$request->order_document_id)->update($data);
        }
        else{
            $data = [
                'order_id' => $request->order_id,
                'status' => 0,
                'document_id' => $request->document_id
            ];
            if($request->due_date){
                $data = array_merge($data,['due_date' => \Carbon\Carbon::parse($request->due_date)->format('Y-m-d h:m:d')]);
            }
            if($request->order_document_name == 'Commercial Invoice') {
                $newname = $request->order_id."_commercial_invoice".'.'.$request->document->getClientOriginalExtension();
                $destinationPath = public_path() . '/uploads/documents';
                $request->file('document')->move($destinationPath, $newname);
                $data = array_merge($data,['document' => $newname]);
            }
            elseif($request->order_document_name == 'Packing List') {
                $packing_resume = $request->order_id."_packing_list".'.'.$request->document->getClientOriginalExtension();
                $destinationPath = public_path() . '/uploads/documents';
                $request->file('document')->move($destinationPath, $packing_resume);
                $data = array_merge($data,['document' => $packing_resume]);
            }
            elseif($request->order_document_name == 'Photos') {
                $packing_resume = $request->order_id."_photos".'.'.$request->document->getClientOriginalExtension();
                $destinationPath = public_path() . '/uploads/documents';
                $request->file('document')->move($destinationPath, $packing_resume);
                $data = array_merge($data,['document' => $packing_resume]);
            }
            Order_document::create($data);
        }
        return back();
    }
    public function deleteDocument(Request $request){
        Order_document::where('id',$request->delete_document_id)->delete();
        return back();
    }
    public function updateLogisticsDetails(Request $request){

        $delivery_data = array();
        $pre_alert_data = array();

        if($request->location_of_goods){
            $delivery_data = array_merge($delivery_data, ['location_of_goods' => $request->location_of_goods]);
        }
        if($request->delivery_date){
            $delivery_data = array_merge($delivery_data, ['ETA_to_warehouse' =>  \Carbon\Carbon::parse($request->delivery_date)->format('Y-m-d h:m:s')]);
        }

        Delivery::where('order_id',$request->order_id)->update($delivery_data);

        if($request->shipment_contain){
            Custom_requirement_detail::where('customer_requirement_id',$request->customer_requirement_id)->delete();

            foreach ($request->shipment_contain as $value) {
                Custom_requirement_detail::create([
                    'customer_requirement_id' => $request->customer_requirement_id,
                    'custom_id' => $value
                ]);
            }
        }

        CustomerClearance::where('order_id',$request->order_id)->update(['FDA_clearance' => 0 ,'lacey_act' => 0 ,'OGA' => 0]);
        
        if($request->additional_service){
            foreach ($request->additional_service as $value) {
                CustomerClearance::where('order_id',$request->order_id)->update([$value => '1','terminal_fees' => $request->port_fees]);
            }
        }
        if($request->ETD){
            $pre_alert_data = array_merge($pre_alert_data, ['ETD_china' => \Carbon\Carbon::parse($request->ETD)->format('Y-m-d h:m:s')]);
        }
        if($request->ETA){
            $pre_alert_data = array_merge($pre_alert_data, ['ETA_US' => \Carbon\Carbon::parse($request->ETA)->format('Y-m-d h:m:s')]);
        }
        
        if($request->port_fees){
            CustomerClearance::where('order_id',$request->order_id)->update(['terminal_fees' => $request->port_fees]);
        }
        
        Prealert_detail::where('order_id',$request->order_id)->update($pre_alert_data);

        return back();
    }
    public function addLocation(Request $request){
        Location::create(['name' => $request->location]);
        return back();
    }
    public function updatelocation(Request $request)
    {
        if ($request->location == '1') {
            Order::where('order_id', $request->order_id)->update(array('location' => $request->location, 'is_activated' => '8'));
        }
        else if ($request->location == '2') {
            Order::where('order_id', $request->order_id)->update(array('location' => $request->location, 'is_activated' => '13'));
        }
        else if ($request->location == '3') {
            Order::where('order_id', $request->order_id)->update(array('location' => $request->location, 'is_activated' => '14'));
        }
    }


    public function approveDocument($document_id){
        
        Order_document::where('id',$document_id)->update(['status' => 1]);
        return back();
    }

    public function revisionDocument($document_id){
        
        Order_document::where('id',$document_id)->update(['status' => 3]);
        return back();
    }

}