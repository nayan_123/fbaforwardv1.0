<?php

namespace App\Http\Controllers;
use App\Hello_sign_detail;
use App\User_agreement_detail;
use Illuminate\Http\Request;
use HelloSign;
class User_agreement extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(){}
    public function create(){}
    public function store(Request $request){
        $user=\Auth::user();
        $hellosign = Hello_sign_detail::first();
        $client = new HelloSign\Client($hellosign->sign_key);
        $request = new HelloSign\SignatureRequest;
        $request->enableTestMode();
        $request->addSigner($user->email, $user->email);
        $request->addFile(public_path() . '/uploads/test.pdf');
        $client_id = $hellosign->client_id;
        $embedded_request = new HelloSign\EmbeddedSignatureRequest($request, $client_id);
        $response = $client->createEmbeddedSignatureRequest($embedded_request);
        $agreement_data = array('user_id' => $user->id,
            'signature_request_id' => $response->signature_request_id,
            'response' => "test",
            'status' => '0'
        );
        User_agreement_detail::create($agreement_data);
        $client1 = new HelloSign\Client($hellosign->sign_key);
        $response1 = $client1->getEmbeddedSignUrl($response->signatures[0]->signature_id);
        $claim_url = $response1->getSignUrl();
        $detail=array();
        $detail['claim_url']= $claim_url;
        $detail['client_id']= $client_id;
        return json_encode($detail);
    }
    public function show($id){}
    public function edit($id){}
    public function update(Request $request, $id){}
    public function destroy($id){}
}