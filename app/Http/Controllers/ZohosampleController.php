<?php

namespace App\Http\Controllers;
use App\ZohoDetail;
use App\ZohoAccountManager;
use App\User;
use App\Order;

class ZohosampleController extends Controller{
    public function authGenerate(){
        $auth = $this->api('https://accounts.zoho.com/apiauthtoken/nb/create?SCOPE=ZohoCRM%2Fcrmapi&EMAIL_ID=webdimensionsindia%40gmail.com&PASSWORD=webdimensions%40123&DISPLAY_NAME=webdimensionsindia');
        $arr = explode("\n", $auth);
        $str = strstr($arr[2], '=');
        $token = ltrim($str, '=');
        if($arr[3] == "RESULT=TRUE"){
            $zoho_detail = ZohoDetail::create([
                'auth_token' => $token
            ]);
        }
        else{
            dd('Auth already generated.');
        }
       return back();
    }
    public function check_token(){
        $zoho_detail = ZohoDetail::first();
        if(!$zoho_detail){
            $this->authGenerate();
            $zoho_detail = ZohoDetail::first();
        }
        return $zoho_detail->auth_token;
    }
    public function getLead(){
        $token = $this->check_token();
        $lead = $this->api("https://crm.zoho.com/crm/private/json/Leads/getRecords?authtoken=".$token."&scope=crmapi");
        $arrayOfEmails=json_decode($lead);
        dd($lead,$arrayOfEmails);
        return back();
    }
    public function insertNote(){
        $token = $this->check_token();
        $insert_note = $this->api("https://crm.zoho.com/crm/private/xml/Leads/insertRecords?newFormat=1&authtoken=".$token."&scope=crmapi&xmlData=%3CLeads%3E%3Crow%20no%3D%221%22%3E%3CFL%20val%3D%22Lead%20Source%22%3EWeb%20Download%3C%2FFL%3E%3CFL%20val%3D%22Company%22%3EYour%20Company%3C%2FFL%3E%3CFL%20val%3D%22First%20Name%22%3EHannah%3C%2FFL%3E%3CFL%20val%3D%22Last%20Name%22%3ESmith%3C%2FFL%3E%3CFL%20val%3D%22Email%22%3Etesting%40testing.com%3C%2FFL%3E%3CFL%20val%3D%22Title%22%3EManager%3C%2FFL%3E%3CFL%20val%3D%22Phone%22%3E1234567890%3C%2FFL%3E%3CFL%20val%3D%22Home%20Phone%22%3E0987654321%3C%2FFL%3E%3CFL%20val%3D%22Other%20Phone%22%3E1212211212%3C%2FFL%3E%3CFL%20val%3D%22Fax%22%3E02927272626%3C%2FFL%3E%3CFL%20val%3D%22Mobile%22%3E292827622%3C%2FFL%3E%3C%2Frow%3E%3C%2FLeads%3E");

        $response = simplexml_load_string($insert_note);
        
        dd($response->result->recorddetail->FL[0]);
    }
    public function getUsers(){
        $token = $this->check_token();
        $users= $this->api("https://crm.zoho.com/crm/private/json/Users/getUsers?authtoken=94481b29dfb6ad6813af6a3cde62d7da&scope=crmapi&type=ActiveUsers");
        $data = json_decode($users);
        foreach ($data as $key => $user) {
            foreach ($user as $value) {
                foreach ($value as $val) {
                    if($val->role == 'Consultant'){
                        $manager = ZohoAccountManager::where('email',$val->email)->first();
                        if(!isset($manager)){
                            ZohoAccountManager::create([
                                'zoho_user_account_id' => $val->id,
                                'email' => $val->email,
                                'role' => $val->role,
                                'phone' => $val->phone,
                                'name' => $val->content,
                                'mobile' => $val->mobile,
                                'status' => 0
                            ]);
                        }
                    }
                }
            }
        }
        $deactive_users = $this->api("https://crm.zoho.com/crm/private/json/Users/getUsers?authtoken=94481b29dfb6ad6813af6a3cde62d7da&scope=crmapi&type=DeactiveUsers");
        $data = json_decode($deactive_users);
        foreach ($data as $key => $user) {
            foreach ($user as $value) {
                if(isset($value->code)){
                    if($value->code == '4422'){
                        dd('exit');
                    }
                }
                foreach ($value as $val) {
                    if($val->role == 'Manager'){
                        ZohoAccountManager::where('zoho_user_account_id',$val->id)
                            ->update(['status' => 1]);
                    }
                }
            }
        }

        return back();
    }
    public function getModules(){
        $token = $this->check_token();
        $modules= $this->api("https://crm.zoho.com/crm/private/xml/Info/getModules?authtoken=".$token."&scope=crmapi");
        dd($modules);
    }
    public function insertAccount(){
        $token = $this->check_token();
        $accounts = $this->api("https://crm.zoho.com/crm/private/xml/Accounts/insertRecords?authtoken=".$token."&scope=crmapi&newFormat=1&xmlData=<Accounts><row no=".'5'."><FL val=".'ACCOUNTID'.">2957445000000159003</FL><FL val=".'SMOWNERID'.">2957445000000135011</FL><FL val=".'Account Owner'."><![CDATA[webdimensionsindia]]></FL><FL val=".'Account Name'."><![CDATA[test]]></FL><FL val=".'Fax'."><![CDATA[545534]]></FL><FL val=".'Website'."><![CDATA[www.test.com]]></FL><FL val=".'Account Number'."><![CDATA[435435]]></FL><FL val=".'Ownership'."><![CDATA[test]]></FL><FL val=".'Industry'."><![CDATA[test]]></FL><FL val=".'Employees'."><![CDATA[4543]]></FL><FL val=".'Annual Revenue'."><![CDATA[756867867]]></FL><FL val=".'SMCREATORID'.">2957445000000135011</FL><FL val=".'Created By'."><![CDATA[test]]></FL><FL val=".'MODIFIEDBY'.">2957445000000135011</FL><FL val=".'Modified By'."><![CDATA[test]]></FL><FL val=".'Created Time'."><![CDATA[2017-11-26 17:51:39]]></FL><FL val=".'Modified Time'."><![CDATA[2017-11-26 17:51:39]]></FL><FL val=".'Last Activity Time'."><![CDATA[2017-11-26 17:51:39]]></FL></row></Accounts>");
        dd($accounts);
    }
    public function getAccount(){
        $token = $this->check_token();
        $get_account = $this->api("https://crm.zoho.com/crm/private/xml/Accounts/getRecords?newFormat=1&authtoken=".$token."&scope=crmapi");
        dd($get_account);
    }


    public function getOrdersFromZoho(){
        
        $token = $this->check_token();
        $orders = $this->api("https://crm.zoho.com/crm/private/json/CustomModule4/getRecords?newFormat=1&authtoken=".$token."&scope=crmapi");

        $order_response = json_decode($orders);

        $order_data = Order::where('zoho_order_id',null)->pluck('order_no')->toArray();

        foreach ($order_response->response->result->CustomModule4->row as $key => $response) {
            foreach ($response->FL as $value) {
                if($value->val == 'CUSTOMMODULE4_ID'){
                    $zoho_order_id = $value->content;
                }

                if($value->val == 'Order Name'){
                    if(in_array($value->content, $order_data)){
                        Order::where('order_no',$value->content)->update(['zoho_order_id' => $zoho_order_id]);
                    }
                }
            }
        }

        dd('Done');
    }

    public function api($url){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $response;
        }
    }
}