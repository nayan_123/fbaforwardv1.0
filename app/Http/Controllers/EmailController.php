<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
class EmailController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(){}
     public function getnotify(){
        return view('example');
    }
    public function notify(Request $request){
        $listId = env('MAILCHIMP_LIST_ID');
        $mailchimp = new \Mailchimp(env('MAILCHIMP_KEY'));
        $campaign = $mailchimp->campaigns->create('regular', [
            'list_id' => $listId,
            'subject' => 'New Article from Scotch',
            'from_email' => 'webdimensionsindia@gmail.com',
            'from_name' => 'test',
            'to_name' => 'test'

        ], [
            'html' => $request->input('content'),
            'text' => strip_tags($request->input('content'))
        ]);
        $mailchimp->campaigns->send($campaign['id']);
        return redirect('/notify')->with('success', 'Your Mail Send Successfully');
    }
}