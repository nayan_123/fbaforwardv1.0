<?php
namespace App\Http\Controllers;
use App\Charges;
use App\Customer_requirement;
use App\Customer_requirement_detail;
use App\Customer_requirement_method;
use App\Prepservicequote;
use App\Shipping_charges;
use App\Shipping_method;
use App\Shipping_quote;
use App\Order;
use App\Shipping_quote_charge;
use App\User_info;
use Illuminate\Http\Request;
use App\Libraries;
use Carbon\Carbon;
use PDF;
use DNS1D;
use App\QuoteHistory;
use App\OrderHistory;
use App\ShipperShipment;
use App\Prealert_detail;
use App\Prep_requirement;
class ShipperController extends Controller{
    public function __construct(){
        $this->middleware(['auth']);
    }
    public function index(){}
    public function viewrejectquote(){
        $title = "Rejected Shipping Quote";
        $user = \Auth::user();
        $user_role = $user->role_id;
        $shipping_quote=Shipping_quote::selectRaw('shipping_quotes.user_id, shipping_quotes.status, customer_requirements.*, user_infos.company_name, user_infos.contact_email')
            ->join('customer_requirements','customer_requirements.id','=','shipping_quotes.customer_requirement_id')
            ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id')
            ->where('shipping_quotes.status','2')
            ->where('shipping_quotes.user_id',$user->id)
            ->get();
        $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('shipper.shippingquote_detail')->with(compact('shipping_quote', 'orderStatus', 'user_role', 'title'));
    }
    public function viewquote(Request $request){
        $id = $request->id;
        $user_id = $request->user_id;
        $shipment = Customer_requirement::selectRaw('shipping_quotes.*, customer_requirements.*, customer_requirements.id as customer_requirement_id, suppliers.company_name')
            ->join('suppliers','suppliers.supplier_id','=','customer_requirements.supplier')
            ->join('shipping_quotes', 'shipping_quotes.customer_requirement_id', '=', 'customer_requirements.id')
            ->where('customer_requirements.id', $id)
            ->groupby('customer_requirements.id')
            ->get();
        $shipment_detail = Customer_requirement_detail::selectRaw('shipping_methods.shipping_name, amazon_inventories.product_name, amazon_inventories.product_nick_name, customer_requirement_details.qty_per_box, customer_requirement_details.no_boxs, customer_requirement_details.total')
            ->join('amazon_inventories', 'amazon_inventories.id', '=', 'customer_requirement_details.product_id', 'left')
            ->join('customer_requirements','customer_requirements.id','=','customer_requirement_details.customer_requirement_id')
            ->join('shipping_methods', 'shipping_methods.shipping_method_id', '=', 'customer_requirements.shipping_method_id')
            ->where('customer_requirement_details.customer_requirement_id', $id)
            ->get();
        return view('shipper/viewshippingquote')->with(compact('shipment','shipment_detail','charges'));
    }
    public function quotelist(){
        $title="Shipping Quote";
        $user = \Auth::user();
        $user_role = $user->role_id;
        $user_id = $user->id;
        $quote_request = Customer_requirement::selectRaw('amazon_inventories.product_name, user_infos.company_name, customer_requirements.created_at, customer_requirements.is_activated, customer_requirements.id,customer_requirement_details.product_name as customer_requirement_details_product_name')
                        ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                        ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id','left')
                        ->leftjoin('user_infos','user_infos.user_id','=','customer_requirements.user_id')
                        ->join('customer_requirement_methods','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                        ->where('customer_requirements.order_type','1')
                        ->WhereNull('customer_requirements.expire_date')
                        ->where('customer_requirements.is_activated','0')
                        ->orwhere('customer_requirements.is_activated','5')
                        ->orWhere('customer_requirements.is_activated','3')
                        ->groupby('customer_requirement_methods.customer_requirement_id')
                        ->get();
              
        $quote_expire = Customer_requirement::selectRaw('amazon_inventories.product_name, user_infos.company_name, customer_requirements.created_at, customer_requirements.is_activated, customer_requirements.id, shipping_quotes.created_at as submitted_date,customer_requirement_details.product_name as customer_requirement_details_product_name')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->leftjoin('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id')
            ->join('shipping_quotes','shipping_quotes.customer_requirement_id','=','customer_requirements.id','left')
            ->where('customer_requirements.order_type','1')
            ->where('customer_requirements.is_activated','0')
            ->where('customer_requirements.expire_date','<',Carbon::now()->toDateString())
            ->Orwhere('customer_requirements.is_activated','2')
            ->groupby('shipping_quotes.customer_requirement_id')
            ->get();
        return view('shipper/quotelist')->with(compact('title','quote_request','quote_expire','user_id'));
    }
    public function getshippingrequest(){
        $id = $_POST['id'];
        $quote_request = Customer_requirement::selectRaw('customer_requirement_details.product_name as productname, customer_requirement_details.product_id, customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box, customer_requirements.shipment_weight, customer_requirements.shipment_volume, customer_requirements.shipment_size_type, customer_requirements.incoterms, customer_requirements.port_of_origin, customer_requirements.goods_ready_date, amazon_inventories.product_name, user_infos.company_name, customer_requirements.created_at, customer_requirements.is_activated, customer_requirements.id, customer_requirements.shipment_notes, customer_requirements.expire_date, customer_requirements.is_activated, customer_requirements.units, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->leftjoin('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
            ->join('user_infos','user_infos.user_id','=','customer_requirements.user_id')
            ->leftjoin('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type')
            ->leftjoin('incoterms','incoterms.id','=','customer_requirements.incoterms')
            ->where('customer_requirements.id',$id)
            ->get();
        $quote_method = Shipping_method::selectRaw('shipping_methods.shipping_name')
             ->join('customer_requirement_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
             ->where('customer_requirement_methods.customer_requirement_id',$id)
             ->get();
        $detail_charge = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
            ->leftjoin('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id')
            ->join('charges','charges.id','=','shipping_quote_charges.charge_id')
            ->where('shipping_quotes.customer_requirement_id',$id)
            ->get();
     echo json_encode(array("quote_request"=>$quote_request, "quote_method"=>$quote_method,"detail_charge"=>$detail_charge));
    }
    public function shippingquoteform(Request $request){
        $title = "Shipping Quote";
        $id = $request->id;
        $user = User_info::selectRaw('user_infos.company_name, user_infos.contact_email, user_infos.user_id')
            ->join('customer_requirements', 'customer_requirements.user_id', '=', 'user_infos.user_id')
            ->where('customer_requirements.id', $id)
            ->first();
        $user_id= isset($user) ? $user->user_id : '';
        $charges = Charges::all();
        $quote_request = Customer_requirement::selectRaw('customer_requirement_details.no_boxs, customer_requirement_details.qty_per_box,customer_requirement_details.product_id, customer_requirement_details.product_name as productname, customer_requirements.shipment_weight, customer_requirements.shipment_volume, customer_requirements.shipment_size_type, customer_requirements.incoterms, customer_requirements.port_of_origin, customer_requirements.goods_ready_date, amazon_inventories.product_name, user_infos.company_name, customer_requirements.created_at, customer_requirements.is_activated, customer_requirements.id, customer_requirements.shipment_notes, customer_requirements.units, shipment_types.id as shipment_type_id, shipment_types.short_name as shipment_type_short_name, shipment_types.name as shipment_type_name, shipment_types.rate as shipment_type_rate, incoterms.short_name as incoterm_short_name, incoterms.name as incoterm_name')
            ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
            ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id','left')
            ->leftjoin('user_infos','user_infos.user_id','=','customer_requirements.user_id')
            ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type')
            ->join('incoterms','incoterms.id','=','customer_requirements.incoterms')
            ->where('customer_requirements.id',$id)
            ->get();
        $quote_method = Shipping_method::selectRaw('shipping_methods.shipping_name, shipping_methods.shipping_method_id')
            ->join('customer_requirement_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
            ->where('customer_requirement_methods.customer_requirement_id',$id)
            ->get();
        $shipping_charges = Shipping_charges::selectRaw('charges.name, shipping_charges.*')
                            ->join('charges','charges.id','=','shipping_charges.charge_id')
                            ->get();
        $shipping_quote = Shipping_quote::selectRaw('shipping_quotes.*')
                            ->where('customer_requirement_id',$id)
                            ->where('is_activated','0')
                            ->get()->toArray();
        $shipping_quote_charge = Shipping_quote_charge::selectRaw('shipping_quote_charges.*, shipping_quotes.shipping_method_id')
            ->join('shipping_quotes','shipping_quote_charges.shipping_quote_id','=','shipping_quotes.id')
            ->where('shipping_quotes.customer_requirement_id',$id)
            ->get();
        $shipping_method_detail = Customer_requirement_method::selectRaw('customer_requirement_methods.shipping_method_id')
                                ->join('customer_requirements','customer_requirements.id','=','customer_requirement_methods.customer_requirement_id')
                                ->where('customer_requirements.id',$id)
                                ->get()->toArray();
        $shipping_method_detail = array_map('current',$shipping_method_detail);
        $shipping_method = Shipping_method::whereNotIn('shipping_method_id',$shipping_method_detail)->get();
        return view('shipper.shippingquote')->with(compact('quote_request', 'charges', 'quote_method', 'user', 'title','shipping_charges','shipping_quote','shipping_quote_charge','shipping_method'));
    }
    // to add shipping quote form details
    public function addshippingquoteform(Request $request){
        $today=Carbon::now();
        $expire_date=$today->addDays(7);
        $count = $request->input("counting");
        $update=0;
        for($cnt=1;$cnt<$count;$cnt++) {
            $total_freight_price=0;
            $shipping_quote = array("customer_requirement_id" => $request->input('customer_requirement_id'),
                "shipping_method_id" => $request->input('method' . $cnt),
                "shipping_port" => $request->input('port' . $cnt),
                "freight_fee" => $request->input('freight_fee' . $cnt),
                "additional_note" => $request->input('additional_note' . $cnt),
                "chargeable_weight" => $request->input('charge_weight' . $cnt),
            );
            $shipping_quotes = new Shipping_quote($shipping_quote);
            $shipping_quotes->save();
            $last_id = $shipping_quotes->id;
            $total_freight_price+=$request->input('freight_fee' . $cnt);
            $sub_count = $request->input("cnt" . $cnt);
            for ($sub_cnt = 1; $sub_cnt < $sub_count; $sub_cnt++) {
                if (!empty($request->input("charge_price" . $cnt . "_" . $sub_cnt))) {
                    $shipping_quote_charges = array('shipping_quote_id' => $last_id,
                        'charge_id' => $request->input("charges" . $cnt . "_" . $sub_cnt),
                        'charge' => $request->input("charge_price" . $cnt . "_" . $sub_cnt)
                    );
                    $shipping_quote_charge = new Shipping_quote_charge($shipping_quote_charges);
                    $shipping_quote_charge->save();
                    $total_freight_price+=$request->input("charge_price" . $cnt . "_" . $sub_cnt);
                }
            }
            Shipping_quote::where('id',$last_id)->update(array('total_freight_price'=>$total_freight_price));
            if (!empty($request->input('shipping_quote_id' . $cnt))) {
                $old_shipping_quote = array('is_activated' => '3');
                Shipping_quote::where('id', $request->input('shipping_quote_id' . $cnt))->update($old_shipping_quote);
                $update=1;
            }
        }
        if($update==0)
        $customer_requirement = array("expire_date" => $expire_date);
        else
        $customer_requirement = array("expire_date" => $expire_date,"is_activated"=>'4');
        if($request->input('is_activated')=='5')
            $customer_requirement = array_merge($customer_requirement,array('is_activated'=>'6'));
        Customer_requirement::where('id',$request->input('customer_requirement_id'))->update($customer_requirement);
        QuoteHistory::create([
            'quote_history_status' => 2,
            'customer_requirement_id' => $request->customer_requirement_id
        ]);
        return redirect('shipper/quotelist')->with('success', 'Shipping Quote Submitted Successfully!');
    }
    public function getnewmethod(Request $request){
        if ($request->ajax()) {
            $post = $request->all();
            $id= $post['customer_requirement_id'];
            $method_id = $post['shipping_method_id'];
            $shipping_method = Shipping_method::where('shipping_method_id',$method_id)->get();
            $shipping_charges = Shipping_charges::selectRaw('charges.name, shipping_charges.*')
                ->join('charges', 'charges.id', '=', 'shipping_charges.charge_id')
                ->where('shipping_charges.shipping_method_id',$method_id)
                ->get();
        }
        echo json_encode(array("shipping_method"=>$shipping_method, "shipping_charges"=>$shipping_charges));
    }
    public function getshipmentdetail(Request $request){
        if($request->ajax()) {
            $post = $request->all();
            $id=$post['id'];
            $detail = Customer_requirement::selectRaw('customer_requirements.id,customer_requirements.shipment_weight,customer_requirements.shipment_volume,user_infos.contact_fname,user_infos.contact_lname,user_infos.contact_phone,user_infos.contact_email,user_infos.company_address,user_infos.company_address2,user_infos.company_city,user_infos.company_state,user_infos.company_country,suppliers.contact_name,suppliers.phone_number,suppliers.email,customer_requirement_details.no_boxs,customer_requirement_details.total,incoterms.short_name as incoterm_short_name')
                      ->join('orders','orders.order_id','=','customer_requirements.order_id')
                      ->join('user_infos','user_infos.user_id','=','orders.user_id')
                      ->join('suppliers','suppliers.supplier_id','=','customer_requirements.supplier')
                      ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                      ->join('incoterms','incoterms.id','=','customer_requirements.incoterms')
                      ->where('customer_requirements.id',$id)
                      ->get()->toArray();
        }
        echo json_encode(array("shipmentdetail"=>$detail));
    }
    public function getquotedetail(Request $request){
        if($request->ajax()) {
            $post = $request->all();
            $id=$post['id'];
            $detail = array();
        }
        echo json_encode(array("quotedetail"=>$detail));
    }
    public function shipper_status(Request $request){
        if($request->ajax()) {
            $post = $request->all();
            $order_id=$post['order_id'];
            $status = $post['status'];
            $arr = array("shipper_status"=>$status);
            Order::where('order_id',$order_id)->update($arr);
            $data = [
                'order_history_status' => $status,
                'order_id' => $order_id
            ];
            OrderHistory::create($data);
        }
    }
    public function updatedetail(Request $request){
       $order_id=$_POST['order_id'];
       $sbnumber=$_POST['sbnumber'];
       $remark=$_POST['remark'];
       if($sbnumber!='') {
           $arr = array("sb_number"=>$sbnumber);
           Order::where('order_id',$order_id)->update($arr);
       }
       else if($remark!='') {
           $arr = array("remark"=>$remark);
           Order::where('order_id',$order_id)->update($arr);
       }
       return redirect('order');
    }
    public function viewShipmentPage(){
        $order = Order::selectRaw('orders.*,user_infos.company_name,amazon_inventories.product_name,customer_requirements.port_of_origin,customer_requirements.goods_ready_date,customer_requirements.id,customer_requirements.port_of_origin,customer_requirements.shipper_destination,prealert_details.ETD_china,prealert_details.ETA_US,prealert_details.created_at as prealert_details_ctreated_at,shipping_methods.shipping_name')
                ->join('user_infos','user_infos.user_id','=','orders.user_id')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type')
                ->join('prealert_details','prealert_details.order_id','=','orders.order_id','left')
                ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id','left')
                ->orderBy('order_id','desc')
                ->where('orders.is_activated','<=','6')
                ->where('customer_requirement_methods.is_activated',1)
                ->get()->toArray();
        $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('shipper.index')->with(compact('orderStatus','order'));
    }
    public function viewShipment($order_id){
        $order = Order::selectRaw('orders.*,user_infos.company_name,user_infos.contact_phone,user_infos.contact_email,user_infos.company_address,amazon_inventories.product_name,amazon_inventories.product_nick_name,customer_requirement_details.product_id,customer_requirement_details.total,customer_requirement_details.no_boxs,customer_requirements.shipment_weight,customer_requirements.shipment_volume,suppliers.company_name as supplier_company_name,suppliers.email as supplier_email,suppliers.phone_number as supplier_phone_number,suppliers.city as supplier_city, incoterms.name as incoterm_name,customer_requirements.incoterms,shipping_methods.shipping_name,customer_requirements.id as customer_requirement_id,customer_requirements.units as customer_requirement_units,customer_requirements.port_of_origin,customer_requirements.shipper_destination,prealert_details.ETD_china,prealert_details.ETA_US,customer_requirements.order_type')
                        ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                        ->join('user_infos','user_infos.user_id','=','orders.user_id')
                        ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                        ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                        ->join('suppliers','suppliers.supplier_id','=','customer_requirements.supplier','left')
                        ->join('incoterms','incoterms.id','=','customer_requirements.incoterms','left')
                        ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                        ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id')
                        ->join('prealert_details','prealert_details.order_id','=','orders.order_id','left')
                        ->where('orders.order_id',$order_id)
                        ->first();
        $shipping_quote_charges = Shipping_quote::selectRaw('shipping_quote_charges.*,shipping_quotes.is_activated,charges.name')
                                    ->join('customer_requirements','customer_requirements.id','=','shipping_quotes.customer_requirement_id')
                                    ->join('shipping_quote_charges','shipping_quote_charges.shipping_quote_id','=','shipping_quotes.id')
                                    ->join('charges','charges.id','=','shipping_quote_charges.charge_id')
                                    ->where('customer_requirements.id',$order['customer_requirement_id'])
                                    ->where('shipping_quotes.is_activated','1')
                                    ->get();
        $orderStatus = array('Order Received','Supplier Contacted, Shipment Confirmed','Bill of Lading','Bill of Lading Approved','Shipment Booked','Shipment Departed','Shipment Arrived, On Vessel','Telex Release','Shipment Off Vessel','Awaiting Arrival at Port','Shipping Hold at CFS','Customs Cleared','Shipment Held for Customs Exam','Shipment Deconsolidation at CFS','Shipment at CFS','Delivery Booked','Awaiting Arrival at FBAforward','Received at Warehouse, Processing','Prep Submit','Warehouse Processing Complete','Shipping label Print','Released to Amazon, Complete','Storage');
        return view('shipper.view_shipment')->with(compact('order','shipping_quote_charges','orderStatus'));
    }
    public function createShipment(Request $request){
        $shipment = array();
        if($request->cargo_ready_date){
            $shipment = array_merge($shipment, ['cargo_ready_date' =>  \Carbon\Carbon::parse($request->cargo_ready_date)->format('Y-m-d h:i:s')]);
        }
        if($request->closing_date){
            $shipment = array_merge($shipment, ['closing_date' =>  \Carbon\Carbon::parse($request->closing_date)->format('Y-m-d h:i:s')]);
        }
        if($request->carrier){
            $shipment = array_merge($shipment, ['carrier' => $request->carrier]);
        }
        if($request->containers){
            $shipment = array_merge($shipment, ['containers' => $request->containers]);
        }
        if($request->container_height){
            $shipment = array_merge($shipment, ['container_height' => $request->container_height]);
        }
        if($request->container_width){
            $shipment = array_merge($shipment, ['container_width' => $request->container_width]);
        }
        if($request->notes){
            $shipment = array_merge($shipment, ['notes' => $request->notes]);
        }
        if($request->order_id){
            $shipment = array_merge($shipment, ['order_id' => $request->order_id]);
        }
        ShipperShipment::create($shipment);
        $order_data = array();
        if($request->sb_number){
            $order_data = array_merge($order_data, ['sb_number' => $request->sb_number]);
        }
        if($request->tracking){
            $order_data = array_merge($order_data, ['tracking_number' => $request->tracking]);
        }
        Order::where('order_id',$request->order_id)->update($order_data);
        $prealert_data = array();
        if($request->etd){
            $prealert_data = array_merge($prealert_data, ['ETD_china' => \Carbon\Carbon::parse($request->etd)->format('Y-m-d h:i:s')]);
        }
        if($request->eta){
            $prealert_data = array_merge($prealert_data, ['ETA_US' => \Carbon\Carbon::parse($request->eta)->format('Y-m-d h:i:s')]);
        }
        Prealert_detail::where('order_id',$request->order_id)->update($prealert_data);
        return redirect('shipper/shipment');
    }
    public function invoiceDashboard(){
        $orders = Order::selectRaw('orders.*,user_infos.company_name,amazon_inventories.product_name,customer_requirements.port_of_origin,customer_requirements.goods_ready_date,customer_requirements.id,customer_requirements.port_of_origin,customer_requirements.shipper_destination,prealert_details.ETD_china,prealert_details.ETA_US,prealert_details.created_at as prealert_details_ctreated_at,shipping_methods.shipping_name,customer_requirement_details.total,customer_requirement_details.no_boxs,customer_requirements.shipment_weight,customer_requirements.shipment_volume,shipping_quotes.chargeable_weight')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('user_infos','user_infos.user_id','=','orders.user_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type')
                ->join('prealert_details','prealert_details.order_id','=','orders.order_id','left')
                ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id','left');
                $orders->leftjoin('shipping_quotes',function($join) {
                    $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                    $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
                });
                $orders->orderBy('orders.order_id','desc')
                ->where('orders.is_activated','>=','0')
                ->groupby('orders.order_id')
                ->where('customer_requirement_methods.is_activated','1');
                $orders = $orders->get();
        return view('shipper.invoice_dashboard')->with(compact('orders'));
    }
    public function viewInvoice($order_id){
        $orders = Order::selectRaw('orders.*,user_infos.company_name,amazon_inventories.product_name,customer_requirements.port_of_origin,customer_requirements.goods_ready_date,customer_requirements.id as customer_requirement_id,customer_requirements.port_of_origin,customer_requirements.shipper_destination,prealert_details.ETD_china,prealert_details.ETA_US,prealert_details.created_at as prealert_details_ctreated_at,shipping_methods.shipping_name,shipping_methods.shipping_method_id,customer_requirement_details.total,customer_requirement_details.no_boxs,customer_requirement_details.qty_per_box,customer_requirements.shipment_weight,customer_requirements.shipment_volume,shipping_quotes.chargeable_weight,shipment_types.rate as shipment_type_rate,customer_requirements.order_type,shipment_types.trucking_rate,outbound_methods.outbound_method_id,shipment_types.container_unload_rate,customer_requirements.units,shipment_types.id as shipment_type_id')
                ->join('customer_requirements','customer_requirements.order_id','=','orders.order_id')
                ->join('user_infos','user_infos.user_id','=','orders.user_id')
                ->join('customer_requirement_details','customer_requirement_details.customer_requirement_id','=','customer_requirements.id')
                ->join('amazon_inventories','amazon_inventories.id','=','customer_requirement_details.product_id')
                ->join('shipment_types','shipment_types.id','=','customer_requirements.shipment_size_type')
                ->join('prealert_details','prealert_details.order_id','=','orders.order_id','left')
                ->join('customer_requirement_methods','customer_requirement_methods.customer_requirement_id','=','customer_requirements.id')
                ->join('outbound_methods','outbound_methods.outbound_method_id','=','customer_requirements.outbound_method_id')
                ->join('shipping_methods','shipping_methods.shipping_method_id','=','customer_requirement_methods.shipping_method_id','left');
                $orders->leftjoin('shipping_quotes',function($join) {
                    $join->on('shipping_quotes.customer_requirement_id','=','customer_requirements.id');
                    $join->on('shipping_quotes.shipping_method_id','=','customer_requirement_methods.shipping_method_id');
                });
                $orders->orderBy('orders.order_id','desc')
                ->where('orders.is_activated','>=','0')
                ->where('customer_requirement_methods.is_activated',1)
                ->where('orders.order_id',$order_id);
                $order = $orders->first();
                $detail_charge = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
                    ->join('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id')
                    ->join('charges','charges.id','=','shipping_quote_charges.charge_id')
                    ->where('shipping_quotes.customer_requirement_id',$order['customer_requirement_id'])
                    ->where('shipping_quotes.shipping_method_id',$order['shipping_method_id'])
                    ->where('shipping_quotes.is_activated','0')
                    ->get();
                $detail_prep = Prep_requirement:: selectRaw('prep_services.service_name, prep_services.price, prep_requirements.*')
                    ->join('prep_services','prep_services.prep_service_id','=','prep_requirements.prep_id')
                    ->where('customer_requirement_id',$order['customer_requirement_id'])->get();
                $quote_history = QuoteHistory::where('order_id',$order['order_id'])->orderBy('id')->first();
                $additional_charges = Shipping_quote_charge::selectRaw('shipping_quote_charges.id, shipping_quote_charges.charge_id, shipping_quote_charges.charge, charges.name')
                    ->leftjoin('shipping_quotes','shipping_quotes.id','=','shipping_quote_charges.shipping_quote_id')
                    ->join('charges','charges.id','=','shipping_quote_charges.charge_id')
                    ->where('shipping_quotes.customer_requirement_id',$order['customer_requirement_id'])
                    ->get();
                $shipment_fee = Shipping_quote::where('customer_requirement_id',$order['customer_requirement_id'])
                ->where('shipping_method_id',$order['shipping_method_id'])
                ->first();
        return view('shipper.view_invoice')->with(compact('order','detail_charge','detail_prep','quote_history','shipping_charges','quote_method','charge_ids','additional_charges','shipment_fee'));
    }
    public function updateInvoice(Request $request){
        QuoteHistory::create([
            'customer_requirement_id' => $request->customer_requirement_id,
            'units' => $request->old_units,
            'cartons' => $request->old_cartons,
            'cbm' => $request->old_cbm,
            'weight' => $request->old_weight,
            'chargable_weight' => $request->old_chargable_weight,
            'order_id' => $request->order_id
        ]);
        if($request->cartons){
            Customer_requirement_detail::where('customer_requirement_id',$request->customer_requirement_id)
                                    ->update([ 'no_boxs' => $request->cartons,
                                                'total' => ($request->cartons * $request->qty_per_box) ]);
        }
        $customer_requirement = array();
        if($request->cbm){
            $customer_requirement = array_merge($customer_requirement,['shipment_volume' => $request->cbm]);
        }
        if($request->weight){
            $customer_requirement = array_merge($customer_requirement,['shipment_weight' => $request->weight]);
        }
        if($customer_requirement){
            Customer_requirement::where('id',$request->customer_requirement_id)
                                    ->update($customer_requirement);
        }
        if($request->chargeable_weight){
            Shipping_quote::where('customer_requirement_id',$request->customer_requirement_id)->where('shipping_method_id',$request->shipping_method_id)->update(['chargeable_weight' => $request->chargeable_weight]);
        }
        return back();
    }
}