<?php

namespace App;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model as Eloquent;

class OrderHold extends Eloquent
{

    protected $table = 'order_holds';
    protected $fillable = ['order_id','hold_id','sub_hold_id','note','is_deleted','fee'];

}