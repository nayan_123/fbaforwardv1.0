<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_document extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['order_id','document','due_date','document_id'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}