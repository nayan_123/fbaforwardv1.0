@extends('layouts.member.app')
@section('title', 'Dashboard')
@section('css')
{!! Html::style('assets/dist/css/settings.css') !!}
<style>
 .head { margin-left: 20%; font-size: 23px;  color: #7d7f80; padding-top: 1%; display: inline-block;  }
 .nav-tabs { border-bottom: none; }
 .disabled i{ color: #fff !important; }
 label { font-weight: 700 !important;color:#333;text-transform:uppercase;  }
 .custom-file-upload { font-weight: normal !important;text-transform: unset;}
 .item-title span, input { margin: 10px 0px !important; }
 a:hover{ color:#fff;cursor: pointer; }
 .form-group{ margin-bottom: 0px; }
 input { margin-bottom: 0px !important;margin-top:0px !important; }
 .padding-right-0{ padding-right: 0px; }
 a:hover{ color:#28a7e9; }
 .box-body { padding-bottom: 30px !important}
 .identity{ padding-left: 35%; }
 @media (max-width:1300px) and (min-width:1100px) { .identity, .identity:hover{ padding-left: 30%!important; } }
 .identity, .identity:hover{ padding-left: 30%!important;cursor: pointer; }
</style>
@endsection
@section('content')
@if($user->role_id=='3' && $user->first_login==0)
<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="wizard">
                <span class="head">Complete your profile now or save your work to finish later.</span>
                <div class="wizard-inner">
                    <div class="connecting-line"></div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active col-md-2"><a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1"><span class="round-tab"><i class="fa fa-circle" aria-hidden="true"></i></span><span class="round-text">COMPANY DETAILS</span></a></li>
                        <li role="presentation" class="disabled col-md-2"><a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2"><span class="round-tab"><i class="fa fa-circle" aria-hidden="true"></i></span><span class="round-text">BILLING</span></a></li>
                        <li role="presentation" class="disabled col-md-2"><a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3"><span class="round-tab"><i class="fa fa-circle" aria-hidden="true"></i></span><span class="round-text">AMAZON INTEGRATION</span></a></li>
                        <li role="presentation" class="disabled col-md-2"><a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="Step 4"><span class="round-tab"><i class="fa fa-circle" aria-hidden="true"></i></span><span class="round-text">AUTHORIZE</span></a></li>
                        <li role="presentation" class="disabled col-md-2"><a href="#step5" data-toggle="tab" aria-controls="step5" role="tab" title="Step 5"><span class="round-tab"><i class="fa fa-circle" aria-hidden="true"></i></span><span class="round-text">SERVICE AGREEMENT</span></a></li>
                        <li role="presentation" class="disabled col-md-2"><a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete"><span class="round-tab"><i class="fa fa-circle" aria-hidden="true"></i></span><span class="round-text">PROFILE COMPLETE!</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="wizard">
        <div role="form">
            <div class="tab-content">
                <div class="tab-pane active" role="tabpanel" id="step1">
                {!! Form::open(['url' => 'customer/adduser_details', 'method' => 'post', 'class' => 'form-horizontal', 'id'=>'company_form','name'=>'company_form', "enctype" => "multipart/form-data"]) !!}
                    <div class="row">
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-body">
                            <div class="col-md-12">
                        <h2>COMPANY DETAILS</h2>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="btype" class="control-label col-md-12"> TYPE OF BUSINESS </label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-12">

                                        <select name="btype" id="btype" class="select2 validate[required]" >
                                            <option value=""></option>
                                            @foreach($business_type as $btype)
                                                <option value="{{$btype->id}}" @if(!empty($user_details->primary_bussiness_type)) @if($user_details->primary_bussiness_type==$btype->id) selected @endif @endif>{{$btype->business_type_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="" class="control-label col-md-12"> LEGAL COMPANY NAME </label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-12">
                                        {!! Form::text('lcname', old('lcname',isset($user_details->company_name)?$user_details->company_name:null), ['class' => 'form-control validate[required]']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="" class="control-label col-md-12"> DOING BUSINESS AS </label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-12">
                                        {!! Form::text('business_as', old('business_as',(isset($user_details->business_as))?$user_details->business_as:null), ['class' => 'form-control validate[required]']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="address1" class="control-label col-md-12"> MAILING ADDRESS </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('address1', old('address1',isset($user_details->company_address)?$user_details->company_address:null), ['class' => 'form-control validate[required]','placeholder'=>'STREET ADDRESS']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="address2" class="control-label col-md-12"> </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('address2', old('address2',isset($user_details->company_address2)?$user_details->company_address2:null), ['class' => 'form-control','placeholder'=>'ADDRESS LINE 2']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="city" class="control-label col-md-12"> </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('city', old('city',isset($user_details->company_city)?$user_details->company_city:null), ['class' => 'form-control validate[required]','placeholder'=>'CITY']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="postal_code" class="control-label col-md-12"> </label>
                                        <div class="col-md-4 padding-right-0">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('postal_code', old('postal_code',isset($user_details->company_zipcode)?$user_details->company_zipcode:null), ['class' => 'form-control validate[required] custom[integer]','placeholder'=>'ZIP/POSTAL CODE']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('country', old('country',isset($user_details->company_country)?$user_details->company_country:null), ['class' => 'form-control validate[required]','placeholder'=>'COUNTRY']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="" class="control-label col-md-12" style="padding-left: 15px !important;"> COUNTRY OF BUSINESS REGISTRATION </label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-12">
                                    {!! Form::select('business_country', array_add($country, '','Please Select'), old('business_country',isset($user_details->business_country)?$user_details->business_country:null), ['class' => 'select2 validate[required]','onchange'=>'changeCountryWiseState(this.value)']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="select_state">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="" class="control-label col-md-12" style="padding-left: 15px !important;"> STATE OF BUSINESS REGISTRATION </label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                        {!! Form::select('business_state', array_add($states, '','Please Select'), old('business_state',isset($user_details->business_state)?$user_details->business_state:null), ['class' => 'select2 validate[required]']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="" class="control-label col-md-12"> COMPANY'S TAX ID <abbr title="COMPANY'S TAX ID"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-12">
                                        <select name="taxid" id="taxid" class="select2" onchange="gettax(this.value)">
                                            <option value=""></option>
                                            @foreach($taxes as $tax)
                                                <option value="{{$tax->id}}" @if(!empty($user_details)) @if($tax->id==$user_details->tax_id){{"selected"}}@endif @endif>{{$tax->tax_name}} @if(!empty($tax->short_name)) {{ "(".$tax->short_name.")" }} @endif</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group tax_div" id="ein_div" hidden>
                            <div class="col-md-12">
                                <label for="" class="control-label col-md-12"> UPLOAD EIN NOTICE </label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-6"> </div>
                                    <div class="input-group col-md-6">
                                        <input type="file" name="ein" id="ein" class="inputfile">
                                        <label for="ein" class="custom-file-upload btn btn-primary" style="padding-left: 20px !important; padding-right: 20px !important;"> Browse  </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group tax_div" id="ssn_div" hidden>
                            <div class="col-md-12">
                                <label for="" class="control-label col-md-12"> SSN (SOCIAL SECURITY NUMBER ) <abbr title="SOCIAL SECURITY NUMBER"><i class="fa fa-question-circle compulsary"></i></abbr> </label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-12">
                                        {!! Form::text('ssn', old('ssn',isset($user_details->ssn)?$user_details->ssn:null), ['class' => 'form-control  validate[required] custom[integer]']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group tax_div" id="itin_div" hidden>
                            <div class="col-md-12">
                                <label for="" class="control-label col-md-12"> UPLOAD ITIN NOTICE </label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-6"> </div>
                                    <div class="input-group col-md-6">
                                        <input type="file" name="itin" id="itin" class="inputfile">
                                        <label for="itin" class="custom-file-upload btn btn-primary" style="padding-left: 20px !important; padding-right: 20px !important;"> Browse  </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group tax_div" id="no_div" hidden>
                            <div class="col-md-12">
                                <label for="" class="control-label col-md-12"> DON'T HAVE A TAX ID ? </label>
                                <div class="col-md-12">
                                    <a name="contact_us" id="contact_us" class="button contact col-md-4" href="mailto:support@fbaforward.com">CONTACT US</a>
                                    <a name="learn_more" id="learn_more" class="button col-md-4">LEARN MORE</a>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-body">
                        <div class="col-md-12" style="margin-bottom: 3% !important;">
                            <h2>PRIMARY CONTACT</h2>
                            <div class="form-group">
                                {{--*/ $name = explode(' ',$user->name) /*--}}
                                {{--*/ $fname = $name[0] /*--}}
                                @if(isset($name[1]))
                                {{--*/ $lname = $name[1] /*--}}
                                @else
                                {{--*/ $lname='' /*--}}
                                @endif
                                <div class="col-md-12">
                                    <label for="name" class="control-label col-md-12">PRIMARY CONTACT NAME </label>
                                    <div class="col-md-6" style="padding-bottom: 0%;">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('fname', old('fname',(!empty($user_details->contact_fname)) ? $user_details->contact_fname  : $fname ), ['class' => 'form-control validate[required]','placeholder'=>'FIRST NAME']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding-bottom: 0%;">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('lname', old('lname',(!empty($user_details->contact_lname))?$user_details->contact_lname: $lname), ['class' => 'form-control validate[required]','placeholder'=>'LAST NAME']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="phone" class="control-label col-md-12">PRIMARY CONTACT PHONE NUMBER </label>
                                    <div class="col-md-4">
                                        <div class="input-group col-md-12">
                                        {!! Form::text('ccode', old('ccode',isset($user_details->contact_country_code)?$user_details->contact_country_code:null), ['class' => 'form-control validate[required] custom[integer]','placeholder'=>'COUNTRY CODE']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="input-group col-md-12">
                                        {!! Form::text('phone', old('phone',isset($user_details->contact_phone)?$user_details->contact_phone:null), ['class' => 'form-control validate[required] custom[integer]']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="email" class="control-label col-md-12">PRIMARY CONTACT EMAIL </label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                        {!! Form::email('email', old('email',isset($user_details->contact_email)?$user_details->contact_email: $user->email), ['class' => 'form-control validate[required] custom[email]']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2>ADDITIONAL CONTACTS</h2>
                            @if(!empty($user_details))
                                @if($user_details->secondary_contact_fname!='')
                                <div id="additional_div">
                                @else
                                <div id="additional_div" hidden>
                                @endif
                            @else
                                <div id="additional_div" hidden>
                            @endif
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="name" class="control-label col-md-12">ADDITIONAL CONTACT NAME </label>
                                            <div class="col-md-6" style="padding-bottom: 0%;">
                                                <div class="input-group col-md-12">
                                                    {!! Form::text('fname1', old('fname1',isset($user_details->secondary_contact_fname)?$user_details->secondary_contact_fname:null), ['class' => 'form-control ','placeholder'=>'FIRST NAME']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="padding-bottom: 0%;">
                                                <div class="input-group col-md-12">
                                                    {!! Form::text('lname1', old('lname1',isset($user_details->secondary_contact_lname)?$user_details->secondary_contact_lname:null), ['class' => 'form-control ','placeholder'=>'LAST NAME']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="phone" class="control-label col-md-12">ADDITIONAL CONTACT PHONE NUMBER </label>
                                            <div class="col-md-4">
                                                <div class="input-group col-md-12">
                                                    {!! Form::text('ccode1', old('ccode1',isset($user_details->secondary_contact_country_code)?$user_details->secondary_contact_country_code:null), ['class' => 'form-control ','placeholder'=>'COUNTRY CODE']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="input-group col-md-12">
                                                    {!! Form::text('phone1', old('phone1',isset($user_details->secondary_contact_phone)?$user_details->secondary_contact_phone:null), ['class' => 'form-control ']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="email" class="control-label col-md-12">ADDITIONAL CONTACT EMAIL </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    {!! Form::email('email1', old('email1',isset($user_details->secondary_contact_email)?$user_details->secondary_contact_email:null), ['class' => 'form-control ']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="additional_div1" hidden>
                                    <hr>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="name" class="control-label col-md-12">ADDITIONAL CONTACT NAME </label>
                                            <div class="col-md-6" style="padding-bottom: 0%;">
                                                <div class="input-group col-md-12">
                                                    {!! Form::text('fname2', old('fname2',isset($user_details->third_contact_fname)?$user_details->third_contact_fname:null), ['class' => 'form-control','placeholder'=>'FIRST NAME']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="padding-bottom: 0%;">
                                                <div class="input-group col-md-12">
                                                    {!! Form::text('lname2', old('lname2',isset($user_details->third_contact_lname)?$user_details->third_contact_lname:null), ['class' => 'form-control','placeholder'=>'LAST NAME']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="phone" class="control-label col-md-12">ADDITIONAL CONTACT PHONE NUMBER </label>
                                            <div class="col-md-4">
                                                <div class="input-group col-md-12">
                                                    {!! Form::text('ccode2', old('ccode2',isset($user_details->third_contact_country_code)?$user_details->third_contact_country_code:null), ['class' => 'form-control ','placeholder'=>'COUNTRY CODE']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="input-group col-md-12">
                                                    {!! Form::text('phone2', old('phone2',isset($user_details->third_contact_phone)?$user_details->third_contact_phone:null), ['class' => 'form-control ']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="email" class="control-label col-md-12">ADDITIONAL CONTACT EMAIL </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    {!! Form::email('email2', old('email2',isset($user_details->third_contact_email)?$user_details->third_contact_email:null), ['class' => 'form-control ']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div id="button_div" style="float: right; font-weight: bold;">
                                        <a onclick="addadditional()">+ ADD ADDITIONAL CONTACT</a>
                                    </div>
                                    <div class="col-md-12 buttons_div">
                                        <ul class="list-inline pull-right">
                                            <li><button type="button" class="next-step skipbutton">SKIP</button></li>
                                            <li><button type="button" class="darkbluebutton next-step" id="company_detail">SAVE AND CONTINUE</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                {!! Form::close() !!}
                </div>
                <div class="tab-pane" role="tabpanel" id="step2">
                    {!! Form::open(['url' => 'member/addcreditcarddetail', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'card_form','name'=>'card_form',"enctype" => "multipart/form-data"]) !!}
                    <div class="row">
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-body">
                                <div class="col-md-12">
                                <h2>CREDIT CARD</h2>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="nick_name" class="control-label col-md-12"> CARD NICKNAME (OPTIONAL) </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control" name="nick_name" id="nick_name" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="card_name" class="control-label col-md-12"> NAME ON CARD </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="card_name" id="card_name" value="" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="card_number" class="control-label col-md-12"> CARD NUMBER </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required, condRequired[creditCard]]" name="card_number" id="card_number" value="" onkeypress="return AvoidSpace(event)">
                                                <input type="hidden"  name="card_type" id="card_type" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 padding-right-0" style="padding-bottom: 0%;">
                                        <label for="exp_date" class="control-label col-md-12"> EXPIRATION DATE </label>
                                        <div class="col-md-6 padding-right-0">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required, custom[integer]]" name="exp_month" id="exp_month" value="" placeholder="MONTH" onkeypress="return AvoidSpace(event)" maxlength="2">
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-right-0">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required, custom[integer]]" name="exp_year" id="exp_year" value="" placeholder="YEAR" onkeypress="return AvoidSpace(event)" maxlength="4">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding: 0%;">
                                        <label for="security_code" class="control-label col-md-12"> SECURITY CODE </label>
                                        <div class="col-md-12" style="padding-bottom: 0%;">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required,condRequired[maxSize[4]]] custom[integer]" name="security_code" id="security_code" value="" maxlength="4" onkeypress="return AvoidSpace(event)">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="default" class="control-label col-md-12"> </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="checkbox" name="default" id="default" value="1"> PRIMARY PAYMENT METHOD
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="card_div" hidden>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="nick_name" class="control-label col-md-12"> CARD NICKNAME </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control" name="nick_name1" id="nick_name1" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="card_name" class="control-label col-md-12"> NAME ON CARD </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required]" name="card_name1" id="card_name1" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="card_number" class="control-label col-md-12"> CARD NUMBER </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required, condRequired[creditCard]]" name="card_number1" id="card_number1" value="" onkeypress="return AvoidSpace(event)">
                                                    <input type="hidden"  name="card_type1" id="card_type1" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 padding-right-0" style="padding-bottom: 0%;">
                                            <label for="exp_date" class="control-label col-md-12"> EXPIRATION DATE </label>
                                            <div class="col-md-6 padding-right-0">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required, custom[integer]]" name="exp_month1" id="exp_month1" value="" placeholder="MONTH" onkeypress="return AvoidSpace(event)" maxlength="2">
                                                </div>
                                            </div>
                                            <div class="col-md-6 padding-right-0">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[requiredm, custom[integer]]" name="exp_year1" id="exp_year1" value="" placeholder="YEAR" onkeypress="return AvoidSpace(event)" maxlength="4">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-left-0" style="padding-bottom: 0%;">
                                            <label for="security_code" class="control-label col-md-12"> SECURITY CODE </label>
                                            <div class="col-md-12" style="padding-bottom: 0%;">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required,condRequired[maxSize[4]]]" name="security_code1" id="security_code1" value="" maxlength="4" onkeypress="return AvoidSpace(event)">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="default" class="control-label col-md-12"> </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <input type="checkbox" name="default1" id="default1" value="1"> PRIMARY PAYMENT METHOD
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div id="card_button_div" style="float: right; font-weight: bold;">
                                            <a onclick="addcard()">+ ADD ADDITIONAL CREDIT CARD</a>
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($card_detail))
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="" class="control-label col-md-12">Previous Cards</label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <select name="cards" id="cards" class="form-control" onchange="getcarddetail(this.value)">
                                                        <option value="">Select Creadit Card</option>
                                                        @foreach($card_detail as $details)
                                                            <option value="{{ $details['id'] }}">{{ $details['credit_card_number'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div id="card_detail_div">
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-body">
                            <div class="col-md-12">
                            <h2 style="float:left;">BILLING ADDRESS </h2> <span style="float:right; margin-top: 10%"><input type="checkbox" name="com_address" id="com_address" value="1"> SAME AS COMPANY ADDRESS</span>
                            {{--*/ $address1='' /*--}} {{--*/ $address2='' /*--}} {{--*/ $city='' /*--}} {{--*/ $postal_code='' /*--}} {{--*/ $country='' /*--}} {{--*/ $country_code='' /*--}} {{--*/ $phone_number='' /*--}}
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> COMPANY ADDRESS </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="caddress1" id="caddress1" value="{{ $address1 }}" placeholder="STREET ADDRESS">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control" name="caddress2" id="caddress2" value="{{$address2}}" placeholder="ADDRESS LINE 2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="ccity" id="ccity" value="{{$city}}" placeholder="CITY">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> </label>
                                        <div class="col-md-4">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required] custom[integer]" name="cpostal_code" id="cpostal_code" value="{{ $postal_code }}" placeholder="ZIP/POSTAL CODE">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="ccountry" id="ccountry" value="{{$country}}" placeholder="COUNTRY">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="cphone" class="control-label col-md-12">BILLING PHONE NUMBER </label>
                                        <div class="col-md-4">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="com_ccode" id="com_ccode" value="{{$country_code}}" placeholder="COUNTRY CODE">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required] custom[integer]" name="cphone" id="cphone" value="{{$phone_number}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class=" col-md-12 buttons_div">
                                        <ul class="list-inline pull-right">
                                            <li><button type="button" class="next-step skipbutton">SKIP</button></li>
                                            <li><button type="button" class="darkbluebutton next-step" id="card_detail">SAVE AND CONTINUE</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="tab-pane" role="tabpanel" id="step3">
                    {!! Form::open(['url' =>  '/amazon_credential', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'amazon_form','name'=>'amazon_form',"enctype" => "multipart/form-data"]) !!}
                    @if(!empty($customer_amazon_detail) && $customer_amazon_detail->mws_seller_id !='')
                    {{--*/$seller_id=decrypt($customer_amazon_detail->mws_seller_id) /*--}} {{--*/$auth_token=decrypt($customer_amazon_detail->mws_authtoken) /*--}} {{--*/$market_place_id=$customer_amazon_detail->market_place_id /*--}}
                    @else
                    {{--*/ $seller_id=''/*--}} {{--*/$auth_token=''/*--}} {{--*/$market_place_id=''/*--}}
                    @endif
                    <div class="row">
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-body">
                                <h3>HOW TO CONNECT AMAZON<br>MARKETPLACE WITH FBAFORWARD</h3>
                                <div style="color:#7d7f80; font-size:15px">
                                    <ol>
                                        <li>Go to <a href="http://developer.amazonservices.com" target="_blank">http://developer.amazonservices.com</a> and Click "Sign up for MWS"</li>
                                        <li>Log in with your Amazon seller username and password</li>
                                        <li>Select "I want to give a developer access to my Amazon seller account with MWS"<br>
                                        <span style="margin-left: 5%">Developer's Name: FBAFORWARD</span><br>
                                        <span style="margin-left: 5%">Developer ID: 3803-7002-6808</span>
                                        </li>
                                        <li>Click "Next", accept the license agreement click "Next" again</li>
                                        <li>You will be given unique identifiers.You will need the <b>Seller ID</b> and <b>MWS Auth Token.</b></li>
                                        <li>Come back to this page and enter the <b>Seller ID</b> and <b>MWS Auth Token</b> into the appropriate fields</li>
                                    </ol>
                                </div>
                                <h4 style=" color:#28a7e9; font-size:15px; ">Why do I have to do this?</h4>
                                <div>&nbsp;</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-body">
                                <div class="col-md-12">
                                <h2>AMAZON INTEGRATION SETUP</h2>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> AMAZON SELLER ID </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="seller_id" id="seller_id" value=" {{ $seller_id }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> MWS AUTH TOKEN </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="auth_token" id="auth_token" value="{{ $auth_token }}">
                                                <input type="hidden" name="market_place" id="market_place" value="{{ isset($market_place_id) ? $market_place_id : null }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> </label>
                                        <div class="col-md-12">
                                            <input type="button" name="verify" id="verify" value="VERIFY ACCOUNT" class="button">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 buttons_div">
                                        <ul class="list-inline pull-right">
                                            <li><button type="button" class="next-step skipbutton">SKIP</button></li>
                                            <li><button type="button" class="darkbluebutton next-step" id="amazon_detail">SAVE AND CONTINUE</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="tab-pane" role="tabpanel" id="step4">
                    {!! Form::open(['url' =>  '/amazon_credential', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'attorney_form','name'=>'attorney_form',"enctype" => "multipart/form-data"]) !!}
                        @if(!empty($attorney) && isset($attorney->name))
                        {{--*/$name= $attorney->name /*--}} {{--*/$title = $attorney->title/*--}} {{--*/$email= $attorney->email/*--}}
                        @else
                        {{--*/$name=''/*--}} {{--*/$title=''/*--}} {{--*/ $email='' /*--}}
                        @endif
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-body">
                                <div class="col-md-12">
                                <h3 style="">WHAT IS A POWER OF ATTORNEY <br> AND WHY DO I NEED TO SIGN ONE?</h3>
                                <div style="color:#7d7f80; font-size:15px;  margin-top: 5%; margin-left: 0%">
                                    <p>What is Power of Attorney (POA)?<br> It is the autherity to act on behalf of another person in specified matters.</p>
                                    <p>Why do I need one?<br>This gives FBAforward the legal ability to conduct customs business on behalf of the importer of Record (you!).(19CFR 141.46) </p>
                                    <p>Who can sign the POA ?<br>The person signing a power of attorney must have the power to bind an entity on behalf of which the person is signing. For corporations, an officer of the company must be sign. For LLCs, please refer here (https:/www.irs.gov/business/powers-of-attorney-for-llcs). For partnerships, one of the partners must sign. For sole proprietorships, it must be signed by the owner.</p>
                                </div>
                                <div>&nbsp;</div>
                                </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-body">
                                <div class="col-md-12">
                                <h2>POWER OF ATTORNEY</h2>
                                    <input type="checkbox" name="agree" id="agree" style="margin-left: 3%;" value="1" @if(!empty($attorney)) @if($attorney[0]['not_agree']=='1'){{ "checked" }} @endif @endif>  I WILL NOT BE USING FBAFORWARD'S SHIPPING OR CUSTOMS SERVICES
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> REPRESENTATIVE'S NAME </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="rep_name" id="rep_name" @if(count($attorney) != 0) value="{{ $attorney[0]['name'] }}" @endif @if(!empty($attorney)) @if($attorney[0]['not_agree']=='1'){{ "disabled" }} @endif @endif>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> REPRESENTATIVE'S TITLE </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="rep_title" id="rep_title" @if(count($attorney) != 0) value="{{ $attorney[0]['title'] }}" @endif @if(!empty($attorney)) @if($attorney[0]['not_agree']=='1'){{ "disabled" }} @endif @endif>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> REPRESENTATIVE'S EMAIL </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required] custom[email]" name="rep_email" id="rep_email" @if(count($attorney) != 0) value="{{ $attorney[0]['email'] }}" @endif @if(!empty($attorney)) @if($attorney[0]['not_agree']=='1'){{ "disabled" }} @endif @endif>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> UPLOAD PRIMARY SIGNER'S PHOTO IDENTIFICATION <abbr title="UPLOAD PRIMARY SIGNER'S PHOTO IDENTIFICATION"><i class="fa fa-question-circle compulsary"></i></abbr> </label>
                                        <div class="col-md-12">
                                            <div id="fileList" class="col-md-6"></div>
                                            <div class="input-group col-md-6">
                                                <input type="file" name="identity[]" id="identity" class="inputfile validate[required]" multiple onchange="javascript:updateList()" @if(!empty($attorney)) @if($attorney[0]['not_agree']=='1'){{ "disabled" }} @endif @endif>
                                                <label for="identity" class="button pull-right margin-right-0" style="padding-left: 20px !important; padding-right: 20px !important;"> Browse  </label>
                                            </div>
                                        </div>
                                    </div>
                                    @if(!empty($user_details))
                                        @if( $user_details->company_name!='' || $user_details->tax_id!='' || $user_details->primary_bussiness_type!='')
                                        <div class="col-md-12">
                                            <div id="attorney_div"></div>
                                            <div class="input-group col-md-12" style="padding-right:5px;">
                                                <a class="button pull-right" onclick="getattorney()">Prepare Power Of Attorney</a>
                                            </div>
                                        </div>
                                        @endif
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 buttons_div">
                                        <ul class="list-inline pull-right">
                                            <li><button type="button" class="next-step skipbutton">SKIP</button></li>
                                            <li><button type="button" class="darkbluebutton next-step" id="attorney_detail">SAVE AND CONTINUE</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="tab-pane" role="tabpanel" id="step5">
                    {!! Form::open(['url' =>  '/service', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'service_form','name'=>'attorney_form',"enctype" => "multipart/form-data"]) !!}
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <div class="col-md-12">
                                <h2>SERVICE AGREEMENT</h2>
                                <h3 class="text-left">TERMS & CONDITIONS</h3>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"></label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="checkbox" name="service_agree validate[required]" class="validate[minCheckbox[1]]" id="service_agree" required="true">  I have read and agree to the terms of service.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-9">
                                        <label for="" class="control-label col-md-12">ELECTRONIC SIGNATURE</label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" name="electronic_signature" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="" class="control-label col-md-12">DATE</label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" name="service_date" id="service_date" class="form-control validate[required] datepicker" placeholder='&#xf073;' style="font-family: FontAwesome, 'Open Sans', Verdana, sans-serif;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 buttons_div">
                                        <ul class="list-inline pull-right">
                                            <li><button type="button" class="darkbluebutton next-step" id="service_detail">SAVE AND CONTINUE</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="tab-pane" role="tabpanel" id="complete">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <h2 style="float: left; margin-top: 5%; margin-bottom:5%">CONGRATULATIONS, @if(!empty($user_details))({{ $user_details->company_name }})! @endif</h2>  <span style="float:left; font-size: 25px; color:#7d7f80;letter-spacing: 2px;margin-top: 5%; margin-bottom:5%">&nbsp;&nbsp;  YOUR PROFILE IS COMPLETE.</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <h5 style="font-size: 25px;color: #28a7e9;letter-spacing: 2px; margin-left:2%; margin-top: 5%">READY TO GET STARTED?</h5><br>
                                <div class="col-md-6">
                                    <div class="col-md-3 text-center"><img src="{{ asset('uploads/images/bookconsultation.png') }}" class="img"></div>
                                    <div class="col-md-9">
                                        <h4 class="h4">Book a Consultation</h4>
                                        <p style="font-size:13.5px;">Get a free 30-minute consultation. Speak to our knowledgable sales reps about how we can help your business.</p>
                                    </div>
                                    <div class="col-md-3 text-center"><img src="{{ asset('uploads/images/getquote.png') }}" class="img"></div>
                                    <div class="col-md-9">
                                        <h4 class="h4">Get a Quote</h4>
                                        <p style="font-size:13.5px;">Receive a quote in no time. Enter in some simple details about your shipment and goods. You can receive a quote on shipping, customs and/or general warehouse services.</p>
                                    </div>
                                    <div class="col-md-12 no-padding" style="margin-bottom:15px;">
                                    <div class="col-md-3 text-center"><img src="{{ asset('uploads/images/orders.png') }}" class="img"></div>
                                    <div class="col-md-9">
                                        <h4 class="h4">Complete Your Order</h4>
                                        <p style="font-size:13.5px;">Once you are happy with your quote, convert it to an order, pay, and we well begin working on your order.</p>
                                    </div>
                                    </div>
                                    <div class="col-md-3 text-center"><img src="{{ asset('uploads/images/wetakeover.png') }}" class="img"></div>
                                    <div class="col-md-9">
                                        <h4 class="h4">We Take Over!</h4>
                                        <p style="font-size:13.5px;">That's It!We may contact you regarding some customs details or prep specifications, but we take care of the heavy lifting while yoy work on growing your booming Amazon business.</p>
                                    </div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9"><a class="button" style="width:80%;margin-bottom:10px;" id="complete_settings">GOT IT! LET'S GO</a></div>
                                </div>
                                <div class="col-md-6">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
    </div>
<div class="clearfix"></div>
<div class="modal fade" id="learn_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Learn More</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" id="main">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@elseif($user->role_id=='3')
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="col-md-12 no-padding">
                    <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                        <li class="nav-item col-md-4 nav-tab no-padding active" id="tab1">
                            <a class="nav-link active" data-toggle="tab" href="#tabs1">Account</a>
                        </li>
                        <li class="nav-item col-md-4 nav-tab no-padding" id="tab2">
                            <a class="nav-link" data-toggle="tab" href="#tabs2">Company</a>
                        </li>
                        <li class="nav-item col-md-4 nav-tab no-padding" id="tab3">
                            <a class="nav-link" data-toggle="tab" href="#tabs3">Billing</a>
                        </li>
                        <li class="nav-item col-md-4 nav-tab no-padding" id="tab4">
                            <a class="nav-link" data-toggle="tab" href="#tabs4">Amazon Integration</a>
                        </li>
                        <li class="nav-item col-md-4 nav-tab no-padding" id="tab5">
                            <a class="nav-link" data-toggle="tab" href="#tabs5">Documents</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active in" id="tabs1" name="tabs" role="tabpanel" aria-labelledby="">
                            {!! Form::open(['url' =>  'settings', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'accountform']) !!}
                            <div class="form-group">
                                 <div class="col-md-12">
                                     <label for="" class="control-label col-md-12">Email</label>
                                     <div class="col-md-12">
                                         {{ $user->email }}<br>
                                         <a href="javascript:void(0)" id="email_label" onclick="showemaildiv()">Change Email</a>
                                        <div class="form-group" id="email_div" hidden>
                                            <div class="col-md-12">
                                                <label for="" class="control-label col-md-12"></label>
                                                <div class="col-md-12">
                                                    <div class="input-group col-md-4">
                                                        <input type="text" name="email" id="email" class="form-control validate[required,custom[email]]" placeholder="New Email">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                     </div>
                                 </div>
                             </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="" class="control-label col-md-12">Password</label>
                                    <div class="col-md-12">
                                        <a href="javascript:void(0)"  id="password_label" onclick="showpassdiv()">Change Password</a>
                                        <div class="form-group" id="password_div" hidden>
                                            <div class="col-md-12 no-padding">

                                                <div class="col-md-12">
                                                    <label for="" class="control-label col-md-12"></label>
                                                    <div class="col-md-12">
                                                        <div class="input-group col-md-4">
                                                            <input type="text" name="password" id="current_password" class="form-control validate[required]" placeholder="Current Password">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <label for="" class="control-label col-md-12"></label>
                                                    <div class="col-md-12">
                                                        <div class="input-group col-md-4">
                                                            <input type="password" name="new_password" id="new_password" class=" validate[required] form-control" placeholder="New Password">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <label for="" class="control-label col-md-12"></label>
                                                    <div class="col-md-12">
                                                        <div class="input-group col-md-4">
                                                            <input type="password" name="confirm_password" id="confirm_password" class="validate[required,equals[new_password]] form-control" placeholder="Re-Enter Password">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="notify" class="control-label col-md-12">Notification Settings</label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            <input type="checkbox" name="action_notify" id="action_notify" value="1" class="" @if(isset($user_details)) @if($user_details->action_notify == 1) checked @endif @endif> Action Alerts<br>
                                            <p class="notified">These are email notifications regarding an action you must take forFBAforward to continue processing your shipment. If you do not wish to get emails, you can still see the alerts within this portal. We highly recommend leaving this setting on.</p>
                                            <input type="checkbox" name="notificate_notify" id="notificate_notify" value="1" class="" @if(isset($user_details)) @if($user_details->notificate_notify == 1) checked @endif @endif> Notification Alerts<br>
                                            <p class="notified">These are email notifications regarding your shipment that do not require any action to be taken. The emails are sent to simply keep you updated of your shipment’s whereabouts. If you do not wish to get emails, you can still see the alerts within this portal.</p>
                                            <input type="checkbox" name="email_notify" id="email_notify" value="1" class="" @if(isset($user_details)) @if($user_details->email_notify == 1) checked  @endif @endif> Emails about new services, offers, reminders, Amazon news<br>
                                            <p class="notified">We will only send you emails that we think most apply to you, whether that means important company updates, shipping deadlines for upcoming holidays, cost savings, or FBA news.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group" id="password_div" hidden>
                                <div class="col-md-12">

                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12">Change Password</label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-4">
                                                <input type="text" name="password" id="current_password" class="form-control" placeholder="Current Password">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"></label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-4">
                                                <input type="password" name="new_password" id="new_password" class=" validate[required] form-control" placeholder="New Password">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"></label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-4">
                                                <input type="password" name="confirm_password" id="confirm_password" class="validate[required,equals[new_password]] form-control" placeholder="Re-Enter Password">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-10">
                                   <input type="submit" id="update_account" name="update_account" class="button" value=" Update ">
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>

                        <div class="tab-pane fade" id="tabs2" name="tabs" role="tabpanel" aria-labelledby="">
                    <div class="col-md-6">
                        <div class="">
                            <div class="box-body">
                                <h4>Company Detail</h4>
                            {!! Form::open(['url' =>  'settings', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'companyform']) !!}
                            <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                            <div class="form-group margin-bottom-0">
                                <div class="col-md-12">
                                    <label for="btype" class="control-label col-md-12"> TYPE OF BUSINESS </label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            <select name="btype" id="btype" class="select2 validate[required]" >
                                                <option value=""></option>
                                                @foreach($business_type as $btype)
                                                    <option value="{{$btype->id}}" @if(!empty($user_details->primary_bussiness_type)) @if($user_details->primary_bussiness_type==$btype->id) selected @endif @endif>{{$btype->business_type_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group margin-bottom-0">
                                <div class="col-md-12">
                                    <label for="company_name" class="control-label col-md-12">Legal Name</label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('company_name', old('company_name',isset($user_details->company_name)?$user_details->company_name:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group margin-bottom-0">
                                <div class="col-md-12">
                                    <label for="business_email" class="control-label col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('business_email', old('business_email',(isset($user_details->business_email))?$user_details->business_email:null), ['class' => 'form-control validate[required] no-margin custom[email]']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group margin-bottom-0">
                                <div class="col-md-12">
                                    <label for="business_phone" class="control-label col-md-12">Phone</label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('business_phone', old('business_phone', (isset($user_details->business_phone))?$user_details->business_phone:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group margin-bottom-0">
                                <div class="col-md-12">
                                    <label for="" class="control-label col-md-12"> DOING BUSINESS AS </label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('business_as', old('business_as',(isset($user_details->business_as))?$user_details->business_as:null), ['class' => 'form-control validate[required]']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group margin-bottom-0">
                                <div class="col-md-12">
                                     <div class="col-md-12">
                                        <label class=""></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('address1', old('address1',isset($user_details->company_address)?$user_details->company_address:null), ['class' => 'form-control validate[required] no-margin','placeholder'=>'STREET ADDRESS']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group margin-bottom-0">
                                <div class="col-md-12">
                                    <label for="address2" class="control-label col-md-12"></label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('address2', old('address2',isset($user_details->company_address2)?$user_details->company_address2:null), ['class' => 'form-control no-margin','placeholder'=>'ADDRESS LINE 2']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="city" class="control-label col-md-12"></label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('city', old('city',isset($user_details->company_city)?$user_details->company_city:null), ['class' => 'form-control validate[required] no-margin','placeholder'=>'CITY']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group margin-bottom-0">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label for="postal_code" class="control-label col-md-12 no-padding"></label>
                                        <div class="input-group col-md-12">
                                            {!! Form::text('postal_code', old('postal_code',isset($user_details->company_zipcode)?$user_details->company_zipcode:null), ['class' => 'form-control validate[required] no-margin','placeholder'=>'ZIP/POSTAL CODE']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <label for="postal_code" class="control-label col-md-12 no-padding"> </label>
                                        <div class="input-group col-md-12">
                                            {!! Form::text('country', old('country',isset($user_details->company_country)?$user_details->company_country:null), ['class' => 'form-control validate[required] no-margin','placeholder'=>'COUNTRY']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group margin-bottom-0">
                                <div class="col-md-12">
                                    <label for="" class="control-label col-md-12" style="padding-left: 15px !important;"> COUNTRY OF BUSINESS REGISTRATION</label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                        {!! Form::select('business_country', array_add($country, '','Please Select'), old('business_country',isset($user_details->business_country)?$user_details->business_country:null), ['class' => 'select2 validate[required]','onchange'=>'changeCountryWiseState(this.value)']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="select_state">
                                <div class="form-group margin-bottom-0">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12" style="padding-left: 15px !important;"> STATE OF BUSINESS REGISTRATION </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                            {!! Form::select('business_state', array_add($states, '','Please Select'), old('business_state',isset($user_details->business_state)?$user_details->business_state:null), ['class' => ' select2 validate[required]']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group margin-bottom-0">
                                <div class="col-md-12">
                                    <label for="" class="control-label col-md-12"> TAX ID </label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            <select name="taxid" id="taxid" class=" select2" onchange="gettax(this.value)">
                                                <option value=""></option>
                                                @foreach($taxes as $tax)
                                                    <option value="{{$tax->id}}" @if(!empty($user_details)) @if($tax->id==$user_details->tax_id) selected @endif @endif>{{$tax->tax_name}} @if(!empty($tax->short_name)) {{ "(".$tax->short_name.")" }} @endif</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group tax_div" id="ein_div" hidden>
                                        <div class="col-md-12">
                                            <label for="" class="control-label col-md-12"> UPLOAD EIN NOTICE </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-6"> </div>
                                                <div class="input-group col-md-6">
                                                    <input type="file" name="ein" id="ein" class="inputfile">
                                                    <label for="ein" class="custom-file-upload btn btn-primary" style="padding-left: 20px !important; padding-right: 20px !important;"> Browse  </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <h4>EIN</h4>
                                        </div>                            
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Ein</th>
                                                    <th>View</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th> {{ $ein_notice ? $ein_notice->file : '' }}</th>
                                                    <th><a @if($ein_notice) @if(file_exists( public_path().'/uploads/ein_notice/'.$ein_notice->file)) target="_blank" href="{{ url('uploads/ein_notice',isset($ein_notice->file) ? $ein_notice->file : '') }}" @else href="#" @endif @endif>View</a></th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group tax_div" id="ssn_div" hidden>
                                        <div class="col-md-12">
                                            <label for="" class="control-label col-md-12"> SSN (SOCIAL SECURITY NUMBER ) <abbr title="SOCIAL SECURITY NUMBER"><i class="fa fa-question-circle compulsary"></i></abbr> </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    {!! Form::text('ssn', old('ssn',isset($user_details->ssn)?$user_details->ssn:null), ['class' => 'form-control  validate[required] custom[integer]']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group tax_div" id="itin_div" hidden>
                                        <div class="col-md-12">
                                            <label for="" class="control-label col-md-12"> UPLOAD ITIN NOTICE </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-6"> </div>
                                                <div class="input-group col-md-6">
                                                    <input type="file" name="itin" id="itin" class="inputfile">
                                                    <label for="itin" class="custom-file-upload btn btn-primary" style="padding-left: 20px !important; padding-right: 20px !important;"> Browse  </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group tax_div" id="no_div" hidden>
                                        <div class="col-md-12">
                                            <label for="" class="control-label col-md-12"> DON'T HAVE A TAX ID ? <abbr title="SDON'T HAVE A TAX ID ?"><i class="fa fa-question-circle compulsary"></i></abbr> </label>
                                            <div class="col-md-12">
                                                <a name="contact_us" id="contact_us" class="button contact" href="mailto:support@fbaforward.com">Contact Us</a>
                                                <a name="learn_more" id="learn_more" class="button">Learn More</a>
                                            </div>
                                            <br>
                                            <div class='col-md-12'>
                                                <div style="margin-top:10px;">
                                                    <input type="hidden" class="form-control" name="tex_id" value="{{ isset($user_details) ? $user_details->tax_id : null}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                        <div class="box-body">
                            <h4>Primary Contact</h4>
                            <div class="col-md-12 no-padding" style="margin-bottom:30px;">
                            <div class="form-group margin-bottom-0">
                                <div class="col-md-6">
                                    <label for="contact_fname" class="control-label col-md-12">First Name</label>
                                    <div class="col-md-12 padding-right-0">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('contact_fname', old('contact_fname',isset($user_details) ? $user_details->contact_fname : null), ['class' => 'form-control validate[required] no-margin']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 padding-left-0">
                                    <label for="contact_lname" class="control-label col-md-12">Last Name</label>
                                    <div class="col-md-12 padding-left-0">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('contact_lname', old('contact_lname',isset($user_details) ? $user_details->contact_lname : null), ['class' => 'form-control validate[required] no-margin']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group margin-bottom-0">
                                <div class="col-md-12">
                                    <div class="col-md-4 padding-right-0">
                                        <label for="ccode" class="control-label col-md-12 no-padding">Country Code</label>
                                        <div class="col-md-12 no-padding">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('ccode', old('ccode',isset($user_details->contact_country_code)?$user_details->contact_country_code:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 no-padding">
                                        <label for="primary_phone_number" class="control-label col-md-12">Phone Number</label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('primary_phone_number', old('primary_phone_number',isset($user_details->contact_phone)?$user_details->contact_phone:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="primary_email" class="control-label col-md-12">Email Address</label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('primary_email', old('primary_email',isset($user_details->contact_email)?$user_details->contact_email:null), ['class' => 'form-control validate[required] no-margin custom[email]']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(isset($user_details))
                            @if(!$user_details->secondary_contact_fname)
                            <div id="button_div" style="float: right; font-weight: bold;">
                                <a onclick="show_additional_section()">+ ADD ADDITIONAL CONTACT</a>
                            </div>
                            @endif
                        @endif

                            <div id="additional_section" class="additional_section  @if(isset($user_details)) @if(!$user_details->secondary_contact_fname) hide @endif @endif">
                                <h4 style="margin-top:30px;">Additional Contacts</h4>
                                <div class="form-group margin-bottom-0">
                                    <div class="col-md-6">
                                        <label for="secondary_contact_fname" class="control-label col-md-12">First Name</label>
                                        <div class="col-md-12 padding-right-0">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('secondary_contact_fname', old('secondary_contact_fname', isset($user_details->secondary_contact_fname) ? $user_details->secondary_contact_fname:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-left-0">
                                        <label for="secondary_contact_lname" class="control-label col-md-12">Last Name</label>
                                        <div class="col-md-12 padding-left-0">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('secondary_contact_lname', old('secondary_contact_lname',isset($user_details->secondary_contact_lname) ? $user_details->secondary_contact_lname:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group margin-bottom-0">
                                    <div class="col-md-12">
                                        <div class="col-md-4 padding-right-0">
                                            <label for="secondary_contact_country_code" class="control-label col-md-12 no-padding">Country Code</label>
                                            <div class="col-md-12 no-padding">
                                                <div class="input-group col-md-12">
                                                    {!! Form::text('secondary_contact_country_code', old('secondary_contact_country_code',isset($user_details->secondary_contact_country_code)?$user_details->secondary_contact_country_code:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 padding-right-0">
                                        <label for="secondary_phone_number" class="control-label col-md-12">Phone Number</label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('secondary_phone_number', old('secondary_phone_number',isset($user_details->secondary_contact_phone)?$user_details->secondary_contact_phone:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group margin-bottom-0">
                                    <div class="col-md-12">
                                        <label for="secondary_email" class="control-label col-md-12">Email Address</label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('secondary_email', old('secondary_email',isset($user_details->secondary_contact_email)?$user_details->secondary_contact_email:null), ['class' => 'form-control validate[required] custom[email] no-margin']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <hr/>
                                <div class="form-group margin-bottom-0">
                                    <div class="col-md-6">
                                        <label for="third_contact_fname" class="control-label col-md-12">First Name</label>
                                        <div class="col-md-12 padding-right-0">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('third_contact_fname', old('third_contact_fname',isset($user_details->third_contact_fname) ? $user_details->third_contact_fname:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-left-0">
                                        <label for="third_contact_lname" class="control-label col-md-12">Last Name</label>
                                        <div class="col-md-12 padding-left-0">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('third_contact_lname', old('third_contact_lname',isset($user_details->third_contact_lname) ? $user_details->third_contact_lname:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group margin-bottom-0">
                                    <div class="col-md-12">
                                        <div class="col-md-4 padding-right-0">
                                            <label for="third_contact_country_code" class="control-label col-md-12 no-padding">Country Code</label>
                                            <div class="col-md-12 no-padding">
                                                <div class="input-group col-md-12">
                                                    {!! Form::text('third_contact_country_code', old('third_contact_country_code',isset($user_details->third_contact_country_code)?$user_details->third_contact_country_code:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 padding-right-0">
                                        <label for="third_phone_number" class="control-label col-md-12">Phone Number</label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('third_phone_number', old('third_phone_number',isset($user_details->third_contact_phone)?$user_details->third_contact_phone:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group margin-bottom-0">
                                    <div class="col-md-12">
                                        <label for="third_email" class="control-label col-md-12">Email Address</label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('third_email', old('third_email',isset($user_details->third_contact_email)?$user_details->third_contact_email:null), ['class' => 'form-control validate[required] custom[email] no-margin']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2 col-md-offset-10">
                    <input type="submit" name="update_company" id="update_company" class="button" value="Update">
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="tab-pane fade" id="tabs3" name="tabs" role="tabpanel" aria-labelledby="">
            {!! Form::open(['url' =>  'creditcard', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'billform']) !!}
            <input type="hidden" name="add_card" value="add_card">
            <div class="col-md-6">
                        <div>
                            <div class="box-body no-padding">
                                <div class="col-md-12">
                                <h4>CREDIT CARD</h4>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="nick_name" class="control-label col-md-12"> CARD NICKNAME (OPTIONAL) </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control" name="nick_name" id="nick_name" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="card_name" class="control-label col-md-12"> NAME ON CARD </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="card_name" id="card_name" value="" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="card_number" class="control-label col-md-12"> CARD NUMBER </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required, condRequired[creditCard]]" name="card_number" id="card_number" value="" onkeypress="return AvoidSpace(event)">
                                                <input type="hidden"  name="card_type" id="card_type" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 padding-right-0" style="padding-bottom: 0%;">
                                        <label for="exp_date" class="control-label col-md-12"> EXPIRATION DATE </label>
                                        <div class="col-md-6 padding-right-0">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required, custom[integer]]" name="exp_month" id="exp_month" value="" placeholder="MONTH" onkeypress="return AvoidSpace(event)" maxlength="2">
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-right-0">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required, custom[integer]]" name="exp_year" id="exp_year" value="" placeholder="YEAR" onkeypress="return AvoidSpace(event)" maxlength="4">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="padding: 0%;">
                                        <label for="security_code" class="control-label col-md-12"> SECURITY CODE </label>
                                        <div class="col-md-12" style="padding-bottom: 0%;">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required,condRequired[maxSize[4]]] custom[integer]" name="security_code" id="security_code" value="" maxlength="4" onkeypress="return AvoidSpace(event)">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="default" class="control-label col-md-12"> </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="checkbox" name="default" id="default" value="1"> PRIMARY PAYMENT METHOD
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="card_div" hidden>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="nick_name" class="control-label col-md-12"> CARD NICKNAME </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control" name="nick_name1" id="nick_name1" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="card_name" class="control-label col-md-12"> NAME ON CARD </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required]" name="card_name1" id="card_name1" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="card_number" class="control-label col-md-12"> CARD NUMBER </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required, condRequired[creditCard]]" name="card_number1" id="card_number1" value="" onkeypress="return AvoidSpace(event)">
                                                    <input type="hidden"  name="card_type1" id="card_type1" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 padding-right-0" style="padding-bottom: 0%;">
                                            <label for="exp_date" class="control-label col-md-12"> EXPIRATION DATE </label>
                                            <div class="col-md-6 padding-right-0">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required, custom[integer]]" name="exp_month1" id="exp_month1" value="" placeholder="MONTH" onkeypress="return AvoidSpace(event)" maxlength="2">
                                                </div>
                                            </div>
                                            <div class="col-md-6 padding-right-0">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required, custom[integer]]" name="exp_year1" id="exp_year1" value="" placeholder="YEAR" onkeypress="return AvoidSpace(event)" maxlength="4">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-left-0" style="padding-bottom: 0%;">
                                            <label for="security_code" class="control-label col-md-12"> SECURITY CODE </label>
                                            <div class="col-md-12" style="padding-bottom: 0%;">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required,condRequired[maxSize[4]]]" name="security_code1" id="security_code1" value="" maxlength="4" onkeypress="return AvoidSpace(event)">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="default" class="control-label col-md-12"> </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <input type="checkbox" name="default1" id="default1" value="1"> PRIMARY PAYMENT METHOD
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div id="card_button_div" style="float: right; font-weight: bold;">
                                            <a onclick="addcard()">+ ADD ADDITIONAL CREDIT CARD</a>
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($card_detail))
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="" class="control-label col-md-12">Previous Cards</label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <select name="cards" id="cards" class="form-control" onchange="getcarddetail(this.value)">
                                                        <option value="">Select Creadit Card</option>
                                                        @foreach($card_detail as $details)
                                                            <option value="{{ $details['id'] }}">{{ $details['credit_card_number'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div id="card_detail_div">
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div>
                            <div class="box-body no-padding">
                            <div class="col-md-12">
                            <h4>BILLING ADDRESS </h4> <span style="float:right;"><input type="checkbox" name="com_address" id="com_address" value="1"> SAME AS COMPANY ADDRESS</span>
                            {{--*/ $address1='' /*--}} {{--*/ $address2='' /*--}} {{--*/ $city='' /*--}} {{--*/ $postal_code='' /*--}} {{--*/ $country='' /*--}} {{--*/ $country_code='' /*--}} {{--*/ $phone_number='' /*--}}
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> COMPANY ADDRESS </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="caddress1" id="caddress1" value="{{ $address1 }}" placeholder="STREET ADDRESS">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control" name="caddress2" id="caddress2" value="{{$address2}}" placeholder="ADDRESS LINE 2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="ccity" id="ccity" value="{{$city}}" placeholder="CITY">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> </label>
                                        <div class="col-md-4">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required] custom[integer]" name="cpostal_code" id="cpostal_code" value="{{ $postal_code }}" placeholder="ZIP/POSTAL CODE">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="ccountry" id="ccountry" value="{{$country}}" placeholder="COUNTRY">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="cphone" class="control-label col-md-12">BILLING PHONE NUMBER </label>
                                        <div class="col-md-4">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required]" name="com_ccode" id="com_ccode" value="{{$country_code}}" placeholder="COUNTRY CODE">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required] custom[integer]" name="cphone" id="cphone" value="{{$phone_number}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            </div>
                        </div>
                    </div>

            <div class="col-md-12">
                <br>
                @if(count($card_details) != 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nickname</th>
                            <th>Name on card</th>
                            <th>Credit card number</th>
                            <th>Credit type</th>
                            <th>Security code</th>
                        </tr>
                    </thead>
                @foreach($card_details as $card_detail)
                    <tbody>
                        <tr>
                            <td>{{ $card_detail->nick_name }}</td>
                            <td>{{ $card_detail->first_name.' '.$card_detail->last_name }}</td>
                            <td>{{ $card_detail->credit_card_number }}</td>
                            <td>{{ $card_detail->credit_card_type }}</td>
                            <td>{{ $card_detail->cvv }}</td>
                        </tr>
                    </tbody>
                @endforeach
                </table>
                @endif
            </div>

            <div class="form-group" style="margin-top:20px">
                <div class="col-md-4 col-md-offset-8">
                    <button type="submit" name="update_bill" id="update_bill" class="button pull-right" value="Update">Update</button>
                   <!--  <a class="button pull-right" data-toggle="modal" data-target="#addCardModal">Add Card</a> -->
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="tab-pane fade padding30" id="tabs4" name="tabs" role="tabpanel" aria-labelledby="">
        <div class="col-md-6">
            <div class="box-body wizard padding-left-0">
                <h3>HOW TO CONNECT AMAZON<br>MARKETPLACE WITH FBAFORWARD</h3>
                <div style="color:#7d7f80; font-size:15px">
                    <ol>
                        <li>Go to <a href="http://developer.amazonservices.com" target="_blank">http://developer.amazonservices.com</a> and Click "Sign up for MWS"</li>
                        <li>Log in with your Amazon seller username and password</li>
                        <li>Select "I want to give a developer access to my Amazon seller account with MWS"<br>
                        <span style="margin-left: 5%">Developer's Name: FBAFORWARD</span><br>
                        <span style="margin-left: 5%">Developer ID: 3803-7002-6808</span>
                        </li>
                        <li>Click "Next", accept the license agreement click "Next" again</li>
                        <li>You will be given unique identifiers.You will need the <b>Seller ID</b> and <b>MWS Auth Token.</b></li>
                        <li>Come back to this page and enter the <b>Seller ID</b> and <b>MWS Auth Token</b> into the appropriate fields</li>
                    </ol>
                </div>
                <h4 style=" color:#28a7e9; font-size:15px; ">Why do I have to do this?</h4>
                <div>&nbsp;</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box-body">
                {!! Form::open(['url' =>  '/amazon_credential', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'amazonform']) !!}
                 @if(!empty($customer_amazon_detail) && $customer_amazon_detail->mws_seller_id !='')
                {{--*/$seller_id=decrypt($customer_amazon_detail->mws_seller_id) /*--}} {{--*/$auth_token=decrypt($customer_amazon_detail->mws_authtoken) /*--}} {{--*/$market_place_id=$customer_amazon_detail->market_place_id /*--}}
                @else
                {{--*/ $seller_id=''/*--}} {{--*/$auth_token=''/*--}} {{--*/$market_place_id=''/*--}}
                @endif
                <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="" class="control-label col-md-12"> AMAZON SELLER ID </label>
                        <div class="col-md-12">
                            <div class="input-group col-md-12">
                                {!! Form::text('seller_id', old('seller_id',$seller_id), ['class' => 'form-control validate[required]','disabled' => 'true','id' => 'seller_id']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="" class="control-label col-md-12"> MWS AUTH TOKEN </label>
                        <div class="col-md-12">
                            <div class="input-group col-md-12">
                                {!! Form::text('auth_token', old('auth_token',$auth_token), ['class' => 'form-control validate[required]','disabled' => 'true','id' => 'auth_token']) !!}
                                <input type="hidden" name="market_place" id="market_place" value="{{isset($customer_amazon_detail->mws_market_place_id) ? $customer_amazon_detail->mws_market_place_id: null}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-top:20px">
                    <div id="edit_amazon_div" class="col-md-2 col-md-offset-10 col-md-offset-7 text-right">
                        <input type="button" name="edit_amazon" id="edit_amazon" class="button" value="Edit">
                    </div>
                    <div id="verify_div" class="col-md-4 col-md-offset-5 text-right hide">
                        <input type="button" name="verify" id="verify" value="Verify Account" class="button col-md-12">
                    </div>
                    <div class="col-md-2">
                        <input type="submit" name="update_amazon" id="update_amazon" class="button" value="Update" disabled>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        </div>
        <div class="tab-pane fade" id="tabs5" name="tabs" role="tabpanel" aria-labelledby="">
            @if(!empty($user_agreement_detail))
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="box-body">
                        <b>What is a Power of Attorney and Why Do I Need to Sign One?</b>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-body">
                        <form id="power_of_attorney_form" method="post" action="{{ url('/add-power_of_attorney') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <h4><b>Power of Attorney</b></h4>
                            <div class="col-md-12 no-padding">
                                <div class="form-group">
                                    <label class="col-md-12 no-padding">Representative's Name</label>
                                    <div class="col-md-12 no-padding">
                                        <input type="textbox" name="name" @if($power_of_attorney) value="{{ $power_of_attorney->name }}" @endif id="name" class="form-control validate[required]">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12 no-padding">Representative's Title</label>
                                    <div class="col-md-12 no-padding">
                                        <input type="textbox" name="title" @if($power_of_attorney) value="{{ $power_of_attorney->title }}" @endif id="title" class="form-control validate[required]">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12 no-padding">Representative's Email</label>
                                    <div class="col-md-12 no-padding">
                                        <input type="textbox" name="email" @if($power_of_attorney) value="{{ $power_of_attorney->email }}" @endif id="email" class="form-control validate[required]">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-8 no-padding" style="margin-top:20px;">Upload Primary Signer's Photo Identification</label>
                                    <div class="col-md-4 no-padding" style="margin-top:20px;">
                                        <input type="file" name="identity[]" id="identity" class="inputfile">
                                        <label for="identity" class="button identity col-md-12"> Browse</label>
                                        <button type="button" class="darkbluebutton col-md-12" onclick="getattorney()">Prepare POA</button>
                                        <button type="submit" class="button col-md-12" style="margin-top:5px;">Update</button>                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-md-12">
                <div class="box-body">
                    <h4 class="pull-left"><b>Documents</b></h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Document Name</th>
                                <th>Type</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($user_documents) != 0)
                                @foreach($user_documents as $key=>$user_document)
                                    <tr>
                                        <td>{{ $user_document->document }}</td>
                                        <td>Primary Signer's Photo Identification</td>
                                        <td>{{ \Carbon\Carbon::parse($user_document->created_at)->format('m/d/Y') }}</td>
                                        <td><a target="_blank" href="{{ url('uploads/customer_document/'.$user_document->document) }}">View</a></td>
                                    </tr>
                                @endforeach
                            @endif
                            @if(count($user_agreement) != 0)
                                    <tr>
                                        <td>{{ $user_agreement->signature_request_id }}</td>
                                        <td>User Agreement</td>
                                        <td>{{ \Carbon\Carbon::parse($user_agreement->created_at)->format('m/d/Y') }}</td>
                                        <td></td>
                                    </tr>
                            @endif
                            @if(count($service_agreements) != 0)
                                @foreach($service_agreements as $key=>$service_agreement)
                                    <tr>
                                        <td>{{ $service_agreement->signature_request_id }}</td>
                                        <td>Service Agreement</td>
                                        <td>{{ \Carbon\Carbon::parse($service_agreement->created_at)->format('m/d/Y') }}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            @endif
                            @if((count($user_documents) == 0) && (!isset($user_agreement)) && (count($service_agreements) == 0))
                                <tr>
                                    <td>No document found.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
        </div>
    </div>
    </div>
</section>
  <div class="modal fade" id="addCardModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Card</h4>
        </div>
        <div class="modal-body">
            <form method="post" id="validate" class="form-horizontal"  action="{{ url('add-card') }}">
                {{ csrf_field() }}
                <input type="hidden" name="add_card" value="1">
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="nick_name" class="control-label col-md-12 text-left no-padding"> CARD NICKNAME(OPTIONAL) </label>
                        <div class="">
                            <div class="input-group col-md-12">
                                <input type="text" class="form-control no-margin" name="nick_name" id="nick_name" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                        <div class="col-md-12">
                            <label for="card_name" class="control-label col-md-12 no-padding"> NAME ON CARD </label>
                        <div class="">
                            <div class="input-group col-md-12">
                                <input type="text" class="form-control validate[required] no-margin" name="card_name" id="card_name" value="" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="card_number" class="control-label col-md-12 no-padding"> CARD NUMBER </label>
                        <div class="">
                            <div class="input-group col-md-12">
                                <input type="text" class="form-control validate[required, condRequired[creditCard]] no-margin" name="card_number" id="card_number" value="" onkeypress="return AvoidSpace(event)">
                                <input type="hidden"  name="card_type" id="card_type" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 no-padding">
                        <label for="exp_date" class="control-label col-md-12"> EXPIRATION DATE </label>
                        <div class="col-md-6">
                            <div class="input-group col-md-12">
                                <input type="text" class="form-control validate[required, custom[integer]] no-margin" name="exp_month" id="exp_month" value="" placeholder="MONTH" onkeypress="return AvoidSpace(event)" maxlength="2">
                            </div>
                        </div>
                        <div class="col-md-6 no-padding">
                            <div class="input-group col-md-12">
                                <input type="text" class="form-control validate[required, custom[integer]] no-margin" name="exp_year" id="exp_year" value="" placeholder="YEAR" onkeypress="return AvoidSpace(event)" maxlength="4">
                            </div>
                        </div>
                    </div>
                                <div class="col-md-6 no-padding">
                                    <label for="security_code" class="control-label col-md-12"> SECURITY CODE </label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            <input type="text" class="form-control validate[required,condRequired[maxSize[4]]] custom[integer] no-margin" name="security_code" id="security_code" value="" maxlength="4" onkeypress="return AvoidSpace(event)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div id="card_div" hidden>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="nick_name" class="control-label col-md-12"> CARD NICKNAME </label>
                                            <div class="">
                                                <div class="input-group">
                                                    <input type="text" class="form-control no-margin" name="nick_name1" id="nick_name1" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="card_name" class="control-label col-md-12"> NAME ON CARD </label>
                                            <div class="">
                                                <div class="input-group">
                                                    <input type="text" class="form-control validate[required] no-margin" name="card_name1" id="card_name1" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="card_number" class="control-label col-md-12"> CARD NUMBER </label>
                                            <div class="">
                                                <div class="input-group">
                                                    <input type="text" class="form-control validate[required, condRequired[creditCard]] no-margin" name="card_number1" id="card_number1" value="" onkeypress="return AvoidSpace(event)">
                                                    <input type="hidden"  name="card_type1" id="card_type1" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6" style="padding-bottom: 0%;">
                                            <label for="exp_date" class="control-label col-md-12"> EXPIRATION DATE </label>
                                            <div class="col-md-3">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required, custom[integer]]" name="exp_month1" id="exp_month1" value="" placeholder="MONTH" onkeypress="return AvoidSpace(event)" maxlength="2">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required, custom[integer]]" name="exp_year1" id="exp_year1" value="" placeholder="YEAR" onkeypress="return AvoidSpace(event)" maxlength="4">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="padding: 0%;">
                                            <label for="security_code" class="control-label col-md-12"> SECURITY CODE </label>
                                            <div class="col-md-6" style="padding-bottom: 0%;">
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control validate[required,condRequired[maxSize[4]]]" name="security_code1" id="security_code1" value="" maxlength="4" onkeypress="return AvoidSpace(event)">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="default" class="control-label col-md-12"> </label>
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <input type="checkbox" name="default1" id="default1" value="1"> PRIMARY PAYMENT METHOD
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12 no-padding"> COMAPNY ADDRESS </label>
                                        <div class="">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required] no-margin" name="caddress1" id="caddress1" value="" placeholder="STREET ADDRESS">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="">
                                        <label for="" class="control-label col-md-12"> </label>
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control no-margin" name="caddress2" id="caddress2" value="" placeholder="ADDRESS LINE 2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12"> </label>
                                        <div class="">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required] no-margin" name="ccity" id="ccity" value="" placeholder="CITY">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 no-padding">
                                        <label for="" class="control-label col-md-12"> </label>
                                        <div class="col-md-4">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required] custom[integer] no-margin" name="cpostal_code" id="cpostal_code" value="" placeholder="ZIP/POSTAL CODE">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required] no-margin" name="ccountry" id="ccountry" value="" placeholder="COUNTRY">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 no-padding">
                                        <label for="cphone" class="control-label col-md-12">BILLING PHONE NUMBER </label>
                                        <div class="col-md-4">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required] no-margin" name="com_ccode" id="com_ccode" value="" placeholder="COUNTRY CODE">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required] custom[integer] no-margin" name="cphone" id="cphone" placeholder="COUNTRY PHONE" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
        </div>
        <div class="modal-footer">
           <div class="form-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" value="submit" name="Add Card" class="btn btn-primary pull-right">Add Card</button>
                </div>
            </form>
        </div>
      </div>
      
    </div>
  </div>
@else
<section class="content">
    <div class="box">
        <div class="box-body"></div>
    </div>
</section>
@endif
@endsection
@section('js')
    {!! Html::script('https://s3.amazonaws.com/cdn.hellosign.com/public/js/hellosign-embedded.LATEST.min.js') !!}
<script>
    
    function show_additional_section()
    {
        $('#button_div').hide();
        $("#additional_section").removeClass('hide');
    }

$(document).ready(function () {

    var prefix = 's2id_';
    $("#accountform").validationEngine('attach',
        {
            promptPosition: "bottomRight", scroll: false,
            prettySelect: true,
            usePrefix: prefix
        });
    $("#companyform").validationEngine('attach',
        {
            promptPosition: "bottomRight", scroll: false,
            prettySelect: true,
            usePrefix: prefix
        });
    $("#billform").validationEngine('attach',
        {
            promptPosition: "bottomRight", scroll: false,
            prettySelect: true,
            usePrefix: prefix
        });
    $("#amazonform").validationEngine('attach',
        {
            promptPosition: "bottomRight", scroll: false,
            prettySelect: true,
            usePrefix: prefix
        });
    $("form[id^='validate']").validationEngine('attach',
    {
        promptPosition: "bottomRight", scroll: false,
        prettySelect: true,
        usePrefix: prefix
    });
    $("form[id^='power_of_attorney_form']").validationEngine('attach',
    {
        promptPosition: "bottomRight", scroll: false,
        prettySelect: true,
        usePrefix: prefix
    });

    $('.select2').addClass('col-md-12');

    $('#edit_amazon').click(function(){
        $('#seller_id').removeAttr('disabled');
        $('#auth_token').removeAttr('disabled');
        $('#update_amazon').removeAttr('disabled');
        $('#verify_div').removeClass('hide');
        $('#edit_amazon_div').addClass('hide');
    });
    $('#validate').submit(function(e){
        e.preventDefault();
        if ($("#validate").validationEngine('validate')){
            var formData = new FormData(document.getElementById("validate"));
            $.ajax({
                headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
                },
                method: 'POST',
                url: '{{ url("add-card") }}',
                async: false,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    $('#addCardModal').hide();
                    alert('Card added successfully.');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
    });
    $('#complete_settings').click(function(){
        $.ajax({
            headers: {
            'X-CSRF-Token': '{{ csrf_token() }}',
            },
            method: 'POST',
            url: '/settings/complete',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                window.location.href = "{{ url('member/home') }}";
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    });
    $(document).ready(function () {
        $('.datepicker').datepicker({
            startDate: new Date(),
        });
    });
    $(".select2").select2({
    placeholder: "Please Select",
    allowClear: true,
    width: '100%'
    });
    var value=$("#taxid").val();
    gettax(value);
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var $target = $(e.target);
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });
    $(".next-step").click(function (e) {
        button_id= $(this).attr('id');
        if(button_id=='company_detail')
        {
            if ($("#company_form").validationEngine('validate'))
            {
                var formData = new FormData(document.getElementById("company_form"));
                $('.preloader').css("display", "block");
                $.ajax({
                    headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    method: 'POST',
                    url: '/customer',
                    async: false,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        $('.preloader').css("display", "none");
                        console.log(response);
                        next_step();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        }
        else if(button_id=='card_detail')
        {
            if ($("#card_form").validationEngine('validate')) {
                var form = $(document.forms['card_form']);
                $('.preloader').css("display", "block");
                $.ajax({
                    headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    method: 'POST',
                    url: '/creditcard',
                    data: form.serializeArray(),
                    success: function (response) {
                        $('.preloader').css("display", "none");
                        console.log(response);
                        next_step();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        }
        else if(button_id=='amazon_detail')
        {
            if ($("#amazon_form").validationEngine('validate')) {
                var form = $(document.forms['amazon_form']);
                $('.preloader').css("display", "block");
                $.ajax({
                    headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    method: 'POST',
                    url: '/amazon_credential',
                    data: form.serializeArray(),
                    success: function (response) {
                        $('.preloader').css("display", "none");
                        console.log(response);
                        next_step();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        }
        else if(button_id=='attorney_detail')
        {
            if ($("#attorney_form").validationEngine('validate')) {
                var formData = new FormData(document.getElementById("attorney_form"));
                $('.preloader').css("display", "block");
                $.ajax({
                    headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    method: 'POST',
                    url: '/powerofatterney',
                    async: false,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        $('.preloader').css("display", "none");
                        console.log(response);
                        next_step();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        }
        else if(button_id=='service_detail')
        {
            if($("#service_form").validationEngine('validate')){
                var formData = new FormData(document.getElementById("service_form"));
                $('.preloader').css("display", "block");
                $.ajax({
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    method: 'POST',
                    url: '/serviceagreement',
                    async: false,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        $('.preloader').css("display", "none");
                        console.log(response);
                        next_step();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        }
        else
        {
            next_step();
        }
    });
    function next_step()
    {
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);
    }
});
function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function addadditional()
{
    $("#additional_div").show();
    $("#additional_div1").show();
    $("#button_div").hide();
}
function addcard()
{
    $("#card_div").show();
    $("#card_button_div").hide();
}
function changeCountryWiseState(val) {
    $.ajax({
        headers: {
        'X-CSRF-Token': '{{ csrf_token() }}',
        },
        method: 'POST',
        url: '/customer/selectState',
        data: {
        'country_code': val,
        },
        success: function (response) {
            $('#select_state').html(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(JSON.stringify(jqXHR));
            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
        }
    });
}
function gettax(value) {
    $(".tax_div").hide();
    if(value=='1')
    {
    $("#ein_div").show();
    }
    if(value=='2')
    {
    $("#ssn_div").show();
    }
    if(value=='3')
    {
    $("#itin_div").show();
    }
    if(value=='4')
    {
    $("#no_div").show();
    }
}
$("#card_number").blur(function() {
    var result = getCreditCardType($("#card_number").val());
    $("#card_type").val(result);
});
$("#card_number1").blur(function() {
    var result = getCreditCardType($("#card_number1").val());
    $("#card_type1").val(result);
});
function getCreditCardType(card_number)
{
var result = "unknown";
if (/^5[1-5]/.test(card_number)) { result = "mastercard"; }
else if (/^4/.test(card_number)) { result = "visa"; }
else if (/^3[47]/.test(card_number)) { result = "amex"; }
else if (/^6([045])/.test(card_number)) { result = "discover"; }
else if (/^35/.test(card_number)) { result = "jcb"; }
// else if (/^6([39|7])|5([018])/.test(card_number)) { result = "maestro"; }
return result;
}
$("#com_address").on("click", function(){
    if($(this).is(":checked")) {
        $.ajax({
            headers: {
            'X-CSRF-Token': '{{ csrf_token() }}',
            },
            method: 'POST',
            url: '/settings/getuser',
            success: function (response) {
                var data = $.parseJSON(response); console.log(response);
                $.each(data,function (index, value) {
                $("#caddress1").val(value.company_address);
                $("#caddress2").val(value.company_address2);
                $("#ccity").val(value.company_city);
                $("#ccountry").val(value.business_country);
                $("#cpostal_code").val(value.company_zipcode);
                $("#com_ccode").val(value.contact_country_code);
                $("#cphone").val(value.contact_phone);

                $("#caddress1").removeClass('error');
                $("#caddress2").removeClass('error');
                $("#ccity").removeClass('error');
                $("#ccountry").removeClass('error');
                $("#cpostal_code").removeClass('error');
                $("#com_ccode").removeClass('error');
                $("#cphone").removeClass('error');

                $("#caddress1").focusout();
                $("#caddress2").focusout();
                $("#ccity").focusout();
                $("#ccountry").focusout();
                $("#cpostal_code").focusout();
                $("#com_ccode").focusout();
                $("#cphone").focusout();

                })
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }
    else
    {
        $("#caddress1").val('');
        $("#caddress2").val('');
        $("#ccity").val('');
        $("#ccountry").val('');
        $("#cpostal_code").val('');
        $("#com_ccode").val('');
        $("#cphone").val('');
    }
});
$('#verify').click(function () {
    seller_id = $("#seller_id").val();
    auth_token = $("#auth_token").val();
    market_place = $("#market_place_id").val();
    if ($("#amazon_form").validationEngine('validate')) {
        $('.preloader').css("display", "flex");
        $.ajax({
            headers: {
            'X-CSRF-Token': '{{ csrf_token() }}',
            },
            method: 'POST',
            url: '/test_connection',
            data: {
            'seller_id': seller_id,
            'auth_token': auth_token,
            'market_place': market_place
            },
            success: function (response) {
                if(response == 'Your Amazon Credentail is invalid'){
                    $('#amazon_detail').attr('disabled','true');
                    $('#amazon_detail').css('cursor','no-drop');
                }
                else{
                    $('#amazon_detail').removeAttr('disabled');
                    $('#amazon_detail').css('cursor','pointer');
                }
                $('.preloader').css("display", "none");
                swal(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }
});
updateList = function() {
    var input = document.getElementById('identity');
    var output = document.getElementById('fileList');
    output.innerHTML = '<ul>';
    for (var i = 0; i < input.files.length; ++i) {
    output.innerHTML += '<li>' + input.files.item(i).name + '</li>';
    }
    output.innerHTML += '</ul>';
}
$("#agree").on('click',function(){
    if($(this).is(":checked")) {
        $("#rep_name").attr("disabled", "disabled");
        $("#rep_title").attr("disabled","disabled");
        $("#rep_email").attr("disabled","disabled");
        $("#identity").attr("disabled","disabled");
    }
    else
    {
        $("#rep_name").removeAttr("disabled");
        $("#rep_title").removeAttr("disabled");
        $("#rep_email").removeAttr("disabled");
        $("#identity").removeAttr("disabled");
    }
});
function getattorney()
{
    $.ajax({
        headers: {
        'X-CSRF-Token': '{{ csrf_token() }}',
        },
        method: 'POST',
        url: '/useragreement',
        data: {
        },
        success: function (response) {
            $('.preloader').css("display", "none");
            var data = $.parseJSON(response);
            claim_url=data.claim_url;
            if(claim_url !=='') {
                HelloSign.init(data.client_id);
                HelloSign.open({
                    url: claim_url,
                    "skipDomainVerification": true,
                    messageListener: function (eventData) {
                        if (eventData.event == HelloSign.EVENT_SENT) {
                            $("#attorney_div").html("/sent?signature_request_id=" + eventData["signature_request_id"]);
                        }
                    }
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(JSON.stringify(jqXHR));
            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
        }
    });
}
function getcarddetail(value) {
    $.ajax({
        headers: {
        'X-CSRF-Token': '{{ csrf_token() }}',
        },
        method: 'POST',
        url: '/creditcard/getcarddetail',
        data: {
        id:value
        },
        success: function (response) {
            $('.preloader').css("display", "none");
            var data = $.parseJSON(response);
            var html='';
            html+="<div class='col-md-12'><div class='col-md-6'>Nickname</div><div class='col-md-6'>"+data.nick_name+"</div><div class='col-md-6'>Name On Card</div><div class='col-md-6'>"+data.first_name+" "+data.last_name+"</div><div class='col-md-6'>Credit card number</div><div class='col-md-6'>"+data.credit_card_number+"</div><div class='col-md-6'>Credit type</div><div class='col-md-6'>"+data.credit_card_type+"</div><div class='col-md-6'>Security Code</div><div class='col-md-6'>"+data.cvv+"</div></div>";
            $("#card_detail_div").append(html);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(JSON.stringify(jqXHR));
            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
        }
    });
}
$("#learn_more").click(function(){
    $("#learn_modal").modal('show');
});
function showpassdiv() {
    $("#password_div").show();
    $('#password_label').hide();
}
function showemaildiv() {
    $("#email_div").show();
    $('#email_label').hide();
}
function AvoidSpace(event) {
    var k = event ? event.which : window.event.keyCode;
    if (k == 32) return false;
}
</script>
@endsection