@extends('layouts.admin.app')
@section('title', 'Place Order')
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    {!! Html::style('assets/dist/css/custom_quotes.css') !!}
<style>
.background{  background: #ecf0f5;  }
.box3{ margin-bottom: 20px;  background: #c7e0e6;  padding: 5px;  }
.link:hover{ color: #0088cc !important;cursor: pointer; }
.form-horizontal .control-label{ text-align: left !important; }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1> Place Order </h1>
        <small>Review order and make edits where necessary. Choose add-on products, if desired.</small>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('quote') }}"><i class="fa fa-anchor"></i>Quote</a></li>
            <li class="active"><a href="{{ url('quote/edit/') }}">  <!-- @if(!empty($customer_requirement)) Request Again @else Quote Details @endif --> Place Order</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
             <div class="box-body no-padding">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="col-md-12 no-padding">
                            <div class="col-md-12 no-padding height42">
                                <div class="col-md-12 product-data border-left-1 text-overflow">
                                    <b>Product :</b>
                                    @if(empty($place_order->product_nick_name)){{ $place_order->product_name }} @else {{$place_order->product_nick_name}} @endif
                                </div>
                            </div>
                        </div>
                         @if($place_order->order_type == 1)
                        <div class="col-md-12 no-padding">
                            <div class="col-md-3 big-order-box product-data height40 border-left-1">
                                @if($place_order->length)
                                    <b>Dimensions:</b>&nbsp; <span id="l">{{ $place_order->length }}</span> &nbsp;&nbsp; <b>x</b> &nbsp;&nbsp; <span id="w">{{ $place_order->width }}</span> &nbsp;&nbsp; <b>x</b> &nbsp;&nbsp; <span id="h">{{ $place_order->height }}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="unit">{{ $place_order->unit }}</span>
                                @else
                                    <b>Dimensions:</b>
                                @endif
                            </div>
                            <div class="col-md-3 small-order-box no-padding">
                                <div class="col-md-12 product-data">
                                    <b>Units : </b><span> {{ $place_order->units }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding">
                                <div class="col-md-12 product-data">
                                    <b>Cartoons: </b><span> {{ $place_order->cartoons }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding">
                                <div class="col-md-12 product-data">
                                    <b>Weight : </b><span> {{ $place_order->weight }} @if($place_order->units==1){{"KG"}}@else{{"LB"}}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding">
                                <div class="col-md-12 product-data">
                                    <b>Volume : </b><span> {{ $place_order->volume }} @if($place_order->units==1){{"CBM"}}@else{{"CFT"}}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding left-border">
                                <div class="col-md-12 product-data ">
                                    <b>Inbound Shipping Method : </b><span>{{ $shipping_method->shipping_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding">
                                <div class="col-md-12 product-data ">
                                    <b>Type: </b><span>{{ $place_order->shipment_short_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding">
                                <div class="col-md-12 product-data">
                                    <b>Incoterms : </b><span>{{ $place_order->incoterm_short_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding">
                                <div class="col-md-12 product-data text-overflow">
                                    <b>Port of Origin : </b><span>{{ $place_order->port_of_origin }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding">
                                <div class="col-md-12 product-data">
                                     <b>Port of Arrival:</b><span>
                                        @if($shipping_method->shipping_method_id==1)
                                            Long Beach
                                        @elseif($shipping_method->shipping_method_id==2 || $shipping_method->shipping_method_id==3)
                                            San Diego
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3 bigger-order-box no-padding">
                                    <div class="col-md-12 product-data border-left-1">
                                        <b>Supplier :</b>
                                        {{ $place_order->supplier_name }}
                                    </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding"  style="width:32% !important">
                                <div class="col-md-12 product-data">
                                    <b>Approx. Goods Ready Date : </b><span>{{ \Carbon\Carbon::parse($place_order->apporx_goods_ready_date)->format('m/d/Y') }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding" style="width:26% !important">
                                <div class="col-md-12 product-data">
                                    <b>Outbound Shipping Method : </b><span>{{ $place_order->outbound_short_name}}</span>
                                </div>
                            </div>
                            @if($place_order->shipment_notes !='')
                            <div class="col-md-12 no-padding">
                                <div class="col-md-12 product-data left-border">
                                    <b>Shipping Notes : </b><span>{{ $place_order->shipment_notes }}</span>
                                </div>
                            </div>
                            @endif
                        </div>
                        @elseif($place_order->order_type == 2)
                            <div class="col-md-12 no-padding">
                            <div class="col-md-3 big-order-box product-data height40 border-left-1">
                                    <b>Dimensions:</b>&nbsp; <span id="l">{{ $place_order->length }}</span> &nbsp;&nbsp; <b>x</b> &nbsp;&nbsp; <span id="w">{{ $place_order->width }}</span> &nbsp;&nbsp; <b>x</b> &nbsp;&nbsp; <span id="h">{{ $place_order->height }}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="unit">{{ $place_order->unit }}</span>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Units : </b><span> {{ $place_order->units }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Cartoons: </b><span> {{ $place_order->cartoons }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Weight : </b><span> {{ $place_order->weight }} @if($place_order->units==1){{"KG"}}@else{{"LB"}}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Volume : </b><span> {{ $place_order->volume }} @if($place_order->units==1){{"CBM"}}@else{{"CFT"}}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding left-border grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Inbound Shipping Method : </b><span>{{ $shipping_method->shipping_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg" style="width:32% !important;">
                                <div class="col-md-12 product-data">
                                    <b>Type: </b><span>{{ $place_order->type }}</span>
                                </div>
                            </div>
                            
                            <div class="col-md-6 no-padding" style="width:42%;" >
                                <div class="col-md-12 product-data">
                                    <b>Outbound Shipping Method : </b><span>{{ $place_order->outbound_shipping_method }}</span>
                                </div>
                            </div>
                        @else
                            <div class="col-md-12 no-padding">
                            <div class="col-md-3 big-order-box product-data height40 border-left-1">
                                   <b>Dimensions:</b>&nbsp; <span id="l">{{ $place_order->length }}</span> &nbsp;&nbsp; <b>x</b> &nbsp;&nbsp; <span id="w">{{ $place_order->width }}</span> &nbsp;&nbsp; <b>x</b> &nbsp;&nbsp; <span id="h">{{ $place_order->height }}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="unit">{{ $place_order->unit }}</span>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data ">
                                    <b>Units : </b><span> {{ $place_order->units }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data ">
                                    <b>Cartoons: </b><span> {{ $place_order->cartoons }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data ">
                                    <b>Weight : </b><span> {{ $place_order->weight }} @if($place_order->units==1){{"KG"}}@else{{"LB"}}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data ">
                                    <b>Volume : </b><span> {{ $place_order->volume }} @if($place_order->units==1){{"CBM"}}@else{{"CFT"}}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding left-border grey-bg">
                                <div class="col-md-12 product-data height56">
                                    <b>Inbound Shipping Method : </b><span>{{ $shipping_method->shipping_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data height56">
                                    <b>Type: </b><span>{{ $place_order->shipment_short_name }}</span>
                                </div>
                            </div>

                            <div class="col-md-3 small-order-box no-padding grey-bg text-overflow">
                                <div class="col-md-12 product-data height56">
                                    <b>Portof Origin : </b><span>{{ $place_order->port_of_origin }}</span>
                                </div>
                            </div>
                            
                            <div class="col-md-3 small-order-box no-padding">
                                <div class="col-md-12 product-data height56">
                                    <b>Outbound Shipping Method : </b><span>{{ $place_order->outbound_short_name }}</span>
                                </div>
                            </div>

                            <div class="col-md-3 big-order-box no-padding">
                                <div class="col-md-12 product-data height56">
                                    <b>Date of Arrival at FBAforward: </b><span> 
                                    @if(!empty($place_order->date_of_arrival_at_fbaforward)) 
                                        {{ \Carbon\Carbon::parse($place_order->date_of_arrival_at_fbaforward)->format('m/d/Y') }}
                                    @endif
                                    </span>
                                </div>
                            </div>
                            @if($place_order->shipment_notes !='')
                            <div class="col-md-12 no-padding">
                                <div class="col-md-12 product-data left-border">
                                    <b>Shipping Notes:</b><span>{{ $place_order->shipment_notes }}</span>
                                </div>
                            </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    <div class="col-md-12 blank-class"></div>
    <div class="row">
        <div class="col-md-12 background">
            <div class="box2 col-md-9 no-padding">
                <div class="box-header with-border order-details-heading no-border">
                    <h3 class="box-title">Order Details</h3>
                </div>
                {{--*/ $charge_qty = 1 /*--}}
                {{--*/ $shipping_sub_total = 0 /*--}}
                {{--*/ $prep_price = "" /*--}}
                {{--*/ $prep_sub_total = 0 /*--}}
                {{--*/ $trucking_qty="" /*--}}
                {{--*/ $port_fee = 0 /*--}}
                {{--*/ $port_fee_name =  env('PORT_FEE_NAME') /*--}}
                {{--*/ $trucking_name =  env('TRUCKING_NAME') /*--}}
                {{--*/ $isf_rate =  env('ISF_RATE') /*--}}
                {{--*/ $isf_name =  env('ISF_NAME') /*--}}
                {{--*/ $entry_rate = env('ENTRY_RATE') /*--}}
                {{--*/ $entry_name = env('ENTRY_NAME') /*--}}
                {{--*/ $bond_rate =  env('BOND_RATE') /*--}}
                {{--*/ $bond_name =  env('BOND_NAME') /*--}}
                {{--*/ $container_unloading= env('CONTAINER_UNLOADING') /*--}}
                {{--*/ $palletizing= env('PALLETIZING') /*--}}
                {{--*/ $amazon_approve_pallet= env('AMAZON_APPROVE_PALLET') /*--}}
                {{--*/ $pallet_charge= env('pallet_charge') /*--}}
                {{--*/ $receive_forward_sub_total= '' /*--}}
                {{--*/ $custom_sub_total= '' /*--}}
                {{--*/ $pre_shipment_sub_total= '' /*--}}
                {{--*/ $factory_audit_total= '' /*--}}
                {{--*/ $pre_inspection_total= '' /*--}}
                {{--*/ $order_total= '' /*--}}
                {{--*/ $processing_fee= 0 /*--}}
                @if($place_order->units==1)
                    {{--*/$trucking_qty=round($place_order->shipment_volume / env('PALLET_CBM')) /*--}}
                @else
                    {{--*/$trucking_qty=round($place_order->shipment_volume / env('PALLET_CFT'))/*--}}
                @endif
                @if($trucking_qty > env('TRUCKING_QTY'))
                    {{--*/ $trucking_price = ($trucking_qty -  env('TRUCKING_QTY')) * env('TRUCKING_ADDITIONAL_PRICE') + env('TRUCKING_PRICE')/*--}}
                @else
                    {{--*/ $trucking_price =  env('TRUCKING_PRICE') /*--}}
                @endif
                @if($place_order->factory_audit=='1' || $place_order->pre_inspection=='1')
                <table class="table quote-summery-table">
                  <thead>
                    <tr>
                        <th class="col-md-6">Pre-Shipment</th>
                        <th class="col-md-2">Rate</th>
                        <th class="col-md-2">Qty</th>
                        <th class="col-md-2">Total</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if($place_order->factory_audit=='1')
                      <tr>
                          <td class="col-md-6">Factory Audit</td>
                          <td class="col-md-2">${{ env('FACTORY_AUDIT_FEE') }}</td>
                          <td class="col-md-2">{{ $charge_qty }}</td>
                          <td class="col-md-2">${{number_format(($charge_qty * env('FACTORY_AUDIT_FEE')))}} {{--*/ $factory_audit_total = ($charge_qty * env('FACTORY_AUDIT_FEE')) /*--}}</td>
                      </tr>
                  @endif
                  @if($place_order->pre_inspection=='1')
                  <tr>
                      <td class="col-md-6">Pre-Inspection</td>
                      <td class="col-md-2">${{ env('INSPECTION_AUDIT_FEE') }}</td>
                      <td class="col-md-2">{{ $charge_qty }}</td>
                      <td class="col-md-2">${{number_format(($charge_qty * env('FACTORY_AUDIT_FEE')))}} {{--*/ $pre_inspection_total = ($charge_qty * env('FACTORY_AUDIT_FEE')) /*--}}</td>
                  </tr>
                  @endif
                  <tr>
                      <td><b>Pre-Shipment Subtotal</b></td>
                      <td></td>
                      <td></td>
                      <td><b>${{--*/ $pre_shipment_sub_total = $factory_audit_total + $pre_inspection_total /*--}}{{number_format(($pre_shipment_sub_total))}}</td></b></td>
                  </tr>
                  </tbody>
                </table>
            @endif
      @if($place_order->order_type == 1)
      <table class="table quote-summery-table">
        <thead>
          <tr>
              <th>Shipping</th>
              <th></th>
              <th></th>
              <th></th>
          </tr>
        </thead>
        <tbody>
        <tr>
            <td class="col-md-6">{{$place_order->shipping_name}}</td>
            <td class="col-md-2">${{$place_order->shipment_type_rate}}</td>
            <td class="col-md-2">{{$place_order->shipment_volume}}</td>
            <td class="col-md-2">${{number_format(($place_order->shipment_type_rate*$place_order->shipment_volume))}} </td>
        </tr>
          {{--*/ $shipping_sub_total += ($place_order->shipment_type_rate*$place_order->shipment_volume) /*--}}
          @foreach($detail_charges as  $detail_charge)
              <?php
                  $charge_price = $detail_charge->charge * $charge_qty;
                  $shipping_sub_total += $charge_price;
              ?>
              <tr>
                  <td class="col-md-6">{{ $detail_charge->name }}</td>
                  <td class="col-md-2">${{ $detail_charge->charge }}</td>
                  <td class="col-md-2">{{ $charge_qty }}</td>
                  <td class="col-md-2">${{number_format(($charge_price))}}</td>
              </tr>
          @endforeach
        @if($place_order->shipping_name!=="undefined")
            @if($place_order->shipping_method_id >1)
                {{--*/  $port_fee= env('PORT_FEE')/*--}}
                <tr>
                    <td class="col-md-6">{{$port_fee_name}}</td>
                    <td class="col-md-2">${{$port_fee}}</td>
                    <td class="col-md-2">{{$charge_qty}}</td>
                    <td class="col-md-2">${{number_format(($port_fee*$charge_qty))}}</td>
                </tr>

                <tr>
                    <td class="col-md-6"> {{$trucking_name}}</td>
                    <td class="col-md-2">$99/first 3 pallets, $25/additional pallets</td>
                    <td class="col-md-2">{{$trucking_qty}}</td>
                    <td class="col-md-2">${{$trucking_price}}</td>
                </tr>
            @else
                @if($place_order->shipment_type_id==1)
                    {{--*/ $port_fee= env('OCEAN_LCL_PORT_FEE') /*--}}
                @else
                    {{--*/ $port_fee= env('OCEAN_PORT_FEE') /*--}}
                @endif
                <tr>
                    <td class="col-md-6">{{$port_fee_name}}</td>
                    <td class="col-md-2">${{$port_fee}}</td>
                    <td class="col-md-2">{{$charge_qty}}</td>
                    <td class="col-md-2">${{number_format($port_fee*$charge_qty)}}</td>
                </tr>
                <tr>
                    <td class="col-md-6">{{$trucking_name}}</td>
                    <td class="col-md-2">${{$place_order->trucking_rate}}/pallet</td>
                    <td class="col-md-2">{{$trucking_qty}}</td>
                    <td class="col-md-2">{{--*/$trucking_price = ($place_order->trucking_rate*$trucking_qty) /*--}}${{number_format($trucking_price)}} </td>
                </tr>
            @endif
            {{--*/$shipping_sub_total+=$port_fee*$charge_qty/*--}}
            {{--*/$shipping_sub_total+=$trucking_price/*--}}
        @endif
          <tr>
              <td class="col-md-6">{{ env('WIRE_TRANS_NAME') }}</td>
              <td class="col-md-2">${{ env('WIRE_TRANS_FEE') }}</td>
              <td class="col-md-2">&nbsp;</td>
              <td class="col-md-2">${{number_format(env('WIRE_TRANS_FEE'))}}</td>
          </tr>
          {{--*/ $shipping_sub_total +=env('WIRE_TRANS_FEE') /*--}}
          <tr>
              <td><b>Shipping Subtotal</b></td>
              <td></td>
              <td></td>
              <td><b>${{ number_format($shipping_sub_total) }}</b></td>
          </tr>

        </tbody>
      </table>
      @endif
      @if(($place_order->order_type == 1) || ($place_order->order_type == 3))
      <table class="table quote-summery-table">
        <thead>
          <tr>
              <th class="col-md-6">Custom</th>
              <th class="col-md-2">&nbsp;</th>
              <th class="col-md-2">&nbsp;</th>
              <th class="col-md-2">&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          @if($place_order->shipping_method_id == 1)
          <tr>
                  <td class="col-md-6">{{$isf_name}}</td>
                  <td class="col-md-2">${{$isf_rate}}</td>
                  <td class="col-md-2">{{$charge_qty}}</td>
                  <td class="col-md-2">${{ number_format($isf_rate * $charge_qty) }}</td>
              </tr>
              {{--*/$custom_sub_total= ($isf_rate*$charge_qty) /*--}}
          @endif
          <tr>
              <td class="col-md-6">{{$entry_name}}</td>
              <td class="col-md-2">${{$entry_rate}}</td>
              <td class="col-md-2">{{$charge_qty}}</td>
              <td class="col-md-2">${{ number_format($entry_rate*$charge_qty) }}</td>
          </tr>
          <tr>
              <td class="col-md-6">{{$bond_name}}</td>
              <td class="col-md-2">${{$bond_rate}}</td>
              <td class="col-md-2">{{$charge_qty}}</td>
              <td class="col-md-2">${{ number_format($bond_rate*$charge_qty) }}</td>
          {{--*/ $custom_sub_total+= (int)($entry_rate*$charge_qty) + ($bond_rate*$charge_qty)/*--}}
          <tr>
          <tr>
              <td colspan="3"><b>Customs Subtotal </b></td>
              <td><b>${{ number_format($custom_sub_total) }}</b></td>
          </tr>
        </tbody>
      </table>
      @endif
      <table class="table quote-summery-table">
        <thead>
          <tr>
              <th>Receiving & Forwarding</th>
              <th></th>
              <th></th>
              <th></th>
          </tr>
        </thead>
        <tbody>
          @if($place_order->shipment_type_id >1)
              <tr>
                  <td class="col-md-6">container_unloading</td>
                  <td class="col-md-2">${{$place_order->container_unload_rate}}</td>
                  <td class="col-md-2">{{$charge_qty}}</td>
                  <td class="col-md-2">${{ number_format($place_order->container_unload_rate*$charge_qty) }}</td>
              </tr>
          @endif
          {{--*/$receive_forward_sub_total+=$place_order->container_unload_rate*$charge_qty/*--}}
          @if($place_order->outbound_method_id >1)
              <tr>
                  <td class="col-md-6">{{$palletizing}}</td>
                  <td class="col-md-2">${{$pallet_charge}}</td>
                  <td class="col-md-2">{{$trucking_qty}}</td>
                  <td class="col-md-2">${{ number_format($pallet_charge*$trucking_qty) }}</td>
              </tr>
              {{--*/$receive_forward_sub_total+=$pallet_charge*$trucking_qty/*--}}
              <tr>
                  <td class="col-md-6">{{$amazon_approve_pallet}}</td>
                  <td class="col-md-2">${{$pallet_charge}}</td>
                  <td class="col-md-2">{{$trucking_qty}}</td>
                  <td class="col-md-2">${{ number_format($pallet_charge*$trucking_qty) }}</td>
              </tr>
              {{--*/$receive_forward_sub_total+=$pallet_charge*$trucking_qty/*--}}
          @endif
          <tr>
              <td class="col-md-6">{{$place_order->outbound_shipping_method}}</td>
              <td class="col-md-2">&nbsp;</td>
              <td class="col-md-2">&nbsp;</td>
              <td class="col-md-2"><i>varies</i></td>
          </tr>
          <tr>
              <td class="col-md-6"><b>Receiving & Forwarding subtotal</b></td>
              <td class="col-md-2"></td>
              <td class="col-md-2"></td>
              <td class="col-md-2"><b>${{ number_format($receive_forward_sub_total) }}</b></td>
          </tr>
        </tbody>
      </table>
      <table class="table quote-summery-table">
        <thead>
          <tr>
              <th>Prep & Inspection</th>
              <th></th>
              <th></th>
              <th></th>
          </tr>
        </thead>
        <tbody>
           @foreach($detail_prep as $prep_key=>$prep_value )
              {{--*/$detail_prep = $prep_value->price * $place_order->total/*--}}
              @if($detail_prep % 1 !=0)
                  <tr>
                      <td class="col-md-6">{{$prep_value->service_name}}</td>
                      <td class="col-md-2">${{$prep_value->price}}</td>
                      <td class="col-md-2">{{$place_order->total}}</td>
                      <td class="col-md-2">${{ number_format($detail_prep) }}</td>
                      {{--*/$prep_sub_total+=$detail_prep/*--}}
                  </tr>
              @else
                  <tr>
                      <td class="col-md-6">{{$prep_value->service_name}}</td>
                      <td class="col-md-2">${{$prep_value->price}}</td>
                      <td class="col-md-2">{{$place_order->total}}</td>
                      <td class="col-md-2">${{ number_format($detail_prep) }}</td>
                      {{--*/$prep_sub_total+=$detail_prep/*--}}
                  </tr>
              @endif
              @endforeach
              <tr>
                  <td class="col-md-6"><b>Prep & Inspection subtotal</b></td>
                  <td class="col-md-2"></td>
                  <td class="col-md-2"></td>
                  <td class="col-md-2"><b>${{ number_format($prep_sub_total) }}</b></td>
              </tr>
        </tbody>
      </table>
      {{--*/ $order_total = $pre_shipment_sub_total + $shipping_sub_total + $custom_sub_total + $receive_forward_sub_total + $prep_sub_total /*--}}
      {{--*/ $processing_fee = ($order_total*3)/100 /*--}}
      <table class="table quote-summery-table">
          <tbody>
              <tr>
                  <td class="col-md-6">Processing Fee <abbr title="Processing Fee"><i class="fa fa-question-circle compulsary"></i></abbr></td>
                  <td class="col-md-2"></td>
                  <td class="col-md-2"></td>
                  <td class="col-md-2">${{ number_format($processing_fee) }}</td>
              </tr>
          </tbody>
      </table>
      <div class="col-md-12 background box3">
          <div class="col-md-10 text-left">
              <b>Order Total</b>
          </div>
          {{--*/ $order_total += $processing_fee/*--}}
          <div class="col-md-1 text-center no-padding">
              <b>${{ number_format($order_total) }}</b>
          </div>
      </div>
    </div>
    <div class="box2 col-md-3 action-box no-padding">
      <div class="col-md-12 no-padding">
          <div class="box-header with-border">
              <h3 class="box-title">Payment Method</h3>
          </div>
           <div class="box-body">
               <form method="post" class="form-horizontal"  action="{{ url('order/create') }}">
                   {{ csrf_field() }}
                  <p class="col-md-12">Choose Credit Card: </p>
                  <select class="form-control select-box" name="credit_card">
                      @foreach($cards as $card)
                          @if($card->make_deafult == 1)
                              <option value="{{ $card->id }}" selected="selected">{{ $card->credit_card_number }}</option>
                          @endif
                          <option value="{{ $card->id }}">{{ $card->credit_card_number }}</option>
                      @endforeach
                  </select>
                  <input type="hidden" name="order_total" id="order_total" value="{{ $order_total }}">
                  <input type="hidden" name="customer_requirement_id" value="{{ $place_order->id }}">
                  <input type="hidden" name="method_id" value="{{ $shipping_method->shipping_method_id }}">
                  <button type="submit" class="btn btn-primary btn-reject-quote col-md-12 margintop10">Submit Order</button>
              </form>
          </div>
      </div>
      <div class="col-md-12 blank-class"></div>
      <div class="col-md-12 no-padding">
          <div class="box-header with-border">
              <h3 class="box-title">Pre-Shipment Notes</h3>
               <div class="box-tools pull-right">
                  <a class="link" data-toggle="modal" data-target="#updatePreShipmentModal">Edit</a>
              </div>
          </div>
           <div class="box-body">
              {{ $place_order->pre_shipment_note }}
          </div>
      </div>
      <div class="col-md-12 blank-class"></div>
      <div class="col-md-12 no-padding">
          <div class="box-header with-border">
              <h3 class="box-title">Prep Notes</h3>
               <div class="box-tools pull-right">
                  <a class="link" data-toggle="modal" data-target="#updatePreModal">Edit</a>
              </div>
          </div>
           <div class="box-body">
              <span>{{ $place_order->prep_note }}</span>
          </div>
      </div>
      <div class="col-md-12 blank-class"></div>
      <div class="col-md-12 no-padding">
          <div class="box-header with-border">
              <h3 class="box-title">Inspection Notes</h3>
               <div class="box-tools pull-right">
                  <a class="link" data-toggle="modal" data-target="#updateInspectionModal">Edit</a>
              </div>
          </div>
           <div class="box-body">
              <span>{{ $place_order->inspection_note }}</span>
          </div>
      </div>
    </div>
    </div>
    </div>
</section>
<div class="modal fade" id="addCardModal" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Add Card</h4>
</div>
<div class="modal-body">
<form method="post" id="validate" class="form-horizontal"  action="{{ url('add-card') }}">

  {{ csrf_field() }}
  <input type="hidden" name="add_card" value="1">
  <div class="form-group">
      <div class="col-md-12">
          <label for="nick_name" class="control-label col-md-12 text-left"> CARD NICKNAME(OPTIONAL) </label>
          <div class="">
              <div class="input-group">
                  <input type="text" class="form-control no-margin" name="nick_name" id="nick_name" value="">
              </div>
          </div>
      </div>
  </div>
  <div class="form-group">
          <div class="col-md-12">
              <label for="card_name" class="control-label col-md-12"> NAME ON CARD </label>
          <div class="">
              <div class="input-group">
                  <input type="text" class="form-control validate[required] no-margin" name="card_name" id="card_name" value="" >
              </div>
          </div>
      </div>
  </div>
  <div class="form-group">
      <div class="col-md-12">
          <label for="card_number" class="control-label col-md-12"> CARD NUMBER </label>
          <div class="">
              <div class="input-group col-md-12">
                  <input type="text" class="form-control validate[required, condRequired[creditCard]] no-margin" name="card_number" id="card_number" value="" onkeypress="return AvoidSpace(event)">
                  <input type="hidden"  name="card_type" id="card_type" value="">
              </div>
          </div>
      </div>
  </div>
  <div class="form-group">
      <div class="col-md-6">
          <label for="exp_date" class="control-label col-md-12"> EXPIRATION DATE </label>
          <div class="col-md-6 no-padding">
              <div class="input-group">
                  <input type="text" class="form-control validate[required] no-margin" name="exp_month" id="exp_month" value="" placeholder="MONTH" onkeypress="return AvoidSpace(event)" maxlength="2">
              </div>
          </div>
          <div class="col-md-6 no-padding">
              <div class="input-group">
                  <input type="text" class="form-control validate[required] no-margin" name="exp_year" id="exp_year" value="" placeholder="YEAR" onkeypress="return AvoidSpace(event)" maxlength="4">
              </div>
          </div>
      </div>
      <div class="col-md-6">
          <label for="security_code" class="control-label col-md-12"> SECURITY CODE </label>
          <div class="col-md-12 padding-left-0">
              <div class="input-group col-md-12">
                  <input type="text" class="form-control validate[required,condRequired[maxSize[4]]] custom[integer] no-margin" name="security_code" id="security_code" value="" maxlength="4" onkeypress="return AvoidSpace(event)">
              </div>
          </div>
      </div>
  </div>
  <div id="card_div" hidden>
      <div class="form-group">
          <div class="col-md-12">
              <label for="nick_name" class="control-label col-md-12"> CARD NICKNAME </label>
              <div class="">
                  <div class="input-group">
                      <input type="text" class="form-control no-margin" name="nick_name1" id="nick_name1" value="">
                  </div>
              </div>
          </div>
      </div>
      <div class="form-group">
          <div class="col-md-12">
              <label for="card_name" class="control-label col-md-12"> NAME ON CARD </label>
              <div class="">
                  <div class="input-group">
                      <input type="text" class="form-control validate[required] no-margin" name="card_name1" id="card_name1" value="">
                  </div>
              </div>
          </div>
      </div>
      <div class="form-group">
          <div class="col-md-12">
              <label for="card_number" class="control-label col-md-12"> CARD NUMBER </label>
              <div class="">
                  <div class="input-group">
                      <input type="text" class="form-control validate[required, condRequired[creditCard]] no-margin" name="card_number1" id="card_number1" value="">
                      <input type="hidden"  name="card_type1" id="card_type1" value="">
                  </div>
              </div>
          </div>
      </div>
      <div class="form-group">
          <div class="col-md-6" style="padding-bottom: 0%;">
              <label for="exp_date" class="control-label col-md-12"> EXPIRATION DATE </label>
              <div class="col-md-3">
                  <div class="input-group col-md-12">
                      <input type="text" class="form-control validate[required]" name="exp_month1" id="exp_month1" value="" placeholder="MONTH">
                  </div>
              </div>
              <div class="col-md-3">
                  <div class="input-group col-md-12">
                      <input type="text" class="form-control validate[required]" name="exp_year1" id="exp_year1" value="" placeholder="YEAR">
                  </div>
              </div>
          </div>
          <div class="col-md-6" style="padding-bottom: 0%;">
              <label for="security_code" class="control-label col-md-12"> SECURITY CODE </label>
              <div class="col-md-6" style="padding-bottom: 0%;">
                  <div class="input-group col-md-12">
                      <input type="text" class="form-control validate[required,condRequired[maxSize[4]]]" name="security_code1" id="security_code1" value="" maxlength="4">
                  </div>
              </div>
          </div>
      </div>
      <div class="form-group">
          <div class="col-md-12">
              <label for="default" class="control-label col-md-12"> </label>
              <div class="col-md-12">
                  <div class="input-group col-md-12">
                      <input type="checkbox" name="default1" id="default1" value="1"> PRIMARY PAYMENT METHOD
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="form-group">
      <div class="col-md-12">
          <label for="" class="control-label col-md-12"> BILLING ADDRESS </label>
          <div class="">
              <div class="input-group">
                  <input type="text" class="form-control validate[required] no-margin" name="caddress1" id="caddress1" value="" placeholder="STREET ADDRESS">
              </div>
          </div>
      </div>
  </div>
  <div class="form-group">
      <div class="">
          <label for="" class="control-label col-md-12"> </label>
          <div class="col-md-12">
              <div class="input-group ">
                  <input type="text" class="form-control no-margin" name="caddress2" id="caddress2" value="" placeholder="ADDRESS LINE 2">
              </div>
          </div>
      </div>
  </div>
  <div class="form-group">
      <div class="col-md-12">
          <label for="" class="control-label col-md-12"> </label>
          <div class="">
              <div class="input-group">
                  <input type="text" class="form-control validate[required] no-margin" name="ccity" id="ccity" value="" placeholder="CITY">
              </div>
          </div>
      </div>
  </div>
  <div class="form-group">
      <div class="col-md-12">
          <label for="" class="control-label col-md-12"> </label>
          <div class="col-md-4 no-padding">
              <div class="input-group">
                  <input type="text" class="form-control validate[required] custom[integer] no-margin" name="cpostal_code" id="cpostal_code" value="" placeholder="ZIP/POSTAL CODE">
              </div>
          </div>
          <div class="col-md-8">
              <div class="input-group">
                  <input type="text" class="form-control validate[required] no-margin" name="ccountry" id="ccountry" value="" placeholder="COUNTRY">
              </div>
          </div>
      </div>
  </div>
  <div class="form-group">
      <div class="col-md-12">
          <label for="cphone" class="control-label col-md-12">BILLING PHONE NUMBER </label>
          <div class="col-md-4 no-padding">
              <div class="input-group">
                  <input type="text" class="form-control validate[required] no-margin" name="com_ccode" id="com_ccode" value="" placeholder="COUNTRY CODE">
              </div>
          </div>
          <div class="col-md-8">
              <div class="input-group">
                  <input type="text" class="form-control validate[required] custom[integer] no-margin" name="cphone" id="cphone" placeholder="COUNTRY PHONE" value="">
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal-footer">
 <div class="form-group">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

          <button type="submit" value="submit" name="Add Card" class="btn btn-primary pull-right">Add Card</button>
      </div>
  </form>
</div>
</div>
</div>
</div>
<div class="modal fade" id="updatePreModal" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Update Prep Note</h4>
</div>
<div class="modal-body">
<form method="post" id="prep_note_validate" name="prep_note_validate" class="form-horizontal" action="{{ url('quote/update-prep-note') }}">
{{ csrf_field() }}
<input type="hidden" name="customer_requirement_id" value="{{ $place_order->id }}">
<div class="form-group">
  <div class="col-md-12">
      <label class="control-label col-md-12 text-left"></label>
      <div class="input-group">
          <textarea class="form-control no-margin " name="prep_note" id="prep_note">{{ $place_order->prep_note }}</textarea>
      </div>
  </div>
</div>
</div>
<div class="modal-footer">
<div class="form-group">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" value="submit" name="Add Card" class="btn btn-primary pull-right">Update</button>
  </div>
</form>
</div>
</div>
</div>
</div>
<div class="modal fade" id="updateInspectionModal" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Update Inspection Note</h4>
</div>
<div class="modal-body">
<form method="post" id="inspection_note_validate" name="inspection_note_validate" class="form-horizontal" action="{{ url('quote/update-inspection-note') }}">
{{ csrf_field() }}
<input type="hidden" name="customer_requirement_id" value="{{ $place_order->id }}">
<div class="form-group">
  <div class="col-md-12">
      <label class="control-label col-md-12 text-left"></label>
      <div class="input-group">
          <textarea class="form-control no-margin" name="inspection_note" id="inspection_note">{{ $place_order->inspection_note }}</textarea>
      </div>
  </div>
</div>
</div>
<div class="modal-footer">
<div class="form-group">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="submit" value="submit" name="Add Card" class="btn btn-primary pull-right">Update</button>
</div>
</form>
</div>
</div>
</div>
</div>
<div class="modal fade" id="updatePreShipmentModal" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Update Pre Shipment Note</h4>
</div>
<div class="modal-body">
<form method="post" id="pre_shipment_validate" name="pre_shipment_validate" class="form-horizontal" action="{{ url('quote/update-pre_shipment-note') }}">
{{ csrf_field() }}
<input type="hidden" name="customer_requirement_id" value="{{ $place_order->id }}">
<div class="form-group">
  <div class="col-md-12">
      <label class="control-label col-md-12 text-left"></label>
      <div class="input-group">
          <textarea class="form-control no-margin" name="pre_shipment_note" id="pre_shipment_note">{{ $place_order->pre_shipment_note }}</textarea>
      </div>
  </div>
</div>
</div>
<div class="modal-footer">
<div class="form-group">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" value="submit" name="Add Card" class="btn btn-primary pull-right">Update</button>
  </div>
</form>
</div>
</div>
</div>
</div>
@endsection
@section('js')
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
{!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
{!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}


<script type="text/javascript">
$(document).ready(function () {

var prefix = 's2id_';
$("form[id^='validate']").validationEngine('attach', {
promptPosition: "bottomRight", scroll: false,
prettySelect: true,
usePrefix: prefix
});
});
$("#card_number").blur(function() {
var result = getCreditCardType($("#card_number").val());
$("#card_type").val(result);
});
$("#card_number1").blur(function() {
var result = getCreditCardType($("#card_number1").val());
$("#card_type1").val(result);
});
function getCreditCardType(card_number)
{
var result = "unknown";
if (/^5[1-5]/.test(card_number)) { result = "mastercard"; }
else if (/^4/.test(card_number)) { result = "visa"; }
else if (/^3[47]/.test(card_number)) { result = "amex"; }
return result;
}
function AvoidSpace(event) {
    var k = event ? event.which : window.event.keyCode;
    if (k == 32) return false;
}
</script>
@endsection