<html>
<table>
    <tr>
        <th>Id</th>
        <th>Order Number</th>
        <th>Product Name</th>
        <th>Date Ordered</th>
        <th>Status</th>
    </tr>
    @foreach($orders as $order)
        <tr>
            <td>{{ $order->order_id }}</td>
            <td>{{ $order->order_no }}</td>
            <td>{{ $order->product_name }}</td>
            <td>{{ date('m/d/Y',strtotime($order->created_at)) }}</td>
            <td>{{ $orderStatus[$order->is_activated] }}</td>
        </tr>
    @endforeach
</table>
</html>