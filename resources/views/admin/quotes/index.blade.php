@extends('layouts.admin.app')
@section('title', 'Quotes')
@section('css')
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
    {!! Html::style('assets/dist/css/style.css') !!}
    <style>
        .dataTables_wrapper{ margin-bottom:30px; }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-anchor"></i> Quotes</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-anchor"></i> Quotes</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Quotes</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <table id="active_quote" class="table datatable dt-responsive" style="width:100%;">
                    <thead>
                    <tr>
                        <th class="col-md-6">Active Quote Requests</th>
                        <th class="col-md-2">Company</th>
                        <th class="col-md-2">Date Quoted</th>
                        <th class="col-md-2">View & Approve Quote</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <table id="expire_quote" class="table datatable dt-responsive" style="width:100%;">
                    <thead>
                    <tr>
                        <th class="col-md-6">Expired Quote Requests</th>
                        <th class="col-md-2">Company</th>
                        <th class="col-md-2">Date Quoted</th>
                        <th class="col-md-2">View & Approve Quote</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <table id="past_quote" class="table datatable dt-responsive" style="width:100%;">
                    <thead>
                    <tr>
                        <th class="col-md-6">Past Quote Requests</th>
                        <th class="col-md-2">Company</th>
                        <th class="col-md-2">Date Quoted</th>
                        <th class="col-md-2">View & Approve Quote</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @include('layouts.admin.includes.message_boxes', ['item' => 'quote', 'delete' => true])
@endsection
@section('js')
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $("#active_quote").DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url("admin/datatables/activequotes") !!}',
                columns: [
                    {data: 'product', name: 'product'},
                    {data: 'company', name: 'company'},
                    {data: 'date_ordered', name: 'date_ordered'},
                    {data: 'view_quote', name: 'view_quote' , orderable: false, searchable: false},
                ]
            });
            table.column('2:visible').order('desc').draw();
            var table1 = $("#expire_quote").DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url("admin/datatables/expirequotes") !!}',
                columns: [
                    {data: 'product', name: 'product'},
                    {data: 'company', name: 'company'},
                    {data: 'date_ordered', name: 'date_ordered'},
                    {data: 'view_quote', name: 'view_quote' , orderable: false, searchable: false},
                ]
            });
            table1.column('2:visible').order('desc').draw();
            var table2 = $("#past_quote").DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url("admin/datatables/pastquotes") !!}',
                columns: [
                    {data: 'product', name: 'product'},
                    {data: 'company', name: 'company'},
                    {data: 'date_ordered', name: 'date_ordered'},
                    {data: 'view_quote', name: 'view_quote' , orderable: false, searchable: false},
                ]
            });
            table2.column('2:visible').order('desc').draw();

            $('#active_quote_filter').append("<a href='{{ url('admin/active-quote-export') }}'><button class='button btn-export'>Export</button>");

            $('#expire_quote_filter').append("<a href='{{ url('admin/expire-quote-export') }}'><button class='button btn-export'>Export</button>");

            $('#past_quote_filter').append("<a href='{{ url('admin/past-quote-export') }}'><button class='button btn-export'>Export</button>");

        });
    </script>
@endsection