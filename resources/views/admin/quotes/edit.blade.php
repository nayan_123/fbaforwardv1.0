@extends('layouts.admin.app')
@section('title', 'Quote Details')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/custom_quotes.css') !!}
<style>
.product-name{ text-decoration: none; overflow: hidden; white-space: nowrap;  text-overflow: ellipsis;  }
.table>tbody>tr>td{ text-align: left !important; }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1> Quote Details </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('quote') }}"><i class="fa fa-anchor"></i>Shipping Quotes</a></li>
            <li class="active"><a href="{{ url('quote/edit/') }}">  Quote Details </a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
        <div class="box">
             <div class="box-body no-padding">
                <div class="row">
                          <div class="col-md-12 ">
                        <div class="col-md-12 no-padding product-date">
                            <p class="pull-left quote-top-dates"><b>{{ $company }}</b>
                                @if(isset($details->created_at))
                                    Requested on {{ date('F d, Y',strtotime($details->created_at)) }}
                                @endif
                            </p>
                            <p class="pull-right red-color quote-top-dates">
                                @if(isset($details->expire_date))
                                    Expires on {{ date('F d, Y',strtotime($details->expire_date)) }}
                                @endif
                            </p>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="col-md-3 quotes no-padding width27">
                                <div class="col-md-12 product-data product-name">
                                    <b>Product : </b>
                                    <span>
                                        @if(isset($details))
                                        @if(empty($details->customer_requirement_details_product_name))
                                             @if(!empty($details->product_nick_name)) 
                                                {{ $details->product_nick_name }} 
                                            @else 
                                                {{ $details->product_name }} 
                                            @endif
                                        @else
                                            {{ $details->customer_requirement_details_product_name }}
                                        @endif
                                        @endif

                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3 quotes no-padding width12">
                                <div class="col-md-12 product-data">
                                    <b>Units : </b><span> @if(isset($details)){{ $details->qty_per_box }} @endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 quotes no-padding width18">
                                <div class="col-md-12 product-data">
                                    <b>Cartons : </b><span> @if(isset($details)){{ $details->no_boxs }}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 quotes no-padding width18">
                                <div class="col-md-12 product-data">
                                    <b>Weight : </b><span>@if(isset($details)){{ $details->chargeable_weight }}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 quotes no-padding width25">
                                <div class="col-md-12 product-data border-right-0">
                                    <b>Volume : </b><span> @if(isset($details)){{ $details->shipment_volume }}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 quotes no-padding width27">
                                <div class="col-md212 product-data ">
                                    <b>Inbound Shipping Method : </b>
                                    <span>
                                    @if(count($method)>1)
                                        TBD
                                    @else
                                       {{$method[0]['shipping_name'] }}
                                    @endif
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3 quotes no-padding width12">
                                <div class="col-md-12 product-data ">
                                    <b>Type : </b><span>@if(isset($details)) {{ $details->shipment_type_short_name }}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 quotes no-padding width18">
                                <div class="col-md-12 product-data ">
                                    <b>Incoterms : </b><span>@if(isset($details)) {{ $details->incoterm_short_name }}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 quotes no-padding width18">
                                <div class="col-md-12 product-data ">
                                    <b>Port of Origin : </b><span>@if(isset($details)) {{ $details->port_of_origin }}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 quotes no-padding width25">
                                <div class="col-md-12 product-data border-right-0">
                                    <b>Approx. Goods Ready Date : </b>
                                    <span>
                                        @if(isset($details))
                                        @if($details->goods_ready_date)
                                            {{ date('m/d/Y',strtotime($details->goods_ready_date)) }}
                                        @endif
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-12 no-padding">
            <div class="box2 col-md-9 no-padding">
                <div class="box-header with-border quote-summery-heading no-border">
                    <h3 class="box-title">Quote Summery</h3>
                    <div class="box-tools pull-right"><a id="print"><i class="fa fa-print"></i></a>&nbsp;&nbsp;&nbsp; <a id="quote_print_link" target="_blank" href="{{ url('quote/print-quote/'.$method_id.'/'.$quote_id.'/save') }}"><i class="fa fa-save"></i></a>
                </div>
                </div>
                 <div class="box-body no-padding">
                    <div class="col-md-12 no-padding">
                        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                            @foreach($method as $key=>$methods)
                                {{--*/ $shipping_method_id = $methods['shipping_method_id'] /*--}}
                                @if($key==0)
                                <li class="nav-item col-md-4 nav-tab no-padding active" id="tab{{ $key+1 }}">
                                    <a class="nav-link active text-capitalize" data-toggle="tab" href="javascript:void(0)" onclick="getdata('{{$key+1}}','{{$details->id}}','{{ $shipping_method_id }}','{{$details->order_type}}')">{{$methods['shipping_name']}}</a>
                                </li>
                                @else
                                <li class="nav-item col-md-4 nav-tab no-padding" id="tab{{ $key+1 }}">
                                    <a class="nav-link text-capitalize" data-toggle="tab" href="javascript:void(0)" onclick="getdata('{{$key+1}}','{{$details->id}}','{{ $shipping_method_id }}','{{$details->order_type}}')">{{$methods['shipping_name']}}</a>
                                </li>
                                @endif
                            @endforeach
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            @foreach($method as $key=>$methods)
                                {{--*/ $shipping_method_id = $methods['shipping_method_id'] /*--}}
                                @if($key==0)
                                    <div class="tab-pane fade active in" id="tabs{{$key+1}}" name="tabs" role="tabpanel" aria-labelledby="{{$shipping_method_id}}">
                                @else
                                    <div class="tab-pane fade" id="tabs{{$key+1}}" name="tabs" role="tabpanel" aria-labelledby="{{$shipping_method_id}}">
                                @endif
                                        {{--*/ $charge_qty = 1 /*--}}
                                        {{--*/ $shipping_sub_total = 0 /*--}}
                                        {{--*/$custom_sub_total=0;/*--}}
                                        {{--*/$receive_forward_sub_total=0;/*--}}
                                        {{--*/ $prep_price = "" /*--}}
                                        {{--*/ $prep_sub_total = 0 /*--}}
                                        {{--*/ $trucking_qty="" /*--}}
                                        {{--*/ $port_fee = 0 /*--}}
                                        {{--*/ $port_fee_name =  env('PORT_FEE_NAME') /*--}}
                                        {{--*/ $trucking_name =  env('TRUCKING_NAME') /*--}}
                                        {{--*/ $isf_rate =  env('ISF_RATE') /*--}}
                                        {{--*/ $isf_name =  env('ISF_NAME') /*--}}
                                        {{--*/ $entry_rate = env('ENTRY_RATE') /*--}}
                                        {{--*/ $entry_name = env('ENTRY_NAME') /*--}}
                                        {{--*/ $bond_rate =  env('BOND_RATE') /*--}}
                                        {{--*/ $bond_name =  env('BOND_NAME') /*--}}
                                        {{--*/ $container_unloading= env('CONTAINER_UNLOADING') /*--}}
                                        {{--*/ $palletizing= env('PALLETIZING') /*--}}
                                        {{--*/ $amazon_approve_pallet= env('AMAZON_APPROVE_PALLET') /*--}}
                                        {{--*/ $pallet_charge= env('pallet_charge') /*--}}
                                        @if($details->units==1)
                                            {{--*/$trucking_qty=round($details->shipment_volume / env('PALLET_CBM')) /*--}}
                                        @else
                                            {{--*/$trucking_qty=round($details->shipment_volume / env('PALLET_CFT'))/*--}}
                                        @endif
                                        @if($trucking_qty > env('TRUCKING_QTY'))
                                            {{--*/ $trucking_price = ($trucking_qty -  env('TRUCKING_QTY')) * env('TRUCKING_ADDITIONAL_PRICE') + env('TRUCKING_PRICE')/*--}}
                                        @else
                                            {{--*/ $trucking_price =  env('TRUCKING_PRICE') /*--}}
                                        @endif
                                        @if($details->order_type==1)
                                        <table class="table quote-summery-table">
                                            <thead>
                                            <tr>
                                                <th class="col-md-6">Shipping</th>
                                                <th class="col-md-2">Rate</th>
                                                <th class="col-md-2">Qty</th>
                                                <th class="col-md-2">Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="col-md-6">{{$details->shipping_name}}</td>
                                                <td class="col-md-2">${{$details->shipment_type_rate}}</td>
                                                <td class="col-md-2">{{$details->shipment_volume}}</td>
                                                <td class="col-md-2">${{number_format(($details->shipment_type_rate*$details->shipment_volume))}}</td>
                                            </tr>
                                            {{--*/ $shipping_sub_total+=($details->shipment_type_rate*$details->shipment_volume)/*--}}
                                            @foreach($detail_charge as  $charge_key=>$charge_value )
                                                <tr>
                                                    <td class="col-md-6">{{$charge_value->name}}</td>
                                                    <td class="col-md-2">${{$charge_value->charge}}</td>
                                                    <td class="col-md-2">{{$charge_qty}}</td>
                                                    <td class="col-md-2">${{number_format(($charge_value->charge * $charge_qty))}}</td>
                                                </tr>
                                                {{--*/ $shipping_sub_total+=($charge_value->charge * $charge_qty)/*--}}
                                            @endforeach
                                            @if($details->shipping_name!=="undefined")
                                                @if($details->shipping_method_id == 2)
                                                    {{--*/  $port_fee= env('PORT_FEE')/*--}}
                                                    <tr>
                                                        <td class="col-md-6">{{$port_fee_name}}</td>
                                                        <td class="col-md-2">${{$port_fee}}</td>
                                                        <td class="col-md-2">{{$charge_qty}}</td>
                                                        <td class="col-md-2">${{number_format(($port_fee*$charge_qty))}}</td>
                                                    </tr>

                                                    <tr>
                                                        <td class="col-md-6"> {{$trucking_name}}</td>
                                                        <td class="col-md-2">$99/first 3 pallets, $25/additional pallets</td>
                                                        <td class="col-md-2">{{$trucking_qty}}</td>
                                                        <td class="col-md-2">${{number_format(($trucking_price))}}</td>
                                                    </tr>
                                                @elseif($details->shipping_method_id == 1)
                                                    @if($details->shipment_type_id==1)
                                                        {{--*/ $port_fee= env('OCEAN_LCL_PORT_FEE') /*--}}
                                                    @else
                                                        {{--*/ $port_fee= env('OCEAN_PORT_FEE') /*--}}
                                                    @endif
                                                    <tr>
                                                        <td class="col-md-6">{{$port_fee_name}}</td>
                                                        <td class="col-md-2">${{$port_fee}}</td>
                                                        <td class="col-md-2">{{$charge_qty}}</td>
                                                        <td class="col-md-2">${{number_format(($port_fee*$charge_qty))}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col-md-6">{{$trucking_name}}</td>
                                                        <td class="col-md-2">${{$details->trucking_rate}}/pallet</td>
                                                        <td class="col-md-2">{{$trucking_qty}}</td>
                                                        <td class="col-md-2">{{--*/$trucking_price = ($details->trucking_rate*$trucking_qty) /*--}}${{number_format(($trucking_price))}}</td>
                                                    </tr>
                                                @else
                                                    @if($details->shipment_type_id==1)
                                                        {{--*/ $port_fee= env('OCEAN_LCL_PORT_FEE') /*--}}
                                                    @else
                                                        {{--*/ $port_fee= env('OCEAN_PORT_FEE') /*--}}
                                                    @endif
                                                    <tr>
                                                        <td class="col-md-6">{{$port_fee_name}}</td>
                                                        <td class="col-md-2">${{$port_fee}}</td>
                                                        <td class="col-md-2">{{$charge_qty}}</td>
                                                        <td class="col-md-2">${{number_format(($port_fee*$charge_qty))}}</td>
                                                    </tr>
                                                    {{--*/ $trucking_price = 0; /*--}}
                                                @endif
                                                <tr>
                                                    <td class="col-md-6">{{ env('WIRE_TRANS_NAME') }}</td>
                                                    <td class="col-md-2">${{ env('WIRE_TRANS_FEE') }}</td>
                                                    <td class="col-md-2">&nbsp;</td>
                                                    <td class="col-md-2">${{number_format((env('WIRE_TRANS_FEE')))}}</td>
                                                </tr>
                                                {{--*/$shipping_sub_total+=$port_fee*$charge_qty/*--}}
                                                {{--*/$shipping_sub_total+=$trucking_price/*--}}
                                                {{--*/ $shipping_sub_total +=env('WIRE_TRANS_FEE') /*--}}
                                            @endif
                                            <tr>
                                                <td colspan="3"><b>Shipping Subtotal</b></td>
                                                <td><b>${{number_format(($shipping_sub_total))}}</b></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        @endif
                                        @if($details->order_type==1 || $details->order_type==3)
                                        <table class="table quote-summery-table">
                                            <thead>
                                            <tr>
                                                <th class="col-md-6">Customs</th>
                                                <th class="col-md-2">Rate</th>
                                                <th class="col-md-2">Qty</th>
                                                <th class="col-md-2">Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($details->shipping_method_id == 1)
                                                <tr>
                                                    <td class="col-md-6">{{$isf_name}}</td>
                                                    <td class="col-md-2">${{$isf_rate}}</td>
                                                    <td class="col-md-2">{{$charge_qty}}</td>
                                                    <td class="col-md-2">${{number_format(($isf_rate * $charge_qty))}}</td>
                                                </tr>
                                                {{--*/$custom_sub_total= ($isf_rate*$charge_qty) /*--}}
                                            @endif
                                            <tr>
                                                <td class="col-md-6">{{$entry_name}}</td>
                                                <td class="col-md-2">${{$entry_rate}}</td>
                                                <td class="col-md-2">{{$charge_qty}}</td>
                                                <td class="col-md-2">${{number_format(($entry_rate*$charge_qty))}}</td>
                                            </tr>
                                            <tr>
                                                <td class="col-md-6">{{$bond_name}}</td>
                                                <td class="col-md-2">${{$bond_rate}}</td>
                                                <td class="col-md-2">{{$charge_qty}}</td>
                                                <td class="col-md-2">${{number_format(($bond_rate*$charge_qty))}}</td>
                                            {{--*/ $custom_sub_total+= (int)($entry_rate*$charge_qty) + ($bond_rate*$charge_qty)/*--}}
                                            <tr>
                                            <tr>
                                                <td colspan="3"><b>Customs Subtotal </b></td>
                                                <td><b>${{number_format(($custom_sub_total))}}</b></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        @endif
                                        <table class="table quote-summery-table">
                                            <thead>
                                            <tr>
                                                <th class="col-md-6">Receiving & Forwarding</th>
                                                <th class="col-md-2">Rate</th>
                                                <th class="col-md-2">Qty</th>
                                                <th class="col-md-2">Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($details->shipment_type_id >1)
                                                <tr>
                                                    <td class="col-md-6">Container Unloading</td>
                                                    <td class="col-md-2">${{$details->container_unload_rate}}</td>
                                                    <td class="col-md-2">{{$charge_qty}}</td>
                                                    <td class="col-md-2">${{number_format(($details->container_unload_rate*$charge_qty))}}</td>
                                                </tr>
                                            @endif
                                            {{--*/$receive_forward_sub_total+=$details->container_unload_rate*$charge_qty/*--}}
                                            @if($details->outbound_method_id >1)
                                                <tr>
                                                    <td class="col-md-6">{{$palletizing}}</td>
                                                    <td class="col-md-2">${{$pallet_charge}}</td>
                                                    <td class="col-md-2">{{$trucking_qty}}</td>
                                                    <td class="col-md-2">${{number_format(($pallet_charge*$trucking_qty))}}</td>
                                                </tr>
                                                {{--*/$receive_forward_sub_total+=$pallet_charge*$trucking_qty/*--}}
                                                <tr>
                                                    <td class="col-md-6">{{$amazon_approve_pallet}}</td>
                                                    <td class="col-md-2">${{$pallet_charge}}</td>
                                                    <td class="col-md-2">{{$trucking_qty}}</td>
                                                    <td class="col-md-2">${{number_format(($pallet_charge*$trucking_qty))}}</td>
                                                </tr>
                                                {{--*/$receive_forward_sub_total+=$pallet_charge*$trucking_qty/*--}}
                                            @endif
                                            <tr>
                                                <td class="col-md-6">{{$details->outbound_name}}</td>
                                                <td class="col-md-2">&nbsp;</td>
                                                <td class="col-md-2">&nbsp;</td>
                                                <td class="col-md-2"><i>varies</i></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><b>Receiving & Forwarding Subtotal </b></td>
                                                <td><b>${{number_format(($receive_forward_sub_total))}}</b></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table class="table quote-summery-table">
                                            <thead>
                                            <tr>
                                                <th class="col-md-6">Prep & Inspection</th>
                                                <th class="col-md-2">Rate</th>
                                                <th class="col-md-2">Qty</th>
                                                <th class="col-md-2">Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($detail_prep as $prep_key=>$prep_value )
                                            {{--*/$prep_price = $prep_value->price * $details->total/*--}}
                                            @if($prep_price % 1 !=0)
                                                <tr>
                                                    <td class="col-md-6">{{$prep_value->service_name}}</td>
                                                    <td class="col-md-2">${{$prep_value->price}}</td>
                                                    <td class="col-md-2">{{$details->total}}</td>
                                                    <td class="col-md-2">${{number_format(($prep_price))}}</td>
                                                    {{--*/$prep_sub_total+=$prep_price/*--}}
                                                </tr>
                                            @else
                                                <tr>
                                                    <td class="col-md-6">{{$prep_value->service_name}}</td>
                                                    <td class="col-md-2">${{$prep_value->price}}</td>
                                                    <td class="col-md-2">{{$details->total}}</td>
                                                    <td class="col-md-2">${{number_format(($prep_price))}}</td>
                                                    {{--*/$prep_sub_total+=$prep_price/*--}}
                                                </tr>
                                            @endif
                                            @endforeach
                                            <tr>
                                                <td colspan="3"><b>Prep & Inspection</b></td>
                                                <td><b>${{number_format(($prep_sub_total))}}</b></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        {{--*/$total = $shipping_sub_total+$prep_sub_total+$custom_sub_total+$receive_forward_sub_total/*--}}
                                        <table class="table quote-summery-table">
                                            <thead>
                                        <tr>
                                            <td class="col-md-10"><b>Total Deposit Due</b></td>
                                            <td class="col-md-2"><b>${{number_format(($total))}}</b></td>
                                        </tr>
                                            </thead>
                                        </table>
                        </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
    <div class="box2 col-md-3 action-box no-padding">
        <div class="box-header with-border">
            <h3 class="box-title">ACTIONS</h3>
        </div>
        <div class="box-body">
            <span class="col-md-12"><b>Quote Selected:</b> </span>
            <p id="freight" class="col-md-12 text-center">{{$method[0]['shipping_name'] }}</p>
            @if($quote == 'past_quotes')
                <form method="post" action="{{ url('quote/resubmit-quote?user_role='.$role_id) }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="customer_requirement_id" value="{{ $details->id }}">


                <button type="submit" class="btn btn-primary col-md-12 margintop10">Resubmit Quote</button>

                </form>
            @else
                @if($user_role=='4')
                    <a class="btn btn-primary col-md-12 margintop10" target="_blank" href="https://accounts.zoho.com/u/h#home">View Company in CRM</a>
                @else
                    <a class="btn btn-primary col-md-12 margintop10" id="approvelink" href="{{url('order/create/?id='.$details->id.'&method_id='.$method[0]['shipping_method_id'])}}">Approved, Place Order</a>
                @endif
                @if($details->order_type == 1)
                    <a class="btn btn-primary btn-request-quote col-md-12 margintop10" onclick="revisionquote('{{$details->id}}')">Request Quote Revision</a>
                    <a class="btn btn-primary btn-reject-quote col-md-12 margintop10" onclick="rejectquote('{{$details->id}}')">Reject Quote</a>
                @endif
            @endif
        </div>
    </div>
    <div class="box2 col-md-3 action-box no-padding">
        @if($user_role=='4')
            <div class="box-header with-border">
                <h3 class="box-title">HISTORY</h3>
            </div>
            <div class="box-body">
                @foreach($histories as $history)
                    <p>{{ \Carbon\Carbon::parse($history->created_at)->format('m/d/Y') }}
                        @if($history->quote_history_status == 1)
                            {{ "Quote Requested" }}
                        @elseif($history->quote_history_status == 2)
                            {{ "Quote Submitted" }}
                        @elseif($history->quote_history_status == 3)
                            {{ "Quote Approved" }}
                        @elseif($history->quote_history_status == 4)
                            {{ "Quote Rejected" }}
                        @elseif($history->quote_history_status == 5)
                            {{ "Quote Expired" }}
                        @elseif($history->quote_history_status == 6)
                            {{ "Revision Requested" }}
                        @endif
                    </p>
                @endforeach
            </div>
        @else
            <div class="box-header with-border">
                <h3 class="box-title">QUOTE TERMS</h3>
            </div>
            <div class="box-body">
                <small>By placing an order, you declare that the information you inputted to receive the quote is correct to the best of your knowledge. If weights, dimensions, volume, or count need to be adjusted based on the information your supplier provides after the order is placed, you will be responsible for the additional charges without notification. The additional charges will be updated on your order and can be updated if requested up until your shipment has departed origin.</small>
            </div>
        @endif
    </div>
    </div>
    </div>
</div>
        </div>
        <div class="modal fade" id="rejectquotemodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Reject Shipping Quote</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" id="main">
                                {!! Form::open(['url' =>  'quote/rejectshippingquote', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'rejectquoteform']) !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::hidden('require_id',old('require_id'), ['id'=>'require_id']) !!}
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <select name="reason" id="reason" class="validate[required] form-control">
                                                        <option value="">Select Reason</option>
                                                        <option value="1">High Price</option>
                                                        <option value="2">No Longer in need of Services</option>
                                                        <option value="3">Wrong Quote</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    {!! Form::submit('  Submit  ', ['class'=>'btn btn-primary pull-right margin-right-0',  'id'=>'add']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
</section>

<div id="print_quote"></div>

@endsection
@section('js')
    {!! Html::script("assets/dist/js/number-divider.js") !!}
    {!! Html::script("assets/dist/js/number-divider.min.js") !!}
    <script type="text/javascript">
        $('.divide').divide();


    $('#print').click(function(){

        var method_id = "{{ $method_id }}";
        var quote_id = "{{ $quote_id }}";
        var html = '';

        $.ajax({
            headers: {
            'X-CSRF-Token':  "{{ csrf_token() }}"
            },
            method: 'GET', // Type of response and matches what we said in the route
            url: '/quote/print-quote/'+method_id+'/'+quote_id+'/print', // This is the url we gave in the route
            data: {
            }, // a JSON object to send back
            success: function (response) {
                
                $('#print_quote').hide();
                $('#print_quote').html(response);

                var prtContent = document.getElementById("print_quote");
                var WinPrint = window.open('', '', 'left=0,top=0,width=500,height=200,toolbar=0,scrollbars=0,status=0');
                WinPrint.document.write(prtContent.innerHTML);
                WinPrint.document.close();
                WinPrint.focus();
                WinPrint.print();
                WinPrint.close();
                
                $('#print_quote').hide();

            }
        });
    });


    function getdata(key,id,method_id,order_type)
    {
        var customer_requirement_id = "{{ $quote_id }}";
        $.ajax({
            headers: {
            'X-CSRF-Token':  "{{ csrf_token() }}"
            },
            method: 'POST', // Type of response and matches what we said in the route
            url: '/quote/getshippingquote', // This is the url we gave in the route
            data: {
            'id': id,
            'method_id':method_id,
            'order_type' : order_type,
            }, // a JSON object to send back
            success: function (response) { // What to do if we succeed
                response = $.parseJSON(response);
                console.log(response);
                var total = 0;
                var table='';
                var method_id = response.method_id;
                $.each( response.details, function( detail_key, value ) {
                    var shipment_type = value.shipment_type_short_name;
                    var shipment_type_rate = value.shipment_type_rate;
                    var incoterm = value.incoterm_short_name;
                    var order_type= value.order_type;
                    var trucking_rate = value.trucking_rate;
                    var shipment_type_price = shipment_type_rate * value.shipment_volume;
                    var charge_qty = '1';
                    var shipping_sub_total = 0;
                    var prep_price = '';
                    var prep_sub_total = 0;
                    var trucking_qty = '';
                    var port_fee = 0;
                    var min_order_fee = 0;
                    var port_fee_name = '{{ env('PORT_FEE_NAME') }}';
                    var trucking_name = '{{ env('TRUCKING_NAME') }}';
                    var isf_rate = '{{ env('ISF_RATE') }}';
                    var isf_name = '{{ env('ISF_NAME') }}';
                    var entry_rate = '{{ env('ENTRY_RATE') }}'
                    var entry_name = '{{ env('ENTRY_NAME') }}';
                    var bond_rate = '{{ env('BOND_RATE') }}'
                    var bond_name = '{{ env('BOND_NAME') }}';
                    var container_unloading = '{{ env('CONTAINER_UNLOADING') }}';
                    var palletizing = '{{ env('PALLETIZING') }}';
                    var amazon_approve_pallet = '{{ env('AMAZON_APPROVE_PALLET') }}';
                    var pallet_charge = '{{ env('pallet_charge') }}';
                    var wire_transfer_name ='{{ env('WIRE_TRANS_NAME') }}';
                    var wire_transfer_fee ='{{ env('WIRE_TRANS_FEE') }}';
                    var port_fees = 0;
                    var isf_fees = 0;
                    var entry_fees = 0;
                    var bond_fees = 0;
                    var unload_fees = 0;
                    var pallet_fees = 0;
                    if (value.units == '1')
                        trucking_qty = Math.round(value.shipment_volume / '{{ env('PALLET_CBM') }}')
                    else
                        trucking_qty = Math.round(value.shipment_volume / '{{ env('PALLET_CFT') }}')
                    var method_name = "null";
                    method_name = value.shipping_name;
                    if(order_type==1) {
                        table += '<table class="table quote-summery-table"><thead><tr><th class="col-md-6">Shipping</th><th class="col-md-2">Rate</th><th class="col-md-2">Qty</th><th class="col-md-2">Total</th></tr></thead><tbody><tr><td class="col-md-6">' + value.shipping_name + '</td><td class="col-md-2">$' + shipment_type_rate + '</td><td class="col-md-2">' + value.shipment_volume + '</td><td>$' + shipment_type_price.toLocaleString() + '</td></tr>';
                        shipping_sub_total += shipment_type_price;
                        $.each(response.detail_charge, function (charge_key, charge_value) {
                            var charge_price = charge_value.charge * charge_qty;
                            shipping_sub_total += charge_price;
                            table += '<tr><td class="col-md-6">' + charge_value.name + '</td>' + '<td class="col-md-2">$' + charge_value.charge + '</td>' + '<td class="col-md-2">' + charge_qty + '</td>' + '<td class="col-md-2 divide">$' + charge_price.toLocaleString() + '</td></tr>';
                        });
                        var shipping_method_id = "null";
                        if (typeof  value.shipping_name !== 'undefined') {
                            shipping_method_id = value.shipping_method_id;
                            if (value.shipping_method_id == '2') {
                                port_fee = parseInt('{{ env('PORT_FEE') }}');
                                port_fees = port_fee * charge_qty;
                                table += "<tr><td class='col-md-6'>" + port_fee_name + "</td>" + "<td class='col-md-2'>$" + port_fee + "</td>" + "<td class='col-md-2'>" + charge_qty + "</td>" + "<td class='col-md-2'>$" + port_fees.toLocaleString() + "</td></tr>";
                                if (trucking_qty > '{{ env('TRUCKING_QTY') }}') {
                                    trucking_price = parseInt((trucking_qty - '{{ env('TRUCKING_QTY') }}') * '{{ env('TRUCKING_ADDITIONAL_PRICE') }}') + parseInt('{{ env('TRUCKING_PRICE') }}');
                                }
                                else {
                                    trucking_price = parseInt('{{ env('TRUCKING_PRICE') }}');
                                }
                                table += "<tr><td class='col-md-6'>" + trucking_name + "</td>" + "<td class='col-md-2'>$99/first 3 pallets, $25/additional pallets</td>" + "<td class='col-md-2'>" + trucking_qty + "</td>" + "<td class='col-md-2'>$" + trucking_price.toLocaleString() + "</td></tr>";
                            }
                            else if (value.shipping_method_id == '1') {
                                if (value.shipment_type_id == 1)
                                    port_fee = parseInt('{{ env('OCEAN_LCL_PORT_FEE') }}');
                                else
                                    port_fee = parseInt('{{ env('OCEAN_PORT_FEE') }}');
                                port_fees = port_fee * charge_qty;
                                table += "<tr><td class='col-md-6'>" + port_fee_name + "</td>" + "<td class='col-md-2'>$" + port_fee + "</td>" + "<td class='col-md-2'>" + charge_qty + "</td>" + "<td class='col-md-2'>$" + port_fees.toLocaleString() + "</td></tr>";
                                trucking_price = trucking_rate * trucking_qty;
                                table += "<tr><td class='col-md-6'>" + trucking_name + "</td>" + "<td class='col-md-2'>$" + trucking_rate + "/pallet</td>" + "<td class='col-md-2'>" + trucking_qty + "</td>" + "<td class='col-md-2'>$" + trucking_price.toLocaleString() + "</td></tr>";
                            }
                            else{
                                if (value.shipment_type_id == 1)
                                    port_fee = parseInt('{{ env('OCEAN_LCL_PORT_FEE') }}');
                                else
                                    port_fee = parseInt('{{ env('OCEAN_PORT_FEE') }}');
                                    port_fees = port_fee * charge_qty;
                                table += "<tr><td class='col-md-6'>" + port_fee_name + "</td>" + "<td class='col-md-2'>$" + port_fee + "</td>" + "<td class='col-md-2'>" + charge_qty + "</td>" + "<td class='col-md-2'>$" + port_fees.toLocaleString() + "</td></tr>";

                                trucking_price = 0;
                            }
                            table += "<tr><td class='col-md-6'>" + wire_transfer_name + "</td>" + "<td class='col-md-2'>$" + wire_transfer_fee + "</td>" + "<td class='col-md-2'>&nbsp;</td>" + "<td class='col-md-2'>$" + wire_transfer_fee.toLocaleString() + "</td></tr>";
                            shipping_sub_total += port_fees;
                            shipping_sub_total += trucking_price;
                            shipping_sub_total += parseInt(wire_transfer_fee);
                        }
                        table += '<tr><td colspan="3"><b>Shipping Subtotal</b></td><td class="divide"><b>$' + shipping_sub_total.toLocaleString() + '</b></td></tr></tbody></table>';
                    }
                    var custom_sub_total = 0;
                    var receive_forward_sub_total = 0;
                    if(order_type==1 || order_type==3) {
                        table += '<table class="table quote-summery-table"><thead><tr><th class="col-md-6">Customs</th><th class="col-md-2">Rate</th><th class="col-md-2">Qty</th><th class="col-md-2">Total</th></tr></thead><tbody>';
                        if (value.shipping_method_id == 1) {
                            isf_fees = isf_rate * charge_qty;
                            table += "<tr><td class='col-md-6'>" + isf_name + "</td>" + "<td class='col-md-2'>$" + isf_rate + "</td>" + "<td class='col-md-2'>" + charge_qty + "</td>" + "<td class='col-md-2'>$" + isf_fees.toLocaleString() + "</td></tr>";
                            custom_sub_total = (isf_fees);
                        }
                        entry_fees = entry_rate * charge_qty;
                        bond_fees =  bond_rate * charge_qty;
                        table += "<tr><td class='col-md-6'>" + entry_name + "</td>" + "<td class='col-md-2'>$" + entry_rate + "</td>" + "<td class='col-md-2'>" + charge_qty + "</td>" + "<td class='col-md-2'>$" + entry_fees.toLocaleString()  + "</td></tr>";
                        table += "<tr><td class='col-md-6'>" + bond_name + "</td>" + "<td class='col-md-2'>$" + bond_rate + "</td>" + "<td class='col-md-2'>" + charge_qty + "</td>" + "<td class='col-md-2'>$" + bond_fees.toLocaleString() + "</td>";
                        custom_sub_total += (entry_fees) + (bond_fees);
                        table += '<tr><td colspan="3"><b>Customs Subtotal </b></td><td><b>$' + custom_sub_total.toLocaleString() + '</b></td></tr></tbody></table>';
                    }
                    table += '<table class="table quote-summery-table"><thead><tr><th class="col-md-6">Receiving & Forwarding</th><th class="col-md-2">Rate</th><th class="col-md-2">Qty</th><th class="col-md-2">Total</th></tr></thead><tbody>';
                    if (value.shipment_type_id > '1') {
                        unload_fees = value.container_unload_rate * charge_qty;
                        table += "<tr><td>" + container_unloading + "</td><td>$" + value.container_unload_rate + "</td><td>" + charge_qty + "</td><td>$" + unload_fees.toLocaleString() + "</td></tr>";
                        receive_forward_sub_total += value.container_unload_rate * charge_qty;
                    }
                    if (value.outbound_method_id > '1') {
                        pallet_fees = pallet_charge * trucking_qty;
                        table += "<tr><td class='col-md-6'>" + palletizing + "</td>" + "<td class='col-md-2'>$" + pallet_charge + "</td>" + "<td class='col-md-2'>" + trucking_qty + "</td>" + "<td class='col-md-2'>$" + pallet_fees.toLocaleString() + "</td></tr>";
                        receive_forward_sub_total += pallet_fees;
                        table += "<tr><td class='col-md-6'>" + amazon_approve_pallet + "</td>" + "<td class='col-md-2'>$" + pallet_charge + "</td>" + "<td class='col-md-2'>" + trucking_qty + "</td>" + "<td class='col-md-2'>$" + pallet_fees.toLocaleString() + "</td>";
                        receive_forward_sub_total += pallet_fees;
                    }
                    table += "<tr><td class='col-md-6'>" + value.outbound_name + "</td>" + "<td class='col-md-2'>&nbsp;</td>" + "<td class='col-md-2'>&nbsp;</td>" + "<td class='col-md-2'><i>varies</i></td></tr>";
                    table += '<tr><td colspan="3"><b>Receiving & Forwarding Subtotal </b></td><td><b>$' + receive_forward_sub_total.toLocaleString() + '</b></td></tr></tbody></table>';
                    table += '<table class="table quote-summery-table"><thead><tr><th class="col-md-6">Prep & Inspection</th><th class="col-md-2">Rate</th><th class="col-md-2">Qty</th><th class="col-md-2">Total</th></tr></thead><tbody>';
                    $.each(response.detail_prep, function (prep_key, prep_value) {
                        prep_price = prep_value.price * value.total;
                        if (prep_price % 1 != 0)
                            table += "<tr><td class='col-md-6'>" + prep_value.service_name + "</td>" + "<td class='col-md-2'>$" + prep_value.price + "</td>" + "<td class='col-md-2'>" + value.total + "</td>" + "<td class='col-md-2'>$" + prep_price.toFixed(2) + "</td>";
                        else
                            table += "<tr><td class='col-md-6'>" + prep_value.service_name + "</td>" + "<td class='col-md-2'>$" + prep_value.price + "</td>" + "<td class='col-md-2'>" + value.total + "</td>" + "<td class='col-md-2'>$" + prep_price.toLocaleString() + "</td>";
                        prep_sub_total += prep_price;
                    });
                    table += '<tr><td colspan="3"><b>Prep & Inspection</b></td><td><b>$' + prep_sub_total.toLocaleString() + '</b></td></tr></tbody></table>';
                    total = shipping_sub_total + prep_sub_total + custom_sub_total + receive_forward_sub_total;
                    if (total < 300){
                        min_order_fee = 300 - total;
                        table += '<table class="table quote-summery-table"><thead><tr><td  class="col-md-10"><b>Minimum Order Fee</b></td><td class="col-md-2"><b>$' + total.toLocaleString() + '</b></td></tr></thead></table>';
                    }
                    total = shipping_sub_total+prep_sub_total+custom_sub_total+receive_forward_sub_total+min_order_fee;
                    table+='<table class="table quote-summery-table"><thead><tr><td  class="col-md-10"><b>Total Deposit Due</b></td><td class="col-md-2"><b>$'+total.toLocaleString()+'</b></td></tr></thead></table>';

                    $("#freight").text(method_name);
                    $("#freight_id").val(method_id);
                    var url = "{{ url('') }}";

                    $('#quote_print_link').attr("href",""+url+"/quote/print-quote/"+method_id+"/"+customer_requirement_id+"/save");

                });

                var url="{{ url('') }}";
                var link = url+'/order/create/?id='+id+'&method_id='+method_id;
                $("#approvelink").attr("href",link);
                $(".tab-pane").removeClass('active in');
                $("#tabs"+key).addClass('active in');
                $(".tab-pane").empty();
                $("#tabs"+key).append(table);
            }
        });
    }
    function rejectquote(customer_requirement_id) {
        $("#require_id").val(customer_requirement_id);
        $("#rejectquotemodel").modal('toggle');
    }
    function revisionquote(customer_requirement_id) {
        swal({
                title: "Are you sure?",
                text: "Are you sure you want to revise this shipping quote?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Revise now",
                cancelButtonText: "No!",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token': "{{ csrf_token() }}"
                        },
                        method: 'POST', // Type of response and matches what we said in the route
                        url: '/quote/revisionshippingquote', // This is the url we gave in the route
                        data: {
                            'id': customer_requirement_id,
                        }, // a JSON object to send back
                        success: function (response) { // What to do if we succeed
                            $('.preloader').css("display", "none");
                            console.log(response);
                            user_role = "{{ \Auth::user()->role_id }}";
                            window.location="{{ url('admin/quotes')}}";
                        },
                        error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                            $('.preloader').css("display", "none");
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        }
                    });
                }
            });
    }
</script>
@endsection
