<html>
<table>
    <tr>
        <th>Id</th>
        <th>Product Name</th>
        <th>Company</th>
        <th>Date Quated</th>
    </tr>
    @foreach($pastquotes as $pastquote)
        <tr>
            <td>{{ $pastquote->id }}</td>
            <td>{{ $pastquote->product_name }}</td>
            <td>{{ $pastquote->company_name }}</td>
            <td>{{ date('m/d/Y',strtotime($pastquote->created_at)) }}</td>
        </tr>
    @endforeach
</table>
</html>