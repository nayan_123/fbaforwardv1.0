<html>
<table>
    <tr>
        <th>Id</th>
        <th>Product Name</th>
        <th>Company</th>
        <th>Date Quated</th>
    </tr>
    @foreach($activequotes as $activequote)
        <tr>
            <td>{{ $activequote->id }}</td>
            <td>{{ $activequote->product_name }}</td>
            <td>{{ $activequote->company_name }}</td>
            <td>{{ date('m/d/Y',strtotime($activequote->created_at)) }}</td>
        </tr>
    @endforeach
</table>
</html>