@extends('layouts.admin.app')
@section('title', "Send Inventory To Amazon")
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
<style type="text/css">
.col-md-12 {  padding-left: 2%;  }
.darkheading {  background-color: #003c6b; color:#ffffff; padding-left:3%; margin-bottom: 1%; width: 99.5%;  }
.item-title span, input { margin: 10px 0px !important; }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1> Send Inventory To Amazon</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('inventory') }}"><i class="fa fa-star"></i>Inventory</a></li>
            <li class="active"><a href="{{ url('amazoninventory/create') }}"> Send Inventory To Amazon</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Send Inventory To Amazon</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['url' => 'amazoninventory', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                        {!! Form::hidden('customer_requirement_id',old('customer_requirement_id',!empty($customer_requirement)?$customer_requirement[0]['id'] : null),['id'=>'customer_requirement_id']) !!}
                        <div class="row">
                            <div class="darkheading">
                                <span class="heading">{{ $details->product_name }}</span>
                                <input type="hidden" name="product_id" id="product_id" value="{{ $details->id }}">
                                <input type="hidden" name="sku" id="sku" value="{{ $details->sellerSKU }}">
                            </div>
                            <div class="col-md-12">
                                {{--*/$units=0/*--}}
                                @if(($user_role=='3') || ($user_role=='1'))
                                    {{--*/ $units = $details->atfbaforward /*--}}
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="" class="control-label col-md-12" style="text-align:left">NUMBER OF UNITS STORED AT FBAFORWARD</label>
                                        </div>
                                        <div class="col-md-12">
                                            {{$units}}
                                            <input type="hidden" name="old_unit" id="old_unit" class = "form-control" value="{{$units}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 3%">
                                        <div class="col-md-12">
                                            <label for="units" class="control-label col-md-12" style="text-align:left">NUMBER OF UNITS TO SEND TO AMAZON</label>
                                        </div>
                                        <div class="col-md-12">
                                            @if($units==0 || empty($units))
                                                <input type="text" name="units" id="units" class = "form-control validate[required]" value="{{$units}}" readonly>
                                            @else
                                                <input type="text" name="units" id="units" class = "form-control validate[required]" value="{{$units}}">
                                            @endif
                                        </div>
                                        <div class="col-md-12" id="error"></div>
                                        <div class="col-md-12">
                                            <label for="outbound" class="control-label col-md-12" style="text-align:left">OUTBOUND SHIPPING METHOD DESIRED</label>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="switch-field">
                                                @foreach($outbound_method as $outbound_methods)
                                                    <input type="radio" name="outbound" id="outbound{{$outbound_methods->outbound_method_id}}" class="validate[required]radio" value="{{$outbound_methods->outbound_method_id}}">
                                                    <label for="outbound{{$outbound_methods->outbound_method_id}}" >{{ $outbound_methods->outbound_name }}</label>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="shipping_note" class="control-label col-md-12" style="text-align:left">OUTBOUND SHIPPING NOTES (OPTIONAL)</label>
                                        </div>
                                        <div class="col-md-12">
                                            <textarea name="shipping_note" id="shipping_note" class = "form-control"></textarea>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-12" style="text-align: right">
                                    @if($units==0 || empty($units))
                                        {!! Form::submit('  Submit  ', ['class'=>'button','id'=>'submit','disabled'=>'true' ]) !!}
                                    @else
                                        {!! Form::submit('  Submit  ', ['class'=>'button','id'=>'submit']) !!}
                                    @endif
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    
    {!! Html::script('assets/plugins/iCheck/icheck.min.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}

    <script type="text/javascript">
        $(document).ready(function () {
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach', {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
            $('#units').keyup(function(e) {
                var $th = $(this);
                var new_unit=$th.val();
                if('{{ $units}}' < new_unit) {
                    $("#submit").attr('disabled',true);
                    $("#error").empty();
                    $("#error").append("<label style='color: red'>Please enter unit less than or equal to storage FBAforward units</label>");
                }
                else {
                    $("#submit").attr('disabled',false);
                    $("#error").empty();
                }
            });
        });
        function getvalidate(order_id, qty) {
            var new_unit=parseInt($("#units"+order_id).val());
            qty = parseInt(qty);
            if(qty < new_unit) {
                $("#submit").attr('disabled',true);
                $("#error"+order_id).empty();
                $("#error"+order_id).append("<label style='color: red'>Please enter quantity less than or equal to Pending quantity </label>");
            }
            else {
                $("#submit").attr('disabled',false);
                $("#error"+order_id).empty();
            }
        }
    </script>
@endsection