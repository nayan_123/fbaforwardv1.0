@extends('layouts.admin.app')
@section('title', 'Inventories')
@section('css')
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
    {!! Html::style('assets/dist/css/style.css') !!}
<style>
.dropbtn { padding: 16px;font-size: 16px;border: none;cursor: pointer;  }
.dropdown { position: relative;display: inline-block; }
.dropdown-content { display: none;position: absolute;background-color: #f9f9f9;min-width: 160px;overflow: auto;box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);z-index: 1;  }
.dropdown-content a { color: black; padding: 12px 16px;text-decoration: none;  display: block;  }
.dropdown a:hover {background-color: #f1f1f1; color:#000000 !important;}
.show {display:block;}
.margin-bottom {margin-bottom: 5px;}
a, a:hover {color:#000;}
.dropdown-content{ margin-left: -100px;  z-index: 3;  }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-truck"></i> Inventories</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-truck"></i> Inventories</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Inventory List</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <table id="data_table" class="table datatable dt-responsive" style="width:100%;">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>SKU</th>
                        <th>Last Updated</th>
                        <th>Inbound to FBAforward</th>
                        <th>Processing at FBAforward</th>
                        <th>Stored at FBAforward</th>
                        <th>Inbound to Amazon</th>
                        <th>At Amazon</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <div class="modal fade" id="product_nickname" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Product Nick Name</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" id="nickname_div">
                            {!! Form::open(['url' =>  'inventory/addnickname', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group" id="">
                                        {!! Form::hidden('id',old('id'), ['id'=>'id']) !!}
                                        {!! Form::label('nickname', 'Product Nick Name ',['class' => 'control-label col-md-5']) !!}
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                 {!! Form::text('nickname', old('nickname'), ['class' => 'form-control','id'=>'nickname']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', '',['class' => 'control-label col-md-5']) !!}
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                {!! Form::submit('  Submit  ', ['class'=>'btn btn-primary',  'id'=>'add']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.admin.includes.message_boxes', ['item' => 'Order', 'delete' => true])
@endsection
@section('js')
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    <script type="text/javascript">

        function myFunction(id) {
            document.getElementById("myDropdown"+id).classList.toggle("show");
        }

        window.onclick = function(event) {
            if (!event.target.matches('.dropbtn')) {

                var dropdowns = document.getElementsByClassName("dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show')) {
                        openDropdown.classList.remove('show');
                    }
                }
            }
        }

        function getnickname(id, name) {
            $("#id").val(id);
            $("#nickname").val(name);
            $("#product_nickname").modal('show');
        }



        $(document).ready(function () {
            var table = $("#data_table").DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url("admin/datatables/inventories") !!}',
                columns: [
                    {data: 'product', name: 'product'},
                    {data: 'sku', name: 'sku'},
                    {data: 'last_updated', name: 'last_updated'},
                    {data: 'infba', name: 'infba'},
                    {data: 'processfba', name: 'processfba'},
                    {data: 'storefba', name: 'storefba'},
                    {data: 'inamazon', name: 'inamazon'},
                    {data: 'atamazon', name: 'atamazon'},
                    {data: 'action', name: 'action' , orderable: false, searchable: false},
                ]
            });
            table.column('4:visible').order('desc').draw();
        
            $('#data_table_filter').append("<a href='{{ url('admin/inventory-export') }}'><button class='button btn-export'>Export</button>");

        });
    </script>
@endsection