<html>
<table>
    <tr>
        <th>Id</th>
        <th>Product Name</th>
        <th>SKU</th>
        <th>Last Updated</th>
        <th>Inbound to FBAforward</th>
        <th>Processing at FBAforward</th>
        <th>Stored at FBAforward</th>
        <th>Inbound to Amazon</th>
        <th>At Amazon</th>
    </tr>
    @foreach($inventories as $inventory)
        <tr>
            <td>{{ $inventory->id }}</td>
            <td>{{ $inventory->product_name }}</td>
            <td>{{ $inventory->sellerSKU }}</td>
            <td>{{ date('m/d/Y',strtotime($inventory->updated_at)) }}</td>
            <td>{{ $inventory->infbaforward }}</td>
            <td>{{ $inventory->processfbaforward }}</td>
            <td>{{ $inventory->atfbaforward }}</td>
            <td>{{ $inventory->inamazon }}</td>
            <td>{{ $inventory->atamazon }}</td>
        </tr>
    @endforeach
</table>
</html>