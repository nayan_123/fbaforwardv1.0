@extends('layouts.admin.app')
@section('title', 'Profile')
@section('css')
@endsection
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Profile
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url('admin/users') }}"><i class="fa fa-users"></i> Zoho Account Manager</a></li>
        <li class="active"><i class="fa fa-user"></i> Zoho Account Manager</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $zoho_account_manager->name. " 's Details" }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    @if($zoho_account_manager->image)
                        <div class="col-md-3">
                            <img src="{{ asset('uploads/zoho_managers/'.$zoho_account_manager->image) }}" class="img-responsive img-circle" alt="{{ $zoho_account_manager->name }}" style="height: 170px;">
                        </div>
                        <div class="col-md-9">
                    @else
                        <div class="col-md-12">
                    @endif
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <label class="control-label">Name :</label>
                            </div>
                            <div class="col-md-10">
                                {{ $zoho_account_manager->name }}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2">
                                <label class="control-label">Email :</label>
                            </div>
                            <div class="col-md-10">
                                {{ $zoho_account_manager->email }}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2">
                                <label class="control-label">Zoho User Account Id :</label>
                            </div>
                            <div class="col-md-10">
                                {{ $zoho_account_manager->zoho_user_account_id }}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2">
                                <label class="control-label">Mobile :</label>
                            </div>
                            <div class="col-md-10">
                                {{ $zoho_account_manager->mobile }}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2">
                                <label class="control-label">Phone :</label>
                            </div>
                            <div class="col-md-10">
                                {{ $zoho_account_manager->phone }}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2">
                                <label class="control-label">Role :</label>
                            </div>
                            <div class="col-md-10">
                                {{ $zoho_account_manager->role }}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <form method="post" action="{{ url('zohoaccountmanager/update') }}"  enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <input type="hidden" name="id" value="{{ $zoho_account_manager->id }}">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-2">
                                        <label class="control-label">Image :</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="file" name="image">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-1 col-md-offset-11">
                                        <button type="submit" name="submit" class="button">Save</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->
@endsection

@section('js')
    <script type="text/javascript">

    </script>
@endsection
