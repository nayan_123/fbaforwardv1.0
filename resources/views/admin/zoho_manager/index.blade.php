@extends('layouts.admin.app')

@section('title', 'Zoho Account Managers')

@section('css')
        <!-- DataTables -->
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
@endsection
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-users"></i> Zoho Account Managers
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-users"></i> Zoho Account Manager</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Zoho Account Manager List</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <table id="data_table" class="table datatable dt-responsive" style="width:100%;">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Zoho Account Id</th>
                    <th>Role</th>
                    <th>Mobile</th>
                    <th>Phone</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <p class="text-muted small">
                <i class="fa fa-pencil"></i> Edit User |
                <i class="fa fa-remove"></i> Delete User
            </p>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->

@include('layouts.admin.includes.message_boxes', ['item' => 'User', 'delete' => true])

@endsection

@section('js')
        <!-- DataTables -->
{!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}

{!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}

{!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}

{!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}

<script type="text/javascript">
    $(document).ready(function () {
        var table = $("#data_table").DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! url("admin/datatables/zohoaccountmanagers") !!}',
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'zoho_user_account_id', name: 'zoho_user_account_id'},
                {data: 'role', name: 'role'},
                {data: 'mobile', name: 'mobile'},
                {data: 'phone', name: 'phone'},
                {data: 'actions', name: 'actions', orderable: false, searchable: false}
            ]
        });
        table.column('4:visible').order('asc').draw();
    });
    // function storeuser(user_id) {
    //     $.ajax({
    //         headers: {
    //             'X-CSRF-Token': "{{ csrf_token() }}"
    //         },
    //         method: 'POST', // Type of response and matches what we said in the route
    //         url: '/member/storeuser', // This is the url we gave in the route
    //         data: {
    //             'user_id': user_id
    //         }, // a JSON object to send back
    //         success: function (response) { // What to do if we succeed
    //             $('.preloader').css("display", "none");
    //             window.location.assign('{{url("/member/switchuser")}}');
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
    //             $('.preloader').css("display", "none");
    //             console.log(JSON.stringify(jqXHR));
    //             console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
    //         }
    //     });
    // }
</script>
@endsection