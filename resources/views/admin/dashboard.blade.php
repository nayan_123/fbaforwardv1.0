@extends('layouts.admin.app')
@section('title', 'Dashboard')
@section('css')
{!! Html::style('assets/dist/css/ionicons.min.css') !!}
<style>
    .no-border-radius { border-radius: 0px; }
</style>>
@endsection
@section('content')
<section class="content-header">
    <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
    <ol class="breadcrumb"><li class="active"><i class="fa fa-dashboard"></i> Dashboard</li></ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Admin Dashboard</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>{{ $users }}</h3>
                                    <p>Users</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person"></i>
                                </div>
                                <a href="{{ url('admin/users') }}" class="small-box-footer">More info <i
                                            class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-fuchsia">
                                <div class="inner">
                                    <h3>{{ $customers }}</h3>
                                    <p>Customers</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person"></i>
                                </div>
                                <a href="{{ url('admin/customers') }}" class="small-box-footer">More info <i
                                            class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>{{ $active_order }}</h3>
                                    <p>Active Order</p>
                                </div>
                                <div class="icon">
                                    <i class="ion-android-cart"></i>
                                </div>
                                <a href="{{ url('admin/orders') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>{{ $active_quote }}</h3>
                                    <p>Active Quote</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-anchor"></i>
                                </div>
                                <a href="{{ url('admin/quotes') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>{{ $past_quote }}</h3>
                                    <p>Past Quote</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-anchor"></i>
                                </div>
                                <a href="{{ url('admin/quotes') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-blue">
                                <div class="inner">
                                    <h3>{{ $expire_quote }}</h3>
                                    <p>Expired Quote</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-anchor"></i>
                                </div>
                                <a href="{{ url('admin/quotes') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-orange">
                                <div class="inner">
                                    <h3>{{ $active_inventory }}</h3>
                                    <p>Active Inventory</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-truck"></i>
                                </div>
                                <a href="{{ url('admin/inventories') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="box-footer">
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <input type="hidden" value="{{ $order_year_count }}">
                    <h3 class="box-title">Order Chart</h3>
                    <!-- <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div> -->
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="col-md-3">
                                <label class="control-label">Select Year</label>
                            </div>
                            <div class="col-md-9">
                                <input type='text' name="year" class="form-control datepicker no-border-radius" placeholder="Select Year">
                            </div>
                        </div> 
                        <div class="col-md-12">

                            <div id="chartContainer">FusionCharts will render here</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <input type="hidden" value="{{ $order_year_count }}">
                    <h3 class="box-title">Payment Chart</h3>
                    <!-- <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div> -->
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="col-md-3">
                                <label class="control-label">Select Year</label>
                            </div>
                            <div class="col-md-9">
                                <input type='text' name="year" class="form-control" id="datepicker"  placeholder="Select Year">
                            </div>
                        </div> 
                        <div class="col-md-12">

                            <div id="chart-container">FusionCharts will render here</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@section('js')

{!! Html::script('assets/fusioncharts/js/fusioncharts.js') !!}
{!! Html::script('assets/fusioncharts/js/fusioncharts.charts.js') !!}

<script type="text/javascript">

    $(document).ready(function () {

        $('.datepicker').datepicker({
            format: " yyyy",
            viewMode: "years", 
            minViewMode: "years",
            endDate: '+0d',
        }).on("changeDate", function (e) {
        
            $.ajax({
                headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '/orders/get-chart',
                data: {
                    'year': $('.datepicker').val(),
                },
                success: function (response) {

                    var chart = $.parseJSON(response);

                    FusionCharts.ready(function(){
                        var revenueChart = new FusionCharts({
                            "type": "column3d",
                            "renderAt": "chartContainer",
                            "width": "500",
                            "height": "350",
                            "dataFormat": "json",
                            "dataSource": {
                              "chart": {
                                  "caption": "Monthly Order's Count",
                                  "subCaption": "Orders",
                                  "xAxisName": "Month",
                                  "theme": "fint"
                               },
                              "data": [
                                  {
                                     "label": "Jan",
                                     "value": chart[1],
                                  },
                                  {
                                     "label": "Feb",
                                     "value": chart[2],
                                  },
                                  {
                                     "label": "Mar",
                                     "value": chart[3],
                                  },
                                  {
                                     "label": "Apr",
                                     "value": chart[4],
                                  },
                                  {
                                     "label": "May",
                                     "value": chart[5],
                                  },
                                  {
                                     "label": "Jun",
                                     "value": chart[6],
                                  },
                                  {
                                     "label": "Jul",
                                     "value": chart[7],
                                  },
                                  {
                                     "label": "Aug",
                                     "value": chart[8],
                                  },
                                  {
                                     "label": "Sep",
                                     "value": chart[9],
                                  },
                                  {
                                     "label": "Oct",
                                     "value": chart[10],
                                  },
                                  {
                                     "label": "Nov",
                                     "value": chart[11],
                                  },
                                  {
                                     "label": "Dec",
                                     "value": chart[12]
                                  }
                               ]
                            }
                        });

                        revenueChart.render();
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });

        });

        var data = '<?php echo $order_year_count; ?>';

        var chart = $.parseJSON(data);

        FusionCharts.ready(function(){
            var revenueChart = new FusionCharts({
                "type": "column3d",
                "renderAt": "chartContainer",
                "width": "500",
                "height": "350",
                "dataFormat": "json",
                "dataSource": {
                  "chart": {
                      "caption": "Monthly Order's Count",
                      "subCaption": "Orders",
                      "xAxisName": "Month",
                      "theme": "fint"
                   },
                  "data": [
                      {
                         "label": "Jan",
                         "value": chart[1],
                      },
                      {
                         "label": "Feb",
                         "value": chart[2],
                      },
                      {
                         "label": "Mar",
                         "value": chart[3],
                      },
                      {
                         "label": "Apr",
                         "value": chart[4],
                      },
                      {
                         "label": "May",
                         "value": chart[5],
                      },
                      {
                         "label": "Jun",
                         "value": chart[6],
                      },
                      {
                         "label": "Jul",
                         "value": chart[7],
                      },
                      {
                         "label": "Aug",
                         "value": chart[8],
                      },
                      {
                         "label": "Sep",
                         "value": chart[9],
                      },
                      {
                         "label": "Oct",
                         "value": chart[10],
                      },
                      {
                         "label": "Nov",
                         "value": chart[11],
                      },
                      {
                         "label": "Dec",
                         "value": chart[12]
                      }
                   ]
                }
            });

            revenueChart.render();
        });


        $('#datepicker').datepicker({
            format: " yyyy",
            viewMode: "years", 
            minViewMode: "years",
            endDate: '+0d',
        }).on("changeDate", function (e) {
            $.ajax({
                headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '/payment/get-chart',
                data: {
                    'year': $('#datepicker').val(),
                },
                success: function (response) {

                    var payment_chart = $.parseJSON(response);

                    FusionCharts.ready(function () {
                        var visitChart = new FusionCharts({
                            type: 'msline',
                            renderAt: 'chart-container',
                            width: '500',
                            height: '350',
                            dataFormat: 'json',
                            dataSource: {
                                "chart": {
                                    "caption": "Monthly Payment",
                                    //"subCaption": "Bakersfield Central vs Los Angeles Topanga",
                                    "captionFontSize": "14",
                                    "subcaptionFontSize": "14",
                                    "subcaptionFontBold": "0",
                                    "paletteColors": "#0075c2,#1aaf5d",
                                    "bgcolor": "#ffffff",
                                    "showBorder": "0",
                                    "showShadow": "0",
                                    "showCanvasBorder": "0",
                                    "usePlotGradientColor": "0",
                                    "legendBorderAlpha": "0",
                                    "legendShadow": "0",
                                    "showAxisLines": "0",
                                    "showAlternateHGridColor": "0",
                                    "divlineThickness": "1",
                                    "divLineIsDashed": "1",
                                    "divLineDashLen": "1",
                                    "divLineGapLen": "1",
                                    "xAxisName": "Month",
                                    "showValues": "0"               
                                },
                                "categories": [
                                    {
                                        "category": [
                                            { "label": "Jan" }, 
                                            { "label": "Feb" }, 
                                            { "label": "Mar" },
                                            { "label": "Apr" }, 
                                            { "label": "May" }, 
                                            { "label": "Jun" }, 
                                            { "label": "Jul" },
                                            { "label": "Aug" }, 
                                            { "label": "Sep" }, 
                                            { "label": "Oct" }, 
                                            { "label": "Nov" }, 
                                            { "label": "Dec" },
                                        ]
                                    }
                                ],
                                "dataset": [ 
                                    {
                                        "data": [
                                            { "value": payment_chart[1] }, 
                                            { "value": payment_chart[2] }, 
                                            { "value": payment_chart[3] }, 
                                            { "value": payment_chart[4] }, 
                                            { "value": payment_chart[5] }, 
                                            { "value": payment_chart[6] }, 
                                            { "value": payment_chart[7] },
                                            { "value": payment_chart[8] }, 
                                            { "value": payment_chart[9] }, 
                                            { "value": payment_chart[10] }, 
                                            { "value": payment_chart[11] }, 
                                            { "value": payment_chart[12] },
                                        ]
                                    }   
                                ],
                            }
                        }).render();
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        });
        
        var payment = '<?php echo $payment_chart; ?>';

        var payment_chart = $.parseJSON(payment);

        FusionCharts.ready(function () {
            var visitChart = new FusionCharts({
                type: 'msline',
                renderAt: 'chart-container',
                width: '500',
                height: '350',
                dataFormat: 'json',
                dataSource: {
                    "chart": {
                        "caption": "Monthly Payment",
                        //"subCaption": "Bakersfield Central vs Los Angeles Topanga",
                        "captionFontSize": "14",
                        "subcaptionFontSize": "14",
                        "subcaptionFontBold": "0",
                        "paletteColors": "#0075c2,#1aaf5d",
                        "bgcolor": "#ffffff",
                        "showBorder": "0",
                        "showShadow": "0",
                        "showCanvasBorder": "0",
                        "usePlotGradientColor": "0",
                        "legendBorderAlpha": "0",
                        "legendShadow": "0",
                        "showAxisLines": "0",
                        "showAlternateHGridColor": "0",
                        "divlineThickness": "1",
                        "divLineIsDashed": "1",
                        "divLineDashLen": "1",
                        "divLineGapLen": "1",
                        "xAxisName": "Month",
                        "showValues": "0"               
                    },
                    "categories": [
                        {
                            "category": [
                                { "label": "Jan" }, 
                                { "label": "Feb" }, 
                                { "label": "Mar" },
                                { "label": "Apr" }, 
                                { "label": "May" }, 
                                { "label": "Jun" }, 
                                { "label": "Jul" },
                                { "label": "Aug" }, 
                                { "label": "Sep" }, 
                                { "label": "Oct" }, 
                                { "label": "Nov" }, 
                                { "label": "Dec" },
                            ]
                        }
                    ],
                    "dataset": [ 
                        {
                            "data": [
                                { "value": payment_chart[1] }, 
                                { "value": payment_chart[2] }, 
                                { "value": payment_chart[3] }, 
                                { "value": payment_chart[4] }, 
                                { "value": payment_chart[5] }, 
                                { "value": payment_chart[6] }, 
                                { "value": payment_chart[7] },
                                { "value": payment_chart[8] }, 
                                { "value": payment_chart[9] }, 
                                { "value": payment_chart[10] }, 
                                { "value": payment_chart[11] }, 
                                { "value": payment_chart[12] },
                            ]
                        }
                    ],
                }
            }).render();
        });

});
</script>
@endsection
