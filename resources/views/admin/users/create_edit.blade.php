@extends('layouts.admin.app')
@section('title', 'Users')
@section('css')
@endsection
@section('content')
<style>
.contact, .contact:hover {
    background-color: #969696 !important;
    border-color: #969696 !important;
}
.button, .button:hover {
    display: inline-block;
    /* width: 90px; */
    margin-right: 10px;
    background-color: #0088cc;
    color: rgba(255, 255, 255, 1);
    font-size: 15px;
    font-weight: normal !important;
    text-align: center;
    text-shadow: none;
    padding: 2px 10px;
    border: none;
    /* border-radius: 30px; */
    border-radius: 4px !important;
}
</style>
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-user"></i> {{ !empty($user) ? 'Edit' : 'Add' }} User
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url('admin/users') }}"><i class="fa fa-users"></i> Users</a></li>
        <li class="active"><i
                    class="fa {{ !empty($user) ? 'fa-pencil' : 'fa-plus' }}"></i> {{ !empty($user) ? 'Edit' : 'Add' }}
            User
        </li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">User Details Form</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                            class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open(['url' =>  !empty($user) ? 'admin/users/'.$user->id  :  'admin/users', 'method' => !empty($user) ? 'put' : 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
            {!! Form::hidden('user_id', !empty($user) ? $user->id: null) !!}
            <fieldset>
                <legend>Login Information</legend>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('name', 'Name <span class="required">*</span>', ['class' => 'control-label col-md-3'])) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    {!! Form::text('name', old('name', !empty($user) ? $user->name: null), ['class' => 'form-control validate[required]', 'placeholder'=>'Name']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('email', 'Email <span class="required">*</span>', ['class' => 'control-label col-md-3'])) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    {!! Form::text('email', old('email', !empty($user) ? $user->email: null), ['class' => 'form-control validate[required,custom[email]]', 'placeholder'=>'Email']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', 'Password', ['class' => 'control-label col-md-3']) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    {!! Form::password('password', ['class' => 'form-control validate[required]']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Confirmation', ['class' => 'control-label col-md-3']) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    {!! Form::password('password_confirmation', ['class' => !empty($user) ? 'form-control validate[equals[password]]': 'form-control validate[required,equals[password]]' ]) !!}
                                </div>
                            </div>
                        </div>
                    </div><!-- .col-md-6 -->

                    <div class="col-md-6">
                        @if(isset($user))
                        @if($user->role_id != 1)
                            <div class="form-group">
                                {!! htmlspecialchars_decode(Form::label('role', 'Role <span class="required">*</span>', ['class' => 'control-label col-md-3'])) !!}
                                <div class="col-md-9">
                                    {!! Form::select('role', array_add($roles, '','Please Select'), old('role', !empty($user) ? $user->role_id: null), ['class' => 'form-control select2 validate[required]', 'onchange'=>'show()']) !!}
                                </div>
                            </div>
                        @endif
                        @else
                            <div class="form-group">
                                {!! htmlspecialchars_decode(Form::label('role', 'Role <span class="required">*</span>', ['class' => 'control-label col-md-3'])) !!}
                                <div class="col-md-9">
                                    {!! Form::select('role', array_add($roles, '','Please Select'), old('role', !empty($user) ? $user->role_id: null), ['class' => 'form-control select2 validate[required]', 'onchange'=>'show()']) !!}
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            {!! Form::label('avatar', 'Avatar', ['class' => 'control-label col-md-3']) !!}
                            @if(!empty($user) && $user->avatar !="")
                                <div class="col-md-9">
                                    <img src="{{ asset($user->avatar) }}" width="300px" height="300px" class="img-circle col-md-offset-1" alt="User Avatar"/>
                                </div>
                            @else
                                <div class="col-md-9">
                                    <img src="{{ asset('uploads/avatars/avatar.png') }}" width="30%"
                                         class="img-circle" alt="User Avatar"/>
                                </div>
                            @endif
                            <div class="col-md-7 col-md-offset-5" style="margin-top: 10px;">
								<span class="btn  btn-file  btn-primary">Upload Avatar
                                    {!! Form::file('avatar') !!}
								</span>
                            </div>
                        </div>
                    </div><!-- .col-md-6 -->
                </div><!-- .row -->
            </fieldset>
            <div id="main" hidden>
            <fieldset>
                <legend>Company Details</legend>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_name', 'Type of business <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                    <select name="btype" id="btype" class="select2 col-md-12 validate[required]" >
                                        <option value=""></option>
                                        @foreach($business_type as $btype)
                                            <option value="{{$btype->id}}" @if($user_details) @if($user_details->primary_bussiness_type == $btype->id) {{ "selected" }} @endif @endif>{{$btype->business_type_name}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_address', 'Legal Company Name <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                <div class="input-group col-md-12">
                                    {!! Form::text('lcname', old('lcname',isset($user_details->company_name)?$user_details->company_name:null), ['class' => 'form-control validate[required]']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('company_address2', 'Doing Business As', ['class' => 'control-label col-md-3 no-padding']) !!}
                            <div class="col-md-9">  
                                {!! Form::text('business_as', old('business_as',(isset($user_details->business_as))?$user_details->business_as:null), ['class' => 'form-control validate[required]']) !!}
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_country', 'Mailing Address <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                {!! Form::text('address1', old('address1',isset($user_details->company_address)?$user_details->company_address:null), ['class' => 'form-control validate[required]','placeholder'=>'STREET ADDRESS']) !!}
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_city', 'Address Line 2 <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                {!! Form::text('address2', old('address2',isset($user_details->company_address2)?$user_details->company_address2:null), ['class' => 'form-control','placeholder'=>'ADDRESS LINE 2']) !!}
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_city', 'City <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                {!! Form::text('city', old('city',isset($user_details->company_city)?$user_details->company_city:null), ['class' => 'form-control validate[required]','placeholder'=>'CITY']) !!}
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_city', 'ZIP/POSTAL Code <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                {!! Form::text('postal_code', old('postal_code',isset($user_details->company_zipcode)?$user_details->company_zipcode:null), ['class' => 'form-control validate[required] custom[integer]','placeholder'=>'ZIP/POSTAL Code']) !!}
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_city', 'Country <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                {!! Form::text('country', old('country',isset($user_details->company_country)?$user_details->company_country:null), ['class' => 'form-control validate[required]','placeholder'=>'Country']) !!}
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_city', 'Country of business registration <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                {!! Form::select('business_country', array_add($country, '','Please Select'), old('business_country',isset($user_details->business_country)?$user_details->business_country:null), ['class' => 'select2 validate[required]','onchange'=>'changeCountryWiseState(this.value)']) !!}
                            </div>
                        </div>

                        <div class="col-md-6" id="select_state">
                            <div class="form-group">
                                <div class="form-group col-md-12">
                                    {!! htmlspecialchars_decode(Form::label('company_city', 'State <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                                    <div class="col-md-9">
                                        {!! Form::select('business_state', array_add($states, '','Please Select'), old('business_state',isset($user_details->business_state)?$user_details->business_state:null), ['class' => 'select2 validate[required]']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_city', 'Company Tax Id <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                <select name="taxid" id="taxid" class="select2" onchange="gettax(this.value)">
                                    <option value=""></option>
                                    @foreach($taxes as $tax)
                                        <option value="{{$tax->id}}" @if(!empty($user_details)) @if($tax->id==$user_details->tax_id){{"selected"}}@endif @endif>{{$tax->tax_name}} @if(!empty($tax->short_name)) {{ "(".$tax->short_name.")" }} @endif</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 

                        <div class="form-group col-md-12"></div>

                        <div class="form-group tax_div" id="ein_div" hidden>
                            <div class="col-md-6">
                                <label for="" class="control-label col-md-3"> UPLOAD EIN NOTICE </label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="file" name="ein" id="ein" class="inputfile hidden">
                                        <label for="ein" class="custom-file-upload btn btn-primary" style="padding-left: 20px !important; padding-right: 20px !important;"> Browse  </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group tax_div" id="ssn_div" hidden>
                            <div class="col-md-6">
                                <label for="" class="control-label col-md-3"> SSN (SOCIAL SECURITY NUMBER ) </label>
                                <div class="col-md-9">
                                        {!! Form::text('ssn', old('ssn',isset($user_details->ssn)?$user_details->ssn:null), ['class' => 'form-control  validate[required] custom[integer]']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group tax_div" id="itin_div" hidden>
                            <div class="col-md-6">
                                <label for="" class="control-label col-md-3"> UPLOAD ITIN NOTICE </label>
                                <div class="col-md-9">
                                    <div class="input-group col-md-6">
                                        <input type="file" name="itin" id="itin" class="inputfile hidden">
                                        <label for="itin" class="custom-file-upload btn btn-primary" style="padding-left: 20px !important; padding-right: 20px !important;"> Browse  </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group tax_div" id="no_div" hidden>
                            <div class="col-md-6">
                                <label for="" class="control-label col-md-3"> DON'T HAVE A TAX ID ? </label>
                                <div class="col-md-9">
                                    <a name="contact_us" id="contact_us" class="button contact col-md-6" href="mailto:support@fbaforward.com">CONTACT US</a>
                                    <a name="learn_more" id="learn_more" class="button col-md-5">LEARN MORE</a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                        
                </div><!-- .row -->
            </fieldset>
            
            <fieldset>
                <legend>Primary Contact</legend>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_name', 'First Name <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                {!! Form::text('contact_fname', old('contact_fname',isset($user_details) ? $user_details->contact_fname : null), ['class' => 'form-control validate[required] no-margin']) !!}
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_address', 'Last Name <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                <div class="input-group col-md-12">
                                    {!! Form::text('contact_lname', old('contact_lname',isset($user_details) ? $user_details->contact_lname : null), ['class' => 'form-control validate[required] no-margin']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {!! Form::label('company_address2', 'Country Code', ['class' => 'control-label col-md-3 no-padding']) !!}
                            <div class="col-md-9">  
                                {!! Form::text('ccode', old('ccode',isset($user_details->contact_country_code)?$user_details->contact_country_code:null), ['class' => 'form-control validate[required] no-margin']) !!}
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_country', 'Phone Number <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                {!! Form::text('primary_phone_number', old('primary_phone_number',isset($user_details->contact_phone)?$user_details->contact_phone:null), ['class' => 'form-control validate[required] no-margin']) !!}
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {!! htmlspecialchars_decode(Form::label('company_city', 'Email Address <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                            <div class="col-md-9">
                                {!! Form::text('primary_email', old('primary_email',isset($user_details->contact_email)?$user_details->contact_email:null), ['class' => 'form-control validate[required] no-margin custom[email]']) !!}
                            </div>
                        </div>

                        <!-- @if(isset($user_details))
                            @if(!$user_details->secondary_contact_fname)
                            <div id="button_div" style="float: right; font-weight: bold;">
                                <a onclick="show_additional_section()">+ ADD ADDITIONAL CONTACT</a>
                            </div>
                            @endif
                        @endif -->

                        <div id="button_div" style="float: right; font-weight: bold;">
                            <a onclick="show_additional_section()">+ ADD ADDITIONAL CONTACT</a>
                        </div>

                        </div>
                    </div>
                </fieldset>

                <div id="additional_section" class="additional_section hide">
                <fieldset>
                    <legend>Additional Contact</legend>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-6">
                                {!! htmlspecialchars_decode(Form::label('company_city', 'Additional First Name <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                                <div class="col-md-9">
                                    {!! Form::text('secondary_contact_fname', old('secondary_contact_fname', isset($user_details->secondary_contact_fname) ? $user_details->secondary_contact_fname:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                {!! htmlspecialchars_decode(Form::label('company_city', 'Additional Last Name <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                                <div class="col-md-9">
                                    {!! Form::text('secondary_contact_lname', old('secondary_contact_lname',isset($user_details->secondary_contact_lname) ? $user_details->secondary_contact_lname:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                {!! htmlspecialchars_decode(Form::label('company_city', 'Additional Country Code <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                                <div class="col-md-9">
                                    {!! Form::text('secondary_contact_country_code', old('secondary_contact_country_code',isset($user_details->secondary_contact_country_code)?$user_details->secondary_contact_country_code:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                {!! htmlspecialchars_decode(Form::label('company_city', 'Additional Phone Number <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                                <div class="col-md-9">
                                    {!! Form::text('secondary_phone_number', old('secondary_phone_number',isset($user_details->secondary_contact_phone)?$user_details->secondary_contact_phone:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                {!! htmlspecialchars_decode(Form::label('company_city', 'Additional Contact Email <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                                <div class="col-md-9">
                                    {!! Form::text('secondary_email', old('secondary_email',isset($user_details->secondary_contact_phone)?$user_details->secondary_contact_phone:null), ['class' => 'form-control validate[required] no-margin']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <hr>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group col-md-6">
                                {!! htmlspecialchars_decode(Form::label('company_city', 'Additional First Name <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                                <div class="col-md-9">
                                    {!! Form::text('fname2', old('fname2',isset($user_details->third_contact_fname)?$user_details->third_contact_fname:null), ['class' => 'form-control validate[required]','placeholder'=>'FIRST NAME']) !!}
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                {!! htmlspecialchars_decode(Form::label('company_city', 'Additional Last Name <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                                <div class="col-md-9">
                                    {!! Form::text('lname2', old('lname2',isset($user_details->third_contact_lname)?$user_details->third_contact_lname:null), ['class' => 'form-control validate[required]','placeholder'=>'LAST NAME']) !!}
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                {!! htmlspecialchars_decode(Form::label('company_city', 'Additional Country Code <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                                <div class="col-md-9">
                                    {!! Form::text('ccode2', old('ccode2',isset($user_details->third_contact_country_code)?$user_details->third_contact_country_code:null), ['class' => 'form-control validate[required] ','placeholder'=>'COUNTRY CODE']) !!}
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                {!! htmlspecialchars_decode(Form::label('company_city', 'Additional Contact Number <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                                <div class="col-md-9">
                                    {!! Form::text('phone2', old('phone2',isset($user_details->third_contact_phone)?$user_details->third_contact_phone:null), ['class' => 'form-control validate[required]']) !!}
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                {!! htmlspecialchars_decode(Form::label('company_city', 'Additional Contact Email <span class="required">*</span>', ['class' => 'control-label col-md-3 no-padding'])) !!}
                                <div class="col-md-9">
                                    {!! Form::email('email2', old('email2',isset($user_details->third_contact_email)?$user_details->third_contact_email:null), ['class' => 'form-control validate[required]']) !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </fieldset>
            </div>

            </div>

            <fieldset>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            {!! Form::submit((!empty($user)?'Update': 'Add'). ' User', ['class'=>'btn btn-primary']) !!}
                            <a class="btn btn-default btn-close" href="{{ '../' }}">Cancel</a>
                        </div>
                    </div>
                </div>
            </fieldset>

            {!! Form::close() !!}
        </div><!-- /.box-body -->
        <div class="box-footer">
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->
@endsection
@section('js')
        <!-- iCheck 1.0.1 -->
{!! Html::script('assets/plugins/iCheck/icheck.min.js') !!}
{!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
{!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
<script type="text/javascript">
    $(document).ready(function () {
        //Initialize Select2 Elements
        $(".select2").select2({
            placeholder: "Please Select",
            allowClear: true,
            width: '100%'
        });

        var value=$("#taxid").val();
        gettax(value);

        // Validation Engine init
        var prefix = 's2id_';
        $("form[id^='validate']").validationEngine('attach',
        {
            promptPosition: "bottomRight", scroll: false,
            prettySelect: true,
            usePrefix: prefix
        });

    });

    function show_additional_section()
    {
        $('#button_div').hide();
        $("#additional_section").removeClass('hide');
    }

    function gettax(value) {
        $(".tax_div").hide();
        if(value=='1')
        {
        $("#ein_div").show();
        }
        if(value=='2')
        {
        $("#ssn_div").show();
        }
        if(value=='3')
        {
        $("#itin_div").show();
        }
        if(value=='4')
        {
        $("#no_div").show();
        }
    }

    function changeCountryWiseState(val){
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            method: 'POST', // Type of response and matches what we said in the route
            url: '/admin/users/selectState', // This is the url we gave in the route
            data: {
                'country_code': val,
            }, // a JSON object to send back
            success: function (response) { // What to do if we succeed
                $('#select_state').html(response);
                $('.select2-container--default').css('width','100%');
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }
    function  show() {
      if($('#role').val()==3)
      {
          $('#main').show();
      }
      else
      {
          $('#main').hide();
      }
    }
    $(document).ready(function () {
        if($('#role').val()==3)
        {
            $('#main').show();
        }
        else
        {
            $('#main').hide();
        }
    });
</script>
@endsection