@extends('layouts.admin.app')
@section('title', 'Inventories')
@section('css')
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
{!! Html::style('assets/dist/css/style.css') !!}
@endsection
@section('content')
<section class="content-header">
    <h1><i class="fa fa-shopping-cart"></i> Inventories</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-shopping-cart"></i> Inventories</li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Inventories List</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-12" style="padding-bottom:20px;">
                <div class="col-md-5 no-padding">
                    <select name="product_name" id="product_name" class="form-control select2">
                        <option value=""></option>
                        @foreach($product as $products)
                            <option value="{{ $products->id }}">{{ $products->product_name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-6 no-padding">
                    <div class="col-md-6 no-padding">
                        <input type="text" name="from" id="from" placeholder="From Date" class="form-control datepicker">
                    </div>
                    <div class="col-md-6 no-padding">
                        <input type="text" name="to" id="to" placeholder="To Date" class="form-control datepicker">
                    </div>
                </div>

                <div class="col-md-1 no-padding">
                    <button id="generate_report" name="submit" class="button" style="padding:8.5px 25px;">Submit</button>
                </div>

            </div>
            <table id="data_table" class="table datatable dt-responsive" style="width:100%;">
                <thead>
                <tr>
                    <th>Product Name</th>
                    <th>SKU</th>
                    <th>Last Updated</th>
                    <th>Inbound to FBAforward</th>
                    <th>Processing at FBAforward</th>
                    <th>Stored at FBAforward</th>
                    <th>Inbound to Amazon</th>
                    <th>At Amazon</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
@include('layouts.admin.includes.message_boxes', ['item' => 'Order', 'delete' => true])
@endsection
@section('js')
{!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
{!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function () {
        var table = $("#data_table").DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            ajax: '{!! url("admin/datatables/inventories") !!}',
            columns: [
                {data: 'product', name: 'product'},
                {data: 'sku', name: 'sku'},
                {data: 'last_updated', name: 'last_updated'},
                {data: 'infba', name: 'infba'},
                {data: 'processfba', name: 'processfba'},
                {data: 'storefba', name: 'storefba'},
                {data: 'inamazon', name: 'inamazon'},
                {data: 'atamazon', name: 'atamazon'},
            ]
        });

        table.column('3:visible').order('desc').draw();

        $(".select2").select2({
            placeholder: "Please Select Product",
            allowClear: true
        });

        $(".select_status").select2({
            placeholder: "Please Select Status",
            allowClear: true
        });

        $('.datepicker').datepicker({

        });

        $('#generate_report').click(function(){
            $("#data_table").DataTable().destroy();
            var table = $("#data_table").DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                "ajax": {
                    "url": '{!! url("/inventory/generate_report") !!}',
                    "type": "POST",
                    data: {
                        'product_name': $('select[name=product_name]').val(),
                        'from': $('#from').val(),
                        'to': $('#to').val(),
                        '_token': "{{ csrf_token() }}"
                    },
                },
                columns: [
                    {data: 'product', name: 'product'},
                    {data: 'sku', name: 'sku'},
                    {data: 'last_updated', name: 'last_updated'},
                    {data: 'infba', name: 'infba'},
                    {data: 'processfba', name: 'processfba'},
                    {data: 'storefba', name: 'storefba'},
                    {data: 'inamazon', name: 'inamazon'},
                    {data: 'atamazon', name: 'atamazon'},
                ]
            });

        });

    });
</script>
@endsection