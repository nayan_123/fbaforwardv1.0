@extends('layouts.admin.app')
@section('title', 'Orders')
@section('css')
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
{!! Html::style('assets/dist/css/style.css') !!}
@endsection
@section('content')
<section class="content-header">
    <h1><i class="fa fa-shopping-cart"></i> Orders</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-shopping-cart"></i> Orders</li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Orders List</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-12" style="padding-bottom:20px;">
                <div class="col-md-3 no-padding">
                    <select name="product_name" id="product_name" class="form-control select2">
                        <option value=""></option>
                        @foreach($product as $products)
                            <option value="{{ $products->id }}">{{ $products->product_name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-5 no-padding">
                    <div class="col-md-6 no-padding">
                        <input type="text" name="from" id="from" placeholder="From Date" class="form-control datepicker">
                    </div>
                    <div class="col-md-6 no-padding">
                        <input type="text" name="to" id="to" placeholder="To Date" class="form-control datepicker">
                    </div>
                </div>
                
                <div class="col-md-3 no-padding">
                    <select name="status" id="status" class="form-control select_status">
                        <option value=""></option>
                        <option value="0">Order Received</option>
                        <option value="1">New Orders</option>
                        <option value="2">Bill Of Upload</option>
                        <option value="3">Bill of approve</option>
                        <option value="4">Pre alert</option>
                        <option value="5">Delivery</option>
                        <option value="6">Warehouse Chekin</option>
                        <option value="7">Admin Review</option>
                        <option value="8">Order Labor</option>
                        <option value="9">Manager review</option>
                        <option value="10">Complete shipment</option>
                        <option value="11">Order complete</option>
                    </select>
                </div>

                <div class="col-md-1 no-padding">
                    <button id="generate_report" name="submit" class="button" style="padding:8.5px 25px;">Submit</button>
                </div>

            </div>
            <table id="data_table" class="table datatable dt-responsive" style="width:100%;">
                <thead>
                <tr>
                    <th>Order</th>
                    <th>Product</th>
                    <th>Date Ordered</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</section>
@include('layouts.admin.includes.message_boxes', ['item' => 'Order', 'delete' => true])
@endsection
@section('js')
{!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
{!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function () {
        var table = $("#data_table").DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            ajax: '{!! url("admin/datatables/orders") !!}',
            columns: [
                {data: 'order', name: 'order'},
                {data: 'product', name: 'product'},
                {data: 'date_ordered', name: 'date_ordered'},
                {data: 'status', name: 'status'}
            ]
        });
        table.column('3:visible').order('desc').draw();

        $(".select2").select2({
            placeholder: "Please Select Product",
            allowClear: true
        });

        $(".select_status").select2({
            placeholder: "Please Select Status",
            allowClear: true
        });

        $('.datepicker').datepicker({

        });

        $('#generate_report').click(function(){
            $("#data_table").DataTable().destroy();
            var table = $("#data_table").DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                "ajax": {
                    "url": '{!! url("/orders/generate_report") !!}',
                    "type": "POST",
                    data: {
                        'product_name': $('select[name=product_name]').val(),
                        'status': $('select[name=status]').val(),
                        'from': $('#from').val(),
                        'to': $('#to').val(),
                        '_token': "{{ csrf_token() }}"
                    },
                },
                columns: [
                    {data: 'order', name: 'order'},
                    {data: 'product', name: 'product'},
                    {data: 'date_ordered', name: 'date_ordered'},
                    {data: 'status', name: 'status'}
                ]
            });

            // $.ajax({
            //     headers: {
            //         'X-CSRF-Token': "{{ csrf_token() }}"
            //     },
            //     method: 'POST',
            //     url: '/orders/generate_report',
            //     data: {
            //         'product_name': $('select[name=product_name]').val(),
            //         'status': $('select[name=status]').val(),
            //         'from': $('#from').val(),
            //         'to': $('#to').val()
            //     },
            //     success: function (response) {
            //         var html = '';

            //         html+='<table id="data_table" class="table datatable dt-responsive"';
            //         html+='style="width:100%;">';
            //         html+='<thead>';
            //         html+='<tr>';
            //         html+='<th>Order</th>';
            //         html+='<th>Product</th>';
            //         html+='<th>Date Ordered</th>';
            //         html+='<th>Status</th>';
            //         html+='</tr>';
            //         html+='</thead>';
            //         html+='<tbody>';
            //         $.each(response.orders, function( key, order ) {
            //             html+='<tr>';
            //             html+='<td>'+order.order_no+'</td>';
            //             html+='<td>'+order.product_name+'</td>';
            //             html+='<td>'+order.created_at+'</td>';
            //             html+='<td>'+response.orderStatus[order.is_activated]+'</td>';
            //             html+='</tr>';
            //         });
            //         html+='<tr>';
            //         html+='</tbody>';
            //         html+='</table>';

            //         $('#data_table').html(html);
            //     },
            //     error: function (jqXHR, textStatus, errorThrown) {
            //         console.log(JSON.stringify(jqXHR));
            //         console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            //     }
            // });

        });

    });
</script>
@endsection