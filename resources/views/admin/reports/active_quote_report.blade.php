@extends('layouts.admin.app')
@section('title', 'Quotes')
@section('css')
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
    {!! Html::style('assets/dist/css/style.css') !!}
    <style>
        .dataTables_wrapper{ margin-bottom:30px; }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-anchor"></i> Active Quotes</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-anchor"></i> Active Quotes</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Active Quotes</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-12" style="padding-bottom:20px;">
                    <div class="col-md-3 no-padding">
                        <select name="product_name" id="product_name" class="form-control select2">
                            <option value=""></option>
                            @foreach($product as $products)
                                <option value="{{ $products->id }}">{{ $products->product_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-5 no-padding">
                        <div class="col-md-6 no-padding">
                            <input type="text" name="from" id="from" placeholder="From Date" class="form-control datepicker">
                        </div>
                        <div class="col-md-6 no-padding">
                            <input type="text" name="to" id="to" placeholder="To Date" class="form-control datepicker">
                        </div>
                    </div>
                    
                    <div class="col-md-3 no-padding">
                        <select name="company_name" id="company_name" class="form-control select_company_name">
                            @foreach($user_infos as $user_infos)
                                <option value="{{ $user_infos->company_name }}">{{ $user_infos->company_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-1 no-padding">
                        <button id="generate_report" name="submit" class="button" style="padding:8px 25px;">Submit</button>
                    </div>

                </div>

                <table id="active_quote" class="table datatable dt-responsive" style="width:100%;">
                    <thead>
                        <tr>
                            <th class="col-md-6">Active Quote Requests</th>
                            <th class="col-md-2">Company</th>
                            <th class="col-md-2">Date Quoted</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
    </section>
    @include('layouts.admin.includes.message_boxes', ['item' => 'quote', 'delete' => true])
@endsection
@section('js')
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $("#active_quote").DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                ajax: '{!! url("admin/datatables/activequotes") !!}',
                columns: [
                    {data: 'product', name: 'product'},
                    {data: 'company', name: 'company'},
                    {data: 'date_ordered', name: 'date_ordered'},
                ]
            });
            table.column('2:visible').order('desc').draw();

            $(".select2").select2({
                placeholder: "Please Select Product",
                allowClear: true
            });

            $(".select_company_name").select2({
                placeholder: "Please Select Company Name",
                allowClear: true
            });

            $('.datepicker').datepicker();

            $('#generate_report').click(function(){
                $("#active_quote").DataTable().destroy();
                var table = $("#active_quote").DataTable({
                    processing: true,
                    serverSide: true,
                    searching: false,
                    "ajax": {
                        "url": '{!! url("/quotes/generate_report") !!}',
                        "type": "POST",
                        data: {
                            'product_name': $('select[name=product_name]').val(),
                            'company_name': $('select[name=status_company_name]').val(),
                            'from': $('#from').val(),
                            'to': $('#to').val(),
                            'quote': 'active',
                            '_token': "{{ csrf_token() }}"
                        },
                    },
                    columns: [
                        {data: 'product', name: 'product'},
                        {data: 'company', name: 'company'},
                        {data: 'date_ordered', name: 'date_ordered'}
                    ]
                });
            });

        });
    </script>
@endsection