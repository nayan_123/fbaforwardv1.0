@extends('layouts.member.app')
@section('title', 'Pre-Alert')
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-product-hunt"></i> Pre-Alert
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-product-hunt"></i> Pre-Alert</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Pre-Alert</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <table id="pre_alert" class="table" >
                    <thead>
                    <tr>
                        <th class="col-md-3">Order Number</th>
                        <th class="col-md-3">Product</th>
                        <th class="col-md-2">Company</th>
                        <th class="col-md-2">Shipping Method</th>
                        <th class="col-md-2">Enter New Pre-Alert</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($order as $orders)
                            <tr>
                                <td>{{ $orders->order_no }}</td>
                                <td>{{ $orders->product_name }}</td>
                                <td>{{ $orders->company_name }}</td>
                                <td>{{ $orders->shipping_name }}</td>
                                <td><a class="button" href="{{ url('prealert/create?id='.$orders->order_id) }}">Submit Pre-Alert</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @endsection
@section('js')
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('#pre_alert').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
    });
</script>
@endsection