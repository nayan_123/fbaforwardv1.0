@extends('layouts.member.app')
@section('title', 'Pre-Alert Upload')
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    <style>
        .col-md-12
        {
            padding-left: 35px;
        }

        .shead
        {
            padding-right: 1px;
        }

    </style>
@endsection
@section('content')
    <section class="content-header">
        <h1>
             Pre-Alert Upload
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('prealert') }}"><i class="fa fa-product-hunt"></i> Pre-Alert</a></li>
            <li class="active"><a href="{{ url('prealert/create') }}"> Pre-Alert Upload</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Upload Pre-Alert</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                                class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                {!! Form::open(['url' => 'prealert', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div style="background-color: #003c6b; color:#ffffff; padding-left:3%; width: 99%">
                                <span class="heading">ORDER DETAILS</span>
                            </div>
                            <div style="width: 99%; padding-left:2%;">
                                <table>
                                    @foreach($order as $orders)
                                        <tr  class='trdetails' style="border-bottom: 0px;">
                                            <td colspan='6'>
                                                <span class='shead col-md-2'>Order Number </span><span class='sdetail col-md-10'>{{ $orders->order_no }}</span>
                                                <span class='shead col-md-2'>Product </span><span class='sdetail col-md-10'>{{ $orders->product_name }}</span><span class='col-md-12'>&nbsp;</span>
                                                <div class='col-md-12' style='padding-left: 0px'>
                                                    <span class='shead col-md-2'>Shipping Method </span><span class='sdetail col-md-3'>{{ $orders->shipping_name }}</span>
                                                    <span class='shead col-md-2'>SB Number </span><span class='sdetail col-md-3'>{{ $orders->sb_number}}</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <input type="hidden" name="order_id" id="order_id" value="{{ $orders->order_id }}">
                                        <input type="hidden" name="shipping_method_id" id="shipping_method_id" value="{{ $orders->shipping_method_id }}">
                                    @endforeach
                                </table>
                            </div>
                            <div style="background-color: #003c6b; color:#ffffff; padding-left:3%; margin-bottom:20px; width: 99%" id="">
                                <span class="heading">Pre-Alert Upload</span>
                            </div>
                            <div class="form-group" style="padding-left: 2%" id="">
                                @if($orders->shipping_method_id=='1')
                                <div>
                                    <label for="isf" class="control-label col-md-12" style="text-align:left">ISF</label>
                                </div>
                                <div>
                                   <div class="input-group col-md-12">
                                       <input type="file" name="isf" id="isf" class="validate[required] validate[checkFileType[jpg|jpeg|gif|JPG|png|PNG|docx|doc|pdf|pdfx]]">
                                   </div>
                                </div>
                                @endif
                                @if($orders->shipping_method_id=='1' || $orders->shipping_method_id=='2' )
                                <div>
                                    <label for="hbl" class="control-label col-md-12" style="text-align:left">HBL/HAWB</label>
                                </div>
                                <div>
                                    <div class="input-group col-md-12">
                                        <input type="file" name="hbl" id="hbl" class="validate[required] validate[checkFileType[jpg|jpeg|gif|JPG|png|PNG|docx|doc|pdf|pdfx]]">
                                    </div>
                                </div>
                                <div>
                                    <label for="mbl" class="control-label col-md-12" style="text-align:left">MBL/MAWB</label>
                                </div>
                                <div>
                                    <div class="input-group col-md-12">
                                        <input type="file" name="mbl" id="mbl" class="validate[required] validate[checkFileType[jpg|jpeg|gif|JPG|png|PNG|docx|doc|pdf|pdfx]]">
                                    </div>
                                </div>
                                @endif
                                @if($orders->shipping_method_id=='2' )
                                <div>
                                    <label for="cargo" class="control-label col-md-12" style="text-align:left">Cargo Manifest</label>
                                </div>
                                <div>
                                    <div class="input-group col-md-12">
                                        <input type="file" name="cargo" id="cargo" class="validate[required]">
                                    </div>
                                </div>
                                @endif
                                @if($orders->shipping_method_id=='3' || $orders->shipping_method_id=='2' )
                                <div>
                                    <label for="commercial" class="control-label col-md-12" style="text-align:left">Commercial Invoice</label>
                                </div>
                                <div>
                                    <div class="input-group col-md-12">
                                        <input type="file" name="commercial" id="commercial" class="validate[required]">
                                    </div>
                                </div>
                                <div>
                                    <label for="packing" class="control-label col-md-12" style="text-align:left">Packing List</label>
                                </div>
                                <div>
                                    <div class="input-group col-md-12">
                                        <input type="file" name="packing" id="packing" class="validate[required]">
                                    </div>
                                </div>
                                @endif
                                <div>
                                    <label for="etd_original" class="control-label col-md-12" style="text-align:left">ETD Origin</label>
                                </div>
                                <div>
                                    <div class="input-group col-md-12">
                                        <input type="text" name="etd_original" id="etd_original" class="form-control datepicker validate[required]">
                                    </div>
                                </div>
                                <div>
                                    <label for="etd_us" class="control-label col-md-12" style="text-align:left">ETD U.S</label>
                                </div>
                                <div>
                                    <div class="input-group col-md-12">
                                        <input type="text" name="etd_us" id="etd_us" class="form-control datepicker validate[required]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="text-align: right">
                        {!! Form::submit('  Submit Pre-Alert  ', ['class'=>'btn btn-primary', ]) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
    @endsection
@section('js')
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            startDate: new Date(),
        });
        var prefix = 's2id_';
        $("form[id^='validate']").validationEngine('attach',
            {
                promptPosition: "bottomRight", scroll: false,
                prettySelect: true,
                usePrefix: prefix
            });
    });
</script>
@endsection