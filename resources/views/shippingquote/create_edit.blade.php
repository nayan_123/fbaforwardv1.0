@extends('layouts.member.app')

@section('title', $title)

@section('css')

@endsection

@section('content')
        <section class="content-header">
            <h1>
                <i class="fa fa-star"></i> {{$title}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><i class="fa fa-star"></i>{{$title}}</li>
            </ol>
        </section>
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$title}}</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
    <div class="row">
        {!! Form::open(['url' =>  isset($company) ? 'partnercompany/'.$company->id  :  'partnercompany', 'method' => isset($company) ? 'put' : 'post', 'class' => 'form-horizontal', 'id'=>'validate']) !!}
        <div class="col-md-12">
            <h4>{{ $title }}</h4>
            <hr>
            <br/>
            <div class="form-group{{ $errors->has('delivery_company') ? ' has-error' : '' }}">
                <label class="col-md-2 control-label">Delivery Company <span class="required">*</span></label>

                <div class="col-md-7">
                    <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" placeholder="Delivery Company" class="form-control validate[required]" name="delivery_company" value="{{ old('delivery_company', isset($company) ? $company->delivery_company : null) }}">
                    </div>
                    @if ($errors->has('delivery_company'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('delivery_company') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('terminal') ? ' has-error' : '' }}">
                <label class="col-md-2 control-label">Terminal <span class="required">*</span></label>
                <div class="col-md-7">
                    <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" placeholder="Terminal" class="form-control validate[required]" name="terminal" value="{{ old('terminal', isset($company) ? $company->terminal : null) }}">
                    </div>
                    @if ($errors->has('terminal'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('terminal') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('destination') ? ' has-error' : '' }}">
                <label class="col-md-2 control-label">Destination <span class="required">*</span></label>

                <div class="col-md-7">
                    <div class="input-group">
                        <span class="input-group-addon"></span>
                        <input type="text" placeholder="Destination" class="form-control validate[required]" name="destination" value="{{ old('destination', isset($company) ? $company->destination : null) }}">
                    </div>
                    @if ($errors->has('destination'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('destination') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-primary">Submit </button>
            </div>
        </div>
        </form>
    </div>
    </div>
            </div>
        </section>
@endsection

@section('js')
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            // Validation Engine init
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach',
                {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
        });
    </script>
@endsection
