@extends('layouts.member.app')
@section('title', $title)
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/custom_quotes.css') !!}
@endsection
@section('content')
<style>
.content{ min-height: 620px;  }
.product-name{  text-decoration: none;  overflow: hidden;  white-space: nowrap;  text-overflow: ellipsis;  }
.btn-download:hover{ cursor: pointer;color:#09517b !important; }
</style>
<section class="content">
    <section class="content-header">
        <h1><i class="fa fa-file-text"></i> {{$title}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-file-text"></i>{{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-12 no-padding">
            <div class="box2 col-md-9 no-padding">
                <div class="col-md-12 no-padding">
                    <div class="box-body no-padding">
                        <div class="">
                            <div class="col-md-12 no-padding">
                                <div class="col-md-4 blue-bg product-data">
                                    <b>Order</b> {{$order->order_no}}
                                </div>
                                <div class="col-md-8 product-data text-right">
                                    <b>Final Invoice</b>
                                </div>
                            </div>
                            <div class="col-md-12 no-padding">
                                <div class="col-md-4 product-data">
                                    <b>Invoice Number : </b>
                                </div>
                                <div class="col-md-4 product-data">
                                    <b> Date Invoiced: </b> {{ date('m/d/Y',strtotime($order->updated_at)) }}
                                </div>
                                <div class="col-md-4 product-data">
                                    <b>Due Date : </b> {{ date('m/d/Y',strtotime($order->payment_date)) }}
                                </div>
                                <div class="col-md-4 product-data product-name">
                                    <b>Product : </b>{{$order->product_name}}
                                </div>
                                <div class="col-md-4 product-data">
                                    <b>Units : </b>{{$order->total}}
                                </div>
                                <div class="col-md-4 product-data">
                                    <b>cartons : </b>{{$order->no_boxs}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 blank-class"></div>
                <div class="col-md-12 no-padding">
                    <div class="box2 col-md-12 invoice-summery-box no-padding">
                        <div class="box-header with-border invoice-summery-heading">
                            <h3 class="box-title">Invoice Summary</h3>
                        </div>
                        <div class="col-md-12">
                            <table class="table quote-summery-table">
                              <thead>
                                <tr>
                                    <th>Service</th>
                                    <th>Cost</th> 
                                    <th>Qty</th>
                                    <th>Total</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td>Customs duties</td>
                                    <td>$100</td>
                                    <td>1</td>
                                    <td>$100</td>
                                </tr>
                                <tr>
                                    {{--*/ $port_fee = 0; /*--}}
                                    {{--*/ if($order->shipment_size_type == 1){
                                        $port_fee = 250;
                                    }
                                    elseif(($order->shipment_size_type == 2) || ($order->shipment_size_type == 3)) {
                                        $port_fee = 75;
                                    }
                                    /*--}}

                                    <td>Port fees</td>
                                    <td>${{ $port_fee }}</td>
                                    <td>1</td>
                                    <td>${{ $port_fee }}</td>
                                </tr>
                                <tr>
                                    <td>Customs exams</td>
                                    <td>${{ $custom_fee }}</td>
                                    <td>1</td>
                                    <td>${{ $custom_fee }}</td>
                                </tr>
                                {{--*/ $total = 0; /*--}}
                                {{--*/ $total = 100 + $port_fee + $custom_fee; /*--}}
                                <tr style="border-top:1px solid #f4f4f4;">
                                    <td><b>BALANCE DUE</b></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>${{ $total }}</b></td>
                                </tr>
                              </tbody>
                          </table>
                        </div>
                        <div class="col-md-12">
                            <p>All invoices due on receipt. Invoices not paid within 3 days are subject to a late fee of 10% of the invoice balance or $50, whichever is greater, and then 1.5% per month thereafter. If payment has not been made after 45 days of invoice disbersment, your goods will be considered abandoned and you will forfeit your ownership of the products. Payment must be made prior to shipment to Amazon.</p>
                        </div>
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                            <a href="{{ url('billing/invoice-download',$order->order_id) }}" class="button btn-download col-md-6" >Download</a>
                            <button class="button col-md-5">Pay</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box2 order-payment-history-box no-padding col-md-3">
                <div class="box-header order-payment-history-heading with-border">
                    <h3 class="box-title">Order Payment History</h3>
                </div>
                {{--*/ $paid_total=0 /*--}}
                @if(count($order_paid_history)>0)
                   @foreach($order_paid_history as $order_paid_histories)
                   <div class="col-md-12 border-bottom no-border">
                       <div class="col-md-12 no-padding">
                            {{ date('m/d/Y',strtotime($order_paid_histories->created_at)) }}
                       </div>
                       <div class="col-md-6 text-left no-padding">
                            @if($order_paid_histories->transaction_type=='1')
                                Deposite
                            @elseif($order_paid_histories->transaction_type=='2')
                                Storage
                            @elseif($order_paid_histories->transaction_type=='3')
                                Final
                            @endif
                       </div>
                       <div class="col-md-6 text-right no-padding">
                            {{ $order_paid_histories->amount }}
                       </div>
                   </div>
                       {{--*/ $paid_total+= $order_paid_histories->amount /*--}}
                    @endforeach
                @endif
                <div class="col-md-12 text-right">
                    Paid Order Total: {{ $paid_total }}
                </div>
                <div class="col-md-12 text-right">
                    Order Total: {{ $paid_total + $total }}
                </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection