@extends('layouts.member.app')
@section('title', $title)
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
<section class="content no-padding">
    <section class="content-header">
        <h1>
            <i class="fa fa-shopping-cart"></i> {{$title}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-shopping-cart"></i>{{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive no-padding">
                    <div class="col-md-6 padding-0-5">
                        <h3>Open Invoices</h3>
                    </div>
                    <table id="inprogress_data" class="table" >
                        <thead>
                        <tr>
                            <th class="col-md-2">Order</th>
                            <th class="col-md-4">Product</th>
                            <th class="col-md-1">Date</th>
                            <th class="col-md-1">Total Due</th>
                            <th class="col-md-1">Invoice</th>
                            <th class="col-md-3"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($active_order as $orders)
                            <tr>
                                <td class="col-md-2">#{{$orders->order_no}}</td>
                                <td class="col-md-4">{{$orders->product_name}}</td>
                                <td class="col-md-1">
                                    @if(date('m/d/Y',strtotime($orders->payment_date)) < \Carbon\Carbon::now()->format('m/d/Y'))
                                        <font color="red">{{date('m/d/Y',strtotime($orders->payment_date))}}</font>
                                    @else
                                        {{date('m/d/Y',strtotime($orders->payment_date))}}
                                    @endif
                                </td>

                                <td class="col-md-1">&nbsp;</td>
                                <td class="col-md-1">Final</td>
                                <td class="col-md-3"><a href="{{ url('billing/'.$orders->order_id) }}" class="button col-md-6">View</a> <a class="button col-md-5">Pay</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                 <div class="table-responsive no-padding">
                    <div class="col-md-6 padding-0-5">
                        <h3>History</h3>
                    </div>
                    <table id="history" class="table" >
                        <thead>
                        <tr>
                            <th class="col-md-2">Order</th>
                            <th class="col-md-4">Product</th>
                            <th class="col-md-1">Date</th>
                            <th class="col-md-1">Total Due</th>
                            <th class="col-md-1">Invoice</th>
                            <th class="col-md-3"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($past_order as $orders)
                            <tr>
                                <td class="col-md-2">{{$orders->order_no}}</td>
                                <td class="col-md-4">{{$orders->product_name}}</td>
                                <td class="col-md-1">{{date('m/d/Y',strtotime($orders->created_at))}}</td>
                                <td class="col-md-1">&nbsp;</td>
                                <td class="col-md-1">&nbsp;</td>
                                <td class="col-md-3"><a href="{{ url('billing/create') }}" class="button col-md-12">View</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            </div>
        </div>
    </section>
</section>
@endsection
@section('js')
{!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
{!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('#history').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });

        $('#inprogress_data').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });

    });
</script>
@endsection