<style>
.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
    /*float: left;*/
}
.col-md-6 {
    width: 50%;
}
.heading{
    color: #7d7d7d;
    font-weight: 600;
    font-size: 30px;
}
.grey-font{
    color: #7d7d7d;
    font-weight: 600;
}
@media (min-width: 992px){
    .col-md-12 {
        width: 100%;
    }
}
.order_table>thead>tr>th{ background-color:#0088cc;color:#fff;text-transform: uppercase;text-align:left; }
.order_table>tbody{ border:1px solid #e0dddd;border-top: none; }
.order_table>tbody>tr>td{ border-right:1px solid #e0dddd;text-align:left; }
.order_table>thead>tr>th,.order_table>tbody>tr>td{
    padding: 8px;
    text-align: center;
}
.order_table {
    background-color: transparent;
    border-spacing: 0;
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 30px;    
}
h4{ text-transform: uppercase; }
.balance-due{ border-top: 1px solid }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6" style="float:left;">
                
                <img src = "<?php echo public_path();?>/uploads/images/FBAFORWARD_LOGO.png" style="width:70%;">
                <div class="col-md-12 grey-font">
                    <p style="margin:5px 0px;font-size: 15px;">1550 HOTEL CIRCLE NORTH</p>
                    <p style="margin:5px 0px;font-size: 15px;">SUITE 360</p>
                    <p style="margin:5px 0px;font-size: 15px;">SAN DIEGO, CA 92108</p>
                    <p style="margin:5px 0px;font-size: 15px;">+1 866 526 3951</p>
                </div>
            </div>
            <div class="col-md-6" style="margin-left: 53%;">
                <h1 class="heading">FINAL INVOICE</h1>
                <div class="col-md-12 grey-font">
                    <p><b>FOR: </b>{{ $order->company_name }}</p>
                    <p><b>Email: </b>{{ $email }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <h4>Invoice Summary</h4>
            <table class="table order_table">
                <thead>
                    <tr>
                        <th>Service</th>
                        <th>Cost</th> 
                        <th>Qty</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Customs duties</td>
                        <td>$100</td>
                        <td>1</td>
                        <td>$100</td>
                    </tr>
                    <tr>
                        {{--*/ $port_fee = 0; /*--}}
                        {{--*/ if($order->shipment_size_type == 1){
                            $port_fee = 250;
                        }
                        elseif(($order->shipment_size_type == 2) || ($order->shipment_size_type == 3)) {
                            $port_fee = 75;
                        }
                        /*--}}

                        <td>Port fees</td>
                        <td>${{ $port_fee }}</td>
                        <td>1</td>
                        <td>${{ $port_fee }}</td>
                    </tr>
                    <tr>
                        <td>Customs exams</td>
                        <td>${{ $custom_fee }}</td>
                        <td>1</td>
                        <td>${{ $custom_fee }}</td>
                    </tr>
                    {{--*/ $total = 0; /*--}}
                    {{--*/ $total = 100 + $port_fee + $custom_fee; /*--}}
                    <tr class="balance-due">
                        <td>BALANCE DUE</td>
                        <td></td>
                        <td></td>
                        <td>${{ $total }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <p>All invoices due on receipt. Invoices not paid within 3 days are subject to a late fee of 10% of the invoice balance or $50, whichever is greater, and then 1.5% per month thereafter. If payment has not been made after 45 days of invoice disbersment, your goods will be considered abandoned and you will forfeit your ownership of the products. Payment must be made prior to shipment to Amazon.</p>
        </div>

        <div class="col-md-12">
            <table class="table order_table">
                <thead>
                    <tr>
                        <th>Order Payment History</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {{--*/ $paid_total=0 /*--}}
                @if(count($order_paid_history)>0)
                    @foreach($order_paid_history as $order_paid_histories)
                        <tr>
                            <td>
                                @if($order_paid_histories->transaction_type=='1')
                                    Deposite
                                @elseif($order_paid_histories->transaction_type=='2')
                                   Storage
                                @elseif($order_paid_histories->transaction_type=='3')
                                    Final
                                @endif
                            </td>
                            <td>{{ date('m/d/Y',strtotime($order_paid_histories->created_at)) }}</td>
                            <td>${{ $order_paid_histories->amount }}</td>
                        </tr>
                        {{--*/ $paid_total+= $order_paid_histories->amount /*--}}
                    @endforeach
                @endif
                    <tr>
                        <td>Paid Order Total</td>
                        <td>-</td>
                        <td>${{ $paid_total }}</td>
                    </tr>
                    <tr>
                        <td>Order Total</td>
                        <td>-</td>
                        <td>${{ $paid_total + $total }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <!-- <div class="col-md-12">
            <div class="box">
                <div class="box-header order-payment-history-heading with-border">
                    <h3 class="box-title">Order Payment History</h3>
                </div>
                {{--*/ $paid_total=0 /*--}}
                @if(count($order_paid_history)>0)
                   @foreach($order_paid_history as $order_paid_histories)
                    <div class="col-md-12 border-bottom no-border">
                        <div class="col-md-6 no-padding" style="text-align:right">
                            {{ date('m/d/Y',strtotime($order_paid_histories->created_at)) }}
                        </div>
                        <div class="col-md-6"></div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                @if($order_paid_histories->transaction_type=='1')
                                    Deposite
                                @elseif($order_paid_histories->transaction_type=='2')
                                   Storage
                                @elseif($order_paid_histories->transaction_type=='3')
                                    Final
                                @endif
                            </div>
                            <div class="col-md-6" style="text-align:right;">
                                {{ $order_paid_histories->amount }}
                            </div>
                        </div>
                    </div>
                       {{--*/ $paid_total+= $order_paid_histories->amount /*--}}
                    @endforeach
                @endif
                <div class="col-md-12 text-right">
                    Paid Order Total: {{ $paid_total }}
                </div>
                <div class="col-md-12 text-right">
                    Order Total: {{ $paid_total + $total }}
                </div>
            </div>
        </div> -->

    </div>
</div>