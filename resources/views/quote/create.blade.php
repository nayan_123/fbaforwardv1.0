@extends('layouts.member.app')
@section('title', $title)
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
<style type="text/css">
    .col-md-12 {  padding-left: 2%;  }
    .darkheading {  background-color: #003c6b; color:#ffffff; padding-left:3%; margin-bottom: 1%; width: 99.5%;  }
    .item-title span, input { margin: 10px 0px !important; }
    .my-checkbox{ position: absolute;margin-left: -15px !important;margin-top: 5px !important; }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1> @if(!empty($customer_requirement)) Request Again @else Request New Quote @endif </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('quote') }}"><i class="fa fa-anchor"></i>{{$title}}</a></li>
            <li class="active"><a href="{{ url('quote/create') }}">  @if(!empty($customer_requirement)) Request Again @else Request New Quote @endif</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['url' => 'quote', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                        @if(!$resubmit_quote)
                            {!! Form::hidden('customer_requirement_id',old('customer_requirement_id',!empty($customer_requirement)?$customer_requirement[0]['id'] : null),['id'=>'customer_requirement_id']) !!}
                        @endif
                        @if($user_role!=0)
                        <div class="form-group" style="margin-bottom: 3%">
                            <div class="col-md-12">
                                <label for="company" class="control-label col-md-12" style="text-align:left">Company </label>
                            </div>
                            <div class="col-md-12">
                                <input type="hidden" name="user_role" id="user_role" value="{{$user_role}}">
                                <select id="customer" name="customer" class="select2">
                                    <option value=""></option>
                                    @foreach($customer as $customers)
                                        <option value="{{$customers->user_id}}">{{$customers->company_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <div class="darkheading">
                                <span class="heading">PRODUCT DETAILS</span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="margin-bottom: 3%">
                                    <div class="col-md-12">
                                        <label for="product" class="control-label col-md-12" style="text-align:left">PRODUCT <abbr title="Product"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    @if(!$resubmit_quote)
                                        @if(!empty($customer_requirement))
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <div class="col-md-3 no-padding">
                                                        <input type="radio" id="select_product" name="select_product" value="0" @if(!$customer_requirement[0]['customer_requirement_details_product_name']) {{ "checked" }} @endif> Select From My Amazon Products
                                                    </div>

                                                    <div class="col-md-3">
                                                        <input type="radio" id="select_product" name="select_product" value="1" @if($customer_requirement[0]['customer_requirement_details_product_name']) {{ "checked" }} @endif> Enter New Product
                                                    </div>
                                                </div>
                                            </div>
                                            @if($customer_requirement[0]['customer_requirement_details_product_name'])
                                                <div class="col-md-12 hide" id="product_selectbox" >
                                            @else
                                                <div class="col-md-12" id="product_selectbox">
                                            @endif
                                        @else
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <div class="col-md-3 no-padding">
                                                        <input type="radio" id="select_product" name="select_product" value="0" checked> Select From My Amazon Products
                                                    </div>

                                                    <div class="col-md-3">
                                                        <input type="radio" id="select_product" name="select_product" value="1"> Enter New Product
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="col-md-12" id="product_selectbox">
                                        @endif
                                        <select name="product" id="product" class = "validate[required] select2" onchange="getsupplier(this.value)">
                                            <option value=""></option>
                                            @foreach($product as $products)
                                                <option value="{{ $products->id." ".$products->FNSKU }}" @if(!empty($customer_requirement)) @if($customer_requirement[0]['product_id']==$products->id) selected @endif @else @if($product_id==$products->id) selected @endif @endif>{{ $products->product_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if(!empty($customer_requirement))
                                        @if($customer_requirement[0]['customer_requirement_details_product_name'])
                                            <div class="col-md-12" id="product_textbox">
                                        @else
                                            <div class="col-md-12 hide" id="product_textbox">
                                        @endif
                                    @else
                                       <div class="col-md-12 hide" id="product_textbox">
                                    @endif
                                        <div class="input-group col-md-12">
                                            {!! Form::text('product_name', old('product',!empty($customer_requirement)?$customer_requirement[0]['customer_requirement_details_product_name']:null ), ['class' => 'form-control validate[required] margin-top-0', 'id'=>'product']) !!}
                                        </div>
                                    </div>
                                    @else
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <div class="col-md-3 no-padding">
                                                    <input type="radio" id="select_product" name="select_product" value="0" @if(!$customer_requirement[0]->customer_requirement_details_product_name) {{ "checked" }} @endif> Select From My Amazon Products
                                                </div>

                                                <div class="col-md-3">
                                                    <input type="radio" id="select_product" name="select_product" value="1" @if($customer_requirement[0]->customer_requirement_details_product_name) {{ "checked" }} @endif> Enter New Product
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12  @if(!$customer_requirement[0]->customer_requirement_details_product_name) {{ 'hide' }} @endif" id="product_textbox">
                                            <div class="input-group col-md-12">
                                                {!! Form::text('product_name', old('product',!empty($customer_requirement)?$customer_requirement[0]['customer_requirement_details_product_name']:null ), ['class' => 'form-control validate[required] margin-top-0', 'id'=>'product']) !!}
                                            </div>
                                        </div>

                                        <div class="col-md-12 @if($customer_requirement[0]->customer_requirement_details_product_name) {{ 'hide' }} @endif" id="product_selectbox">
                                            <select name="product" id="product" class = " validate[required] select2 col-md-12" onchange="getsupplier(this.value)">
                                                <option value=""></option>
                                                @foreach($product as $products)
                                                    <option value="{{ $products->id.' '.$products->FNSKU }}" @if(!empty($customer_requirement)) @if($customer_requirement[0]['product_id']==$products->id) {{"selected"}} @endif @else @if($product_id==$products->id){{"selected"}} @endif @endif>{{ $products->product_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    @endif
                                    <div class="col-md-12">
                                        <label for="no_of_carton" class="control-label col-md-12" style="text-align:left;margin-top:10px;"># OF CARTONS <abbr title="# of cartons"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('no_of_carton', old('no_of_carton',!empty($customer_requirement)?$customer_requirement[0]['no_boxs']:null ), ['class' => 'form-control validate[required, custom[integer]] margin-top-0', 'id'=>'no_of_carton','placeholder'=>'','onchange'=>'calculation()']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="qty_per_carton" class="control-label col-md-12" style="text-align:left">QTY PER CARTON <abbr title="qty per carton"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('qty_per_carton', old('qty_per_carton',!empty($customer_requirement)? $customer_requirement[0]['qty_per_box']: null ), ['class' => 'form-control validate[required, custom[integer]] margin-top-0','id'=>'qty_per_carton', 'placeholder'=>'','onchange'=>'calculation()']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="total_unit" class="control-label col-md-12" style="text-align:left">TOTAL UNITS <abbr title="total units"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('total_unit', old('total_unit',!empty($customer_requirement)? $customer_requirement[0]['total']: null ), ['class' => 'form-control validate[required, custom[integer]] margin-top-0', 'id'=>'total_unit','placeholder'=>'','readonly']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="supplier" class="control-label col-md-12" style="text-align:left">SUPPLIER <abbr title="supplier"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            <div class="col-md-3 no-padding">
                                                <input type="radio" id="select_supplier" name="select_supplier" value="0" checked> Select Suppliers
                                            </div>

                                            <div class="col-md-3">
                                                <input type="radio" id="select_supplier" name="select_supplier" value="1"> Enter New Supplier
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="supplier_selectbox">
                                        <select name="supplier" id="supplier" class = " validate[required] select2">
                                            <option value=""></option>
                                            @foreach($supplier as $suppliers)
                                                <option value="{{ $suppliers->supplier_id }}" @if(!empty($customer_requirement))@if($customer_requirement[0]['supplier']==$suppliers->supplier_id) {{"selected"}} @endif @endif>{{ $suppliers->contact_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12 hide" id="supplier_textbox">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('supplier_name', old('supplier_name'), ['class' => 'form-control validate[required] margin-top-0', 'id'=>'supplier_name']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 darkheading">
                                <span class="heading"><!-- Pre-Shipment Services --> PRE-SHIPMENT SERVICES</span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="input-group col-md-12">
                                                <input type="checkbox" name="factory_audit" id="factory_audit" @if(!empty($customer_requirement)) @if($customer_requirement[0]['factory_audit']=='1') {{ "checked" }} @endif  @endif value="1" onclick="getchecked()"> Factory Audit<abbr title="Factory Audit"><i class="fa fa-question-circle compulsary"></i></abbr>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group col-md-12">
                                            <input type="checkbox" name="pre_inspection" id="pre_inspection" @if(!empty($customer_requirement)) @if($customer_requirement[0]['pre_inspection']=='1') {{ "checked" }} @endif  @endif value="1" onclick="getchecked()"> Shipment Pre-Inspection in China<abbr title="Shipment Pre-Inspection in China"><i class="fa fa-question-circle compulsary"></i></abbr>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="pre_ship_div" @if(!$customer_requirement) hidden @endif>
                                <div class="col-md-12">
                                    <label for="pre_ship_note" class="control-label col-md-12" style="text-align:left">Pre Shipment Note <abbr title="Pre Shipment Note"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom:3%">
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="input-group col-md-12">
                                                    <textarea name="pre_ship_note" id="pre_ship_note" class="form-control validate[required]"> @if($customer_requirement) {{$customer_requirement[0]['pre_shipment_note']}} @endif</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 darkheading">
                                <span class="heading col-md-6 no-padding">INTERNATIONAL SHIPMENT DETAILS </span><span class="col-md-6" style="text-align: right"><input type="checkbox" name="shipment_details" class="my-checkbox" id="shipment_details">I do not require shipping support</span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group margin-bottom-0">
                                    <div class="col-md-12">
                                        <label id="shipping_method" for="shipping_method" class="control-label col-md-12" style="text-align:left">INTERNATIONAL SHIPPING METHOD DESIRED<abbr title="shipping method desired"><i class="fa fa-question-circle compulsary"></i></abbr><span style="margin-left: 10px; font-style: italic; font-size: 12px;">Select all shipping methods that you would like to receive a quote for</span></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="switch-field">
                                            @foreach($shipping_method as $shipping_methods)
                                                <input type="checkbox" id="shipping_method{{ $shipping_methods->shipping_method_id }}" name="shipping_method[]" value="{{ $shipping_methods->shipping_method_id }}" class="validate[minCheckbox[1]]checkbox" @if(!empty($customer_requirement_method)) @if(in_array($shipping_methods->shipping_method_id,$customer_requirement_method)) {{ "checked" }} @endif @endif>
                                                <label class="text-uppercase" for="shipping_method{{$shipping_methods->shipping_method_id}}" >{{ $shipping_methods->shipping_name  }}</label>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="shipment_type" class="control-label col-md-12" style="text-align:left">SHIPMENT TYPE <abbr title="shipment type"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            <div class="switch-field1">
                                                @foreach($shipment_type as $shipment_types)
                                                    <input type="radio" id="shipment_type{{ $shipment_types->id }}" name="shipment_type" value="{{ $shipment_types->id }}" class="validate[required]radio"@if(!empty($customer_requirement)) @if($customer_requirement[0]['shipment_size_type']==$shipment_types->id) {{ "checked" }} @endif @endif/>
                                                    <label for="shipment_type{{ $shipment_types->id }}">{{ $shipment_types->name }}</label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="shipment_size" class="control-label col-md-12" style="text-align:left">SHIPMENT SIZE <abbr title="shipment size"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        {!! Form::label('total_weight', 'TOTAL WEIGHT ', ['class' => 'col-md-4', 'style'=>'text-align:left; padding-left:0% !important; font-weight: normal !important;']) !!}
                                        {!! Form::label('total_volume', 'TOTAL VOLUME ', ['class' => 'col-md-4', 'style'=>'text-align:left; padding-left:0% !important; font-weight: normal !important;']) !!}
                                        {!! Form::label('unit', 'UNITS   ', ['class' => 'col-md-4', 'style'=>'text-align:left; font-weight: normal !important;']) !!}
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4" style="padding-left: 2px;">
                                            {!! Form::text('total_weight', old('total_weight',!empty($customer_requirement)?$customer_requirement[0]['shipment_weight']:null ), ['class' => 'form-control validate[required, custom[integer]] margin-top-0', 'placeholder'=>'']) !!}
                                        </div>
                                        <div class="col-md-4" style="padding-left: 2px;">
                                            {!! Form::text('total_volume', old('total_volume',!empty($customer_requirement)?$customer_requirement[0]['shipment_volume']:null), ['class' => 'form-control validate[required, custom[integer]] margin-top-0', 'placeholder'=>'']) !!}
                                        </div>
                                        <div class="col-md-4">
                                            <input type="checkbox" name="unit" id="unit1" value="1" class="validate[required]radio chk margin-top-0" @if(!empty($customer_requirement))  @if($customer_requirement[0]['units']=='1') {{ "checked" }} @endif @endif> KILOGRAMS(KG)/CUBIC METERS(CBM)<br>
                                            <input type="checkbox" name="unit" id="unit2" value="2" class="validate[required]radio chk margin-top-0" @if(!empty($customer_requirement))  @if($customer_requirement[0]['units']=='2') {{ "checked" }} @endif @endif> POUNDS(LB)/CUBIC FEET(CFT)
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="shipment_detail_div" style="margin-bottom:3%">

                                    <div class="col-md-12">
                                        <label for="incoterm" class="control-label col-md-12" style="text-align:left;padding-top:0px;">INCOTERMS <abbr title="incoterms"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="switch-field">
                                            @foreach($incoterm as $incoterms)
                                                <input type="radio" id="incoterm{{ $incoterms->id }}" name="incoterm" value="{{ $incoterms->id }}" class="validate[required]radio" @if(!empty($customer_requirement)) @if($customer_requirement[0]['incoterms']==$incoterms->id) {{ "checked" }} @endif @endif/>
                                                <label for="incoterm{{ $incoterms->id }}" >{{ $incoterms->name }}</label>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="port_of_origin" class="control-label col-md-12" style="text-align:left">CITY, PROVINCE OF SUPPLIER'S LOCATION <abbr title="City, Province of Supplier's Location"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('port_of_origin',old('port_of_origin',!empty($customer_requirement)?$customer_requirement[0]['port_of_origin']:null ),['class'=>'margin-top-0 form-control validate[required]','id'=>'port_of_origin']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="good_ready_date" class="control-label col-md-12" style="text-align:left">APPROXIMATE GOOD READY DATE <abbr title="approx. good ready date"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            {!! Form::text('good_ready_date', old('good_ready_date',!empty($customer_requirement)?date('m/d/Y',strtotime($customer_requirement[0]['goods_ready_date'])) : null), ['class' => 'form-control datepicker validate[required] margin-top-0', 'placeholder'=>'&#xf073; MM/DD/YYYY']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="shipment_note" class="control-label col-md-12" style="text-align:left">ADDITIONAL SHIPMENT NOTES </label>
                                    </div>
                                    <div class="col-md-12">
                                        {!! Form::textarea('shipment_note',!empty($customer_requirement)?$customer_requirement[0]['shipment_notes']: null ,['class'=>'form-control', 'rows' => 4, 'cols' => 40]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 darkheading">
                                <span class="heading col-md-6 no-padding">CUSTOM DETAILS </span><span class="col-md-6 " style="text-align: right" id="custom_detail_span"><input type="checkbox" name="custom_details" class="my-checkbox" id="custom_details" disabled><span>I do not require custom support</span></span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" id="custom_detail_div" style="margin-bottom: 3%">
                                    <div class="col-md-12">
                                        <label for="shipment_contain" class="control-label col-md-12" style="text-align:left">DOES YOUR SHIPMENT CONTAIN... <abbr title="Does your shipment contain"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        @foreach($shipment_contain as $shipment_contains)
                                            <div class="col-md-6">
                                                <input type="checkbox" name="shipment_contain[]" id="shipment_contain{{$shipment_contains->id}}" value="{{ $shipment_contains->id }}"  @if(!empty($custom_detail)) @if(in_array($shipment_contains->id,$custom_detail)){{ "checked" }} @endif @endif>&nbsp;{{ $shipment_contains->name }}?
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 darkheading">
                                <span class="heading col-md-6 no-padding">FBAFORWARD WAREHOUSE SERVICES (OPTIONAL) <abbr title="fbaforward warehouse services(optional) "><i class="fa fa-question-circle compulsary"></i></abbr></span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" id="warehouse_service_div">
                                    <div class="col-md-12">
                                        <label for="prep_service" class="control-label col-md-12" style="text-align:left">CHOOSE PREP SERVICES: <abbr title="choose prep services "><i class="fa fa-question-circle compulsary" aria-hidden="true"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-8">
                                            @foreach($prep_service as $prep_services)
                                                <div class="col-md-6"><input type="checkbox" name="prep_service[]" id="prep_service{{ $prep_services->prep_service_id }}" @if(!empty($prep_detail)) @if(in_array($prep_services->prep_service_id,$prep_detail)){{ "checked" }} @endif @endif value="{{ $prep_services->prep_service_id }}"> {{ $prep_services->service_name }}</div>
                                            @endforeach
                                        </div>
                                        <div class="col-md-4">
                                            <label for="prep_note" class="control-label col-md-12" style="text-align:left; padding-left: 2% !important;">PREP NOTES <abbr title="prep notes "><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                            <div class="col-md-12">
                                                {!! Form::textarea('prep_note',!empty($customer_requirement)?$customer_requirement[0]['prep_note']: null , ['class' => 'form-control', 'placeholder'=>'','rows'=>2, 'cols'=>60 ]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="inspection_service" class="control-label col-md-12" style="text-align:left; ">CHOOSE INSPECTION SERVICES: <abbr title="choose inspection services "><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            @foreach($inspection_service as $key=>$inspect_services)
                                                <input type="text" name="inspect_service{{$inspect_services->id}}"  id="inspect_service{{$inspect_services->id}}" value="{{!empty($inspection_detail_data)?$inspection_detail_data[$inspect_services->id]:''}}" placeholder="QTY" class="validate[custom[integer]]" style="width:18%; height: 2%; border:1px solid #ccc;">
                                                &nbsp;&nbsp;{{ $inspect_services->name }}<br>
                                            @endforeach
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="inspect_note" class="control-label col-md-12" style="text-align:left; padding-left: 2% !important;">INSPECTION NOTES <abbr title="inspection notes "><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                            <div class="col-md-12">
                                                {!! Form::textarea('inspect_note',!empty($customer_requirement)?$customer_requirement[0]['inspection_note']:null , ['class' => 'form-control', 'placeholder'=>'','rows'=>2, 'cols'=>60 ]) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 darkheading">
                                <span class="heading col-md-12 no-padding">OUTBOUND SHIPMENT DETAILS </span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" id="outbound_detail_div" style="margin-bottom: 0%">
                                    <div class="col-md-12">
                                        <label for="outbound" class="control-label col-md-12" style="text-align:left">OUTBOUND SHIPPING METHOD DESIRED <abbr title="Domestic shipping from FBAforward to Amazon Fulfillment Centers"><i class="fa fa-question-circle compulsary"></i></abbr></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-12">
                                            <div class="switch-field">
                                                @foreach($outbound_method as $outbound_methods)
                                                    <input type="radio" id="outbound{{$outbound_methods->outbound_method_id}}" name="outbound" value="{{$outbound_methods->outbound_method_id}}" class="validate[required]radio"
                                                    @if(!empty($customer_requirement)) @if($customer_requirement[0]['outbound_method_id']==$outbound_methods->outbound_method_id) {{ "checked" }} @endif @endif/>
                                                    <label for="outbound{{$outbound_methods->outbound_method_id}}">{{ $outbound_methods->outbound_name }}</label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="" style="margin-bottom: 3%">
                                    <div class="col-md-12">
                                        <label for="outbound_note" class="control-label col-md-12" style="text-align:left">OUTBOUND SHIPMENT NOTES </label>
                                    </div>
                                    <div class="col-md-12">
                                        {!! Form::textarea('outbound_note',!empty($customer_requirement)?$customer_requirement[0]['outbound_method_note']: null , ['class' => 'form-control', 'placeholder'=>'','rows'=>2, 'cols'=>60 ]) !!}
                                    </div>
                                </div>
                            </div>
                            @if($resubmit_quote)
                                <input type="hidden" name="resubmit_quote" value="{{ $resubmit_quote }}">
                                <input type="hidden" name="old_customer_requirement_id" value="{{ $customer_requirement_id }}">
                            @endif
                            <div class="col-md-12" style="text-align: right">
                                {!! Form::submit('  Submit Quote Request  ', ['class'=>'button btn-submit-quote']) !!}
                            </div>
                        </div><!-- .row -->
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            // Validation Engine init
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach',
                {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#shipment_details").change(function(){
                if($("#shipment_details").prop('checked') == true){
                    $('#shipping_method').html('INTERNATIONAL SHIPPING METHOD <i class="fa fa-question-circle compulsary"</i>');
                }
                else{
                    $('#shipping_method').html('<label id="shipping_method" for="shipping_method" class="control-label col-md-12" style="text-align:left">INTERNATIONAL SHIPPING METHOD DESIRED<abbr title="shipping method desired"><i class="fa fa-question-circle compulsary"></i></abbr><span style="margin-left: 10px; font-style: italic; font-size: 12px;">Select all shipping methods that you would like to receive a quote for</span></label>');
                }
            });
           //Initialize Select2 Elements
            $(".select2").select2({
                placeholder: "Please Select",
                allowClear: true
            });
            $('.select2-container').css('width','995px');
        });
        $(document).ready(function () {
            $('.datepicker').datepicker({
                startDate: new Date(),
            });
            $('input[type=radio][name=select_product]').change(function() {
                if(this.value == 0){
                    $('#product_selectbox').removeClass('hide');
                    $('#product_textbox').hide();
                    $('#product_selectbox').show();
                }
                if(this.value == 1){
                    $('#product_textbox').removeClass('hide');
                    $('#product_selectbox').hide();
                    $('#product_textbox').show();

                    $.ajax({
                        headers: {
                            'X-CSRF-Token': "{{ csrf_token() }}"
                        },
                        method: 'POST',
                        url: '{{ url("quote/get-all-supplier") }}',
                        data: {
                            '_token': '{{ csrf_token() }}',
                            'user_id': $('#customer').val()
                        },
                        success: function (supplier) {
                            var html = '';
                            html+= '<select name="supplier" id="supplier" class = "form-control validate[required] select2">';
                            html+= '<option value=""></option>';
                            if(supplier.length ==0){
                               html+= '<option value=""></option>';
                            }
                            else{
                                $.each(supplier, function( key, value ) {
                                    html+= "<option value="+value.supplier_id+">"+value.contact_name+"</option>";
                                });
                            }
                            html+='</select>';
                            $('#supplier').html(html);


                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        }
                    });
                    
                }
            });
            $('input[type=radio][name=select_supplier]').change(function() {
                if(this.value == 0){
                    $('#supplier_selectbox').removeClass('hide');
                    $('#supplier_textbox').hide();
                    $('#supplier_selectbox').show();
                }
                if(this.value == 1){
                    $('#supplier_textbox').removeClass('hide');
                    $('#supplier_selectbox').hide();
                    $('#supplier_textbox').show();
                    $("#port_of_origin").val('');

                    /*$.ajax({
                        headers: {
                            'X-CSRF-Token': "{{ csrf_token() }}"
                        },
                        method: 'POST',
                        url: '{{ url("quote/get-all-supplier") }}',
                        data: {
                            '_token': '{{ csrf_token() }}',
                            'user_id': $('#customer').val()
                        },
                        success: function (supplier) {
                            var html = '';
                            html+= '<select name="supplier" id="supplier" class = "form-control validate[required] select2">';
                            html+= '<option value=""></option>';
                            if(supplier.length ==0){
                                html+= '<option value=""></option>';
                            }
                            else{
                                $.each(supplier, function( key, value ) {
                                    html+= "<option value="+value.supplier_id+">"+value.contact_name+"</option>";
                                });
                            }
                            html+='</select>';
                            $('#supplier').html(html);


                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        }
                    });*/

                }
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#customer').change(function(){
                $.ajax({
                    headers: {
                        'X-CSRF-Token': "{{ csrf_token() }}"
                    },
                    method: 'POST',
                    url: '{{ url("quote/get-product") }}',
                    data: {
                        '_token': '{{ csrf_token() }}','user_id': $('#customer').val(),
                    },
                    success: function (data) {
                        var html = '';
                        var html2 = '';
                        html+= '<select name="product" id="product" class = "form-control validate[required] select2" onchange="getsupplier(this.value)">';
                        html+= '<option value=""></option>';
                        if(data.products.length ==0){
                           html+= '<option value=""></option>';
                        }
                        else{
                            $.each( data.products, function( key, value ) {
                                html+= "<option value='"+value.id+" "+value.FNSKU+"'>"+value.product_name+"</option></select>";
                            });
                        }
                        $('#product').html(html);
                        html2+= '<select name="supplier" id="supplier" class = "validate[required] select2">';
                        html2+= '<option value=""></option>';
                        if(data.suppliers.length ==0){
                           html+= '<option value=""></option>';
                        }
                        else{
                            $.each( data.suppliers, function( key, value ) {
                            html2+= '<option value='+value.supplier_id+'>'+value.contact_name+'</option>';
                            });
                        }
                        html2+='</select>';
                        $('#supplier').html(html2);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            });
        });
        $('#supplier').change(function(){
            $.ajax({
                headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '/quote/get-supplier-location',
                data: {
                    'supplier_id': $('select[name=supplier]').val(),
                },
                success: function (response) {
                    //$('#form-validation-field-0').val(response); port_of_origin
                    $('input[name=port_of_origin]').val(response)
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        });

        $("#shipment_details").change(function() {
            if(this.checked) {
                //Do stuff
                $("#shipment_detail_div").hide();
                // $("#custom_detail_span").removeClass('checkbox');
                $("#custom_details").removeAttr("disabled");
            }
            else {
                $("#shipment_detail_div").show();
                // $("#custom_detail_span").addClass('checkbox');
                $("#custom_details").attr("disabled","disabled");
                $('#custom_details').attr('checked', false);
                $("#custom_detail_div").show();
            }
        });
        $("#custom_details").change(function() {
            if(this.checked) {
                $("#custom_detail_div").hide();
            }
            else {
                $("#custom_detail_div").show();
            }
        });
        function divtoggle() {
            $("#warehouse_service_div").toggle();
            if($("#button").hasClass("fa fa-plus-circle")) {
                $("#button").removeClass("fa fa-plus-circle");
                $("#button").addClass("fa fa-minus-circle");
            }
            else {
                $("#button").removeClass("fa fa-minus-circle");
                $("#button").addClass("fa fa-plus-circle");
            }
        }
        function calculation() {
            no_carton=1;
            qty_carton=1;
            if($("#no_of_carton").val()!='') {
                no_carton = $("#no_of_carton").val();
                total = no_carton*qty_carton;
            }
            if($("#qty_per_carton").val()!='') {
                qty_carton = $("#qty_per_carton").val();
                total = no_carton*qty_carton;
            }
            if($("#no_of_carton").val()=='' && $("#qty_per_carton").val()==''){
                total = '';
            }
            $("#total_unit").val(total);
        }
        $('abbr').click(function(){
            $(this).mouseover();
            // after a slight 2 second fade, fade out the tooltip for 1 second
            $(this).next().animate({opacity: 0.9},{duration: 2000, complete: function(){
                $(this).fadeOut(1000);
            }});
        });
        $('.chk').click(function(){
            $('.chk').each(function(){
                $(this).prop('checked', false);
            });
            $(this).prop('checked', true);
        });
         function getsupplier(product_id){
                $.ajax({
                    headers: {
                        'X-CSRF-Token': "{{ csrf_token() }}"
                    },
                    method: 'POST',
                    url: '{{ url("quote/get-supplier") }}',
                    data: {
                        '_token': '{{ csrf_token() }}','user_id': $('#customer').val(),'product_id': product_id,
                    },
                    success: function (supplier) {
                        var html = '';
                        html+= '<select name="supplier" id="supplier" class = "validate[required] select2">';
                        html+= '<option value=""></option>';
                        if(supplier.length ==0){
                           html+= '<option value=""></option>';
                        }
                        else{
                            $.each(supplier, function( key, value ) {
                                html+= "<option value="+value.supplier_id+">"+value.contact_name+"</option>";
                            });
                        }
                        $('#supplier').html(html);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
            function getchecked()
            {
                if ( $('#factory_audit').is( ":checked" ) || $('#pre_inspection').is( ":checked" ) ){
                        $("#pre_ship_div").show();
                }
                else{
                    $("#pre_ship_div").hide();
                    $("#pre_ship_note").val('');
                }
            }
    </script>
@endsection