<html>
<head>

</head>
<body>
<style>
.heading{
    color: #7d7d7d;
    font-weight: 600;
    font-size: 30px;
}
.grey-font{
    color: #7d7d7d;
    font-weight: 600;
}
.border-box{ border:1px solid #e0dddd; }
.print_quote >thead>tr>th{ background-color:#0088cc !important;color:#fff;text-transform: uppercase; }
.print_quote >tbody{ border:1px solid #e0dddd;border-top: none; }
.print_quote >tbody>tr>td{ border-right:1px solid #e0dddd; }
.border-box{ border:1px solid #e0dddd; }
.margin-top-30{ margin-top: 30px; }
.contact-info{ color:#0088cc; }
.border-top-1{ border-top:1px solid #e0dddd; }
.margin-top-10{ margin-top: 10px; }
.margin-bottom-0{ margin-bottom: 0px; }
.padding-10{ padding: 10px; }
.border-1{ border:1px solid #e0dddd;border-top: none; }
.font-size-16{ font-size: 16px; }
.text-center{ text-align: center; }
@media (min-width: 992px){
    .col-md-12 {
        width: 100%;
    }
    .col-md-offset-3 {
        margin-left: 25%;
    }
    .col-md-3 {
        width: 25%;
    }
}
.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
    /*float: left;*/
}
.col-md-8 {
    width: 66.66666667%;
}
.col-md-6 {
    width: 50%;
}
.col-md-3 {
    width: 25%;
}
.col-md-9 {
    width: 75%;
}
.pull-right{
    float: right;
}
.col-md-5 {
    width: 41.66666667%;
}
.col-md-7 {
    width: 58.33333333%;
}
.logo-font p,.font-size-13 { font-size: 13px; }
.no-border tbody{ border: none !important; }
.print_quote  {
    background-color: transparent;
    border-spacing: 0;
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 30px;    
}
.print_quote >thead>tr>th,table>tbody>tr>td{
    padding: 8px;
    text-align: center;
}
.no-border{ border: none !important; }
.margin-top-60{ margin-top:60px; }
.margin-top-20{ margin-top:40px; }
.page-break{
    page-break-after: always;
}
.text-left{ text-align: left; }
@page { margin-bottom: 0px;margin-top:10px; }
</style>
{{--*/ $charge_qty = 1 /*--}}
{{--*/ $shipping_sub_total = 0 /*--}}
{{--*/$custom_sub_total=0;/*--}}
{{--*/$receive_forward_sub_total=0;/*--}}
{{--*/ $prep_price = "" /*--}}
{{--*/ $prep_sub_total = 0 /*--}}
{{--*/ $trucking_qty="" /*--}}
{{--*/ $port_fee = 0 /*--}}
{{--*/ $port_fee_name =  env('PORT_FEE_NAME') /*--}}
{{--*/ $trucking_name =  env('TRUCKING_NAME') /*--}}
{{--*/ $isf_rate =  env('ISF_RATE') /*--}}
{{--*/ $isf_name =  env('ISF_NAME') /*--}}
{{--*/ $entry_rate = env('ENTRY_RATE') /*--}}
{{--*/ $entry_name = env('ENTRY_NAME') /*--}}
{{--*/ $bond_rate =  env('BOND_RATE') /*--}}
{{--*/ $bond_name =  env('BOND_NAME') /*--}}
{{--*/ $container_unloading= env('CONTAINER_UNLOADING') /*--}}
{{--*/ $palletizing= env('PALLETIZING') /*--}}
{{--*/ $amazon_approve_pallet= env('AMAZON_APPROVE_PALLET') /*--}}
{{--*/ $pallet_charge= env('pallet_charge') /*--}}
@if($details->units==1)
    {{--*/$trucking_qty=round($details->shipment_volume / env('PALLET_CBM')) /*--}}
@else
    {{--*/$trucking_qty=round($details->shipment_volume / env('PALLET_CFT'))/*--}}
@endif
@if($trucking_qty > env('TRUCKING_QTY'))
    {{--*/ $trucking_price = ($trucking_qty -  env('TRUCKING_QTY')) * env('TRUCKING_ADDITIONAL_PRICE') + env('TRUCKING_PRICE')/*--}}
@else
    {{--*/ $trucking_price =  env('TRUCKING_PRICE') /*--}}
@endif
@if($details->shipment_type_id==1)
    @if($details->units==1)
        {{--*/$trucking_qty=round($details->shipment_volume / env('PALLET_CBM'))/*--}}
    @else
        {{--*/$trucking_qty=round($details->shipment_volume / env('PALLET_CFT'))/*--}}
    @endif

    {{--*/ $trucking_price = $trucking_qty* env('TRUCKING_PRICE')  /*--}}
@else

    {{--*/ $trucking_qty=1 /*--}}
    {{--*/ $trucking_price = $details->trucking_rate /*--}}
@endif

<div class="container" style="margin: 0px;">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6" style="float:left;">
     
                <img src = "<?php echo public_path();?>/uploads/images/FBAFORWARD_LOGO.png" style="width:70%;">
                <div class="col-md-12 grey-font">
                    <p style="margin:5px 0px;font-size: 15px;">1550 HOTEL CIRCLE NORTH</p>
                    <p style="margin:5px 0px;font-size: 15px;">SUITE 360</p>
                    <p style="margin:5px 0px;font-size: 15px;">SAN DIEGO, CA 92108</p>
                    <p style="margin:5px 0px;font-size: 15px;">+1 866 526 3951</p>
                </div>
            </div>
            <div class="col-md-6" style="margin-left: 53%;">
                <h1 class="heading">SHIPPING QUOTE</h1>
                <div class="col-md-12 grey-font">
                    <p style="margin:5px 0px;font-size: 15px;"><b>FOR : </b>{{ $detail_method->company_name }}</p>
                    <p style="margin:5px 0px;font-size: 15px;"><b>Email : </b>{{ $email }}</p>
                </div>
            </div>
        </div>

        {{--*/ $total_charge = 0 /*--}}

        <div class="col-md-12" style="margin-top: 50px;">
            <table class="table print_quote">
                <thead>
                    <tr>
                        <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;">QUOTE TYPE</th>
                        <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;">ITEM DESCRIPTION</th>
                        <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;">QUOTE DATE</th>
                        <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;">EXPIRATION DATE</th>
                    </tr>
                </thead>
                <tbody style="border: 1px solid #e0dddd;border-top: none;">
                    <tr>
                        <td style="border-right: 1px solid #e0dddd;">{{ $details->shipping_name }}</td>
                        <td style="border-right: 1px solid #e0dddd;">{{ $details->product_name }}</td>
                        <td style="border-right: 1px solid #e0dddd;">{{ date('m/d/Y',strtotime($details->created_at)) }}</td>
                        <td style="border-right: 1px solid #e0dddd;">@if($details->expire_date) {{ date('m/d/Y',strtotime($details->expire_date)) }} @endif</td>
                    </tr>
                </tbody>
            </table>

            <table class="table print_quote">
                <thead>
                    <tr>
                        <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;">WEIGHT (@if($details->units==1){{"KG"}}@else{{"LB"}}@endif)</th>
                        <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;">VOLUME (@if($details->units==1){{"CBM"}}@else{{"CFT"}}@endif)</th>
                        <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;">SHIPPING PORT</th>
                        <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;"># OF UNITS</th>
                        <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;">TERMS</th>
                    </tr>
                </thead>
                <tbody style="border: 1px solid #e0dddd;border-top: none;">
                    <tr>
                        <td style="border-right: 1px solid #e0dddd;">{{ $details->chargeable_weight }}</td>
                        <td style="border-right: 1px solid #e0dddd;">{{ $details->shipment_volume }}</td>
                        <td style="border-right: 1px solid #e0dddd;">@if($shipping_quote){{ $shipping_quote->shipping_port }}@endif</td>
                        <td style="border-right: 1px solid #e0dddd;">{{ $details->qty_per_box }}</td>
                        <td style="border-right: 1px solid #e0dddd;">{{ $details->incoterm_short_name }}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table print_quote" style="margin-bottom: 10px;">
                <thead>
                    <tr>
                        <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-9">CHARGE</th>
                        <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3">AMOUNT</th>
                    </tr>
                </thead>
                <tbody style="border: 1px solid #e0dddd;border-top: none;">
                    <tr>
                        <td class="col-md-9 text-left">INTERNATIONAL SHIPPING & PORT FEES</td>
                        <td class="col-md-3 text-right" id="international_shipping">${{ $sum_details_charge }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-9 text-left">U.S. CUSTOMS BROKERAGE</td>
                        <td class="col-md-3 text-right" id="custom_sub_total">${{ $sum_custom_total }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-9 text-left">RECEIVING & FORWARDING</td>
                        <td class="col-md-3 text-right" id="receive_forward_sub_total">${{ $sum_receive_forward_total }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-9 text-left">PREP & INSPECTION</td>
                        <td class="col-md-3 text-right" id="prep_sub_total">${{ $sum_prep_total }}</td>
                    </tr>
                </tbody>
            </table>

            <div class="col-md-12" style="padding:0px">
                <table class="table print_quote margin-bottom-0 no-border" style="margin-bottom:0px;">
                    <tr>
                        <td class="col-md-9 no-border" style="text-align:right;">TOTAL COST :</td>
                        <td class="col-md-3 border-box" style="width:25%;margin-right:30%;" id="grand_total">${{ $total_cost }}</td>
                    </tr>
                </table>
            </div>

            <table class="table margin-bottom-0 no-border print_quote" style="margin-top: 10px;">
                <tr>
                    <td class="col-md-9" style="text-align:right;">PER UNIT COST:</td>
                    <td class="col-md-3 border-box" id="per_unit_cost">${{  number_format(($total_cost)/$details->qty_per_box,2) }}</td>
                </tr>
            </table>

        <div class="col-md-12" style="position:relative;">
            <div class="col-md-12 text-center" style="position:relative;">
                <h4 class="grey-font" style="margin:0px 0px;">We look forward to helping you build, grow, and scale your e-commerce business!</h4>
            </div>

            <div class="col-md-12 contact-info text-center" style="position:relative;">
                <div class="col-md-12">
                    <p class="col-md-12" style="position:relative;">If you have any questions about this quote, please call or email our order support team:</p>
                    <div class="col-md-3 col-md-offset-3 text-left" style="width:27%;float:left;margin-left:12%;position:relative;">
                        Phone:+1 866 526 3951
                    </div>

                    <div class="col-md-3 text-left" style="float:left;margin-left:50%;position:relative;"> Email:support@fbaforward.com
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 text-center grey-font border-box" style="position:relative;float:left;margin-top:30px;">
            <p>The above quote is valid for 7 days.  Quoted amounts are subject to change as shipping rates and service amounts fluctuate, however changes of greater than 5% are very unlikely.  Port fees are an estimate based off of our preivous experiences, and generally fall between $200 and $300 for sea shipments, and $50 and $150 for air shipments, however can be significantly increased if your shipment is flagged for a random customs x-ray or intensive exam.  Customs duties vary by product and are not included in this quote.  You can find the duty rate for your product by visiting www.dutycalculator.com.  All charges quoted are from third party service providers, and are aggregated and paid by FBAforward as a courtesy, and costs are passed on to you at face value.  FBAforward is not a provider of freight forwarding or customs brokerage services. </p>
        </div>

    </div>

    <div class="row ">
        <div class="col-md-12 ">
            <div class="page-break"></div>
            <h1 class="heading text-center">FEE BREAKDOWN</h1>
            <div class="col-md-12">
                @if($details->order_type==1)
                <table class="table print_quote" style="margin-bottom:10px;">
                    <thead>
                        <tr>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-6 no-border">Line Item</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Rate</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Quantity</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Total</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px solid #e0dddd;border-top: none;">
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$details->shipping_name}}</td>
                            <td class="col-md-2 no-border">${{$details->shipment_type_rate}}</td>
                            <td class="col-md-2 no-border">{{$details->shipment_volume}}</td>
                            <td class="col-md-2 no-border">${{$details->shipment_type_rate*$details->shipment_volume}}</td>
                        </tr>
                        
                        @foreach($detail_charge as  $charge_key=>$charge_value )
                            <tr>
                                <td style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-6 no-border text-left">{{$charge_value->name}}</td>
                                <td style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-2 no-border">${{$charge_value->charge}}</td>
                                <td style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-2 no-border">{{$charge_qty}}</td>
                                <td style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-2 no-border">${{$charge_value->charge * $charge_qty}}</td>
                            </tr>
                            
                        @endforeach
                        @if($details->shipping_name!=="undefined")
                            @if($details->shipping_method_id == 2)
                                {{--*/  $port_fee= env('PORT_FEE')/*--}}
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$port_fee_name}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee}}</td>
                                    <td class="col-md-2 no-border">{{$charge_qty}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee*$charge_qty}}</td>
                                </tr>

                                <tr>
                                    <td class="col-md-6 no-border text-left"> {{$trucking_name}}</td>
                                    <td class="col-md-2 no-border">${{$trucking_price}}</td>
                                    <td class="col-md-2 no-border">{{$trucking_qty}}</td>
                                    <td class="col-md-2 no-border">${{$trucking_price}}</td>
                                </tr>
                            @elseif($details->shipping_method_id == 1)
                                @if($details->shipment_type_id==1)
                                    {{--*/ $port_fee= env('OCEAN_LCL_PORT_FEE') /*--}}
                                @else
                                    {{--*/ $port_fee= env('OCEAN_PORT_FEE') /*--}}
                                @endif
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$port_fee_name}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee}}</td>
                                    <td class="col-md-2 no-border">{{$charge_qty}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee*$charge_qty}}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$trucking_name}}</td>
                                    <td class="col-md-2 no-border">${{$details->trucking_rate}}</td>
                                    <td class="col-md-2 no-border">{{$trucking_qty}}</td>
                                    <td class="col-md-2 no-border">${{$trucking_price = ($details->trucking_rate*$trucking_qty) }}</td>
                                </tr>
                            @else
                                @if($details->shipment_type_id==1)
                                    {{--*/ $port_fee= env('OCEAN_LCL_PORT_FEE') /*--}}
                                @else
                                    {{--*/ $port_fee= env('OCEAN_PORT_FEE') /*--}}
                                @endif
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$port_fee_name}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee}}</td>
                                    <td class="col-md-2 no-border">{{$charge_qty}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee*$charge_qty}}</td>
                                </tr>
                                {{--*/ $trucking_price = 0; /*--}}
                            @endif
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{ env('WIRE_TRANS_NAME') }}</td>
                                    <td class="col-md-2 no-border">${{ env('WIRE_TRANS_FEE') }}</td>
                                    <td class="col-md-2 no-border">&nbsp;</td>
                                    <td class="col-md-2 no-border">${{number_format((env('WIRE_TRANS_FEE')))}}</td>
                                </tr>
                            
                        @endif
                        <tr class="border-top-1">
                            <td class="col-md-6 no-border text-left">SUB TOTAL</td>
                            <td class="col-md-2 no-border">-</td>
                            <td class="col-md-2 no-border">-</td>
                            <td class="col-md-2 no-border"><b>${{$sum_details_charge}}</b></td>
                        </tr>
                        </tbody>
                </table>
                @endif
                @if($details->order_type==1 || $details->order_type==3)
                <h4 style="margin:0px;">U.S. CUSTOMS BROKERAGE</h4>
                <table class="table print_quote" style="margin-bottom:10px;">
                    <thead>
                        <tr>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-6 no-border">Line Item</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Rate</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Quantity</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Amount</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px solid #e0dddd;border-top: none;">
                       @if($details->shipping_method_id == 1)
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$isf_name}}</td>
                            <td class="col-md-2 no-border">${{$isf_rate}}</td>
                            <td class="col-md-2 no-border">{{$charge_qty}}</td>
                            <td class="col-md-2 no-border">${{$isf_rate * $charge_qty}}</td>
                        </tr>
                       
                        @endif
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$entry_name}}</td>
                            <td class="col-md-2 no-border">${{$entry_rate}}</td>
                            <td class="col-md-2 no-border">{{$charge_qty}}</td>
                            <td class="col-md-2 no-border">${{ $entry_rate*$charge_qty}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$bond_name}}</td>
                            <td class="col-md-2 no-border">${{$bond_rate}}</td>
                            <td class="col-md-2 no-border">{{$charge_qty}}</td>
                            <td class="col-md-2 no-border">${{$bond_rate*$charge_qty}}</td>
                        
                        </tr>
                        <tr class="border-top-1">
                            <td class="col-md-6 no-border text-left">SUB TOTAL</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border"><b>${{ $sum_custom_total }}</b></td>
                        </tr>
                    </tbody>
                </table>
                @endif
                <h4 style="margin:0px;">RECEIVING & FORWARDING</h4>
                <table class="table print_quote" style="margin-bottom:10px;">
                    <thead>
                        <tr>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-6 no-border">Line Item</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Rate</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Quantity</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Amount</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px solid #e0dddd;border-top: none;">
                        @if($details->shipment_type_id >1)
                        <tr>
                            <td class="col-md-6 no-border text-left">Container Unloading</td>
                            <td class="col-md-3 no-border">${{$details->container_unload_rate}}</td>
                            <td class="col-md-3 no-border">{{$charge_qty}}</td>
                            <td class="col-md-3 no-border">${{$details->container_unload_rate*$charge_qty}}</td>
                        </tr>
                        @endif
                       
                        @if($details->outbound_method_id >1)
                            <tr>
                                <td class="col-md-6 no-border text-left">{{$palletizing}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge}}</td>
                                <td class="col-md-3 no-border">{{$trucking_qty}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge*$trucking_qty}}</td>
                            </tr>
                            
                            <tr>
                                <td class="col-md-6 no-border text-left">{{$amazon_approve_pallet}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge}}</td>
                                <td class="col-md-3 no-border">{{$trucking_qty}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge*$trucking_qty}}</td>
                            </tr>
                           
                        @endif
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$details->outbound_name}}</td>
                            <td class="col-md-3 no-border">&nbsp;</td>
                            <td class="col-md-3 no-border">&nbsp;</td>
                            <td class="col-md-3 no-border"><i>varies</i></td>
                        </tr>
                        <tr class="border-top-1">
                            <td class="col-md-6 no-border text-left">SUB TOTAL</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border"><b>${{ $sum_receive_forward_total }}</b></td>
                        </tr>
                    </tbody>
                </table>

                <!-- <div class="page-break"></div> -->
                <h4 style="margin:0px;">PREP & INSPECTION</h4>
                <table class="table print_quote" style="margin-bottom:10px;">
                    <thead>
                        <tr>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-6 no-border">Line Item</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Rate</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Quantity</th>
                            <th style="background-color: #0088cc !important;color: #fff;text-transform: uppercase;padding: 8px;text-align: center;" class="col-md-3 no-border">Amount</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px solid #e0dddd;border-top: none;">
                        @foreach($detail_prep as $prep_key=>$prep_value )
                            {{--*/$prep_price = $prep_value->price * $details->total/*--}}
                            @if($prep_price % 1 !=0)
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$prep_value->service_name}}</td>
                                    <td class="col-md-3 no-border">${{$prep_value->price}}</td>
                                    <td class="col-md-3 no-border">{{$details->total}}</td>
                                    <td class="col-md-3 no-border">${{number_format($prep_price)}}</td>
                                    
                                </tr>
                            @else
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$prep_value->service_name}}</td>
                                    <td class="col-md-3 no-border">${{$prep_value->price}}</td>
                                    <td class="col-md-3 no-border">{{$details->total}}</td>
                                    <td class="col-md-3 no-border">${{number_format($prep_price)}}</td>
                                   
                                </tr>
                            @endif
                        @endforeach
                        <tr class="border-top-1">
                            <td class="col-md-6 no-border text-left">TOTAL</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border"><b>${{ $sum_prep_total }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
</body>
</html>