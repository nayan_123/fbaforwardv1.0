@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
    {!! Html::style('assets/dist/css/style.css') !!}
@endsection
<style type="text/css">
.previous a:hover{  color:#777 !important;  }
.next a:hover{  color:#777 !important;  }
@media (max-width: 1000px){  .search-label{ display:none; }  }
    tbody{ font-size: 14px; }
    thead{ font-size: 14px;vertical-align:middle; }
</style>
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-anchor"></i> {{$title}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-anchor"></i> {{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                @if($user_role_id=='3')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4 padding-0-5">
                                <h3>Active Quotes</h3>
                            </div>
                            <div class="col-md-8">
                                <div class="pull-right margin-top-10 btn-align">
                                    @if($user_role_id=='4')
                                        <a href="{{ url('quote/create/?user_role='.$user_role_id) }}" class="btn btn-success pull-right padding-4-12">Request New Quote</a>
                                    @elseif($user_role_id=='3')
                                        <a href="{{ url('quote/create') }}" class="btn btn-success pull-right padding-4-12">Request New Quote</a>
                                    @endif
                                </div>

                                <div class="pull-right no-padding">
                                    <div class="col-md-2">
                                        <label class="search-label">Search:</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" placeholder="Search" class="form-control input-sm" id="search_all">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 margin-top-20">
                            <table id="quote" class="table">
                                <thead>
                                <tr>
                                    <th class="col-md-6">Product Name</th>
                                    <th class="col-md-1 no-padding">Units</th>
                                    <th class="col-md-1 no-padding">Cartons</th>
                                    <th class="col-md-2 no-padding">Date Quoted</th>
                                    <th class="col-md-2 no-padding">View & Approve Quote</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($active_quote)>0)
                                    @foreach($active_quote as $active_quotes)
                                        <tr id="tr{{$active_quotes->id}}">
                                            <td class="col-md-6">

                                                @if(empty($active_quotes->customer_requirement_details_product_name))
                                                    {{ $active_quotes->product_name }}
                                                @else
                                                    {{ $active_quotes->customer_requirement_details_product_name }}
                                                @endif
                                            </td>
                                            <td class="col-md-1">{{ $active_quotes->total }}</td>
                                            <td class="col-md-1">{{ $active_quotes->no_boxs }}</td>
                                            <td class="col-md-2">
                                                @if(empty($active_quotes->shipping_id) && $active_quotes->order_type=='1')
                                                    PENDING
                                                @else
                                                    {{ date('m/d/Y',strtotime($active_quotes->created_at)) }}
                                                @endif
                                            </td>
                                            <td class="col-md-2 text-center" id="td_button{{$active_quotes->id}}">
                                                @if($active_quotes->order_type!='1')
                                                    <a href="{{ url('quote/edit/'.$active_quotes->id.'/active_quotes') }}" class="button border-radius col-md-12" name="view{{$active_quotes->id}}" id="view{{$active_quotes->id}}">View Quote</a>
                                                @else
                                                    @if((!empty($active_quotes->shipping_id) && ($active_quotes->shipping_quote_is_activated=='0'))   || empty($active_quotes->shipping_method_id))
                                                        <a href="{{ url('quote/edit/'.$active_quotes->id.'/active_quotes') }}" class="button border-radius col-md-12" name="view{{$active_quotes->id}}" id="view{{$active_quotes->id}}">View Quote</a>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            @if(count($expire_quote) > 0)
                            <h3 class="padding-0-5">Expired Quotes</h3>
                            <table class="table" id="expire_quote">
                                <thead>
                                <tr>
                                    <th class="col-md-6"></th>
                                    <th class="col-md-1"></th>
                                    <th class="col-md-1"></th>
                                    <th class="col-md-2"></th>
                                    <th class="col-md-2 text-center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($expire_quote))
                                    @foreach($expire_quote as $expire_quotes)
                                        <tr>
                                            <td class="col-md-6">
                                                @if(empty($expire_quotes->customer_requirement_details_product_name))
                                                   {{ $expire_quotes->product_name }}
                                                @else
                                                    {{ $expire_quotes->customer_requirement_details_product_name }}
                                                @endif
                                            </td>
                                            <td class="col-md-1">{{ $expire_quotes->total }}</td>
                                            <td class="col-md-1">{{ $expire_quotes->no_boxs }}</td>
                                            <td class="col-md-2">EXPIRED ON {{ date('m/d/Y',strtotime($expire_quotes->expire_date)) }}</td>
                                            <td class="col-md-3 text-center"><a class="button border-radius col-md-12" href="{{ url('/quote/create?id='.$expire_quotes->id) }}">REQUEST AGAIN</a></td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            @endif
                            @if(count($past_quote) > 0)
                            <h3 class="padding-0-5">Past Quotes</h3>
                            <table class="table" id="past_quote">
                                <thead>
                                <tr>
                                    <th class="col-md-6"></th>
                                    <th class="col-md-1"></th>
                                    <th class="col-md-1"></th>
                                    <th class="col-md-2"></th>
                                    <th class="col-md-2 text-center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($past_quote))
                                    @foreach($past_quote as $past_quotes)
                                        <tr id="tr{{$past_quotes->id}}">
                                            <td class="col-md-6">
                                                @if(empty($past_quotes->customer_requirement_details_product_name))
                                                    {{ $past_quotes->product_name }}
                                                @else
                                                    {{ $past_quotes->customer_requirement_details_product_name }}
                                                @endif
                                            </td>
                                            <td class="col-md-1">{{ $past_quotes->total }}</td>
                                            <td class="col-md-1">{{ $past_quotes->no_boxs }}</td>
                                            <td class="col-md-2"> {{ date('m/d/Y',strtotime($past_quotes->created_at)) }}</td>
                                            <td class="col-md-2 text-center" id="td_button{{$past_quotes->id}}">
                                                <a href="{{ url('quote/edit/'.$past_quotes->id.'/past_quotes') }}" class="button border-radius col-md-12" name="view{{$past_quotes->id}}" id="view{{$past_quotes->id}}" onclick="getdetails('{{$past_quotes->id}}')">View Quote</a>
                                                {{--$past_quotes->shipping_name--}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            @endif
                        @elseif($user_role_id=='4')
                            <table id="quote" class="table" >
                                <thead>
                                <tr>
                                    <th class="col-md-6">Active Quote Requests</th>
                                    <th class="col-md-2">Company</th>
                                    <th class="col-md-2">Date Quoted</th>
                                    <th class="col-md-2">View & Approve Quote</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($active_quote)>0)
                                    @foreach($active_quote as $active_quotes)
                                        <tr id="tr{{$active_quotes->id}}">
                                            <td class="col-md-6">
                                                {{ $active_quotes->product_name }}
                                            </td>
                                            <td class="col-md-2">
                                                {{$active_quotes->company_name}}
                                            </td>
                                            <td class="col-md-2">
                                                @if(empty($active_quotes->shipping_method_id))
                                                    No entries
                                                @elseif(empty($active_quotes->shipping_id))
                                                    PENDING
                                                @else
                                                    {{ date('m/d/Y',strtotime($active_quotes->created_at)) }}
                                                @endif
                                            </td>
                                            <td class="col-md-2" id="td_button{{$active_quotes->id}}">
                                                @if($active_quotes->order_type!='1')
                                                    <a href="{{ url('quote/edit/'.$active_quotes->id.'/active_quotes') }}" class="button" name="view{{$active_quotes->id}}" id="view{{$active_quotes->id}}">View Quote</a>
                                                @else
                                                    @if(!empty($active_quotes->shipping_id)  || empty($active_quotes->shipping_method_id))
                                                        <a href="{{ url('quote/edit/'.$active_quotes->id.'/active_quotes') }}" class="button col-md-12" name="view{{$active_quotes->id}}" id="view{{$active_quotes->id}}">View Quote</a>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            @if(count($expire_quote) > 0)
                            <table class="table" id="expire_quote">
                                <thead>
                                <tr>
                                    <th class="col-md-6">Expired Quote Requests</th>
                                    <th class="col-md-2"></th>
                                    <th class="col-md-2"></th>
                                    <th class="col-md-2"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($expire_quote))
                                    @foreach($expire_quote as $expire_quotes)
                                        <tr>
                                            <td class="col-md-6">{{ $expire_quotes->product_name }}</td>
                                            <td class="col-md-2">
                                                {{$expire_quotes->company_name}}
                                            </td>
                                            <td class="col-md-2">EXPIRED ON {{ date('m/d/Y',strtotime($expire_quotes->expire_date)) }}</td>
                                            <td class="col-md-2">
                                                <a class="button col-md-12" href="{{ url('/quote/create?id='.$expire_quotes->id) }}">REQUEST AGAIN</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            @endif
                            @if(count($past_quote) > 0)
                            <table class="table" id="past_quote">
                                <thead>
                                <tr>
                                    <th class="col-md-6">Past Quotes</th>
                                    <th class="col-md-2"></th>
                                    <th class="col-md-2"></th>
                                    <th class="col-md-2"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($past_quote))
                                    @foreach($past_quote as $past_quotes)
                                        <tr id="tr{{$past_quotes->id}}">
                                            <td class="col-md-6">{{ $past_quotes->product_name }}</td>
                                            <td class="col-md-2">{{$past_quotes->company_name}}</td>
                                            <td class="col-md-2"> {{ date('m/d/Y',strtotime($past_quotes->created_at)) }}</td>
                                            <td class="col-md-2" id="td_button{{$past_quotes->id}}">
                                                <a href="{{ url('quote/edit/'.$past_quotes->id.'/past_quotes') }}" class="button col-md-12" name="view{{$past_quotes->id}}" id="view{{$past_quotes->id}}" onclick="getdetails('{{$past_quotes->id}}')">View Quote</a>
                                                {{--$past_quotes->shipping_name--}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            @endif
                        @elseif($user_role_id=='8')
                            <table id="quote" class="table" >
                                <thead>
                                <tr>
                                    <th class="col-md-6">Product</th>
                                    <th class="col-md-3">Company</th>
                                    <th class="col-md-3">View Quote</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($quote)>0)
                                    @foreach($quote as $quotes)
                                        <tr>
                                            <td class="col-md-6">
                                                {{ $quotes->product_name }}
                                            </td>
                                            <td class="col-md-3">
                                                {{$quotes->company_name}}
                                            </td>
                                            <td class="col-md-3">
                                                    <a href="{{ url('quote/'.$quotes->order_id) }}" class="button col-md-12" name="">View Quote</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            $.fn.dataTableExt.oApi.fnFilterAll = function (oSettings, sInput, iColumn, bRegex, bSmart) {
                var settings = $.fn.dataTableSettings;

                for (var i = 0; i < settings.length; i++) {
                    settings[i].oInstance.fnFilter(sInput, iColumn, bRegex, bSmart);
                }
            };
            var quote_table =$('#quote').DataTable({
                "iDisplayLength": 5,
                "bLengthChange": false,
                "bInfo": false,
                columnDefs: [ { orderable: false, targets: [-1] } ]
            });
            var Table0 = $("#quote").dataTable();
            $("#search_all").keyup(function () {
                Table0.fnFilterAll(this.value);
            });
            var expirequote_table=$('#expire_quote').DataTable({
                "iDisplayLength": 5,
                "bLengthChange": false,
                "bInfo": false,
                columnDefs: [ { orderable: false, targets: [-1] } ]
            });
            var Table1 = $("#expire_quote").dataTable();
            $("#search_all").keyup(function () {
                Table1.fnFilterAll(this.value);
            });
            var pastquote_table=$('#past_quote').DataTable({
                "iDisplayLength": 5,
                "bLengthChange": false,
                "bInfo": false,
                columnDefs: [ { orderable: false, targets: [-1] } ]
            });
            var Table2 = $("#past_quote").dataTable();
            $("#search_all").keyup(function () {
                Table2.fnFilterAll(this.value);
            });
            $('#quote_filter').hide();
            $('#past_quote_filter').hide();
            $("#expire_quote_filter").hide();
            quote_table.columns().iterator( 'column', function (ctx, idx) {
                $( quote_table.column(idx).header()).append('<span class="sort-icon"/>');
            });
            expirequote_table.columns().iterator( 'column', function (ctx, idx) {
                $( expirequote_table.column(idx).header()).append('<span class="sort-icon"/>');
            });
            pastquote_table.columns().iterator( 'column', function (ctx, idx) {
                $( pastquote_table.column(idx).header()).append('<span class="sort-icon"/>');
            });
            $('.dataTables_filter').addClass('pull-right');
        });
    </script>
@endsection