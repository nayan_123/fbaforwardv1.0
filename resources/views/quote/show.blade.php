@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    {!! Html::style('assets/dist/css/custom_quotes.css') !!}
    <style>
        .col-md-12
        {
            padding-left: 35px;
        }
        .shead
        {
            padding-right: 1px;
        }
        .product-data{
            border:1px solid #c8c8c8;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <h1>{{$title}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('quote') }}"><i class="fa fa-shopping-cart"></i> Quotes</a></li>
            <li class="active">{{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{$title}}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12"><h4>Details</h4></div>
                    <!-- <div class="col-md-12">
                        <div class="col-md-2"><b>Number of Units</b></div><div class="col-md-2">{{$quote->total}}</div>
                        <div class="col-md-2"><b>Number of Cartons</b></div><div class="col-md-2">{{$quote->no_boxs}}</div>
                        <div class="col-md-2"><b>Shipping Method</b></div><div class="col-md-2">{{$quote->shipping_name}}</div>
                        <div class="col-md-2"><b>Shipment Type</b></div><div class="col-md-2">{{$quote->shipment_type_name}}</div>
                        <div class="col-md-2"><b>Shipment Size Weight</b></div><div class="col-md-2">{{$quote->shipment_weight}}</div>
                        <div class="col-md-2"><b>Shipment Size Volume</b></div><div class="col-md-2">{{$quote->shipment_volume}}</div>
                        @if($quote->order_type=='1')
                            <div class="col-md-2"><b>Shipping and Customs </b></div><div class="col-md-2">$299</div>
                        @elseif($quote->order_type=='3')
                            <div class="col-md-2"><b>Customs </b></div><div class="col-md-2">$199</div>
                        @endif
                    </div> -->

                    <div class="col-md-12 ">
                            <div class="col-md-4 no-padding">
                                <div class="col-md-12 product-data product-name">
                                    <b>Number of Units : </b><span>{{$quote->total}}</span>
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="col-md-12 product-data">
                                    <b>Number of Cartons : </b><span>{{$quote->no_boxs}}</span>
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="col-md-12 product-data">
                                    <b>Shipping Method : </b><span>{{$quote->shipping_name}}</span>
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="col-md-12 product-data">
                                    <b>Shipment Type : </b><span>{{$quote->shipment_type_name}}</span>
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="col-md-12 product-data">
                                    <b>Shipment Size Weight : </b><span>{{$quote->shipment_weight}}</span>
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="col-md-12 product-data">
                                    <b>Shipment Size Volume : </b><span>{{$quote->shipment_volume}}</span>
                                    @if($quote->order_type=='1')
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="col-md-12 product-data">
                                        <b>Shipping and Customs </b><span>$299</span>
                                    @elseif($quote->order_type=='3')
                                        <b>Customs </b><span>$199</span>
                                    @endif
                                </div>
                            </div>

                        </div>
                    @if(count($prep_details)>0)
                        <div class="col-md-12 margintop20"><h4>Warehouse Services</h4></div>
                        <div class="col-md-12">
                           <table class="table">
                               <thead>
                                   <tr>
                                       <th>Warehouse Services</th>
                                       <th>Rate</th>
                                       <th>Qty</th>
                                       <th class="text-center">Price</th>
                                   </tr>
                               </thead>
                               <tbody>
                               @foreach($prep_details as $prep_detail)
                                    <tr>
                                        <td>{{ $prep_detail->service_name }}</td>
                                        <td>${{ $prep_detail->price }} </td>
                                        <td>{{ $quote->total }}</td>
                                        <td class="text-center">${{ $prep_detail->price * $quote->total }}</td>
                                    </tr>
                               @endforeach
                               </tbody>
                           </table>
                        </div>
                    @endif
                    <div class="col-md-12"><h4>Receiving and Forwarding Rates</h4></div>
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Receiving and Forwarding Rates</th>
                                <th class="text-center">Rate</th>
                                <th class="text-center">Qty</th>
                                <th class="text-center">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($quote->shipment_size_type >'1')
                                <tr>
                                    <td>Container Unloading</td>
                                    <td class="text-center">${{$quote->container_unload_rate}}</td>
                                    <td class="text-center">1</td>
                                    <td class="text-center">${{$quote->container_unload_rate}}</td>
                                </tr>
                            @endif
                                <tr>
                                    <td>Receiving & Forward</td>
                                    <td class="text-center">${{$quote->receive_forward}}</td>
                                    <td class="text-center">{{$quote->no_boxs}}</td>
                                    <td class="text-center">${{$quote->no_boxs * $quote->receive_forward}}</td>
                                </tr>
                            @if($quote->outbound_method_id >1)
                                {{--*/ $palletizing= env('PALLETIZING') /*--}}
                                {{--*/ $amazon_approve_pallet= env('AMAZON_APPROVE_PALLET') /*--}}
                                {{--*/ $pallet_charge= env('PALLET_CHARGE') /*--}}
                                @if($quote->units==1)
                                    {{--*/$trucking_qty=round($quote->shipment_volume / env('PALLET_CBM')) /*--}}
                                @else
                                    {{--*/$trucking_qty=round($quote->shipment_volume / env('PALLET_CFT'))/*--}}
                                @endif
                                <tr>
                                    <td class="col-md-6">{{$palletizing}}</td>
                                    <td class="col-md-2">${{$pallet_charge}}</td>
                                    <td class="col-md-2">{{$trucking_qty}}</td>
                                    <td class="col-md-2">${{$pallet_charge*$trucking_qty}}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-6">{{$amazon_approve_pallet}}</td>
                                    <td class="col-md-2">${{$pallet_charge}}</td>
                                    <td class="col-md-2">{{$trucking_qty}}</td>
                                    <td class="col-md-2">${{$pallet_charge*$trucking_qty}}</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12"><h4>Outbound Shipping Method</h4></div>
                    <div class="col-md-12"><b>{{$quote->outbound_name}}</b></div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
<script type="text/javascript">
    function getedit(id)
    {
        $("#span"+id).hide();
        $("#price"+id).show();

    }
    function getprice(id) {
        var newprice= $("#price"+id).val();
        $("#span"+id).text(newprice);
    }
</script>
@endsection