<html>
<head>

</head>
<body>
<style>
.heading{
    color: #7d7d7d;
    font-weight: 600;
    font-size: 30px;
}
.grey-font{
    color: #7d7d7d;
    font-weight: 600;
}
.border-box{ border:1px solid #e0dddd; }
table>thead>tr>th{ background-color:#0088cc;color:#fff;text-transform: uppercase; }
table>tbody{ border:1px solid #e0dddd;border-top: none; }
table>tbody>tr>td{ border-right:1px solid #e0dddd; }
.border-box{ border:1px solid #e0dddd; }
.margin-top-30{ margin-top: 30px; }
.contact-info{ color:#0088cc; }
.border-top-1{ border-top:1px solid #e0dddd; }
.margin-top-10{ margin-top: 10px; }
.margin-bottom-0{ margin-bottom: 0px; }
.padding-10{ padding: 10px; }
.border-1{ border:1px solid #e0dddd;border-top: none; }
.font-size-16{ font-size: 16px; }
.text-center{ text-align: center; }
@media (min-width: 992px){
    .col-md-12 {
        width: 100%;
    }
    .col-md-offset-3 {
        margin-left: 25%;
    }
    .col-md-3 {
        width: 25%;
    }
}
.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
    /*float: left;*/
}
.col-md-8 {
    width: 66.66666667%;
}
.col-md-6 {
    width: 50%;
}
.col-md-3 {
    width: 25%;
}
.col-md-9 {
    width: 75%;
}
.pull-right{
    float: right;
}
.col-md-5 {
    width: 41.66666667%;
}
.col-md-7 {
    width: 58.33333333%;
}
.logo-font p,.font-size-13 { font-size: 13px; }
.no-border tbody{ border: none !important; }
table {
    background-color: transparent;
    border-spacing: 0;
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 30px;    
}
table>thead>tr>th,table>tbody>tr>td{
    padding: 8px;
    text-align: center;
}
.no-border{ border: none !important; }
.margin-top-60{ margin-top:60px; }
.page-break{
    page-break-after: always;
}
.text-left{ text-align: left; }
</style>
{{--*/ $charge_qty = 1 /*--}}
{{--*/ $shipping_sub_total = 0 /*--}}
{{--*/$custom_sub_total=0;/*--}}
{{--*/$receive_forward_sub_total=0;/*--}}
{{--*/ $prep_price = "" /*--}}
{{--*/ $prep_sub_total = 0 /*--}}
{{--*/ $trucking_qty="" /*--}}
{{--*/ $port_fee = 0 /*--}}
{{--*/ $port_fee_name =  env('PORT_FEE_NAME') /*--}}
{{--*/ $trucking_name =  env('TRUCKING_NAME') /*--}}
{{--*/ $isf_rate =  env('ISF_RATE') /*--}}
{{--*/ $isf_name =  env('ISF_NAME') /*--}}
{{--*/ $entry_rate = env('ENTRY_RATE') /*--}}
{{--*/ $entry_name = env('ENTRY_NAME') /*--}}
{{--*/ $bond_rate =  env('BOND_RATE') /*--}}
{{--*/ $bond_name =  env('BOND_NAME') /*--}}
{{--*/ $container_unloading= env('CONTAINER_UNLOADING') /*--}}
{{--*/ $palletizing= env('PALLETIZING') /*--}}
{{--*/ $amazon_approve_pallet= env('AMAZON_APPROVE_PALLET') /*--}}
{{--*/ $pallet_charge= env('pallet_charge') /*--}}
@if($details->units==1)
    {{--*/$trucking_qty=round($details->shipment_volume / env('PALLET_CBM')) /*--}}
@else
    {{--*/$trucking_qty=round($details->shipment_volume / env('PALLET_CFT'))/*--}}
@endif
@if($trucking_qty > env('TRUCKING_QTY'))
    {{--*/ $trucking_price = ($trucking_qty -  env('TRUCKING_QTY')) * env('TRUCKING_ADDITIONAL_PRICE') + env('TRUCKING_PRICE')/*--}}
@else
    {{--*/ $trucking_price =  env('TRUCKING_PRICE') /*--}}
@endif
<div class="container">
    <div class="row " id="quote_print">
            <div class="page-break"></div>
            <h1 class="heading text-center">FEE BREAKDOWN</h1>
        <div class="col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="col-md-6 no-border">Line Item</th>
                            <th class="col-md-3 no-border">Rate</th>
                            <th class="col-md-3 no-border">Quantity</th>
                            <th class="col-md-3 no-border">Total</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px solid #e0dddd;border-top: none;">
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$details->shipping_name}}</td>
                            <td class="col-md-2 no-border">${{$details->shipment_type_rate}}</td>
                            <td class="col-md-2 no-border">{{$details->shipment_volume}}</td>
                            <td class="col-md-2 no-border">${{$details->shipment_type_rate*$details->shipment_volume}}</td>
                        </tr>
                        
                        @foreach($detail_charge as  $charge_key=>$charge_value )
                            <tr>
                                <td class="col-md-6 no-border text-left">{{$charge_value->name}}</td>
                                <td class="col-md-2 no-border">${{$charge_value->charge}}</td>
                                <td class="col-md-2 no-border">{{$charge_qty}}</td>
                                <td class="col-md-2 no-border">${{$charge_value->charge * $charge_qty}}</td>
                            </tr>
                            
                        @endforeach
                        @if($details->shipping_name!=="undefined")
                            @if($details->shipping_method_id == 2)
                                {{--*/  $port_fee= env('PORT_FEE')/*--}}
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$port_fee_name}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee}}</td>
                                    <td class="col-md-2 no-border">{{$charge_qty}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee*$charge_qty}}</td>
                                </tr>

                                <tr>
                                    <td class="col-md-6 no-border text-left"> {{$trucking_name}}</td>
                                    <td class="col-md-2 no-border">$99/first 3 pallets, $25/additional pallets</td>
                                    <td class="col-md-2 no-border">{{$trucking_qty}}</td>
                                    <td class="col-md-2 no-border">${{$trucking_price}}</td>
                                </tr>
                            @elseif($details->shipping_method_id == 1)
                                @if($details->shipment_type_id==1)
                                    {{--*/ $port_fee= env('OCEAN_LCL_PORT_FEE') /*--}}
                                @else
                                    {{--*/ $port_fee= env('OCEAN_PORT_FEE') /*--}}
                                @endif
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$port_fee_name}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee}}</td>
                                    <td class="col-md-2 no-border">{{$charge_qty}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee*$charge_qty}}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$trucking_name}}</td>
                                    <td class="col-md-2 no-border">${{$details->trucking_rate}}/pallet</td>
                                    <td class="col-md-2 no-border">{{$trucking_qty}}</td>
                                    <td class="col-md-2 no-border">${{$trucking_price = ($details->trucking_rate*$trucking_qty) }}</td>
                                </tr>
                            @else
                                @if($details->shipment_type_id==1)
                                    {{--*/ $port_fee= env('OCEAN_LCL_PORT_FEE') /*--}}
                                @else
                                    {{--*/ $port_fee= env('OCEAN_PORT_FEE') /*--}}
                                @endif
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$port_fee_name}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee}}</td>
                                    <td class="col-md-2 no-border">{{$charge_qty}}</td>
                                    <td class="col-md-2 no-border">${{$port_fee*$charge_qty}}</td>
                                </tr>
                                {{--*/ $trucking_price = 0; /*--}}
                            @endif
                            
                        @endif
                        <tr class="border-top-1">
                            <td class="col-md-6 no-border text-left">SUB TOTAL</td>
                            <td class="col-md-2 no-border">-</td>
                            <td class="col-md-2 no-border">-</td>
                            <td class="col-md-2 no-border"><b>${{$sum_details_charge}}</b></td>
                        </tr>
                        </tbody>
                </table>

                <h4 class="margin-top-60">U.S. CUSTOMS BROKERAGE</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="col-md-6 no-border">Line Item</th>
                            <th class="col-md-3 no-border">Rate</th>
                            <th class="col-md-3 no-border">Quantity</th>
                            <th class="col-md-3 no-border">Amount</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px solid #e0dddd;border-top: none;">
                       @if($details->shipping_method_id == 1)
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$isf_name}}</td>
                            <td class="col-md-2 no-border">${{$isf_rate}}</td>
                            <td class="col-md-2 no-border">{{$charge_qty}}</td>
                            <td class="col-md-2 no-border">${{$isf_rate * $charge_qty}}</td>
                        </tr>
                       
                        @endif
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$entry_name}}</td>
                            <td class="col-md-2 no-border">${{$entry_rate}}</td>
                            <td class="col-md-2 no-border">{{$charge_qty}}</td>
                            <td class="col-md-2 no-border">${{ $entry_rate*$charge_qty}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$bond_name}}</td>
                            <td class="col-md-2 no-border">${{$bond_rate}}</td>
                            <td class="col-md-2 no-border">{{$charge_qty}}</td>
                            <td class="col-md-2 no-border">${{$bond_rate*$charge_qty}}</td>
                        
                        </tr>
                        <tr class="border-top-1">
                            <td class="col-md-6 no-border text-left">SUB TOTAL</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border"><b>${{ $sum_custom_total }}</b></td>
                        </tr>
                    </tbody>
                </table>

                <h4 class="margin-top-60">RECEIVING & FORWARDING</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="col-md-6 no-border">Line Item</th>
                            <th class="col-md-3 no-border">Rate</th>
                            <th class="col-md-3 no-border">Quantity</th>
                            <th class="col-md-3 no-border">Amount</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px solid #e0dddd;border-top: none;">
                        @if($details->shipment_type_id >1)
                        <tr>
                            <td class="col-md-6 no-border text-left">Container Unloading</td>
                            <td class="col-md-3 no-border">${{$details->container_unload_rate}}</td>
                            <td class="col-md-3 no-border">{{$charge_qty}}</td>
                            <td class="col-md-3 no-border">${{$details->container_unload_rate*$charge_qty}}</td>
                        </tr>
                        @endif
                       
                        @if($details->outbound_method_id >1)
                            <tr>
                                <td class="col-md-6 no-border text-left">{{$palletizing}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge}}</td>
                                <td class="col-md-3 no-border">{{$trucking_qty}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge*$trucking_qty}}</td>
                            </tr>
                            
                            <tr>
                                <td class="col-md-6 no-border text-left">{{$amazon_approve_pallet}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge}}</td>
                                <td class="col-md-3 no-border">{{$trucking_qty}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge*$trucking_qty}}</td>
                            </tr>
                           
                        @endif
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$details->outbound_name}}</td>
                            <td class="col-md-3 no-border">&nbsp;</td>
                            <td class="col-md-3 no-border">&nbsp;</td>
                            <td class="col-md-3 no-border"><i>varies</i></td>
                        </tr>
                        <tr class="border-top-1">
                            <td class="col-md-6 no-border text-left">SUB TOTAL</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border"><b>${{ $sum_receive_forward_total }}</b></td>
                        </tr>
                    </tbody>
                </table>

                <!-- <div class="page-break"></div> -->
                <h4 class="margin-top-60">PREP & INSPECTION</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="col-md-6 no-border">Line Item</th>
                            <th class="col-md-3 no-border">Rate</th>
                            <th class="col-md-3 no-border">Quantity</th>
                            <th class="col-md-3 no-border">Amount</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px solid #e0dddd;border-top: none;">
                        @foreach($detail_prep as $prep_key=>$prep_value )
                            {{--*/$prep_price = $prep_value->price * $details->total/*--}}
                            @if($prep_price % 1 !=0)
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$prep_value->service_name}}</td>
                                    <td class="col-md-3 no-border">${{$prep_value->price}}</td>
                                    <td class="col-md-3 no-border">{{$details->total}}</td>
                                    <td class="col-md-3 no-border">${{$prep_price}}</td>
                                    
                                </tr>
                            @else
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$prep_value->service_name}}</td>
                                    <td class="col-md-3 no-border">${{$prep_value->price}}</td>
                                    <td class="col-md-3 no-border">{{$details->total}}</td>
                                    <td class="col-md-3 no-border">${{$prep_price}}</td>
                                   
                                </tr>
                            @endif
                        @endforeach
                        <tr class="border-top-1">
                            <td class="col-md-6 no-border text-left">TOTAL</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border"><b>${{ $sum_prep_total }}</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
     
    </div>
</div>
@section('js')
    <script type="text/javascript">
        
        $(document).ready(function() {
            var prtContent = document.getElementById("quote_print");
            var WinPrint = window.open('', '', 'left=0,top=0,width=500,height=200,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        });
        
    </script>
@endsection
</body>
</html>