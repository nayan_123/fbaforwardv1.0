
<table id="quote" class="table" >
    <thead>

    </thead>
    <tbody>
    @foreach($details as $detail )
        <tr>
            <th>Product</th>
            <td>{{ $detail->product_name }}</td>
        </tr>
        <tr>
            <th>Cartons</th>
            <td>{{$detail->no_boxs}}</td>
        </tr>
        <tr>
            <th>Weight</th>
            <td>{{ $detail->chargeable_weight }}KG</td>
        </tr>
        <tr>
            <th>Shipping Method </th>
            <td>{{ $detail->shipping_name }}</td>
        </tr>
        <tr>
            <th>Incoterms </th>
            <td>
                @if($detail->incoterms=='1')
                    {{ "FOB" }}
                @else
                    {{ "EXW" }}
                @endif
            </td>
        </tr>
        <tr>
            <th>Approx. Goods Ready Date </th>
            <td>{{ $detail->goods_ready_date }}</td>
        </tr>
        <tr>
            <th>Units</th>
            <td>{{ $detail->qty_per_box}}</td>
        </tr>
        <tr>
            <th>Volume</th>
            <td>{{ $detail->shipment_volume }} </td>
        </tr>
        <tr>
            <th>Shipment Type </th>
            <td>
                @if($detail->shipment_size_type=='1')
                    {{"LCL" }}
                @elseif($detail->shipment_size_type=='2')
                    {{ "20' (FCL)" }}
                @elseif($detail->shipment_size_type=='3')
                    {{ "40'+ (FCL)" }}
                @else
                        {{ "Not Sure" }}
                @endif
            </td>
        </tr>
        <tr>
            <th>Port of Origin </th>
            <td>{{$detail->port_of_origin}}</td>
        </tr>
        <tr>
            <th>Quote Expiration Date </th>
            <td> {{ $detail->expire_date }}</td>
        </tr>
    @endforeach
    </tbody>
</table>