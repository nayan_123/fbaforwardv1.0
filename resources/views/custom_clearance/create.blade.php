@extends('layouts.member.app')
@section('title', 'Customs Clearance')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
@endsection
@section('content')
{!! Html::script('assets/bootstrap/js/datePicker.js') !!}
{!! Html::script('assets/plugins/jQuery/jQuery-2.1.4.min.js') !!}
<style>
.item-title span, input { margin: 10px 0px !important; }
</style>
    <section class="content-header">
        <h1><i class="fa fa-shopping-cart"></i> Customs Clearance</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-shopping-cart"></i> Customs Clearance</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form method="post" class="form col-md-12" id="validate" action="{{ url('customs-clearance/create') }}" enctype="multipart/form-data">
                	{{ csrf_field() }}
                	<input type="hidden" name="order_id" value="{{ $order_id }}">
                	<div class="col-md-12">
	                	<div class="form-group">
	                		<div class="col-md-12">
	                			<label>3461: </label>
	                		</div>
	                		<div class="col-md-12">
	                			<input type="file" class="form-control  validate[checkFileType[jpg|jpeg|gif|JPG|png|PNG|docx|doc|pdf|pdfx]] validate[required]" name="file_3461" >
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-12">
	                	<div class="form-group">
	                		<div class="col-md-12">
	                			<label>Customs Duties: </label>
	                		</div>
	                		<div class="col-md-12">
	                			<input type="text" class="form-control validate[required] custom[integer]" name="custom_duties" >
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-12">
	                	<div class="form-group">
	                		<div class="col-md-2">
	                			<label>Additional Services: </label>
	                		</div>
	                		<div class="col-md-10">
	                			<div class="col-md-12">
	                				<input type="checkbox" class="validate[minCheckbox[1]]" name="additional_service[]" value="FDA_clearance">FDA Clearance
	                			</div>
	                			<div class="col-md-12">
	                				<input type="checkbox" class="validate[minCheckbox[1]]" name="additional_service[]" value="lacey_act">Lacey Act
	                			</div>
	                			<div class="col-md-12">
	                				<input type="checkbox" class="validate[minCheckbox[1]]" name="additional_service[]" value="OGA">OGA/PGA
	                			</div>
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-12 width94">
	                	<div class="form-group">
	                		<button class="btn btn-primary pull-right" type="submit">Submit</button>
	                	</div>
	                </div>
                </form>
            </div>
        </div>
    </section>
    @endsection
@section('js')
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
{!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
{!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
<script type="text/javascript">
    $(document).ready(function () {
        var prefix = 's2id_';
        $("form[id^='validate']").validationEngine('attach', {
                promptPosition: "bottomRight", scroll: false,
                prettySelect: true,
                usePrefix: prefix
            });
    });
 	$(document).ready(function () {
        $('.datepicker').datepicker({
            startDate: new Date(),
        });
    });
</script>
@endsection