@extends('layouts.member.app')
@section('title', 'Customs Clearance')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-shopping-cart"></i> Customs Clearance</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-shopping-cart"></i> Customs Clearance</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <table id="delivery" class="table" >
                    <thead>
                    <tr>
                        <th class="col-md-2">Order Number</th>
                        <th class="col-md-4">Product</th>
                        <th class="col-md-2">Company</th>
                        <th class="col-md-2">Date Ordered</th>
                        <th class="col-md-2">Submit Customs</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td class="col-md-2">{{ $order->order_no }}</td>
                                <td class="col-md-4">{{ $order->product_name }}</td>
                                <td class="col-md-2">{{ $order->company_name }}</td>
                                <td class="col-md-2">{{ \Carbon\Carbon::parse($order->created_at)->format('Y-m-d') }}</td>
                                <td class="col-md-2"><a class="button" href="{{ url('/customs-clearance/create?id='.$order->order_id) }}">Submit Customs</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @endsection
@section('js')
    <script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('#delivery').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
        $('#incoming').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
        $('#non_truckey').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
        $('#schedule').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
    });
</script>
@endsection