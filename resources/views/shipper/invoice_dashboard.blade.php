@extends('layouts.member.app')
@section('title', 'Invoicing')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
<section class="content">
    <section class="content-header">
        <h1><i class="fa fa-bold"></i> Invoicing</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-bold"></i>Invoicing</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive no-padding">
                            <table id="inprogress_data" class="table" >
                                <thead>
	                                <tr>
	                                    <th class="col-md-1">Order No.</th>
	                                    <th class="col-md-4">Product</th>
	                                    <th class="col-md-3">Company</th>
	                                    <th class="col-md-2">Date Ordered</th>
	                                    <th class="col-md-2">Action</th>
	                                    <th></th>
	                                </tr>
                                </thead>
                                <tbody>
                                	@foreach($orders as $order)
	                                	<tr>
		                                    <td class="col-md-1">#{{ $order->order_no }}</td>
		                                    <td class="col-md-4">{{ $order->product_name }}</td>
		                                    <td class="col-md-3">{{ $order->company_name }}</td>
		                                    <td class="col-md-2">{{ \Carbon\Carbon::parse($order->created_at)->format('m-d-Y') }}</td>
		                                    <td class="col-md-2"><a href="{{ url('shipper/view_invoice',$order->order_id) }}"><button class="button">Upload</button></td>
		                                </tr>
	                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection

@section('js')
    <script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {


    });
</script>
@endsection