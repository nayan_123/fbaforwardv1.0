@extends('layouts.member.app')
@section('title', 'View Shipment')
@section('css')
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
    {!! Html::style('assets/dist/css/style.css') !!}
    {!! Html::style('assets/dist/css/custom_quotes.css') !!}
@endsection
@section('content')
<section class="content-header">
    <h1>
        <i class="fa fa-bold"></i> View Invoice
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-bold"></i>View Invoice</li>
    </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box border-top-0">
				<div class="box-body">
					<form id="validate" class="form-horizontal" method="post" action="{{ url('shipper/update-invoice') }}">
						{{ csrf_field() }}

						<input type="hidden" name="order_id" value="{{ $order->order_id }}">
						<input type="hidden" name="customer_requirement_id" value="{{ $order->customer_requirement_id }}">
						<input type="hidden" name="old_units" value="{{ $order->total }}">
						<input type="hidden" name="old_cartons" value="{{ $order->no_boxs }}">
						<input type="hidden" name="old_cbm" value="{{ $order->shipment_volume }}">
						<input type="hidden" name="old_weight" value="{{ $order->shipment_weight }}">
						<input type="hidden" name="shipping_method_id" value="{{ $order->shipping_method_id }}">
						<input type="hidden" name="qty_per_box" value="{{ $order->qty_per_box }}">

						@if($order->chargable_weight)
						<input type="text" name="old_chargable_weight" value="{{ $order->chargable_weight }}">
						@endif
				    	<div class="row ">
				    		<div class="col-md-12 padding-0-7">
				    			<div class="shipment-box">
				    				<h3>Invoice</h3>
				    			</div>
				    		</div>
				    		<div class="col-md-12">
					    		<div class="col-md-6">
				            		<span class="col-md-3 no-padding"><b>Order</b></span>
				            		<span class="sdetail col-md-9">#{{ $order->order_id }}</span>
				            	</div>

				            	<div class="col-md-6">
				            		<span class="col-md-3 no-padding"><b>Product</b></span>
				            		<span class="sdetail col-md-9 text-overflow">{{ $order->product_name }}</span>
				            	</div>

				            	<div class="col-md-6">
				            		<b><span class=" col-md-3 no-padding">Company</span></b>
				            		<span class="sdetail col-md-9">{{ $order->company_name }}</span>
				            	</div>

				            	<div class="col-md-6">
				            		<b><span class=" col-md-3 no-padding">Date Ordered</span></b>
				            		<span class="sdetail col-md-9">{{ \Carbon\Carbon::parse($order->created_at)->format('m-d-Y') }}</span>
				            	</div>

				            	<div class="row">
					            	<div class="col-md-6">
					            		<b><span class="col-md-3">Units</span></b>
					            		<span class="sdetail col-md-9 padding-0-25">{{ $order->total }}
					            		<input type="text" name="units" class="form-control validate[required] hide" id="units" style="margin: 0%;"></span>
					            	</div>

					            	<div class="col-md-6">
					            		<b><span class=" col-md-3 no-padding">Cartons</span></b>
					            		<span class="sdetail col-md-9">{{ $order->no_boxs }}
					            			<input type="text" name="cartons" class="form-control validate[required] hide" id="cartons" style="margin: 0%;"><i class="fa fa-edit" id="edit_cartons"></i></span>
					            	</div>
					            </div>

					            <div class="row">
					            	<div class="col-md-6">
					            		<b><span class=" col-md-3">CBM</span></b>
					            		<span class="sdetail col-md-9 padding-0-25">{{ $order->shipment_volume }}
						            		<input type="text" name="cbm" class="form-control validate[required] hide" id="cbm" style="margin: 0%;"><i class="fa fa-edit" id="edit_cbm"></i></span>
					            	</div>

					            	<div class="col-md-6">
					            		<b><span class=" col-md-3 no-padding">Weight</span></b>
					            		<span class="sdetail col-md-9">{{ $order->shipment_weight }}
					            			<input type="text" name="weight" class="form-control validate[required] hide" id="weight" style="margin: 0%;"><i class="fa fa-edit" id="edit_weight"></i></span>
					            	</div>
					            </div>

					            @if($order->chargeable_weight)
				            	<div class="col-md-6">
				            		<b><span class=" col-md-3 no-padding">Chargable Weight</span></b>
				            		<span class="sdetail col-md-9">{{ $order->chargeable_weight }}
				            			<input type="text" name="chargeable_weight" class="form-control validate[required] hide" id="chargeable_weight" style="margin: 0%;"><i class="fa fa-edit" id="edit_chargable_weight"></i></span>
				            	</div>
				            	@endif
				           	</div>
				        </div>

				        <!-- <div class="col-md-12">
                            <label for="charges" class="control-label col-md-12" style="text-align:left">ADDITIONAL CHARGES</label>
                        </div>
                        <div>
                            <div class="input-group col-md-12">
                                <table class="table">
                                	<thead>
                                		<tr>
                                			<th>Shipping</th>
											<th>Rate</th>
											<th>Qty</th>
											<th>Price</th>
										</tr>
                                	</thead>
                                	<tbody>
                                		@foreach($additional_charges as $additional_charge)
	                                		{{-- */ 
	                                			$charge_qty = 1;
	                                			$shipping_sub_total = 0;
	                                			$charge_price = $additional_charge->charge * $charge_qty;
	                                        	$shipping_sub_total += $charge_price /* --}}
	                                		<tr>
	                                			<td>{{ $additional_charge->name }}</td>
	                                			<td>{{ $additional_charge->charge }}</td>
	                                			<td>{{ $charge_qty }}</td>
	                                			<td>{{ $charge_price }}</td>
	                                		</tr>
                                		@endforeach
                                	</tbody>
                                </table>
                            </div>
                        </div> -->

			            <div class="col-md-12 margin-top-20">
			            	<button class="button pull-right" type="submit">Submit</button>
			            </div>
			        </form>
				</div>
			</div>

			{{--*/ $additional_charge_sub_total = '' /*--}}
			<div class="col-md-12 no-padding">
				<div class="box2 col-md-12 no-padding">
					<div class="box-header with-border quote-summery-heading no-border">
						<h3 class="box-title">Invoice</h3>
					</div>

					<div class="box-body no-padding">
						<div class="col-md-12 no-padding">

							<table class="table quote-summery-table">
								<thead>
									<tr>
										<th class="no-border col-md-10">{{ $order->shipping_name }} @if($order->shipping_method_id==1 && $order->shipment_type_id=='1' )
                                                {{ $order->shipping_name }} (FEE (PER CBM))
                                            @elseif($order->shipping_method_id==1)
                                                (FCL Fee)
                                            @else
                                                (FEE)
                                            @endif
                                        </th>
										<td class="col-md-2">@if($shipment_fee){{ $shipment_fee->freight_fee }}@endif</td>
									</tr>
								</thead>
							</table>

							<table class="table quote-summery-table">
								<thead>
									<tr>
										<th class="col-md-6">Additional Charges</th>
										<th class="col-md-2">Rate</th>
										<th class="col-md-2">Qty</th>
										<th class="col-md-2">Total</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach($additional_charges as $additional_charge)
                                		{{-- */ 
                                			$charge_qty = 1;
                                			$shipping_sub_total = 0;
                                			$charge_price = $additional_charge->charge * $charge_qty;
                                        	$additional_charge_sub_total += $charge_price /* --}}
                                		<tr>
                                			<td class="col-md-6">{{ $additional_charge->name }}</td>
                                			<td class="col-md-2">{{ $additional_charge->charge }}</td>
                                			<td class="col-md-2">{{ $charge_qty }}</td>
                                			<td class="col-md-2">{{ $charge_price }}</td>
                                		</tr>
                            		@endforeach
								</tbody>
							</table>

							<table class="table quote-summery-table">
								<thead>
									<tr>
										<th class="no-border col-md-10">Total</th>
										<td class="col-md-2">@if($shipment_fee){{ $additional_charge_sub_total + $shipment_fee->freight_fee }} @else {{ $additional_charge_sub_total }}@endif</td>
									</tr>
								</thead>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('js')
<script>
	$(document).ready(function () {

		var prefix = 's2id_';
        $("form[id^='validate']").validationEngine('attach',
            {
                promptPosition: "bottomRight", scroll: false,
                prettySelect: true,
                usePrefix: prefix
            });

        $('.datepicker').datepicker({
            startDate: new Date(),
		});

        $('#edit_units').click(function(){
        	$('#units').removeClass('hide');
        	$('#edit_units').hide();
        });

        $('#edit_cartons').click(function(){
        	$('#cartons').removeClass('hide');
        	$('#edit_cartons').hide();
        });

        $('#edit_cbm').click(function(){
        	$('#cbm').removeClass('hide');
        	$('#edit_cbm').hide();
        });

        $('#edit_weight').click(function(){
        	$('#weight').removeClass('hide');
        	$('#edit_weight').hide();
        });

        $('#edit_chargeable_weight').click(function(){
        	$('#chargeable_weight').removeClass('hide');
        	$('#edit_chargeable_weight').hide();
        });

	});
</script>
@endsection
