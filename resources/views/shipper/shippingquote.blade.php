@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
<style>
    .col-md-12 {
        padding-left: 35px;
    }
    .col-md-9 {
        width: 69%;
    }
    .col-md-3 {
        width: 31%;
    }
    .shead {
        padding-right: 1px;
    }
    .col-md-2 {
        width: 16%;
    }
    .line-height-2{ line-height: 2; }
    .margin-bottom-30{ margin-bottom: 30px; }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1>
            <!-- <i class="fa fa-anchor"></i> --> Enter New Shipping Quote <!-- {{$title}} -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-anchor"></i>{{$title}}</li>
            <li>Enter New Quote</li>
        </ol>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-body">
                <div class="col-md-12 no-padding">
                    <div class="col-md-2">
                        <label class="control-label">Status :</label>
                    </div>
                    <div class="col-md-10">
                        @if($quote_request[0]->is_activated=='3')
                            Revision Requested
                        @else
                            New
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="box">
            <div class="box-body">
                {!! Form::open(['url' => 'shipper/shippingquoteform', 'method' => 'PUT', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                <input type="hidden" name="is_activated" id="is_activated" value="{{$quote_request[0]->is_activated}}">
                <div class="row">
                <div class="col-md-12 no-padding">

                    <div class="col-md-12">
                        <div class="row">
                            <div style="border-bottom:1px solid #c8c8c8;width: 99%">
                                <span class="heading">PRODUCT DETAILS</span>
                            </div>
                            <div class="col-md-12 no-padding margin-bottom-30">
                                @foreach($quote_request as $quote_requests)
                                    {{--*/ $shipment_type = $quote_requests->shipment_type_short_name; /*--}}
                                    {{--*/$incoterm = $quote_requests->incoterm_short_name; /*--}}
                                    <div class="col-md-12 no-padding">
                                        <div class="col-md-2">
                                            <label class="control-label">Product</label>
                                        </div>
                                        <div class="col-md-10">
                                            @if($quote_requests->product_id=='') {{ $quote_requests->productname }} @else{{ $quote_requests->product_name }}@endif
                                        </div>
                                    </div>

                                    <div class="col-md-12 no-padding">
                                        <div class="col-md-2">
                                            <label class="control-label">Company</label>
                                        </div>
                                        <div class="col-md-10">
                                            @if($quote_requests->company_name=='')&nbsp; @else {{ $quote_requests->company_name }} @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6 no-padding">
                                        <div class="col-md-3">
                                            <label class="control-label">Total Cartons</label>
                                        </div>
                                        <div class="col-md-9 line-height-2">
                                            {{ $quote_requests->no_boxs }}
                                        </div>
                                    </div>

                                    <div class="col-md-6 no-padding">
                                        <div class="col-md-3">
                                            <label class="control-label">Shipment Type</label>
                                        </div>
                                        <div class="col-md-9 line-height-2">
                                           {{ $shipment_type }}
                                        </div>
                                    </div>

                                    <div class="col-md-6 no-padding">
                                        <div class="col-md-3">
                                            <label class="control-label">Total Unit</label>
                                        </div>
                                        <div class="col-md-9 line-height-2">
                                            {{ $quote_requests->qty_per_box }}
                                        </div>
                                    </div>

                                    <div class="col-md-6 no-padding">
                                        <div class="col-md-3">
                                            <label class="control-label">Incoterms</label>
                                        </div>
                                        <div class="col-md-9 line-height-2">
                                           {{ $incoterm }}
                                        </div>
                                    </div>

                                    <div class="col-md-6 no-padding">
                                        <div class="col-md-3">
                                            <label class="control-label">Weight (KG)</label>
                                        </div>
                                        <div class="col-md-9 line-height-2">
                                            @if($quote_requests->units==1) {{ $quote_requests->shipment_weight}} @else {{ $quote_requests->shipment_weight*16.02}} @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6 no-padding">
                                        <div class="col-md-3">
                                            <label class="control-label">Supplier Location</label>
                                        </div>
                                        <div class="col-md-9 line-height-2">
                                            {{ $quote_requests->port_of_origin }}
                                        </div>
                                    </div>

                                    <div class="col-md-6 no-padding">
                                        <div class="col-md-3">
                                            <label class="control-label">Volumn (CBM)</label>
                                        </div>
                                        <div class="col-md-9 line-height-2">
                                            @if($quote_requests->units==1) {{ $quote_requests->shipment_volume}} @else {{ $quote_requests->shipment_volume*16.02}} @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6 no-padding">
                                        <div class="col-md-3">
                                            <label class="control-label">Goods Ready Date</label>
                                        </div>
                                        <div class="col-md-9 line-height-2">
                                            {{ date("m/d/Y",strtotime($quote_requests->goods_ready_date)) }}
                                        </div>
                                    </div>

                                    <div class="col-md-6 no-padding">
                                        <div class="col-md-3">
                                            <label class="control-label">Shipment Notes</label>
                                        </div>
                                        <div class="col-md-9 line-height-2">
                                            {{ $quote_requests->shipment_notes }}
                                        </div>
                                    </div>
                                    
                                @endforeach
                            </div>

                            <div class="col-md-12 no-padding margin-bottom-30">
                                {{--*/$count=1/*--}}
                                <input type="hidden" name="customer_requirement_id" id="customer_requirement_id" value="{{ $quote_requests->id }}">
                                @if(!empty($shipping_quote))
                                @foreach($shipping_quote as $shipping_quotes)
                                    @foreach($quote_method as $quote_methods)
                                        @if($quote_methods->shipping_method_id==$shipping_quotes['shipping_method_id'])
                                            {{--*/$charge_id=array();/*--}}
                                            @foreach($shipping_quote_charge as $shipping_quote_charges)
                                                @if($shipping_quote_charges->shipping_quote_id==$shipping_quotes['id'])
                                                    {{--*/ $charge_id[]=$shipping_quote_charges->charge_id /*--}}
                                                @endif
                                            @endforeach
                                    <input type="hidden" name="shipping_quote_id{{$count}}" id="shipping_quote_id{{$count}}" value="{{$shipping_quotes['id']}}">
                                    <input type="hidden" name="method{{$count}}" id="method{{$count}}" value="{{$quote_methods->shipping_method_id}}">
                                    <div style="border-bottom:1px solid #c8c8c8;width: 99%">
                                        <span class="heading">SHIPPING QUOTE -  {{ strtoupper($quote_methods->shipping_name) }}</span>
                                    </div>

                                    <div class="form-group" style="padding-left: 2%" id="method_detail_div{{$count}}">
                                        <div class="col-md-12 no-padding">
                                            <label for="port" class="control-label col-md-2" style="text-align:left">Closest Shipping @if($quote_methods->shipping_method_id >1)Airport @else Port @endif </label>

                                            <div class="col-md-4 no-padding">
                                                <input type="text" name="port{{$count}}" id="port{{$count}}" class="form-control validate[required]" value="{{ $shipping_quotes['shipping_port'] }}">
                                            </div>
                                        </div>
                                        @if($quote_methods->shipping_method_id >1)
                                            <div class="col-md-12 no-padding">
                                                <label for="charge_weight" class="control-label col-md-2" style="text-align:left">CHARGEABLE WEIGHT (KGS)</label>
                                                <div>
                                                    <!-- <div class="input-group col-md-12"> -->
                                                    <div class="col-md-4 no-padding">
                                                        <input type="text" name="charge_weight{{$count}}" id="charge_weight{{$count}}" class="form-control validate[required, custom[number]]" value="{{ $shipping_quotes['chargeable_weight'] }}">
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-md-12 no-padding">
                                            <label for="freight_fee" class="control-label col-md-2" style="text-align:left">
                                                @if($quote_methods->shipping_method_id==1 && $quote_requests->shipment_type_id=='1' )
                                                    {{ $quote_methods->shipping_name }} FEE (PER CBM)
                                                @elseif($quote_methods->shipping_method_id==1)
                                                    FCL Fee
                                                @else
                                                    {{ $quote_methods->shipping_name }} FEE
                                                @endif
                                            </label>
                                            <div>
                                                <div class="col-md-4 no-padding">
                                                    <input type="text" name="freight_fee{{$count}}" id="freight_fee{{$count}}" class="form-control validate[required] custom[integer]" value="{{ $shipping_quotes['freight_fee'] }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <label for="charges" class="control-label col-md-12" style="text-align:left">ADDITIONAL CHARGES</label>
                                        </div>
                                        <div>
                                            <div class="input-group col-md-12">
                                                {{--*/$cnt=1/*--}}
                                                @foreach($shipping_charges as $charges)
                                                    @if($charges->shipping_method_id==$quote_methods->shipping_method_id)
                                                            <span class="col-md-4"> <input type="checkbox" name="charges{{$count}}_{{$charges->charge_id}}" id="charges{{$count}}_{{$charges->charge_id}}" class="" onchange="get_charges('{{$count}}','{{$cnt}}','{{$charges->charge_id}}','{{$charges->name}}')" onLoad="get_charges('{{$count}}','{{$cnt}}','{{$charges->charge_id}}','{{$charges->name}}')" value="{{ $charges->charge_id }}" @if(in_array($charges->charge_id,$charge_id)) {{ "checked" }} @endif> {{ $charges->name }} </span>
                                                            {{--*/$cnt++ /*--}}
                                                            @endif
                                                @endforeach
                                                <input type="hidden" name="cnt{{$count}}" id="cnt{{$count}}" value="{{$cnt}}">
                                            </div>
                                        </div>
                                        <div>
                                            <div class="input-group col-md-12" id="charges_div{{$count}}">
                                                @foreach($shipping_charges as $charges)
                                                    @foreach($shipping_quote_charge as $shipping_quote_charges)
                                                        @if($charges->shipping_method_id==$quote_methods->shipping_method_id  && $shipping_quote_charges->shipping_method_id == $charges->shipping_method_id && $shipping_quotes['id'] == $shipping_quote_charges->shipping_quote_id && $shipping_quote_charges->charge_id==$charges->charge_id)
                                                            <div class='col-md-3' id='charge_price_div{{$count}}_{{$charges->charge_id}}'>
                                                                <label for='charge_price' class='control-label col-md-10' style='text-align:left'>{{ $charges->name}}</label>
                                                                <div class='input-group col-md-10'>
                                                                    <input type='text' name='charge_price{{$count}}_{{$charges->charge_id}}' id='charge_price{{$count}}_{{$charges->charge_id}}' class='form-control' value="{{ $shipping_quote_charges->charge }}">
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </div>
                                        </div>
                                        <div>
                                            <input type="checkbox" name="additional{{$count}}" id="additional{{$count}}" onchange="additionaldetail('{{$count}}')" @if(!empty($shipping_quotes['additional_note'])) {{ "checked" }} @endif>&nbsp;<label for="additional_note" class="control-label" style="text-align:left; max-width: 35%" @if(!empty($shipping_quotes->additional_note)) {{ "checked" }} @endif> SEND ADDITIONAL NOTE TO FBAFORWARD? (OPTIONAL)</label>
                                        </div>
                                        <div>
                                            <div class="input-group col-md-12">
                                                @if(!empty($shipping_quotes['additional_note']))
                                                    <textarea name="additional_note{{$count}}" id="additional_note{{$count}}" class="form-control validate[required]" rows="3" cols="20">{{ $shipping_quotes['additional_note'] }}</textarea>
                                                @else
                                                    <textarea name="additional_note{{$count}}" id="additional_note{{$count}}" class="form-control validate[required]" rows="3" cols="20" style="display: none;">{{ $shipping_quotes['additional_note'] }}</textarea>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                   {{--*/$count++/*--}}
                                            @endif
                                        @endforeach
                                @endforeach
                                    @if($count<4)
                                        <select name="shipping_method"  id="shipping_method" class="select2">
                                            <option value="">Select Shipping Method</option>
                                            @foreach($shipping_method as $shipping_methods)
                                                <option value="{{ $shipping_methods->shipping_method_id }}">{{ $shipping_methods->shipping_name }}</option>
                                            @endforeach
                                        </select>
                                        <button class="btn btn-primary" id="add_method_button">Add Shipping Method For Quote</button>
                                    @endif
                                @else
                                    @foreach($quote_method as $quote_methods)
                                            <input type="hidden" name="method{{$count}}" id="method{{$count}}" value="{{$quote_methods->shipping_method_id}}">
                                            <!-- <div style="background-color: #003c6b; color:#ffffff; padding-left:3%; margin-bottom:20px; width: 99%" id="method_div{{$count}}">
                                                <span class="heading">SHIPPING QUOTE - {{ strtoupper($quote_methods->shipping_name) }}</span>
                                            </div> -->

                                            <div style="border-bottom:1px solid #c8c8c8;width: 99%" id="method_div{{$count}}">
                                                <span class="heading">SHIPPING QUOTE - {{ strtoupper($quote_methods->shipping_name) }}</span>
                                            </div>

                                            <div class="form-group" style="padding-left: 2%" id="method_detail_div{{$count}}">
                                                <div class="col-md-12 no-padding">
                                                    <label for="port" class="control-label col-md-2" style="text-align:left">Closest Shipping  @if($quote_methods->shipping_method_id >1)Airport @else Port @endif</label>

                                                    <div class="col-md-4 no-padding">
                                                        <input type="text" name="port{{$count}}" id="port{{$count}}" class="form-control validate[required]">
                                                    </div>
                                                </div>
                                                @if($quote_methods->shipping_method_id >1)
                                                    <div class="col-md-12 no-padding">
                                                        <label for="charge_weight" class="control-label col-md-2" style="text-align:left">CHARGEABLE WEIGHT (KGS)</label>
                                                        <div>
                                                            <div class="col-md-4 no-padding">
                                                                <input type="text" name="charge_weight{{$count}}" id="charge_weight{{$count}}" class="form-control validate[required, custom[number]]" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="col-md-12 no-padding">
                                                    <label for="freight_fee" class="control-label col-md-2" style="text-align:left">{{ strtoupper($quote_methods->shipping_name) }} FEE @if($quote_methods->shipping_method_id==1) {{ "(PER CBM)" }}@endif</label>
                                                        <div>
                                                            <div class="col-md-4 no-padding">
                                                                <input type="text" name="freight_fee{{$count}}" id="freight_fee{{$count}}" class="form-control validate[required] custom[integer]" >
                                                            </div>
                                                            <div>
                                                                <label for="charges" class="control-label col-md-12" style="text-align:left">ADDITIONAL CHARGES</label>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div>
                                                    <div class="input-group col-md-12">
                                                        {{--*/$cnt=1/*--}}
                                                        @foreach($shipping_charges as $charges)
                                                            @if($charges->shipping_method_id==$quote_methods->shipping_method_id )
                                                                  <span class="col-md-4"> <input type="checkbox" name="charges{{$count}}_{{$charges->charge_id}}" id="charges{{$count}}_{{$charges->charge_id}}" class="" onchange="get_charges('{{$count}}','{{$cnt}}','{{$charges->charge_id}}','{{$charges->name}}')" value="{{ $charges->charge_id }}" > {{ $charges->name }} </span>
                                                                  {{--*/$cnt++ /*--}}
                                                            @endif
                                                        @endforeach
                                                        <input type="hidden" name="cnt{{$count}}" id="cnt{{$count}}" value="{{$cnt}}">
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="input-group col-md-12" id="charges_div{{$count}}">
                                                    </div>
                                                </div>
                                                <div>
                                                    <input type="checkbox" name="additional{{$count}}" id="additional{{$count}}" onchange="additionaldetail('{{$count}}')" onLoad="additionaldetail('{{$count}}')" @if(!empty($shipping_quotes->additional_note)) {{ "checked" }} @endif>&nbsp;<label for="additional_note" class="control-label" style="text-align:left; max-width: 35%"> SEND ADDITIONAL NOTE TO FBAFORWARD? (OPTIONAL)</label>
                                                </div>
                                                <div>
                                                    <div class="input-group col-md-12">
                                                        <textarea name="additional_note{{$count}}" id="additional_note{{$count}}" class="form-control validate[required]" rows="3" cols="20" style="display: none;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--*/$count++/*--}}
                                @endforeach
                            @endif
                            </div>
                            <input type="hidden" name="counting" id="counting" value="{{$count}}">
                         </div>
                    <div class="col-md-12" style="text-align: right">
                        {!! Form::submit('  Submit Quote  ', ['class'=>'btn btn-primary', ]) !!}
                    </div>
                     </div>
                {!! Form::close() !!}
                 </div>
                 </div>
                 </div>
             </div>
    </section>
    @endsection
    @section('js')
        {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
        {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
        {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
         <script type="text/javascript">
            $(document).ready(function () {
                // Validation Engine init
                 var prefix = 's2id_';
                // $("form[id^='validate']").validationEngine('attach',
                // {
                //      promptPosition: "bottomRight", scroll: false,
                //      prettySelect: true,
                //      usePrefix: prefix
                // });

                $("#validate").validationEngine();
             });

         </script>
         <script type="text/javascript">
             function additionaldetail(id)
             {
                 if($("#additional"+id).prop('checked') == true) {
                     $("#additional_note"+id).show();
                 }
                 else
                 {
                     $("#additional_note"+id).hide();
                 }
             }
             function get_charges(count,cnt,charge_id, charge_name) {
                 if($("#charges"+count+"_"+charge_id).prop('checked') == true){
                    $("#charges_div"+count).append("<div class='col-md-3' id='charge_price_div"+count+"_"+charge_id+"'><label for='charge_price' class='control-label col-md-10' style='text-align:left'>"+charge_name+"</label><div class='input-group col-md-10'><input type='text' name='charge_price"+count+"_"+charge_id+"' id='' class='form-control validate[required]'></div></div>");
                }
                else {
                    $("#charge_price_div"+count+"_"+charge_id).remove();
                    $("#charge_price"+count+"_"+charge_id).val('');
                 }
             }
             $("#add_method_button").click(function () {
                 var shipping_method_id= $("#shipping_method").val();
                 var customer_requirement_id = $("#customer_requirement_id").val();
                 var count=$("#counting").val();
                 var old_count=count-1;
                 var charge_span='';
                 $.ajax({
                     headers: {
                         'X-CSRF-Token': "{{ csrf_token() }}"
                     },
                     method: 'POST', // Type of response and matches what we said in the route
                     url: '/shipper/getnewmethod', // This is the url we gave in the route
                     data: {
                         'customer_requirement_id': customer_requirement_id,
                         'shipping_method_id': shipping_method_id,
                     }, // a JSON object to send back
                     success: function (response) { // What to do if we succeed
                         response = $.parseJSON(response);

                         $.each( response.shipping_method, function( key, value ) {
                             var closest_shipping_port='port';
                             var chargeable_weight_span='';
                             if(value.shipping_method_id>1) {
                                 closest_shipping_port = 'Airport';
                                 chargeable_weight_span='<div class="col-md-12 no-padding">' +
                                     '<label for="charge_weight" class="control-label col-md-2" style="text-align:left">CHARGEABLE WEIGHT (KGS)</label>' +
                                 '<div>' +
                                 '<div class="col-md-4 no-padding">' +
                                     '<input type="text" name="charge_weight'+count+'" id="charge_weight'+count+'" class="form-control validate[required, custom[number]]" >'+
                                     '</div>' +
                                     '</div>' +
                                     '</div>';
                             }
                             var sub_count=1;
                             $.each(response.shipping_charges, function (key, charge_value) {
                                 var charge_name = charge_value.name;
                                 charge_span+= '<span class="col-md-4"><input type="checkbox" name="charges'+count+'_'+charge_value.charge_id+'" id="charges'+count+'_'+charge_value.charge_id+'" class="" onchange="get_charges('+count+','+sub_count+','+charge_value.charge_id+',\''+charge_name+'\')" value="'+charge_value.charge_id+'" > '+charge_value.name+' </span>';
                                    sub_count++;
                             });
                             $("#method_detail_div"+old_count).after('<input type="hidden" name="method'+count+'" id="method'+count+'" value="'+shipping_method_id+'"><div style="border-bottom:1px solid #c8c8c8;width: 99%" id="method_div'+count+'">' +
                                 '<span class="heading">SHIPPING QUOTE - '+value.shipping_name+'</span>' +
                                 '</div>' +
                                 '<div class="form-group" style="padding-left: 2%" id="method_detail_div'+count+'">' +
                                 '<div class="col-md-12 no-padding">' +
                                 '<label for="port" class="control-label col-md-2" style="text-align:left">Closest Shipping '+closest_shipping_port+'</label>' +
                                 '<div>' +
                                 '<div class="col-md-4 no-padding">' +
                                 '<input type="text" name="port'+count+'" id="port'+count+'" class="form-control validate[required]">' +
                                 '</div>' +
                                 '</div>' +
                                 '</div>' + chargeable_weight_span +
                                 '<div class="col-md-12 no-padding">' +
                                 '<label for="freight_fee" class="control-label col-md-2" style="text-align:left">'+value.shipping_name+' FEE </label>' +
                                 '<div>' +
                                 '<div class="col-md-4 no-padding">' +
                                 '<input type="text" name="freight_fee'+count+'" id="freight_fee'+count+'" class="form-control validate[required, custom[number]]" >' +
                                 '</div>' +
                                 '</div>' +
                                 '</div>' +
                                 '<div>' +
                                 '<label for="charges" class="control-label col-md-12" style="text-align:left">ADDITIONAL CHARGES</label>' +
                                 '</div>' +
                                 '<div>' +
                                 '<div class="input-group col-md-12">' +charge_span+'<input type="hidden" name="cnt'+sub_count+'" id="cnt'+sub_count+'" value="'+sub_count+'">'+
                                 '</div>' +
                                 '</div>' +
                                 '<div>' +
                                 '<div class="input-group col-md-12" id="charges_div'+count+'">' +
                                 '</div>' +
                                 '</div>' +
                                 '<div>' +
                                 '<input type="checkbox" name="additional'+count+'" id="additional'+count+'" onchange="additionaldetail('+count+')">&nbsp;<label for="additional_note" class="control-label" style="text-align:left; max-width: 35%"> SEND ADDITIONAL NOTE TO FBAFORWARD? (OPTIONAL)</label>' +
                                 '</div>' +
                                 '<div>' +
                                 '<div class="input-group col-md-12">' +
                                 '<textarea name="additional_note'+count+'" id="additional_note'+count+'" class="form-control validate[required]" rows="3" cols="20" style="display: none;"></textarea>' +
                                 '</div>' +
                                 '</div>' +
                                 '</div>');
                             count++;
                             $("#counting").val(count);
                             if(count>3) {
                                 $("#add_method_button").hide();
                                 //$("#shipping_method").hide();
                                 $(".select2").css("display", "none");

                             }
                             else {
                                 $("#add_method_button").show();
                                 $("#shipping_method option[value='"+shipping_method_id+"']").remove();
                                 $("#shipping_method").show();
                             }
                         });
                     },
                     error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                         console.log(JSON.stringify(jqXHR));
                         console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                     }
                 });
                 return false;
             });
             $(document).ready(function () {
                 //Initialize Select2 Elements
                 $(".select2").select2({
                     placeholder: "Please Select",
                     allowClear: true
                 });
             });
         </script>
    @endsection
