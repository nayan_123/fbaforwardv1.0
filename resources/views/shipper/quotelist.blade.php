@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
    {!! Html::style('assets/dist/css/style.css') !!}
    <style>
        .col-md-9
        {
            width: 69%;
        }
        .col-md-3
        {
            width: 31%;
        }
        .col-md-4
        {
            width: 30%;
        }
    </style>
@endsection
@section('content')
    <!-- Main content -->
        <section class="content-header">
            <h1>
                <i class="fa fa-anchor"></i> {{$title}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><i class="fa fa-anchor"></i>{{$title}}</li>
            </ol>
        </section>
        <section class="content">
            <div class="box">
                <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive no-padding">
                    <div class="col-md-6">
                        <h3>Quote Requests</h3>
                    </div>

                    <table id="data_table" class="table">
                        <thead>
                        <tr>
                           <th class="col-md-4">Product</th>
                           <th class="col-md-1">Company</th>
                           <th class="col-md-2">Status</th>
                           <th class="col-md-2">Date Quote Requested</th>
                           <th class="col-md-2">View Details</th>
                           <th class="col-md-2" style="padding-left:0px">Enter New Quote</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($quote_request as $quote_requests)
                            <tr id="tr{{$quote_requests->id}}">
                                <td class="col-md-4">
                                    @if(empty($quote_requests->customer_requirement_details_product_name))
                                        {{ $quote_requests->product_name }}
                                    @else
                                        {{ $quote_requests->customer_requirement_details_product_name }}
                                    @endif
                                </td>
                                <td class="col-md-1">{{ $quote_requests->company_name }}</td>
                                <td class="col-md-2">@if($quote_requests->is_activated=='0'){{ "NEW" }} @elseif($quote_requests->is_activated=='3') Revision Requested @elseif($quote_requests->is_activated=='5'){{ "NEW" }}@endif</td>
                                <td class="col-md-1">{{ date('m/d/Y',strtotime($quote_requests->created_at)) }}</td>
                                <td class="col-md-2"><button class="button" onclick="getdetails('{{ $quote_requests->id}}')" name="view{{$quote_requests->id}}" id="view{{$quote_requests->id}}">VIEW DETAILS</button></td>
                                <td class="col-md-2"><a class="button" href="{{ url('/shipper/shippingquoteform/'.$quote_requests->id) }}" >SUBMIT QUOTE</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="col-md-6">
                        <h3>Expired or Rejected Quotes</h3>
                    </div>
                    <table id="data_table_quotes" class="table">
                        <thead>
                        <tr>
                            <th class="col-md-4">Product</th>
                            <th class="col-md-2">Company</th>
                            <th class="col-md-2">Status</th>
                            <th class="col-md-2">Date Quote Submitted</th>
                            <th class="col-md-2">View Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($quote_expire as $quote_expires)
                            <tr id="tr{{$quote_expires->id}}">
                                <td class="col-md-4"> @if(empty($quote_expires->customer_requirement_details_product_name))
                                        {{ $quote_expires->product_name }}
                                    @else
                                        {{ $quote_expires->customer_requirement_details_product_name }}
                                    @endif</td>
                                <td class="col-md-2">{{ $quote_expires->company_name }}</td>
                                <td class="col-md-2">@if($quote_expires->is_activated=='2') REJECT @else EXPIRED @endif</td>
                                <td class="col-md-2">{{ date('m/d/Y',strtotime($quote_expires->submitted_date)) }}</td>
                                <td class="col-md-2"><button class="button" onclick="getdetails('{{ $quote_expires->id}}')" name="view{{$quote_expires->id}}" id="view{{$quote_expires->id}}">VIEW DETAILS</button></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
                </div>
            </div>
    </section>
    @endsection
@section('js')
    <script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#data_table').DataTable({
                "bSort" : false,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false
            });

            $('#data_table_quotes').DataTable({
                "bSort" : false,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false
            });
        });
        function getdetails(id) {
            $('#tr'+id).toggleClass("trhead");
            if($('#tr'+id).hasClass("trhead"))
            {
                $('tr').removeClass("trhead");
                $('#tr'+id).addClass("trhead");
                $("button").removeClass("button1");
                $("button").not( ".btn-box-tool" ).addClass("button");
                $("#view"+id).removeClass("button");
                $("#view"+id).addClass("button1");
                $('.preloader').css("display", "block");
                $.ajax({
                    headers: {
                        'X-CSRF-Token':  "{{ csrf_token() }}"
                    },
                    method: 'POST', // Type of response and matches what we said in the route
                    url: '/shipper/getshippingrequest', // This is the url we gave in the route
                    data: {
                        'id': id,
                    }, // a JSON object to send back
                    success: function (response) { // What to do if we succeed
                        $('.preloader').css("display", "none");
                        response = $.parseJSON(response);
                        console.log(response);
                        $(".trdetails").remove();
                        $(".trdetails1").remove();
                        var method='';

                        $.each( response.quote_request, function( key, value ) {
                            var approx_date = "";
                            if(value.goods_ready_date != '0000-00-00 00:00:00' || value.goods_ready_date!=null) {
                                var d = Date.parse(value.goods_ready_date);
                               if(d !==null)
                                approx_date = d.toString('MM/dd/yyyy');
                            }
                            var shipment_type = value.shipment_type_short_name;
                            var shipment_type_rate = value.shipment_type_rate;
                            var incoterm=value.incoterm_name;
                            var shipment_type_price = shipment_type_rate*value.shipment_volume;
                            var method_span = '';
                            var charge_qty = '1';
                            var shipping_sub_total = 0;
                            shipping_sub_total+=shipment_type_price;
                            $.each( response.quote_method, function( key, method_value ) {
                                if(key==0)
                                    method+="<span>"+method_value.shipping_name+"</span><br>";
                                else
                                    method+="<span style='padding-left:200px;'>"+method_value.shipping_name+"</span><br>";
                                if(value.expire_date!=null) {
                                    method_span += "<span class='col-md-5' style='background-color: #d8d8d8'>SHIPPING - " + method_value.shipping_name + "</span>" + "<span class='col-md-3' style='background-color: #d8d8d8'>RATE</span>" + "<span class='col-md-2' style='background-color: #d8d8d8; width: 19%'>QTY</span>" + "<span class='col-md-1' style='background-color: #d8d8d8'>PRICE</span>";
                                    method_span += "<span class='col-md-5'>" + method_value.shipping_name + " - " + shipment_type + "</span>" + "<span class='col-md-3'>$" + shipment_type_rate + "</span>" + "<span class='col-md-2' style='width: 19%'>" + value.shipment_volume + "</span>" + "<span class='col-md-1'>$" + shipment_type_price + "</span>";
                                    var charge_span = '';
                                    $.each(response.detail_charge, function (charge_key, charge_value) {
                                        var charge_price = charge_value.charge * charge_qty;
                                        shipping_sub_total += charge_price;
                                        charge_span += "<span class='col-md-5'>" + charge_value.name + "</span>" + "<span class='col-md-3'>$" + charge_value.charge + "</span>" + "<span class='col-md-2' style='width: 19%'>" + charge_qty + "</span>" + "<span class='col-md-1'>$" + charge_price + "</span>";
                                    });
                                    method_span += charge_span;
                                    method_span += "<span class='col-md-11' style='background-color: #f3fafe'>Shipping Subtotal</span><span class='col-md-1' style='background-color: #f3fafe'>$" + shipping_sub_total + "</span>";
                                }
                            });
                            if(value.incoterms=='1')
                                incoterm ="FOB ";
                            else if(value.incoterms=='2')
                                incoterm="EXW ";
                            var expire='';
                            if(value.expire_date==null)
                               expire = "<span class='shead col-md-1'>Company </span><span class='sdetail col-md-11'>"+value.company_name+"</span>";
                            else {
                                var expire_date = Date.parse(value.expire_date);
                                expire = "<span class='shead col-md-1'>Company </span><span class='sdetail col-md-7'>" + value.company_name + "</span><span class='shead col-md-2'>Date Quote Expired </span><span class='sdetail col-md-2'>" + expire_date.toString('MM/dd/yyyy') + "</span>";
                            }
                            var revision ='';
                            if(value.is_activated=='3')
                                revision="<span class='col-md-2' style='color: #8B0000; font-weight: bold;'>Revision Request: </span><span class='sdetail'>Additional Notes- </span><span class='sdetail'>"+value.shipment_notes+"</span>";
                            else
                                revision="<span class='shead'>Additional Notes: </span><span class='sdetail'>"+value.shipment_notes+"</span>";
                            var product_name='';
                            if(value.product_id=='')
                                 product_name=value.productname;
                            else
                                product_name=value.product_name;
                            $("#tr" + id).after("<tr  class='trdetails'>" +
                            "<td colspan='6'>" + expire+
                            "<span class='shead col-md-1'>Product </span><span class='sdetail col-md-7'>"+product_name+"</span><span class='sdetail col-md-4'>&nbsp;</span><span class='col-md-12'>&nbsp;</span>" +
                            "<div class='col-md-9' style='padding-left: 0px'>" +
                            "<span class='shead col-md-1'>Cartons </span><span class='sdetail col-md-1'>"+value.no_boxs+"</span>" +
                            "<span class='shead col-md-1'>Weight </span><span class='sdetail col-md-2'>"+value.shipment_weight+ (value.units=='1' ? 'KG':'LB')+ "</span>" +
                            "<span class='shead col-md-2'>shipment Type</span><span class='sdetail col-md-2' style='padding:0px; width:10%'>"+shipment_type+"</span>" +
                            "<span class='shead col-md-2'>Port of Orgin</span><span class='sdetail col-md-1' style='padding:0px;'>"+value.port_of_origin+"</span>" +
                            "<span class='shead col-md-1'> Units </span><span class='sdetail col-md-1'>"+value.qty_per_box+"</span>" +
                            " <span class='shead col-md-1'>Volume </span><span class='sdetail col-md-2' >"+value.shipment_volume+(value.units=='1' ? 'CBM':'CFT')+"</span>" +
                            "<span class='shead col-md-2'> Incoterms </span><span class='sdetail col-md-2' style='padding:0px; width:10%'>"+incoterm+"</span>" +
                            "<span class='shead col-md-2'>Approx. Goods Ready Date</span><span class='sdetail col-md-1' style='padding:0px;'>"+approx_date+"</span>" +
                            "</div>" +
                            "<div class='col-md-3'><span class='shead'>Shipping Method(s) Requested</span><span class='sdetail'>"+method+"</span></div>" +
                            "<div class='col-md-12'>"+revision+"</div>" +
                            "</td>" +
                            "</tr>" +
                            "<tr class='trdetails1'><td colspan='6' style='padding:0px'>" +method_span+"</td></tr>");

                        });

                    },
                    error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                        $('.preloader').css("display", "none");
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
            else
            {
                $("#view"+id).removeClass("button1");
                $("#view"+id).addClass("button");
                $(".trdetails").remove();
                $(".trdetails1").remove();
            }
        }
    </script>
@endsection
