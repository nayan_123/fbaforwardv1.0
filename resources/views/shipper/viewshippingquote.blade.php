@foreach($shipment as $key=>$shipments)
    <div>
        <div class="table-responsive no-padding">
            <table class="table" id="list">
                <thead>
                <tr>
                    <th class="col-md-6"><span>Product</span></th>
                    <th class="col-md-2"><span>Qty Per Case</span></th>
                    <th class="col-md-2"><span># Of Case</span></th>
                    <th><span>Total</span></th>
                    <th><span>Shipment Type</span></th>
                </tr>
                </thead>
                <tbody>

                @foreach($shipment_detail as $shipment_details)
                        <tr>
                            <td>{{ $shipment_details->product_name }}</td>
                            <td>{{ $shipment_details->qty_per_box}}</td>
                            <td>{{ $shipment_details->no_boxs}}</td>
                            <td>{{ $shipment_details->total}}</td>
                            <td>{{ $shipment_details->shipping_name }}</td>
                        </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-4">
                        {!! Form::label('', 'Goods Ready Date :', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6">
                            {{ date('Y-m-d',strtotime($shipments->goods_ready_date)) }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('', 'Shipment Reference :', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6">
                            {{ $shipments->title }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('', 'Incoterms :', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6" >
                            {{ $shipments->incoterms }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        {!! Form::label('', 'Supplier :', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6" >
                            {{ $shipments->company_name }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('', 'Port of Origin :', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6" >
                            {{ $shipments->port_of_origin }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('', 'Shipment Size :', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6" >
                            @if($shipments->shipment_size=='1')
                                Less than Container
                            @elseif($shipments->shipment_size=='2')
                                Full Container
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        {!! Form::label('', 'Shipment Size Type :', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6" >
                            @if($shipments->shipment_size_type=='1')
                                Package dimensions
                            @elseif($shipments->shipment_size_type=='2')
                                Entire Shipment Dimensions
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('', 'Units :', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6" >
                            {{ $shipments->units }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('', 'Does shipment contain lithium ion batteries? :', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6" >
                            @if($shipments->batteries=='0')
                                yes
                            @else
                                No
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        {!! Form::label('', 'Does shipment contain magnets? :', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6" >
                            @if($shipments->magnets=='0')
                                yes
                            @else
                                No
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('', 'Does shipment contain hazardous materials? :', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6" >
                            @if($shipments->hazard=='0')
                                yes
                            @else
                                No
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($shipments->shipment_size_type=='1')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-6">
                            {!! Form::label('', 'Package Dimension ', ['class' => 'control-label col-md-6']) !!}
                            <div class="input-group col-md-6">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-2">
                            {!! Form::label('', 'Length:', ['class' => 'control-label col-md-6']) !!}
                            <div class="input-group col-md-6" >
                                {{ $shipments->package_length }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('', 'Width:', ['class' => 'control-label col-md-6']) !!}
                            <div class="input-group col-md-6" >
                                {{ $shipments->package_width }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('', 'Height:', ['class' => 'control-label col-md-6']) !!}
                            <div class="input-group col-md-6" >
                                {{ $shipments->package_height }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            {!! Form::label('', 'Weight:', ['class' => 'control-label col-md-6']) !!}
                            <div class="input-group col-md-6" >
                                {{ $shipments->package_weight }}
                            </div>
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('', '# Of Cartoon:', ['class' => 'control-label col-md-6']) !!}
                            <div class="input-group col-md-6" >
                                {{ $shipments->package_no_of_cartoon }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-6">
                        {!! Form::label('', 'Shipment Dimension ', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-2">
                        {!! Form::label('', 'Weight:', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6" >
                            {{ $shipments->shipment_weight }}
                        </div>
                    </div>
                    <div class="col-md-2">
                        {!! Form::label('', 'Volume:', ['class' => 'control-label col-md-6']) !!}
                        <div class="input-group col-md-6" >
                            {{ $shipments->shipment_volume }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('', 'Shipment Notes :', ['class' => 'control-label col-md-2']) !!}
                <div class="input-group col-md-10">
                    {{ $shipments->shipment_notes }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('', 'Charges:', ['class' => 'control-label col-md-2']) !!}
                <div class="input-group col-md-10">
                    @foreach($charges as $charge)
                    {{ $charge->name }},
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                {!! Form::label('', 'Rate:', ['class' => 'control-label col-md-6']) !!}
                <div class="input-group col-md-6" >
                    {{ $shipments->rate }}
                </div>
            </div>
            <div class="col-md-2">
                {!! Form::label('', 'Quantity:', ['class' => 'control-label col-md-6']) !!}
                <div class="input-group col-md-6" >
                    {{ $shipments->qty }}
                </div>
            </div>
            <div class="col-md-2">
                {!! Form::label('', 'Price:', ['class' => 'control-label col-md-6']) !!}
                <div class="input-group col-md-6" >
                    {{ $shipments->price }}
                </div>
            </div>
            <div class="col-md-6">
                {!! Form::label('', 'Reason Of Rejection :', ['class' => 'control-label col-md-3']) !!}
                <div class="input-group col-md-9">
                    {{ $shipments->reason }}
                </div>
            </div>
        </div>
    </div>
@endforeach