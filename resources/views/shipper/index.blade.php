@extends('layouts.member.app')
@section('title', 'Shipment')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
<section class="content">
    <section class="content-header">
        <h1><i class="fa fa-shopping-cart"></i> Shipment</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-shopping-cart"></i>Shipment</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive no-padding">
                            <table id="inprogress_data" class="table" >
                                <thead>
                                <tr>
                                    <th class="col-md-1">Order Number</th>
                                    <th class="col-md-2">Product</th>
                                    <th class="col-md-2">Company</th>
                                    <th class="col-md-1">Cargo Ready</th>
                                    <th class="col-md-2">Status</th>
                                    <th class="col-md-1">ETA</th>
                                    <th class="col-md-1">ETD</th>
                                    <th class="col-md-2">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($order))
                                    @foreach($order as $orders)
                                        <tr>
                                            <td class="col-md-1">{{ $orders['order_no'] }}</td>
                                            <td class="col-md-2">{{ $orders['product_name'] }}</td>
                                            <td class="col-md-2">{{ $orders['company_name'] }}</td>
                                            <td class="col-md-1"></td>
                                            <td class="col-md-2">{{ $orderStatus[$orders['is_activated']] }}</td>
                                            <td class="col-md-1">{{ $orders['ETA_US'] }}</td>
                                            <td class="col-md-1">{{ $orders['ETD_china'] }}</td>
                                            <td class="col-md-2"> <a href="{{ url('shipper/view-shipment',$orders['order_id']) }}"><button class="button">View Shipment</button></a> <!-- <a href="{{ url('shipper/view-shipment',$orders['order_id']) }}"><button class="button"  style="margin-top:5px;">Edit Dates</button></a> --></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="col-md-4">No data available in table</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection

@section('js')
    <script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {


    });
</script>
@endsection