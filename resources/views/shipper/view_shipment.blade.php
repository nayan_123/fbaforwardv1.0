@extends('layouts.member.app')
@section('title', 'View Shipment')
@section('css')
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
    {!! Html::style('assets/dist/css/style.css') !!}
    <style>
    	.padding-left-30{ padding-left:30px; }
		.datepicker { border-radius: 0px; }
	</style>
@endsection
@section('content')
<section class="content-header">
    <h1><i class="fa fa-anchor"></i> View Shipment</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-anchor"></i>View Shipment</li>
    </ol>
</section>
<section class="content">
	<div class="box border-top-0">
        <div class="box-body ">
        	<div class="row ">
        		<div class="col-md-12">
        			<div class="col-md-1">
        				<b>Status :</b>
        			</div>
        			<div class='col-md-11'>
        				{{ $orderStatus[$order['is_activated']] }}
        			</div>
        		</div>
        	</div>
		</div>
	</div>

	<div class="box border-top-0">
		<div class="box-body ">
			<form id="validate_shipment" class="form-horizontal" method="post" action="{{ url('shipper/create-shipment') }}">
				{{ csrf_field() }}

				<input type="hidden" name="order_id" value="{{ $order->order_id }}">
		    	<div class="row ">
		    		<div class="col-md-12 padding-0-7">
		    			<div class="shipment-box">
		    				<h3>Order Details</h3>
		    			</div>
		    		</div>
		    		<div class="col-md-12">
			    		<div class="col-md-6">
		            		<span class="col-md-3 no-padding"><b>Order</b></span>
		            		<span class="sdetail col-md-9">{{ $order->order_no }}</span>
		            	</div>
		            	<div class="col-md-6">
		            		<b><span class=" col-md-3">Phone Number</span></b>
		            		<span class="sdetail col-md-9">{{ $order->contact_phone }}</span>
		            	</div>

		            	<div class="row">
			            	<div class="col-md-6 padding-left-30">
			            		<b><span class=" col-md-3 no-padding">Seabay Number</span></b>
			            		<span class="sdetail col-md-9 text-center"><input type="text" name="sb_number" class="form-control validate[required] hide" id="sb_number" style="margin: 0%;"><i class="fa fa-edit" id="edit_sb"></i></span>
			            	</div>

			            	<div class="col-md-6">
			            		<b><span class=" col-md-3">Email</span></b>
			            		<span class="sdetail col-md-9">{{ $order->contact_email }}</span>
			            	</div>
			            </div>

		            	<div class="col-md-6">
		            		<b><span class=" col-md-3 no-padding">Company</span></b>
		            		<span class="sdetail col-md-9">{{ $order->company_name }}</span>
		            	</div>

		            	<div class="col-md-6">
		            		<b><span class=" col-md-3">Address</span></b>
		            		<span class="sdetail col-md-9">{{ $order->company_address }}</span>
		            	</div>

		            	<div class="col-md-6">
		            		<b><span class="col-md-3 no-padding">Contact</span></b>
		            		<span class="sdetail col-md-9">{{ $order->company_name }}</span>
		            	</div>
		            </div>

		            <div class="col-md-12 margin-top-20 padding-0-7">
		            	<div class="shipment-box">
		    				<h3>Supplier Details</h3>
		    			</div>
		            </div>
		            <div class="col-md-12">
		            	<div class="col-md-6">
		            		<b><span class="col-md-3 no-padding">Contact Name</span></b>
		            		<span class="sdetail col-md-9">{{ $order->supplier_company_name }}</span>
		            	</div>

		            	<div class="col-md-6">
		            		<b><span class="col-md-3">Email</span></b>
		            		<span class="sdetail col-md-9">{{ $order->supplier_email }}</span>
		            	</div>

		            	<div class="col-md-6">
		            		<b><span class="col-md-3 no-padding">Phone Number</span></b>
		            		<span class="sdetail col-md-9">{{ $order->supplier_phone_number }}</span>
		            	</div>

		            	<div class="col-md-6">
		            		<b><span class="col-md-3">Province</span></b>
		            		<span class="sdetail col-md-9">{{ $order->supplier_city }}</span>
		            	</div>
		            </div>

		            <div class="col-md-12 margin-top-20 padding-0-7">
		            	<div class="shipment-box">
		    				<h3>Product Details</h3>
		    			</div>
		            </div>
		            <div class="col-md-12">
		            	<div class="col-md-12">
		            		<b><span class=" col-md-2 no-padding">Product</span></b>
		            		<span class="sdetail col-md-10">{{ $order->product_name }}</span>
		            	</div>

		            	<div class="col-md-6">
		            		<b><span class=" col-md-3 no-padding">Total Cartons</span></b>
		            		<span class="sdetail col-md-9">{{ $order->no_boxs }}</span>
		            	</div>

		            	<div class="col-md-6">
		            		<b><span class=" col-md-3">Weight(@if($order->customer_requirement_units==1){{"KG"}}@else{{"LB"}}@endif)</span></b>
		            		<span class="sdetail col-md-9">{{ $order->shipment_weight }}</span>
		            	</div>

		            	<div class="col-md-6">
		            		<b><span class=" col-md-3 no-padding">Total Units</span></b>
		            		<span class="sdetail col-md-9">{{ $order->total }}</span>
		            	</div>

		            	<div class="col-md-6">
		            		<b><span class=" col-md-3">Volume(@if($order->customer_requirement_units==1){{"CBM"}}@else{{"CFT"}}@endif)</span></b>
		            		<span class="sdetail col-md-9">{{ $order->shipment_volume }}</span>
		            	</div>
		            </div>

		            <div class="col-md-12 margin-top-20 padding-0-7">
		            	<div class="shipment-box">
		    				<h3>Shipment Details</h3>
		    			</div>
		            </div>
		            <div class="col-md-12">
		            	<div class="col-md-6 no-padding">
		            		<b><span class=" col-md-3">Shipment Type</span></b>
		            		<span class="sdetail col-md-9">{{ $order->shipping_name }}</span>
		            	</div>

		            	<div class="col-md-6">
		            		<b><span class=" col-md-3">POL</span></b>
		            		<span class="sdetail col-md-9">{{ $order->port_of_origin }}</span>
		            	</div>

		            	<div class="col-md-6 no-padding">
		            		<b><span class=" col-md-3">Incoterms</span></b>
		            		<span class="sdetail col-md-9">{{ $order->incoterm_name }}</span>
		            	</div>

		            	<div class="col-md-6">
		            		<b><span class=" col-md-3">POD</span></b>
		            		<span class="sdetail col-md-9">{{ $order->shipper_destination }}</span>
		            	</div>

		            	<div class="row">
			            	<div class="col-md-6 padding-left-30" style="margin-top:10px">
			            		<b><span class=" col-md-3 no-padding">Cargo Ready Date</span></b>
			            		<span class="sdetail col-md-9 text-center"><input type="text" name="cargo_ready_date" class="form-control datepicker hide" id="cargo_ready_date" ><i class="fa fa-edit" id="edit_cargo_ready_date"></i></span>
			            	</div>

			            	<div class="col-md-6" style="margin-top:10px">
			            		<b><span class=" col-md-3">Carrier</span></b>
			            		<span class="sdetail col-md-9 text-center"><input type="text" name="carrier" class="form-control validate[required] hide" id="carrier"><i class="fa fa-edit" id="edit_carrier"></i></span>
			            	</div>
			            </div>

			            <div class="row">
			            	<div class="col-md-6 padding-left-30" style="margin-top:10px">
			            		<b><span class=" col-md-3 no-padding">Closing Date</span></b>
			            		<span class="sdetail col-md-9 text-center"><input type="text" name="closing_date" class="form-control validate[required] datepicker hide" id="closing_date"><i class="fa fa-edit" id="edit_closing_date"></i></span>
			            	</div>

			            	<div class="col-md-6" style="margin-top:10px">
			            		<b><span class=" col-md-3">Tracking</span></b>
			            		<span class="sdetail col-md-9 text-center"><input type="text" name="tracking" class="form-control validate[required] hide" id="tracking"><i class="fa fa-edit" id="edit_tracking"></i></span>
			            	</div>
			            </div>

			            <div class="row">
			            	<div class="col-md-6 padding-left-30" style="margin-top:10px">
			            		<b><span class=" col-md-3 no-padding">ETD</span></b>
			            		<span class="sdetail col-md-9 text-center"><input type="text" name="etd" class="form-control validate[required] datepicker hide" id="etd"><i class="fa fa-edit" id="edit_etd"></i></span>
			            	</div>

			            	<div class="col-md-6" style="margin-top:10px">
			            		<b><span class=" col-md-3"># of Containers</span></b>
			            		<span class="sdetail col-md-9 text-center"><input type="text" name="containers" class="form-control validate[required] custom[integer] hide" id="containers"><i class="fa fa-edit" id="edit_containers"></i></span>
			            	</div>
			            </div>

			            <div class="row">
			            	<div class="col-md-6 padding-left-30" style="margin-top:10px">
			            		<b><span class=" col-md-3 no-padding">ETA</span></b>
			            		<span class="sdetail col-md-9 text-center"><input type="text" name="eta" class="form-control validate[required] datepicker hide" id="eta"><i class="fa fa-edit" id="edit_eta"></i></span>
			            	</div>

			            	<div class="col-md-6" style="margin-top:10px">
			            		<b><span class=" col-md-3">Container Size</span></b>
			            		<span class="sdetail col-md-9 text-center">
			            			<select class="form-control col-md-12 validate[required] hide" name="container_height" id="container_height">
			            				<option value="Standard">Standard</option>
			            				<option value="HQ">HQ</option>
			            			</select>

			            			<select class="form-control col-md-12 validate[required] hide" name="container_width" id="container_width" style="margin-top:10px;">
			            				<option value="20">20</option>
			            				<option value="40">40</option>
			            				<option value="45">45</option>
			            			</select>

			            			<i class="fa fa-edit" id="edit_container_size"></i></span>
			            	</div>
			            </div>
		            </div>

		            <div class="col-md-12 margin-top-20 padding-0-7">
		            	<div class="shipment-box">
		    				<h3>Shipping Quote</h3>
		    			</div>
		            </div>
		            <div class="col-md-12">
		            	@if($shipping_quote_charges != null)
			            	@foreach($shipping_quote_charges as $shipping_quote_charge)
			            		<div class="col-md-6">
				            		<b><span class="col-md-3">{{ $shipping_quote_charge->name }}</span></b>
				            		<span class="sdetail col-md-9">{{ $shipping_quote_charge->charge }}</span>
				            	</div>
			            	@endforeach
			            @endif
			        </div>
	            </div>

	            <div class="col-md-12 margin-top-20 padding-0-7">
	            	<div class="shipment-box">
	    				<h3>Shipment Notes</h3>
	    			</div>
	            </div>
	            <div class="col-md-12 margin-top-20">
	            	<div class="col-md-1">
	            		<label class="control-label">Notes</label>
	            	</div>
	            	<div class="col-md-11">
	            		<textarea name="notes" class="form-control" style="width:100%"></textarea>
	            	</div>
	            </div>

	            <div class="col-md-12 margin-top-20">
	            	<button class="button pull-right" type="submit">Submit</button>
	            </div>
	        </form>
		</div>
	</div>
</div>

</section>
@endsection
@section('js')

<script>

	$(document).ready(function () {

		$('#validate_shipment').validationEngine();

        $('.datepicker').datepicker({
            startDate: new Date(),
		});

        $('#edit_sb').click(function(){
        	$('#sb_number').removeClass('hide');
        	$('#edit_sb').hide();
        });

        $('#edit_cargo_ready_date').click(function(){
        	$('#cargo_ready_date').removeClass('hide');
        	$('#edit_cargo_ready_date').hide();
        });

        $('#edit_carrier').click(function(){
        	$('#carrier').removeClass('hide');
        	$('#edit_carrier').hide();
        });

        $('#edit_closing_date').click(function(){
        	$('#closing_date').removeClass('hide');
        	$('#edit_closing_date').hide();
        });

        $('#edit_tracking').click(function(){
        	$('#tracking').removeClass('hide');
        	$('#edit_tracking').hide();
        });

        $('#edit_etd').click(function(){
        	$('#etd').removeClass('hide');
        	$('#edit_etd').hide();
        });

        $('#edit_containers').click(function(){
        	$('#containers').removeClass('hide');
        	$('#edit_containers').hide();
        });

        $('#edit_eta').click(function(){
        	$('#eta').removeClass('hide');
        	$('#edit_eta').hide();
        });

        $('#edit_container_size').click(function(){
        	$('#container_height').removeClass('hide');
        	$('#container_width').removeClass('hide');
        	$('#edit_container_size').hide();
        });

	});
</script>
@endsection
