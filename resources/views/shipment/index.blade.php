@extends('layouts.member.app')
@section('title', $title)
@section('content')
<section class="content">
    <section class="content-header">
        <h1>
            <i class="fa fa-star"></i> {{$title}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-star"></i>{{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{$title}}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive no-padding">
                    <table id="inprogress_data" class="table" >
                        <thead>
                        <tr>
                            <th>Order No</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            </div>
        </div>
    </section>
</section>
@endsection