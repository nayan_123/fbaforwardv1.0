@extends('layouts.member.app')
@section('title', $title)
@section('css')
    <style type="text/css">
        .align{
            text-align: center;
        }
        .border
        {
            border-bottom: 1px solid #000000;
        }
        .background
        {
            background-color: #f2f2f2;
        }
    </style>

@endsection
@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-star"></i> {{$title}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-star"></i>{{$title}}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{$title}}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                                class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                @foreach($shipment as $key=>$shipments)
                    <table width="100%">
                        <tr>
                            <th width="70%" align="left"><h2><font color="#28a7e9">FBAForward</font></h2></th>
                            <th width="30%" align="left"></th>
                        </tr>
                        <tr>
                            <th width="70%" align="left"><span>1550 HOTEL CIRCLE NORTH</span><br><span>SUITE 360</span><br><span>SAN DIEGO, CA 92108</span><br><span>+1 866 526 3951</span></th>
                            <th width="30%" align="justify">SHIPMENT NAME: {{ $shipments->title }}<br>SHIPMENT ID:  {{ $shipments->cus_id }}<br>QUOTE ID:  {{$shipments->id }}</th>
                        </tr>
                    </table>
                    <br><br>
                    <h3><font color="#28a7e9">Quote Details</font></h3>
                    <table  id=""  width="95%">
                        <tr>
                            <th width="30%"><span>TRANSPORTATION MODE</span></th>
                            <th width="30%"><span>FREIGHT PRICE/KG</span></th>
                            <th width="30%"><span> TOTAL PRICE / KG </span></th>
                        </tr>
                        <tr>
                            <td width="30%" class="align"><span>{{ $shipments->shipping_name }}</span></td>
                            <td width="30%" class="align"><span></span></td>
                            <td width="30%" class="align"><span></span></td>
                        </tr>
                        <tr>
                            <th width="30%"><span>TOTAL PRICE</span></th>
                            <th width="30%"><span>FREIGHT SERVICE </span></th>
                        </tr>
                        <tr>
                            <td width="30%" class="align"><span></span></td>
                            <td width="30%" class="align"><span>{{ $shipments->port_of_origin  }}</span></td>
                        </tr>
                        </tbody>
                    </table>
                    <div>
                        <h3><font color="#28a7e9">Items</font></h3>
                        <table width="95%">
                            <tr>
                                <th colspan="5" class="align">Description</th>
                            </tr>
                            @foreach($charge_category as $charge_categorys)

                                <tr>
                                    <td colspan="5" class="background"><b>{{ $charge_categorys->category_name }}</b></td>
                                </tr>
                                @foreach($charges as $charge)
                                    @if($charge->category==$charge_categorys->id)
                                        <tr>
                                            <td colspan="5" class="border">{{ $charge->name }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endforeach
                            <tr>
                                <td colspan="4" class="background"><b>Quantity</b></td>
                                <td class="align background"><b>{{ $shipments->qty }}</b></td>
                            </tr>
                            <tr>
                                <td colspan="4" class="background"><b>Total</b></td>
                                <td class="align background"><b>${{ $shipments->price }}</b></td>
                            </tr>
                        </table>
                        <h3><font color="#28a7e9">Prep Service</font></h3>
                        <table width="95%">
                            <tr>
                                <th>Product</th>
                                <th>Prep Service</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Amount In USD</th>
                            </tr>
                            {{--*/ $prep_total=0 /*--}}
                            {{--*/$prep_sub_total=0/*--}}
                            @foreach($prep_quote as $prep_quotes)
                                @if($prep_quotes->id==$shipments->cus_id)

                                    <tr>
                                        <td class="border">{{ $prep_quotes->product_name }}</td>
                                        <td class="border">{{ $prep_quotes->service_name }}</td>
                                        <td class="align border">${{ $prep_quotes->per_qty_service_price }}</td>
                                        <td class="align border">{{ $prep_quotes->total }}</td>
                                        <td class="align border">${{ $prep_quotes->per_qty_service_price*$prep_quotes->total }}</td>
                                        {{--*/ $prep_sub_total+=$prep_quotes->per_qty_service_price*$prep_quotes->total /*--}}
                                    </tr>
                                @endif
                            @endforeach
                            {{--*/$prep_total+=$prep_sub_total/*--}}
                            <tr>
                                <td colspan="4" class="background"><b>Total</b></td>
                                <td class="align background"><b>${{ $prep_total }}</b></td>
                            </tr>
                        </table>
                        {{--*/$grand_total=$shipments->price+$prep_total/*--}}
                    </div>
                    <br>
                @endforeach
                <table width="95%">
                    <tr>
                        <td colspan="4" class="background"><b>Grand Total</b></td>
                        <td class="align background"><b>${{ $grand_total }}</b></td>
                    </tr>
                </table>
                <br>
                <h1 align="center">we look forward to helping you build, grow, and scale your e-commerce bussiness!</h1>
                <span align="center">If you have any questions about this quote, please call or email our order support team:</span><br>
                <span align="center">Phone: +1 866 526 3951     Email:orders@fbaforward.com</span><br>
                    <table width="95%">
                        <tr>
                            <td><a href="{{url('payment')}}" class="btn btn-success">Payment</a></td>
                            <td align="right"><a href="{{ url()->previous() }}" class="btn btn-primary">Back</a></td>
                        </tr>
                    </table>
            </div>
        </div>
            </div>
        </div>
    </section>
    @endsection
