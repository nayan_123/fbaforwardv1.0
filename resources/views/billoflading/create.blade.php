@extends('layouts.member.app')
@section('title', 'Bill Of Lading Upload')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
<style>
.col-md-12 {  padding-left: 35px;  }
.shead {  padding-right: 1px;  }
.item-title span, input { margin: 10px 0px !important; }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1>Bill Of Lading Upload</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('billoflading') }}"><i class="fa fa-bold"></i> Bill Of Lading</a></li>
            <li class="active"><a href="{{ url('billoflading/create') }}"> Bill Of Lading Upload</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Upload Bill Of Lading</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                {!! Form::open(['url' => 'billoflading', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div style="background-color: #003c6b; color:#ffffff; padding-left:3%; width: 99%">
                                <span class="heading">ORDER DETAILS</span>
                            </div>
                            <div style="width: 99%; padding-left:2%;">
                                <table>
                                    @foreach($order as $orders)
                                        <tr  class='trdetails' style="border-bottom: 0px;">
                                            <td colspan='6'>
                                                <span class='shead col-md-2'>Order Number </span><span class='sdetail col-md-10'>{{ $orders->order_no }}</span>
                                                <span class='shead col-md-2'>Product </span><span class='sdetail col-md-10'>{{ $orders->product_name }}</span><span class='col-md-12'>&nbsp;</span>
                                                <div class='col-md-12' style='padding-left: 0px'>
                                                    <span class='shead col-md-2'>Shipping Method </span><span class='sdetail col-md-3'>{{ $orders->shipping_name }}</span>
                                                    <span class='shead col-md-2'>SB Number </span><span class='sdetail col-md-3'>{{ $orders->sb_number}}</span>
                                                </div>
                                                @if($orders->bill_lading_id)

                                                <div class='col-md-12' style='padding-left: 0px'>
                                                    <span class='shead col-md-2' style='color: #8B0000; font-weight: bold;'>Revision Notes</span><span class='sdetail col-md-3'>{{ $orders->revision_note }}</span>
                                                </div>
                                                @endif
                                            </td>
                                        </tr>
                                        <input type="hidden" name="order_id" id="order_id" value="{{ $orders->order_id }}">
                                    @endforeach
                                </table>
                            </div>
                            <div style="background-color: #003c6b; color:#ffffff; padding-left:3%; margin-bottom:20px; width: 99%" id="">
                                <span class="heading">Bill Of Lading Upload</span>
                            </div>
                            <div class="form-group" style="padding-left: 2%" id="">
                                <div>
                                    <label for="" class="control-label col-md-12" style="text-align:left">Bill Of Lading</label>
                                </div>
                                <div>
                                   <div class="input-group col-md-12">
                                       <input type="file" name="bill" id="bill" class="validate[required] validate[checkFileType[jpg|jpeg|gif|JPG|png|PNG|docx|doc|pdf|pdfx]] no-padding">
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="text-align: right">
                        {!! Form::submit('  Submit Bill Of Lading  ', ['class'=>'btn btn-primary', ]) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
    @endsection
@section('js')
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script>
        $(document).ready(function () {
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach', {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
        });
    </script>
@endsection