@extends('layouts.member.app')
@section('title', 'Bill Of Lading')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-bold"></i> Bill Of Lading</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-bold"></i> Bill Of Lading</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <table id="bill_of_lading" class="table" >
                    <thead>
                    <tr>
                        <th class="col-md-2">Order Number</th>
                        <th class="col-md-3">Product</th>
                        <th class="col-md-2">Company</th>
                        <th class="col-md-2">Shipping Method</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-2">Enter New B/L</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($order as $orders)
                            <tr>
                                <td>{{ $orders->order_no }}</td>
                                <td>{{ $orders->product_name }}</td>
                                <td>{{ $orders->company_name }}</td>
                                <td>{{ $orders->shipping_name }}</td>
                                <td>@if($orders->bill_lading_id) Revision Requested @else New @endif</td>
                                <td><a class="button" href="{{ url('billoflading/create?id='.$orders->order_id) }}">Upload B/L</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @endsection
@section('js')
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('#bill_of_lading').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
    });
</script>
@endsection