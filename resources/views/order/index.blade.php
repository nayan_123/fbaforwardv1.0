@extends('layouts.member.app')
@section('title', $title)
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
<section class="content no-padding">
    <section class="content-header">
        <h1><i class="fa fa-shopping-cart"></i> {{$title}} </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-shopping-cart"></i>{{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive no-padding">
                    @if($user_role=='10')
                        <table id="inprogress_data" class="table" >
                            <thead>
                            <tr>
                                <th class="col-md-2">Order Number</th>
                                <th class="col-md-4">Product</th>
                                <th class="col-md-2">Company</th>
                                <th class="col-md-1">Status</th>
                                <th class="col-md-3">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($order))
                                @foreach($order as $orders)
                                    <tr>
                                        <td>{{ $orders['order_no'] }}</td>
                                        <td>{{ $orders['product_name'] }}</td>
                                        <td>{{ isset($orders['company_name']) ? $orders['company_name'] : null }}</td>
                                        <td>{{ $orderStatus[$orders['is_activated']] }}</td>
                                        <td><a href="{{ url('order/detail/'.$orders['shipping_method_id'].'/'.$orders['customer_requirement_id']) }}" class="button col-md-12">View</a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    @elseif($user_role=='8')
                        <table id="inprogress_data" class="table" >
                            <thead>
                            <tr>
                                <th class="col-md-2">Order Number</th>
                                <th class="col-md-4">Product</th>
                                <th class="col-md-2">Company</th>
                                <th class="col-md-1">Status</th>
                                <th class="col-md-3">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($order))
                                @foreach($order as $orders)
                                    <tr>
                                        <td>{{ $orders['order_no'] }}</td>
                                        <td>{{ $orders['product_name'] }}</td>
                                        <td>{{ isset($orders['company_name']) ? $orders['company_name'] : null }}</td>
                                        <td>{{ $orderStatus[$orders['is_activated']] }}</td>
                                        <td><a href="{{ url('order/detail/'.$orders['shipping_method_id'].'/'.$orders['customer_requirement_id']) }}" class="button col-md-12">View</a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    @else
                        <table id="inprogress_data" class="table" >
                            <thead>
                            <tr>
                                <th class="col-md-2">Order</th>
                                <th class="col-md-4">Product</th>
                                <th class="col-md-2">Date Ordered</th>
                                <th class="col-md-2">Status</th>
                                <th class="col-md-2">View Details</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($order))
                                @foreach($order as $orders)
                                    <tr>
                                        <td>{{ $orders['order_no'] }}</td>
                                        <td> @if(empty($orders['customer_requirement_details_product_name']))
                                                {{ $orders['product_name'] }}
                                            @else
                                                {{ $orders['customer_requirement_details_product_name'] }}
                                            @endif</td>
                                        <td>{{ date('m/d/Y', strtotime($orders['created_at'])) }}</td>
                                        <td>{{ $orderStatus[$orders['is_activated']] }}</td>
                                        <td>
                                            @if($user_role=='4' || $user_role=='3' || $user_role=='7' ||  $user_role=='6')
                                                <a href="{{ url('order/detail/'.$orders['shipping_method_id'].'/'.$orders['customer_requirement_id']) }}" class="button col-md-12">View</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    @endif
                    @if($user_role=='6')
                    <table id="past_order" class="table">
                        <thead>
                            <b><h4>Past Orders</h4></b>
                            <tr>
                                <th class="col-md-2">Order</th>
                                <th class="col-md-4">Product</th>
                                <th class="col-md-2">Date Ordered</th>
                                <th class="col-md-2">Status</th>
                                <th class="col-md-2">View Details</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($past_order as $orders)
                                <tr>
                                    <td>{{ $orders['order_no'] }}</td>
                                    <td>{{ $orders['product_name'] }}</td>
                                    <td>{{ date('m/d/Y', strtotime($orders['created_at'])) }}</td>
                                    <td>{{ $orderStatus[$orders['is_activated']] }}</td>
                                    <td>
                                        @if($user_role=='4' || $user_role=='3' || $user_role=='7' || $user_role=='8' || $user_role=='6')
                                        <a href="{{ url('order/detail/'.$orders['shipping_method_id'].'/'.$orders['customer_requirement_id']) }}" class="button col-md-12">View</a>
                                        @endif
                                    </td>
                                </tr>
                           @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
            </div>
        </div>
    </section>
</section>
@endsection

@section('js')
    <script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('#inprogress_data').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
        var user_role = "{{ $user_role }}";
        if((user_role == 3) || (user_role == 4)){
            var url = "{{ url('quote/create') }}";
            $('.dataTables_filter').append('<a href="'+url+'" class="btn btn-success">+ Request New Quote</a>');
        }
        $('#past_order').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });

    });
</script>
@endsection