@extends('layouts.member.app')
@section('title', 'Edit Order')
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    <style type="text/css">
        .col-md-12
        {
            padding-left: 2%;
        }
        .darkheading
        {
            background-color: #003c6b; color:#ffffff; padding-left:3%; margin-bottom: 1%; width: 99.5%;
        }
        .item-title span, input { margin: 10px 0px !important; }
        .my-checkbox{ position: absolute;margin-left: -15px !important;margin-top: 5px !important; }
        .control-label{ line-height: 3.4;text-align: center; }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <h1>Edit order </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('order') }}"><i class="fa fa-anchor"></i>Order</a></li>
            <li class="active"><a>Edit Order</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row" style="margin-left:-10px;">
                    <div class="darkheading">
                        <span class="heading">EDIT ORDER</span>
                    </div>
                <form method="post" action="{{ url('order/update') }}">
                    {{ csrf_field() }}
                    
                    <input type="hidden" name="customer_requirement_id" value="{{ $customer_requirement_id }}">
                    <input type="hidden" name="method_id" value="{{ $method_id }}">
                    <div class="col-md-12">
                        <h4><b>Pre-Shipment</b></h4>
                        @if(count($pre_shipment) != 0)
                        @foreach($pre_shipment as $pre_shipments)
                            <div class="form-group" style="margin-bottom: 3%">
                                <h4>{{ $pre_shipments->name }}</h4>
                                <div class="col-md-2 no-padding">
                                    <label for="product" class="control-label col-md-12">Rate</label>
                                </div>
                                <div class="col-md-10 no-padding">
                                    <div class="input-group col-md-12">
                                        <input type="text" name="{{ $pre_shipments->name }}" class="form-control" value="{{ $pre_shipments->rate }}">
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @else
                            <div class="form-group" style="margin-bottom: 3%">
                                <h4>Factory Audit</h4>
                                <div class="col-md-2 no-padding">
                                    <label for="product" class="control-label col-md-12">Rate</label>
                                </div>
                                <div class="col-md-10 no-padding">
                                    <div class="input-group col-md-12">
                                        <input type="text" name="factory_audit" class="form-control" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 3%">
                                <h4>Pre-Inspection</h4>
                                <div class="col-md-2 no-padding">
                                    <label for="product" class="control-label col-md-12">Rate</label>
                                </div>
                                <div class="col-md-10 no-padding">
                                    <div class="input-group col-md-12">
                                        <input type="text" name="pre_inspection" class="form-control" value="">
                                    </div>
                                </div>
                                
                            </div>
                    </div>
                    @endif

                    <div class="col-md-12">
                        <div class="form-group" style="margin-bottom: 3%">
                            <h4>Pre-Inspection</h4>
                            @foreach($prep_service as $prep_services)
                                <div class="col-md-6"><input type="checkbox" name="prep_service[]" id="prep_service{{ $prep_services->prep_service_id }}" @if(!empty($prep_detail)) @if(in_array($prep_services->prep_service_id,$prep_detail)){{ "checked" }} @endif @endif value="{{ $prep_services->prep_service_id }}"> {{ $prep_services->service_name }}</div>
                            @endforeach
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group" style="margin-bottom: 3%">
                            <input type="submit" class="button pull-right" name="update" value="Update">
                        </div>
                    </div>

                </form>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            // Validation Engine init
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach',
                {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
        });
    </script>
@endsection
