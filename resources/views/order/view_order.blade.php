@extends('layouts.member.app')
@section('title', 'Order Details')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/custom_quotes.css') !!}
@endsection
@section('content')
<style>
.content{  min-height: 620px;  }
.box-size {  width: 20% !important;  }
.nav-tabs>li {  margin-bottom: 0px;  }
.icons { font-size: 25px !important;color: #0088cc;cursor: pointer;}
.nav-tabs{ border: none; }
.box2{ border-top:none; }
.item-title span, input { margin: 10px 0px !important; }
a{ color:#555; }
.icon-link:hover{ color:#555 !important;cursor: pointer; }
.place-date{    width: 21.66% !important;  border-bottom: 1px solid #c8c8c8;  }
.height51{ min-height: 51px; }
.place-date p{ line-height: 2.3; }
.status-box{ width:30% !important; }
.customer-status{ width:40% !important; }
.height77{ min-height: 77px; }
@media (min-width: 1650px) { .route{ padding-left: 5em; }  }
</style>
<section class="content">
    <section class="content-header">
        <h1> Order Details</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-shopping-cart"></i>Order Details</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
        <div class="col-md-12 no-padding">
            <div class="col-md-12 box2 no-padding">
                    <div class="box-body no-padding">
                        <div class="">
                            <div class="col-md-12 no-padding ">
                                <div class="col-md-3 box-size blue-bg product-data @if($user_role == 4) height77 @else height51 @endif text-overflow" style="padding-top: 12px;">
                                    <b>Order</b> #{{ $order->order_no }}
                                </div>
                                <div class="col-md-3 place-date text-left @if($user_role == 4) height77 @else height51 @endif">
                                    <p style="padding-top: 8px;">
                                    @if($order->created_at) 
                                        Placed on {{ \Carbon\Carbon::parse($order->created_at)->format('M d,Y') }}
                                    @endif
                                    </p>
                                </div>
                                @if($user_role == 3)
                                <div class="col-md-7 product-data text-right">
                                    <div class="col-md-9 padding-right-0">
                                        <b class="col-md-2 no-padding">STATUS:</b>
                                        <p class="green"><b>{{ $orderStatus[$order->order_status] }}</b></p>
                                        @if($order->hold > 0)
                                            <p class="red"><b>Hold</b></p>
                                        @endif
                                    </div>
                                    <div class="col-md-3">
                                        <button data-toggle="modal" data-target="#learnMoreModal" class="btn-load button">Learn More</button>
                                    </div>
                                </div>
                                @elseif($user_role == 4)
                                <div class="col-md-7 product-data text-right height51">
                                    <div class="col-md-3 text-center">
                                        <a href="{{ url('customer') }}" class="icon-link">
                                            <i class="fa fa-users icons"></i>
                                            <p>View Company</p>
                                        </a>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <a href="{{ url('inventory') }}" class="icon-link">
                                            <i class="fa fa-barcode icons"></i>
                                            <p>View Inventory</p>
                                        </a>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <a data-toggle="modal" data-target="#addNotesModal" class="icon-link">
                                            <i class="fa fa-file-text-o icons" ></i>
                                            <p>Add Order Note</p>
                                        </a>
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <a href="https://accounts.zoho.com" class="icon-link" target="_blank">
                                            <i class="fa fa-users icons"></i>
                                            <p>GO to CRM</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-12 no-padding">
                                    <div class="col-md-4 customer-status product-data">
                                        <p>Customer Status : </p>
                                    </div>
                                    <div class="col-md-4 status-box product-data">
                                        <p>Logistics Status : </p>
                                    </div>
                                    <div class="col-md-4 status-box product-data">
                                        <p>Warehouse Status : </p>
                                    </div>
                                </div>
                                <div class="col-md-12 blank-class"></div>
                                @else
                                    <div class="col-md-7 product-data height51 text-right"></div>
                                @endif
                            </div>
                            <div class="col-md-12 no-padding">
                                <div class="col-md-3 box-size product-data text-overflow">
                                    <b>Product : </b>
                                        @if(empty($order->customer_requirement_details_product_name))
                                            @if(!empty($order->product_nick_name)) 
                                                {{ $order->product_nick_name }} 
                                            @else 
                                                {{ $order->product_name }}
                                            @endif
                                        @else
                                            {{ $order->customer_requirement_details_product_name }}
                                        @endif
                                </div>
                                <div class="col-md-3 box-size product-data">
                                    <b> Units: </b>{{ $order->total }}
                                </div>
                                <div class="col-md-3 box-size product-data">
                                    <b>Cartons : </b>{{ $order->no_boxs }}
                                </div>
                                <div class="col-md-3 box-size product-data">
                                    <b>Weight : </b>{{ $order->chargeable_weight }}
                                </div>
                                <div class="col-md-3 box-size product-data">
                                    <b>Volume : </b>{{ $order->shipment_volume }}
                                </div>
                                <div class="col-md-3 box-size product-data height61">
                                    <b>Inbound Shipping Method :  </b>{{ $shipping_method->shipping_name }}
                                </div>
                                <div class="col-md-3 box-size product-data height61">
                                    <b>Type: </b>{{ $order->shipment_type_short_name }}
                                </div>
                                <div class="col-md-3 box-size product-data height61">
                                    <b>Incoterms : </b>{{ $order->incoterm_short_name }}
                                </div>
                                <div class="col-md-3 box-size product-data height61">
                                    @if($order->order_type=='1')
                                    <b> Transit Time :</b>
                                        @if($order->prealert_id)
                                            <?php
                                            $date1 = strtotime($order->ETD_china);
                                            $date2 = strtotime($order->ETA_US);
                                            $datediff = $date2 - $date1;
                                            if(($shipping_method->shipping_name == 'OCEAN FREIGHT') || ($shipping_method->shipping_name == 'AIR FREIGHT')){
                                                echo '<br>Port to port: '.floor($datediff / (60 * 60 * 24)). 'Days';
                                            }
                                            else if($shipping_method->shipping_name == 'AIR EXPRESS'){
                                                echo '<br>Origin to FBAforward: '.floor($datediff / (60 * 60 * 24)). 'Days';
                                            }
                                            ?>
                                        @else
                                            {{ "TBD" }}
                                        @endif
                                    @else
                                        <b>Expected Delivery : </b> {{ date('m/d/Y',strtotime($order->date_of_arrival_at_fbaforward)) }}
                                    @endif
                                </div>
                                <div class="col-md-3 box-size product-data height61">
                                    <b>Outbound Shipping Method : </b> {{ $order->outbound_method_short_name }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 blank-class"></div>
                <div class="col-md-12 no-padding">
                    <div class="box2 col-md-12 invoice-summery-box no-padding">
                        <div class="col-md-12">
                            <h4 class="box-title">Route</h4>
                        </div>
                        <div class="col-md-12 no-padding">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs">
                                <li class="col-md-3 text-center box-size route-icon">
                                    <p class="round-tab">
                                        <i class="fa fa-university"></i>
                                    </p>
                                </li>
                                <li class="col-md-3 text-center box-size route-icon">
                                    <p class="round-tab">
                                        <i class="fa fa-anchor"></i>
                                    </p>
                                </li>
                                <li class="col-md-3 text-center box-size route-icon">
                                    <p class="round-tab">
                                        <i class="fa fa-anchor"></i>
                                    </p>
                                </li>
                                <li class="col-md-3 text-center box-size route-icon">
                                    <p class="round-tab">
                                        <i class="fa fa-circle"></i>
                                    </p>
                                </li>
                                <li class="col-md-3 text-center box-size route-icon">
                                    <p class="round-tab">
                                        <i class="fa fa-amazon"></i>
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3 box-size route padding-left-70 padding-right-0">
                            <b><p>ORIGIN</p></b>
                            <p>Supplier's Name: {{ $order->contact_name }}</p>
                            <p>{{ $order->port_of_origin }}</p>
                            <small><b>Cargo Ready Date:</b> {{ \Carbon\Carbon::parse($order->goods_ready_date)->format('M d,Y') }}</small><br>
                            <small><b>Pick Up Date:</b>
                                @if($order->incoterm_id=='2')
                                    {{ date('M d, Y',strtotime($order->ETA_US)) }}
                                @endif
                            </small><br>
                        </div>
                        <div class="col-md-3 box-size padding-left-40 padding-right-0 route">
                            <b><p>PORT DEPARTURE</p></b>
                            <p>{{ $order->shipper_destination }}</p>
                            <small><b>Arrival Date:</b> @if($order->prealert_id) {{ \Carbon\Carbon::parse($order->ETA_US)->format('M d,Y') }} @endif</small><br>
                            <small><b>Departure Date:</b> @if($order->prealert_id) {{ \Carbon\Carbon::parse($order->ETD_china)->format('M d,Y') }} @endif</small><br>
                            @if($order->shipping_method_id=='3')
                                <a href="https://wwwapps.ups.com/WebTracking/track?track=yes&amp;trackNums=&quot;TRACKINGNUMBERGOESHERE" class="light-blue-link"><small>Track Shipment</small></a>
                            @endif
                        </div>
                        <div class="col-md-3 box-size padding-left-40 padding-right-0  route">
                            <b><p>PORT ARRIVAL</p></b>
                            <p>{{ $order->shipper_destination }}</p>
                            <small> <b>Arrival Date:</b> @if($order->prealert_id) {{ \Carbon\Carbon::parse($order->ETA_US)->format('M d,Y') }} @endif</small><br>
                            <small> <b>Customs Release Date:</b> @if($order->delivery_id) {{ date('M d, Y',strtotime($order->custom_release_date)) }} @endif</small><br>
                            <small> <b>Cargo Release Date:</b> @if($order->delivery_id) {{ date('M d, Y',strtotime($order->cargo_release)) }} @endif</small><br>
                            <small> <b>Pick Up Date:</b> @if($order->delivery_id) {{ date('M d, Y',strtotime($order->ETA_to_warehouse)) }} @endif</small>
                        </div>
                        <div class="col-md-3 box-size padding-left-40 padding-right-0  route">
                            <b><p>FBAFORWARD</p></b>
                            <p>San Diego, CA</p>
                            <p>Ningbo, China</p>
                            <small>Checked In: @if($order->checkin_id) {{ date('M d, Y',strtotime($order->checkin_date)) }} @endif</small><br>
                            <small>Status: Prep Completed</small><br>
                            <small><b>Departure Date:</b>  </small><br>
                            <a class="light-blue-link" href="{{ url('inventory') }}"><small>View Inventory</small></a>
                        </div>
                        <div class="col-md-3 box-size padding20 route">
                            <b><p>AMAZON</p></b>
                            <p>{{$order->destination_name}}</p>
                            <small><b>ETA</b>: </small><br>
                            <small>Checked In: </small>
                        </div>
                    </div>
                </div>
        </div>

        <div class="col-md-12 no-padding">
            @if(($user_role=='3') || ($user_role == '4') || ($user_role == '8'))
                <div class="col-md-9 no-padding">
            @else
                <div class="col-md-12 no-padding">
            @endif
                <div class="col-md-12 no-padding">
                    <div class="box-body no-padding">
                        <div class="">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 no-padding">
                    <div class="box2 col-md-12 invoice-summery-box no-padding">
                        <div class="box-header with-border invoice-summery-heading">
                            <h3 class="box-title">Order Summary</h3> &nbsp;&nbsp;&nbsp;@if(($user_role=='3') || ($user_role == '4'))<a class="light-blue-link" href="{{ url('order/edit-order'.'/'.$method_id.'/'.$customer_requirement_id) }}">Edit Order</a>@endif<div class="pull-right"><i class="fa fa-print" id="print"></i> &nbsp;&nbsp;&nbsp; <a target="_blank" href="{{ url('order/print-order/'.$method_id.'/'.$customer_requirement_id.'/save') }}"><i class="fa fa-save"></i></a> </div>
                        </div>
                        <div class="col-md-12">
                            {{--*/ $charge_qty = 1 /*--}} {{--*/ $shipping_sub_total = 0 /*--}} {{--*/ $prep_price = "" /*--}} {{--*/ $prep_sub_total = 0 /*--}} {{--*/ $trucking_qty="" /*--}} {{--*/ $port_fee = 0 /*--}} {{--*/ $port_fee_name =  '' /*--}}  {{--*/ $trucking_name =  env('TRUCKING_NAME') /*--}} {{--*/ $isf_rate =  env('ISF_RATE') /*--}} {{--*/ $isf_name =  env('ISF_NAME') /*--}} {{--*/ $entry_rate = env('ENTRY_RATE') /*--}} {{--*/ $entry_name = env('ENTRY_NAME') /*--}} {{--*/ $bond_rate =  env('BOND_RATE') /*--}} {{--*/ $bond_name =  env('BOND_NAME') /*--}} {{--*/ $container_unloading= env('CONTAINER_UNLOADING') /*--}} {{--*/ $palletizing= env('PALLETIZING') /*--}} {{--*/ $amazon_approve_pallet= env('AMAZON_APPROVE_PALLET') /*--}} {{--*/ $pallet_charge= env('pallet_charge') /*--}} {{--*/ $receive_forward_sub_total= '' /*--}} {{--*/ $custom_sub_total= '' /*--}} {{--*/ $listing_service_sub_total= '' /*--}} {{--*/ $photo_services_sub_total= '' /*--}} {{--*/ $list_optimization_sub_total= 0 /*--}} {{--*/ $pre_shipment_sub_total= '' /*--}} {{--*/ $factory_audit_total= '' /*--}} {{--*/ $pre_inspection_total= '' /*--}} {{--*/ $order_total= '' /*--}} {{--*/ $processing_fee= 0 /*--}}
                            <table class="table quote-summery-table">
                                <thead>
                                <tr>
                                    <th class="col-md-6">Pre-Shipment</th>
                                    <th class="col-md-2">Rate</th>
                                    <th class="col-md-2">Qty</th>
                                    <th class="col-md-2">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pre_shipment as $pre_shipments)
                                <tr>
                                    <td class="col-md-6">{{ $pre_shipments->name }}</td>
                                    <td class="col-md-2">${{ $pre_shipments->rate }}</td>
                                    <td class="col-md-2">{{ $order->no_boxs }}</td>
                                    <td class="col-md-2">${{ ($order->no_boxs * $pre_shipments->rate) }}</td>
                                </tr>
                                {{--*/ $pre_shipment_sub_total += ($order->no_boxs * $pre_shipments->rate) /*--}}
                                @endforeach
                                <tr>
                                    <td><b>Deposit Paid</b></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>$ {{ $pre_shipment_sub_total }}</td></b></td>
                                </tr>
                                </tbody>
                            </table>
                            @if($order->order_type == 1)
                                @if($order->shipment_type_id==1)
                                    @if($order->units==1)
                                        {{--*/$trucking_qty=round($order->shipment_volume / env('PALLET_CBM'))/*--}}
                                    @else
                                        {{--*/$trucking_qty=round($order->shipment_volume / env('PALLET_CFT'))/*--}}
                                    @endif
                                    {{--*/ $trucking_price = $trucking_qty*env('TRUCKING_PRICE')  /*--}}
                                @else
                                    {{--*/ $trucking_qty=1 /*--}}
                                    {{--*/ $trucking_price = $order->trucking_rate /*--}}
                                @endif
                                <table class="table quote-summery-table">
                                    <thead>
                                    <tr>
                                        <th>Shipping</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="col-md-6">{{$order->shipping_name}}</td>
                                        <td class="col-md-2">${{$order->shipment_type_rate}}</td>
                                        <td class="col-md-2">{{$order->shipment_volume}}</td>
                                        <td class="col-md-2">${{$order->shipment_type_rate*$order->shipment_volume}}</td>
                                    </tr>
                                    {{--*/ $shipping_sub_total += ($order->shipment_type_rate*$order->shipment_volume) /*--}}
                                    @foreach($detail_charge as  $detail_charges)
                                        <?php
                                        $charge_price = $detail_charges->charge * $charge_qty;
                                        $shipping_sub_total += $charge_price;
                                        ?>
                                        <tr>
                                            <td class="col-md-6">{{ $detail_charges->name }}</td>
                                            <td class="col-md-2">${{ $detail_charges->charge }}</td>
                                            <td class="col-md-2">{{ $charge_qty }}</td>
                                            <td class="col-md-2">${{ $charge_price }}</td>
                                        </tr>
                                    @endforeach
                                    @if($order->shipping_name!=="undefined")
                                        @if($order->shipping_method_id  == 2)
                                            {{--*/  $port_fee= env('PORT_FEE')/*--}}
                                            {{--*/ $port_fee_name = env('PORT_FEE_NAME')/*--}}
                                            <tr>
                                                <td class="col-md-6">{{$port_fee_name}}</td>
                                                <td class="col-md-2">${{$port_fee}}</td>
                                                <td class="col-md-2">{{$charge_qty}}</td>
                                                <td class="col-md-2">${{$port_fee*$charge_qty}}</td>
                                            </tr>
                                            <tr>
                                                <td class="col-md-6"> {{$trucking_name}}</td>
                                                <td class="col-md-2">${{$trucking_price}}</td>
                                                <td class="col-md-2">{{$trucking_qty}}</td>
                                                <td class="col-md-2">${{$trucking_price}}</td>
                                            </tr>
                                        @elseif($order->shipping_method_id  == 1)
                                            @if($order->shipment_type_id==1)
                                                {{--*/ $port_fee= env('OCEAN_LCL_PORT_FEE') /*--}}
                                                {{--*/ $port_fee_name = env('LCL_PORT_FEE_NAME')/*--}}
                                            @else
                                                {{--*/ $port_fee= env('OCEAN_PORT_FEE') /*--}}
                                                {{--*/ $port_fee_name = env('PORT_FEE_NAME')/*--}}
                                            @endif
                                            <tr>
                                                <td class="col-md-6">{{$port_fee_name}}</td>
                                                <td class="col-md-2">${{$port_fee}}</td>
                                                <td class="col-md-2">{{$charge_qty}}</td>
                                                <td class="col-md-2">${{$port_fee*$charge_qty}}</td>
                                            </tr>
                                            <tr>
                                                <td class="col-md-6"> {{$trucking_name}}</td>
                                                <td class="col-md-2">${{$trucking_price}}</td>
                                                <td class="col-md-2">{{$trucking_qty}}</td>
                                                <td class="col-md-2">${{$trucking_price}}</td>
                                            </tr>
                                        @else
                                            {{--*/  $port_fee= env('PORT_FEE')/*--}}
                                            {{--*/ $port_fee_name = env('PORT_FEE_NAME')/*--}}
                                            <tr>
                                                <td class="col-md-6">{{$port_fee_name}}</td>
                                                <td class="col-md-2">${{$port_fee}}</td>
                                                <td class="col-md-2">{{$charge_qty}}</td>
                                                <td class="col-md-2">${{$port_fee*$charge_qty}}</td>
                                            </tr>
                                            {{--*/ $trucking_price = 0 /*--}}
                                        @endif
                                        {{--*/$shipping_sub_total+=$port_fee*$charge_qty/*--}}
                                        {{--*/$shipping_sub_total+=$trucking_price/*--}}
                                    @endif
                                    <tr>
                                        <td class="col-md-6">{{ env('WIRE_TRANS_NAME') }}</td>
                                        <td class="col-md-2">${{ env('WIRE_TRANS_FEE') }}</td>
                                        <td class="col-md-2">&nbsp;</td>
                                        <td class="col-md-2">${{ env('WIRE_TRANS_FEE') }}</td>
                                    </tr>
                                    {{--*/ $shipping_sub_total +=env('WIRE_TRANS_FEE') /*--}}
                                    <tr>
                                        <td><b>Deposit Paid</b></td>
                                        <td></td>
                                        <td></td>
                                        <td><b>${{ $shipping_sub_total }}</b></td>
                                    </tr>

                                    </tbody>
                                </table>
                            @endif
                            @if(($order->order_type == 1) || ($order->order_type == 3))
                                <table class="table quote-summery-table">
                                    <thead>
                                    <tr>
                                        <th class="col-md-6">Custom</th>
                                        <th class="col-md-2">&nbsp;</th>
                                        <th class="col-md-2">&nbsp;</th>
                                        <th class="col-md-2">&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($order->shipping_method_id == 1)
                                        <tr>
                                            <td class="col-md-6">{{$isf_name}}</td>
                                            <td class="col-md-2">${{$isf_rate}}</td>
                                            <td class="col-md-2">{{$charge_qty}}</td>
                                            <td class="col-md-2">${{$isf_rate * $charge_qty}}</td>
                                        </tr>
                                        {{--*/$custom_sub_total= ($isf_rate*$charge_qty) /*--}}
                                    @endif
                                    <tr>
                                        <td class="col-md-6">{{$entry_name}}</td>
                                        <td class="col-md-2">${{$entry_rate}}</td>
                                        <td class="col-md-2">{{$charge_qty}}</td>
                                        <td class="col-md-2">${{ $entry_rate*$charge_qty}}</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6">{{$bond_name}}</td>
                                        <td class="col-md-2">${{$bond_rate}}</td>
                                        <td class="col-md-2">{{$charge_qty}}</td>
                                        <td class="col-md-2">${{$bond_rate*$charge_qty}}</td>
                                    {{--*/ $custom_sub_total+= (int)($entry_rate*$charge_qty) + ($bond_rate*$charge_qty)/*--}}
                                    <tr>
                                    <tr>
                                        <td colspan="3"><b>Deposit Paid</b></td>
                                        <td><b>${{$custom_sub_total}}</b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            @endif

                            <table class="table quote-summery-table">
                                <thead>
                                <tr>
                                    <th>Receiving & Forwarding</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($order->shipment_type_id >1)
                                    <tr>
                                        <td class="col-md-6">container_unloading</td>
                                        <td class="col-md-2">${{$order->container_unload_rate}}</td>
                                        <td class="col-md-2">{{$charge_qty}}</td>
                                        <td class="col-md-2">${{$order->container_unload_rate*$charge_qty}}</td>
                                    </tr>
                                @endif
                                {{--*/$receive_forward_sub_total+=$order->container_unload_rate*$charge_qty/*--}}
                                @if($order->outbound_method_id >1)
                                    <tr>
                                        <td class="col-md-6">{{$palletizing}}</td>
                                        <td class="col-md-2">${{$pallet_charge}}</td>
                                        <td class="col-md-2">{{$trucking_qty}}</td>
                                        <td class="col-md-2">${{$pallet_charge*$trucking_qty}}</td>
                                    </tr>
                                    {{--*/$receive_forward_sub_total+=$pallet_charge*$trucking_qty/*--}}
                                    <tr>
                                        <td class="col-md-6">{{$amazon_approve_pallet}}</td>
                                        <td class="col-md-2">${{$pallet_charge}}</td>
                                        <td class="col-md-2">{{$trucking_qty}}</td>
                                        <td class="col-md-2">${{$pallet_charge*$trucking_qty}}</td>
                                    </tr>
                                    {{--*/$receive_forward_sub_total+=$pallet_charge*$trucking_qty/*--}}
                                @endif
                                <tr>
                                    <td class="col-md-6">{{$order->outbound_name}}</td>
                                    <td class="col-md-2">&nbsp;</td>
                                    <td class="col-md-2">&nbsp;</td>
                                    <td class="col-md-2"><i>varies</i></td>
                                </tr>
                                <tr>
                                    <td class="col-md-6"><b>Deposit Paid</b></td>
                                    <td class="col-md-2"></td>
                                    <td class="col-md-2"></td>
                                    <td class="col-md-2"><b>${{ $receive_forward_sub_total }}</b></td>
                                </tr>
                                </tbody>
                            </table>

                            <table class="table quote-summery-table">
                                <thead>
                                <tr>
                                    <th>Prep & Inspection</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($detail_prep as $prep_key=>$prep_value )
                                    {{--*/$detail_prep = $prep_value->price * $order->qty_per_box/*--}}
                                    @if($detail_prep % 1 !=0)
                                        <tr>
                                            <td class="col-md-6">{{$prep_value->service_name}}</td>
                                            <td class="col-md-2">${{$prep_value->price}}</td>
                                            <td class="col-md-2">{{$order->qty_per_box}}</td>
                                            <td class="col-md-2">${{$detail_prep}}</td>
                                            {{--*/$prep_sub_total+=$detail_prep/*--}}
                                        </tr>
                                    @else
                                        <tr>
                                            <td class="col-md-6">{{$prep_value->service_name}}</td>
                                            <td class="col-md-2">${{$prep_value->price}}</td>
                                            <td class="col-md-2">{{$order->qty_per_box}}</td>
                                            <td class="col-md-2">${{$detail_prep}}</td>
                                            {{--*/$prep_sub_total+=$detail_prep/*--}}
                                        </tr>
                                    @endif
                                @endforeach
                                <tr>
                                    <td class="col-md-6"><b>Deposit Paid</b></td>
                                    <td class="col-md-2"></td>
                                    <td class="col-md-2"></td>
                                    <td class="col-md-2"><b>${{ $prep_sub_total }}</b></td>
                                </tr>
                                </tbody>
                            </table>



                           
                            <table class="table quote-summery-table">
                                  <thead>
                                    <tr>
                                        <th class="col-md-6">Additional Fees</th>
                                        <th class="col-md-2 text-right"></th>
                                        <th class="col-md-2 text-right"></th>
                                        <th class="col-md-2 text-right"></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                   {{-- <tr>
                                        <td class="col-md-6">Shipping: Additional Port Fees</td>
                                        <td class="col-md-2">1</td>
                                        <td class="col-md-2">$150</td>
                                        <td class="col-md-2">$150</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6">Customs: Customs Duties</td>
                                        <td class="col-md-2">1</td>
                                        <td class="col-md-2">$150</td>
                                        <td class="col-md-2">$150</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6">Prep & Inspection: Suffocaton Labels</td>
                                        <td class="col-md-2">1</td>
                                        <td class="col-md-2">$150</td>
                                        <td class="col-md-2">$150</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6">Prep & Inspection: Poly Bagging (Refund)</td>
                                        <td class="col-md-2">1</td>
                                        <td class="col-md-2">$150</td>
                                        <td class="col-md-2">$150</td>
                                    </tr> --}}
                                    <tr class="bg-total">
                                        <td class="col-md-6">Total To Be Invoiced</td>
                                        <td class="col-md-2"></td>
                                        <td class="col-md-2"></td>
                                        <td class="col-md-2">$0</td>
                                    </tr>
                                  </tbody>
                            </table>
                        </div>

                        <div class="col-sm-6"></div>
                        <div class="col-sm-6 text-right">
                            @if(($user_role=='3') || ($user_role == '4'))
                                <div class="col-sm-6">
                                    <p>Invoice Ready</p>
                                    <p><b>Due </b></p>
                                </div>
                                <div class="col-sm-6 no-padding">
                                    <button class="button">Pay</button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @if(($user_role=='3') || ($user_role == '4'))
            <div class="box2 order-payment-history-box no-padding col-md-3">
                <div class="white-bg box-header order-payment-history-heading with-border">
                    <h3 class="box-title">REQUIRED DOCUMENTS</h3>
                </div>
                <div class="col-md-12 no-border">
                    @foreach($documents as $document)
                        <div class="col-md-12 required-documents no-padding">
                            <div class="col-md-1 no-padding text-center">
                                @if($document->document_id)
                                        @if($document->status == 1)
                                            <i class="green fa fa-check"></i>
                                        @elseif($document->status == 0)
                                            <i class="yellow fa fa-hourglass-half"></i>
                                        @elseif($document->status == 3)
                                            <i class="yellow fa fa-exclamation"></i>
                                        @endif
                                @else
                                    <i class="red fa fa-close"></i>
                                @endif
                            </div>
                            <div class="col-md-10 no-padding">
                                <span>&nbsp;&nbsp;{{ $document->document_name }}<small>&nbsp;&nbsp;@if($document->due_date) Due on {{ \Carbon\Carbon::parse($document->due_date)->format('m/d') }} @endif</small>
                                </span>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-md-12 margintop20">
                        <button class="btn btn-primary btn-reject-quote col-md-12" data-toggle="modal" data-target="#requiredDocumentModal">SUBMIT DOCUMENTS</button>
                    </div>
                </div>
            </div>
            @endif
            @if(($user_role=='3') || ($user_role == '4') || ($user_role == '8'))
                @if($user_role =='8')
                    <div class="box2 order-payment-history-box no-padding col-md-3">
                            <div class="white-bg box-header order-payment-history-heading with-border">
                                <h3 class="box-title">HOLDS</h3> <a class="pull-right" onclick="viewwarehold()"> View all</a>
                            </div>
                            <div class="col-md-12 no-border">
                                @foreach($warehouse_hold_detail as $warehouse_hold_details)
                                    @if($warehouse_hold_details['status']=='0')
                                        <div class="col-md-12 required-documents no-padding">
                                            <i class="yellow fa fa-hourglass-half"><span> {{ $warehouse_hold_details['name'] }} <small>&nbsp;&nbsp;</small></span></i>
                                        </div>
                                    @elseif($warehouse_hold_details['status']=='1')
                                        <div class="col-md-12 required-documents no-padding">
                                            <i class="green fa fa-check"><span> {{ $warehouse_hold_details['name'] }} <small>&nbsp;Approved&nbsp;</small></span></i>
                                        </div>
                                    @elseif($warehouse_hold_details['status']=='2')
                                        <div class="col-md-12 required-documents no-padding">
                                            <i class="red fa fa-close"><span> {{ $warehouse_hold_details['name'] }} <small>&nbsp;Rejected&nbsp;</small></span></i>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="col-md-12 margintop20">
                                        <button class="btn btn-primary btn-reject-quote col-md-12" onclick="submithold()">SUBMIT HOLD</button>
                                </div>
                            </div>
                    </div>
                @endif
            @endif
            @if(($user_role=='3') || ($user_role == '4'))
             <div class="box2 order-payment-history-box no-padding col-md-3">
                <div class="white-bg box-header order-payment-history-heading with-border">
                    <h3 class="box-title">ORDER ALERTS</h3><a class="pull-right">View all</a>
                </div>
                <div class="col-md-12 no-border">
                    <div class="col-md-12 required-documents no-padding">
                       <i class="yellow fa fa-exclamation"><span>&nbsp;&nbsp;Customs Exam, 1/20/18<br>
                        <small>No actions need to be taken at this time. You will be notified when the exam is complete.</small></span></i>
                    </div>
                    <div class="col-md-12 required-documents no-padding">
                       <i class="red fa fa-close"><span>&nbsp;&nbsp;Customs Documents Requested, 1/20/18<br><small>Submit documents using the button above.Your shipment will be delayed if documents are not submitted</small></span></i>
                    </div>
                    <div class="col-md-12 margintop20">
                    </div>
                </div>
            </div>
                @if($order->order_type=='1')
                    <div class="box2 order-payment-history-box no-padding col-md-3">
                        <div class="white-bg box-header order-payment-history-heading with-border">
                            <h3 class="box-title">UPLOAD TRACKING NUMBER</h3>
                        </div>

                        <div class="col-md-12 no-border">
                            <div class="col-md-12 padding20 margintop10">
                                <button class="btn btn-primary btn-reject-quote col-md-12" data-toggle="modal" data-target="#uploadTrackingNumber">Upload Tracking Number</button>
                            </div>
                        </div>
                    </div>
                @endif
            <div class="box2 order-payment-history-box no-padding col-md-3">
                <div class="white-bg box-header order-payment-history-heading with-border">
                    <h3 class="box-title">PENDING APPROVAL</h3>
                </div>
                <div class="col-md-12 no-border">
                    <div class="col-md-12 margintop20">
                        <button class="btn btn-primary btn-reject-quote col-md-12" data-toggle="modal" data-target="#pendingapproveModal">View</button>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="col-md-12 no-padding">
            <div class="box2 col-md-12 invoice-summery-box no-padding">
                <div class="box-header with-border invoice-summery-heading">
                    <h3 class="box-title col-md-6 no-padding">Order History</h3>
                    <div class="pull-right text-right col-md-6"><span><a id="previous"><i class="fa fa-caret-left"></i> Previous Link</a> &nbsp;&nbsp; <span id="entries"></span> &nbsp;&nbsp; <a id="next">Next Page <i class="fa fa-caret-right"></i></span></a>
                    </div>
                </div>
                 <table id="order_history" class="table quote-summery-table">
                              <thead>
                                <tr>
                                    <th class="col-md-4">DATE</th>
                                    <th class="col-md-4">UPDATE</th>
                                    <th class="col-md-4">DETAILS</th>
                                </tr>
                              </thead>
                            <tbody>
                            @foreach($order_histories as $order_history)
                                <tr>
                                    <td class="col-md-4">{{ \Carbon\Carbon::parse($order_history->created_at)->format('M d,Y') }}</td>
                                    <td class="col-md-4">
                                    @if($order_history->order_history_status == 1)

                                    @elseif($order_history->order_history_status == 2)

                                    @elseif($order_history->order_history_status == 3)

                                    @elseif($order_history->order_history_status == 4)
                                        {{ "Shipment Booked" }}
                                    @elseif($order_history->order_history_status == 5)

                                    @elseif($order_history->order_history_status == 6)

                                    @elseif($order_history->order_history_status == 7)

                                    @elseif($order_history->order_history_status == 8)

                                    @elseif($order_history->order_history_status == 9)

                                    @elseif($order_history->order_history_status == 10)
                                        {{ "Shipment Departed" }}
                                    @elseif($order_history->order_history_status == 11)
                                        {{ "Shipment Arrived at Port" }}
                                    @elseif($order_history->order_history_status == 12)

                                    @elseif($order_history->order_history_status == 13)

                                    @endif
                                </td>
                                <td class="col-md-4">
                                    @if($order_history->order_history_status == 1)

                                    @elseif($order_history->order_history_status == 2)

                                    @elseif($order_history->order_history_status == 3)

                                    @elseif($order_history->order_history_status == 4)
                                        <?php echo $order_history->ETD_china.' '.\Carbon\carbon::parse($order_history->prealert_details_created_at)->format('M d,Y'); ?>
                                    @elseif($order_history->order_history_status == 5)

                                    @elseif($order_history->order_history_status == 6)

                                    @elseif($order_history->order_history_status == 7)

                                    @elseif($order_history->order_history_status == 8)

                                    @elseif($order_history->order_history_status == 9)

                                    @elseif($order_history->order_history_status == 10)
                                        {{ $order_history->port_of_origin }}
                                    @elseif($order_history->order_history_status == 11)
                                        {{ $order_history->shipper_destination }}
                                    @elseif($order_history->order_history_status == 12)

                                    @elseif($order_history->order_history_status == 13)

                                    @endif
                                    </td>
                                </tr>
                                <tr>
                                @foreach($quote_histories as $quote_history)
                                    <td class="col-md-4">{{ \Carbon\Carbon::parse($quote_history->created_at)->format('M d,Y') }}</td>
                                    <td class="col-md-4">
                                        @if($quote_history->quote_history_status == 3)
                                            {{ "Shipping Quote Approved" }}
                                        @elseif($quote_history->quote_history_status == 5)
                                            {{ "Shipping Quote Expired" }}
                                        @endif
                                    </td>
                                    <td class="col-md-4">
                                        @if($quote_history->quote_history_status == 3)
                                            {{ "Paid" }}
                                        @elseif($quote_history->quote_history_status == 5)
                                            <a class="request-quote-link" href="{{ url('quote/create') }}">Request New Quote</a>
                                        @endif
                                    </td>

                                @endforeach
                            </tr>
                            @endforeach
                            </tbody>
                </table>
            </div>
        </div>
       </div>
            <div id="print_div"></div>
    </section>
</section>

<div id="print_order"></div>
<div class="modal fade" id="requiredDocumentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document" style="width:60%">
        <div class="modal-content">
          <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="learnMoreModalLabel">Edit Document</h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-md-12">
            <div id="document_section" class="col-md-12 no-padding">
                <table class="table quote-summery-table">
                    <thead>
                    <tr>
                        <th class="col-md-4">Customer Documents</th>
                        <th class="col-md-3">Date Submitted</th>
                        <th class="col-md-2">Due Date</th>
                        <th class="col-md-3">View</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($documents as $document)
                            <tr>
                                {{--*/ $due_date = ''; /*--}}
                                @if($document->due_date) 
                                    {{--*/ $due_date = \Carbon\Carbon::parse($document->due_date)->format('m/d/Y'); /*--}}
                                @else
                                    {{--*/ $due_date = null; /*--}}
                                @endif
                                <td class="col-md-4"><input type="checkbox" name="checkbox" @if($document->status == 1) {{ "checked" }} @endif><span>{{ $document->document }}</span></td>
                                <td class="col-md-3">{{ \Carbon\Carbon::parse($document->order_document_created_at)->format('m/d/Y') }}</td>
                                <td class="col-md-2">{{ $due_date }}</td>
                                <td class="col-md-3"><span value="{{ $document->order_document_id.",".$document->document.",".$due_date.",".$document->id }}" class="edit_document_link light-blue-link">@if($document->document_id) Edit @else Upload Document @endif</span></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="modal-footer">
                    <button type="submit" class="btn-load" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">Close</button>
                </div>
            </div>
            <div id="edit_document_section" class="col-md-12 no-padding ">
                <div class="col-md-12 margin-bottom-20">
                    <div class="col-md-6 text-left">
                        <a id="back" class="light-blue-link">Back</a>
                    </div>
                </div>
                <form method="post" action="{{ url('logistics/update-document') }}" id="validate_edit_document" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="order_document_id" id="order_document_id">
                    <input type="hidden" name="order_document_name" id="order_document_name">
                    <input type="hidden" name="order_id" value="{{ $order->order_id }}">
                    <input type="hidden" name="document_id" id="document_id">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <label class="control-label">File(s)</label>
                            </div>
                            <div class="col-md-10" id="file_name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <label class="control-label"></label>
                        </div>
                        <div class="col-md-10">
                            <input type="file" class="form-control validate[required]" name="document">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <button class="btn-load pull-right" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">Close</button>
                            <button type="submit" class="button pull-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
</div>
<div class="modal fade" id="pendingapproveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width:60%">
        <div class="modal-content">
            <div class="modal-header col-md-12">
                <h4 class="modal-title col-md-10" id="pendingapproveLabel">Pending Approval</h4>
                <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body col-md-12">
                <div id="document_section" class="col-md-12 no-padding">
                    <table class="table quote-summery-table">
                        <thead>
                        <tr>
                            <th class="col-md-2">Type</th>
                            <th class="col-md-4">Details</th>
                            <th class="col-md-1">Cost</th>
                            <th class="col-md-1">Number</th>
                            <th class="col-md-2">Total</th>
                            <th class="col-md-2">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($warehouse_hold_detail as $warehouse_hold_details)
                            <tr>
                                <td class="col-md-2">{{ $warehouse_hold_details['hold_type'] }}</td>
                                <td class="col-md-4">{{ $warehouse_hold_details['name'] }} <br><b>Notes: </b> {{ $warehouse_hold_details['notes'] }}</td>
                                <td class="col-md-1">{{ $warehouse_hold_details['cost'] }}</td>
                                <td class="col-md-1">{{ $warehouse_hold_details['numbers'] }}</td>
                                <td class="col-md-2">{{ $warehouse_hold_details['cost']*$warehouse_hold_details['numbers'] }}</td>
                                <td class="col-md-2">
                                    @if($warehouse_hold_details['status']=='0')
                                    <a onclick="warehousehold('{{ $warehouse_hold_details['id'] }}','1')" class="button">Approve</a>
                                    <a onclick="warehousehold('{{ $warehouse_hold_details['id'] }}','2')" class="button">Reject</a>
                                    @elseif($warehouse_hold_details['status']=='1')
                                        Approved {{ date('m/d/Y',strtotime($warehouse_hold_details['updated_at'])) }}
                                    @elseif($warehouse_hold_details['status']=='2')
                                        Rejected {{ date('m/d/Y',strtotime($warehouse_hold_details['updated_at'])) }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                    <div class="modal-footer">
                        <button type="submit" class="btn-load" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteDocumentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="learnMoreModalLabel">Delete Document</h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-md-12">
            <div class="col-md-12 no-padding">
                <form method="post" action="{{ url('logistics/delete-document') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="delete_document_id" id="delete_document_id">
                    Are you sure want to delete ?
                    <div class="form-Control">
                        <div class="col-md-12">
                            <button type="submit" class="button pull-right">Yes</button>
                            <button class="btn-load pull-right" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">No</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
</div>

<div class="modal fade" id="uploadTrackingNumber" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header col-md-12">
        <h4 class="modal-title col-md-10" id="exampleModalLabel"><b>Upload Tracking Number Modal</b></h4>
        <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body col-md-12">
        <div class="col-md-12">
            <form method="post" id="validate" action="{{ url('order/upload_tracking_number') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="order_id" value="{{ $order->order_id }}">
                <div class="form-group">
                    <div class="col-md-2">
                        <label>Tracking Number : </label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" name="tracking_number" class="validate[required] form-control">
                    </div>
                </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-reject-quote">Upload</button>
            </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="addNotesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="exampleModalLabel"><b>Add Note Modal</b></h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
              <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body col-md-12">
            <div class="col-md-12 no-padding">
                <form method="post" action="{{ url('order/add_note') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="order_id" value="{{ $order->order_id }}">
                    <input type="hidden" name="zoho_id" value="{{ $order->zoho_id }}">
                    <div class="form-group">
                        <div class="col-md-2">
                            <label style="line-height:3;">Add Note:</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="note" class="form-control validate[required]"></textarea>
                        </div>
                    </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-reject-quote">Add</button>
                </form>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="learnMoreModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="learnMoreModalLabel">What Current Status</h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-md-12">
            <div class="col-md-12 no-padding">

            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn" data-dismiss="modal" aria-label="Close">Close</button>
          </div>
        </div>
      </div>
    </div>
<div class="modal fade" id="wareholdModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header col-md-12">
                <h4 class="modal-title col-md-10" id="wareholdModalLabel">Warehouse Holds</h4>
                <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body col-md-12">
                <div class="col-md-12 no-padding">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Hold Type</th>
                                <th>Details</th>
                                <th>Cost</th>
                                <th>Notes</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tboady>
                        @foreach($warehouse_hold_detail as $warehouse_hold_details)
                            <tr>
                                <td>{{$warehouse_hold_details['hold_type']}}</td>
                                <td>{{$warehouse_hold_details['name']}}</td>
                                <td>{{$warehouse_hold_details['cost']}}</td>
                                <td>{{$warehouse_hold_details['notes']}}</td>
                                <td>@if($warehouse_hold_details['status']=='0') Pending @elseif($warehouse_hold_details['status']=='1') Approved @elseif($warehouse_hold_details['status']=='2')  Rejected @endif</td>
                                <td><a onclick="editwarehold({{$warehouse_hold_details['id']}})">Edit</a></td>
                            </tr>
                        @endforeach
                        </tboady>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editwareholdModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header col-md-12">
                <h4 class="modal-title col-md-10" id="editwareholdModalLabel">Warehouse Holds</h4>
                <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body col-md-12">
                <div class="col-md-12 no-padding">
                    <a onclick="backmodal()">Back</a>
                    <div id="detail"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="submitholdmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header col-md-12">
                <h4 class="modal-title col-md-10" id="editwareholdModalLabel">Warehouse Holds</h4>
                <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body col-md-12">
                {!! Form::open(['url' => 'warehouse/approvereview', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                {!! Form::hidden('order',old('order',$order->order_id),['id'=>'order']) !!}
                {!! Form::hidden('status',old('status',$order->hold),['id'=>'status']) !!}
                {!! Form::hidden('new',old('new',1),['id'=>'new']) !!}
                @if($order->hold=='1')
                <div class="col-md-12 no-padding">
                    <div class="form-group">
                        <div class="col-md-6">
                            <span class="control-label col-md-6">Type</span>
                            <div class="col-md-6">
                                <select name="type" id="type" class="form-control select2" onchange="getdetails(this.value)">
                                    <option value="">Type</option>
                                    @foreach($warehouse_hold_type as $warehouse_hold_types)
                                        <option value="{{ $warehouse_hold_types->id }}">{{$warehouse_hold_types->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <span class="control-label col-md-6">Cost</span>
                            <div class="col-md-6" id="cost_div">
                                <input type="text" name="cost" id="cost" class="form-control" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <span class="control-label col-md-6">Details</span>
                            <div class="col-md-6" id="detail_div">
                                <input type="text" name="details" id="details" class="form-control" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <span class="control-label col-md-6">Measurement</span>
                            <div class="col-md-6" id="measurement_div">
                                <select name="measurement" id="measurement" class="form-control">
                                    <option value="">Measurement</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">&nbsp;</div>
                        <div class="col-md-6">
                            <span class="control-label col-md-6">Number of Unit/Cartons</span>
                            <div class="col-md-6">
                                <input type="text" name="number_unit" id="number_unit" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <span class="control-label col-md-3">Notes </span>
                            <div class="col-md-9">
                                <textarea name="notes" id="notes" cols="10" rows="4" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                @elseif($order->hold=='2')
                    <div class="col-md-12 no-padding">
                        <div class="form-group">
                            <div class="col-md-12">
                                <span class="control-label col-md-3">Type</span>
                                <div class="col-md-9">
                                    <input type="text" name="type" id="type" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <span class="control-label col-md-3">Details</span>
                                <div class="col-md-9">
                                    <input type="text" name="details" id="details" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <span class="control-label col-md-3">Notes </span>
                                <div class="col-md-9">
                                    <textarea name="notes" id="notes" cols="10" rows="4" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn" data-dismiss="modal" aria-label="Close">Close</button>
                <input type="submit" class="btn btn-primary" value="Submit Hold">
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('js')
    <script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">

        $(document).ready(function () {
            var prefix = 's2id_';
            $("form[id^='validate_edit_document']").validationEngine('attach', {
                promptPosition: "bottomRight", scroll: false,
                prettySelect: true,
                usePrefix: prefix
            });

            $("form[id^='validate']").validationEngine('attach', {
                promptPosition: "bottomRight", scroll: false,
                prettySelect: true,
                usePrefix: prefix
            });
        });

        $('#edit_document_section').hide();
        $('#back').click(function(){
            $('#document_section').show();
            $('#edit_document_section').hide();
        });

        $('.edit_document_link').click(function(){
            var value = $(this).attr('value');
            var split = value.split(',');
           
            $('#delete_document_id').val(split[0]);
            if(split[0]){
                $('#Delete_Requirement').show();
            }
            else{
                $('#Delete_Requirement').hide();
            }
            $('#file_name').html(split[1]);
            $('#order_document_name').val(split[1]);
            $('#order_document_id').val(split[0]);
            $('#order_document_due_date').val(split[2]);
            $('#document_id').val(split[3]);
            $('#document_section').hide();
            $('#edit_document_section').show();
        });

        $('.datepicker').datepicker({
            startDate: new Date(),
        });

        $(document).ready(function () {
            $('#document_submit').click(function(){
                var prefix = 's2id_';
                $("form[id^='validate']").validationEngine('attach',
                {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
            });
            $('#order_history').DataTable({
                "bSort" : false,
                "bPaginate": true,
                "bFilter": false,
                "bInfo": true,
                "pageLength": 5,
            });
            $('#order_history_length').addClass('hide');
            $('#order_history_paginate').addClass('hide');
            $('#order_history_info').addClass('hide');
            $('#entries').html($('#order_history_info').text());
        });
        $('#previous').click(function(){
            $('#order_history_previous').click();
            $('#entries').html($('#order_history_info').text());
        });
        $('#next').click(function(){
            $('#order_history_next').click();
            $('#entries').html($('#order_history_info').text());
        });
        $('#print').click(function(){
        var method_id = "{{ $method_id }}";
        var customer_requirement_id = "{{ $customer_requirement_id }}";
        var html = '';
        $.ajax({
            headers: {
            'X-CSRF-Token':  "{{ csrf_token() }}"
            },
            method: 'GET', // Type of response and matches what we said in the route
            url: '/order/print-order'+'/'+method_id+'/'+customer_requirement_id+'/print', // This is the url we gave in the route
            data: {
            },
            success: function (response) {
                $('#print_order').hide();
                $('#print_order').html(response);
                var prtContent = document.getElementById("print_order");
                var WinPrint = window.open('', '', 'left=0,top=0,width=500,height=200,toolbar=0,scrollbars=0,status=0');
                WinPrint.document.write(prtContent.innerHTML);
                WinPrint.document.close();
                WinPrint.focus();
                WinPrint.print();
                WinPrint.close();
                $('#print_order').hide();

            }
        });
        });
        function viewwarehold(){
            $("#wareholdModal").modal('show');
        }
        function editwarehold(id){
            $("#wareholdModal").modal('hide');
            $("#detail").empty();
            $.ajax({
                headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '{{ url("warehouse/getdetails") }}',
                data : {
                    id:id,
                },
                success: function (data) {
                    var html='';
                    if(data.detail.hold_type_id!=0) {
                        $.each(data.warehouse_hold_type, function (key, value) {
                            html += '<option value="' + value.id + '">' + value.name + '</option>';
                        });
                        if(data.detail.hold_type_id==4) {
                            $("#detail").append('<div class="form-group"><div class="col-md-6"><span class="control-label col-md-6">Type</span> <div class="col-md-6"><select name="type" id="type" class="form-control select2" onchange="getdetails(' + this.value + ')"><option vlaue="">Type</option>' + html + '</select></div></div><div class="col-md-6"><span class="control-label col-md-6">Cost</span><div class="col-md-6" id="status_cost_div"><input type="text" name="cost" id="cost" class="form-control" value="'+data.detail.cost+'"></div></div></div><div class="form-group"><div class="col-md-6"><span class="control-label col-md-6">Details</span><div class="col-md-6"><input type="text" name="details" id="details" class="form-control" value="'+data.detail.detail_text +'"></div></div><div class="col-md-6"><span class="control-label col-md-6">Measurement</span><div class="col-md-6" id="status_measurement_div"><select name="measurement" id="measurement" class="form-control"><option value="">Measurement</option></select></div></div></div><div class="form-group"><div class="col-md-6">&nbsp;</div><div class="col-md-6"><span class="control-label col-md-6">Number of Unit/Cartons</span><div class="col-md-6" id="status_number_div"><input type="text" name="number_unit" id="number_unit" class="form-control" value="'+data.detail.no_of_unit+'"></div></div></div><div class="form-group"><div class="col-md-12"><span class="control-label col-md-3">Notes </span><div class="col-md-9"><textarea name="notes" id="notes" cols="10" rows="4" class="form-control">'+data.detail.notes+'</textarea></div></div></div>');
                            $("#type").val(data.detail.hold_type_id);
                        }
                        else {
                            if(data.detail.hold_type_id==1) {
                                var html1 = '';
                                html1 += '<select name="details" id="details" class="form-control select2" onchange="getcostmesure('+data.detail.hold_type_id+',this.value)">';
                                $.each(data.type_details, function (prep_key, prep_value) {
                                    html1 += '<option value="' + prep_value.prep_service_id + '">' + prep_value.service_name + '</option>';
                                });
                                html1 += '</select>';
                            }
                            else if(data.detail.hold_type_id==2){
                                var html1 = '';
                                html1 += '<select name="details" id="details" class="form-control select2" onchange="getcostmesure('+data.detail.hold_type_id+',this.value)">';
                                $.each(data.type_details, function (dam_key, dam_value) {
                                    html1 += '<option value="' + dam_value.id + '">' + dam_value.name + '</option>';
                                });
                                html1 += '</select>';
                            }
                            else if(data.detail.hold_type_id==3){
                                var html1 = '';
                                html1 += '<select name="details" id="details" class="form-control select2" onchange="getcostmesure('+data.detail.hold_type_id+',this.value)>';
                                $.each(data.type_details, function (que_key, que_value) {
                                    html1 += '<option value="' + que_value.id + '">' + que_value.name + '</option>';
                                });
                                html1 += '</select>';
                            }
                            $("#detail").append('<div class="form-group"><div class="col-md-6"><span class="control-label col-md-6">Type</span> <div class="col-md-6"><select name="type" id="type" class="form-control select2" onchange="getdetails(' + this.value + ')"><option vlaue="">Type</option>' + html + '</select></div></div><div class="col-md-6"><span class="control-label col-md-6">Cost</span><div class="col-md-6" id="status_cost_div"><input type="text" name="cost" id="cost" class="form-control" value="'+data.detail.cost+'"></div></div></div><div class="form-group"><div class="col-md-6"><span class="control-label col-md-6">Details</span><div class="col-md-6">'+html1+'</div></div><div class="col-md-6"><span class="control-label col-md-6">Measurement</span><div class="col-md-6" id="status_measurement_div"><select name="measurement" id="measurement" class="form-control"><option value="">Measurement</option></select></div></div></div><div class="form-group"><div class="col-md-6">&nbsp;</div><div class="col-md-6"><span class="control-label col-md-6">Number of Unit/Cartons</span><div class="col-md-6" id="status_number_div"><input type="text" name="number_unit" id="number_unit" class="form-control" value="'+data.detail.no_of_unit+'"></div></div></div><div class="form-group"><div class="col-md-12"><span class="control-label col-md-3">Notes </span><div class="col-md-9"><textarea name="notes" id="notes" cols="10" rows="4" class="form-control">'+data.detail.notes+'</textarea></div></div></div>');
                            $("#type").val(data.detail.hold_type_id);
                        }
                    }
                    else{
                        $("#detail").append('<div class="form-group"><div class="col-md-6"><span class="control-label col-md-6">Type</span> <div class="col-md-6"><input name="type" id="type" class="form-control" value="'+data.detail.hold_type_text+'"></div></div><div class="form-group"><div class="col-md-6"><span class="control-label col-md-6">Details</span><div class="col-md-6"><input type="text" name="detail" id="detail" value="'+data.detail.detail_text+'"></div></div><div class="form-group"><div class="col-md-6">&nbsp;</div><div class="col-md-6"><span class="control-label col-md-6">Number of Unit/Cartons</span><div class="col-md-6" id="status_number_div"><input type="text" name="number_unit" id="number_unit" class="form-control" value="'+data.detail.no_of_unit+'"></div></div></div><div class="form-group"><div class="col-md-12"><span class="control-label col-md-3">Notes </span><div class="col-md-9"><textarea name="notes" id="notes" cols="10" rows="4" class="form-control">'+data.detail.notes+'</textarea></div></div></div>');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });

            $("#editwareholdModal").modal('show');
        }
        function backmodal(){
            $("#wareholdModal").modal('show');
            $("#editwareholdModal").modal('hide');
        }
        function submithold(){
            $("#submitholdmodal").modal('show');
        }
        function getdetails(type_id){
            $.ajax({
                headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '{{ url("warehouse/getdetails") }}',
                data : {
                    type_id:type_id,
                },
                success: function (data) {
                    if(type_id==1) {
                        var html = '';
                        html += '<select name="details" id="details" class="form-control select2" onchange="getcostmesure('+type_id+',this.value)">';
                        $.each(data, function (key, value) {
                            html += '<option value="' + value.prep_service_id + '">' + value.service_name + '</option>';
                        });
                        html += '</select>';
                    }
                    else if(type_id==2){
                        var html = '';
                        html += '<select name="details" id="details" class="form-control select2" onchange="getcostmesure('+type_id+',this.value)">';
                        $.each(data, function (key, value) {
                            html += '<option value="' + value.id + '">' + value.name + '</option>';
                        });
                        html += '</select>';
                    }
                    else if(type_id==3){
                        var html = '';
                        html += '<select name="details" id="details" class="form-control select2" onchange="getcostmesure('+type_id+',this.value)>';
                        $.each(data, function (key, value) {
                            html += '<option value="' + value.id + '">' + value.name + '</option>';
                        });
                        html += '</select>';
                    }
                    else {
                        var html='';
                        html += '<input type="text" name="details" id="details" class="form-control">';
                    }
                    $("#detail_div").empty();
                    $("#detail_div").append(html);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
        function getcostmesure(type_id,detail_id){
            $.ajax({
                headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '{{ url("warehouse/getcostmesure") }}',
                data : {
                    type_id:type_id,
                    detail_id:detail_id,
                },
                success: function (data) {
                    var html = '';
                    var price='';
                    var id='';
                    var name='';
                    if(data.price != undefined)
                        price = data.price;
                    if(data.id != undefined)
                        id=data.id;
                    if(data.name != undefined)
                        name=data.name;
                    html += '<input type="text" name="cost" id="cost" class="form-control" value="'+price+'">';
                    $("#cost_div").empty();
                    $("#cost_div").append(html);
                    var html1 = '';
                    html1 += '<select name="measurement" id="measurement" class="form-control select2">';
                    html1 += '<option value="'+id+'">'+name+'</option>';
                    html1 += '</select>';
                    $("#measurement_div").empty();
                    $("#measurement_div").append(html1);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
        function warehousehold(id,status){
            $.ajax({
                headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '{{ url("warehouse/warehouseholdstatus") }}',
                data : {
                    id:id,
                    status:status,
                },
                success: function (data) {
                    console.log(data);
                    location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
    </script>
@endsection