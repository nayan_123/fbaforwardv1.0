@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
    {!! Html::style('assets/dist/css/style.css') !!}
    <style>
        .new_nav > li > a{
            color: #777;
            text-decoration: none;
            cursor: default;
            background-color: #eee;
            border: 1px solid #ddd;
            border-bottom-color: transparent;
        }
        .new_nav > li.active > a, .new_nav > li.active > a:focus, .new_nav > li.active > a:hover{
            color: #ffffff;
            text-decoration: none;
            cursor: default;
            background-color: #0088cc;
            border: 1px solid #ddd;
            border-bottom-color: transparent;
        }
        .nav-tabs {
            border-bottom: 0px solid #ddd;
        }
        .nav-tabs > li >a{
            margin-right: 0px;
            padding: 10px 5px;
        }
        #tab > li > a {
            height: 95px;
            overflow: auto;
        }
        select {
            width:70px;
            white-space:pre;
            -webkit-appearance: none;
            text-wrap: normal;
        }
    </style>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-shopping-cart"> {{$title}}</i>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-shopping-cart"></i> {{$title}}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                    <div role="tabpanel" id="tabdiv">
                    <ul class="nav nav-tabs new_nav nav-justified" role="tablist" id="tab">
                        <li class="active" id="tab1"><a href="#tabs1" data-toggle="tab">New Order, Checking Now</a></li>
                        <li class="" id="tab2"><a href="#tabs2" data-toggle="tab">Waiting for Goods Ready</a></li>
                        <li class="" id="tab3"><a href="#tabs3" data-toggle="tab">Final Quote Submitted</a></li>
                        <li class="" id="tab4"><a href="#tabs4" data-toggle="tab">Booked Space</a></li>
                        <li class="" id="tab5"><a href="#tabs5" data-toggle="tab">Loaded Containers</a></li>
                        <li class="" id="tab6"><a href="#tabs6" data-toggle="tab">Picked Up</a></li>
                        <li class="" id="tab7"><a href="#tabs7" data-toggle="tab">Customs Clearance Ready</a></li>
                        <li class="" id="tab8"><a href="#tabs8" data-toggle="tab">ISF Filed</a></li>
                        <li class="" id="tab9"><a href="#tabs9" data-toggle="tab">On Board</a></li>
                        <li class="" id="tab10"><a href="#tabs10" data-toggle="tab">Departure</a></li>
                        <li class="" id="tab11"><a href="#tabs11" data-toggle="tab">Arrived</a></li>
                        <li class="" id="tab12"><a href="#tabs12" data-toggle="tab">Released</a></li>
                        <li class="" id="tab13"><a href="#tabs13" data-toggle="tab">Cancelled or No Reply</a></li>
                    </ul>
                    </div>
                    <div class="tab-content responsive">
                        @for($count=1;$count<=13;$count++)
                            @if($count==1)
                        <div class="row tab-pane fade in active" id="tabs{{$count}}">
                            @else
                        <div class="row tab-pane fade" id="tabs{{$count}}">
                            @endif
                            <div class="col-md-12">
                                <div class="table-responsive no-padding">
                                <table id="order" class="table">
                            <thead>
                                <tr>
                                    <th>Order #</th>
                                    <th>SB #</th>
                                    <th>Company</th>
                                    <th>Supplier</th>
                                    <th>Product</th>
                                    <th>POL</th>
                                    <th>POD</th>
                                    <th>Ready date</th>
                                    <th>ETD</th>
                                    <th>ETA</th>
                                    <th>Shipment type</th>
                                    <th>Remark</th>
                                    <th>Shipper Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($order as $orders)
                                @if($orders['shipper_status']==$count)

                                <tr id="tr{{$orders['id']}}" class="trmain">
                                    <td>{{ $orders['order_no'] }}</td>
                                    <td>@if(!empty($orders['sb_number'])) {{ $orders['sb_number'] }} @else <a href="javascript:void(0)" id="sbedit{{$orders['order_id']}}" >edit</a> @endif</td>
                                    <td>{{ $orders['company_name'] }}</td>
                                    <td>{{ $orders['supplier_company_name'] }}</td>
                                    <td>{{ $orders['product_name'] }}</td>
                                    <td>{{ $orders['port_of_origin'] }}</td>
                                    <td></td>
                                    <td>{{ date('m/d/Y',strtotime($orders['goods_ready_date'])) }}</td>
                                    <td></td>
                                    <td></td>
                                    <td>{{ $orders['shipment_type_short_name'] }}</td>
                                    <td>@if(!empty($orders['remark'])) {{ $orders['remark'] }} @else <a href="javascript:void(0)" id="remarkedit{{$orders['order_id']}}">edit</a> @endif</td>
                                    <td class="last">
                                        <div class="dropdown" id="div{{$orders['id']}}" >
                                            <select style="padding-right: 0px; padding-left: 0px;" class="button1 dropdown-toggle" type="button" id="dropdownMenu{{$orders['id']}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onchange="updatestatus(this.value,'{{$orders['order_id']}}','{{$orders['port_of_origin']}}','{{$orders['shipper_destination']}}','{{$orders['ETD_china']}}','{{$orders['prealert_details_ctreated_at']}}')">
                                                <option value="1" class="dropdown-item" @if($orders['shipper_status']=='1') {{ "selected" }}@endif>New Order, Checking Now</option>
                                                <option value="2" class="dropdown-item" @if($orders['shipper_status']=='2') {{ "selected" }}@endif>Waiting for Goods Ready</option>
                                                <option value="3" class="dropdown-item" @if($orders['shipper_status']=='3') {{ "selected" }}@endif>Final Quote Submitted</option>
                                                <option value="4" class="dropdown-item" @if($orders['shipper_status']=='4') {{ "selected" }}@endif>Booked Space</option>
                                                <option value="5" class="dropdown-item" @if($orders['shipper_status']=='5') {{ "selected" }}@endif>Loaded Containers</option>
                                                <option value="6" class="dropdown-item" @if($orders['shipper_status']=='6') {{ "selected" }}@endif>Picked Up</option>
                                                <option value="7" class="dropdown-item" @if($orders['shipper_status']=='7') {{ "selected" }}@endif>Customs Clearance Ready</option>
                                                <option value="8" class="dropdown-item" @if($orders['shipper_status']=='8') {{ "selected" }}@endif>ISF Filed</option>
                                                <option value="9" class="dropdown-item" @if($orders['shipper_status']=='9') {{ "selected" }}@endif>On Board</option>
                                                <option value="10" class="dropdown-item" @if($orders['shipper_status']=='10') {{ "selected" }}@endif>Departure</option>
                                                <option value="11" class="dropdown-item" @if($orders['shipper_status']=='11') {{ "selected" }}@endif>Arrived</option>
                                                <option value="12" class="dropdown-item" @if($orders['shipper_status']=='12') {{ "selected" }}@endif>Released</option>
                                                <option value="13" class="dropdown-item" @if($orders['shipper_status']=='13') {{ "selected" }}@endif>Cancelled or No Reply</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                            </div>
                        </div>
                            @endfor
                    </div>
                </div>
                </div>
            </div>
        </div>
            <div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Update</h4>
                        </div>
                        <div class="modal-body" style="padding:80px;">
                            <div class="row">
                                <div class="col-md-12" id="main">
                                    {!! Form::open(['url' =>  'shipper/updatedetail', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                {!! Form::hidden('order_id',old('order_id'), ['id'=>'order_id']) !!}
                                                <label class="col-md-3" id="sblabel">SBNumber </label>
                                                <div class="col-md-6" id="sbdiv" hidden>
                                                    <div class="input-group">
                                                        <input type="text" id="sbnumber" name="sbnumber"  class="form-control validate[required]">
                                                    </div>
                                                </div>
                                                <label class="col-md-3" id="remarklabel">Remark </label>
                                                <div class="col-md-6" id="remarkdiv" hidden>
                                                    <div class="input-group col-md-9">
                                                        <textarea id="remark" name="remark" cols="20" rows="4" class="form-control validate[required]"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="input-group">
                                                        {!! Form::submit('  Submit  ', ['class'=>'btn btn-primary',  'id'=>'add']) !!}
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
@section('js')
    <script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}

    <script type="text/javascript">
        $(document).ready(function () {
            // Validation Engine init
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach',
                {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#quote').DataTable({
                "bSort" : false,
                "bPaginate": false,
                //"bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                //"bAutoWidth": false
            });
            $('#order').DataTable({
                "bSort" : false,
                "bPaginate": false,
                "bFilter": true,
                "bInfo": false,
            });
        });
        $(document).on("click", "table tr", function(e) {
            var firstInput = this.getElementsByTagName('select')[0];
            firstInput.checked = !firstInput.checked;
            var id= this.id;
            id=id.substr(2);
            var td_id=parseInt($(this).index());
            $(".trmain").removeClass('trhead');
            $("select").removeClass('button');
            $("select").addClass('button1');
          $("#tr"+id).addClass('trhead');
            $("#dropdownMenu"+id).removeClass('button1');
          $("#dropdownMenu"+id).addClass('button');
            if($("#shipmentdetail"+id).length > 0)
            {
            }
            else {
                $(".tr").remove();
                $("#tr" + id).after('<tr class="tr" id="shipmentdetail' + id + '" onclick="getshipmentdetail(' + id + ')">' +
                    '<td style="border-top: 0px;">Shipment Details</td>' +
                    '</tr>' +
                    '<tr class="tr" id="quotedetail' + id + '" onclick="getquotedetail(' + id + ')" style="border-bottom: 3px solid #000000;">' +
                    '<td style="border-top: 0px;">Quote Detail</td>' +
                    '</tr>');
                getshipmentdetail(id);
            }
        });
        function getshipmentdetail(id) {
            $.ajax({
                headers: {
                    'X-CSRF-Token':  "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '/shipper/getshipmentdetail',
                data: {
                    'id': id,
                },
                success: function (details) {
                    $('.preloader').css("display", "none");
                    var shipment_details = $.parseJSON(details);
                    var contact ='';
                    var phone='';
                    var email='';
                    var address='';
                    var city='';
                    var supplier_name='';
                    var supplier_phone='';
                    var supplier_email='';
                    var cartons='';
                    var units='';
                    var weight='';
                    var volume='';

                    $.each(shipment_details.shipmentdetail, function (key, value) {
                            contact = value.contact_fname+" "+value.contact_lname;
                            phone   = value.contact_phone;
                            email = value.contact_email;
                            address  = value.company_address+", "+value.company_address2;
                            city  = value.company_city+", "+value.company_state+", "+value.company_country;
                            supplier_name=value.contact_name;
                            supplier_phone=value.phone_number;
                            supplier_email=value.email;
                            cartons=value.no_boxs;
                            units=value.total;
                            weight=value.shipment_weight;
                            volume=value.shipment_volume;
                    });
                    $('#shipmentdetail'+id).find('td').not(':first').remove();
                    $("#shipmentdetail"+id).append('<td rowspan="2"><b>Contact<br>Phone<br>Email<br>Address</b></td>' +
                        '<td rowspan="2">'+contact+'<br>'+phone+'<br>'+email+'<br>'+address+'<br>'+city+'</td>' +
                        '<td rowspan="2">&nbsp;</td>' +
                        '<td rowspan="2"><b>Supplier<br>Phone<br>Email</b></td>' +
                        '<td rowspan="2">'+supplier_name+'<br>'+supplier_phone+'<br>'+supplier_email+'</td>' +
                        '<td rowspan="2">&nbsp;</td>' +
                        '<td rowspan="2"><b>Cartons<br>Units<br>Weight<br>Volume</b></td>' +
                        '<td rowspan="2">'+cartons+'<br>'+units+'<br>'+weight+'<br>'+volume+'</td>' +
                        '<td rowspan="2">&nbsp;</td>' +
                        '<td rowspan="2"><b>Carrier<br>Tracking #<br># of Container(s)<br>Container Size</b></td>' +
                        '<td rowspan="2">&nbsp;</td>' +
                        '<td rowspan="2">&nbsp;</td>');
                    $('#shipmentdetail'+id).find('td').css('background-color','#EEE');
                    $('#quotedetail'+id).css('background-color','#FFF');

                }
            });
        }
        function getquotedetail(id) {
            $.ajax({
                headers: {
                    'X-CSRF-Token':  "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '/shipper/getshipmentdetail',
                data: {
                    'id': id,
                },
                success: function (details) {
                    $('.preloader').css("display", "none");
                    var shipment_details = $.parseJSON(details);
                    var incoterms='';
                    $.each(shipment_details.shipmentdetail, function (key, value) {
                        incoterms=value.incoterm_short_name;
                    });
                    $('#shipmentdetail'+id).find('td').not(':first').remove();
                    $("#shipmentdetail"+id).append('<td rowspan="2"><b>Incoterms</b></td>' +
                        '<td rowspan="2">'+incoterms+'</td>' +
                        '<td rowspan="2"><b>Freight Fee<br>Handling<br>Insurance</b></td>' +
                        '<td rowspan="2">$0.00<br>$0.00<br>$0.00</td>' +
                        '<td rowspan="2"><b>Other Fee<br>Other Fee<br>Other Fee</b></td>' +
                        '<td rowspan="2">$0.00<br>$0.00<br>$0.00</td>' +
                        '<td rowspan="2"><b>Other Fee<br>Other Fee<br>Other Fee</b></td>' +
                        '<td rowspan="2">$0.00<br>$0.00<br>$0.00</td>' +
                        '<td rowspan="2">&nbsp;</td>' +
                        '<td rowspan="2">&nbsp;</td>' +
                        '<td rowspan="2">&nbsp;</td>' +
                        '<td rowspan="2">&nbsp;</td>');
                    $('#shipmentdetail'+id).find('td').not(':first').css('background-color','#EEE');
                    $('#shipmentdetail'+id).find('td:first').css('background-color','#FFF');
                    $('#quotedetail'+id).css('background-color','#EEE');

                }
            });
        }
        function updatestatus(status,order_id,port_of_origin,shipper_destination,ETD_china,prealert_details_ctreated_at)
        {
            $.ajax({
                headers: {
                    'X-CSRF-Token':  "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '/shipper/shipper_status',
                data: {
                    'order_id': order_id,
                    'status': status,
                    'port_of_origin': port_of_origin,
                    'shipper_destination': shipper_destination,
                    'ETD_china': ETD_china,
                    'prealert_details_ctreated_at': prealert_details_ctreated_at
                },
                success: function (details) {
                    $('.preloader').css("display", "none");
                    location.reload();
                }
            });

        }
        $(document).on("click", "table tr td a", function(e) {
            var id=this.id;
            var str= id.split(/([a-zA-Z]+)(?=[0-9])/);
            $("#updatemodal").modal('show');
            $("#order_id").val(str[2]);
            if(str[1]=='sbedit')
            {
                $("#sbdiv").show();
                $("#sblabel").show();
                $("#remarkdiv").hide();
                $("#remarklabel").hide();
                $("#remark").val('');
            }
            else if(str[1]=='remarkedit')
            {
                $("#remarkdiv").show();
                $("#remarklabel").show();
                $("#sbdiv").hide();
                $("#sblabel").hide();
                $("#sbnumber").val('');
            }
        });

    </script>
@endsection