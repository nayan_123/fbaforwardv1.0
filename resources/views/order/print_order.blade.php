<html>
<head>

</head>
<body>
<style>
.heading{
    color: #7d7d7d;
    font-weight: 600;
    font-size: 30px;
}
.grey-font{
    color: #7d7d7d;
    font-weight: 600;
}
.border-box{ border:1px solid #e0dddd; }
.order_table>thead>tr>th{ background-color:#0088cc;color:#fff;text-transform: uppercase; }
.order_table>tbody{ border:1px solid #e0dddd;border-top: none; }
.order_table>tbody>tr>td{ border-right:1px solid #e0dddd; }
.border-box{ border:1px solid #e0dddd; }
.margin-top-30{ margin-top: 30px; }
.contact-info{ color:#0088cc; }
.border-top-1{ border-top:1px solid #e0dddd; }
.margin-top-10{ margin-top: 10px; }
.margin-bottom-0{ margin-bottom: 0px; }
.padding-10{ padding: 10px; }
.border-1{ border:1px solid #e0dddd;border-top: none; }
.font-size-16{ font-size: 16px; }
.text-center{ text-align: center; }
@media (min-width: 992px){
    .col-md-12 {
        width: 100%;
    }
    .col-md-offset-3 {
        margin-left: 25%;
    }
}
.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
    /*float: left;*/
}
.col-md-8 {
    width: 66.66666667%;
}
.col-md-6 {
    width: 50%;
}
.col-md-9 {
    width: 75%;
}
.pull-right{
    float: right;
}
.col-md-5 {
    width: 41.66666667%;
}
.col-md-7 {
    width: 58.33333333%;
}
.logo-font p,.font-size-13 { font-size: 13px; }
.no-border tbody{ border: none !important; }
.order_table {
    background-color: transparent;
    border-spacing: 0;
    border-collapse: collapse;
    width: 100%;
    margin-bottom: 30px;    
}
.order_table>thead>tr>th,.order_table>tbody>tr>td{
    padding: 8px;
    text-align: center;
}
.no-border{ border: none !important; }
.margin-top-60{ margin-top:60px; }
.page-break{
    page-break-after: always;
}
.text-left{ text-align: left !important; }
@page { margin-bottom: 0px;margin-top:10px; }
</style>
{{--*/ $charge_qty = 1 /*--}}
{{--*/ $shipping_sub_total = 0 /*--}}
{{--*/ $prep_price = "" /*--}}
{{--*/ $prep_sub_total = 0 /*--}}
{{--*/ $trucking_qty="" /*--}}
{{--*/ $port_fee = 0 /*--}}
{{--*/ $port_fee_name =  env('PORT_FEE_NAME') /*--}}
{{--*/ $trucking_name =  env('TRUCKING_NAME') /*--}}
{{--*/ $isf_rate =  env('ISF_RATE') /*--}}
{{--*/ $isf_name =  env('ISF_NAME') /*--}}
{{--*/ $entry_rate = env('ENTRY_RATE') /*--}}
{{--*/ $entry_name = env('ENTRY_NAME') /*--}}
{{--*/ $bond_rate =  env('BOND_RATE') /*--}}
{{--*/ $bond_name =  env('BOND_NAME') /*--}}
{{--*/ $container_unloading= env('CONTAINER_UNLOADING') /*--}}
{{--*/ $palletizing= env('PALLETIZING') /*--}}
{{--*/ $amazon_approve_pallet= env('AMAZON_APPROVE_PALLET') /*--}}
{{--*/ $pallet_charge= env('pallet_charge') /*--}}
{{--*/ $receive_forward_sub_total= '' /*--}}
{{--*/ $custom_sub_total= '' /*--}}
{{--*/ $listing_service_sub_total= '' /*--}}
{{--*/ $photo_services_sub_total= '' /*--}}
{{--*/ $list_optimization_sub_total= 0 /*--}}
{{--*/ $pre_shipment_sub_total= '' /*--}}
{{--*/ $factory_audit_total= '' /*--}}
{{--*/ $pre_inspection_total= '' /*--}}
{{--*/ $order_total= '' /*--}}
{{--*/ $processing_fee= 0 /*--}}
@if($order->units==1)
    {{--*/$trucking_qty=round($order->shipment_volume / env('PALLET_CBM')) /*--}}
@else
    {{--*/$trucking_qty=round($order->shipment_volume / env('PALLET_CFT'))/*--}}
@endif
@if($trucking_qty > env('TRUCKING_QTY'))
    {{--*/ $trucking_price = ($trucking_qty -  env('TRUCKING_QTY')) * env('TRUCKING_ADDITIONAL_PRICE') + env('TRUCKING_PRICE')/*--}}
@else
    {{--*/ $trucking_price =  env('TRUCKING_PRICE') /*--}}
@endif
@if($order->shipment_type_id==1)
    @if($order->units==1)
        {{--*/$trucking_qty=round($order->shipment_volume / env('PALLET_CBM'))/*--}}
    @else
        {{--*/$trucking_qty=round($order->shipment_volume / env('PALLET_CFT'))/*--}}
    @endif

    {{--*/ $trucking_price = $trucking_qty* env('TRUCKING_PRICE')  /*--}}
@else

    {{--*/ $trucking_qty=1 /*--}}
    {{--*/ $trucking_price = $order->trucking_rate /*--}}
@endif

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6" style="float:left;">
                
                <img src = "<?php echo public_path();?>/uploads/images/FBAFORWARD_LOGO.png" style="width:70%;">
                <div class="col-md-12 grey-font">
                    <p style="margin:5px 0px;font-size: 15px;">1550 HOTEL CIRCLE NORTH</p>
                    <p style="margin:5px 0px;font-size: 15px;">SUITE 360</p>
                    <p style="margin:5px 0px;font-size: 15px;">SAN DIEGO, CA 92108</p>
                    <p style="margin:5px 0px;font-size: 15px;">+1 866 526 3951</p>
                </div>
            </div>
            <div class="col-md-6" style="margin-left: 53%;">
                <h1 class="heading">ORDER</h1>
                <div class="col-md-12 grey-font">
                    <p><b>FOR: </b>{{ $order->company_name }}</p>
                    <p><b>Email: </b>{{ $email }}</p>
                </div>
            </div>
        </div>

        {{--*/ $total_charge = 0 /*--}}

        <div class="col-md-12" style="margin-top: 10px;">
            <table class="table order_table" style="margin-bottom: 20px;">
                <thead>
                    <tr>
                        <th>Order type</th>
                        <th>Item Description</th>
                        <th>Quote Date</th>
                        <th>Expiration Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $order->shipping_name }}</td>
                        <td>{{ $order->product_name }}</td>
                        <td>{{ date('m/d/Y',strtotime($order->created_at)) }}</td>
                        <td>@if($order->expire_date) {{ date('m/d/Y',strtotime($order->expire_date)) }} @endif</td>
                    </tr>
                </tbody>
            </table>

            <table class="table order_table" style="margin-bottom: 20px;">
                <thead>
                    <tr>
                        <th>WEIGHT (@if($order->units==1){{"KG"}}@else{{"LB"}}@endif)</th>
                        <th>VOLUME (@if($order->units==1){{"CBM"}}@else{{"CFT"}}@endif)</th>
                        <th>SHIPPING PORT</th>
                        <th># OF UNITS</th>
                        <th>TERMS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $order->chargeable_weight }}</td>
                        <td>{{ $order->shipment_volume }}</td>
                        <td>{{ $order->shipping_port }}</td>
                        <td>{{ $order->total }}</td>
                        <td>{{ $order->incoterm_short_name }}</td>
                    </tr>
                </tbody>
            </table>

            <table class="table margin-bottom-0 order_table" style="margin-bottom: 10px;">
                <thead>
                    <tr>
                        <th class="col-md-9">charge</th>
                        <th class="col-md-3">Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-md-9 text-left">PRE-SHIPMENT</td>
                        <td class="col-md-3 text-right" id="international_shipping">${{ $sum_pre_shipment }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-9 text-left">INTERNATIONAL SHIPPING & PORT FEES</td>
                        <td class="col-md-3 text-right" id="international_shipping">${{ $sum_shipping_total }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-9 text-left">U.S. CUSTOMS BROKERAGE</td>
                        <td class="col-md-3 text-right" id="custom_sub_total">${{ $sum_custom_total }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-9 text-left">RECEIVING & FORWARDING</td>
                        <td class="col-md-3 text-right" id="receive_forward_sub_total">${{ $sum_receive_forward_total }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-9 text-left">PREP & INSPECTION</td>
                        <td class="col-md-3 text-right" id="prep_sub_total">${{ $sum_prep_total }}</td>
                    </tr>
                </tbody>
            </table>

            <div class="col-md-12" style="padding:0px">
                <table class="table margin-bottom-0 no-border order_table" style="margin-bottom:0px;">
                    <tr>
                        <td class="col-md-9 no-border" style="text-align:right;">TOTAL COST :</td>
                        <td class="col-md-3 border-box" style="width:25%;margin-right:30%;" id="grand_total">${{ $sum_shipping_total + $sum_custom_total + $sum_receive_forward_total + $sum_prep_total + $sum_pre_shipment }}</td>
                    </tr>
                </table>
            </div>

            <table class="table margin-bottom-0 no-border order_table" style="margin-top: 10px;margin-bottom:10px;">
                <tr>
                    <td class="col-md-9" style="text-align:right;">PER UNIT COST:</td>
                    <td class="col-md-3 border-box" id="per_unit_cost">${{ number_format(($sum_shipping_total + $sum_custom_total + $sum_receive_forward_total + $sum_prep_total + $sum_pre_shipment)/$order->total,2) }}</td>
                </tr>
            </table>

        <!--<div class="col-md-12 text-center margin-top-60" style="margin-top: 65px;">
            <h4 class="grey-font">We look forward to helping you build, grow, and scale your e-commerce business!</h4>
        </div>

        <div class="col-md-12 contact-info text-center" style="position:relative;">
            <div class="col-md-12" style="position:relative;">
                <p class="col-md-12">If you have any questions about this quote, please call or email our order support team:</p>
                <div class="col-md-3 col-md-offset-3 text-left" style="float:left;;margin-left:12%;">
                    Phone:+1 866 526 3951
                </div>

                <div class="col-md-3 text-left" style="float:left;margin-left:52%;">
                    Email:support@fbaforward.com
                </div>
            </div>
        </div> -->

        <div class="col-md-12 text-center" style="position:relative;margin-bottom:0px;">
            <h4 class="grey-font" style="margin:0px 0px;">We look forward to helping you build, grow, and scale your e-commerce business!</h4>
        </div>

        <div class="col-md-12" style="position:relative;margin-bottom:0px;">
            <div class="col-md-12 contact-info text-center" style="position:relative;">
                <div class="col-md-12">
                    <p class="col-md-12" style="position:relative;">If you have any questions about this quote, please call or email our order support team:</p>
                    <div class="col-md-3 col-md-offset-3 text-left" style="width:27%;float:left;margin-left:12%;position:relative;">
                        Phone:+1 866 526 3951
                    </div>

                    <div class="col-md-3 text-left" style="float:left;margin-left:50%;position:relative;"> Email:support@fbaforward.com
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 text-center grey-font margin-top-60 border-box" style="position:relative;float:left;margin-top:30px;">
            <p>The above quote is valid for 7 days.  Quoted amounts are subject to change as shipping rates and service amounts fluctuate, however changes of greater than 5% are very unlikely.  Port fees are an estimate based off of our preivous experiences, and generally fall between $200 and $300 for sea shipments, and $50 and $150 for air shipments, however can be significantly increased if your shipment is flagged for a random customs x-ray or intensive exam.  Customs duties vary by product and are not included in this quote.  You can find the duty rate for your product by visiting www.dutycalculator.com.  All charges quoted are from third party service providers, and are aggregated and paid by FBAforward as a courtesy, and costs are passed on to you at face value.  FBAforward is not a provider of freight forwarding or customs brokerage services. </p>
        </div>

    </div>

    <div class="row ">
        <div class="col-md-12 ">
            <div class="page-break"></div>
            <h1 class="heading text-center">FEE BREAKDOWN</h1>
            <div class="col-md-12">
                <h4 style="margin:0px;">PRE-SHIPMENT</h4>
                <table class="table order_table" style="margin-bottom:10px;">
                    <thead>
                        <tr>
                            <th class="col-md-6 text-left">Pre-Shipment</th>
                            <th class="col-md-2">Rate</th>
                            <th class="col-md-2">Qty</th>
                            <th class="col-md-2">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($pre_shipment as $pre_shipments)
                    <tr>
                        <td class="col-md-6 text-left">{{ $pre_shipments->name }}</td>
                        <td class="col-md-">${{ $pre_shipments->rate }}</td>
                        <td class="col-md-2">{{ $order->no_boxs }}</td>
                        <td class="col-md-2">${{ ($order->no_boxs * $pre_shipments->rate) }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td class="text-left">SUB TOTAL</td>
                        <td>-</td>
                        <td>-</td>
                        <td>$ {{ $sum_pre_shipment }}</td></td>
                    </tr>
                    </tbody>
                </table>

                <h4 style="margin:0px;">SHIPPING</h4>
                <table class="table order_table" style="margin-bottom:10px;">
                    <thead>
                        <tr>
                            <th class="col-md-6 no-border text-left">Line Item</th>
                            <th class="col-md-3 no-border">Rate</th>
                            <th class="col-md-3 no-border">Quantity</th>
                            <th class="col-md-3 no-border">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$order->shipping_name}}</td>
                            <td class="col-md-3 no-border">${{$order->shipment_type_rate}}</td>
                            <td class="col-md-3 no-border">{{$order->shipment_volume}}</td>
                            <td class="col-md-3 no-border">${{$order->shipment_type_rate*$order->shipment_volume}}</td>
                        </tr>
                       
                        @foreach($detail_charge as  $detail_charges)
                            <?php
                            $charge_price = $detail_charges->charge * $charge_qty;
                           
                            ?>
                            <tr>
                                <td class="col-md-6 no-border text-left">{{ $detail_charges->name }}</td>
                                <td class="col-md-3 no-border">${{ $detail_charges->charge }}</td>
                                <td class="col-md-3 no-border">{{ $charge_qty }}</td>
                                <td class="col-md-3 no-border">${{ $charge_price }}</td>
                            </tr>
                        @endforeach
                        @if($order->shipping_name!=="undefined")
                            @if($order->shipping_method_id  == 2)
                                {{--*/  $port_fee= env('PORT_FEE')/*--}}
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$port_fee_name}}</td>
                                    <td class="col-md-3 no-border">${{$port_fee}}</td>
                                    <td class="col-md-3 no-border">{{$charge_qty}}</td>
                                    <td class="col-md-3 no-border">${{$port_fee*$charge_qty}}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-6 no-border text-left"> {{$trucking_name}}</td>
                                    <td class="col-md-3 no-border">${{$trucking_price}}</td>
                                    <td class="col-md-3 no-border">{{$trucking_qty}}</td>
                                    <td class="col-md-3 no-border">${{$trucking_price}}</td>
                                </tr>
                            @elseif($order->shipping_method_id  == 1)
                                @if($order->shipment_type_id==1)
                                    {{--*/ $port_fee= env('OCEAN_LCL_PORT_FEE') /*--}}
                                @else
                                    {{--*/ $port_fee= env('OCEAN_PORT_FEE') /*--}}
                                @endif
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$port_fee_name}}</td>
                                    <td class="col-md-3 no-border">${{$port_fee}}</td>
                                    <td class="col-md-3 no-border">{{$charge_qty}}</td>
                                    <td class="col-md-3 no-border">${{$port_fee*$charge_qty}}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$trucking_name}}</td>
                                    <td class="col-md-3 no-border">${{$order->trucking_rate}}/pallet</td>
                                    <td class="col-md-3 no-border">{{$trucking_qty}}</td>
                                    <td class="col-md-3 no-border">${{$trucking_price = ($order->trucking_rate*$trucking_qty) }}</td>
                                </tr>
                            @else
                                {{--*/  $port_fee= env('PORT_FEE')/*--}}
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$port_fee_name}}</td>
                                    <td class="col-md-3 no-border">${{$port_fee}}</td>
                                    <td class="col-md-3 no-border">{{$charge_qty}}</td>
                                    <td class="col-md-3 no-border">${{$port_fee*$charge_qty}}</td>
                                </tr>
                                {{--*/ $trucking_price = 0 /*--}}
                            @endif
                        @endif
                        <tr>
                            <td class="col-md-6 no-border text-left">{{ env('WIRE_TRANS_NAME') }}</td>
                            <td class="col-md-3 no-border">${{ env('WIRE_TRANS_FEE') }}</td>
                            <td class="col-md-3 no-border">&nbsp;</td>
                            <td class="col-md-3 no-border">${{ env('WIRE_TRANS_FEE') }}</td>
                        </tr>
                        
                        <tr>
                            <td class="col-md-6 no-border text-left">SUBTOTAL</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">$ {{ $sum_shipping_total }}</td></td>
                        </tr>
                    </tbody>
                </table>

                <h4 style="margin:0px;">U.S. CUSTOMS BROKERAGE</h4>
                <table class="table order_table" style="margin-bottom:10px;">
                    <thead>
                        <tr>
                            <th class="col-md-6 no-border text-left">Line Item</th>
                            <th class="col-md-3 no-border">Rate</th>
                            <th class="col-md-3 no-border">Quantity</th>
                            <th class="col-md-3 no-border">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($order->shipping_method_id == 1)
                            <tr>
                                <td class="col-md-6 no-border text-left">{{$isf_name}}</td>
                                <td class="col-md-3 no-border">${{$isf_rate}}</td>
                                <td class="col-md-3 no-border">{{$charge_qty}}</td>
                                <td class="col-md-3 no-border">${{$isf_rate * $charge_qty}}</td>
                            </tr>
                            
                        @endif
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$entry_name}}</td>
                            <td class="col-md-3 no-border">${{$entry_rate}}</td>
                            <td class="col-md-3 no-border">{{$charge_qty}}</td>
                            <td class="col-md-3 no-border">${{ $entry_rate*$charge_qty}}</td>
                        </tr>
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$bond_name}}</td>
                            <td class="col-md-3 no-border">${{$bond_rate}}</td>
                            <td class="col-md-3 no-border">{{$charge_qty}}</td>
                            <td class="col-md-3 no-border">${{$bond_rate*$charge_qty}}</td>
                       
                        </tr>
                        <tr>
                            <td class="col-md-6 no-border text-left">SUBTOTAL</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">$ {{ $sum_custom_total }}</td></td>
                        </tr>
                    </tbody>
                </table>

                <h4 style="margin:0px;">RECEIVING & FORWARDING</h4>
                <table class="table order_table" style="margin-bottom:10px;">
                    <thead>
                        <tr>
                            <th class="col-md-6 no-border text-left">Line Item</th>
                            <th class="col-md-3 no-border">Rate</th>
                            <th class="col-md-3 no-border">Quantity</th>
                            <th class="col-md-3 no-border">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                         @if($order->shipment_type_id >1)
                            <tr>
                                <td class="col-md-6 no-border text-left">container_unloading</td>
                                <td class="col-md-3 no-border">${{$order->container_unload_rate}}</td>
                                <td class="col-md-3 no-border">{{$charge_qty}}</td>
                                <td class="col-md-3 no-border">${{$order->container_unload_rate*$charge_qty}}</td>
                            </tr>
                        @endif
                       
                        @if($order->outbound_method_id >1)
                            <tr>
                                <td class="col-md-6 no-border text-left">{{$palletizing}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge}}</td>
                                <td class="col-md-3 no-border">{{$trucking_qty}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge*$trucking_qty}}</td>
                            </tr>
                            
                            <tr>
                                <td class="col-md-6 no-border text-left">{{$amazon_approve_pallet}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge}}</td>
                                <td class="col-md-3 no-border">{{$trucking_qty}}</td>
                                <td class="col-md-3 no-border">${{$pallet_charge*$trucking_qty}}</td>
                            </tr>
                            
                        @endif
                        <tr>
                            <td class="col-md-6 no-border text-left">{{$order->outbound_name}}</td>
                            <td class="col-md-3 no-border">&nbsp;</td>
                            <td class="col-md-3 no-border">&nbsp;</td>
                            <td class="col-md-3 no-border"><i>varies</i></td>
                        </tr>
                        <tr>
                            <td class="col-md-6 no-border text-left">SUBTOTAL</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">${{ $sum_receive_forward_total }}</td>
                        </tr>
                    </tbody>
                </table>

                <!-- <div class="page-break"></div> -->
                <h4 style="margin:0px;">PREP & INSPECTION</h4>
                <table class="table order_table" style="margin-bottom:10px;">
                    <thead>
                        <tr>
                            <th class="col-md-6 no-border text-left">Line Item</th>
                            <th class="col-md-3 no-border">Rate</th>
                            <th class="col-md-3 no-border">Quantity</th>
                            <th class="col-md-3 no-border">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($detail_prep as $prep_key=>$prep_value )
                            {{--*/$detail_prep = $prep_value->price * $order->qty_per_box/*--}}
                            @if($detail_prep % 1 !=0)
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$prep_value->service_name}}</td>
                                    <td class="col-md-3 no-border">${{$prep_value->price}}</td>
                                    <td class="col-md-3 no-border">{{$order->qty_per_box}}</td>
                                    <td class="col-md-3 no-border">${{$detail_prep}}</td>
                                    
                                </tr>
                            @else
                                <tr>
                                    <td class="col-md-6 no-border text-left">{{$prep_value->service_name}}</td>
                                    <td class="col-md-3 no-border">${{$prep_value->price}}</td>
                                    <td class="col-md-3 no-border">{{$order->qty_per_box}}</td>
                                    <td class="col-md-3 no-border">${{$detail_prep}}</td>
                                    
                                </tr>
                            @endif
                        @endforeach
                        <tr>
                            <td class="col-md-6 no-border text-left">SUBTOTAL</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">-</td>
                            <td class="col-md-3 no-border">${{ $sum_prep_total }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
</body>
</script>
</html>