@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
    <section class="content">
        <section class="content-header">
            <h1><i class="fa fa-shopping-cart"></i> {{ $title }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><i class="fa fa-shopping-cart"></i>{{ $title }}</li>
            </ol>
        </section>
        <section class="content">
            <div class="box border-top-0">
                <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                    <li class="nav-item col-md-4 nav-tab no-padding active" id="tab">
                        <a class="nav-link active text-capitalize" data-toggle="tab" href="javascript:void(0)"  onclick="tabchange('1')">All Orders </a>
                    </li>
                    <li class="nav-item col-md-4 nav-tab no-padding" id="tab">
                        <a class="nav-link text-capitalize" data-toggle="tab" href="javascript:void(0)" onclick="tabchange('2')">Orders On Track </a>
                    </li>
                    <li class="nav-item col-md-4 nav-tab no-padding" id="tab">
                        <a class="nav-link text-capitalize" data-toggle="tab" href="javascript:void(0)" onclick="tabchange('3')">Orders On Hold </a>
                    </li>
                </ul>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive no-padding">
                                <table id="shipment_data" class="table" >
                                    <thead>
                                    <tr>
                                        <th class="col-md-2">Order #</th>
                                        <th class="col-md-2">Product</th>
                                        <th class="col-md-2">Company</th>
                                        <th class="col-md-2">Check In Date</th>
                                        <th class="col-md-2">Due Date</th>
                                        <th class="col-md-1">Status</th>
                                        <th class="col-md-1">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($order))
                                        @foreach($order as $orders)
                                            <tr>
                                                <td class="col-md-2">{{ $orders['order_no'] }}</td>
                                                <td class="col-md-2">{{ substr($orders['product_name'],0,35) }} @if(strlen($orders['product_name']) > 35) {{ "..." }} @endif</td>
                                                <td class="col-md-2">{{ $orders['company_name'] }}</td>
                                                <td class="col-md-2">&nbsp;</td>
                                                <td class="col-md-2">&nbsp;</td>
                                                <td class="col-md-1">{{ $orderStatus[$orders['is_activated']] }}</td>
                                                <td class="col-md-1"><a href="{{ url('order/detail/'.$orders['shipping_method_id'].'/'.$orders['customer_requirement_id']) }}" class="button">View</a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
@section('js')
    <script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#shipment_data').DataTable({
                "bSort" : true,
                "bPaginate": false,
                "bFilter": true,
                "bInfo": false,
            });
        });
    </script>
    <script type="text/javascript">
        function tabchange(tab_id){
            var html= '';
            var url = '{{ url('') }}';
            $.ajax({
                headers: {
                    'X-CSRF-Token':  "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '/warehouse/getorder',
                data: { 'tab_id': tab_id },
                success: function (response) {
                    html +='<table id="shipment_data" class="table" >';
                    html +='<thead><tr>';
                    html +='<th class="col-md-2">Order Number</th>';
                    html +='<th class="col-md-2">Product</th>';
                    html +='<th class="col-md-2">Company</th>';
                    html +='<th class="col-md-2">Check In Date</th>';
                    html +='<th class="col-md-2">Due Date</th>';
                    html +='<th class="col-md-1">Status</th>';
                    html +='<th class="col-md-1">Action</th>';
                    html +='</tr></thead>';
                    html += '<tbody>';
                        $.each(response.order, function (index, value) {
                            html += '<tr>';
                            html += '<td class="col-md-2">' + value.order_no + '</td>';
                            html += '<td class="col-md-2">' + value.product_name.substr(0, 35) + '</td>';
                            html += '<td class="col-md-2">' + value.company_name + '</td>';
                            html += '<td class="col-md-2">&nbsp;</td>';
                            html += '<td class="col-md-2">&nbsp;</td>';
                            html += '<td class="col-md-1">' + response.orderStatus[value.is_activated] + '</td>';
                            html += '<td class="col-md-1"><a href="'+url+'/order/detail/'+value.shipping_method_id+'/'+value.customer_requirement_id+'" class="button">View</a></td>';
                            html += '</tr>';
                        });
                    html +='</tbody>';
                    html +='</table>';
                    $('#shipment_data').html(html);
                }
            });
        }
    </script>
@endsection