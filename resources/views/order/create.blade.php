@extends('layouts.member.app')
@section('title', 'Create Order')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/custom_quotes.css') !!}
<style>
.height81{ min-height: 81px !important; }
.height41{ height: 41px !important; }
.productformError, .parentFormvalidate, .formError{  opacity: 0.87 !important;  position: absolute !important;  top: 3px !important;  left: 300px !important;  margin-top: 0px !important;  }
.fa-edit{ cursor: pointer; }
.blue-circle{  min-height: 135px;  background: #005f8e;  border-radius: 50%;  }
.icon-link{    font-size: 4em;  color: #fff;  padding: 39px 27px;  }
.grey-font{ color: #7c7d7f; }
.btn-setting{ background: #ffffff;  border: 1px solid #00a4f6;  color: #00a4f6;  padding: 5px 15px;  border-radius: 4px;  }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1> Place Order </h1>
        <small>Review order and make edits where necessary. Choose add-on products, if desired.</small>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('quote') }}"><i class="fa fa-anchor"></i>Quote</a></li>
            <li class="active"><a href="{{ url('quote/edit/') }}">  <!-- @if(!empty($customer_requirement)) Request Again @else Quote Details @endif --> Place Order</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
             <div class="box-body no-padding">
                <div class="row">
                    <div class="col-md-12 ">
                        @if($place_order->order_type != 1)
                        <div class="col-md-12 no-padding">
                            <div class="col-md-6 no-padding top-border height40">
                                    <div class="col-md-12 product-data text-overflow border-left-1 no-padding height40">
                                        <b class="col-md-2">Product:</b>
                                        @if(!empty($place_order->product_name))
                                           {{ $place_order->product_name }}
                                        @else
                                            <input type="hidden" name="customer_requirement_details_id" value="{{ $place_order->customer_requirement_details_id }}">
                                        <div class="col-md-10 no-padding">
                                            <select name="product" id="product" class = " validate[required] select2 height40" style="width:100%;" data-prompt-position="bottomRight:-100">
                                                <option class="col-md-6" value=""></option>
                                                @foreach($product as $products)
                                                    <option value="{{ $products->id.' '.$products->FNSKU }}">{{ $products->product_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @endif
                                    </div>
                            </div>
                            <div class="col-md-6 no-padding top-border height40">
                                <div class="col-md-12 product-data">
                                    <b>Product Nickname (Optional):</b> {{ $place_order->product_nick_name }}
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($place_order->order_type == 1)
                        <div class="col-md-12 no-padding">
                            <div class="col-md-3 big-order-box product-data height40 border-left-1" style="height:40px;">
                                    @if($place_order->length)
                                        <b>Dimensions:</b>&nbsp; <span id="l">{{ $place_order->length }}</span> &nbsp;<b>x</b> &nbsp;<span id="w">{{ $place_order->width }}</span> &nbsp;<b>x</b> &nbsp;<span id="h">{{ $place_order->height }}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="unit">{{ $place_order->unit }}</span>
                                    @else
                                        <b>Dimensions:</b>&nbsp; <span id="l">L</span>  <b>x</b> &nbsp;<span id="w">W</span> &nbsp;<b>x</b><span id="h">H</span>&nbsp;<span id="unit">unit</span>
                                            <input type="text" class="validate[] no-display" name="dimensions">
                                    @endif
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#dimensionModal"></i>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Units : </b><span> {{ $place_order->units }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Cartons: </b><span> {{ $place_order->cartoons }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Weight : </b><span> {{ $place_order->weight }} @if($place_order->customer_requirement_units==1){{"KG"}}@else{{"LB"}}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Volume : </b><span> {{ $place_order->volume }} @if($place_order->customer_requirement_units==1){{"CBM"}}@else{{"CFT"}}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding left-border grey-bg">
                                <div class="col-md-12 product-data ">
                                    <b>Inbound Shipping Method : </b><span>{{ $shipping_method->shipping_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data ">
                                    <b>Type: </b><span>{{ $place_order->shipment_short_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Incoterms : </b><span>{{ $place_order->incoterm_short_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg ">
                                <div class="col-md-12 product-data text-overflow">
                                    <b>Port of Origin : </b><span>{{ $place_order->port_of_origin }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                     <b>Port of Arrival:</b><span>
                                        @if($shipping_method->shipping_method_id==1)
                                            Long Beach
                                        @elseif($shipping_method->shipping_method_id==2 || $shipping_method->shipping_method_id==3)
                                            San Diego
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </div>
                        @elseif($place_order->order_type == 2)
                            <div class="col-md-12 no-padding">
                            <div class="col-md-3 big-order-box product-data height40 border-left-1">
                                    @if($place_order->length)
                                        <b>Dimensions:</b>&nbsp; <span id="l">{{ $place_order->length }}</span> &nbsp;<b>x</b> &nbsp;<span id="w">{{ $place_order->width }}</span> &nbsp;<b>x</b> &nbsp;<span id="h">{{ $place_order->height }}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="unit">{{ $place_order->unit }}</span>
                                    @else
                                        <b>Dimensions:</b>&nbsp; <span id="l">L</span> &nbsp;<b>x</b> &nbsp;<span id="w">W</span> &nbsp;<b>x</b> &nbsp;<span id="h">H</span>&nbsp;&nbsp;&nbsp;<span id="unit">unit</span>
                                        <input type="text" class="validate[] no-display" name="dimensions">
                                    @endif
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#dimensionModal"></i>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Units : </b><span> {{ $place_order->units }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Cartons: </b><span> {{ $place_order->cartoons }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Weight : </b><span> {{ $place_order->weight }} @if($place_order->customer_requirement_units==1){{"KG"}}@else{{"LB"}}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Volume : </b><span> {{ $place_order->volume }} @if($place_order->customer_requirement_units==1){{"CBM"}}@else{{"CFT"}}@endif</span>
                                </div>
                            </div>
                            <div class="big-order-box no-padding left-border grey-bg">
                                <div class="col-md-12 product-data">
                                    <b>Inbound Shipping Method : </b><span>{{ $shipping_method->shipping_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg" style="width:32% !important;">
                                <div class="col-md-12 product-data">
                                    <b>Type: </b><span>{{ $place_order->type }}</span>
                                </div>
                            </div>
                            <div class="col-md-6 no-padding" style="width:42%;" >
                                <div class="col-md-12 product-data">
                                    <b>Outbound Shipping Method : </b><span>{{ $place_order->outbound_shipping_method }}</span>
                                </div>
                            </div>
                        @else
                            <div class="col-md-12 no-padding">
                            <div class="col-md-3 big-order-box product-data height41 border-left-1">
                                    @if($place_order->length)
                                        <b>Dimensions:</b>&nbsp; <span id="l">{{ $place_order->length }}</span> &nbsp;<b>x</b> &nbsp;<span id="w">{{ $place_order->width }}</span> &nbsp;<b>x</b> &nbsp;<span id="h">{{ $place_order->height }}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="unit">{{ $place_order->unit }}</span>
                                    @else
                                        <b>Dimensions:</b>&nbsp; <span id="l">L</span> &nbsp;&nbsp; <b>x</b> &nbsp;<span id="w">W</span> &nbsp;<b>x</b> &nbsp;<span id="h">H</span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="unit">unit</span>
                                        <input type="text" class="validate[] no-display" name="dimensions">
                                    @endif
                                    <i class="fa fa-edit" data-toggle="modal" data-target="#dimensionModal"></i>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data ">
                                    <b>Units : </b><span> {{ $place_order->units }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data ">
                                    <b>Cartons: </b><span> {{ $place_order->cartoons }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data ">
                                    <b>Weight : </b><span> {{ $place_order->weight }} @if($place_order->customer_requirement_units==1){{"KG"}}@else{{"LB"}}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data ">
                                    <b>Volume : </b><span> {{ $place_order->volume }} @if($place_order->customer_requirement_units==1){{"CBM"}}@else{{"CFT"}}@endif</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding left-border grey-bg">
                                <div class="col-md-12 product-data height56">
                                    <b>Inbound Shipping Method : </b><span>{{ $shipping_method->shipping_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data height56">
                                    <b>Type: </b><span>{{ $place_order->shipment_short_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding grey-bg">
                                <div class="col-md-12 product-data height56 text-overflow">
                                    <b>Port of Origin : </b><span>{{ $place_order->port_of_origin }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 small-order-box no-padding">
                                <div class="col-md-12 product-data height56">
                                    <b>Outbound Shipping Method : </b><span>{{ $place_order->outbound_short_name }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 big-order-box no-padding">
                                <div class="col-md-12 product-data height56">
                                    <b>Date of Arrival at FBAforward: </b><span> </span>
                                    <input placeholder="DD-MM-YYYY" id="date_of_arrival_datepicker" type="text" class="form-control datepicker col-md-12 no-padding no-margin height23">
                                </div>
                            </div>
                            <div class="col-md-12 no-padding">
                                <div class="col-md-12 product-data left-border">
                                    <b>Shipping Notes:</b><span>{{ $place_order->shipment_notes }}</span>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 blank-class"></div>
        <form method="post" id="validate" action="{{ url('member/order/update_order') }}">
            {{ csrf_field() }}
            <input type="hidden" name="customer_requirement_id" value="{{ $place_order->id }}">
            <input type="hidden" name="update_products" value="yes">
            <input type="hidden" name="shipping_method" value="{{ $shipping_method->shipping_method_id }}">
            <div class="row">
                <div class="col-md-12">
                    <div class="box2 col-md-12 no-padding">
                        <div class="box-header with-border quote-summery-heading no-border">
                            <h3 class="box-title">Confirm Product Details</h3>
                        </div>
                        <div class="box-body padding15">
                            <div class="col-md-6 no-padding">
                                <div class="col-md-12 no-padding">
                                    <b class="col-md-12">Product: </b>
                                        <div class="col-md-12">
                                            <select name="product" id="product" class = "validate[required] select2 height40" style="width:100%;margin-left: 2%;" data-prompt-position="bottomRight:-100">
                                                <option class="col-md-6" value=""></option>
                                                @foreach($product as $products)
                                                    <option value="{{ $products->id.' '.$products->FNSKU }}" @if($place_order->product_id==$products->id) selected @endif>{{ $products->product_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                </div>
                                <div class="col-md-12 no-padding">
                                    <b class="col-md-12">Product Nickname (Optional):</b>
                                    <input type="text" name="product_name" class="form-control" value="{{ $place_order->product_nick_name }}" style="margin-left: 13px;">
                                </div>
                                <div class="col-md-12 no-padding">
                                    <b class="col-md-12">Supplier: </b>
                                    <div class="col-md-12">
                                        <select name="supplier" id="supplier" class = "select2 height40" style="width:100%;margin-left: 2%;" data-prompt-position="bottomRight:-100">
                                            <option class="col-md-6" value=""></option>
                                            @foreach($suppliers as $supplier)
                                                <option value="{{ $supplier->supplier_id }}" @if($place_order->supplier==$supplier->supplier_id) selected @endif>{{ $supplier->contact_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12 no-padding">
                                    <b class="col-md-12">Outbound Shipping Method:</b>
                                    <select name="outbound" class="col-md-12 form-control validate[required]" style="margin-left: 2%;">
                                        @foreach($outbound_method as $outbound_methods)
                                            <option value="{{$outbound_methods->outbound_method_id}}" @if($outbound_methods->outbound_method_id==$place_order->outbound_method_id) selected @endif>{{ $outbound_methods->outbound_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 no-padding">
                                    <b class="col-md-12">Approx. Goods Ready Date:</b>
                                    <input type="text" class="form-control  validate[required] datepicker" value="@if($place_order->apporx_goods_ready_date) {{ \Carbon\Carbon::parse($place_order->apporx_goods_ready_date)->format('m/d/Y') }} @endif" name="goods_ready_date">
                                </div>
                                <div class="col-md-12 no-padding">
                                    <b class="col-md-12">Shipping Notes:</b>
                                    <textarea class="form-control" name="shipment_notes" style="margin-left:2%;">{{ $place_order->shipment_notes }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 button-box">
                    <div class="pull-right">
                        @if($customer_amazon_detail->mws_seller_id)
                            <a><button type="submit" class="button col-md-12">Review Order & Pay</button></a>
                        @else
                            <a data-toggle="modal" data-target="#amazonModal" class="button col-md-12">Review Order & Pay</a>
                        @endif
                    </div>
                </div>
            </div>
        </form>
    </section>
<div id="dimensionModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Dimensions</h4>
      </div>
      <div class="modal-body">
        <form id="validate2" class="form-horizontal" method="post" action="{{ url('quote/add-dimensions') }}">
            {{ csrf_field() }}
            <input type="hidden" name="customer_requirement_details_id" value="{{ $place_order->customer_requirement_details_id }}">
             <div class="form-group">
                <div class="col-md-2">
                    <label class="control-label line-height-2">Unit : </label>
                </div>
                <div class="col-md-10">
                    <select class="form-control margin-left-2  validate[required]" name="unit">
                        <option value="cm" @if($place_order->unit == 'cm') {{"selected"}} @endif>Centimeter</option>
                        <option value="in" @if($place_order->unit == 'in') {{"selected"}} @endif>Inch</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label class="control-label line-height-2">Length : </label>
                </div>
                <div class="col-md-10">
                    <input class="form-control validate[required] custom[integer]" type="text" name="length" value="@if($place_order->length){{$place_order->length}}@endif">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label class="control-label line-height-2">Width : </label>
                </div>
                <div class="col-md-10">
                    <input class="form-control validate[required] custom[integer]" type="text" name="width" value="@if($place_order->width){{$place_order->width}}@endif">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label class="control-label line-height-2">Height : </label>
                </div>
                <div class="col-md-10">
                    <input class="form-control validate[required] custom[integer]" type="text" name="height" value="@if($place_order->height){{$place_order->height}}@endif">
                </div>
            </div>
      </div>
      <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Add Dimension</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="amazonModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
            <div class="col-md-12">
                <div class="col-md-3 col-md-offset-4 blue-circle">
                    <i class="fa fa-link icon-link"></i>
                </div>
            </div>
            <div class="col-md-12 text-center grey-font">
                <h2>Connect To Your Amazon Account</h2>
                <p>You need to integrate your amazon account to place an order.</p>
                <p>It should only take few minutes.</p>
            </div>
            <div class="col-md-5 col-md-offset-4">
                <a href="{{ url('settings') }}"><button class="btn-setting">Go to Settings</button></a>
            </div>
      </div>
      <div class="modal-footer no-border">
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach', {
                promptPosition: "bottomRight", scroll: false,
                prettySelect: true,
                usePrefix: prefix
            });
            $("form[id^='order_form_validate']").validationEngine('attach', {
                promptPosition: "bottomRight", scroll: false,
                prettySelect: true,
                usePrefix: prefix
            });
            $("form[id^='dimensions_validate_gfgsg']").validationEngine('attach', {
                promptPosition: "bottomRight", scroll: false,
                prettySelect: true,
                usePrefix: prefix
            });
            $("form[id^='validate2']").validationEngine('attach', {
                promptPosition: "bottomRight", scroll: false,
                prettySelect: true,
                usePrefix: prefix
            });
            $(".select2").select2({
                placeholder: "Please Select",
                allowClear: true
            });
            $('.select2-selection').addClass('height40');
            $('.select2').click(function(){
                $('.select2-search__field').addClass('no-margin');
            });
        });
    </script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.datepicker').datepicker({
            startDate: new Date(),
        });
    });

    $('#date_of_arrival_datepicker').change(function(){
        $('#date_of_arrival').val($('#date_of_arrival_datepicker').val());
    });
    $('#product').change(function(){
        var product_id = $('#product').val();
        $.ajax({
            headers: {
                'X-CSRF-Token': "{{ csrf_token() }}"
            },
            method: 'POST',
            url: '{{ url("quote/get-supplier") }}',
            data: {
                '_token': '{{ csrf_token() }}','product_id': product_id,
            },
            success: function (supplier) {
                var html = '';
                html+= '<select name="supplier" id="supplier" class = "validate[required] select2">';
                if(supplier.length ==0){
                   html+= '<option value=""></option>';
                }
                else{
                    $.each(supplier, function( key, value ) {
                        html+= "<option value="+value.supplier_id+">"+value.contact_name+"</option>";
                    });
                }
                $('#supplier').html(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    });
</script>
@endsection