@extends('layouts.member.app')
@section('title', 'TelexRelease')
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-file-text-o"></i> Telex Release</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-file-text-o"></i> Telex Release</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <table id="TelexRelease" class="table" >
                    <thead>
                    @if($user->role_id=='5')
                        <tr>
                            <th class="col-md-3">Order Number</th>
                            <th class="col-md-5">Product</th>
                            <th class="col-md-2">Company</th>
                            <th class="col-md-2">Enter New Telex</th>
                        </tr>
                    @elseif($user->role_id=='6')
                        <tr>
                            <th class="col-md-3">Order Number</th>
                            <th class="col-md-5">Product</th>
                            <th class="col-md-2">Company</th>
                            <th class="col-md-1">Date Ordered</th>
                            <th class="col-md-1">View Telex</th>
                        </tr>
                    @endif
                    </thead>
                    <tbody>
                        @foreach($order as $orders)
                            <tr>
                                <td>{{ $orders->order_no }}</td>
                                <td>{{ $orders->product_name }}</td>
                                <td>{{ $orders->company_name }}</td>
                                @if($user->role_id=='6')
                                    <td>{{ date('m/d/Y',strtotime($orders->created_at)) }}</td>
                                @endif
                                <td>
                                    @if($user->role_id=='5')
                                        <a class="button" href="{{ url('telexrelease/create?id='.$orders->order_id) }}">Submit Telex</a>
                                    @elseif($user->role_id=='6')
                                    <a class="button" href="{{ url('telexrelease/'.$orders->order_id) }}">View Telex</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @endsection
@section('js')
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('#TelexRelease').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
    });
</script>
@endsection