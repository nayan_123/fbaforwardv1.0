@extends('layouts.member.app')
@section('title', 'View Telex Release')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
<style>
.col-md-12 {  padding-left: 35px;  }
.pdf-name:hover{  color: #3c8dbc !important;  }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1>View Telex Release</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('telexrelease') }}"><i class="fa fa-shopping-cart"></i> Telex Release</a></li>
            <li class="active">View Telex Release</li>
        </ol>
    </section>
    <section class="content">
        <div class="box border-top-0">
            <div class="box-body ">
                <div class="row ">
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                <td class="col-md-6">Order #  {{ $telex->order_no }}</td>
                                <td class="col-md-6">Company: {{ $telex->company_name }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <div class="col-md-12"><h4>Telex Release Files</h4></div>
                    <div class="col-md-12">
                        <p><a class="pdf-name" target="_blank" href="{{ url('uploads/telexrelease',$telex->file) }}">{{ $telex->file }}</a></p>
                    </div>
            </div>
            <div class="box-body" id="file_div">
                <div class="col-md-12">
                    <iframe class="col-md-12 no-padding" id="fileframe" height="500" src="{{ url('/uploads/telexrelease/'.$telex->file) }}" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
@endsection