@extends('layouts.member.app')
@section('title', 'Supply Chain')
@section('css')
<style>
.item-title span, input { margin: 10px 0px !important; }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-cog"></i> Supply Chain
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class=""><a href="{{ url('supplychain') }}"><i class="fa fa-cog"></i>Supply Chain</a></li>
            <li class="active">Manage Supplier</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Manage Supplier</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="container">
                    <div class="row">
                        {!! Form::open(['url' =>  isset($supplier) ? 'supplychain/'.$supplier->supplier_id  :  'supplychain', 'method' => isset($supplier) ? 'put' : 'post', 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label" style="text-align:left">SUPPLIER CONTACT NAME </label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-10">
                                        {!! Form::text('contact_name', old('conatct_name', isset($supplier) ? $supplier->contact_name: null), ['class' => 'form-control validate[required]']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 control-label" style="text-align:left">SUPPLIER PHONE NUMBER </label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-10">
                                        {!! Form::text('phone_number', old('phone_number', isset($supplier) ? $supplier->phone_number: null), ['class' => 'form-control validate[required, custom[integer]]']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 control-label" style="text-align:left">SUPPLIER EMAIL </label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-10">
                                        {!! Form::text('email', old('email', isset($supplier) ? $supplier->email: null), ['class' => 'form-control validate[required] custom[email]']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 control-label" style="text-align:left">NICKNAME (OPTIONAL)</label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-10">
                                        {!! Form::text('nick_name', old('nick_name', isset($supplier) ? $supplier->nick_name: null), ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 control-label" style="text-align:left">City, Province </label>
                                <div class="col-md-12">
                                    <div class="input-group col-md-10">
                                        {!! Form::text('city', old('city', isset($supplier) ? $supplier->city: null), ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            @if(isset($supplier) && $supplier->second_contact_name!=='')
                                <div id="second_div">
                            @else
                            <div id="second_div" hidden>
                            @endif
                                <h4>Additional Contact</h4>
                                <div class="form-group">
                                    <label class="col-md-12 control-label" style="text-align:left">SUPPLIER CONTACT NAME </label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-10">
                                            {!! Form::text('second_contact_name', old('second_conatct_name', isset($supplier) ? $supplier->second_contact_name: null), ['class' => 'form-control validate[required]']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12 control-label" style="text-align:left">SUPPLIER PHONE NUMBER </label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-10">
                                            {!! Form::text('second_phone_number', old('second_phone_number', isset($supplier) ? $supplier->second_phone_number: null), ['class' => 'form-control validate[required, custom[integer]]']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12 control-label" style="text-align:left">SUPPLIER EMAIL </label>
                                    <div class="col-md-12">
                                        <div class="input-group col-md-10">
                                            {!! Form::text('second_email', old('second_email', isset($supplier) ? $supplier->second_email: null), ['class' => 'form-control validate[required] custom[email]']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                                        @if(isset($supplier) && $supplier->second_contact_name!=='')
                                            <div style="text-align: right; font-weight: bold" hidden>
                                        @else
                            <div class="col-md-10" style="text-align: right; font-weight: bold">
                                @endif
                                <a onclick="getadditional()" id="additional_button">+ ADD ADDITIONAL CONTACT</a>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 control-label" style="text-align:left">PRODUCT </label>
                                <div class="col-md-12">
                                    <div class="col-md-10 no-padding">
                                        {{--*/ $available_product = array() /*--}}
                                        @if(isset($supplier_product))
                                            @foreach($supplier_product as $supplier_products)
                                                {{--*/ $available_product[] = $supplier_products->product_id /*--}}
                                            @endforeach
                                        @endif
                                        <select name="product[]" id="product" class="form-control validate[required] select2" multiple="multiple">
                                            <option value=""></option>
                                            @foreach($product as $products)
                                                <option value="{{$products->id}}" @if(in_array($products->id,$available_product)) {{ "selected" }} @endif>{{ $products->product_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-9">
                                    {!! Form::submit((isset($supplier)?'Update': 'Add'). ' Supplier', ['class'=>'btn btn-primary']) !!}
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach',
                {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
            $(".select2").select2({
                placeholder: "Please Select",
            });
        });
        function  getadditional() {
            $("#second_div").show();
            $("#additional_button").hide();
        }
    </script>
@endsection
