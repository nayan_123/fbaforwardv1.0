@extends('layouts.member.app')
@section('title', 'Supply Chain')
@section('css')
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
    {!! Html::style('assets/dist/css/style.css') !!}

    <style>
        .dropbtn { padding: 16px;  font-size: 16px;  border: none;  cursor: pointer;  }
        .dropdown { position: relative;  display: inline-block;  }
        .dropdown-content { display: none;  position: absolute;  background-color: #f9f9f9;  min-width: 160px; overflow: auto; box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);  z-index: 1; margin-left: -90px; }
        .dropdown-content a { color: black; padding: 12px 16px;  text-decoration: none; display: block;  }
        .dropdown a:hover { background-color: #f1f1f1 }
        .show { display:block; }
        .margin-right-10{ margin-right: 10px; }
        .dropdown-content{ z-index: 9; }
        .search-label{ line-height: 3.5; }
        .margin-top-10{ margin-top: 10px; }
        .btn-align{ padding-left: 0px;padding-right: 30px; }
        @media (max-width: 1100px){  .search-label{ display:none; }  }
        .btn-manage-product{ margin-top:20px;float:right; }
        .margin-top-10{ margin-top: 10px; }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-globe"></i> Supply Chain</h1>
        <ol class="breadcrumb">
        <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-globe"></i> Supply Chain</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 padding-0-5">
                            <h3 class="margin-top-10">Manage Suppliers</h3>
                        </div>
                        <div class="col-md-6 no-padding">
                            <div class="col-md-12">
                                <div class="col-md-7 no-padding">
                                    <span class="search-label col-md-2">Search:</span>
                                    <div class="col-md-10 padding-right-0">
                                        <input type="text" placeholder="Search" class="form-control input-sm" id="search_all">
                                    </div>
                                </div>
                                <div class="col-md-5 no-padding">
                                    <div class="margin-top-10 btn-align">
                                        <a href="{{ url('supplychain/create') }}" class="btn btn-success pull-right">Add New Supplier</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{--*/ $cnt=1 /*--}}
                        <div class="no-padding">
                            <table id="supplier" class="table">
                                <thead>
                                    <tr>
                                        <th class="col-md-2">Supplier</th>
                                        <th class="col-md-2">Email</th>
                                        <th class="col-md-2">Phone</th>
                                        <th class="col-md-5">Products</th>
                                        <th class="col-md-1"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($supplier as $suppliers)
                                    <tr id="{{$cnt}}">
                                        <td>@if($suppliers->nick_name==''){{ $suppliers->contact_name }}@else{{ $suppliers->nick_name }} @endif</td>
                                        <td>{{ $suppliers->email }}</td>
                                        <td>@if($suppliers->phone_number==0 || empty($suppliers->phone_number)) - @else {{ $suppliers->phone_number }} @endif</td>
                                        <td>
                                            @foreach($supplier_product as $key=>$supplier_products)
                                                @if($key==$suppliers->supplier_id)
                                                    @foreach($supplier_products as $newsupplier)
                                                        {{ $newsupplier }}<br>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <i onclick="myFunction('{{$cnt}}')" class="dropbtn fa fa-cog"></i>
                                                <div id="myDropdown{{$cnt}}" class="dropdown-content">
                                                    <a href="{{ url('supplychain/'.$suppliers->supplier_id.'/edit') }}">Edit</a>
                                                    <a href="javascript:void(0)" onclick="deletesupplier('{{$suppliers->supplier_id}}','{{$cnt}}')">Delete</a>
                                                </div>
                                            </div>
                                            {{--*/ $cnt++ /*--}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="no-padding">
                            <div class="col-md-6 padding-0-5">
                                <h3>Manage Products</h3>
                            </div>
                            <div class="col-md-6">
                                <a onclick="gethideproduct(1)" class="btn btn-primary btn-manage-product" id="viewprod">View Hidden Products</a>
                                            <a onclick="gethideproduct(0)" class="btn btn-primary btn-manage-product" id="hideprod" style="display:none;">Hide Hidden Products</a>
                            </div>
                            <table id="product" class="table" >
                                <thead>
                                    <tr>
                                        <th class="col-md-5">Product</th>
                                        <th class="col-md-3">Nick Name</th>
                                        <th class="col-md-3">Supplier(s)</th>
                                        <th class="col-md-1">

                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($product as $products)
                                    <tr id="{{$cnt}}">
                                        <td>{{ $products->product_name }}</td>
                                        <td>{{ $products->product_nick_name }}</td>
                                        <td>
                                            @foreach($product_supplier as $key=>$product_suppliers)
                                                @if($key==$products->id)
                                                    @foreach($product_suppliers as $newproduct)
                                                        {{ $newproduct }}<br>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <i onclick="myFunction('{{$cnt}}')" class="dropbtn fa fa-cog"></i>
                                                <div id="myDropdown{{$cnt}}" class="dropdown-content">
                                                    <a href="{{ url('inventory/'.$products->id.'/edit') }}">Edit</a>
                                                    <a href="javascript:void(0)" onclick="hideproduct('{{$products->id}}','{{$cnt}}','1')">Hide</a>
                                                </div>
                                            </div>
                                            {{--*/ $cnt++ /*--}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>
@endsection
@section('js')
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    <script type="text/javascript">
    $(document).ready(function() {

        $.fn.dataTableExt.oApi.fnFilterAll = function (oSettings, sInput, iColumn, bRegex, bSmart) {
           var settings = $.fn.dataTableSettings;

           for (var i = 0; i < settings.length; i++) {
               settings[i].oInstance.fnFilter(sInput, iColumn, bRegex, bSmart);
           }
        };

        var suppply_table =$('#supplier').DataTable({
            "iDisplayLength": 5,
            "bLengthChange": false,
            "bInfo": false,
            columnDefs: [ { orderable: false, targets: [-1] } ]
        });

        var Table0 = $("#supplier").dataTable();
        $("#search_all").keyup(function () {
            Table0.fnFilterAll(this.value);
        });

        var prod_table=$('#product').DataTable({
            "iDisplayLength": 5,
            "bLengthChange": false,
            "bInfo": false,
            columnDefs: [ { orderable: false, targets: [-1] } ]
        });
        var Table1 = $("#listing_service").dataTable();
        $("#search_all").keyup(function () {
            Table1.fnFilterAll(this.value);
        });

        $('#supplier_filter').hide();
        $('#product_filter').hide();

        suppply_table.columns().iterator( 'column', function (ctx, idx) {
            $( suppply_table.column(idx).header()).append('<span class="sort-icon"/>');
        });
        prod_table.columns().iterator( 'column', function (ctx, idx) {
            $( prod_table.column(idx).header()).append('<span class="sort-icon"/>');
        });
        $('.dataTables_filter').addClass('pull-right');
    });
    function myFunction(id) {
        document.getElementById("myDropdown"+id).classList.toggle("show");
    }
    window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }

    function deletesupplier(id,count)
    {
        swal({
                title: "Are you sure?",
                text: "Are you sure you want to delete supplier!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No!",
                closeOnConfirm: false,
                closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    headers: {
                        'X-CSRF-Token': "{{ csrf_token() }}"
                    },
                    url: '/supplychain/' + id,
                    type: 'DELETE',  // user.destroy
                    success: function (result) {
                        swal(result.success);
                        $("#"+count).remove();

                        // Do something with the result
                    }
                });
            }
        })
    }
    function gethideproduct(prod)
    {
        var count= $('#supplier').dataTable().fnGetData().length;
        var new_count = count+1;
        $('.preloader').css("display", "block");
        $.ajax({
            headers: {
                'X-CSRF-Token': "{{ csrf_token() }}"
            },
            data: {
                  prod:prod,
                },
            method: 'POST', // Type of response and matches what we said in the route
            url: '/supplychain/getproduct', // This is the url we gave in the route
            success: function (response) { // What to do if we succeed
                $('.preloader').css("display", "none");
                if(prod==1) {
                    $("#hideprod").show();
                    $("#viewprod").hide();
                }
                else
                {
                    $("#viewprod").show();
                    $("#hideprod").hide();
                }
                response = $.parseJSON(response);
                $("#product tbody").empty();

                $('#product').DataTable().clear();
                $.each( response.product, function( key, value ){
                    var html='';
                    var drop='';
                    var url="{{ url('') }}";
                    var link = url+'/inventory/'+value.id+'/edit';
                    drop+='<div class="dropdown">';
                    drop+='<i onclick="myFunction('+new_count+')" class="dropbtn fa fa-cog"></i>';
                    drop+='<div id="myDropdown'+new_count+'" class="dropdown-content">';
                    drop+='<a href="'+link+'">Edit</a>';
                    if(value.hide_show=='0')
                    drop+='<a href="javascript:void(0)" onclick="hideproduct('+value.id+','+new_count+',1)">Hide</a>';
                    else
                    drop+='<a href="javascript:void(0)" onclick="showproduct('+value.id+','+new_count+',0)">Show</a>';
                    drop+='</div></div>';
                    new_count++;
                    $.each( response.product_supplier, function( key1, value1 ) {
                        if (key1 == value.id) {
                            $.each(value1, function (key2, new_value) {
                                html+= new_value+"<br>";
                            });
                        }
                    })
                    $('#product').DataTable().row.add([
                        value.product_name,value.product_nick_name,html,drop
                    ]).draw();
                });

                //$("#product tbody").append(html);

            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                $('.preloader').css("display", "none");
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }
    function hideproduct(id,count,hide)
    {
        $.ajax({
            headers: {
                'X-CSRF-Token': "{{ csrf_token() }}"
            },
            url: '/inventory/' + id,
            data: {
                  cnt:hide,
                },
            type: 'DELETE',  // user.destroy
            success: function (result) {
                swal(result.success);
                $("#"+count).remove();
                // Do something with the result
            }
        });
    }
    function showproduct(id,count,show)
    {
        $.ajax({
            headers: {
                'X-CSRF-Token': "{{ csrf_token() }}"
            },
            url: '/inventory/' + id,
            data: {
                cnt:show,
            },
            type: 'DELETE',  // user.destroy
            success: function (result) {
                swal(result.success);
                // Do something with the result
            }
        });
    }
    </script>
@endsection