@extends('layouts.frontend.app')
@section('title', 'Login')
@section('css')
    {!! Html::style('assets/dist/css/registration.css') !!}
    {!! Html::style('assets/plugins/validationengine/validationEngine.jquery.css') !!}
@endsection
@section('content')
        <div class="row">
            <div class="col-md-10 margin-left no-padding" style="background:#fff;border-radius:7px;box-shadow: 0px 2px 10px 2px rgba(0,0,0,0.5);">
                <div class="col-md-5" style=" padding-left: 0px; padding-right: 0px;">
                    <div class="tab-content responsive">
                        <div class="col-md-12 padding-top-7em padding10 padding-bottom-0">
                            <div class="col-md-11">
                                <img class=" login-logo-img" src="{{ url('uploads/images/FBAFORWARD_LOGO.png') }}">
                            </div>
                            <div class="col-md-12">
                            </div>
                        </div>
                        <form class="form-horizontal" id="validate" role="form" method="POST" action="{{ url('/login') }}">
                            {!! csrf_field() !!}
                            <div class="col-md-12 padding-10-50 padding-top-0">
                                <div class="form-group">
                                    <div class="col-md-12 no-padding">
                                        <div class="col-md-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required, custom[email]]" name="email" id="email"  placeholder="Email">
                                            </div>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 no-padding">
                                        <div class="col-md-12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                            <div class="input-group col-md-12">
                                                <input type="password" class="form-control validate[required]" name="password" id="password" placeholder="Password">
                                            </div>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 margin-top-20">
                                        <div class="col-md-6 margin-top-10 no-padding">
                                            <a class="btn btn-link no-padding font-size-12 grey-color" href="{{ url('/password/reset') }}" style="color: #7d7f80;text-decoration:none;">Forgot password ?</a>
                                        </div>

                                        <div class="col-md-6 text-right no-padding">
                                            <button type="submit" id="login" class="button col-md-12">Log In</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="">
                                        <div class="col-md-12 margin-top-30 no-padding">
                                            <label class="control-label col-md-12 font-size-12">Don't have an account?</label>
                                            <a href="{{ url('register') }}" class="create-account-link  font-size-12">Create my FBAforward Account!</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-7 img-div" style="padding-left: 0px; padding-right: 0px;">
                    <img class="img-login" src="{{ url('/').'/uploads/images/Login_page_image.jpg'  }}"/>
                </div>
            </div>
        </div>
@endsection
@section('js')
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $('#login').click(function(){
                var prefix = 's2id_';
                $("form[id^='validate']").validationEngine('attach',
                {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
            });
            $('.internal-page').css('padding','50px');
            $('body').css('background','#006da3');
            $('#nav_home').hide();
            $('#nav_contact_us').hide();
        });
    </script>
@endsection