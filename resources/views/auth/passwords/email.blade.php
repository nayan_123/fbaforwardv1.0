@extends('layouts.frontend.app')
@section('title', 'Reset')
@section('css')
    {!! Html::style('assets/dist/css/registration.css') !!}
@endsection
@section('content')
        <div class="row">
            <div class="col-md-10 margin-left no-padding" style="background:#fff;border-radius:7px;box-shadow: 0px 2px 10px 2px rgba(0,0,0,0.5);">
                <div class="col-md-5" style=" padding-left: 0px; padding-right: 0px;">
                    <div class="tab-content responsive">
                        <div class="col-md-12 padding-top-10em padding10 padding-bottom-0">
                            <div class="col-md-11">
                                <img class=" login-logo-img" src="{{ url('uploads/images/FBAFORWARD_LOGO.png') }}">
                            </div>
                            <div class="col-md-12">
                            </div>
                        </div>
                        @if(Session::has('success_message'))
                            <div class="col-md-12 instruction">
                                <p class="text-center">
                                    Instructions for resetting your password have been sent to:<br><b> {{ Session::get('success_message') }}</b>
                                </p>
                                <p class="text-center">Please check your email.</p>
                                <a href="{{ url('/') }}" class="text-center no-padding create-account-link font-size-12 col-md-12">Go back to FBAForward.com</a>
                            </div>
                        @else
                        <form class="form-horizontal" id="validate" role="form" method="POST" action="{{ url('/password/email') }}">
                            {!! csrf_field() !!}
                            <div class="col-md-12 padding-10-50 padding-top-0">
                                <div class="form-group">
                                    <div class="col-md-12 no-padding">
                                        <div class="col-md-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="form-control validate[required,custom[email]]" name="email" value="{{ old('email') }}" placeholder="Email">
                                            </div>
                                             @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="col-md-7 no-padding margin-top-10">
                                            <label class="control-label col-md-12 font-size-12 no-padding">Already have an account?</label>
                                            <a href="{{ url('') }}" class=" no-padding create-account-link font-size-12">Sign in to your account!</a>
                                        </div>
                                        <div class="col-md-5 no-padding">
                                            <button type="submit" id="register" class="button col-md-12 margin-top-10">Reset</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                        @endif
                    </div>
                </div>

                <div class="col-md-7 img-div" style="padding-left: 0px; padding-right: 0px;">
                    <img class="img-login" src="{{ url('/').'/uploads/images/Login_page_image.jpg'  }}"/>
                </div>
            </div>
        </div>
@endsection
@section('js')
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $('.internal-page').css('padding','50px');
            $('body').css('background','#006da3');
            $('#nav_home').hide();
            $('#nav_contact_us').hide();
                var prefix = 's2id_';
                $("form[id^='validate']").validationEngine('attach', {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
        });
    </script>
@endsection