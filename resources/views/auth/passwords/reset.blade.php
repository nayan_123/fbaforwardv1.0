@extends('layouts.frontend.app')
@section('title', 'Reset Password')
@section('css')
    {!! Html::style('assets/dist/css/registration.css') !!}
@endsection
@section('content')
<div class="row">
    <div class="col-md-10 margin-left no-padding" style="background:#fff;border-radius:7px;box-shadow: 0px 2px 10px 2px rgba(0,0,0,0.5);">
        <div class="col-md-5" style=" padding-left: 0px; padding-right: 0px;">
            <div class="tab-content responsive">
                <div class="col-md-12 padding-top-10em padding10 padding-bottom-0">
                    <div class="col-md-11">
                        <img class=" login-logo-img" src="{{ url('uploads/images/FBAFORWARD_LOGO.png') }}">
                    </div>
                </div>
                <form id="validate" class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="col-md-12 padding-10-50 padding-top-0">
                        <div class="form-group">
                            <div class="col-md-12 no-padding">
                                <div class="col-md-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control validate[required, custom[email]]" name="email" value="{{ $email or old('email') }}" placeholder="Email">
                                    </div>
                                     @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 no-padding">
                                <div class="col-md-12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="input-group col-md-12">
                                        <input type="password" class="form-control validate[required]" name="password" value="{{ old('password') }}" placeholder="Password">
                                    </div>
                                     @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 no-padding">
                                <div class="col-md-12 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <div class="input-group col-md-12">
                                        <input type="password" class="form-control validate[required]" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Re-enter Password">
                                    </div>
                                     @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group padding-10-50">
                        <div class="col-md-5"></div>
                        <div class="col-md-7">
                            <button id="reset" type="submit" class="button col-md-12">
                                Reset Password
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-7 img-div" style="padding-left: 0px; padding-right: 0px;">
            <img class="img-login" src="{{ url('/').'/uploads/images/Login_page_image.jpg'  }}"/>
        </div>
    </div>
</div>
@endsection
@section('js')
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $('.internal-page').css('padding','50px');
            $('body').css('background','#006da3');
            $('#nav_home').hide();
            $('#nav_contact_us').hide();
        });
        $(document).ready(function () {
            $('#reset').click(function(){
                var prefix = 's2id_';
                $("form[id^='validate']").validationEngine('attach',
                {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
            });
        });
    </script>
@endsection