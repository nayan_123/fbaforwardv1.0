@extends('layouts.frontend.app')
@section('title', 'Register')
@section('css')
    {!! Html::style('assets/dist/css/registration.css') !!}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-10 margin-left no-padding" style="background: #fff;border-radius:7px;box-shadow: 0px 2px 10px 2px rgba(0,0,0,0.5);">
            <div class="col-md-5" style="padding-left: 0px; padding-right: 0px;background: #fff;border-radius:7px">
                <div class="tab-content responsive">
                    <div class="col-md-12 padding-top-50 padding10 padding-bottom-0">
                        <div class="col-md-10 col-md-offset-1">
                            <img class=" login-logo-img margin-bottom-0" src="{{ url('uploads/images/FBAFORWARD_LOGO.png') }}">
                        </div>
                        <div class="col-md-12">
                            <h4 class="login-logo-font font-size-12">Create your free account to access the client portal</h4>
                        </div>
                    </div>
                    <form class="form-horizontal" id="validate" role="form" method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}
                        <div class="col-md-12 padding-10-50">
                            <div class="form-group padding-top-10">
                                <div class="col-md-12">
                                    <div class="col-md-12 {{ $errors->has('fname') ? ' has-error' : '' }}">
                                        <div class="input-group col-md-12">
                                            <input type="text" class="form-control input validate[required]" name="fname" id="fname" placeholder="First Name" value="{{ old('fname') }}" onchange="getname()">
                                        </div>
                                        @if ($errors->has('fname'))
                                            <span class="help-block">
                                             <strong>{{ $errors->first('fname') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-12 {{ $errors->has('lname') ? ' has-error' : '' }}">
                                        <div class="input-group col-md-12">
                                            <input type="text" class="form-control input validate[required]" name="lname" id="lname" placeholder="Last Name" value="{{ old('lname') }}" onchange="getname()">
                                            <input type="hidden" class="form-control input" name="name" id="name">
                                        </div>
                                        @if ($errors->has('lname'))
                                            <span class="help-block">
                                             <strong>{{ $errors->first('lname') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-12 {{ $errors->has('cname') ? ' has-error' : '' }}">
                                        <div class="input-group col-md-12">
                                            <input type="text" placeholder="Company Name" class="form-control validate[required]" name="cname" value="{{ old('cname') }}">
                                        </div>
                                        @if ($errors->has('cname'))
                                            <span class="help-block">
                                             <strong>{{ $errors->first('cname') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <div class="input-group col-md-12">
                                            <input type="text" class="form-control validate[required, custom[email]]" name="email"  placeholder="Email" value="{{ old('email') }}">
                                        </div>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                             <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <div class="input-group col-md-12">
                                            <input type="password" class="form-control validate[required]" name="password"  placeholder="Create Password" id="password" onchange="getpass()">
                                            <input type="password" name="password_confirmation" id="password_confirmation"  placeholder="Create Password" hidden>
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                             <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-7 no-padding">
                                        <label class="control-label col-md-12 font-size-12">Already have an account?</label>
                                        <a href="{{ url('') }}" class="create-account-link font-size-12">Sign into Your Account!</a>
                                    </div>
                                    <div class="col-md-5">
                                        <button type="submit" id="register" class="button col-md-12 margin-top-10">Sign Up</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-7 img-div no-padding" style="padding-left: 0px;">
                <img class="img-register" src="{{ url('/').'/uploads/images/Login_page_image.jpg'  }}"/>
            </div>
        </div>
    </div>
@endsection
@section('js')
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $('#register').click(function(){
                // Validation Engine init
                var prefix = 's2id_';
                $("form[id^='validate']").validationEngine('attach', {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
            });
        });
        function  getname() {
            name = $("#fname").val()+" "+$("#lname").val();
            $("#name").val(name);
        }
        $('.internal-page').css('padding','50px 0px');
        $('body').css('background','#006da3');
        $('#nav_home').hide();
        $('#nav_contact_us').hide();
    </script>
@endsection