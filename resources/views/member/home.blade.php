@extends('layouts.member.app')
@section('title', 'Dashboard')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/home.css') !!}
@endsection
@section('content')
    @if($user->role_id=='3')
        <section class="content padding-bottom-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">
                            <span style="font-size: 25px;">@if(isset($user_details)){{ $user_details->company_name }}'s @endif Dashboard</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                </div>
                <div class="col-md-6 padding-right-0">
                    <div class="box">
                        <div class="box-body" style="min-height:383px;">
                                <div class="col-md-12" style="background-color: #00466A; color:#FFF; line-height: 245%">
                                    <span style="float: left">YOUR RECENT QUOTE</span><span style="float:right"><a href="{{ url('quote') }}" class="view-all-link">View All</a></span>
                                </div>
                            @if(empty($quote))
                                <div class="col-md-12">
                                    <div class="content" style="text-align: center;padding-top: 10%;">
                                    <img src="{{ asset('uploads/images/quote.png') }}" style="width: 50px"><br>
                                    You don't have any quotes!<br>
                                    <a class="button1" href="{{ url('quote/create') }}">Request New Quote</a>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-12">
                                    <div class="content">
                                    <table class="table table-responsive">
                                        @foreach($quote as $quotes)
                                            <tr>
                                                <td style="text-align: left;color: #b5b5b5"><span style="color: #0088cc">@if($quotes['product_nick_name']!='')<a style="color: #0088cc" href="{{ url('quote') }}"><b>{{ $quotes['product_nick_name'] }}</b></a> @else <a href="{{ url('quote') }}"><b>{{ substr($quotes['product_name'], 0, 90) }}</b></a> @endif</span>
                                                    <br>Requested on {{ date('m/d/Y',strtotime($quotes['created_at'])) }}</td>
                                                <td style="text-align: right;color: #b5b5b5">
                                                    @foreach($quote_freight as $quote_freights)
                                                        @if($quote_freights->customer_requirement_id==$quotes['id'])
                                                            {{ $quote_freights->shipping_name." Total: $".$quote_freights->total_freight_price }}<br>
                                                        @endif
                                                    @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-body" style="min-height:383px;">
                                <div class="col-md-12" style="background-color: #00466A; color:#FFF; line-height: 245%">
                                    <span style="float: left">YOUR RECENT ORDERS</span><span style="float:right"><a href="{{ url('order') }}" class="view-all-link">View All</a></span>
                                </div>
                            @if(empty($order))
                                <div class="col-md-12">
                                    <div class="content" style="text-align: center;padding-top: 10%;">
                                    <img src="{{ asset('uploads/images/orders.png') }}" style="width: 50px"><br>
                                    You don't have any orders!<br>
                                    <a class="button1" href="{{url('quote')}}">Approve Quote</a>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-12">
                                    <div class="content">
                                    <table class="table table-responsive">
                                        @foreach($order as $orders)
                                            <tr>
                                                <td style="text-align: left;color: #b5b5b5"><span style="color: #0088cc">@if($orders['product_nick_name']!='')<a style="color: #0088cc" href="{{ url('order') }}"><b>{{ $orders['product_nick_name'] }}</b></a> @else <a href="{{ url('order') }}"><b>{{ substr($orders['product_name'], 0, 55) }}</b> </a> @endif</span>
                                                    <br>Submitted on {{ date('m/d/Y',strtotime($orders['created_at'])) }}</td>
                                                <td style="color: #b5b5b5;">Order #{{ $orders['order_no']  }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">
                                <div class="col-md-12" style="background-color: #00466A; color:#FFF; line-height: 245%">
                                    <span style="float: left">YOUR INVENTORY</span><span style="float:right"><a href="{{ url('inventory') }}" class="view-all-link">View All</a></span>
                                </div>
                            @if(empty($inventory))
                                <div class="col-md-12">
                                    <div style="text-align: center;padding-top: 5%;">
                                    <img src="{{ asset('uploads/images/inventory.png') }}" style="width: 50px"><br>
                                    You don't have any inventory!<br>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-12">
                                    <div class="col-md-12 content" style="min-height:250px;">
                                    <table class="table table-responsive grey-font">
                                        @foreach($inventory as $inventories)
                                            <tr>
                                                <td class="col-md-3" style="text-align: left;color: #b5b5b5"><span style="color: #0088cc">@if($inventories['product_nick_name']!='')<a style="color: #0088cc" class="text-overflow" href="{{ url('member/amazoninventorylist') }}"><b>{{ substr($inventories['product_nick_name'], 0, 45) }}</b> </a>@else <a href="{{ url('member/amazoninventorylist') }}" class="text-overflow"><b>{{ substr($inventories['product_name'], 0, 45)  }}</b></a> @endif</span>
                                                    <br>Last Updated on {{ date('m/d/Y',strtotime($inventories['updated_at'])) }}</td>
                                                <td class="col-md-2">Inbound to FBAforward<br>{{ $inventories['infbaforward'] }}</td>
                                                <td class="col-md-2">Processing at FBAforward<br>{{ $inventories['processfbaforward'] }}</td>
                                                <td class="col-md-2">Stored at FBAforward<br>{{ $inventories['atfbaforward'] }}</td>
                                                <td class="col-md-2">InTransit to Amazon<br>{{ $inventories['inamazon'] }}</td>
                                                <td class="col-md-2">At Amazon<br>{{ $inventories['atamazon'] }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                <div class="col-md-3 padding-right-0 bottom-small-box">
                    <div class="box bottom-box">
                        <div class="box-body bottom-box-body">
                            <div class="col-md-12 text-center">
                                <span class="customer-dashboard-hading col-md-12 text-center">GETTING STARTED</span>
                                <div class="col-md-12 margin-top-20">
                                    <iframe class="col-md-12 no-padding" src="https://www.youtube.com/embed/-cKntB6WHTs" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-md-12 no-padding">
                                    <span style="color: #0088cc">{{--Link to Getting Started Article Another Link About What To Do --}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 padding-right-0 bottom-small-box">
                    <div class="box bottom-box">
                        <div class="box-body bottom-box-body">
                            <div class="col-md-12 div7">
                                 <span class="customer-dashboard-hading col-md-12 text-left" style="color:#00466A">
                                    KNOWLEDGE BASE</span>
                                <ul>
                                    <li>What are Shipping Incoterms?</li>
                                    <li>Amazon Account: Setting up User Permission</li>
                                    <li>Labeling Your Goods For FBA: FNSKU or UPC</li>
                                    <li>Freight Forwarding</li>
                                    <li>How to Get an EIN</li>
                                    {{--<li style="color:#00466A">POPULAR ARTICLES</li>
                                    <li>Link to Article in KNowledge Base </li>
                                    <li>Another link</li>
                                    <li>Shipping article to explain</li>
                                    <li>Other popular articles</li>
                                    <li>Link to Article in KNowledge Base</li>
                                    <li>Another link</li>
                                    <li>Shipping article to explain</li> --}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 bottom-big-box">
                    <div class="box bottom-box">
                        <div class="box-body bottom-box-body">
                            <div class="col-md-5 logistics-box" style="margin: 10px 0%;">
                                <span class="space logi responsive-margin-bottom col-md-12 no-padding" style="padding-bottom: 5% !important;">Logistics is complicated!</span>
                                <span class="col-md-12 no-padding" style="color: #b5b5b5;">That's why your dedicated account manager wil be with you throughout the entire process, from pre-quote questions to the moment your goods are at Amazon... And all over again for your next order. </span>
                            </div>
                            <div class="col-md-7" id="wrapper_1" style="margin: 10px 0%;">
                                <div class="col-md-12 text-left padding-bottom-10">
                                <span class="space">YOUR ACCOUNT MANAGER</span>
                                </div>
                                <div class="col-md-12 no-padding" style="padding-bottom: 5% !important;">
                                   @if(isset($user_details))
                                       @if($user_details->zoho_account_manager_id==0)
                                            <div class="col-md-1"></div>
                                            <img src="{{ asset('uploads/images/no_account_manager.png') }}" class="col-md-4 img-account no-padding">
                                       @else
                                            <img src="{{ asset('uploads/zoho_managers/'.$user_details->image) }}" class="col-md-5 img-account no-padding">
                                        @endif
                                   @else
                                        <div class="col-md-1"></div>
                                        <img src="{{ asset('uploads/images/no_account_manager.png') }}" class="col-md-4 img-account no-padding">
                                   @endif
                                <span class="account_detail col-md-6 account-manager-box no-padding">
                                    @if(isset($user_details))
                                        @if($user_details->zoho_account_manager_id==0)
                                            <b style="padding-bottom: 10px;">FBAforward Support</b><br><br>
                                            <p style="margin-bottom: 10px;">CONTACT<br>
                                                +1-866-526-3951 x 705<br>
                                                support@Fbaforward.com<br>
                                                <div id="email_me" class="hide">
                                                    <span style="color: #00466A;">Need Help?</span> <a href="mailto:support@Fbaforward.com" class="darkbluebutton" style="margin-right: -8px;">Email me</a>
                                                </div>
                                                <div id="chat_now" class="hide">
                                                    <span style="color: #00A94E;margin-top:5px;">I'm Online!</span> <button class="darkbluebutton" style="background-color: #00A94E;margin-right: -8px;margin-top:5px;" onclick='$("#zsiq_agtpic").click()'>Chat Now</button>
                                                </div>
                                        </p>
                                        @else
                                            <b style="padding-bottom: 10px;">ROMAN FISHER</b><br><br>
                                            <p style="margin-bottom: 10px;">CONTACT<br>
                                            @if($user_details->phone)
                                               {{$user_details->phone}}<br>
                                            @elseif($user_details->mobile)
                                                {{ $user_details->mobile }}<br>
                                            @endif
                                            {{ $user_details->email }}<br>
                                                <div id="email_me" class="hide">
                                                    <span style="color: #00466A;">Need Help?</span> <a href="mailto:{{ $user_details->email }}" class="darkbluebutton" style="margin-right:-8px;" >Email me</a>
                                                </div>
                                                <div id="chat_now" class="hide">
                                                    <span style="color: #00A94E;margin-top:5px;">I'm Online!</span> <button class="darkbluebutton" style="background-color: #00A94E;margin-right: -8px;margin-top:5px;" onclick='chatNow()'>Chat Now</button>
                                                </div>
                                            </p>
                                        @endif
                                    @else
                                        <b style="padding-bottom: 10px;">FBAforward Support</b><br><br>
                                        <p style="margin-bottom: 10px;">CONTACT<br>
                                                +1-866-526-3951 x 705<br>
                                                support@Fbaforward.com<br>
                                        <div id="email_me" class="hide">
                                                    <span style="color: #00466A;">Need Help?</span> <a href="mailto:support@Fbaforward.com" class="darkbluebutton" style="margin-right: -8px;">Email me</a>
                                                </div>
                                        <div id="chat_now" class="hide">
                                                    <span style="color: #00A94E;margin-top:5px;">I'm Online!</span> <button class="darkbluebutton" style="background-color: #00A94E;margin-right: -8px;margin-top:5px;" onclick='$("#zsiq_agtpic").click()'>Chat Now</button>
                                                </div>
                                        </p>
                                    @endif
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
            </div>
        </section>
    @endif
@endsection
@section('js')
    <script type="text/javascript">
        var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq ||
            {widgetcode:"1aefbc2b2bdc39337f11143ded60b1cccb63e4cf019c6994f3db04470c50e8159387f867729081d6cb4f5bfe0643c922", values:{},ready:function(){}};
        var d=document;s=d.createElement("script");s.type="text/javascript";s.id="zsiqscript";s.defer=true;
        s.src="https://salesiq.zoho.com/widget";t=d.getElementsByTagName("script")[0];t.parentNode.insertBefore(s,t);d.write("<div id='zsiqwidget'></div>");
        $zoho.salesiq.ready=function(embedinfo) {
            $zoho.salesiq.chat.offline(function() {
                $('#email_me').removeClass('hide');
            });
            $zoho.salesiq.chat.online(function() {
                $('#chat_now').removeClass('hide');
            });
        }
    </script>
@endsection