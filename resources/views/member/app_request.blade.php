@extends('layouts.member.app')
@section('title', 'App Request')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
@endsection
@section('content')
<section class="content">
    <div class="box">
        <div class="box-body" style="min-height: 700px;">
            <div class="row">
                <div class="col-md-12">
                    <p class="col-md-12">Request app features to be implemented into future update. Do you want to request for the app ?</p>
                    <div class="col-md-12">
                        <div class="col-md-5 col-md-offset-2">
                            <a href="{{ url('member/app-request') }}"><button type="submit" class="button">Request</button></a>
                            <a href="{{ url('member/home') }}"><button type="submit" class="button">Skip</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
{!! Html::script('assets/plugins/jQuery/jQuery-2.1.4.min.js') !!}
<script>
</script>