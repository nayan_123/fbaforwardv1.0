@extends('layouts.member.app')
@section('title', 'Edit Profile')
@section('css')
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-star"></i> Edit Profile</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/profile') }}"><i class="fa fa-star"></i> Profile</a></li>
            <li class="active"><i class="fa fa-pencil"></i> Edit Profile
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Profile</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['url' =>  'member/profile/edit', 'method' => 'put', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
            <fieldset>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! htmlspecialchars_decode(Form::label('name', 'Name <span class="required">*</span>', ['class' => 'control-label col-md-3'])) !!}
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                {!! Form::text('name', old('name', isset($user) ? $user->name: null), ['class' => 'form-control validate[required]', 'placeholder'=>'Name']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! htmlspecialchars_decode(Form::label('email', 'Email', ['class' => 'control-label col-md-3'])) !!}
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                {!! Form::email('', $user->email, ['class' => 'form-control', 'disabled']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', 'Password', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                {!! Form::password('password', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('password_confirmation', 'Confirmation Password', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                {!! Form::password('password_confirmation', ['class' => isset($user) ? 'form-control validate[equals[password]]': 'form-control validate[required,equals[password]]' ]) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('avatar', 'Avatar', ['class' => 'control-label col-md-3']) !!}
                        @if(isset($user) && $user->avatar !="")
                            <div class="col-md-9">
                                <img src="{{ asset($user->avatar) }}" width="30%" class="img-circle"
                                     alt="User Avatar"/>
                            </div>
                        @else
                            <div class="col-md-9">
                                <img src="{{ asset('uploads/avatars/avatar.png') }}" width="30%"
                                     class="img-circle" alt="User Avatar"/>
                            </div>
                        @endif
                        <div class="col-md-9 col-md-offset-3" style="margin-top: 10px;">
							<span class="btn  btn-file  btn-primary">Upload Avatar
                                {!! Form::file('avatar') !!}
							</span>
                        </div>
                    </div>
                </div>
            </div>
            </fieldset>
            <fieldset>
            <div class="row">
                <div class="col-md-6">
                    @if($user->role_id==3)
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('company_name', 'Company Name <span class="required">*</span>', ['class' => 'control-label col-md-3'])) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    {!! Form::text('company_name', old('company_name', !empty($user_info) ? $user_info[0]->company_name : null), ['class' => 'form-control validate[required]', 'placeholder'=>'Company Name']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('contact_fname', 'Contact First Name <span class="required">*</span>', ['class' => 'control-label col-md-3'])) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    {!! Form::text('contact_fname', old('contact_fname', !empty($user_info) ? $user_info[0]->contact_fname: null), ['class' => 'form-control validate[required]', 'placeholder'=>'Contact First Name']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('contact_lname', 'Contact Last Name <span class="required">*</span>', ['class' => 'control-label col-md-3'])) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    {!! Form::text('contact_lname', old('contact_lname',!empty($user_info) ? $user_info[0]->contact_lname: null), ['class' => 'form-control validate[required]', 'placeholder'=>'Contact Last Name']) !!}
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-md-6">
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('company_address', 'Street Address <span class="required">*</span>', ['class' => 'control-label col-md-3'])) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    {!! Form::text('company_address', old('company_address',!empty($user_info) ? $user_info[0]->company_address: null), ['class' => 'form-control validate[required]', 'placeholder'=>'Street Address']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('company_address2', 'Address Line 2', ['class' => 'control-label col-md-3']) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    {!! Form::text('company_address2', old('company_address2', !empty($user_info) ? $user_info[0]->company_address2: null), ['class' => 'form-control', 'placeholder'=>'Address Line 2']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('company_phone', 'Phone <span class="required">*</span>', ['class' => 'control-label col-md-3'])) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    {!! Form::text('company_phone', old('company_phone', !empty($user_info) ? $user_info[0]->company_phone: null), ['class' => 'form-control validate[required, custom[integer, maxSize[10]]]', 'placeholder'=>'Phone']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('business_type', 'Primary Business Type <span class="required">*</span>', ['class' => 'control-label col-md-3'])) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    {!! Form::text('business_type', old('business_type', !empty($user_info) ? $user_info[0]->primary_bussiness_type: null), ['class' => 'form-control validate[required]', 'placeholder'=>'Primary Business Type']) !!}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            {!! Form::submit('Update Profile', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                </div>
            </div>
            </fieldset>
            {!! Form::close() !!}
        </div>
    </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach', {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
        });
    </script>
@endsection