@extends('layouts.member.app')
@section('title', 'Profile')
@section('css')
@endsection
@section('content')
    <section class="content-header">
        <h1>Profile</h1>
        <ol class="breadcrumb"><li class="active"><i class="fa fa-user"></i> Profile</li></ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $user->name. ' Profile' }}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <div class="margin-bottom"><a class="btn btn-primary" href="{{ url('member/profile/edit') }}" title="Edit" style="width: 100%;"><i class="fa fa-2 fa-pencil"></i> Edit Your Profile</a></div>
                <img src="{{ asset($user->avatar) }}" class="img-responsive img-circle margin-bottom"
                     alt="{{ $user->name }}">
            </div>
            <div class="col-md-9">
                <div class="table-responsive no-padding">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td style="width: 30%;"><span>Name:</span></td>
                            <td><b class="text-info">{{ $user->name }}</b></td>
                        </tr>
                        <tr>
                            <td><span>Email:</span></td>
                            <td><b class="text-info">{{ $user->email }}</b></td>
                        </tr>
                        @if($user->role_id==3)
                        <tr>
                            <td><span>Company Name:</span></td>
                            <td><b class="text-info"> @if(!empty($user_info)) {{$user_info[0]->company_name }} @endif</b></td>
                        </tr>
                        <tr>
                            <td><span>Contact First Name:</span></td>
                            <td><b class="text-info">@if(!empty($user_info)) {{ $user_info[0]->contact_fname }} @endif</b></td>
                        </tr>
                        <tr>
                            <td><span>Conatct Last Name:</span></td>
                            <td><b class="text-info">@if(!empty($user_info)) {{ $user_info[0]->contact_lname }} @endif</b></td>
                        </tr>
                        <tr>
                            <td><span>Address:</span></td>
                            <td><b class="text-info">@if(!empty($user_info)) {{ $user_info[0]->company_address }}<br>{{ $user_info[0]->company_address2 }} @endif</b></td>
                        </tr>
                        <tr>
                            <td><span>Phone:</span></td>
                            <td><b class="text-info">@if(!empty($user_info)) {{ $user_info[0]->company_phone }} @endif</b></td>
                        </tr>
                        <tr>
                            <td><span>Primary Bussiness Type:</span></td>
                            <td><b class="text-info">@if(!empty($user_info)) {{ $user_info[0]->primary_bussiness_type }} @endif</b></td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
    </script>
@endsection