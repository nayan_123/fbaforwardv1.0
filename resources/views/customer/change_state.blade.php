<div class="form-group">
    <div class="col-md-12">
        <label for="" class="control-label col-md-12" style="padding-left: 15px !important;"> STATE OF BUSSINESS REGISTRATION </label>
        <div class="col-md-12">
            <div class="input-group col-md-12">
                {!! Form::select('business_state', array_add($states, '','Please Select'), old('business_state',isset($user_details->business_state)?$user_details->business_state:null), ['class' => 'form-control select2 validate[required]']) !!}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".select2").select2({
            placeholder: "Please Select",
            allowClear: true
        });
    });
</script>