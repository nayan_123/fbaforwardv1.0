@extends('layouts.member.app')
@section('title', $title)
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
<style>
.col-md-12 {  padding-left: 35px;  }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-anchor"></i> {{$title}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-anchor"></i>{{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div style="background-color: #003c6b; color:#ffffff; padding-left:3%; width: 99%">
                           <span class="heading">COMPANY</span>
                        </div>
                        <table>
                            <tr style="border-bottom: 0px;">
                                <td class="col-md-3"><b>Legal Name</b></td>
                                <td class="col-md-3">{{ $customer->company_name }}</td>
                                <td class="col-md-3"><b>Doing Business As</b></td>
                                <td class="col-md-3">{{ $customer->business_as  }}</td>
                            </tr>
                            <tr style="border-bottom: 0px;">
                                <td class="col-md-3"><b>Type of Business</b></td>
                                <td class="col-md-3">{{ $customer->primary_bussiness_type }}</td>
                                <td class="col-md-3"><b>Country of Business Registration</b></td>
                                <td class="col-md-3">{{ $customer->business_country  }}</td>
                            </tr>
                            <tr style="border-bottom: 0px;">
                                <td class="col-md-3"><b>Does Company Have Tax ID?</b></td>
                                <td class="col-md-3">{{ $customer->tax_id }}</td>
                            </tr>
                        </table>
                        <div style="background-color: #003c6b; color:#ffffff; padding-left:3%; width: 99%">
                            <span class="heading">CONTACT (S)</span>
                        </div>
                        <table>
                            <tr style="border-bottom: 0px;">
                                <td class="col-md-3"><b>Name</b></td>
                                <td class="col-md-3">{{ $customer->contact_fname." ".$customer->lname }}</td>
                                <td class="col-md-3"><b>Email</b></td>
                                <td class="col-md-3">{{ $customer->contact_email  }}</td>
                            </tr>
                            <tr style="border-bottom: 0px;">
                                <td class="col-md-3"><b>Phone</b></td>
                                <td class="col-md-3">{{ $customer->conatct_phone }}</td>
                            </tr>
                        </table>
                        <div style="background-color: #003c6b; color:#ffffff; padding-left:3%; width: 99%">
                            <span class="heading">OTHER</span>
                        </div>
                        <table>
                            <tr style="border-bottom: 0px;">
                                <td class="col-md-3"><b>Amazon Integration? </b></td>
                                <td class="col-md-3"></td>
                                <td class="col-md-3"><b>Power of Attorney</b></td>
                                <td class="col-md-3"></td>
                            </tr>
                        </table>
                        <div style="background-color: #003c6b; color:#ffffff; padding-left:3%; width: 99%">
                            <span class="heading">ORDERS</span>
                        </div>
                        <table class="table">
                            @if(!empty($order))
                            <thead>
                            <tr>
                                <th class="col-md-2">Order</th>
                                <th class="col-md-4">Product</th>
                                <th class="col-md-2">Date Ordered</th>
                                <th class="col-md-2">Status</th>
                                <th class="col-md-2">View Details</th>
                            </tr>
                            </thead>
                                @foreach($order as $orders)
                                    <tr>
                                        <td>{{ $orders['order_no'] }}</td>
                                        <td>{{ $orders['product_name'] }}</td>
                                        <td>{{ date('m/d/Y', strtotime($orders['created_at'])) }}</td>
                                        <td>{{ $orderStatus[$orders['is_activated']] }}</td>
                                        <td>
                                           <a href="{{ url('order/detail/'.$orders['shipping_method_id'].'/'.$orders['customer_requirement_id']) }}" class="button">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
@endsection