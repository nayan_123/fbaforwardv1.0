@extends('layouts.member.app')
@section('title', 'Customer Accounts')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-star"></i>Customer Accounts</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-star"></i>Customer Accounts</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="no-padding">
                            <table class="table" id="customerlist">
                                <thead>
                                <tr>
                                    <th class="col-md-4"><span>Company</span></th>
                                    <th class="col-md-1"><span>Email Address</span></th>
                                    <th class="col-md-1"><span>Phone Number</span></th>
                                    <th class="col-md-1"><span>Country</span></th>
                                    <th class="col-md-1">View More</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->company_name }}</td>
                                        <td>{{ $user->contact_email }}</td>
                                        <td>{{ $user->contact_phone }}</td>
                                        <td>{{ $user->company_country }}</td>
                                        <td><a class="button" href="{{ url('customer/'.$user->user_id)}}">View More</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            var activetable =$('#customerlist').DataTable({
                "iDisplayLength": 5,
                "bLengthChange": false,
                "bInfo": false,
                columnDefs: [ { orderable: false, targets: [-1] } ]
            });
            $('.dataTables_filter').addClass('pull-right');
        });
    </script>
@endsection