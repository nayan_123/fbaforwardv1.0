@extends('layouts.member.app')
@section('title', 'Delivery')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
@endsection
@section('content')
{!! Html::script('assets/bootstrap/js/datePicker.js') !!}
{!! Html::script('assets/plugins/jQuery/jQuery-2.1.4.min.js') !!}
<style>
.error{ color:#dd4b39;  }
.margin10 { margin-top:10px; }
.item-title span, input { margin: 10px 0px !important; }
</style>
    <section class="content-header">
        <h1><i class="fa fa-shopping-cart"></i> Delivery</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-shopping-cart"></i> Delivery</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <form method="post" class="form col-md-12" id="validate" action="{{ url('delivery/create') }}" enctype="multipart/form-data">
                	{{ csrf_field() }}
                	<input type="hidden" name="order_id" value="{{ $order_id }}">
                	<div class="col-md-12 margin10">
	                	<div class="form-group">
	                		<div class="col-md-2">
	                			<label>Delivery : </label>
	                		</div>
	                		<div class="col-md-10">
	                			<input type="radio" class="validate[minCheckbox[1]]checkbox" name="delivery_required" value="1" checked> Yes <br>
	                			<input type="radio" class="validate[minCheckbox[1]]checkbox" name="delivery_required" value="0"> No <br><br>
	                		</div>
	                		<div  class="col-md-10 col-md-offset-2">
	                			@if($errors->has('delivery_required'))
								    <div class="error">{{ $errors->first('delivery_required') }}</div>
								@endif
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-12 margin10">
	                	<div class="form-group">
	                		<div class="col-md-12">
	                			<label>Location of Goods (CFS Name): </label>
	                		</div>
	                		<div class="col-md-12">
	                			<select class="form-control validate[required]" name="location_of_goods" >
	                				<option value="ICT"> ICT</option>
	                				<option value="AZ West"> AZ West</option>
	                				<option value="St. George"> St. George</option>
	                				<option value="Imperial"> Imperial</option>
	                				<option value="MAT"> MAT</option>
	                				<option value="Forward Air"> Forward Air</option>
	                				<option value="CAS"> CAS</option>
	                				<option value="Energy Transport"> Energy Transport</option>
	                				<option value="WAO5"> WAO5</option>
	                				<option value="Other"> Other</option>
	                			</select>
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-12 margin10">
	                	<div class="form-group">
	                		<div class="col-md-12">
	                			<label>Delivery Company: </label>
	                		</div>
	                		<div class="col-md-12">
	                			<select class="form-control validate[required]" name="delivery_company">
	                				<option value="FBAforward"> FBAforward</option>
	                				<option value="Daylight Transport"> Daylight Transport</option>
	                				<option value="WorldTrans Services"> WorldTrans Services</option>
	                				<option value="Other"> Other</option>
	                			</select>
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-12 margin10">
	                	<div class="form-group">
	                		<div class="col-md-12">
	                			<label>Bill of Lading Number: </label>
	                		</div>
	                		<div class="col-md-12">
	                			<input type="text" class="form-control validate[required] custom[integer]" name="bill_of_lading" >
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-12">
	                	<div class="form-group">
	                		<div class="col-md-12">
	                			<label>Container Number: </label>
	                		</div>
	                		<div class="col-md-12">
	                			<input type="text" class="form-control validate[required] custom[integer]" name="container">
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-12">
	                	<div class="form-group">
	                		<div class="col-md-12">
	                			<label>Delivery Order: </label>
	                		</div>
	                		<div class="col-md-12">
	                			<input type="file" class="validate[checkFileType[jpg|jpeg|gif|JPG|png|PNG|docx|doc|pdf|pdfx]] validate[required]" name="delivery_order">
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-12">
	                	<div class="form-group">
	                		<div class="col-md-12">
	                			<label>ETA To Warehouse: </label>
	                		</div>
	                		<div class="col-md-12">
	                			<input type="text" class="datepicker form-control validate[required]" placeholder="DD-MM-YYYY" name="ETA_to_warehouse"  >
	                		</div>
	                	</div>
	                </div>
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-md-12">
								<label>Warehouse and Payment Processing Fees: </label>
							</div>
							<div class="col-md-12">
								<input type="text" class="form-control validate[required] custom[integer]"  name="warehouse_process_fee"  >
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-md-12">
								<label>Payment Method</label>
							</div>
							<div class="col-md-12">
								<select name="payment_method" class="form-control validate[required] select2">
									<option value=""></option>
									<option value="1">Online Portal</option>
									<option value="2">Check</option>
									<option value="3">Driver</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-12">
	                	<div class="form-group">
	                		<div class="col-md-12">
	                			<label>Terminal Fees: </label>
	                		</div>
	                		<div class="col-md-12">
	                			<input type="text" class="form-control validate[required] custom[integer]" name="terminal_fees" >
	                		</div>
	                	</div>
	                </div>
	                <div class="col-md-12 ">
	                	<div class="form-group">
	                		<button class="btn btn-primary pull-right" type="submit">Submit</button>
	                	</div>
	                </div>
                </form>
            </div>
        </div>
    </section>
    @endsection
@section('js')
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
{!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
{!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
<script type="text/javascript">
    $(document).ready(function () {
        var prefix = 's2id_';
        $("form[id^='validate']").validationEngine('attach', {
                promptPosition: "bottomRight", scroll: false,
                prettySelect: true,
                usePrefix: prefix
            });
        $('.datepicker').datepicker({
            startDate: new Date(),
        });
        $(".select2").select2({
            placeholder: "Please Select",
            allowClear: true
        });
    });
</script>
@endsection