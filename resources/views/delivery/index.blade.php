@extends('layouts.member.app')
@section('title', 'Delivery')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-shopping-cart"></i> Delivery</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-shopping-cart"></i> Delivery</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                @if($user_role_id=='6')
                <table id="delivery" class="table" >
                    <thead>
                    <tr>
                        <th class="col-md-3">Order Number</th>
                        <th class="col-md-">Product</th>
                        <th class="col-md-2">Company</th>
                        <th class="col-md-1">Date Ordered</th>
                        <th class="col-md-2">Book Delivery</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{ $order->order_no }}</td>
                                <td>{{ $order->product_name }}</td>
                                <td>{{ $order->company_name }}</td>
                                <td>{{ \Carbon\Carbon::parse($order->created_at)->format('Y-m-d') }}</td>
                                <td><a class="button col-md-12" href="{{ url('/delivery/create?id='.$order->order_id) }}">Book Delivery</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @elseif($user_role_id=='8')
                    <table id="schedule" class="table" >
                        <caption>Scheduled Deliveries to FBAforward</caption>
                        <thead>
                        <tr>
                            <th class="col-md-1">Order</th>
                            <th class="col-md-2">Company</th>
                            <th class="col-md-2">Product</th>
                            <th class="col-md-2">LFD</th>
                            <th class="col-md-2">Pick Up Date</th>
                            <th class="col-md-2">Delivery Date</th>
                            <th class="col-md-1">View Order</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($schedule_shipment as $schedule)
                            <tr>
                                <td>{{$schedule->order_no}}</td>
                                <td>{{$schedule->company_name}}</td>
                                <td>{{$schedule->product_name}}</td>
                                <td>&nbsp;</td>
                                <td>{{date('m/d/Y',strtotime($schedule->ETA_to_warehouse))}}</td>
                                <td>{{date('m/d/Y',strtotime($schedule->created_at))}}</td>
                                <td><a class="button col-md-12" href="{{url('order')}}">View</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <table id="incoming" class="table" >
                        <caption>Incoming Shipments</caption>
                        <thead>
                        <tr>
                            <th class="col-md-1">Order</th>
                            <th class="col-md-2">Company</th>
                            <th class="col-md-2">Product</th>
                            <th class="col-md-2">ETD</th>
                            <th class="col-md-2">ETA</th>
                            <th class="col-md-2">LFD</th>
                            <th class="col-md-1">View Order</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($incoming_shipment as $incoming)
                            <tr>
                                <td>{{$incoming->order_no}}</td>
                                <td>{{$incoming->company_name}}</td>
                                <td>{{$incoming->product_name}}</td>
                                <td>{{date('m/d/Y',strtotime($incoming->ETD_china))}}</td>
                                <td>{{date('m/d/Y',strtotime($incoming->ETA_US))}}</td>
                                <td>&nbsp;</td>
                                <td><a class="button col-md-12" href="{{url('order')}}">View</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <table id="non_truckey" class="table" >
                        <caption>Non-Turnkey Incoming Shipments</caption>
                        <thead>
                        <tr>
                            <th class="col-md-1">Order</th>
                            <th class="col-md-2">Company</th>
                            <th class="col-md-2">Product</th>
                            <th class="col-md-2">ETA To FBAForward</th>
                            <th class="col-md-2">&nbsp;</th>
                            <th class="col-md-2">Tracking Number</th>
                            <th class="col-md-1">View Order</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($non_turkey_shipment as $non_turkey)
                            <tr>
                                <td>{{$non_turkey->order_no}}</td>
                                <td>{{$non_turkey->company_name}}</td>
                                <td>{{$non_turkey->product_name}}</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><a class="button col-md-12" href="{{url('order')}}">View</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </section>
    @endsection
@section('js')
    <script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('#delivery').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
        $('#incoming').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
        $('#non_truckey').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
         $('#schedule').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
    });
</script>
@endsection