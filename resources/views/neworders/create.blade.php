@extends('layouts.member.app')
@section('title', 'Confirm Shipping Details')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
<style>
.col-md-12 {  padding-left: 35px;  }
.item-title span, input { margin: 10px 0px !important; }
.margin10{ margin-top:10px; }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1>Confirm Shipping Details</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('neworders') }}"><i class="fa fa-shopping-cart"></i> New Orders</a></li>
            <li class="active"><a href="{{ url('neworders/create') }}"> Confirm Shipping Details</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Shipping Details</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                {!! Form::open(['url' => 'neworders', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            @foreach($order as $orders)
                                <input type="hidden" id="order_id" name="order_id" value="{{ $orders->order_id }}">
                            <div  style="background-color: #003c6b; color:#ffffff; width:99%"><span class="heading">Product Details</span></div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="no_of_carton" class="control-label col-md-12 " style="text-align:left"># of Cartons</label>
                                    <div class="col-md-12 no-padding">
                                    <input type="text" name="no_of_carton" id="no_of_carton" class="read form-control validate[required] custom[integer]" value="{{$orders->no_boxs}}" onchange="calculation()">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="qty_per_box" class="control-label col-md-12" style="text-align:left">Quantity per Carton</label>
                                    <div class="col-md-12 no-padding">
                                        <input type="text" name="qty_per_box" id="qty_per_box" class="read form-control validate[required] custom[integer]" value="{{$orders->qty_per_box}}" onchange="calculation()">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="total_unit" class="control-label col-md-12" style="text-align:left">Total Units</label>
                                    <div class="col-md-12 no-padding">
                                        <input type="text" name="total_unit" id="total_unit" class="form-control validate[required] custom[integer]" value="{{$orders->total}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div  style="background-color: #003c6b; color:#ffffff; width:99%"><span class="heading">Shipping Details</span></div>
                            <div class="form-group">
                                <div class="col-md-12 margin10">
                                    <label for="shipping_method" class="control-label col-md-12" style="text-align:left">Shipping Method</label>
                                    <div class="col-md-12 no-padding margin10">
                                        <select name="shipping_method" id="shipping_method" class="read form-control select2 validate[required]">
                                            @foreach($shipping_method as $shipping_methods)
                                            <option value="{{ $shipping_methods->shipping_method_id }}" @if($shipping_methods->shipping_method_id==$orders->shipping_method_id) selected @endif>{{ $shipping_methods->shipping_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 margin10">
                                    <label for="incoterm" class="control-label col-md-12" style="text-align:left">Incoterms</label>
                                    <div class="col-md-12 no-padding margin10">
                                        <select name="incoterm" id="incoterm" class="read form-control select2 validate[required]">
                                            @foreach($incoterm as $incoterms)
                                            <option value="{{ $incoterms->id }}" @if($incoterms->id==$orders->incoterms) selected @endif>{{ $incoterms->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="port_of_origin" class="control-label col-md-12" style="text-align:left">Port Of Origin</label>
                                    <div class="col-md-12 no-padding">
                                        <input type="text" name="port_of_origin" id="port_of_origin" class="read form-control validate[required]" value="{{$orders->port_of_origin}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="destination_port" class="control-label col-md-12" style="text-align:left">Destination Port</label>
                                    <div class="col-md-12 no-padding margin10">
                                        <select name="destination_port" id="destination_port" class="read form-control select2 validate[required]">
                                            <option value="1">Port of Long Beach</option>
                                            <option value="2">Port of Los Angeles</option>
                                            <option value="3">Los Angeles Airport</option>
                                            <option value="4">San Diego Airport</option>
                                            <option value="5">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="goods_ready_date" class="control-label col-md-12" style="text-align:left">Goods Ready Date</label>
                                    <div class="col-md-12 no-padding">
                                        <input type="text" name="goods_ready_date" id="goods_ready_date" class="read form-control datepicker validate[required]" value="{{date('m/d/Y',strtotime($orders->goods_ready_date))}}">
                                    </div>
                                </div>
                                <div class="col-md-12 margin10">
                                    <label for="shipment_type" class="control-label col-md-12" style="text-align:left">Shipment Type</label>
                                    <div class="col-md-12 no-padding margin10">
                                        <select name="shipment_type" id="shipment_type" class="read form-control select2 validate[required]">
                                            @foreach($shipment_type as $shipment_types)
                                            <option value="{{$shipment_types->id}}" @if($orders->shipment_size_type==$shipment_types->id) selected @endif>{{ $shipment_types->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="shipment_weight" class="control-label col-md-12" style="text-align:left">Shipment Weight (KG)</label>
                                    <div class="col-md-12 no-padding">
                                        <input type="text" name="shipment_weight" id="shipment_weight" class="read form-control validate[required] custom[integer]" value="{{$orders->shipment_weight}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="shipment_volume" class="control-label col-md-12" style="text-align:left">Shipment Volume (CBM)</label>
                                    <div class="col-md-12 no-padding">
                                        <input type="text" name="shipment_volume" id="shipment_volume" class="read form-control validate[required] custom[integer]" value="{{$orders->shipment_volume}}">
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-12" style="text-align: right">
                        {!! Form::button(' Edit ', ['class'=>'btn btn-primary', 'id'=>'mainedit']) !!}
                        {!! Form::submit('  Update  ', ['class'=>'btn btn-primary','id'=>'edit', 'style'=>'display:none']) !!}
                        {!! Form::submit('  Confirm  ', ['class'=>'btn btn-primary','id'=>'confirm']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
    @endsection
@section('js')
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script>
        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "Please Select",
                allowClear: true
            });
            $('.datepicker').datepicker({
                startDate: new Date(),
            });
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach', {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
            });
        });
        function calculation() {
            no_carton=1;
            qty_carton=1;
            if($("#no_of_carton").val()!='') {
                no_carton = $("#no_of_carton").val();
                total = no_carton*qty_carton;
            }
            if($("#qty_per_box").val()!='') {
                qty_carton = $("#qty_per_box").val();
                total = no_carton*qty_carton;
            }
            if($("#no_of_box").val()=='' && $("#qty_per_box").val()==''){
                total = '';
            }
            $("#total_unit").val(total);
        }
        $("#edit").click(function (event) {
            event.preventDefault();
            if ($("#validate").validationEngine('validate'))
            {
                var no_of_carton = $("#no_of_carton").val();
                var qty_per_box = $("#qty_per_box").val();
                var total_unit = $("#total_unit").val();
                var shipping_method = $("#shipping_method").val();
                var incoterm = $("#incoterm").val();
                var port_of_origin = $("#port_of_origin").val();
                var destination_port = $("#destination_port").val();
                var goods_date = $("#goods_ready_date").val();
                var shipment_type = $("#shipment_type").val();
                var shipment_weight = $("#shipment_weight").val();
                var shipment_volume = $("#shipment_volume").val();
                var order_id = $("#order_id").val();
                $.ajax({
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    method: 'put',
                    url: '/neworders/update',
                    data: {
                        no_of_carton : no_of_carton,
                        qty_per_box : qty_per_box,
                        total_unit : total_unit,
                        shipping_method : shipping_method,
                        incoterm : incoterm,
                        port_of_origin : port_of_origin,
                        destination_port : destination_port,
                        goods_date:goods_date,
                        shipment_type: shipment_type,
                        shipment_weight : shipment_weight,
                        shipment_volume : shipment_volume,
                        order_id:order_id
                    },
                    success: function (response) {
                        console.log(response);
                        window.location="/neworders";
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        });
        $(document).ready(function(){
           $(".read").attr("disabled", true);
        });
        $("#mainedit").on('click',function(){
            $(".read").attr("disabled", false);
            $("#edit").css('display','block');
            $("#mainedit").css('display','none')
        });
    </script>
@endsection