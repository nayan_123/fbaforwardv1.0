@extends('layouts.frontend.single')
@section('title', 'Welcome to '.getSetting('SITE_TITLE'))
@section('css')
    {!! Html::style('assets/plugins/pricingTable/pricingTable.min.css') !!}
    <style type="text/css">
        ul.vpt_plan > li {
            font-family: Sans-Serif;
        }
    </style>
@endsection
@section('content')
    <section id="home">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 wow fadeIn" data-wow-delay="0.3s">
                        <h1 class="text-upper">Welcome to {{ getSetting('SITE_TITLE') }}</h1>
                        <p class="tm-white"></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="contact">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                        <h2 class="text-uppercase">Contact Us</h2>
                        <h4>Address</h4>
                        {!!  getSetting('ADDRESS') !!}
                    </div>
                    <div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                        <div class="contact-form">
                            {!! Form::open(['url' =>  '/contact-us', 'method' => 'post', 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                            <div class="col-md-6">
                                {!! Form::text('name', old('name'), ['class' => 'form-control validate[required]', 'placeholder'=>'Name*', 'id'=>'name']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::email('email', old('email'), ['class' => 'form-control  validate[required,custom[email]]', 'placeholder'=>'Email*','id'=>'email']) !!}
                            </div>
                            <div class="col-md-12">
                                {!! Form::text('subject', old('subject'), ['class' => 'form-control  validate[required]', 'placeholder'=>'Subject','id'=>'subject']) !!}
                            </div>
                            <div class="col-md-12">
                                {!! Form::textarea('message', old('message'), ['class' => 'form-control  validate[required]', 'rows'=> 4, 'id'=>'message','placeholder'=>'Your Query*']) !!}
                            </div>
                            <div class="col-md-8">
                                <input type="button" onclick="contactus()" class="form-control text-uppercase"
                                       value="Send">
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    {!! Html::script('http://maps.googleapis.com/maps/api/js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach',
                {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
            $(function () {
                new WOW().init();
                $('.laraship-nav').singlePageNav({
                    offset: 70,
                    updateHash: true,
                    filter: ':not(.external)',
                });
                $('.navbar-collapse a').click(function () {
                    $(".navbar-collapse").collapse('hide');
                });
            })
        })
        function contactus() {
            name = $("#name").val();
            email = $("#email").val();
            subject = $("#subject").val();
            message = $("#message").val();
            var valid = 'false'
            if(name!= '' && email!='' && message!='')
                valid = 'true'
            else
                swal('Please provide required fields to contact us.');
            if (valid == 'true') {
                $.ajax({
                    headers: {
                        'X-CSRF-Token': "{{ csrf_token() }}"
                    },
                    method: 'POST',
                    url: '/contact-us',
                    data: {
                        'name': name,
                        'email': email,
                        'subject': subject,
                        'message': message
                    },
                    success: function (response) {
                        swal(response);
                        $("#name").val('');
                        $("#email").val('');
                        $("#subject").val('');
                        $("#message").val('');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        }
    </script>
@endsection