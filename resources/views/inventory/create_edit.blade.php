@extends('layouts.member.app')
@section('title', 'Supply Chain')
@section('css')
<style>
.item-title span, input { margin: 10px 0px !important; }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-cog"></i> Supply Chain</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class=""><a href="{{ url('supplychain') }}"><i class="fa fa-cog"></i>Supply Chain</a></li>
            <li class="active">Manage Product</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Manage Product</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['url' =>  isset($product) ? 'inventory/'.$product->id  : null, 'method' => isset($product) ? 'put' : 'post', 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-12 control-label" style="text-align:left">PRODUCT NAME </label>
                            <div class="col-md-12">
                                <div class="input-group col-md-10">
                                    <span>{{ isset($product)? $product->product_name : null }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label" style="text-align:left">PRODUCT NICKNAME (OPTIONAL) </label>
                            <div class="col-md-12">
                                <div class="input-group col-md-10">
                                    {!! Form::text('product_nick_name', old('product_nick_name', isset($product) ? $product->product_nick_name: null), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label" style="text-align:left">SUPPLIER </label>
                            <div class="col-md-12">
                                <div class="col-md-10 no-padding">
                                    {{--*/ $available_supplier = array() /*--}}
                                    @if(isset($product_supplier))
                                        @foreach($product_supplier as $product_suppliers)
                                            {{--*/ $available_supplier[] = $product_suppliers->supplier_id /*--}}
                                        @endforeach
                                    @endif
                                    <select name="supplier[]" id="supplier" class="form-control validate[required] select2" multiple="multiple">
                                        <option value=""></option>
                                        @foreach($supplier as $suppliers)
                                            <option value="{{$suppliers->supplier_id}}" @if(in_array($suppliers->supplier_id,$available_supplier)) {{ "selected" }} @endif>{{ $suppliers->contact_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2 col-md-offset-9">
                                {!! Form::submit((isset($product)?'Update': 'Add'). ' Product', ['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach', {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
            $(".select2").select2({
                placeholder: "Please Select",
            });
        });
    </script>
@endsection