@extends('layouts.member.app')
@section('title', 'Order Details')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/custom_quotes.css') !!}
@endsection
@section('content')
<style>
.content{ min-height: 620px;  }
.nav-tabs>li {  margin-bottom: 0px;  }
.item-title span, input { margin: 10px 0px !important; }
a:hover { text-decoration: none;color:#555 !important; }
a{ color:#555; }
.place-date p{ line-height: 5;  }
</style>
    <section class="content">
        <section class="content-header">
            <h1>Order Details</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><i class="fa fa-shopping-cart"></i>Order Details</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">
                            @foreach($order_detail as $order_details)
                                <div class="col-md-12">
                                    <div class="col-md-2">Order Number</div>
                                    <div class="col-md-2">{{ $order_details->order_no }}</div>
                                    <div class="col-md-2">Warehouse Status</div>
                                    <div class="col-md-2">&nbsp;</div>
                                    <div class="col-md-2">Warehouse Due Date</div>
                                    <div class="col-md-2">
                                        @if(!empty($order_details->ETA_to_warehouse))
                                            {{date('m/d/Y',strtotime($order_details->ETA_to_warehouse))}}
                                        @elseif(!empty($order_details->date_of_arrival_at_fbaforward))
                                            {{date('m/d/Y',strtotime($order_details->date_of_arrival_at_fbaforward))}}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12">Order History</div>
                                    <div class="col-md-12">&nbsp;</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection