@extends('layouts.member.app')
@section('title', 'Amazon Inventory List')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
<style>
.dropbtn { padding: 16px;font-size: 16px;border: none;cursor: pointer;  }
.dropdown { position: relative;display: inline-block; }
.dropdown-content { display: none;position: absolute;background-color: #f9f9f9;min-width: 160px;overflow: auto;box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);z-index: 1;  }
.dropdown-content a { color: black; padding: 12px 16px;text-decoration: none;  display: block;  }
.dropdown a:hover {background-color: #f1f1f1; color:#000000 !important;}
.show {display:block;}
.margin-bottom {margin-bottom: 5px;}
a, a:hover {color:#000;}
.dropdown-content{ margin-left: -100px;  z-index: 3;  }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-star"></i>Inventory List</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-star"></i>Inventory</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        @if($user->role_id=='3' || $user->role_id=='4' || $user->role_id=='7')
                        <div class="no-padding">
                            <div class="col-md-6 padding-0-5">
                                <h3>Active Orders</h3>
                            </div>
                            <table class="table" id="activelist">
                                <thead>
                                    <tr>
                                        <th class="col-md-4"><span>Product Name</span></th>
                                        <th class="col-md-1"><span>SKU</span></th>
                                        <th class="col-md-1"><span>Last Updated</span></th>
                                        <th class="col-md-1"><span>Inbound to FBAforward</span></th>
                                        <th class="col-md-1"><span>Processing at FBAforward</span></th>
                                        <th class="col-md-1"><span>Stored at FBAforward</span></th>
                                        <th class="col-md-1"><span>In Transit to Amazon</span></th>
                                        <th class="col-md-1"><span>At Amazon</span></th>
                                        <th class="col-md-1"><span>Action</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($active_order as $active_orders)
                                    {{--*/ $product_name = strlen($active_orders->product_name) > 50 ? substr($active_orders->product_name,0,50)."..." : $active_orders->product_name /*--}}
                                    <tr id="tr{{$active_orders->id}}" class="trmain">
                                        <td>@if($active_orders->product_nick_name!==''){{ $active_orders->product_nick_name }} @else {{ $product_name }} @endif</td>
                                        <td><a class="asku" href="https://www.amazon.com/dp/{{$active_orders->sellerSKU}}" target="_blank">{{$active_orders->sellerSKU}}</a></td>
                                        <td>{{ date('m/d/Y',strtotime($active_orders->updated_at)) }}</td>
                                        <td>{{ $active_orders->infbaforward }}</td>
                                        <td>{{ $active_orders->processfbaforward }}</td>
                                        <td>{{ $active_orders->atfbaforward }}</td>
                                        <td>{{ $active_orders->inamazon }}</td>
                                        <td>{{ $active_orders->atamazon }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <i onclick="myFunction('{{$active_orders->id}}')" class="dropbtn fa fa-cog"></i>
                                                <div id="myDropdown{{$active_orders->id}}" class="dropdown-content">
                                                    <a href="{{ url('amazoninventory/create?product_id='.$active_orders->id) }}">Send Inventory to Amazon</a>
                                                    @if($active_orders->hide_show=='0')
                                                    <a href="{{ url('quote/create?product_id='.$active_orders->id) }}">Get New Shipping Quote</a>
                                                    @endif
                                                    <a onclick="getnickname('{{$active_orders->id}}','{{$active_orders->product_nick_name}}')">Edit Nickname</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
                        @if($user->role_id=='3')
                        <div class="no-padding">
                            <div class="col-md-6 padding-0-5">
                                <h3>Past Orders</h3>
                            </div>
                            <table class="table" id="pastlist">
                                <thead>
                                <tr>
                                    <th class="col-md-4"><span>Product Name</span></th>
                                    <th class="col-md-5"><span>Last Updated</span></th>
                                    <th class="col-md-1"><span>In Transit to Amazon</span></th>
                                    <th class="col-md-1"><span>At Amazon</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($past_order as $past_orders)
                                    {{--*/ $product_name = strlen($past_orders->product_name) > 50 ? substr($past_orders->product_name,0,50)."..." : $past_orders->product_name /*--}}
                                    <tr>
                                        <td>@if($past_orders->product_nick_name!==''){{ $past_orders->product_nick_name }} @else {{ $product_name }} @endif</td>
                                        <td>&nbsp;</td>
                                        <td>{{ $past_orders->inamazon }}</td>
                                        <td>{{ $past_orders->atamazon }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
                        @if($user->role_id=='8' || $user->role_id=='10')
                            <div class="no-padding">
                                <div class="col-md-6">
                                    <h3>Active Orders</h3>
                                </div>
                                <table class="table" id="activelist">
                                    <thead>
                                    <tr>
                                        <th class="col-md-5"><span>Product Name</span></th>
                                        <th class="col-md-2"><span>SKU</span></th>
                                        <th class="col-md-1"><span>Cartons</span></th>
                                        <th class="col-md-1"><span>Units</span></th>
                                        <th class="col-md-3"><span>Action</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($active_order as $active_orders)
                                        <tr>
                                            <td> {{ $active_orders->product_name }}</td>
                                            <td><a class="asku" href="https://www.amazon.com/dp/{{$active_orders->sellerSKU}}" target="_blank">{{$active_orders->sellerSKU}}</a></td>
                                            <td>{{ $active_orders->carton }}</td>
                                            <td>{{ $active_orders->units }}</td>
                                            <td>
                                                @if($user->role_id==10)
                                                    <a class="button" href="{{url('amazoninventory/create?product_id='.$active_orders->id)}}">Send Inventory to Amazon </a>
                                                @endif
                                                <a class="button" href="{{ url('inventory/'.$active_orders->id) }}">View Order</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="product_nickname" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Product Nick Name</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" id="nickname_div">
                            {!! Form::open(['url' =>  'inventory/addnickname', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group" id="">
                                        {!! Form::hidden('id',old('id'), ['id'=>'id']) !!}
                                        {!! Form::label('nickname', 'Product Nick Name ',['class' => 'control-label col-md-5']) !!}
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                 {!! Form::text('nickname', old('nickname'), ['class' => 'form-control','id'=>'nickname']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', '',['class' => 'control-label col-md-5']) !!}
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                {!! Form::submit('  Submit  ', ['class'=>'btn btn-primary',  'id'=>'add']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    <script type="text/javascript">
    $(document).ready(function() {
        var activetable =$('#activelist').DataTable({
            "iDisplayLength": 5,
            "bLengthChange": false,
            "bInfo": false,
            columnDefs: [ { orderable: false, targets: [-1] } ]
        });
        var pasttable =$('#pastlist').DataTable({
            "iDisplayLength": 5,
            "bLengthChange": false,
            "bInfo": false,
        });

        $('.dataTables_filter').addClass('pull-right');
    });
    function myFunction(id) {
        document.getElementById("myDropdown"+id).classList.toggle("show");
    }
    window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
    function getnickname(id, name) {
        $("#id").val(id);
        $("#nickname").val(name);
        $("#product_nickname").modal('show');
    }
    $(document).on("click", "table tr", function(e){
        var id= this.id;
        id=id.substr(2);
        var td_id=parseInt($(this).index());
        $(".asku").css('color','#000');
        $(".trmain").removeClass('trhead');
        $("#tr"+id).addClass('trhead');
        $("#tr"+id+" td a").not(".dropdown a").css('color','#FFF');
        if($("#details"+id).length > 0) {}
        else {
            $.ajax({
                headers: {
                    'X-CSRF-Token':  "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '/inventory/inventoryhistory',
                data: {
                    'id': id,
                },
                success: function (details) {
                    $('.preloader').css("display", "none");
                    var history = $.parseJSON(details);
                    var html='';
                    var count = $(history.details).length;
                    if(count >0) {
                        $.each(history.details, function (key, value) {
                            var inbound_fba = 0;
                            var process_fba = 0;
                            var store_fba = 0;
                            var inbound_amazon = 0;
                            var at_amazon = 0;
                            if (value.status == '1') {
                                inbound_fba = value.total;
                            }
                            else if (value.status == '2') {
                                process_fba = value.total;
                            }
                            else {
                                var new_key = parseInt(key) + 1;
                                if (history.details[key].created_at == history.details[new_key].created_at) {
                                    if (history.details[key].status == '3') {
                                        store_fba = history.details[key].total;
                                    }
                                    if (history.details[key].status == '4') {
                                        inbound_amazon = history.details[key].total;
                                    }
                                    if (history.details[new_key].status == '3') {
                                        store_fba = history.details[new_key].total;
                                    }
                                    if (history.details[new_key].status == '4') {
                                        inbound_amazon = history.details[new_key].total;
                                    }
                                }
                            }
                            var date = new Date(value.created_at);
                            var day = date.getDate();
                            var month = date.getMonth()+1;
                            var year = date.getFullYear();
                            var created_at = month+"/"+day+"/"+year;
                            if(count>1) {
                                html += '<tr class="tr trdetails" style="border: none">';
                            }
                            html += '<td>' + created_at + '</td>';
                            html += '<td>' + inbound_fba + '</td>';
                            html += '<td>' + process_fba + '</td>';
                            html += '<td>' + store_fba + '</td>';
                            html += '<td>' + inbound_amazon + '</td>';
                            html += '<td>' + at_amazon + '</td>';
                            html += '<td>&nbsp;</td>';
                            if(count>1) {
                                html += '</tr>';
                            }
                        });
                        if(count>1) {

                            $(".tr").remove();
                            $("#tr" + id).after('<tr class="tr trdetails" id="details' + id + '">' +
                                '<td colspan="2" rowspan="' + (count+1) + '"><b>History</b></td>' + html +
                                '</tr>');
                        }
                        else {
                            $(".tr").remove();
                            $("#tr" + id).after('<tr class="tr trdetails" id="details' + id + '">' +
                                '<td colspan="2"><b>History</b></td>' + html +
                                '</tr>');
                        }
                    }
                    else {
                        $(".tr").remove();
                        $("#tr" + id).after('<tr class="tr trdetails" id="details' + id + '" style="border: none">' +
                            '<td colspan="10"><b>History</b></td>'+
                            '</tr>');
                    }
                }
            });
        }
    });
    </script>
@endsection