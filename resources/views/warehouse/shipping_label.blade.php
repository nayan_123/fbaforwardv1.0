@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
@endsection
@section('content')
    <section class="content-header">
        <h1> {{$title}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="javascript:void(0)">Orders</a></li>
            <li><a href="javascript:void(0)">Shipping Labels</a></li>
            <li class="active">{{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="col-md-12 trhead">
                    Order Details
                </div>
                <div class="col-md-12">
                    <div class="col-md-2">
                        Order # :
                    </div>
                    <div class="col-md-10">
                        {{isset($order_detail)? $order_detail->order_no :null}}
                    </div>
                    <div class="col-md-2">
                        Company :
                    </div>
                    <div class="col-md-2">
                        {{isset($order_detail)? $order_detail->company_name :null}}
                    </div>
                    <div class="col-md-2">
                        Customer Email :
                    </div>
                    <div class="col-md-2">
                        {{isset($order_detail)? $order_detail->contact_email :null}}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2">
                        Product :
                    </div>
                    <div class="col-md-10">
                        {{isset($order_detail)? $order_detail->product_name :null}}
                    </div>
                </div>
                <div class="col-md-2">
                  <b> Products to be Shipped </b>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4">
                       Number of Units :
                    </div>
                    <div class="col-md-2">
                        {{isset($order_detail)? $order_detail->total :null}}
                    </div>
                    <div class="col-md-4">
                        Number of Cartons :
                    </div>
                    <div class="col-md-2">
                        {{isset($order_detail)? $order_detail->no_boxs :null}}
                    </div>
                </div>
                <div class="col-md-2">
                    <b> Products to be Stored </b>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4">
                        Number of Units :
                    </div>
                    <div class="col-md-2">
                        {{isset($order_detail)? $order_detail->total :null}}
                    </div>
                    <div class="col-md-4">
                        Number of Cartons :
                    </div>
                    <div class="col-md-2">
                        {{isset($order_detail)? $order_detail->no_boxs :null}}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive no-padding">
                        <table class="table" id="list">
                            <thead>
                            <tr>
                                <th><span>Amazon Destination</span></th>
                                <th><span>Prep Label</span></th>
                                <th><span>Shipping Label</span></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    @foreach($amazon_destination as $amazon_destinations)
                                        @if($amazon_destinations->fulfillment_network_SKU==$order_detail->fnsku)
                                            {{ $amazon_destinations->destination_name }}<br>
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($amazon_destination as $amazon_destinations)
                                        @if($amazon_destinations->fulfillment_network_SKU==$order_detail->fnsku)
                                            {{ $amazon_destinations->label_prep_type }}<br>
                                        @endif
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($amazon_destination as $amazon_destinations)
                                        @if($amazon_destinations->fulfillment_network_SKU==$order_detail->fnsku)
                                            <span id="label_tr{{$amazon_destinations->amazon_destination_id}}">
                                                <a href="{{url('warehouse/printshippinglabel/'.$amazon_destinations->amazon_destination_id)}}">Print </a>
                                                <span id="label{{$amazon_destinations->amazon_destination_id}}">@if($amazon_destinations->shipping_label==1)<a href="javascript:void(0)" onclick="verifyshipment('{{$amazon_destinations->amazon_destination_id}}','2')">verify labels were complete</a>@endif</span>
                                                @if($amazon_destinations->shipping_label==2)<a href="javascript:void(0)" onclick="verifyshipment('{{$amazon_destinations->amazon_destination_id}}','3')">Verify Shipment Load On Truck</a>@endif
                                                <span id="ship_load{{$amazon_destinations->amazon_destination_id}}" colspan="2" hidden><a href="javascript:void(0)" onclick="verifyshipment('{{$amazon_destinations->amazon_destination_id}}','3')">Verify Shipment Load On Truck</a></span><br>
                                                </span>
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        function verifyshipment(amazon_destination_id, status) {
            $('.preloader').css("display", "block");
            $.ajax({
                headers: {
                    'X-CSRF-Token': "<?php echo e(csrf_token()); ?>"
                },
                method: 'POST',
                url: '/warehouse/verifylabel',
                data: {
                    'amazon_destination_id': amazon_destination_id,
                    'status': status
                },
                success: function (response) {
                    $('.preloader').css("display", "none");
                    if (response == '2') {
                        $("#label" + amazon_destination_id).hide();
                        $("#ship_load" + amazon_destination_id).show();
                    }
                    else if (response == '3') {
                        $("#label_tr" + amazon_destination_id).hide();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.preloader').css("display", "none");
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
    </script>
@endsection