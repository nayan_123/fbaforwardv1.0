@extends('layouts.member.app')
@section('title', 'Packing List')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
<style>
    .col-md-12 {  padding-left: 35px;  } .shead { padding-right: 1px;  }  a:hover{ color: #3c8dbc !important;  }
</style>
@endsection
@section('content')
<section class="content-header">
    <h1>Packing List</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="javascript:void(0)"> Check In</a></li>
        <li><a href="javascript:void(0)"> View PL</a></li>
        <li class="active">Packing List</li>
    </ol>
</section>
<section class="content">
    <div class="box border-top-0">
        <div class="box-body ">
            <div class="row ">
                <div class="col-md-12">
                    <table class="table">
                        <tr>
                            <td class="col-md-12">Order #  {{ isset($packaging) ? $packaging->order_no : null }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="col-md-12"><h4>Packaging List</h4></div>
            <div class="col-md-12">
                <p><a target="_blank" href="{{ url('uploads/orderpackage',isset($packaging) ? $packaging->file : null) }}">{{ isset($packaging) ? $packaging->file : null }}</a></p>
            </div>
        </div>
        <div class="box-body" id="file_div">
            <div class="col-md-12">
                <iframe class="col-md-12 no-padding" id="fileframe"  src="{{ url(public_path().'/uploads/orderpackage/'.isset($packaging) ? $packaging->file : null) }}" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')

@endsection