@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
@endsection
@section('content')
    <section class="content-header">
        <h1> {{$title}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="javascript:void(0)">Orders</a></li>
            <li><a href="javascript:void(0)">Review Shipment</a></li>
            <li class="active">{{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="col-md-12 trhead">
                    Order Details
                </div>
                <div class="col-md-12">
                    <div class="col-md-2">
                        Order # :
                    </div>
                    <div class="col-md-10">
                        {{isset($order_detail)? $order_detail->order_no :null}}
                    </div>
                    <div class="col-md-2">
                        Company :
                    </div>
                    <div class="col-md-2">
                        {{isset($order_detail)? $order_detail->company_name :null}}
                    </div>
                    <div class="col-md-2">
                        Customer Email :
                    </div>
                    <div class="col-md-2">
                        {{isset($order_detail)? $order_detail->contact_email :null}}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2">
                        Product :
                    </div>
                    <div class="col-md-10">
                        {{isset($order_detail)? $order_detail->product_name :null}}
                    </div>
                </div>
                <div class="col-md-12 trhead">
                    Amazon Shipment Destails
                </div>
                <div class="col-md-12">
                    <div class="col-md-3"><b>Amazon destination</b></div>
                    <div class="col-md-3"><b>Shipping label printed</b></div>
                    <div class="col-md-3"><b>Label were completed</b></div>
                    <div class="col-md-3"><b>shipment was loaded onto truck</b></div>

                </div>
                <div class="col-md-12">
                    @foreach($amazon_destination as $amazon_destinations)
                        @if($amazon_destinations->fulfillment_network_SKU==$order_detail->fnsku)
                            <div class="col-md-3">{{ $amazon_destinations->destination_name }}</div>
                            <div class="col-md-3">{{ $amazon_destinations->total_units }}</div>
                            <div class="col-md-3">{{ date('m/d/Y',strtotime($amazon_destinations->updated_at)) }}</div>
                            <div class="col-md-3">{{ date('m/d/Y',strtotime($amazon_destinations->updated_at)) }}</div>
                        @endif
                    @endforeach

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ url('warehouse/complete/'.$order_detail->order_id) }}" class="button">Complete Shipment</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        function prepcomplete(order_id, amazon_destination_id) {
            $('.preloader').css("display", "block");

            $.ajax({
                headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}"
                },
                method: 'POST', // Type of response and matches what we said in the route
                url: '/warehouse/prepcomplete', // This is the url we gave in the route
                data: {
                    'order_id': order_id,
                    'amazon_destination_id' : amazon_destination_id,
                }, // a JSON object to send back
                success: function (response) { // What to do if we succeed
                    $('.preloader').css("display", "none");
                    $("#prep" + amazon_destination_id).hide();
                },
                error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                    $('.preloader').css("display", "none");
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
    </script>
@endsection