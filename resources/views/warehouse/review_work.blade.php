@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    <style type="text/css">
    .control-label
    {
        text-align: left !important;
    }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <h1> {{$title}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="javascript:void(0)">Orders</a></li>
            <li><a href="javascript:void(0)">Review Prep</a></li>
            <li class="active">{{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12 trhead">
                        Order Details
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            Order # :
                        </div>
                        <div class="col-md-10">
                            {{isset($order_detail)? $order_detail->order_no :null}}
                        </div>
                        <div class="col-md-3">
                            Outbound Shipping :
                        </div>
                        <div class="col-md-3">
                            {{isset($order_detail)? $order_detail->outbound_name :null }}
                        </div>
                        <div class="col-md-3">
                           <a class="button" href="javascript:void(0)" onclick="getprice('{{$order_detail->order_id}}')"> Amazon Price For Outbound</a>
                        </div>
                        <div class="col-md-3">
                           &nbsp;
                        </div>
                    </div>
                    {!! Form::open(['url' => 'warehouse/submitreviewwork', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                    <input type="hidden" name="order_id" id="order_id" value="{{$order_detail->order_id}}">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! htmlspecialchars_decode(Form::label('no_cartons', 'Number Of Cartons', ['class' => 'control-label col-md-12','style'=>'margin-left:10px'])) !!}
                            <div class="col-md-12">
                                <input type="text" name="no_cartons" id="no_cartons" class="form-control validate[required]">
                            </div>
                            {!! htmlspecialchars_decode(Form::label('number_carton', 'Number of Units Per Carton ', ['class' => 'control-label col-md-12','style'=>'margin-left:10px'])) !!}
                            <div class="col-md-12">
                                <input type="text" name="number_carton" id="number_carton" class="form-control validate[required]">
                            </div>
                            {!! htmlspecialchars_decode(Form::label('outbound_date', 'Estimated Outbound Shipping Date ', ['class' => 'control-label col-md-12','style'=>'margin-left:10px'])) !!}
                            <div class="col-md-12">
                                <input type="text" name="outbound_date" id="outbound_date" class="datepicker form-control validate[required]">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 trhead">Prep Details</div>
                        @foreach($prep_detail as $prep_details)
                        <div class="col-md-12">
                            <div class="col-md-5">{{ $prep_details->service_name }}</div>
                            <div class="col-md-3">@if($prep_details->complete==0) Pending @else Completed @endif</div>
                        </div>
                        @endforeach
                    <div class="col-md-12" style="text-align: right">
                        {!! Form::submit('  Approve Prep  ', ['class'=>'button']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="modal fade" id="price_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">Amazon Price For Outbound</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" id="main">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Amazon Destination</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($amazon_destination as $amazon_destinations)
                                        <tr>
                                            <td>{{$amazon_destinations->destination_name}}</td>
                                            <td><a href="javascript:void(0)" onclick="getapiprice('{{$amazon_destinations->api_shipment_id}}','{{$amazon_destinations->order_id}}')">Get Price</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        $('.datepicker').datepicker({
            startDate: new Date(),
        });
        function prepcomplete(order_id, amazon_destination_id) {
            $('.preloader').css("display", "block");

            $.ajax({
                headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}"
                },
                method: 'POST', // Type of response and matches what we said in the route
                url: '/warehouse/prepcomplete', // This is the url we gave in the route
                data: {
                    'order_id': order_id,
                    'amazon_destination_id' : amazon_destination_id,
                }, // a JSON object to send back
                success: function (response) { // What to do if we succeed
                    $('.preloader').css("display", "none");
                    $("#prep" + amazon_destination_id).hide();
                },
                error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                    $('.preloader').css("display", "none");
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
        function sendquantity(order_id)
        {
            var storageqty = $("#storage").val();
            var amazonqty = $("#amazon").val();
            var product_id = $("#product_id").val();
            $.ajax({
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                method: 'post',
                url: '/warehouse/sendquantity',
                data: {
                    order_id:order_id,
                    storageqty:storageqty,
                    amazonqty:amazonqty,
                    product_id:product_id
                },
                success: function (response) {
                    console.log(response);
                    window.location="/warehouse/reviewwork/".order_id;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
        function getprice() {
            $("#price_modal").modal('show');
        }
        function getapiprice(shipment_id,order_id)
        {
            $.ajax({
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                method: 'post',
                url: '/warehouse/getestimateshippingcost',
                data: {
                    shipment_id:shipment_id,
                    order_id:order_id
                },
                success: function (response) {
                    console.log(response);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
    </script>
@endsection