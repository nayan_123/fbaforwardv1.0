@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    <style>
    .line-width { width:12.49% }  .line-width1 {  width:11.11% } .padding-0-7 { padding: 0px 0px !important;  }  .label{  color: #7d7f80;  font-weight: normal;  }
    </style>
@endsection
@section('content')
<section class="content-header">
    <h1> {{$title}}</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="javascript:void(0)">Orders</a></li>
        <li><a href="javascript:void(0)">Check In</a></li>
        <li class="active">{{$title}}</li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="row">
                {!! Form::open(['url' => 'warehouse/warehousecheckinform', 'method' => 'put', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                {!! Form::hidden('order_id', old('order_id', isset($order_id)?$order_id:null), ['class' => 'form-control']) !!}
                {!! Form::hidden('product_id', old('product_id', isset($user)?$user->product_id:null), ['class' => 'form-control']) !!}
                {!! Form::hidden('total', old('total', isset($user)?$user->total:null), ['class' => 'form-control']) !!}
                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="shipment-box padding-0-7">
                            <h3>Order Details</h3>
                        </div>
                    </div>
                    <div class="col-md-2"><b>Order #</b></div>
                    <div class="col-md-5">{{isset($user)? $user->order_no :null}}</div>
                    <div class="col-md-2"><b>Expected Units</b></div>
                    <div class="col-md-3">{{isset($user)? $user->total :null}}</div>
                    <div class="col-md-2"><b>Company</b></div>
                    <div class="col-md-5">{{isset($user)? $user->company_name :null}}</div>
                    <div class="col-md-2"><b>Expected Cartons</b></div>
                    <div class="col-md-3">{{isset($user)? $user->no_boxs :null}}</div>
                    <div class="col-md-2"><b>Product</b></div>
                    <div class="col-md-5">{{isset($user)? $user->product_name :null}}</div>
                    <div class="col-md-2"><b>Expected Units/Carton</b></div>
                    <div class="col-md-3">{{isset($user)? ($user->total/$user->no_boxs) :null}}</div>
                    @if($warehouse_checkin)
                    <div class="col-md-12" style="border-style: solid; border-color: #FF0000">
                        <span style='color: #FF0000; font-weight: bold;'> Revision Request : {{ $warehouse_checkin->revision_note }}</span>
                    </div>
                   @endif
                    <div class="col-md-12">
                        <div class="shipment-box padding-0-7">
                            <h3>Product Details</h3>
                        </div>
                    </div>
                    <div class="col-md-12" id="label_divs">
                        @if($warehouse_checkin)
                           @foreach($warehouse_labels as $key=>$warehouse_label)
                               @if($key==0)
                                <div class="form-group" id="label_div{{$key+1}}">
                                    {!! htmlspecialchars_decode(Form::label('label', 'Labels', ['class' => 'control-label col-md-2'])) !!}
                                    <div class="col-md-4" style="padding-top: 1%">
                                        <select class="form-control validate[required] select2" name="label" id="label">
                                            <option value=""></option>
                                            <option value="1" @if($warehouse_checkin->label=='1') selected @endif>FNSKU</option>
                                            <option value="2" @if($warehouse_checkin->label=='2') selected @endif>UPC/SKU</option>
                                            <option value="3" @if($warehouse_checkin->label=='3') selected @endif>No Labels</option>
                                        </select>
                                    </div>
                                    {!! htmlspecialchars_decode(Form::label('label_number', 'Label Number(s) ', ['class' => 'control-label col-md-2'])) !!}
                                    <div class="col-md-4">
                                        <div class="col-md-10">
                                            <input type="text" name="label_number{{$key+1}}" id="label_number{{$key+1}}" class="form-control validate[required]" value="{{ $warehouse_label->label_number }}">
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>
                                </div>
                                @else
                                    <div class="form-group" id="label_div{{$key+1}}">
                                        {!! htmlspecialchars_decode(Form::label('', 'Labels', ['class' => 'control-label col-md-2'])) !!}
                                        <div class="col-md-4" style="padding-top: 1%">&nbsp;</div>
                                        {!! htmlspecialchars_decode(Form::label('label_number', 'Label Number(s) ', ['class' => 'control-label col-md-2'])) !!}
                                        <div class="col-md-4">
                                            <div class="col-md-10">
                                                <input type="text" name="label_number{{$key+1}}" id="label_number{{$key+1}}" class="form-control validate[required]" value="{{ $warehouse_label->label_number }}">
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>
                                    </div>
                                @endif
                           @endforeach
                        @else
                    <div class="form-group" id="label_div1">
                        {!! htmlspecialchars_decode(Form::label('label', 'Labels', ['class' => 'control-label col-md-2'])) !!}
                        <div class="col-md-4" style="padding-top: 1%">
                            <select class="form-control validate[required] select2" name="label" id="label">
                                <option value=""></option>
                                <option value="1">FNSKU</option>
                                <option value="2">UPC/SKU</option>
                                <option value="3">No Labels</option>
                            </select>
                        </div>
                        {!! htmlspecialchars_decode(Form::label('label_number', 'Label Number(s) ', ['class' => 'control-label col-md-2'])) !!}
                        <div class="col-md-4">
                            <div class="col-md-10">
                                <input type="text" name="label_number1" id="label_number1" class="form-control validate[required]">
                            </div>
                            <div class="col-md-2">
                               &nbsp;
                            </div>
                        </div>
                    </div>
                         @endif
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-11" style="text-align: right">
                                <label name="add_label_button" id="add_label_button" onclick="addnumber()"  style="color: #0088cc" class="control-label">+ Add Another Number</label>
                            </div>
                            @if(count($warehouse_labels)>0) {{--*/ $count = count($warehouse_labels)/*--}} @else {{--*/ $count=1 /*--}} @endif
                            <input type="hidden" name="label_count" id="label_count" value="{{ $count }}">
                        </div>
                    <div id="lineitems">
                        @if(empty($warehouse_lineitems))
                        <div id="lineitem1" class="lineitem">
                            <div class="col-md-12"><b>Cartoon Details - Product 1</b></div>
                            <div class="col-md-12">
                            <div class="form-group row">
                                <div class="col-md-2 line-width1">
                                    {!! htmlspecialchars_decode(Form::label('length', 'Length (in)', ['class' => 'control-label label'])) !!}
                                    <input type="text" name="length1" id="length1" class="form-control validate[required]">
                                </div>
                                <div class="col-md-2 line-width1">
                                    {!! htmlspecialchars_decode(Form::label('width', 'Width (in)', ['class' => 'control-label label'])) !!}
                                    <input type="text" name="width1" id="width1" class="form-control validate[required]">
                                </div>
                                <div class="col-md-2 line-width1">
                                    {!! htmlspecialchars_decode(Form::label('height', 'Height (in)', ['class' => 'control-label label'])) !!}
                                    <input type="text" name="height1" id="height1" class="form-control validate[required]">
                                </div>
                                <div class="col-md-2 line-width1">
                                    {!! htmlspecialchars_decode(Form::label('weight', 'Weight (lb)', ['class' => 'control-label label'])) !!}
                                    <input type="text" name="weight1" id="weight1" class="form-control validate[required]">
                                </div>
                                <div class="col-md-2 line-width">
                                    {!! htmlspecialchars_decode(Form::label('units_per_carton', 'Units Per Carton ', ['class' => 'control-label label'])) !!}
                                    <input type="text" name="units_per_carton1" id="units_per_carton1" class="form-control validate[required]">
                                </div>
                                <div class="col-md-2 line-width">
                                    {!! htmlspecialchars_decode(Form::label('unit_subtotal', 'Units Subtotal ', ['class' => 'control-label label'])) !!}
                                    <input type="text" name="unit_subtotal1" id="unit_subtotal1" class="form-control validate[required]" onchange="calculation()">
                                </div>
                                <div class="col-md-2 line-width">
                                    {!! htmlspecialchars_decode(Form::label('cartons_subtotal', 'Cartons Subtotal', ['class' => 'control-label label'])) !!}
                                    <input type="text" name="cartons_subtotal1" id="cartons_subtotal1" class="form-control validate[required]" onchange="calculation()">
                                </div>
                                <div class="col-md-2 line-width">
                                    {!! htmlspecialchars_decode(Form::label('volume', 'Volume (cbm) ', ['class' => 'control-label label'])) !!}
                                    <input type="text" name="volume1" id="volume1" class="form-control validate[required]" onchange="calculation()">
                                </div>
                                <div class="col-md-1" style="text-align: right; width:0%">&nbsp;</div>
                            </div>
                            </div>
                        </div>
                        @else
                           @foreach($warehouse_lineitems as $key=>$warehouse_lineitem)
                                <div id="lineitem{{$key+1}}" class="lineitem">
                                    <div class="col-md-12"><b>Cartoon Details - Product {{ $key+1 }}</b></div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col-md-2 line-width1">
                                                {!! htmlspecialchars_decode(Form::label('length', 'Length (in)', ['class' => 'control-label label'])) !!}
                                                <input type="text" name="length{{ $key+1 }}" id="length{{ $key+1 }}" class="form-control validate[required]" value="{{ $warehouse_lineitem->length }}">
                                            </div>
                                            <div class="col-md-2 line-width1">
                                                {!! htmlspecialchars_decode(Form::label('width', 'Width (in)', ['class' => 'control-label label'])) !!}
                                                <input type="text" name="width{{ $key+1 }}" id="width{{ $key+1 }}" class="form-control validate[required]" value="{{ $warehouse_lineitem->width }}">
                                            </div>
                                            <div class="col-md-2 line-width1">
                                                {!! htmlspecialchars_decode(Form::label('height', 'Height (in)', ['class' => 'control-label label'])) !!}
                                                <input type="text" name="height{{ $key+1 }}" id="height{{ $key+1 }}" class="form-control validate[required]" value="{{ $warehouse_lineitem->height }}">
                                            </div>
                                            <div class="col-md-2 line-width1">
                                                {!! htmlspecialchars_decode(Form::label('weight', 'Weight (lb)', ['class' => 'control-label label'])) !!}
                                                <input type="text" name="weight{{ $key+1 }}" id="weight{{ $key+1 }}" class="form-control validate[required]" value="{{ $warehouse_lineitem->weight }}">
                                            </div>
                                            <div class="col-md-2 line-width">
                                                {!! htmlspecialchars_decode(Form::label('units_per_carton', 'Units Per Carton ', ['class' => 'control-label label'])) !!}
                                                <input type="text" name="units_per_carton{{ $key+1 }}" id="units_per_carton{{ $key+1 }}" class="form-control validate[required]" value="{{ $warehouse_lineitem->unit_per_carton }}">
                                            </div>
                                            <div class="col-md-2 line-width">
                                                {!! htmlspecialchars_decode(Form::label('unit_subtotal', 'Units Subtotal ', ['class' => 'control-label label'])) !!}
                                                <input type="text" name="unit_subtotal{{ $key+1 }}" id="unit_subtotal{{ $key+1 }}" class="form-control validate[required]" onchange="calculation()" value="{{ $warehouse_lineitem->unit_subtotal }}">
                                            </div>
                                            <div class="col-md-2 line-width">
                                                {!! htmlspecialchars_decode(Form::label('cartons_subtotal', 'Cartons Subtotal', ['class' => 'control-label label'])) !!}
                                                <input type="text" name="cartons_subtotal{{ $key+1 }}" id="cartons_subtotal{{ $key+1 }}" class="form-control validate[required]" onchange="calculation()" value="{{ $warehouse_lineitem->cartons_subtotal }}">
                                            </div>
                                            <div class="col-md-2 line-width">
                                                {!! htmlspecialchars_decode(Form::label('volume', 'Volume (cbm) ', ['class' => 'control-label label'])) !!}
                                                <input type="text" name="volume{{ $key+1 }}" id="volume{{ $key+1 }}" class="form-control validate[required]" onchange="calculation()" value="{{ $warehouse_lineitem->volume_subtotal }}">
                                            </div>
                                            <div class="col-md-1" style="text-align: right; width:0%">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                           @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        @if(count($warehouse_lineitems) >0) {{--*/ $cnt=count($warehouse_lineitems) /*--}} @else {{--*/ $cnt=1 /*--}} @endif
                        <input type="hidden" id="cnt" name="cnt" value="{{$cnt}}">
                        <div class="col-md-11" style="text-align: right">
                        <label name="add_line_button" id="add_line_button" onclick="additems()"  style="color: #0088cc" class="control-label">+ Add Product</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <div class="col-md-2" style="width:56.93%">&nbsp;</div>
                            <div class="col-md-2 line-width">
                                {!! htmlspecialchars_decode(Form::label('unit_total', 'Unit Total', ['class' => 'control-label'])) !!}
                                <input type="text" name="unit_total" id="unit_total" class="form-control validate[required]" readonly value="{{ isset($warehouse_checkin) ? $warehouse_checkin->total_units : null }}">
                            </div>
                            <div class="col-md-2 line-width">
                                {!! htmlspecialchars_decode(Form::label('carton_total', 'Carton Total ', ['class' => 'control-label'])) !!}
                                <input type="text" name="carton_total" id="carton_total" class="form-control validate[required]" readonly value="{{ isset($warehouse_checkin) ? $warehouse_checkin->total_cartons : null }}">
                            </div>
                            <div class="col-md-2 line-width" style="width:14.49%">
                                {!! htmlspecialchars_decode(Form::label('volume_total', 'Volume (CBM) Total ', ['class' => 'control-label'])) !!}
                                <input type="text" name="volume_total" id="volume_total" class="form-control validate[required]" readonly value="{{ isset($warehouse_checkin) ? $warehouse_checkin->total_volume : null }}">
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-12">
                        <div class="shipment-box padding-0-7">
                            <h3>Shipment Details</h3>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! htmlspecialchars_decode(Form::label('delivered_by', 'Delivered By', ['class' => 'control-label col-md-2'])) !!}
                        <div class="col-md-4">
                            <input type="text" name="delivered_by" id="delivered_by" class="form-control validate[required]" value="{{ isset($warehouse_checkin) ? $warehouse_checkin->deliver_by : null }}">
                        </div>
                        {!! htmlspecialchars_decode(Form::label('air_waybill', 'Air Waybill # ', ['class' => 'control-label col-md-2'])) !!}
                        <div class="col-md-4">
                            <input type="text" name="air_waybill" id="air_waybill" class="form-control" value="{{ isset($warehouse_checkin) ? $warehouse_checkin->air_way : null }}">
                        </div>
                    </div>
                    <div class="form-group">
                        {!! htmlspecialchars_decode(Form::label('shipment_condition', 'Shipment Condition ', ['class' => 'control-label col-md-2'])) !!}
                        <div class="col-md-4">
                            <select name="shipment_condition" id="shipment_condition" class="form-control validate[required] select2">
                                <option value=""></option>
                                <option value="1" @if($warehouse_checkin) @if($warehouse_checkin->shipment_condition=='1') selected @endif @endif>Good</option>
                                <option value="2" @if($warehouse_checkin) @if($warehouse_checkin->shipment_condition=='2') selected @endif @endif>Fair</option>
                                <option value="3" @if($warehouse_checkin) @if($warehouse_checkin->shipment_condition=='3') selected @endif @endif>Poor</option>
                            </select>
                        </div>
                        {!! htmlspecialchars_decode(Form::label('condition_note', 'Condition Notes', ['class' => 'control-label col-md-2'])) !!}
                        <div class="col-md-4">
                            <textarea name="condition_note" id="condition_note" class="form-control" rows="3" cols="12">{{isset($warehouse_checkin) ? $warehouse_checkin->condition_note : null }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! htmlspecialchars_decode(Form::label('photos', 'Photos Of Shipment', ['class' => 'control-label col-md-2'])) !!}
                        <div class="col-md-4">
                            <input type="file" name="photos[]" id="photos" class="validate[required]" multiple>
                        </div>
                        <div class="col-md-6">&nbsp;</div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            {!! htmlspecialchars_decode(Form::label('notified', 'Does the customer need to be notified of issues with the products? ', ['class' => 'control-label'])) !!}
                            <div class="checkbox">
                                <label class="col-md-3"><input type="checkbox" name="notify" id="notify" class="validate[required]radio chk" value="0" @if($warehouse_checkin) @if($warehouse_checkin->notified=='0') checked @endif @endif> No</label>
                                <label class="col-md-9"><input type="checkbox" name="notify" id="notify" class="validate[required]radio chk" value="1" @if($warehouse_checkin) @if($warehouse_checkin->notified=='1') checked @endif @endif> Yes</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <textarea name="notified" id="notified" class="form-control" rows="3" cols="12" style="display: none">{{isset($warehouse_checkin) ? $warehouse_checkin->notified_note : null }}</textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-offset-9">
                            {!! Form::submit(' Submit to Admin ', ['class'=>'btn btn-primary', ]) !!}
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
</section>
@endsection
@section('js')
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
// Validation Engine init
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach',
                {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
            $(".select2").select2({
                placeholder: "Please Select",
                allowClear: true
            });
            $('.chk').click(function(){
                $('.chk').each(function(){
                    $(this).prop('checked', false);
                });
                if($(this).val()=='1') {
                    $("#notified").css('display','block');
                }
                else{
                    $("#notified").css('display','none');
                }
                $(this).prop('checked', true);
            });
        });
    function  additems() {
        var count = $("#cnt").val();
        var new_count = parseInt(count)+1;
        $("#lineitems").append('<div id="lineitem'+new_count+'" class="lineitem"><div class="col-md-12"><b>Cartoon Details - Product '+new_count+'</b></div><div class="col-md-12"><div class="form-group row"><div class="col-md-2 line-width1"><label name="length" class="control-label label">Length (in)</label><input type="text" name="length'+new_count+'" id="length'+new_count+'" class="form-control validate[required]"></div><div class="col-md-2 line-width1"><label name="width" class="control-label label">Width (in)</label><input type="text" name="width'+new_count+'" id="width'+new_count+'" class="form-control validate[required]"></div><div class="col-md-2 line-width1 "><label name="height" class = "control-label label"> Height (in)</label><input type="text" name="height'+new_count+'" id="height'+new_count+'" class="form-control validate[required]"></div><div class="col-md-2 line-width1 "><label name="weight" class="control-label label">Weight (lb)</label><input type="text" name="weight'+new_count+'" id="weight'+new_count+'" class="form-control validate[required]"></div><div class="col-md-2 line-width"><label name="units_per_carton" class ="control-label label">Units Per Carton</label><input type="text" name="units_per_carton'+new_count+'" id="units_per_carton'+new_count+'" class="form-control validate[required]"></div><div class="col-md-2 line-width"><label name="unit_subtotal" class="control-label label">Units Subtotal</label><input type="text" name="unit_subtotal'+new_count+'" id="unit_subtotal'+new_count+'" class="form-control validate[required]" onchange="calculation()"></div><div class="col-md-2 line-width"><label name="cartons_subtotal" class="control-label label">Cartons Subtotal</label><input type="text" name="cartons_subtotal'+new_count+'" id="cartons_subtotal'+new_count+'" class="form-control validate[required]" onchange="calculation()"></div><div class="col-md-2 line-width"><label name="volume" class="control-label label">Volume (cbm)</label><input type="text" name="volume'+new_count+'" id="volume'+new_count+'" class="form-control validate[required]" onchange="calculation()"></div><div class="col-md-1" style="text-align: right; width:0%"><label class="control-label label"></label><i class="fa fa-remove" name="remove_line_button" id="remove_line_button" onclick="removeitems('+new_count+')" style="color: red"></i></div></div></div></div>');
        $("#cnt").val(new_count);
    }
    function removeitems(count)
    {
        $("#unit_total").val(parseInt($("#unit_total").val())- parseInt($("#unit_subtotal"+count).val()));
        $("#carton_total").val(parseInt($("#carton_total").val())- parseInt($("#cartons_subtotal"+count).val()));
        $("#volume_total").val(parseInt($("#volume_total").val())- parseInt($("#volume"+count).val()));
        $("#lineitem"+count).remove();
    }
    function  calculation() {
        var unit_subtotal=0;
        var cartons_subtotal=0;
        var volume_subtotal=0;
        var total_cnt = $("#cnt").val();
        for(var new_cnt=1; new_cnt<=total_cnt;new_cnt++)
        {
            if($("#unit_subtotal"+new_cnt).val()!==undefined) {
                unit_subtotal += parseInt($("#unit_subtotal" + new_cnt).val());
            }
            if($("#cartons_subtotal"+new_cnt).val()!==undefined) {
                cartons_subtotal += parseInt($("#cartons_subtotal" + new_cnt).val());
            }
            if($("#volume"+new_cnt).val()!==undefined) {
                volume_subtotal += parseInt($("#volume" + new_cnt).val());
            }
        }
        $("#unit_total").val(unit_subtotal);
        $("#carton_total").val(cartons_subtotal);
        $("#volume_total").val(volume_subtotal);
    }
    function addnumber()
    {
       var label_count = $("#label_count").val();
       var new_label_count =parseInt(label_count)+1;
         $("#label_divs").append('<div class="form-group" id="label_div'+new_label_count+'"><label class="col-md-2"></label><div class="col-md-4" style="padding-top: 1%">&nbsp;</div><label class="col-md-2"></label> <div class="col-md-4"><div class="col-md-10"> <input type="text" name="label_number'+new_label_count+'" id="label_number'+new_label_count+'" class="form-control validate[required]"></div><div class="col-md-2"><i class="fa fa-remove" name="remove_label_button" id="remove_label_button" onclick="removelabel('+new_label_count+')" style="color: red"></i></div></div></div>');
            $("#label_count").val(new_label_count);
    }
    function removelabel(count)
    {
        $("#label_div"+count).remove();
    }

    </script>
@endsection
