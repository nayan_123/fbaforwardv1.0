@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    {!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
    <section class="content">
        <section class="content-header">
            <h1> {{$title}} </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-shopping-cart"></i> Orders</a></li>
                <li class="active"> {{$title}}</li>
            </ol>
        </section>
        <section class="content">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive no-padding">
                                @if($user_role=='10')
                                    <table id="inprogress_data" class="table" >
                                        <thead>
                                        <tr>
                                            <th class="col-md-2">Order Number</th>
                                            <th class="col-md-4">Product</th>
                                            <th class="col-md-2">Company</th>
                                            <th class="col-md-1">Status</th>
                                            <th class="col-md-3">&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($order))
                                            @foreach($order as $orders)
                                                <tr>
                                                    <td>{{ $orders['order_no'] }}</td>
                                                    <td>{{ $orders['product_name'] }}</td>
                                                    <td>{{ isset($orders['company_name']) ? $orders['company_name'] : null }}</td>
                                                    <td>{{ $orderStatus[$orders['is_activated']] }}</td>
                                                    <td>
                                                        @if($orders['hold']=='2')
                                                            <a href="{{ url('order/approvewarehouse/'.$orders['order_id']) }}" class="button">Approve Warehouse</a>
                                                        @endif
                                                        @if($orders['is_activated']=='15' || $orders['is_activated']=='0')
                                                            <a href="{{url('warehouse/warehousecheckinform/'.$orders['order_id'])}}" class="button">Receive Shipment </a>
                                                            <a href="{{url('warehouse/packaginglist/'.$orders['order_id'])}}" class="button">View PL</a>
                                                        @elseif($orders['is_activated']=='17')
                                                            <a class="button" href="{{url('warehouse/viewchecklist/'.$orders['order_id'].'/0')}}">View Requirements</a>
                                                            <a class="button" href="{{url('warehouse/viewchecklist/'.$orders['order_id'].'/1')}}">Mark Processing Complete </a>
                                                        @elseif($orders['is_activated']=='19')
                                                            <a class="button" href="{{url('warehouse/shippinglabel/'.$orders['order_id'])}}">View Requirements</a>
                                                            <a href="{{url('warehouse/submitshipment/'.$orders['order_id'])}}" class="button">Complete Shipment</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                @elseif($user_role=='8')
                                    <table id="inprogress_data" class="table" >
                                        <thead>
                                        <tr>
                                            <th class="col-md-2">Order Number</th>
                                            <th class="col-md-4">Product</th>
                                            <th class="col-md-2">Company</th>
                                            <th class="col-md-1">Status</th>
                                            <th class="col-md-3">&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($order))
                                            @foreach($order as $orders)
                                                <tr>
                                                    <td>{{ $orders['order_no'] }}</td>
                                                    <td>{{ $orders['product_name'] }}</td>
                                                    <td>{{ isset($orders['company_name']) ? $orders['company_name'] : null }}</td>
                                                    <td>{{ $orderStatus[$orders['is_activated']] }}</td>
                                                    <td>
                                                        @if($orders['is_activated']=='16')
                                                            <a href="{{url('warehouse/warehousecheckinreview/'.$orders['order_id'])}}" class="button">Review Check In</a>
                                                        @elseif($orders['is_activated']=='18')
                                                            <a href="{{url('warehouse/reviewwork/'.$orders['order_id'])}}" class="button">Review Work Complete</a>
                                                        @elseif($orders['is_activated']=='20')
                                                            <a href="{{  url('warehouse/shipmentreview/'.$orders['order_id'])}}" class="button">Review Shipment</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

@section('js')
    <script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#inprogress_data').DataTable({
                "bSort" : false,
                "bPaginate": false,
                "bFilter": true,
                "bInfo": false,
            });
        });
    </script>
@endsection