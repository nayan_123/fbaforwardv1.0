@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
@endsection
@section('content')
    <section class="content-header">
        <h1> {{$title}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="javascript:void(0)">Orders</a></li>
            <li><a href="javascript:void(0)">Prep</a></li>
            <li class="active">{{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12 trhead"> Order Details </div>
                    <div class="col-md-12">
                        <div class="col-md-2"> Order # : </div>
                        <div class="col-md-2"> {{isset($order_detail)? $order_detail->order_no :null}}</div>
                        <div class="col-md-2"> Company : </div>
                        <div class="col-md-2"> {{isset($order_detail)? $order_detail->company_name :null}}</div>
                        <div class="col-md-2"> Customer Email : </div>
                        <div class="col-md-2"> {{isset($order_detail)? $order_detail->contact_email :null}}</div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2"> Product : </div>
                        <div class="col-md-10">  {{isset($order_detail)? $order_detail->product_name :null}}</div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4"> Number of Units :  </div>
                        <div class="col-md-2"> {{isset($order_detail)? $order_detail->total :null}}</div>
                        <div class="col-md-4"> Number of Cartons : </div>
                        <div class="col-md-2"> {{isset($order_detail)? $order_detail->no_boxs :null}}</div>
                    </div>
                    <div class="col-md-12 trhead"> View Requirements</div>
                    <div class="col-md-12">
                        <label class="col-md-4"> Prep Requirements</label>
                        <label class="col-md-4"> Print FNSKU Label</label>
                        <label class="col-md-4"> Print Other Label</label>
                        <div class="col-md-4">
                            @if(!empty($prep_require))
                                @foreach($prep_require as $prep_requires)
                                    {{ $prep_requires->service_name }} @if($prep_requires->complete==0)<a href="javascript:void(0)" onclick="prepcomplete('{{$prep_requires->id}}')" id="prep_require_{{$prep_requires->id}}">Prep Complete</a> @else Completed @endif
                                    <span id="prep_complete_{{$prep_requires->id}}" name="prep_complete_{{$prep_requires->id}}" style="display: none;">Completed</span>
                                    <br>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-4">
                            @if($status=='0')
                                <a href="javascript:void(0)" onclick="getlabel('{{$order_detail->fnsku}}', '{{ $order_detail->order_id }}')">Print Label</a>
                            @else
                                @if($order_detail->label_complete=='0')
                                    Pending Label to print
                                @else
                                    Complete Label to print
                                @endif
                            @endif
                        </div>
                        <div class="col-md-4">
                            @if($status=='0')
                                <a href="javascript:void(0)" onclick="getotherlabel('{{ $order_detail->order_id }}')">Print Label</a>
                            @else
                                @if($order_detail->other_label_complete=='0')
                                    Pending Label to print
                                @else
                                    Complete Label to print
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                @if($status=='1' && $order_detail->label_complete=='1')
                    <div class="row">
                        <div class="col-md-12"><a href="{{ url('warehouse/submitchecklist/'.$order_detail->order_id) }}" class="button">Submit To Admin</a></div>
                    </div>
                @endif
            </div>
        </div>
        <div class="modal fade" id="barcode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
             style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">Barcode</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" id="barcode_div">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
<script>
    function prepcomplete(prep_require_id) {
        $('.preloader').css("display", "block");
        $.ajax({
            headers: {
                'X-CSRF-Token': "{{ csrf_token() }}"
            },
            method: 'POST', // Type of response and matches what we said in the route
            url: '/warehouse/prepcomplete', // This is the url we gave in the route
            data: {
                prep_require_id:prep_require_id,
            }, // a JSON object to send back
            success: function (response) { // What to do if we succeed
                $('.preloader').css("display", "none");
                $('#prep_require_'+prep_require_id).css('display','none');
                $('#prep_complete_'+prep_require_id).css('display','block');
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                $('.preloader').css("display", "none");
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }
    function getlabel(fnsku, order_id) {
        $('.preloader').css("display", "block");
        $.ajax({
            headers: {
                'X-CSRF-Token': "{{ csrf_token() }}"
            },
            method: 'POST', // Type of response and matches what we said in the route
            url: '/warehouse/getlabel', // This is the url we gave in the route
            data: {
                'fnsku': fnsku,
                'order_id':order_id
            }, // a JSON object to send back
            success: function (response) { // What to do if we succeed
                $('.preloader').css("display", "none");
                $('#barcode_div').html(response);
                $("#barcode").modal('show');
                var prtContent = document.getElementById("barcode_div");
                var WinPrint = window.open('', '', 'left=0,top=0,width=500,height=200,toolbar=0,scrollbars=0,status=0');
                WinPrint.document.write(prtContent.innerHTML);
                WinPrint.document.close();
                WinPrint.focus();
                WinPrint.print();
                WinPrint.close();
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                $('.preloader').css("display", "none");
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }
    function getotherlabel(order_id) {
        $('.preloader').css("display", "block");
        $.ajax({
            headers: {
                'X-CSRF-Token': "{{ csrf_token() }}"
            },
            method: 'POST', // Type of response and matches what we said in the route
            data: {
                'order_id':order_id
            },
            url: '/warehouse/getotherlabel', // This is the url we gave in the route
            success: function (response) { // What to do if we succeed
                $('.preloader').css("display", "none");
                $('#barcode_div').html(response);
                $("#barcode").modal('show');
                var prtContent = document.getElementById("barcode_div");
                var WinPrint = window.open('', '', 'left=0,top=0,width=500,height=200,toolbar=0,scrollbars=0,status=0');
                WinPrint.document.write(prtContent.innerHTML);
                WinPrint.document.close();
                WinPrint.focus();
                WinPrint.print();
                WinPrint.close();
            },
            error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                $('.preloader').css("display", "none");
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }
</script>
@endsection