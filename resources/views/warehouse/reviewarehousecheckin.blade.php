@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    <style>
        .line-width { width:12.49% }  .line-width1 {  width:11.11% } .padding-0-7 { padding: 0px 0px !important;  }  .label{  color: #7d7f80;  font-weight: normal;  }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <h1> {{$title}}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="javascript:void(0)">Orders</a></li>
            <li><a href="javascript:void(0)">Review Check In</a></li>
            <li class="active">{{$title}}</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="shipment-box padding-0-7">
                            <h3>Order Details</h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"><b>Order #</b></div>
                <div class="col-md-5">{{isset($order_detail)? $order_detail->order_no :null}}</div>
                <div class="col-md-2"><b>Expected Units</b></div>
                <div class="col-md-3">{{isset($order_detail)? $order_detail->total :null}}</div>
                <div class="col-md-2"><b>Company</b></div>
                <div class="col-md-5">{{isset($order_detail)? $order_detail->company_name :null}}</div>
                <div class="col-md-2"><b>Expected Cartons</b></div>
                <div class="col-md-3">{{isset($order_detail)? $order_detail->no_boxs :null}}</div>
                <div class="col-md-2"><b>Product</b></div>
                <div class="col-md-5">{{isset($order_detail)? $order_detail->product_name :null}}</div>
                <div class="col-md-2"><b>Expected Units/Carton</b></div>
                <div class="col-md-3">{{isset($order_detail)? ($order_detail->total/$order_detail->no_boxs) :null}}</div>
                <div class="col-md-12">
                    <div class="shipment-box padding-0-7">
                        <h3>Product Details</h3>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2">
                        Labels
                    </div>
                    <div class="col-md-4">
                        @if(!empty($order_detail))
                            @if($order_detail->label=='1')
                                FNSKU
                            @elseif($order_detail->label=='2')
                                UPC/SKU
                            @elseif($order_detail->label=='3')
                                No Labels
                            @endif
                        @endif
                    </div>
                    <div class="col-md-2">
                        Label Number(s)
                    </div>
                    <div class="col-md-4">
                        @if(isset($warehouse_labels))
                            @foreach($warehouse_labels as $warehouse_label)
                                {{ $warehouse_label->label_number }}<br>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-md-12">
                    @foreach($warehouse_lineitems as $key=>$warehouse_lineitem)
                        <div class="col-md-12"><b>Cartoon Details - Product {{$key+1}}</b></div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <div class="col-md-2 line-width1">
                                    <span>Length (in)</span><br>
                                    {{ $warehouse_lineitem->length  }}
                                </div>
                                <div class="col-md-2 line-width1">
                                    <span>Width (in)</span><br>
                                    {{ $warehouse_lineitem->width  }}
                                </div>
                                <div class="col-md-2 line-width1">
                                    <span>Height (in)</span><br>
                                    {{ $warehouse_lineitem->height  }}
                                </div>
                                <div class="col-md-2 line-width1">
                                    <span>Weight (lb)</span><br>
                                    {{ $warehouse_lineitem->weight  }}
                                </div>
                                <div class="col-md-2 line-width">
                                    <span>Units Per Carton</span><br>
                                    {{ $warehouse_lineitem->unit_per_carton }}
                                </div>
                                <div class="col-md-2 line-width">
                                    <span>Units Subtotal </span><br>
                                    {{ $warehouse_lineitem->unit_subtotal }}
                                </div>
                                <div class="col-md-2 line-width">
                                    <span>Cartons Subtotal</span><br>
                                    {{ $warehouse_lineitem->cartons_subtotal }}
                                </div>
                                <div class="col-md-2 line-width">
                                    <span>Volume (cbm)</span><br>
                                    {{ $warehouse_lineitem->volume_subtotal }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                        <div class="col-md-12">
                            <div class="form-group row">
                                <div class="col-md-2" style="width:56.93%">&nbsp;</div>
                                <div class="col-md-2 line-width">
                                    <span>Unit Total</span>
                                    <input type="text" name="unit_total" id="unit_total" value="{{$order_detail->total_units }}" class="form-control" readonly>
                                </div>
                                <div class="col-md-2 line-width">
                                    <span>Carton Total</span>
                                    <input type="text" name="carton_total" id="carton_total" value="{{$order_detail->total_cartons }}" class="form-control" readonly>
                                </div>
                                <div class="col-md-2 line-width" style="width:14.49%">
                                    <span>Volume (CBM) Total </span>
                                    <input type="text" name="volume_total" id="volume_total" class="form-control" value="{{ $order_detail->total_volume }}" readonly>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-md-12">
                    <div class="shipment-box padding-0-7">
                        <h3>Shipment Details</h3>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><label> Delivered By </label></div>
                    <div class="col-md-4">{{  isset($order_detail)? $order_detail->deliver_by :null}}</div>
                    <div class="col-md-2"><label> Air Waybill #  </label></div>
                    <div class="col-md-4">{{isset($order_detail)? $order_detail->air_way :null}}</div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2"><label> Shipment Condition </label></div>
                    <div class="col-md-4">
                        @if($order_detail->shipment_condition=='1')
                            Good
                        @elseif($order_detail->shipment_condition=='2')
                            Fair
                        @elseif($order_detail->shipment_condition=='3')
                            Poor
                        @endif
                    </div>
                    <div class="col-md-2"><label> Condition Notes   </label> </div>
                    <div class="col-md-4">{{ $order_detail->condition_notes  }}</div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2">Photos</div>
                    <div class="col-md-10">
                        @foreach($warehouse_images as $warehouse_image)
                            <div class="col-md-4"> <p><a target="_blank" href="{{ url('uploads/telexrelease',$warehouse_image->images) }}">{{ $warehouse_image->images }}</a></p></div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6"><label>Does the customer need to be notified of issues with the products? </label> <br> @if($order_detail->notified=='0') No @else Yes @endif</div>
                    <div class="col-md-6">{{ $order_detail->notified_note  }}</div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <a class="col-md-4" onclick="requestrevision({{$order_id}})">Request Revision</a>
                        <button class="button col-md-3" onclick="approveorder({{$order_id}})">Approve</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myLabel"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" id="approvediv" hidden>
                                {!! Form::open(['url' => 'warehouse/approvereview', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                                {{ csrf_field() }}
                                {!! Form::hidden('order',old('order',$order_id),['id'=>'order']) !!}
                                {!! Form::hidden('customer_requirement_id',old('customer_requirement_id',$order_detail->customer_requirement_id),['id'=>'customer_requirement_id']) !!}
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Warehouse Status</label>
                                    <div class="col-md-9">
                                        <select name="status" id="status" class="form-control select2" onchange="showhide(this.value)">
                                            <option value=""></option>
                                            <option value="0">On Track</option>
                                            @foreach($warehouse_hold as $warehouse_holds)
                                                <option value="{{$warehouse_holds->id}}">{{ $warehouse_holds->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="status1" hidden>
                                    <label class="col-md-3 control-label">Warehouse Due Date</label>
                                    <div class="col-md-9">
                                        <input type="text" name="due_date" id="due_date" class="form-control datepicker">
                                    </div>
                                </div>
                                <div id="status2" hidden>
                                    <div id="statuses2">
                                        <div class="col-md-12" id="status2_1">
                                            <div class="col-md-6"><label>Hold1</label></div><div class="col-md-6" style="text-align: right;">&nbsp;</div><br><hr>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <span class="control-label col-md-6">Type</span>
                                                        <div class="col-md-6">
                                                            <select name="type2_1" id="type2_1" class="form-control select2" onchange="getdetails('2','1',this.value)">
                                                                <option vlaue="">Type</option>
                                                                @foreach($warehouse_hold_type as $warehouse_hold_types)
                                                                    <option value="{{$warehouse_hold_types->id}}">{{$warehouse_hold_types->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <span class="control-label col-md-6">Cost</span>
                                                        <div class="col-md-6" id="status2_cost_div1">
                                                            <input type="text" name="cost1" id="cost1" class="form-control" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <span class="control-label col-md-6">Details</span>
                                                        <div class="col-md-6" id="status2_details_div1">
                                                            <input type="text" name="details1" id="details1" value="" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <span class="control-label col-md-6">Measurement</span>
                                                        <div class="col-md-6" id="status2_measurement_div1">
                                                            <select name="measurement1" id="measurement1" class="form-control">
                                                                <option value="">Measurement</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6">&nbsp;</div>
                                                    <div class="col-md-6">
                                                        <span class="control-label col-md-6">Number of Unit/Cartons</span>
                                                        <div class="col-md-6" id="status2_number_div1">
                                                            <input type="text" name="number_unit1" id="number_unit1" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <span class="control-label col-md-3">Notes </span>
                                                        <div class="col-md-9">
                                                            <textarea name="notes2_1" id="notes2_1" cols="10" rows="4" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="text-align: right">
                                        <input type="hidden" name="count2" id="count2" value="1">
                                        <label class="" onclick="addhold('2')">+ Add Hold</label>
                                    </div>
                                </div>
                                <div id="status3" hidden>
                                    <div id="statuses3">
                                        <div class="col-md-12" id="status3_1">
                                            <div class="col-md-6"><label>Hold1</label></div><div class="col-md-6" style="text-align: right;">&nbsp;</div><br><hr>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <span class="control-label col-md-3">Type</span>
                                                        <div class="col-md-9">
                                                            <input type="text" name="type3_1" id="type3_1" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <span class="control-label col-md-3">Details</span>
                                                        <div class="col-md-9">
                                                            <input type="text" name="details3_1" id="details3_1" class="form-control" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <span class="control-label col-md-3">Notes</span>
                                                        <div class="col-md-9">
                                                            <textarea name="notes3_1" id="notes3_1" cols="10" rows="4" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="text-align: right">
                                        <input type="hidden" name="count3" id="count3" value="1">
                                        <label class="" onclick="addhold('3')">+ Add Hold</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('    Submit    ', ['class'=>'button btn-submit-quote', 'id'=>'approvereview'  ]) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-12" id="revisiondiv" hidden>
                                {!! Form::open(['url' => 'warehouse/checkinrevision', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                                {!! Form::hidden('order_id',old('order_id'),['id'=>'order_id']) !!}
                                <div class="form-group">
                                    <div class="col-md-11">
                                        {!! Form::textarea('revision_note', old('revision_note'),['class'=>'form-control','rows'=>'4','cols'=>'10']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-9 col-md-3">
                                        {!! Form::submit('    Submit    ', ['class'=>'button btn-submit-quote' ]) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
    $(document).ready(function(){
        $('.modal-body select').css('width', '100%');
        $(".select2").select2({
            placeholder: "Please Select",
            allowClear: true
        });
        $('.datepicker').datepicker({
            startDate: new Date(),
        });
    });
    function approveorder(order_id) {
        $("#revisiondiv").hide();
        $("#approvediv").show();
        $("#modal").modal('show');
        $("#myLabel").empty();
        $("#myLabel").append("Approve");
    }
    function requestrevision(order_id) {
        $("#order_id").val(order_id);
        $("#approvediv").hide();
        $("#revisiondiv").show();
        $("#modal").modal('show');
        $("#myLabel").empty();
        $("#myLabel").append("Request Revision");
    }
    function showhide(value) {
        if(value==0) {
            $("#status1").show();
            $("#status2").hide();
            $("#status3").hide();
        }
        else if(value==1) {
            $("#status1").hide();
            $("#status2").show();
            $("#status3").hide();

        }
        else if(value==2) {
            $("#status1").hide();
            $("#status2").hide();
            $("#status3").show();
        }
        else
        {
            $("#status1").hide();
            $("#status2").hide();
            $("#status3").hide();
        }
    }
    function addhold(hold) {
        var count = $("#count"+hold).val();
        var new_count = parseInt(count)+1;
        if(hold=='3') {
            $("#statuses" + hold).append('<div class="col-md-12" id="status' + hold + '_' + new_count + '"><div class="col-md-6"><label>Hold' + new_count + '</label></div><div class="col-md-6" style="text-align: right; color: red" onclick="deletehold(' + hold + ',' + new_count + ')">Delete Hold</div><br><hr><div class="col-md-12"><div class="form-group"><div class="col-md-12"><span class="control-label col-md-3">Type</span><div class="col-md-9"><input type="text" name="type'+hold+'_' + new_count + '" id="type'+hold+'_' + new_count + '" class="form-control"> </div></div><div class="col-md-12"><span class="control-label col-md-3">Details</span><div class="col-md-9"><input type="text" name="details' + new_count + '" id="details' + new_count + '" class="form-control" value=""></div></div></div><div class="form-group"><div class="col-md-12"><span class="control-label col-md-3">Notes</span><div class="col-md-9"><textarea name="notes'+hold+'_' + new_count + '" id="notes'+hold+'_' + new_count + '" cols="10" rows="4" class="form-control"></textarea></div></div></div></div></div>');
        }
        else if(hold=='2'){
            $("#statuses"+hold).append('<div class="col-md-12" id="status'+hold+'_'+new_count+'"><div class="col-md-6"><label>Hold'+new_count+'</label></div><div class="col-md-6" style="text-align: right; color: red" onclick="deletehold(' + hold + ',' + new_count + ')">Delete Hold</div><br><hr><div class="col-md-12"><div class="form-group"><div class="col-md-6"><span class="control-label col-md-6">Type</span><div class="col-md-6"><select name="type'+hold+'_'+new_count+'" id="type'+hold+'_'+new_count+'" class="form-control" onchange="getdetails('+hold+','+new_count+','+this.value+')"><option value="">Type</option>@foreach($warehouse_hold_type as $warehouse_hold_types)<option value="{{$warehouse_hold_types->id}}">{{$warehouse_hold_types->name}}</option>@endforeach</select></div></div><div class="col-md-6"><span class="control-label col-md-6">Cost</span><div class="col-md-6" id="status'+hold+'_cost_div'+new_count+'"><input type="text" name="cost'+new_count+'" id="cost'+new_count+'" class="form-control" value=""></div></div></div><div class="form-group"><div class="col-md-6"><span class="control-label col-md-6">Details</span><div class="col-md-6" id="status'+hold+'_details_div'+new_count+'"><input type="text" name="details'+new_count+'" id="details'+new_count+'" value="" class="form-control"></div></div><div class="col-md-6"><span class="control-label col-md-6">Measurement</span><div class="col-md-6" id="status'+hold+'_measurement_div'+new_count+'"><select name="measurement'+new_count+'" id="measurement'+new_count+'" class="form-control"><option value="">Measurement</option></select></div></div></div><div class="form-group"><div class="col-md-6">&nbsp;</div><div class="col-md-6"><span class="control-label col-md-6">Number of Unit/Cartons</span><div class="col-md-6"><input type="text" name="number_unit'+new_count+'" id="number_unit'+new_count+'" class="form-control"></div></div></div><div class="form-group"><div class="col-md-12"><span class="control-label col-md-3">Notes </span><div class="col-md-9"><textarea name="notes'+hold+'_'+new_count+'" id="notes'+hold+'_'+new_count+'" cols="10" rows="4" class="form-control"></textarea></div></div></div></div></div>');
        }
        $("#count" + hold).val(new_count);
    }
    function deletehold(hold,count){
        $("#status"+hold+"_"+count).remove();
    }
    function getdetails(status,type_no,type_id){
        type_id = $("#type"+status+"_"+type_no).val();
        $.ajax({
            headers: {
                'X-CSRF-Token': "{{ csrf_token() }}"
            },
            method: 'POST',
            url: '{{ url("warehouse/getdetails") }}',
            data : {
                type_id:type_id,
            },
            success: function (data) {
                if(type_id==1) {
                    var html = '';
                    html += '<select name="details' + type_no + '" id="details' + type_no + '" class="form-control select2" onchange="getcostmesure('+status+','+type_no+','+type_id+',this.value)">';
                    $.each(data, function (key, value) {
                        html += '<option value="' + value.prep_service_id + '">' + value.service_name + '</option>';
                    });
                    html += '</select>';
                }
                else if(type_id==2){
                    var html = '';
                    html += '<select name="details' + type_no + '" id="details' + type_no + '" class="form-control select2" onchange="getcostmesure('+status+','+type_no+','+type_id+',this.value)">';
                    $.each(data, function (key, value) {
                        html += '<option value="' + value.id + '">' + value.name + '</option>';
                    });
                    html += '</select>';
                }
                else if(type_id==3){
                    var html = '';
                    html += '<select name="details' + type_no + '" id="details' + type_no + '" class="form-control select2" onchange="getcostmesure('+status+','+type_no+','+type_id+',this.value)>';
                    $.each(data, function (key, value) {
                        html += '<option value="' + value.id + '">' + value.name + '</option>';
                    });
                    html += '</select>';
                }
                else {
                    var html='';
                    html += '<input type="text" name="details' + type_no + '" id="details' + type_no + '" class="form-control">';
                }
                $("#status"+status+"_details_div"+type_no).empty();
                $("#status"+status+"_details_div"+type_no).append(html);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }
    function getcostmesure(status,type_no,type_id,detail_id){

        $.ajax({
            headers: {
                'X-CSRF-Token': "{{ csrf_token() }}"
            },
            method: 'POST',
            url: '{{ url("warehouse/getcostmesure") }}',
            data : {
                type_id:type_id,
                detail_id:detail_id,
            },
            success: function (data) {
                    var html = '';
                    var price='';
                    var id='';
                    var name='';
                    if(data.price != undefined)
                        price = data.price;
                    if(data.id != undefined)
                        id=data.id;
                    if(data.name != undefined)
                        name=data.name;
                    html += '<input type="text" name="cost' + type_no + '" id="cost' + type_no + '" class="form-control" value="'+price+'">';
                    $("#status"+status+"_cost_div"+type_no).empty();
                    $("#status"+status+"_cost_div"+type_no).append(html);
                var html1 = '';
                html1 += '<select name="measurement' + type_no + '" id="measurement' + type_no + '" class="form-control select2">';
                html1 += '<option value="'+id+'">'+name+'</option>';
                html1 += '</select>';
                $("#status"+status+"_measurement_div"+type_no).empty();
                $("#status"+status+"_measurement_div"+type_no).append(html1);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
    }

    </script>
@endsection