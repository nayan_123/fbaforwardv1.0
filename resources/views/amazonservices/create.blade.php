@extends('layouts.member.app')
@section('title', "Order Form")
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
<style type="text/css">
.col-md-12 {  padding-left: 2%;  }
.darkheading {  background-color: #003c6b; color:#ffffff; padding-left:3%; margin-bottom: 1%; width: 99.5%;  }
.span{  font-weight: normal;  color: #999;  }
.check-radio{  -webkit-appearance: checkbox; /* Chrome, Safari, Opera */  -moz-appearance: checkbox;    /* Firefox */  -ms-appearance: checkbox;     /* not currently supported */  }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-anchor"></i> Order Form</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('amazonservices') }}"><i class="fa fa-camera"></i> Amazon Services</a></li>
            <li class="active"><a href="{{ url('amazonservices/create') }}"><i class="fa fa-anchor"></i> Order Form</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Order Form</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['url' => 'amazonservices', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <span class="col-md-12" style="padding-left: 0%;font-size: 15px;">I'd like to order</span>
                                    </div>
                                    <div class="col-md-12" style="padding-left: 2%;font-size: 15px;">
                                        <input type="checkbox" name="like[]" id="like_list" value="list" class="validate[minCheckbox[1]]checkbox"> Listing Services<br>
                                        <input type="checkbox" name="like[]" id="like_photo" value="photo" class="validate[minCheckbox[1]]checkbox"> Product Photography Services<br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div id="listing_div" hidden>
                            <div class="darkheading">
                                <span class="heading">LISTING DEVELOPMENT</span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="margin-bottom: 3%">
                                    <div class="col-md-12">
                                        <label for="frontend_qty" class="control-label col-md-7" style="text-align:left">FRONT END LISTING COPY WRITING<br><span class="span">Description</span></label>
                                        <div class="col-md-5">
                                            <input type="text" name="frontend_qty" id="frontend_qty" class="form-control validate[required] custom[integer]" placeholder="QTY" style="width:18%; height: 2%; border:1px solid #ccc;">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="backend_qty" class="control-label col-md-7" style="text-align:left">BACK END KEYWORD AND COMPETITOR RESEARCH<br><span class="span">Description</span></label>
                                        <div class="col-md-5">
                                            <input type="text" name="backend_qty" id="backend_qty" class="form-control validate[required] custom[integer]" placeholder="QTY" style="width:18%; height: 2%; border:1px solid #ccc;">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="product_details" class="control-label col-md-12" style="text-align:left">PRODUCT DETAILS</label>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea name="product_details" id="product_details" class="form-control validate[required]" placeholder="Any specifics you would like to ensure are in the description/bullet points. Conversion rates tends to be better when there are high-quality details in the product description. (i.e. product dimensions, unique features, materials the product is made of, etc.)"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12" style="text-align:left">PHOTOS OR LINKS TO SIMILAR PRODUCTS</label>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-6" style="text-align:left">PHOTO(S)</label>
                                        <label for="" class="control-label col-md-6" style="text-align:left">LINK(S)</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="file" name="listing_photos[]" id="listing_photos" class="validate[required]  validate[checkFileType[jpg|jpeg|gif|JPG|png|PNG|docx|doc|pdf|pdfx]]" multiple >
                                    </div>
                                    <div class="col-md-6">
                                        <textarea name="listing_links" id="listing_links" class="validate[required]"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12" style="text-align:left">COMPETITORS (OPTIONAL)</label>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12" style="text-align:left">Link(s) to Competitors</label>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea name="link_competitor" id="link_competitor" class="form-control" placeholder="We will research your competitors for you, but please feel welcome to provide links to any competitors you would like us to specifically research."></textarea>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div id="photo_div" hidden>
                            <div class="col-md-12 darkheading">
                                <span class="heading">PRODUCT PHOTOGRAPHY</span>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="margin-bottom:3%">
                                    <div class="col-md-12">
                                        <label for="photo_product" class="control-label col-md-12" style="text-align:left">PRODUCT(S) TO BE PHOTOGRAPHED </label>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="text" id="photo_product" name="photo_product" class="form-control validate[required]">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="standard_photo" class="control-label col-md-7" style="text-align:left">STANDARD PHOTOS - $40/PHOTO<br><span class="span">Any product, alone on a white background</span></label>
                                        <div class="col-md-5">
                                            <input type="text" name="standard_photo" id="standard_photo" class="form-control validate[required] custom[integer]" placeholder="QTY" style="width:18%; height: 2%; border:1px solid #ccc;">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="standard_file" class="control-label col-md-12" style="text-align:left"><span class="span">FILE FORMAT</span></label>
                                        <div class="col-md-2">
                                            <input type="radio" name="standard_file" id="standard_file_jpg" class="check-radio validate[required]radio" value="1"><span class="span"> JPG(default)</span>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="radio" name="standard_file" id="standard_file_psd" class="check-radio validate[required]radio" value="2"><span class="span"> PSD - Edited Photoshop file (+$10 each)</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="prop_photo" class="control-label col-md-7" style="text-align:left">PROP OR ACTION PHOTOS - $60/PHOTO<br><span class="span">Any product, with a prop or in action on a white background. Hand models can be provided.</span></label>
                                        <div class="col-md-5">
                                            <input type="text" name="prop_photo" id="prop_photo" class="form-control validate[required] custom[integer] " placeholder="QTY" style="width:18%; height: 2%; border:1px solid #ccc;">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="prop_file" class="control-label col-md-12" style="text-align:left"><span class="span">FILE FORMAT</span></label>
                                        <div class="col-md-2">
                                            <input type="radio" name="prop_file" id="prop_file_jpg" class="check-radio validate[minCheckbox[1]]" value="1"><span class="span"> JPG(default)</span>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="radio" name="prop_file" id="prop_file_psd" class="check-radio validate[minCheckbox[1]]" value="2"><span class="span"> PSD - Edited Photoshop file (+$10 each)</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="style_photo" class="control-label col-md-7" style="text-align:left">LIFESTYLE PHOTOS - STARTING AT $100/PHOTO<br><span class="span">Lifestyle photos involve using your product in a real-life setting. Full model photos may require the cost of hiring a model. Please enter more details in the "Description of Photos" field below and a representative will reach out to you with an estimate.</span></label>
                                        <div class="col-md-5">
                                            <input type="text" name="style_photo" id="style_photo" class="form-control validate[required] custom[integer]" placeholder="QTY" style="width:18%; height: 2%; border:1px solid #ccc;">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="style_file" class="control-label col-md-12" style="text-align:left"><span class="span">FILE FORMAT</span></label>
                                        <div class="col-md-2">
                                            <input type="radio" name="style_file" id="style_file_jpg" class="check-radio validate[minCheckbox[1]]" value="1"><span class="span"> JPG(default)</span>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="radio" name="style_file" id="style_file_psd" class="check-radio validate[minCheckbox[1]]" value="2"><span class="span"> PSD - Edited Photoshop file (+$10 each)</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="photo_desc" class="control-label col-md-12" style="text-align:left">DESCRIPTION OF PHOTO(S)<br><span class="span">Include detailed instructions of specific setups, groupings or angles, and any other instructions you might have. The more details we have, the quicker the photography process will be. If any photos are requested that are outside our normal scope of work, you will be notified and additional fees may apply.</span></label>
                                    </div>
                                    <div class="col-md-12">
                                        <textarea name="photo_desc" id="photo_desc" class="form-control validate[required]"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-12" style="text-align:left">PHOTOS OR LINKS TO SIMILAR PRODUCTS</label>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="" class="control-label col-md-6" style="text-align:left">PHOTO(S)</label>
                                        <label for="" class="control-label col-md-6" style="text-align:left">LINK(S)</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="file" name="photography_photos[]" id="photography_photos" class="validate[required]  validate[checkFileType[jpg|jpeg|gif|JPG|png|PNG|docx|doc|pdf|pdfx]]" multiple >
                                    </div>
                                    <div class="col-md-6">
                                        <textarea name="photography_links" id="photography_links" class="validate[required]"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <label for="after_photo" class="control-label col-md-12" style="text-align:left">AFTER THE PHOTOS ARE TAKEN, WHAT WOULD YOU LIKE US TO DO WITH THE PRODUCT(S)?<br><span class="span">If a label is not provided and no other instructions have been given within 30 days of order completion, we will donate or dispose of the sample product. We will not add your product back into the shipment, as this can delay the warehouse's processes.</span></label>
                                    </div>
                                    <div class="col-md-12">
                                        <select name="after_photo" id="after_photo" class=" validate[required] select2 col-md-12">
                                            <option value=""></option>
                                            <option value="1">I will provide a prepaid shipping label (UPS, FedEx, USPS)</option>
                                            <option value="2">Donate or dispose of product</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-md-12" style="text-align: right">
                                {!! Form::submit('  Submit Order  ', ['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach', {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
        });

        $(document).ready(function () {
            $(".select2").select2({
                placeholder: "Please Select",
                allowClear: true
            });
        });

        $(document).ready(function () {
            $('.select2-container').css('width','100%');
            $('.select2-search__field').css('margin','0px');
        });
        
        $("#like_list").change(function() {
            if(this.checked) {
                $("#listing_div").show();
            }
            else {
                $("#listing_div").hide();
            }
        });
        $("#like_photo").change(function() {
            if(this.checked) {
                $("#photo_div").show();
            }
            else {
                $("#photo_div").hide();
            }
        });
    </script>
@endsection