@extends('layouts.member.app')
@section('title', "Photo(s) Ready")
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
<style type="text/css">
.col-md-12 {  padding-left: 2%;  }
.darkheading {  background-color: #003c6b; color:#ffffff; padding-left:3%; margin-bottom: 1%; width: 99.5%;  }
input[type="radio"] {  -webkit-appearance: checkbox; /* Chrome, Safari, Opera */  -moz-appearance: checkbox;    /* Firefox */  -ms-appearance: checkbox;     /* not currently supported */  }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1>Photo(s) Ready</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('amazonservices') }}"><i class="fa fa-camera"></i> Amazon Services</a></li>
            <li class="active"><a href="javascript:void(0)"> Photo(s) Ready</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Photo(s) Ready</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i
                                class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="darkheading">
                            <span class="heading">PRODUCT PHOTOGRAPHY: DOWNLOAD FILES</span>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group col-md-12" style="text-align:right">
                                <a class="button1" href="{{ url('photographyphoto/'.$id.'/edit') }}">Download</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-group col-md-12" style="text-align: center">
                                @foreach($photo_document as $photo_documents)
                                    <!-- <img src="{{  public_path("uploads/photography_photo/".$photo_documents->photo) }}" width="20%"> -->
                                    <img src="{{ url('uploads/photography_photo',$photo_documents->photo) }}">
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['url' => 'photographyphoto', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id'=>'validate']) !!}
                        <div class="col-md-12">
                            <label for="revision" class="control-label col-md-6" style="text-align:right">Request Revision?</label>
                            <div class="col-md-1">
                                <input type="radio" id="approve_no" name="approve" value="0"> No
                            </div>
                            <div class="col-md-5">
                                <input type="radio" id="approve_yes" name="approve" value="1"> Yes
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-5">&nbsp;</div>
                            <div class="col-md-4" style="font-weight: normal;color: #999;">Revision requests are available for up to 15 days after your documents have been submitted to you</div>
                            <div class="col-md-3">&nbsp;</div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-5">&nbsp;</div>
                            <div class="col-md-4"><textarea name="detail" id="detail" hidden></textarea><input type="hidden" name="id" id="id" value="{{$id}}"></div>
                            <div class="col-md-3">&nbsp;</div>
                        </div>
                        <div>
                            <div class="col-md-5">&nbsp;</div>
                            <div class="col-md-4"><input class="btn btn-primary" type="submit" name="submit" id="submit" value="Submit"></div>
                            <div class="col-md-3">&nbsp;</div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        $("#approve_yes").change(function() {
            if ($(this).prop('checked') == true) {
                $("#detail").show();
            }
            else {
                $("#detail").hide();
                $("#detail").val('');
            }
        });
        $("#approve_no").change(function() {
            if ($(this).prop('checked') == true) {
                $("#detail").hide();
                $("#detail").val('');
            }
        });
    </script>
@endsection