@extends('layouts.member.app')
@section('title', $title)
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/custom_quotes.css') !!}
@endsection
@section('content')
<style>
.content{ min-height: 620px;  }
.dropbtn { font-size: 16px;  border: none;  cursor: pointer;  }
.dropdown { position: relative;  display: inline-block;  }
.dropdown-content { display: none; position: absolute;  background-color: #f9f9f9;  min-width: 160px;  overflow: auto;  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); z-index: 1;  margin-left: -90px;  }
.dropdown-content a { color: black; padding: 12px 16px;  text-decoration: none;  display: block;  }
.dropdown a:hover {background-color: #f1f1f1}
.show {display:block;}
.background{ background: #ecf0f5 !important; }
</style>
<section class="content">
    <div class="row">
        <section class="content-header">
            <h1><i class="fa fa-file-text"></i> {{$title}}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><i class="fa fa-file-text"></i>{{$title}}</li>
            </ol>
        </section>
        <section class="content">
            <div class="col-md-12 no-padding">
                <div class="box2 col-md-12 no-padding">
                    <div class="col-md-12 no-padding">
                        <div class="box-body no-padding">
                            <div class="">
                                <div class="col-md-12 no-padding">
                                    <div class="col-md-2 blue-bg product-data">
                                        <b>Order</b> #{{ $listing_order['amazon_order'] }}
                                    </div>
                                    <div class="col-md-4 product-data text-right">
                                        Place on {{ date('m/d/Y',strtotime($listing_order['created_at'])) }}
                                    </div>
                                    <div class="col-md-2 product-data text-right">
                                        <b>List Status</b>
                                    </div>
                                    <div class="col-md-3 product-data text-right">
                                        {{ $listing_order_status[$listing_order['is_activated']] }}
                                    </div>
                                    <div class="col-md-1 product-data text-center">
                                        <div class="dropdown">
                                            <i onclick="myFunction()" class="dropbtn fa fa-cog"></i>
                                            <div id="myDropdown" class="dropdown-content">
                                                <a href="javascript:void(0)" onclick="acceptorder({{$listing_order['id']}})">Accept Order</a>
                                                <a href="javascript:void(0)">Add additional services</a>
                                                <a href="javascript:void(0)">Submit Listing</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 blank-class"></div>
                                <div class="col-md-12 no-padding">
                                    <div class="col-md-4 product-data">
                                        <b>Product:</b> {{ $listing_order['product_detail'] }}
                                    </div>
                                    <div class="col-md-4 product-data">
                                        <b>Company : </b> {{ $listing_order['company_name'] }}
                                    </div>
                                    <div class="col-md-4 product-data">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 blank-class"></div>
                    <div class="col-md-12 no-padding background">
                        <div class="box2 col-md-9 invoice-summery-box no-padding">
                            <div class="box-header with-border invoice-summery-heading">
                                <h3 class="box-title">Order Summery</h3>
                            </div>
                            <div class="col-md-12">
                                <table class="table quote-summery-table">
                                    <thead>
                                    <tr>
                                        <th>PHOTOS</th>
                                        <th>QTY</th>
                                        <th>RATE</th>
                                        <th>TOTAL</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--*/$sub_photo_total=0/*--}}
                                    @if(!empty($photo_order))
                                        @foreach($photo_service as $photo_services)
                                            <tr>
                                                <td>{{$photo_services->service_name}}</td>
                                                <td>@if($photo_services->id=='1'){{$photo_order['standard_photo_qty']}} @elseif($photo_services->id=='2'){{ $photo_order['prop_photo_qty'] }} @elseif($photo_services->id=='3'){{ $photo_order['lifestyle_photo_qty'] }} @endif</td>
                                                <td>${{ $photo_services->price }}</td>
                                                <td>
                                                    @if($photo_services->id=='1')
                                                        ${{ $photo_order['standard_photo_qty']*$photo_services->price }}
                                                        {{--*/ $sub_photo_total+=$photo_order['standard_photo_qty']*$photo_services->price /*--}}
                                                    @elseif($photo_services->id=='2')
                                                        ${{ $photo_order['prop_photo_qty']*$photo_services->price }}
                                                        {{--*/ $sub_photo_total+=$photo_order['prop_photo_qty']*$photo_services->price /*--}}
                                                    @elseif($photo_services->id=='3')
                                                        ${{ $photo_order['lifestyle_photo_qty']*$photo_services->price }}
                                                        {{--*/ $sub_photo_total+=$photo_order['lifestyle_photo_qty']*$photo_services->price /*--}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        @if($photo_order['standard_file_format']=='2' || $photo_order['prop_file_format']=='2' || $photo_order['lifestyle_file_format']=='2')
                                            <tr>
                                                <td>.PSD File Format</td>
                                                <td>
                                                    {{--*/$qty=0/*--}}
                                                    @if($photo_order['standard_file_format']=='2')
                                                        {{--*/$qty+=$photo_order['standard_photo_qty']/*--}}
                                                    @endif
                                                    @if($photo_order['prop_file_format']=='2')
                                                        {{--*/$qty+=$photo_order['prop_photo_qty']/*--}}
                                                    @endif
                                                    @if($photo_order['lifestyle_file_format']=='2')
                                                        {{--*/$qty+=$photo_order['lifestyle_photo_qty']/*--}}
                                                    @endif
                                                    {{ $qty }}
                                                    {{--*/ $sub_photo_total+=$qty*10 /*--}}
                                                </td>
                                                <td>$10</td>
                                                <td>${{ $qty*10 }}</td>
                                            </tr>
                                        @endif
                                    @endif
                                    <tr>
                                        <td colspan="3"><b>Photo Subtotal</b></td>
                                        <td><b>${{$sub_photo_total}}</b></td>
                                    </tr>
                                    {{--*/$sub_list_total=0/*--}}
                                        @foreach($listing_service as $list_services)
                                            <tr>
                                                <td>{{$list_services->service_name}}</td>
                                                <td>@if($list_services->listing_service_id=='1'){{$listing_order['frontend_qty']}} @elseif($list_services->listing_service_id=='2'){{ $listing_order['backend_qty'] }}@endif</td>
                                                <td>${{ $list_services->price }}</td>
                                                <td>@if($list_services->listing_service_id=='1')
                                                        ${{ $listing_order['frontend_qty']*$list_services->price }}
                                                        {{--*/$sub_list_total+=$listing_order['frontend_qty']*$list_services->price/*--}}
                                                    @elseif($list_services->listing_service_id=='2')
                                                        ${{ $listing_order['backend_qty']*$list_services->price }}
                                                        {{--*/$sub_list_total+=$listing_order['backend_qty']*$list_services->price/*--}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="3"><b>Listing Subtotal</b></td>
                                            <td><b>${{$sub_list_total}}</b></td>
                                        </tr>
                                    {{--*/$total=0/*--}}
                                    {{--*/$total=$sub_photo_total+$sub_list_total/*--}}
                                    <tr>
                                        <td colspan="3"><b>Total Deposit Paid</b></td>
                                        <td><b>${{ $total }}</b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box2 order-payment-history-box col-md-3">
                            <div class="box-header">
                                <h3 class="box-title">LISTING NOTES</h3>
                            </div>
                            <div class="box-body">
                                Product Details<br>
                                {{ $listing_order['product_detail'] }}
                                <br>
                                Photos or Links to Similar Products
                                <br>
                                @if(!empty($listing_order_photos))
                                    @foreach($listing_order_photos as $listing)
                                        {{ $listing->photo }} view<br>
                                    @endforeach
                                @endif
                                {{ $listing_order['links'] }}<br>
                                Links to Competitor<br>
                                {{ $listing_order['links_to_competitors'] }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="box2 col-md-12 invoice-summery-box no-padding">
                            <div class="box-header with-border invoice-summery-heading">
                                <h3 class="box-title">Order History</h3>
                            </div>
                            <div class="col-md-12">
                                <table class="table quote-summery-table">
                                    <thead>
                                    <tr>
                                        <th>DATE</th>
                                        <th>UPDATE</th>
                                        <th>DETAIL</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="acceptmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Accept Order</h4>
                        </div>
                        <div class="modal-body" style="padding:80px;">
                            <div class="row">
                                <div class="col-md-12" id="main">
                                    {!! Form::open(['url' =>  'listingoptimization', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal', 'id'=>'acceptorder']) !!}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::hidden('require_id',old('require_id'), ['id'=>'require_id']) !!}
                                                    <label class="control-label col-md-5">Lead Time</label>
                                                    <div class="input-group col-md-7">
                                                        <input type="text" name="lead_time" id="lead_time" class="form-control datepicker">
                                                    </div>
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        {!! Form::submit('  Submit  ', ['class'=>'btn btn-primary',  'id'=>'Accept Order']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
@endsection
@section('js')
    <script type="text/javascript">
        $('.datepicker').datepicker({
            startDate: new Date(),
        });
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }
    window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
    function acceptorder(id)
    {
        $("#require_id").val(id);
        $("#acceptmodal").modal('toggle');
    }
    </script>
@endsection