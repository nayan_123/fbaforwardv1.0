@extends('layouts.member.app')
@section('title', 'Amazon Services')
@section('css')
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/style.css') !!}
<style>
.sdetails {  background-color: #D8D8D8;  }
.details {  background-color: #F2F2F2;  }
.padding-0-20{ padding: 0px 20px; }
@media (max-width: 1135px){  .search-label{ display:none; }  }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-camera"></i> Amazon Services</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-camera"></i> Amazon Services</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="col-md-6 no-padding">
                <div class="col-md-12">
                    <h3>Listing Services</h3>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-2">
                    <label class="search-label">Search:</label>
                </div>
                <div class="col-md-7">
                    <input type="text" placeholder="Search" class="form-control input-sm" id="search_all">
                </div>
                <div class=" col-md-2 margin-top-10 no-padding">
                    <a href="{{ url('amazonservices/create') }}" class="btn btn-success"> Place New Order</a>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="listing_service" class="table" >
                            <thead>
                            <tr>
                                <th class="col-md-2">Order</th>
                                <th class="col-md-2">Product</th>
                                <th class="col-md-2">Date Ordered</th>
                                <th class="col-md-2">Status</th>
                                <th class="col-md-2">View Details</th>
                                <th class="col-md-2">Download Document</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list_order as $list_orders)
                                <tr id="{{$list_orders->order_id}}">
                                    <td>{{ $list_orders->order_id }}</td>
                                    <td>{{ $list_orders->product_detail }}</td>
                                    <td>{{ date('m/d/Y', strtotime($list_orders->created_at)) }}</td>
                                    <td>{{ $list_order_status[$list_orders->is_activated] }}</td>
                                    <td><a class="button col-md-12" onclick="showdetail('{{$list_orders->order_id}}')" href="javascript:void(0)">View Details</a></td>
                                    <td><a id="td{{$list_orders->order_id}}" class="link button1 col-md-12" href="{{ url('listingdocument/create?id='.$list_orders->id) }}">Document Ready</a></td>
                                </tr>
                                <tr class="sdetails tr{{$list_orders->order_id}}" hidden>
                                    <td>&nbsp;</td>
                                    <td>SERVICE</td>
                                    <td>QTY</td>
                                    <td>COST PER SERVICE</td>
                                    <td>TOTAL</td>
                                    <td>&nbsp;</td>
                                </tr>
                                {{--*/$total=0/*--}}
                                @foreach($list_service as $list_services)
                                    <tr class="details tr{{$list_orders->order_id}}" hidden>
                                        <td>&nbsp;</td>
                                        <td>{{$list_services->service_name}}</td>
                                        <td>@if($list_services->listing_service_id=='1'){{$list_orders->frontend_qty}} @elseif($list_services->listing_service_id=='2'){{ $list_orders->backend_qty }}@endif</td>
                                        <td>${{ $list_services->price }}</td>
                                        <td>@if($list_services->listing_service_id=='1')
                                                ${{ $list_orders->frontend_qty*$list_services->price }}
                                                {{--*/$total+=$list_orders->frontend_qty*$list_services->price/*--}}
                                            @elseif($list_services->listing_service_id=='2')
                                                ${{ $list_orders->backend_qty*$list_services->price }}
                                                {{--*/$total+=$list_orders->backend_qty*$list_services->price/*--}}
                                            @endif
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                @endforeach
                                <tr class="details tr{{$list_orders->order_id}}" style="border-bottom: 2px solid #D8D8D8" hidden>
                                    <td>&nbsp;</td>
                                    <td><b>TOTAL</b></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><b>${{$total}}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 padding-0-20"><h3>Photography Services</h3></div>
                    <div class="col-md-12">
                        <table id="photo_service" class="table" >
                            <thead>
                            <tr>
                                <th class="col-md-2">Order</th>
                                <th class="col-md-2">Product</th>
                                <th class="col-md-2">Date Ordered</th>
                                <th class="col-md-2">Status</th>
                                <th class="col-md-2">View Details</th>
                                <th class="col-md-2">Download Photo</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($photo_order as $photo_orders)
                                <tr id="{{$photo_orders->order_id}}">
                                    <td>{{ $photo_orders->order_id }}</td>
                                    <td>{{ $photo_orders->product }}</td>
                                    <td>{{ date('m/d/Y', strtotime($photo_orders->created_at)) }}</td>
                                    <td>{{ $photo_order_status[$photo_orders->is_activated] }}</td>
                                    <td><a class="button col-md-12" onclick="showdetail('{{$photo_orders->order_id}}')" href="javascript:void(0)">View Details</a></td>
                                    <td><a id="td{{$photo_orders->order_id}}" class="link button1 col-md-12" href="{{ url('photographyphoto/create?id='.$photo_orders->id) }}">Photo(s) Ready</a></td>
                                </tr>
                                <tr class="sdetails tr{{$photo_orders->order_id}}" hidden>
                                    <td>&nbsp;</td>
                                    <td>SERVICE</td>
                                    <td>QTY</td>
                                    <td>COST PER SERVICE</td>
                                    <td>TOTAL</td>
                                    <td>&nbsp;</td>
                                </tr>
                                {{--*/$total=0/*--}}
                                @foreach($photo_service as $photo_services)
                                    <tr class="details tr{{$photo_orders->order_id}}" hidden>
                                        <td>&nbsp;</td>
                                        <td>{{$photo_services->service_name}}</td>
                                        <td>@if($photo_services->id=='1'){{$photo_orders->standard_photo_qty}} @elseif($photo_services->id=='2'){{ $photo_orders->prop_photo_qty }} @elseif($photo_services->id=='3'){{ $photo_orders->lifestyle_photo_qty }} @endif</td>
                                        <td>${{ $photo_services->price }}</td>
                                        <td>
                                            @if($photo_services->id=='1')
                                                ${{ $photo_orders->standard_photo_qty*$photo_services->price }}
                                                {{--*/ $total+=$photo_orders->standard_photo_qty*$photo_services->price /*--}}
                                            @elseif($photo_services->id=='2')
                                                ${{ $photo_orders->prop_photo_qty*$photo_services->price }}
                                                {{--*/ $total+=$photo_orders->prop_photo_qty*$photo_services->price /*--}}
                                            @elseif($photo_services->id=='3')
                                                ${{ $photo_orders->lifestyle_photo_qty*$photo_services->price }}
                                                {{--*/ $total+=$photo_orders->lifestyle_photo_qty*$photo_services->price /*--}}
                                            @endif
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                @endforeach
                                @if($photo_orders->standard_file_format=='2' || $photo_orders->prop_file_format=='2' || $photo_orders->lifestyle_file_format=='2')
                                <tr class="details tr{{$photo_orders->order_id}}" hidden>
                                    <td>&nbsp;</td>
                                    <td>Upgrade to PSD File Format</td>
                                    <td>
                                        {{--*/$qty=0/*--}}
                                        @if($photo_orders->standard_file_format=='2')
                                        {{--*/$qty+=$photo_orders->standard_photo_qty/*--}}
                                        @endif
                                        @if($photo_orders->prop_file_format=='2')
                                        {{--*/$qty+=$photo_orders->prop_photo_qty/*--}}
                                        @endif
                                        @if($photo_orders->lifestyle_file_format=='2')
                                        {{--*/$qty+=$photo_orders->lifestyle_photo_qty/*--}}
                                        @endif
                                        {{ $qty }}
                                        {{--*/ $total+=$qty*10 /*--}}
                                    </td>
                                    <td>$10</td>
                                    <td>${{ $qty*10 }}</td>
                                    <td>&nbsp;</td>
                                </tr>
                                @endif
                                <tr class="details tr{{$photo_orders->order_id}}" style="border-bottom: 2px solid #D8D8D8" hidden>
                                    <td>&nbsp;</td>
                                    <td><b>TOTAL</b></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><b>${{$total}}</b></td>
                                    <td>&nbsp;</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
{!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
{!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnFilterAll = function (oSettings, sInput, iColumn, bRegex, bSmart) {
           var settings = $.fn.dataTableSettings;
           for (var i = 0; i < settings.length; i++) {
               settings[i].oInstance.fnFilter(sInput, iColumn, bRegex, bSmart);
           }
        };
        $('#listing_service').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
        var Table0 = $("#listing_service").dataTable();
        $("#search_all").keyup(function () {
            Table0.fnFilterAll(this.value);
        });
        $('#photo_service').DataTable({
            "bSort" : false,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
        var Table1 = $("#listing_service").dataTable();
        $("#search_all").keyup(function () {
            Table1.fnFilterAll(this.value);
        });
        $('#listing_service_filter').hide();
        $('#photo_service_filter').hide();
    });
</script>
<script>
 function  showdetail(id) {
     $("tr").removeClass('trhead');
     $("#"+id).addClass('trhead');
     $(".details").hide();
     $(".sdetails").hide();
     $(".tr"+id).show();
     $(".link").addClass('button1');
     $(".link").removeClass('button');
     $("#td"+id).removeClass('button1')
     $("#td"+id).addClass('button');
 }
</script>
@endsection