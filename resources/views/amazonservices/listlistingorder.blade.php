@extends('layouts.member.app')
@section('title', 'AMAZON LISTING SERVICES – LISTING OPTIMIZATON')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
<style>
.button {  width:100px;  }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-camera"></i> AMAZON LISTING SERVICES – LISTING OPTIMIZATON</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-camera"></i> Amazon Services</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">AMAZON LISTING SERVICES – LISTING OPTIMIZATON</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="photo" class="table" >
                            <thead>
                            <tr>
                                <th class="col-md-1">Order #</th>
                                <th class="col-md-2">Product</th>
                                <th class="col-md-2">Company</th>
                                <th class="col-md-1">Date Ordered</th>
                                <th class="col-md-2">Status</th>
                                <th class="col-md-4">View Order Details </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($listing_order_list as $listing_orders)
                                <tr id="{{$listing_orders->order_id}}">
                                    <td>{{ $listing_orders->order_id }}</td>
                                    <td>{{ $listing_orders->product_detials }}</td>
                                    <td>{{ $listing_orders->company_name }}</td>
                                    <td>{{ date('m/d/Y', strtotime($listing_orders->created_at)) }}</td>
                                    <td>{{ $listing_order_status[$listing_orders->is_activated] }}</td>
                                    <td><a class="button" onclick="" href="{{url('listingoptimization/'.$listing_orders->id)}}">View</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
@endsection