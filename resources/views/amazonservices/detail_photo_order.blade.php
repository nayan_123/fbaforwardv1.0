@extends('layouts.member.app')
@section('title', $title)
@section('css')
    {!! Html::style('assets/dist/css/style.css') !!}
    {!! Html::style('assets/dist/css/custom_quotes.css') !!}
@endsection
@section('content')
<style>
.content{  min-height: 620px;  }
.background{ background: #ecf0f5 !important;  }
</style>
<div class="row">
    <section class="content">
        <section class="content-header">
            <h1>
                <i class="fa fa-file-text"></i> {{$title}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><i class="fa fa-file-text"></i>{{$title}}</li>
            </ol>
        </section>
        <section class="content">
            <div class="col-md-12 no-padding">
                <div class="box2 col-md-12 no-padding">
                    <div class="col-md-12 no-padding">
                        <div class="box-body no-padding">
                            <div class="">
                                <div class="col-md-12 no-padding">
                                    <div class="col-md-2 blue-bg product-data">
                                        <b>Order</b> #{{ $photo_order['amazon_order'] }}
                                    </div>

                                    <div class="col-md-4 product-data text-right">
                                        Place on {{ date('m/d/Y',strtotime($photo_order['created_at'])) }}
                                    </div>
                                    <div class="col-md-2 product-data text-right">
                                        <b>Photo Status</b>
                                    </div>
                                    <div class="col-md-3 product-data text-right">
                                        {{ $photo_order_status[$photo_order['is_activated']] }}
                                    </div>
                                    <div class="col-md-1 product-data text-right">
                                        <i class="fa fa-cog"></i>
                                    </div>
                                </div>
                                <div class="col-md-12 blank-class"></div>                                <div class="col-md-12 no-padding">
                                    <div class="col-md-4 product-data">
                                        <b>Product:</b> {{ $photo_order['product'] }}
                                    </div>
                                    <div class="col-md-4 product-data">
                                        <b>Company : </b> {{ $photo_order['company_name'] }}
                                    </div>
                                    <div class="col-md-4 product-data">
                                        <b>Estimated Date of Arrival : </b>
                                    </div>
                                    <div class="col-md-12">
                                        @if($photo_order['revision']!='0')
                                            Revision Requested: {{ $photo_order['revision_note'] }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 blank-class"></div>
                    <div class="col-md-12 no-padding background">
                        <div class="box2 col-md-9 invoice-summery-box no-padding">
                            <div class="box-header with-border invoice-summery-heading">
                                <h3 class="box-title">Order Summary</h3>
                            </div>
                            <div class="col-md-12">
                                <table class="table quote-summery-table">
                                    <thead>
                                    <tr>
                                        <th>PHOTOS</th>
                                        <th>QTY</th>
                                        <th>RATE</th>
                                        <th>TOTAL</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--*/$sub_photo_total=0/*--}}
                                    @foreach($photo_service as $photo_services)
                                        <tr>
                                            <td>{{$photo_services->service_name}}</td>
                                            <td>@if($photo_services->id=='1'){{$photo_order['standard_photo_qty']}} @elseif($photo_services->id=='2'){{ $photo_order['prop_photo_qty'] }} @elseif($photo_services->id=='3'){{ $photo_order['lifestyle_photo_qty'] }} @endif</td>
                                            <td>${{ $photo_services->price }}</td>
                                            <td>
                                                @if($photo_services->id=='1')
                                                    ${{ $photo_order['standard_photo_qty']*$photo_services->price }}
                                                    {{--*/ $sub_photo_total+=$photo_order['standard_photo_qty']*$photo_services->price /*--}}
                                                @elseif($photo_services->id=='2')
                                                    ${{ $photo_order['prop_photo_qty']*$photo_services->price }}
                                                    {{--*/ $sub_photo_total+=$photo_order['prop_photo_qty']*$photo_services->price /*--}}
                                                @elseif($photo_services->id=='3')
                                                    ${{ $photo_order['lifestyle_photo_qty']*$photo_services->price }}
                                                    {{--*/ $sub_photo_total+=$photo_order['lifestyle_photo_qty']*$photo_services->price /*--}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    @if($photo_order['standard_file_format']=='2' || $photo_order['prop_file_format']=='2' || $photo_order['lifestyle_file_format']=='2')
                                        <tr>
                                            <td>.PSD File Format</td>
                                            <td>
                                                {{--*/$qty=0/*--}}
                                                @if($photo_order['standard_file_format']=='2')
                                                    {{--*/$qty+=$photo_order['standard_photo_qty']/*--}}
                                                @endif
                                                @if($photo_order['prop_file_format']=='2')
                                                    {{--*/$qty+=$photo_order['prop_photo_qty']/*--}}
                                                @endif
                                                @if($photo_order['lifestyle_file_format']=='2')
                                                    {{--*/$qty+=$photo_order['lifestyle_photo_qty']/*--}}
                                                @endif
                                                {{ $qty }}
                                                {{--*/ $sub_photo_total+=$qty*10 /*--}}
                                            </td>
                                            <td>$10</td>
                                            <td>${{ $qty*10 }}</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td colspan="3"><b>Photo Subtotal</b></td>
                                        <td><b>${{$sub_photo_total}}</b></td>
                                    </tr>
                                    {{--*/$sub_list_total=0/*--}}
                                    @if(!empty($listing_order))
                                        @foreach($listing_service as $list_services)
                                            <tr>
                                                <td>{{$list_services->service_name}}</td>
                                                <td>@if($list_services->listing_service_id=='1'){{$listing_order['frontend_qty']}} @elseif($list_services->listing_service_id=='2'){{ $listing_order['backend_qty'] }}@endif</td>
                                                <td>${{ $list_services->price }}</td>
                                                <td>@if($list_services->listing_service_id=='1')
                                                        ${{ $listing_order['frontend_qty']*$list_services->price }}
                                                        {{--*/$sub_list_total+=$listing_order['frontend_qty']*$list_services->price/*--}}
                                                    @elseif($list_services->listing_service_id=='2')
                                                        ${{ $listing_order['backend_qty']*$list_services->price }}
                                                        {{--*/$sub_list_total+=$listing_order['backend_qty']*$list_services->price/*--}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="3"><b>Listing Subtotal</b></td>
                                            <td><b>${{$sub_list_total}}</b></td>
                                        </tr>
                                    @endif
                                    {{--*/$total=0/*--}}
                                    {{--*/$total=$sub_photo_total+$sub_list_total/*--}}
                                    <tr>
                                        <td colspan="3"><b>Total Deposit Paid</b></td>
                                        <td><b>${{ $total }}</b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box2 order-payment-history-box col-md-3">
                            <div class="box-header">
                                <h3 class="box-title">PHOTO NOTES</h3>
                            </div>
                            <div class="box-body">
                                Photo Description<br>
                                {{ $photo_order['description_photo'] }}
                                <br>
                                Photos or Links to Similar Products
                                <br>
                                 @if(!empty($photo_order_photos))
                                    @foreach($photo_order_photos as $photos)
                                        {{ $photos->photo }} view
                                    @endforeach
                                 @endif
                                {{ $photo_order['links'] }}<br>
                                What to do with product after?<br>
                                @if($photo_order['after_photo']=='1')
                                    I will provide a prepaid shipping label (UPS, FedEx, USPS)
                                @elseif($photo_order['after_photo']=='2')
                                    Donate or dispose of product
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="box2 col-md-12 invoice-summery-box no-padding">
                            <div class="box-header with-border invoice-summery-heading">
                                <h3 class="box-title">Order Summery</h3>
                            </div>
                            <div class="col-md-12">
                                <table class="table quote-summery-table">
                                    <thead>
                                    <tr>
                                        <th>DATE</th>
                                        <th>UPDATE</th>
                                        <th>DETAIL</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    </div>
@endsection