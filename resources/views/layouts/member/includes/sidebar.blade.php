<style type="text/css">
.skin-purple-light .sidebar-menu > li:hover > a, .skin-purple-light .sidebar-menu > li.active > a {  color: #FFF; font-size: 14px;  }
.skin-purple-light .sidebar-menu > li:hover > a, .skin-purple-light .sidebar-menu > li.active > a {  background: none;  }
.skin-purple-light .main-header {  background: #006da3;  }
.skin-purple-light .sidebar-menu > li:hover > a, .skin-purple-light .sidebar-menu > li.active > a:hover {  background: #00a3f5; font-weight: normal;  text-decoration: none;  }
.skin-purple-light .sidebar a {  color: #FFF;  }
.skin-purple-light .sidebar-menu > li.active {  background: #003c6b;  }
.control-sidebar-dark + , body {  background: #0088cc;  }
.main-sidebar {  position: fixed;  }
.skin-purple-light .sidebar-menu > li > a, .skin-purple-light .sidebar-menu > li.active > a {  font-weight: normal;  text-decoration: none;  }
.skin-purple-light .sidebar-menu > li > .treeview-menu {  background: #0088cc;  }
.sidebar-menu .treeview-menu > li > a { padding: 5px 5px 5px 40px;  display: block;  font-size: 14px;  }
.skin-purple-light .treeview-menu > li > a { color: #FFF;  }
.treeview:hover{ background-color: #00a3f5; }
.sidebar-menu li.active > a:hover{  background: #003c6b !important;  }
</style>
<aside class="main-sidebar" style="background-color: #0088cc">
    <section class="sidebar">
        <ul class="sidebar-menu">
           @if(!isset($old_user))
               <li class="{{Request::is('member/home') ? 'active': 'treeview'}}">
                   <a href="{{ url('member/home') }}"><i class="fa fa-television"></i>&nbsp;<span> DASHBOARD</span>
                   </a>
               </li>
           @endif
           @if('Customer' === \Auth::user()->role->name)
               <li class="{{Request::is('quote') ? 'active': 'treeview'}}">
                   <a href="{{ url('quote') }}"><i class="fa fa-anchor"></i><span> QUOTE</span></a>
               </li>
               <li class="{{Request::is('order') ? 'active': 'treeview'}}">
                   <a href="{{ url('order') }}"><i class="fa fa-shopping-cart "></i><span> ORDERS</span></a>
               </li>
               @if(!isset($old_user))
                   <li class="{{Request::is('inventory') ? 'active': 'treeview'}}">
                       <a href="{{ url('inventory') }}"><i class="fa fa-truck"></i><span> INVENTORY</span></a>
                   </li>
                   <li class="{{Request::is('supplychain') ? 'active': 'treeview'}}">
                       <a href="{{ url('supplychain') }}"><i class="fa fa-globe"></i><span>SUPPLY CHAIN</span></a>
                   </li>
                   <li class="{{Request::is('amazonservices') ? 'active' :  'treeview'}}">
                       <a href="{{ url('amazonservices') }}"><i class="fa fa-camera"></i><span> AMAZON SERVICES</span></a>
                   </li>
                   <li class="treeview">
                       <a href=""><i class="fa fa-cloud "></i><span> KNOWLEDGE BASE</span></a>
                   </li>
                   <li class="{{Request::is('billing') ? 'active' :  'treeview'}}">
                       <a href="{{ url('billing') }}"><i class="fa fa-file-text "></i><span> BILLING</span></a>
                   </li>
               @endif
           @endif
           @if('Shipper'===\Auth::user()->role->name)
               <li class="{{Request::is('shipper/quotelist') ? 'active': 'treeview'}}">
                   <a  href="{{ url('shipper/quotelist') }}"><i class="fa fa-anchor "></i>&nbsp;<span> QUOTE</span></a>
               </li>
               <li class="{{Request::is('neworders') ? 'active': 'treeview'}}">
                   <a href="{{ url('neworders') }}"><i class="fa fa-shopping-cart "></i>&nbsp;<span> NEW ORDERS</span></a>
               </li>
               <li class="{{Request::is('shipper/shipment') ? 'active': 'treeview'}}">
                   <a  href="{{ url('shipper/shipment') }}"><i class="fa fa-product-hunt "></i>&nbsp;<span> SHIPMENTS</span></a>
               </li>
               <li class="{{Request::is('billoflading') ? 'active': 'treeview'}}">
                   <a  href="{{ url('billoflading') }}"><i class="fa fa-bold "></i>&nbsp;<span> BILL OF LADING</span></a>
               </li>
               <li class="{{Request::is('prealert') ? 'active': 'treeview'}}">
                   <a  href="{{ url('prealert') }}"><i class="fa fa-product-hunt "></i>&nbsp;<span> PRE-ALERT</span></a>
               </li>
               <li class="{{Request::is('telexrelease') ? 'active': 'treeview'}}">
                   <a  href="{{ url('telexrelease') }}"> <i class="fa fa-file-text-o "></i>&nbsp;<span> TELEX RELEASE</span></a>
               </li>
                <li class="{{Request::is('shipper/invoicing') ? 'active': 'treeview'}}">
                   <a  href="{{ url('shipper/invoicing') }}"><i class="fa fa-bold "></i>&nbsp;<span> INVOICING</span></a>
               </li>
           @endif
           @if('Sales'===\Auth::user()->role->name)
                   <li class="{{Request::is('quote') ? 'active': 'treeview'}}">
                       <a href="{{ url('quote') }}"><i class="fa fa-anchor"></i> QUOTES</a>
                   </li>
                   <li class="{{Request::is('order') ? 'active': 'treeview'}}">
                       <a href="{{ url('order') }}"><i class="fa fa-shopping-cart "></i> ORDERS</a>
                   </li>
                   <li class="{{Request::is('inventory') ? 'active': 'treeview'}}">
                       <a href="{{ url('inventory') }}"><i class="fa fa-truck"></i> INVENTORY</a>
                   </li>
                   <li class="{{Request::is('customer') ? 'active': 'treeview'}}">
                       <a  href="{{ url('customer') }}"><i class="fa fa-user"></i> CUSTOMER ACCOUNTS</a>
                   </li>
           @endif
           @if('Logistics'===\Auth::user()->role->name)
               <li class="{{Request::is('logistics/shipment') ? 'active': 'treeview'}}">
                   <a href="{{ url('logistics/shipment') }}"><i class="fa fa-user"></i> SHIPMENTS</a>
               </li>
               <li class="{{Request::is('bill_of_lading') ? 'active': 'treeview'}}">
                   <a  href="{{ url('bill_of_lading') }}"><i class="fa fa-file-o"></i> BILL OF LADING</a>
               </li>
               <li class="{{Request::is('telexrelease') ? 'active': 'treeview'}}">
                   <a  href="{{ url('telexrelease') }}"><i class="fa fa-file-o"></i> TELEX RELEASE</a>
               </li>
               <li class="{{Request::is('customs-clearance') ? 'active': 'treeview'}}">
                   <a  href="{{ url('customs-clearance') }}"><i class="fa fa-file-o"></i> CUSTOMS CLEARANCE</a>
               </li>
               <li class="{{Request::is('delivery') ? 'active': 'treeview'}}">
                   <a href="{{ url('delivery') }}"><i class="fa fa-truck"></i> DELIVERY</a>
               </li>
               <li class="{{Request::is('customer') ? 'active': 'treeview'}}">
                   <a  href="{{ url('customer') }}"><i class="fa fa-user"></i> CUSTOMER ACCOUNTS</a>
               </li>
           @endif
           @if('Warehouse Admin'===\Auth::user()->role->name)
               <li class="{{Request::is('quote') ? 'active': 'treeview'}}">
                   <a  href="{{ url('quote') }}"><i class="fa fa-anchor"></i> QUOTES</a>
               </li>
               <li class="{{ Request::is('order')? 'active': 'treeview' }}">
                   <a href="{{ url('order') }}"><i class="fa fa-shopping-cart"></i> ORDERS</a>
               </li>
               <li class="{{ Request::is('warehouse/reviewcheckin')? 'active': 'treeview' }}" style="margin-left: 10%;">
                   <a href="{{url('warehouse/reviewcheckin')}}"> Review Check In</a>
               </li>
               <li class="{{ Request::is('warehouse/reviewprep')? 'active': 'treeview' }}" style="margin-left: 10%;">
                   <a href="{{url('warehouse/reviewprep')}}"> Review Prep</a>
               </li>
               <li class="{{ Request::is('warehouse/reviewshipment')? 'active': 'treeview' }}" style="margin-left: 10%;">
                   <a href="{{url('warehouse/reviewshipment')}}"> Review Shipment</a>
               </li>
               <li class="{{Request::is('inventory') ? 'active': 'treeview'}}">
                   <a  href="{{ url('inventory') }}"><i class="fa fa-truck"></i> INVENTORY</a>
               </li>
               <li class="{{Request::is('delivery') ? 'active': 'treeview'}}">
                   <a  href="{{ url('delivery') }}"><i class="fa fa-truck"></i> DELIVERY</a>
               </li>
               <li class="{{Request::is('customer') ? 'active': 'treeview'}}">
                   <a  href="{{ url('customer') }}"><i class="fa fa-user"></i> CUSTOMER ACCOUNTS</a>
               </li>
           @endif
           @if('Warehouse Lead'===\Auth::user()->role->name)
                   <li class="{{ Request::is('order')? 'active': 'treeview' }}">
                       <a href="{{url('order')}}"><i class="fa fa-shopping-cart"></i> ORDERS </a>
                   </li>
                   <li class="{{ Request::is('warehouse/checkin')? 'active': 'treeview' }}" style="margin-left: 10%;">
                       <a href="{{url('warehouse/checkin')}}"> Check In</a>
                   </li>
                   <li class="{{ Request::is('warehouse/prep')? 'active': 'treeview' }}" style="margin-left: 10%;">
                       <a href="{{url('warehouse/prep')}}"> Prep</a>
                   </li>
                   <li class="{{ Request::is('warehouse/shippinglabels')? 'active': 'treeview' }}" style="margin-left: 10%;">
                       <a href="{{url('warehouse/shippinglabels')}}"> Shipping Labels</a>
                   </li>
                   <li class="{{Request::is('inventory') ? 'active': 'treeview'}}">
                       <a  href="{{ url('inventory') }}"><i class="fa fa-truck"></i> INVENTORY</a>
                   </li>
           @endif
           @if('Billing Service'===\Auth::user()->role->name)
                   <li class="{{Request::is('order') ? 'active': 'treeview'}}">
                       <a  href="{{ url('order') }}"><i class="fa fa-shopping-cart "></i> ORDERS</a>
                   </li>
                   <li class="{{Request::is('inventory') ? 'active': 'treeview'}}">
                       <a  href="{{ url('inventory') }}"><i class="fa fa-truck"></i> INVENTORY</a>
                   </li>
                   <li class="{{Request::is('billing') ? 'active' :  'treeview'}}">
                       <a href="{{ url('billing') }}"><i class="fa fa-file-o"></i> INVOICES</a>
                   </li>
               <li class="{{Request::is('customer') ? 'active': 'treeview'}}">
                   <a  href="{{ url('customer') }}"><i class="fa fa-user"></i> CUSTOMER ACCOUNTS</a>
               </li>
           @endif
           @if('Amazon Listing Service'===\Auth::user()->role->name)
               <li class="{{Request::is('photos') ? 'active': 'treeview'}}">
                   <a  href="{{url('photos')}}"> PHOTOS</a>
               </li>
               <li class="{{Request::is('listingoptimization') ? 'active': 'treeview'}}">
                   <a  href="{{url('listingoptimization')}}"> LISTING OPTIMIZATION</a>
               </li>
           @endif
               <li class="{{Request::is('settings') ? 'active': 'treeview'}}" style="position: absolute; bottom: 0px; width:100%;min-height:51px;">
                   <hr class="no-margin" style="margin-bottom: 0px;">
                   <a href="{{ url('settings') }}"><i class="fa fa-cog"></i> <span>SETTINGS</span>
                   </a>
               </li>
        </ul>
    </section>
</aside>