<style type="text/css">
.button__badge {  background-color: #F39C12;  border-radius: 10px;  color: white;  padding: 0px 5px;  font-size: 10px;  position: absolute;  top: 0;  right: 0;  }
.progress_header { height: 3px;  margin-bottom: 0px;  background-color: #ffffff;  border-radius: 7px;  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);  box-shadow: inset 0 1px 2px rgba(0,0,0,.1);  }
.progress, .progress > .progress-bar, .progress .progress-bar, .progress > .progress-bar .progress-bar {  border-radius: 15px; width:60% !important;  }
.dropdown { display:inline-block;  margin-left:20px;  padding:0px;  }
.notifications { min-width:280px;  }
.notifications-wrapper { overflow:hidden;  max-height:250px;  padding-top: 0px;  background-color: #eee;  padding-left: 15px;  margin-top: -10px;  }
.menu-title { color:#9e9e9e;  font-size:1.5rem;  display:inline-block;  font-weight: bold;  }
.notification-heading{  padding: 0px 10px;  }
.item-title {  font-size:1.3rem;  color:#000;  margin-top: 0px;  }
.item-title span, input {  margin: 10px;  }
.notifications:before { content: ''; position: relative;  bottom: 20px; left: 225px; width: 0;  height: 0;  border: 21px solid transparent;  border-bottom: 14px solid #FFF;  }
.arrow:before { content: ''; position: relative; bottom: 38px;  left: 100px;  width: 0; height: 0;  border: 21px solid transparent;  border-bottom: 14px solid #FFF;  }
.navbar-nav > li > a { padding-top: 10px;  padding-bottom: 10px;  }
.button:hover{ color:#fff !important; }
.white-bg{ background: #fff; }
.blue-font{ color:#006da3; }
.round{ border-radius: 30px; }
.padding1{ padding: 1px 3px; }
.margin-top-17{ margin-top: 17px !important; }
.skin-purple-light .main-header .navbar .nav>li>a:hover, .skin-purple-light .main-header .navbar .nav>li>a:active, .skin-purple-light .main-header .navbar .nav>li>a:focus, .skin-purple-light .main-header .navbar .nav .open>a, .skin-purple-light .main-header .navbar .nav .open>a:hover, .skin-purple-light .main-header .navbar .nav .open>a:focus, .skin-purple-light .main-header .navbar .nav>.active>a {  background: rgb(0, 109, 163);  }
#dLabel:hover{ color:#fff !important; }
.capital{ text-transform: uppercase; }
.avtar{ z-index: 5;height: 90px;width: 90px;border: 3px solid;border-color: transparent;border-color: rgba(255,255,255,0.2);border-radius: 50%;font-size: 53px; }
</style>
<header class="main-header">
    <a href="{{ url('/') }}" class="logo" style="line-height: 46px; position: fixed">
        <span class="logo-mini"><img src="{{ url('/').'/uploads/images/LOGO_white_icon.png'  }}" width="90%"/></span>
        <span class="logo-lg"><img src="{{ url('/').'/uploads/images/new_logo.png'  }}" width="100%"/></span>
    </a>
    <nav class="navbar navbar-static-top navbar-fixed-top" role="navigation" >
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (!Auth::guest())
                    {{--*/$user = \Auth::user();/*--}}
                    @if('Customer' === \Auth::user()->role->name)
                            {{--*/ $complete_count = $user->company_complete+$user->billing_complete+$user->amazon_complete+$user->authorize_complete+$user->service_complete /*--}}
                            {{--*/$complete_percentage = ($complete_count*100)/5/*--}}
                            <li class="dropdown">
                                <a  id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#">
                                    <span style="font-size: 11px">COMPLETE YOUR PROFILE&nbsp;&nbsp;&nbsp;{{ $complete_count }}/5</span><br><div class="progress progress_header" style="width: 100% !important;"><div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{ $complete_percentage }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $complete_percentage }}% !important"></div></div>
                                </a>
                                <ul class="dropdown-menu arrow" role="menu" aria-labelledby="dLabel" style="min-width:220px;">
                                    <div class="notification-heading" style="text-align: center"><h4 class="menu-title" style="margin-bottom: 0px">Complete your profile</h4></div>
                                    <li class="divider"></li>
                                    <div class="notifications-wrapper" style="padding-bottom:10px;">
                                        <span class="content">
                                            <div class="notification-item col-md-12">
                                                <h4 class="item-title"><input type="radio" name="" id="" @if($user->company_complete=='1'){{ "checked" }} @endif disabled>@if($user->company_complete=='1')<span style="text-decoration: line-through;"> @else <span>@endif Company Details</span></h4>
                                            </div>
                                            <div class="notification-item col-md-12">
                                                <h4 class="item-title"><input type="radio" name="" id="" @if($user->billing_complete=='1'){{ "checked" }} @endif disabled>@if($user->billing_complete=='1')<span style="text-decoration: line-through;"> @else <span>@endif Billing Details</span></h4>
                                            </div>
                                            <div class="notification-item col-md-12">
                                                <h4 class="item-title"><input type="radio" name="" id="" @if($user->amazon_complete=='1'){{ "checked" }} @endif disabled>@if($user->amazon_complete=='1')<span style="text-decoration: line-through;"> @else <span>@endif Amazon Integration</span></h4>
                                            </div>
                                            <div class="notification-item col-md-12">
                                                <h4 class="item-title"><input type="radio" name="" id="" @if($user->authorize_complete=='1'){{ "checked" }} @endif disabled>@if($user->authorize_complete=='1')<span style="text-decoration: line-through;"> @else <span>@endif Authorize</span></h4>
                                            </div>
                                            <div class="notification-item col-md-12">
                                                <h4 class="item-title" style="float: left"><input type="radio" name="" id="" @if($user->service_complete=='1'){{ "checked" }} @endif disabled>@if($user->service_complete=='1')<span style="text-decoration: line-through;"> @else <span>@endif Service Agreement</span></h4>
                                            </div>
                                            <div class="notification-item col-md-12">
                                               <h4 class="item-title"><a class="button col-md-12" href="{{ url('settings') }}">FINISH</a></h4>
                                            </div>
                                        </span>
                                    </div>
                                </ul>
                            </li>
                    @endif
                <!-- <li><a href="javascript:void(0)" ><i class="fa fa-search" aria-hidden="true"></i></a></li> -->
                    @if('Customer' === \Auth::user()->role->name)
                        <li class="dropdown">
                            <a  id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#">
                                <i class="fa fa-bell" aria-hidden="true"></i>
                                @if($user->notification()->unread()->count()>0)<span class="button__badge" id="count">{{ $user->notification()->unread()->count()  }}</span>@else<span id="count"></span>@endif
                            </a>
                        {{--*/$notification=array()/*--}}
                        @if($user->notification()->unread()->count()>0)
                             {{--*/ $notification = \App\Notification::where('user_id',$user->id)->where('is_read','0')->get()->toArray() /*--}}
                        @endif
                     <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel" style="font-size: 1px">
                         <div class="notification-heading" style="text-align: center"><h4 class="menu-title" style="margin-bottom: 0px">You have new <span style="color:#F39C12"> {{ $user->notification()->unread()->count() }}</span> alerts</h4></div>
                         <li class="divider"></li>
                         <div style="max-height:55px;padding-top: 0px;background-color: rgb(172,172,172);padding-left: 40px;margin-top: -10px;">
                             <a class="content" href="#">
                                 <div class="notification-item"><h4 class="item-title" style="color: #FFF; font-weight: bold; text-align: center">ACTION ITEMS</h4></div>
                             </a>
                         </div>
                         <div class="notifications-wrapper">
                             <a class="content" href="#">
                                 @foreach($notification as $notifications)
                                 <div class="notification-item">
                                     <h4 style="text-align: right; color:rgb(172,172,172); padding-right: 10px; font-weight: bold" onclick="checkread('{{$notifications['id']}}')">x</h4>
                                     <h4 class="item-title">{{ $notifications['subject'] }}</h4>
                                 </div>
                                     <li class="divider"></li>
                                 @endforeach
                             </a>
                         </div>
                     </ul>
                 </li>
                    @else
                        {{--*/ $role= \App\Role::find($user->role_id) /*--}}
                        <li class="dropdown">
                            <a  id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#">
                                <i class="fa fa-bell" aria-hidden="true"></i>
                                @if($role->notification()->unread()->count()>0)<span class="button__badge" id="count">{{ $role->notification()->unread()->count() }}</span>@else<span id="count"></span>@endif
                            </a>
                            {{--*/$notification=array()/*--}}
                            @if($role->notification()->unread()->count()>0)
                                {{--*/ $notification = \App\Notification::where('role_id',$user->role_id)->where('is_read','0')->get()->toArray() /*--}}
                            @endif
                            <ul class="dropdown-menu notifications" role="menu" aria-labelledby="dLabel">
                                <div class="notification-heading"><h4 class="menu-title">You have new <span style="color:#F39C12"> {{ $role->notification()->unread()->count() }}</span> alerts</h4></div>
                                <li class="divider"></li>
                                <div class="notifications-wrapper">
                                    <a class="content" href="#">
                                        @foreach($notification as $notifications)
                                            <div class="notification-item">
                                                <h4 class="item-title" style="float: left">{{ $notifications['subject'] }}</h4> <span class="item-title" style="float:right" onclick="checkread('{{$notifications['id']}}')">x</span>
                                            </div>
                                        @endforeach
                                    </a>
                                </div>
                                <li class="divider"></li>
                            </ul>
                        </li>
                    @endif
                @endif
                {{--*/ $old_user= session('old_user')/*--}}
                {{--*/$new_user=session('new_user')/*--}}
                {{--*/session()->forget('new_user');/*--}}
                @if(session('old_user'))
                    <li><a href="/member/switchuser">Switch Back</a></li>
                @endif
                <li class="dropdown user user-menu">
                    <a href="javascript:void(0)" class="dropdown-toggle"  data-toggle="dropdown" style="padding-bottom:0px !important;">
                        <!-- <img src="{{ url('/')."/".Auth::user()->avatar  }}" class="user-image" alt="User Image"/> -->
                        <?php
                            $string = explode(" ",\Auth::user()->name);
                            if(isset($string[0])){
                                $name = substr($string[0], 0, 1);
                            }
                            if(isset($string[1])){
                                $name = $name.''.substr($string[1], 0, 1);
                            }
                        ?>

                        <div class="white-bg blue-font round padding1 capital" @if(strlen($name) == 2) style="padding:6px 9px"; @else style="padding:4px 10px" @endif >{{ $name }}</div>
                    </a>
                    <ul class="dropdown-menu margin-top-17">
                        <li class="user-header">
                            <!-- <img src="{{ url('/')."/".Auth::user()->avatar  }}" class="img-circle" alt="User Image"> -->
                            <div class="white-bg blue-font round padding1 capital img-circle avtar col-md-offset-4">{{ $name }}</div>
                            <p>
                                {{ Auth::user()->name }}
                                <small>{{ Auth::user()->job_title }}</small>
                                <small>{{ 'Company: ' . Auth::user()->company_name }}</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ url('settings') }}" class="btn btn-default btn-flat"><i class="fa fa-btn fa-cog"></i> Setting</a>
                            </div>
                            <div class="pull-right">
                                <a href="#" data-box="#mb-signout" class="btn btn-danger btn-flat mb-control"><i class="fa fa-btn fa-sign-out"></i> Log out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <script>
        function checkread(id) {
            $.ajax({
                headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}"
                },
                method: 'POST',
                url: '/member/checkread',
                data: {
                    id: id,
                },
                success: function (response) {
                    location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                }
            });
        }
    </script>
</header>