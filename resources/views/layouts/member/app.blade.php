<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="_token" content="{{ csrf_token() }}" />
        <title> {{ getSetting('SITE_TITLE') }} | @yield('title') </title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('assets/bootstrap/css/sweetalert.css') !!}
        {!! Html::style('assets/dist/css/font-awesome.min.css') !!}
        {!! Html::style('assets/plugins/select2/select2.min.css') !!}
        {!! Html::style('assets/dist/css/AdminLTE.min.css') !!}
        {!! Html::style('assets/bootstrap/css/datepicker.css') !!}
        {!! Html::style('assets/dist/css/animate/animate.min.css') !!}
        {!! Html::style('assets/dist/css/skins/_all-skins.min.css') !!}
		{!! Html::style('assets/dist/css/custom.css') !!}
        {!! Html::style('https://cdn.datatables.net/plug-ins/1.10.16/integration/font-awesome/dataTables.fontAwesome.css') !!}
        {!! Html::style('assets/plugins/validationengine/validationEngine.jquery.css') !!}
        @yield('css')
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .skin-purple-light .main-header .logo {  background: #006da3;  }
            .skin-purple-light .main-header .logo:hover {  background: #00a3f5;  }
            .preloader {  position: fixed;  top: 0;  left: 0;  width: 100%;  height: 100%;  z-index: 99999;  display: -webkit-flex;  display: -ms-flexbox; display: flex; -webkit-flex-flow: row nowrap;  -ms-flex-flow: row nowrap;  flex-flow: row nowrap; -webkit-align-items: center;  -ms-flex-align: center; align-items: center;  }
            .sk-spinner-rotating-plane.sk-spinner {  width: 30px; height: 30px;  background-color: #28a7e9;  margin: 0 auto;  -webkit-animation: sk-rotatePlane 1.2s infinite ease-in-out;  animation: sk-rotatePlane 1.2s infinite ease-in-out;  }
            table.dataTable thead th.sorting::after {  content: "\f0dc";  }
            table.dataTable thead th.sorting_asc::after {  content: "\f0de";  }
            table.dataTable thead th.sorting_desc::after {  content: "\f0dd";  }
            table.dataTable thead th { background: transparent !important;  white-space: pre-line;  }
            table.dataTable thead span.sort-icon { display: inline-block; padding-left: 5px;  width: 16px;  height: 16px;  }
            table.table thead th.sorting::after, table.table thead th.sorting_asc::after, table.table thead th.sorting_desc::after {  top: 60%;  }
        </style>
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini">
        <div class="wrapper" style="background-color: #0088cc">
            @include('layouts.member.includes.header')
            @include('layouts.member.includes.sidebar')
            <div class="content-wrapper">
                @include('layouts.member.includes.notifications')
                @yield('content')
            </div>
            <div class="preloader">
               <div class="sk-spinner sk-spinner-rotating-plane"></div>
            </div>
            @include('layouts.member.includes.footer')
            <div class="message-box animated fadeIn" id="mb-signout">
                <div class="mb-container">
                    <div class="mb-middle">
                        <div class="mb-title"><span class="fa fa-sign-out"></span> Log Out ?</div>
                        <div class="mb-content">
                            <p>Are you sure you want to log out?</p>                    
                            <p>Press No if you want to continue work. Press Yes to log out current user.</p>
                        </div>
                        <div class="mb-footer">
                            <div class="pull-right">
                                <a href="{{ url('logout') }}" class="btn btn-success">Yes</a>
                                <button class="btn btn-default mb-control-close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Html::script('assets/plugins/jQuery/jQuery-2.1.4.min.js') !!}
        {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('assets/bootstrap/js/sweetalert.min.js') !!}
        {!! Html::script('assets/plugins/select2/select2.full.min.js') !!}
        {!! Html::script('assets/plugins/slimScroll/jquery.slimscroll.min.js') !!}
        {!! Html::script('assets/plugins/fastclick/fastclick.min.js') !!}
        {!! Html::script('assets/dist/js/app.min.js') !!}
        {!! Html::script('assets/plugins/noty/packaged/jquery.noty.packaged.js') !!}
        {!! Html::script('assets/dist/js/custom.js') !!}
        {!! Html::script('assets/bootstrap/js/datePicker.js') !!}
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
        <script type="text/javascript">
            $(document).ready(function () {
                $('.preloader').fadeOut(0); // set duration in brackets
            });
        </script>
        @yield('js')
        <style>
            .modal-backdrop
            {
                position: relative;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: 1040;
                background-color: #000;
            }
        </style>
    </body>
</html>