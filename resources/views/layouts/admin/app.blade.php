<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="_token" content="{{ csrf_token() }}" />

        <title> {{ getSetting('SITE_TITLE') }} | @yield('title') </title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <style>
            .error, .error:focus{
                border-color: #ee0101 !important;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            }
            .error .select2-selection .select2-selection--single{
                border-color: #ee0101 !important;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            }
            .formError .formErrorContent {
                width: 100% !important;
                background: #ee0101 !important;
                position: relative !important;
                color: #fff !important;
                min-width: 120px !important;
                font-size: 11px !important;
                border: 2px solid #ddd !important;
                box-shadow: 0 0 6px #000 !important;
                -moz-box-shadow: 0 0 6px #000 !important;
                -webkit-box-shadow: 0 0 6px #000 !important;
                -o-box-shadow: 0 0 6px #000 !important;
                padding: 4px 10px 4px 10px !important;
                border-radius: 6px !important;
                -moz-border-radius: 6px !important;
                -webkit-border-radius: 6px !important;
                -o-border-radius: 6px !important;
            }
            .formError .formErrorArrow div {
                background: #ee0101 !important;
            }
            .formError input[type="text"]{
                background: #ee0101 !important;
            }
        </style>
        {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('assets/bootstrap/css/sweetalert.css') !!}
        {!! Html::style('assets/dist/css/font-awesome.min.css') !!}
        {!! Html::style('assets/plugins/select2/select2.min.css') !!}
        {!! Html::style('assets/dist/css/AdminLTE.min.css') !!}
        {!! Html::style('assets/dist/css/animate/animate.min.css') !!}
        {!! Html::style('assets/dist/css/skins/_all-skins.min.css') !!}
		{!! Html::style('assets/dist/css/custom.css') !!}
        {!! Html::style('assets/bootstrap/css/datepicker.css') !!}
        @yield('css')
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-yellow-light sidebar-mini">
        <div class="wrapper">
            @include('layouts.admin.includes.header')
            @include('layouts.admin.includes.sidebar')
            <div class="content-wrapper">
                @include('layouts.admin.includes.notifications')
                @yield('content')
            </div>
            @include('layouts.admin.includes.footer')
            <div class="message-box animated fadeIn" id="mb-signout">
                <div class="mb-container">
                    <div class="mb-middle">
                        <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                        <div class="mb-content">
                            <p>Are you sure you want to log out?</p>                    
                            <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                        </div>
                        <div class="mb-footer">
                            <div class="pull-right">
                                <a href="{{ url('logout') }}" class="btn btn-success">Yes</a>
                                <button class="btn btn-default mb-control-close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Html::script('assets/plugins/jQuery/jQuery-2.1.4.min.js') !!}
        {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('assets/bootstrap/js/sweetalert.min.js') !!}
        {!! Html::script('assets/plugins/select2/select2.full.min.js') !!}
        {!! Html::script('assets/plugins/slimScroll/jquery.slimscroll.min.js') !!}
        {!! Html::script('assets/plugins/fastclick/fastclick.min.js') !!}
        {!! Html::script('assets/dist/js/app.min.js') !!}
        {!! Html::script('assets/plugins/noty/packaged/jquery.noty.packaged.js') !!}
        {!! Html::script('assets/dist/js/custom.js') !!}
        {!! Html::script('assets/bootstrap/js/datePicker.js') !!}
       @yield('js')
    </body>
</html>