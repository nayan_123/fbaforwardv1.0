<nav class="navbar navbar-default navbar-fixed-top laraship-nav" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>
            <a href="{{ url('/') }}" class="navbar-brand external">{{ getSetting('SITE_TITLE') }}</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right text-uppercase">
                <li id="nav_home"><a href="{{ url('/#home') }}">Home</a></li>
                <li id="nav_contact_us"><a href="{{ url('/#contact') }}">Contact Us</a></li>
				@if (Auth::guest())
                    <li><a class="external" href="{{ url('/login') }}">My Account</a></li>
                @elseif(!Auth::guest())
                    <li><a class="external" href="{{ url('member/home') }}">Dashboard</a></li>
                    <li><a class="external" href="{{ url('logout') }}">Log out</a></li>
				@endif
            </ul>
        </div>
    </div>
</nav>

