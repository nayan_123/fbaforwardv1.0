@extends('layouts.member.app')
@section('title', 'Shipment')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
<section class="content">
    <section class="content-header">
        <h1><i class="fa fa-shopping-cart"></i> Shipment</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-shopping-cart"></i>Shipment</li>
        </ol>
    </section>
    <section class="content">
        <div class="box border-top-0">
        	<ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                <li class="nav-item col-md-4 nav-tab no-padding active" id="tab">
                    <a class="nav-link active text-capitalize" data-toggle="tab" href="javascript:void(0)"  onclick="tabchange('1')">All Shipments </a>
                </li>
                <li class="nav-item col-md-4 nav-tab no-padding" id="tab">
                    <a class="nav-link text-capitalize" data-toggle="tab" href="javascript:void(0)" onclick="tabchange('2')">Pending </a>
                </li>
                <li class="nav-item col-md-4 nav-tab no-padding" id="tab">
                    <a class="nav-link text-capitalize" data-toggle="tab" href="javascript:void(0)" onclick="tabchange('3')">Inbound </a>
                </li>
                <li class="nav-item col-md-4 nav-tab no-padding" id="tab">
                    <a class="nav-link text-capitalize" data-toggle="tab" href="javascript:void(0)" onclick="tabchange('4')">At Port </a>
                </li>
                <li class="nav-item col-md-4 nav-tab no-padding" id="tab">
                    <a class="nav-link text-capitalize" data-toggle="tab" href="javascript:void(0)" onclick="tabchange('5')">Delivered</a>
                </li>
            </ul>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive no-padding">
                            <table id="shipment_data" class="table" >
                                <thead>
                                <tr>
                                    <th class="col-md-2">Order Number</th>
                                    <th class="col-md-3">Product</th>
                                    <th class="col-md-2">Company</th>
                                    <th class="col-md-2">Shipping Method</th>
                                    <th class="col-md-2">Status</th>
                                    <th class="col-md-1">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($order))
                                    @foreach($order as $orders)
                                        <tr>
                                            <td class="col-md-2">{{ $orders['order_no'] }}</td>
                                            <td class="col-md-3">{{ substr($orders['product_name'],0,35) }} @if(strlen($orders['product_name']) > 35) {{ "..." }} @endif</td>
                                            <td class="col-md-2">{{ $orders['company_name'] }}</td>
                                            <td class="col-md-2">{{ $orders['shipping_name'] }}</td>
                                            <td class="col-md-2">{{ $orderStatus[$orders['is_activated']] }}</td>
                                            <td class="col-md-2"> 
                                            	<a href="{{ url('view-logistics-shipment',$orders['order_id']) }}">
                                            		<button class="button">View</button>
                                            	</a>
                                           	</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection
@section('js')
<script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
{!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
{!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
{!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
{!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('#shipment_data').DataTable({
            "bSort" : true,
            "bPaginate": false,
            "bFilter": true,
            "bInfo": false,
        });
    });
</script>
<script type="text/javascript">
	function tabchange(tab_id){
		var html= '';
		var url = '{{ url('') }}';
		 $.ajax({
            headers: {
            'X-CSRF-Token':  "{{ csrf_token() }}"
            },
            method: 'POST', // Type of response and matches what we said in the route
            url: '/logistics/get-shipment', // This is the url we gave in the route
            data: { 'tab_id': tab_id },
            success: function (response) {
            	html +='<table id="shipment_data" class="table" >';
            	html +='<thead><tr>';
            	html +='<th class="col-md-2">Order Number</th>';
            	html +='<th class="col-md-3">Product</th>';
            	html +='<th class="col-md-2">Company</th>';
            	html +='<th class="col-md-2">Shipping Method</th>';
            	html +='<th class="col-md-2">Status</th>';
            	html +='<th class="col-md-1">Action</th>';
            	html +='</tr></thead>';
	            	$.each(response.order, function( index, value ) {
	            		html+= '<tbody>';
					  	html +='<tr>';
					  	html +='<td class="col-md-2">'+value.order_no+'</td>';
					  	html +='<td class="col-md-3">'+value.product_name.substr(0, 35);
                        html +='</td>';
					  	html +='<td class="col-md-2">'+value.company_name+'</td>';
					  	html +='<td class="col-md-2">'+value.shipping_name+'</td>';
					  	html +='<td class="col-md-2">'+response.orderStatus[value.is_activated]+'</td>';
					  	html +='<td class="col-md-2">';
					  	html +='<a href="'+url+'/view-logistics-shipment/'+value.order_id+'">';
					  	html +='<button class="button">View</button>';
					  	html +='</a></td>';
					});
            	html +='</tbody>';
            	html +='</table>';
            	$('#shipment_data').html(html);
            }
        });
	}
</script>
@endsection