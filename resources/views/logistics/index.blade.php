@extends('layouts.member.app')
@section('title', 'New Orders')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-shopping-cart"></i> Bill of lading</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-shopping-cart"></i> Bill of lading</li>
        </ol>
    </section>
    <section class="content">
        <div class="box">
            <div class="box-body">
                <table id="bill_of_lading" class="table" >
                    <thead>
                    <tr>
                        <th class="col-md-3">Order Number</th>
                        <th class="col-md-5">Product</th>
                        <th class="col-md-2">Company</th>
                        <th class="col-md-2">Date Ordered</th>
                        <th class="col-md-2">View</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($order as $orders)
                            <tr>
                                <td>{{ $orders->order_no }}</td>
                                <td>{{ $orders->product_name }}</td>
                                <td>{{ $orders->company_name }}</td>
                                <td>{{ \Carbon\Carbon::parse($orders->created_at)->format('Y-m-d') }}</td>
                                <td><a class="button" href="{{ url('/bill_of_lading/details?id='.$orders->order_id) }}">View</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @endsection
@section('js')
    {!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/dataTables.responsive.min.js') !!}
    {!! Html::script('assets/dist/js/datatable/responsive.bootstrap.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#bill_of_lading').DataTable({
                "bSort" : false,
                "bPaginate": false,
                "bFilter": true,
                "bInfo": false,
            });
        });
    </script>
@endsection