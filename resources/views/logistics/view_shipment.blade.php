@extends('layouts.member.app')
@section('title', 'Shipment Details')
@section('css')
{!! Html::style('assets/dist/css/datatable/dataTables.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/responsive.bootstrap.min.css') !!}
{!! Html::style('assets/dist/css/datatable/dataTablesCustom.css') !!}
{!! Html::style('assets/dist/css/style.css') !!}
{!! Html::style('assets/dist/css/custom_quotes.css') !!}
<style>
.my-label{  line-height: 3;  }
.content{  min-height: 620px;  }
.box-size {  width: 20% !important;  }
.nav-tabs>li {  margin-bottom: 0px;  }
.route{ padding-left: 5%; }
.connecting-line{ top:21%; }
.icons { color: #0088cc;cursor: pointer;}
.nav-tabs{ border: none; }
.box2{ border-top:none; }
.item-title span, input { margin: 10px 0px !important; }
a{ color:#555; }
.icon-link:hover{ color:#555 !important;cursor: pointer; }
.place-date{  width: 21.66% !important;  border-bottom: 1px solid #c8c8c8;  }
.place-date p{ line-height: 2.3; }
@media (min-width: 1650px) {  .route{ padding-left: 8em; }  }
.btn-reject-quote{ color:#fff !important; }
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td{	border-top: 1px solid #f4f4f4 !important;  }
.routep{ font-size: 13px;  color: #333;  font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;  }
</style>
@endsection
@section('content')
<section class="content-header">
    <h1><i class="fa fa-anchor"></i> Shipment Details</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-anchor"></i>Shipment Details</li>
    </ol>
</section>
<section class="content">
	<div class="row padding20">
	<div class="box">
		<div class="col-md-12 box2 no-padding">
            <div class="box-body no-padding">
            	<div class="col-md-12 no-padding ">
                    <div class="col-md-3 box-size blue-bg product-data height71">
                        <b>Order</b> #{{ $order->order_no }}
                    </div>
                    <div class="col-md-3 place-date text-left height71">
                        <p>
                        @if($order->created_at) 
                            Placed on {{ \Carbon\Carbon::parse($order->created_at)->format('M d,Y') }}
                        @endif
                        </p>
                    </div>
                    <div class="col-md-7 product-data text-right height71">
                        <div class="col-md-3 text-center"></div>
                        <div class="col-md-3 text-left pull-right">
                            <a href="{{ url('customer') }}" class="icon-link">
                                <i class="fa fa-users icons"></i>
                                <span class="light-blue-link">View Company</span>
                            </a><br>
                            <a data-toggle="modal" data-target="#addNotesModal" class="icon-link">
                                <i class="fa fa-file-text-o icons" ></i>
                                <span data-toggle="modal" data-target="#addOrderNoteModal" class="light-blue-link">Add Order Note</span>
                            </a><br>
                            <a href="https://accounts.zoho.com" class="icon-link" target="_blank">
                                <i class="fa fa-users icons"></i>
                                <span class="light-blue-link">Go to CRM</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <div class="col-md-4 product-data">
                            <p>Company : {{ $order->company_name }}</p>
                        </div>
                        <div class="col-md-4 product-data">
                            <p>Customer Status : </p>
                        </div>
                        <div class="col-md-4 product-data">
                            <p>Logistics Status : </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 no-padding">
                    <div class="col-md-3 box-size product-data text-overflow">
                        <b>Product : </b>{{ $order->product_name }}
                    </div>
                    <div class="col-md-3 box-size product-data">
                        <b> Units: </b>{{ $order->total }}
                    </div>
                    <div class="col-md-3 box-size product-data">
                        <b>Cartons : </b>{{ $order->no_boxs }}
                    </div>
                    <div class="col-md-3 box-size product-data">
                        <b>Weight : </b>{{ $order->chargeable_weight }}
                    </div>
                    <div class="col-md-3 box-size product-data">
                        <b>Volume : </b>{{ $order->shipment_volume }}
                    </div>
                    <div class="col-md-3 box-size product-data height61">
                        <b>Inbound Shipping Method :  </b>{{ $order->shipping_name }}
                    </div>
                    <div class="col-md-3 box-size product-data height61">
                        <b>Type: </b>{{ $order->shipment_type_short_name }}
                    </div>
                    <div class="col-md-3 box-size product-data height61">
                        <b>Incoterms : </b>{{ $order->incoterm_short_name }}
                    </div>
                    <div class="col-md-3 box-size product-data height61">
                        @if($order->order_type=='1')
                        <b> Transit Time :</b>
                            @if($order->prealert_id)
                                <?php
                                $date1 = strtotime($order->ETD_china);
                                $date2 = strtotime($order->ETA_US);
                                $datediff = $date2 - $date1;
                                if(($shipping_method->shipping_name == 'OCEAN FREIGHT') || ($shipping_method->shipping_name == 'AIR FREIGHT')){
                                    echo '<br>Port to port: '.floor($datediff / (60 * 60 * 24)). 'Days';
                                }
                                else if($shipping_method->shipping_name == 'AIR EXPRESS'){
                                    echo '<br>Origin to FBAforward: '.floor($datediff / (60 * 60 * 24)). 'Days';
                                }
                                ?>
                            @else
                                {{ "TBD" }}
                            @endif
                        @else
                            <b>Transit Time : </b> expected delivery
                        @endif
                    </div>
                    <div class="col-md-3 box-size product-data height61">
                        <b>Outbound Shipping Method : </b> {{ $order->outbound_method_short_name }}
                    </div>
                </div>
            </div>
        </div>
	</div>
	<div class="col-md-12 blank-class"></div>
	<div class="col-md-12 no-padding">
        <div class="box2 col-md-12 invoice-summery-box no-padding">
            <div class="col-md-12">
                <h4 class="box-title">Route</h4>
            </div>
            <div class="connecting-line"></div>
            <ul class="nav nav-tabs">
                <li class="col-md-3 text-center route-icon">
                    <p class="round-tab"><i class="fa fa-university"></i></p>
                </li>
                <li class="col-md-3 text-center route-icon">
                    <p class="round-tab"><i class="fa fa-anchor"></i></p>
                </li>
                <li class="col-md-3 text-center route-icon">
                    <p class="round-tab"><i class="fa fa-anchor"></i></p>
                </li>
                <li class="col-md-3 text-center route-icon">
                    <p class="round-tab"><i class="fa fa-circle"></i></p>
                </li>
            </ul>
            <div class="col-md-2 col-md-offset-1 padding20">
                <b><p class="routep">ORIGIN</p></b>
                <p>Supplier's Name: {{ $order->contact_name }}</p>
                <p>{{ $order->port_of_origin }}</p>
                <small><b>Cargo Ready Date:</b> {{ \Carbon\Carbon::parse($order->goods_ready_date)->format('M d,Y') }}</small><br>
                <small><b>Pick Up Date:</b>@if($order->incoterm_id=='2') {{ date('m d, Y',strtotime($order->ETA_US)) }}@endif</small><br>
            </div>
            <div class="col-md-3 padding20 route">
                <b><p>DEPARTURE PORT</p></b>
                <p>{{ $order->shipper_destination }}</p>
                <small><b>Arrival Date:</b> @if($order->prealert_id) {{ \Carbon\Carbon::parse($order->ETA_US)->format('M d,Y') }} @endif</small><br>
                <small><b>Departure Date:</b> @if($order->prealert_id) {{ \Carbon\Carbon::parse($order->ETD_china)->format('M d,Y') }} @endif</small><br>
                @if($order->shipping_method_id=='3')
                    <a href="https://wwwapps.ups.com/WebTracking/track?track=yes&amp;trackNums=&quot;TRACKINGNUMBERGOESHERE" class="light-blue-link"><small>Track Shipment</small></a>
                @endif
            </div>
            <div class="col-md-3 padding20 route">
                <b><p>ARRIVAL PORT</p></b>
                <p>{{ $order->shipper_destination }}</p>
                <small> <b>Arrival Date:</b> @if($order->prealert_id) {{ \Carbon\Carbon::parse($order->ETA_US)->format('M d,Y') }} @endif</small><br>
                <small> <b>Customs Release Date:</b> @if($order->delivery_id) {{ date('m d, Y',strtotime($order->custom_release_date)) }} @endif</small><br>
                <small> <b>Cargo Release Date:</b> @if($order->delivery_id) {{ date('m d, Y',strtotime($order->cargo_release)) }} @endif</small><br>
                <small> <b>Pick Up Date:</b> @if($order->delivery_id) {{ date('m d, Y',strtotime($order->ETA_to_warehouse)) }} @endif</small>
            </div>
            <div class="col-md-3 padding20 route">
                <b><p class="routep">FBAFORWARD</p></b>
                <p>San Diego, CA</p>
                <p>Ningbo, China</p>
                <small>Checked In: @if($order->checkin_id) {{ date('m d, Y',strtotime($order->checkin_date)) }} @endif</small><br>
                <small>Status: Prep Completed</small><br>
                <small><b>Departure Date:</b>  </small><br>
                
            </div>
        </div>
    </div>
    <div class="col-md-12 no-padding">
    	<div class="col-md-9 no-padding">
    		<div class="col-md-12 no-padding">
                <div class="box2 col-md-12 invoice-summery-box no-padding">
                    <div class="box-header with-border invoice-summery-heading">
                        <h3 class="box-title"><b>Order</b></h3>
                    </div>
                    <div class="col-md-12">
                    	<div class="col-md-9">
                    		@if($order->order_type == 1)
                    			FBAforward Turnkey Service
                    		@else
                    			Shipping and Customs Arrangement
                    		@endif
                    	</div>
                    	<div class="col-md-3 text-right">
                    		$299
                    	</div>
                    </div>
                </div>
            </div>
    		<div class="col-md-12 no-padding">
                <div class="box2 col-md-12 invoice-summery-box no-padding">
                    <div class="box-header with-border invoice-summery-heading">
                        <h3 class="box-title"><b>Documents</b></h3> &nbsp;&nbsp;&nbsp;<a class="light-blue-link pull-right" data-toggle="modal" data-target="#editDocumentModal">Edit</a>
                    </div>
                    <div class="col-md-12">
                        @if(count($order_documents) != 0)
    						<table class="table quote-summery-table">
    	                        <thead>
    	                        <tr>
    	                            <th class="col-md-3">Customer Documents</th>
    	                            <th class="col-md-3">Date Submitted</th>
    	                            <th class="col-md-3">Due Date</th>
    	                            <th class="col-md-3 text-center">View</th>
    	                        </tr>
    	                        </thead>
    	                        <tbody>
    	                        	@foreach($order_documents as $order_document)
    		                        	<tr>
                                            {{--*/ $due_date = ''; /*--}}
                                            @if($order_document->due_date) 
                                                {{--*/ $due_date = \Carbon\Carbon::parse($order_document->due_date)->format('m/d/Y'); /*--}}
                                            @else
                                                {{--*/ $due_date = null; /*--}}
                                            @endif

    			                            <td class="col-md-3">{{ $order_document->document }}</td>
    			                            <td class="col-md-3">{{ \Carbon\Carbon::parse($order_document->created_at)->format('m/d/Y') }}</td>
    			                            <td class="col-md-3">{{ $due_date }}</td>
    			                            <td class="col-md-3"><button data-toggle="modal" data-target="#viewDocumentModal" data-document-id="{{ $order_document->id }}" id="{{ url('uploads/documents',$order_document->document) }}" class="button view_document col-md-12">View</button></td>
    			                        </tr>
    			                    @endforeach
    	                        </tbody>
    	                    </table>
                        @else
                            <table class="table quote-summery-table">
                                <thead>
                                    <tr>
                                        No documents are found.
                                    </tr>
                                </thead>
                            </table>
                        @endif
                        @if(!(empty($prealert_detail)) && (!empty($bill_of_lading)) && (!empty($telex_release)))
    	                    <table class="table quote-summery-table">
    	                        <thead>
    	                        <tr>
    	                            <th class="col-md-3">Seabay Documents</th>
    	                            <th class="col-md-3">Date Submitted</th>
    	                            <th class="col-md-3">Status</th>
    	                            <th class="col-md-3 text-center">View</th>
    	                        </tr>
    	                        </thead>
    	                        <tbody>
    	                        	@if(!empty($prealert_detail))
    	                        	<tr>
    		                            <td class="col-md-3">ISF</td>
    		                            <td class="col-md-3">{{ \Carbon\Carbon::parse($prealert_detail->created_at)->format('m/d/Y') }}</td>
    		                            <td class="col-md-3">@if($prealert_detail->status == 0) {{ "Pending" }} @else {{ "Filed" }} @endif</td>
    		                            <td class="col-md-3"><button class="button col-md-12">View</button></td>
    		                        </tr>
    		                        @endif
    		                        @if(!empty($bill_of_lading))
    		                        <tr>
    		                            <td class="col-md-3">Bill Of Lading</td>
    		                            <td class="col-md-3">{{ \Carbon\Carbon::parse($bill_of_lading->created_at)->format('m/d/Y') }}</td>
    		                            <td class="col-md-3">@if($bill_of_lading->status == 0) {{ "Pending" }} @else {{ "Filed" }} @endif</td>
    		                            <td class="col-md-3"><button class="button col-md-12">View</button></td>
    		                        </tr>
    		                        @endif
    		                        @if(!empty($telex_release))
    		                        <tr>
    		                            <td class="col-md-3">Telex Release</td>
    		                            <td class="col-md-3">{{ \Carbon\Carbon::parse($telex_release->created_at)->format('m/d/Y') }}</td>
    		                            <td class="col-md-3">@if($telex_release->status == 0) {{ "Pending" }} @else {{ "Filed" }} @endif</td>
    		                            <td class="col-md-3"><button class="button col-md-12">View</button></td>
    		                        </tr>
    		                        @endif
    	                        </tbody>
    	                    </table>
                        @endif
                        @if($customer_clearance)
    	                    <table class="table quote-summery-table">
    	                        <thead>
    	                        <tr>
    	                            <th class="col-md-3">Other Documents</th>
    	                            <th class="col-md-3">Date Submitted</th>
    	                            <th class="col-md-3"></th>
    	                            <th class="col-md-3 text-center">View</th>
    	                        </tr>
    	                        </thead>
    	                        <tbody>
    	                        	<tr>
    		                            <td class="col-md-3">3461</td>
    		                            <td class="col-md-3">@if($customer_clearance){{ \Carbon\Carbon::parse($customer_clearance->created_at)->format('m/d/Y') }}@endif</td>
    		                            <td class="col-md-3"></td>
    		                            <td class="col-md-3"><button class="button col-md-12">View</button></td>
    		                        </tr>
    	                        </tbody>
    	                    </table>
                        @endif
					</div>
				</div>
			</div>
			<div class="col-md-12 no-padding">
                <div class="box2 col-md-12 invoice-summery-box no-padding">
                    <div class="box-header with-border invoice-summery-heading">
                        <h3 class="box-title"><b>Logistics Details</b></h3> &nbsp;&nbsp;&nbsp;<span class="light-blue-link pull-right" data-toggle="modal" data-target="#editLogisticModal">Edit</span>
                    </div>
                    <div class="col-md-6">
                    	<div class="col-md-12">
                    		<table class="table quote-summery-table">
		                        <thead>
		                        <tr>
		                            <th class="col-md-6">Shipping</th>
		                            <th class="col-md-6"></th>
		                        </tr>
		                        </thead>
		                        <tbody>
		                        	<tr>
			                            <td class="col-md-6">ETD Origin</td>
			                            <td class="col-md-6">@if($prealert_detail) {{ \Carbon\Carbon::parse($prealert_detail->ETD_china)->format('m/d/Y') }} @endif</td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-6">ETA U.S.</td>
			                            <td class="col-md-6">@if($prealert_detail) {{ \Carbon\Carbon::parse($prealert_detail->ETA_US)->format('m/d/Y') }} @endif</td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-6">Container Number</td>
			                            <td class="col-md-6"></td>
			                        </tr>
		                        </tbody>
		                    </table>
                    	</div>
                    	<div class="col-md-12">
							<table class="table quote-summery-table">
		                        <thead>
		                        <tr>
		                            <th class="col-md-6">Customs</th>
		                            <th class="col-md-6"></th>
		                        </tr>
		                        </thead>
		                        <tbody>
		                        	<tr>
			                            <td class="col-md-6">ISF Status</td>
			                            <td class="col-md-6">@if($prealert_detail)@if($prealert_detail->status == 0) {{ "Pending" }} @else {{ "Filed" }} @endif @else {{ "Pending" }} @endif</td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-6">Duties</td>
			                            <td class="col-md-6"></td>
			                        </tr>
			                        <div class="row">
				                        <tr>
				                            <td class="col-md-6">Exam</td>
				                            <td class="col-md-6">@if($custom_exam_hold) {{ "Yes" }} <br>{{ $custom_exam_hold->note }} @else {{ "No" }} @endif</td>
				                        </tr>
				                    </div>
				                    <div class="row">
					                    <tr>
				                            <td class="col-md-6">Shipment Contains:</td>
				                            <td class="col-md-6">
				                            	@if($shipment_contains)
						                            @foreach($shipment_contains as $shipment_contain)
						                            	- {{ $shipment_contain->name }}<br>
						                            @endforeach
					                            @endif
				                            </td>
				                        </tr>
				                    </div>
			                        <tr>
			                            <td class="col-md-6">Additional Services</td>
			                            <td class="col-md-6">
			                            	@if($order)
			                            	@if($order->FDA_clearance == 1)
			                            		{{ "FDA Clearance" }}<br>
			                            	@endif
			                            	@if($order->lacey_act == 1)
												{{ "Lacey Act" }}<br>
											@endif
			                            	@if($order->OGA == 1)
			                            		{{ "OGA" }}<br>
			                            	@endif
			                            	@endif
			                            </td>
			                        </tr>
		                        </tbody>
		                    </table>
	                	</div>
					</div>
					<div class="col-md-6">
						<div class="col-md-12">
							<table class="table quote-summery-table">
		                        <thead>
		                        <tr>
		                            <th class="col-md-6">Port & CFS</th>
		                            <th class="col-md-6"></th>
		                        </tr>
		                        </thead>
		                        <tbody>
		                        	<tr>
			                            <td class="col-md-6">Port Fees</td>
			                            <td class="col-md-6">@if($customer_clearance){{ $customer_clearance->terminal_fees }}@endif</td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-6">CFS Fees</td>
			                            <td class="col-md-6">@if($cfs_fee) {{ $cfs_fee }} @endif</td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-6">Pallet Exchange</td>
			                            <td class="col-md-6"></td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-6">Cargo Release Date</td>
			                            <td class="col-md-6">@if($order->delivery_id) {{ date('m/d/Y',strtotime($order->cargo_release)) }} @endif</td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-6">LFD</td>
			                            <td class="col-md-6"></td>
			                        </tr>
			                    </tbody>
		                    </table>
		                </div>
		                <div class="col-md-12">
							<table class="table quote-summery-table">
		                        <thead>
		                        <tr>
		                            <th class="col-md-6">Delivery</th>
		                            <th class="col-md-6"></th>
		                        </tr>
		                        </thead>
		                        <tbody>
		                        	<tr>
			                            <td class="col-md-6">Pick Up Date</td>
			                            <td class="col-md-6"></td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-6">Delivery Date</td>
			                            <td class="col-md-6">{{ \Carbon\Carbon::parse($order->ETA_to_warehouse)->format('m/d/Y') }}</td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-6">Company</td>
			                            <td class="col-md-6">{{ $order->delivery_company }}</td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-6">Pick Up Location</td>
			                            <td class="col-md-6">{{ $order->location_of_goods }}</td>
			                        </tr>
		                        </tbody>
		                    </table>
		                </div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 padding-right-0">
	    	<div class="box2 no-padding col-md-12">
	    		<div class="white-bg box-header order-payment-history-heading with-border">
	                <h3 class="box-title">HOLD</h3>
	            </div>
	            <div class="col-md-12 padding20">
                    <div class="col-md-12 no-padding">
                        @if(count($order_holds) != 0)
                            <div class="col-md-12 no-padding">
                                <div class="col-md-4 no-padding">
                                    <label class="control-label">Type</label>
                                </div>
                                <div class="col-md-8 no-padding">
                                    <label class="control-label">Details</label>
                                </div>
                            </div>
                            @foreach($holds as $hold)
                                @foreach($order_holds as $order_hold)
                                    @if(($order_hold->is_deleted == 0) && ($order_hold->hold_id == $hold->id))
                                    <div class="col-md-12 no-padding">
                                        <div class="col-md-4 no-padding">
                                            <label class="control-label">{{ $hold->hold_name }}</label>
                                        </div>
                                        <div class="col-md-8 no-padding">
                                            {{ $order_hold->sub_hold_name }}
                                        </div>
                                    </div>
                                    @endif
                                @endforeach
                            @endforeach
                        @endif
                    </div>
	            	<button data-toggle="modal" data-target="#submitHold" class="btn btn-reject-quote col-md-12" style="margin-top:10px;">SUBMIT HOLD</button>
	            	<button data-toggle="modal" data-target="#releaseHold" class="btn btn-reject-quote col-md-12" style="margin-top:10px;">RELEASE HOLD</button>
	            </div>
	    	</div>
	    	<div class="box2 no-padding col-md-12">
	    		<div class="white-bg box-header order-payment-history-heading with-border">
	                <h3 class="box-title">LOCATION</h3>
	                <div class="pull-right">
	                	<a href="#" class="light-blue-link">Edit</a>
	                </div>
	            </div>
	            <div class="col-md-12 padding20">
	            	<p class="text-center">Current Location:</p>
	            	<select class="form-control" name="location" id="location">
	            		@foreach($locations as $location)
	            			<option value="{{ $location->id }}" @if($order->location==$location->id) selected @endif>{{ $location->name }}</option>
	            		@endforeach
	            	</select>
					<input type="hidden" name="order_id" value="{{ $order->orders_id }}" id="order_id">
	            	<br>
	            	{{--<a><button data-toggle="modal" data-target="#editLocation" class="btn btn-reject-quote col-md-12">Save</button></a> --}}
	            	<a><button class="btn btn-reject-quote col-md-12" onclick="updatelocation()">Save</button></a>
	            </div>
	    	</div>
	    	<div class="box2 no-padding col-md-12">
	    		<div class="white-bg box-header order-payment-history-heading with-border">
	                <h3 class="box-title">ORDER NOTES</h3>
	                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-right"><a class="light-blue-link pull-right" href="#">View All</a></span>
	            </div>
	            <div class="col-md-12 padding20">
                    @foreach($order_notes as $order_note)
                        <p>{{ $order_note->note }}</p>
                    @endforeach
	            </div>
	    	</div>
	    </div>
    </div>
    <div class="col-md-12 no-padding">
        <div class="box2 col-md-12 invoice-summery-box no-padding">
            <div class="box-header with-border invoice-summery-heading">
                <h3 class="box-title col-md-6">Order History</h3>
                <div class="pull-right text-right col-md-6"><span><a id="previous"><i class="fa fa-caret-left"></i> Previous Link</a> &nbsp;&nbsp; <span id="entries"></span> &nbsp;&nbsp; <a id="next">Next Page <i class="fa fa-caret-right"></i></span></a>
                </div>
            </div>
             <table id="order_history" class="table quote-summery-table">
                <thead>
                    <tr>
                        <th class="col-md-4">DATE</th>
                        <th class="col-md-4">UPDATE</th>
                        <th class="col-md-4">DETAILS</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($order_histories as $order_history)
                    <tr>
                        <td class="col-md-4">{{ \Carbon\Carbon::parse($order_history->created_at)->format('M d,Y') }}</td>
                        <td class="col-md-4">
                        @if($order_history->order_history_status == 1)
                            
                        @elseif($order_history->order_history_status == 2)
                           
                        @elseif($order_history->order_history_status == 3)
                            
                        @elseif($order_history->order_history_status == 4)
                            {{ "Shipment Booked" }}
                        @elseif($order_history->order_history_status == 5)

                        @elseif($order_history->order_history_status == 6)

                        @elseif($order_history->order_history_status == 7)

                        @elseif($order_history->order_history_status == 8)

                        @elseif($order_history->order_history_status == 9)

                        @elseif($order_history->order_history_status == 10)
                            {{ "Shipment Departed" }}
                        @elseif($order_history->order_history_status == 11)
                            {{ "Shipment Arrived at Port" }}
                        @elseif($order_history->order_history_status == 12)

                        @elseif($order_history->order_history_status == 13)

                        @endif
                    </td>
                    <td class="col-md-4">
                        @if($order_history->order_history_status == 1)
                            
                        @elseif($order_history->order_history_status == 2)
                           
                        @elseif($order_history->order_history_status == 3)
                            
                        @elseif($order_history->order_history_status == 4)
                            <?php echo $order_history->ETD_china.' '.\Carbon\carbon::parse($order_history->prealert_details_created_at)->format('M d,Y'); ?>
                        @elseif($order_history->order_history_status == 5)

                        @elseif($order_history->order_history_status == 6)

                        @elseif($order_history->order_history_status == 7)

                        @elseif($order_history->order_history_status == 8)

                        @elseif($order_history->order_history_status == 9)

                        @elseif($order_history->order_history_status == 10)
                            {{ $order_history->port_of_origin }}
                        @elseif($order_history->order_history_status == 11)
                            {{ $order_history->shipper_destination }}
                        @elseif($order_history->order_history_status == 12)

                        @elseif($order_history->order_history_status == 13)

                        @endif
                        </td>
                    </tr>
                    <tr>
                    @foreach($quote_histories as $quote_history)
                        <td class="col-md-4">{{ \Carbon\Carbon::parse($quote_history->created_at)->format('M d,Y') }}</td>
                        <td class="col-md-4">
                            @if($quote_history->quote_history_status == 3)
                                {{ "Shipping Quote Approved" }}
                            @elseif($quote_history->quote_history_status == 5)
                                {{ "Shipping Quote Expired" }}
                            @endif
                        </td>
                        <td class="col-md-4">
                            @if($quote_history->quote_history_status == 3)
                                {{ "Paid" }}
                            @elseif($quote_history->quote_history_status == 5)
                                <a class="request-quote-link" href="{{ url('quote/create') }}">Request New Quote</a>
                            @endif
                        </td>
                    @endforeach
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
	</div>
</section>
    <div class="modal fade" id="submitHold" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="learnMoreModalLabel">Submit Hold</h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-md-12">
            <div class="col-md-12 no-padding">
            	@if($order->shipping_method_id == 1)
            	<form method="post" action="{{ url('submit-hold') }}">
            		{{ csrf_field() }}
            		<input type="hidden" name="order_id" value="{{ $order->orders_id }}">
            		<div class="form-group">
            			<div class="col-md-12">
            				@foreach($holds as $hold)
	            				<div class="col-md-12">
	            					<label class="control-label">{{ $hold->hold_name }}</label>
									<input type="hidden" name="hold_id" value="{{ $hold->id }}">
	            				</div>
		            			@foreach($sub_holds as $sub_hold)
		            				@if($hold->id == $sub_hold->hold_id)
		            					<div class="col-md-12">
			            					<input type="checkbox" id="{{$hold->id}}_{{$sub_hold->id}}" class="check validate[required]" name="hold_checkbox_{{$hold->id}}_{{$sub_hold->id}}" value="1">{{ $sub_hold->sub_hold_name }}
			            				</div>
			            				<div class="col-md-12">
			            					<input type="text" class="form-control hide" name="note_{{$hold->id}}_{{$sub_hold->id}}" id="note_{{$hold->id}}_{{$sub_hold->id}}">
			            				</div>
			            				@endif
		            			@endforeach
	            			@endforeach
            			</div>
            		</div>
            		<div class="form-group">
            			<div class="col-md-4 col-md-offset-8">
            				<button type="submit" class="button col-md-6 pull-right">Submit</button>
            			
            				<button type="submit" class="btn-load col-md-5" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">Close</button>
            			</div>
            		</div>
            	</form>
            	@elseif($order->shipping_method_id == 2)
            		<form method="post" action="{{ url('submit-hold') }}">
	            		{{ csrf_field() }}
	            		<input type="hidden" name="order_id" value="{{ $order->orders_id }}">
	            		<div class="form-group">
	            			<div class="col-md-12">
	            				@foreach($holds as $hold)
		            				<div class="col-md-12">
		            					<label class="control-label">{{ $hold->hold_name }}</label>
		            				</div>
			            			@foreach($sub_holds as $sub_hold)
			            				@if($hold->id == $sub_hold->hold_id)
			            					<div class="col-md-12">
			            						<input type="hidden" name="hold_id" value="{{ $hold->id }}">
				            					<input type="checkbox" class=" validate[required]" name="hold_checkbox_{{$hold->id}}_{{$sub_hold->id}}" value="1">{{ $sub_hold->sub_hold_name }}
				            				</div>
				            			@endif
			            			@endforeach
		            			@endforeach
	            			</div>
	            		</div>
	            		<div class="form-group">
	            			<div class="col-md-4 col-md-offset-8">
	            				<button type="submit" class="button col-md-6 pull-right">Submit</button>
	            				<button type="submit" class="btn-load col-md-5" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">Close</button>
	            			</div>
	            		</div>
		           </form>
            	@else
            	<div class="col-md-12 text-center">
            		<a target="_blank" href="https://wwwapps.ups.com/WebTracking/track?track=yes&amp;trackNums={{$order->tracking_number}}"><button class="button">Track Shipment</button></a>
            	</div>
            	@endif
            </div>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="releaseHold" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="learnMoreModalLabel">Release Hold</h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-md-12">
            <div class="col-md-12 no-padding">
            	@if($order->shipping_method_id == 1)
            	<form method="post" action="{{ url('release-hold') }}">
            		{{ csrf_field() }}
            		@foreach($order_holds as $order_hold)
            			@if(($order_hold->hold_id == 2) && ($order_hold->sub_hold_id == 7))
		            		<div class="form-group">
		            			<div class="col-md-12">
		            				<div class="col-md-12">
		            					<label class="control-label">Customs Exam Fee</label>
		            				</div>
		            				<div class="col-md-12">
		            					<input type="text" name="fee_{{$order_hold->hold_id}}_{{$order_hold->sub_hold_id}}" class="form-control">
		            				</div>
		            			</div>
		            		</div>
		            	@else
	            		<div class="col-md-12">
		            		<div class="form-group">
		            			<div class="col-md-12">
		            				<h4>{{ $order_hold->sub_hold_name }}</h4>
		            				<label class="control-label">
		            					Are you sure you want to release the hold ? 
		            				</label>
		            				<br>
		            				<input type="radio" name="confirm_{{$order_hold->hold_id}}_{{$order_hold->sub_hold_id}}" value="1" checked> Yes
		            				<input type="radio" name="confirm_{{$order_hold->hold_id}}_{{$order_hold->sub_hold_id}}" value="0"> No
		            			</div>
		            		</div>
		            	</div>
	            		@endif
	            	@endforeach
            	@endif
            	@if($order->shipping_method_id == 2)
	            	<form method="post" action="{{ url('release-hold') }}">
	            		{{ csrf_field() }}
	            		@foreach($order_holds as $order_hold)
	            			@if($order_hold->is_deleted == 0)
		            		<div class="col-md-12">
			            		<div class="form-group">
			            			<div class="col-md-12">
			            				<h4>{{ $order_hold->sub_hold_name }}</h4>
			            				<label class="control-label">
			            					Are you sure you want to release the hold ? 
			            				</label>
			            				<br>
			            				<input type="radio" name="confirm_{{$order_hold->hold_id}}_{{$order_hold->sub_hold_id}}" value="1" checked> Yes
			            				<input type="radio" name="confirm_{{$order_hold->hold_id}}_{{$order_hold->sub_hold_id}}" value="0"> No
			            			</div>
			            		</div>
			            	</div>
			            	@endif
	            		@endforeach
	            	<div class="form-group">
            		</div>
            	@endif
            </div>
          </div>
          <div class="modal-footer">
            	<div class="col-md-4 col-md-offset-8">
    				<button type="submit" class="button col-md-6 pull-right">Submit</button>
    				<button type="submit" class="btn-load col-md-5" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">Close</button>
    			</div>
    		</form>
          </div>
        </div>
      </div>
    </div>
<div class="modal fade" id="editLocation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="learnMoreModalLabel">Add Location</h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-md-12">
         		<div class="col-md-12 no-padding">
         			<form method="post" action="{{ url('add-location') }}">
         				{{ csrf_field() }}
         				<div class="form-group">
         					<div class="col-md-2">
         						<label class="control-label">Location</label>
         					</div>
         					<div class="col-md-10">
         						<input type="text" name="location" class="form-control">
         					</div>
         				</div>
         		</div>
          </div>
          <div class="modal-footer">
          		<div class="col-md-6 col-md-offset-6">
    				<button type="submit" class="button col-md-6 pull-right">Add Location</button>
    				<button type="submit" class="btn-load col-md-5" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">Close</button>
    			</div>
                </form>
          </div>
        </div>
      </div>
</div>
    <div class="modal fade" id="viewHoldModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="learnMoreModalLabel">View Hold(s)</h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-md-12">
            <div class="col-md-12 no-padding">
            	<table class="table">
            		<thead>
            			<tr>
            				<th>Hold Name</th>
            				<th>Status</th>
            			</tr>
            		</thead>
            		<tbody>
            			@foreach($order_holds as $order_hold)
	            			@if($order_hold->is_deleted == 0)
		            			<tr>
		            				<td>{{ $order_hold->sub_hold_name }}</td>
		            				<td> {{ "Hold" }}</td>
		            			</tr>
		            		@else
		            			<tr>
		            				<td>{{ $order_hold->sub_hold_name }}</td>
		            				<td> {{ "Release" }}</td>
		            			</tr>
		            		@endif
		            	@endforeach
		            </tbody>
			    </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn" data-dismiss="modal" aria-label="Close">Close</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="viewDocumentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document" style="width:80%;">
        <div class="modal-content">
          <div class="modal-header col-md-12 padding-bottom-0 padding-left-0">
          	<div class="col-md-6 no-padding">
	            <h4 class="modal-title col-md-12" id="learnMoreModalLabel">View Document</h4>
	            <!-- <p class=" col-md-12">Commercial Invoice</p> -->
	        </div>
            <div class="col-md-6 no-padding">
            	<a id="revision_request" href="{{ url('logistics/revision-document') }}" class="col-md-7 light-blue-link" href="">Request Revision</a>
            	<div class="col-md-5 no-padding">
            		<a id="approve_request" href="{{ url('logistics/approve-document') }}" class="button col-md-12 pull-right">Approve</a>
            	</div>
            	<div class="col-md-12">
            		<div class="col-md-6 pull-right text-right no-padding">
            			<i class="fa fa-save"></i> &nbsp;&nbsp;&nbsp;&nbsp;
            			<i class="fa fa-print"></i>
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-body col-md-12">
            <div id="iframe" class="col-md-12 no-padding">
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn" data-dismiss="modal" aria-label="Close">Close</button>
          </div>
        </div>
      </div>
    </div>
<div class="modal fade" id="editDocumentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document" style="width:60%">
        <div class="modal-content">
          <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="learnMoreModalLabel">Edit Document</h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-md-12">
            <div id="document_section" class="col-md-12 no-padding">
            	<table class="table quote-summery-table">
                    <thead>
                    <tr>
                        <th class="col-md-4">Customer Documents</th>
                        <th class="col-md-3">Date Submitted</th>
                        <th class="col-md-2">Due Date</th>
                        <th class="col-md-3">View</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($documents as $document)
                            <tr>
                                {{--*/ $due_date = ''; /*--}}
                                @if($document->due_date) 
                                    {{--*/ $due_date = \Carbon\Carbon::parse($document->due_date)->format('m/d/Y'); /*--}}
                                @else
                                    {{--*/ $due_date = null; /*--}}
                                @endif
                                <td class="col-md-4"><input type="checkbox" name="checkbox" @if($document->status == 1) {{ "checked" }} @endif><span>{{ $document->document }}</span></td>
                                <td class="col-md-3">{{ \Carbon\Carbon::parse($document->order_document_created_at)->format('m/d/Y') }}</td>
                                <td class="col-md-2">{{ $due_date }}</td>
                                <td class="col-md-3"><span value="{{ $document->order_document_id.",".$document->document.",".$due_date.",".$document->id }}" class="edit_document_link light-blue-link">@if($document->document_id) Edit @else Upload Document @endif</span></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="modal-footer">
                	<button type="submit" class="btn-load" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">Close</button>
                </div>
            </div>
            <div id="edit_document_section" class="col-md-12 no-padding ">
            	<div class="col-md-12 margin-bottom-20">
	            	<div class="col-md-6 text-left">
	            		<a id="back" class="light-blue-link">Back</a>
	            	</div>
	            	<div class="col-md-6 text-right">
	            		<a id="Delete_Requirement" data-toggle="modal" data-target="#deleteDocumentModal"  value="" class="red-link">Delete Requirement</a>
	            	</div>
	            </div>
            	<form method="post" action="{{ url('logistics/update-document') }}" id="validate_edit_document" enctype="multipart/form-data">
                    {{ csrf_field() }}
            		<input type="hidden" name="order_document_id" id="order_document_id">
            		<input type="hidden" name="order_document_name" id="order_document_name">
            		<input type="hidden" name="order_id" value="{{ $order->orders_id }}">
                    <input type="hidden" name="document_id" id="document_id">
            		<div class="form-group">
            			<div class="col-md-12">
	            			<div class="col-md-2">
	            				<label class="control-label">File(s)</label>
	            			</div>
	            			<div class="col-md-10" id="file_name">
	            			</div>
	            		</div>
            		</div>
            		<div class="form-group">
            			<div class="col-md-2">
            				<label class="control-label"></label>
            			</div>
            			<div class="col-md-10">
            				<input type="file" class="form-control validate[required]" name="document">
            			</div>
            		</div>
            		<div class="form-group">
            			<div class="col-md-2">
            				<label class="control-label">Due Date</label>
            			</div>
            			<div class="col-md-10">
            				<input type="text" id="order_document_due_date" class="form-control datepicker  validate[required]" name="due_date">
            			</div>
            		</div>
            		<div class="form-group">
            			<div class="col-md-12">
		                	<button class="btn-load pull-right" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">Close</button>
		                	<button type="submit" class="button pull-right">Save</button>
		                </div>
	                </div>
            	</form>
            </div>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
</div>
<div class="modal fade" id="deleteDocumentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="learnMoreModalLabel">Delete Document</h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-md-12">
            <div class="col-md-12 no-padding">
            	<form method="post" action="{{ url('logistics/delete-document') }}">
            		{{ csrf_field() }}
            		<input type="hidden" name="delete_document_id" id="delete_document_id">
            		Are you sure want to delete ?
            		<div class="form-Control">
            			<div class="col-md-12">
	            			<button type="submit" class="button pull-right">Yes</button>
	            			<button class="btn-load pull-right" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">No</button>
	            		</div>
            		</div>
            	</form>
            </div>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
</div>
<div class="modal fade" id="editLogisticModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document" style="width:70%">
        <div class="modal-content">
          <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="learnMoreModalLabel">Edit Logistics Details</h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-md-12">
            <form id="validate_logistics_details" method="post" action="{{ url('logistics/update-logistics-details') }}">
            		{{ csrf_field() }}
            	<input type="hidden" name="order_id" value="{{ $order_id }}">
            	<input type="hidden" name="customer_requirement_id" value="{{ $order->customer_requirement_id }}">
            	<div class="col-md-6 no-padding">
            		<h4>Customs</h4>
            		<div class="form-group">
            			<div class="col-md-12">
            				<div class="col-md-4 no-padding">
            					<label class="control-label my-label">ISF Status</label>
            				</div>
            				<div class="col-md-8 no-padding">
            					<select class="form-control validate[required]" name="location_of_goods" >
	                				<option value="ICT" @if($order->location_of_goods == 'ICT') {{ "selected" }} @endif> ICT</option>
	                				<option value="AZ West" @if($order->location_of_goods == 'AZ West') {{ "selected" }} @endif> AZ West</option>
	                				<option value="St. George" @if($order->location_of_goods == 'St. George') {{ "selected" }} @endif> St. George</option>
	                				<option value="Imperial" @if($order->location_of_goods == 'Imperial') {{ "selected" }} @endif> Imperial</option>
	                				<option value="MAT" @if($order->location_of_goods == 'MAT') {{ "selected" }} @endif> MAT</option>
	                				<option value="Forward Air" @if($order->location_of_goods == 'Forward Air') {{ "selected" }} @endif> Forward Air</option>
	                				<option value="CAS" @if($order->location_of_goods == 'CAS') {{ "selected" }} @endif> CAS</option>
	                				<option value="Energy Transport" @if($order->location_of_goods == 'Energy Transport') {{ "selected" }} @endif> Energy Transport</option>
	                				<option value="WAO5" @if($order->location_of_goods == 'WAO5') {{ "selected" }} @endif> WAO5</option>
	                				<option value="Other" @if($order->location_of_goods == 'Other') {{ "selected" }} @endif> Other</option>
	                			</select>
            				</div>
            			</div>
            		</div>
            		<div class="form-group">
            			<div class="col-md-12">
	            			<div class="col-md-4 no-padding">
	            				<label class="control-label my-label">Shipment Contains</label>
	            			</div>
	            			<div class="col-md-8 no-padding">
	            				<div class="col-md-12 no-padding">
	            					@foreach($all_shipment_contains as $all_shipment_contain)
		            					<div class="col-md-12 no-padding">
			                           		<input type="checkbox" value="{{ $all_shipment_contain->id }}" name="shipment_contain[]" @if(in_array($all_shipment_contain->name,$shipment_contain_names)) {{ "checked" }} @endif> {{ $all_shipment_contain->name }}
			                           	</div>
			                        @endforeach
		            			</div>
	            			</div>
	            		</div>
            		</div>
            		<div class="form-group">
            			<div class="col-md-12">
	            			<div class="col-md-4 no-padding">
	            				<label class="control-label my-label">Additional Services</label>
	            			</div>
	            			<div class="col-md-8 no-padding">
	            				<div class="col-md-6 no-padding">
	            					<div class="col-md-12 no-padding">
		                				<input type="checkbox" class="validate[minCheckbox[1]]" name="additional_service[]" value="FDA_clearance" @if($order->FDA_clearance == 1) {{ "checked" }} @endif>FDA Clearance
		                			</div>
		                			<div class="col-md-12 no-padding">
		                				<input type="checkbox" class="validate[minCheckbox[1]]" name="additional_service[]" value="lacey_act" @if($order->lacey_act == 1) {{ "checked" }} @endif>Lacey Act
		                			</div>
		                			<div class="col-md-12 no-padding">
		                				<input type="checkbox" class="validate[minCheckbox[1]]" name="additional_service[]" value="OGA" @if($order->OGA == 1) {{ "checked" }} @endif>OGA/PGA
		                			</div>
		            			</div>
	            			</div>
	            		</div>
            		</div>
            	</div>
	            <div class="col-md-6 no-padding">
	            	<h4>Dates</h4>
	        		<div class="form-group">
	        			<div class="col-md-12">
	        				<div class="col-md-4 no-padding">
	        					<label class="control-label my-label">ETD Origin</label>
	        				</div>
	        				<div class="col-md-8 no-padding">
	        					<input type="text" class="form-control datepicker" name="ETD" value="@if($prealert_detail){{ date('m/d/Y',strtotime($prealert_detail->ETD_china)) }}@endif">
	        				</div>
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<div class="col-md-12">
	        				<div class="col-md-4 no-padding">
	        					<label class="control-label my-label">ETA U.S.</label>
	        				</div>
	        				<div class="col-md-8 no-padding">
	        					<input type="text" class="form-control datepicker" name="ETA" value="@if($prealert_detail){{ date('m/d/Y',strtotime($prealert_detail->ETA_US)) }}@endif">
	        				</div>
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<div class="col-md-12">
	        				<div class="col-md-4 no-padding">
	        					<label class="control-label my-label">Cargo Release Date</label>
	        				</div>
	        				<div class="col-md-8 no-padding">
	        					<input type="text" class="form-control datepicker" name="cargo_release_date" value="{{ date('m/d/Y',strtotime($order->cargo_release)) }}">
	        				</div>
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<div class="col-md-12">
	        				<div class="col-md-4 no-padding">
	        					<label class="control-label my-label">LFD</label>
	        				</div>
	        				<div class="col-md-8 no-padding">
	        					<input type="text" class="form-control datepicker" name="lfd">
	        				</div>
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<div class="col-md-12">
	        				<div class="col-md-4 no-padding">
	        					<label class="control-label my-label">Delivery Date</label>
	        				</div>
	        				<div class="col-md-8 no-padding">
	        					<input type="text" class="form-control datepicker" name="delivery_date" value="{{ \Carbon\Carbon::parse($order->ETA_to_warehouse)->format('m/d/Y') }}">
	        				</div>
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<div class="col-md-12">
	        				<div class="col-md-4 no-padding">
	        					<label class="control-label my-label">Pick Up Date</label>
	        				</div>
	        				<div class="col-md-8 no-padding">
	        					<input type="text" class="form-control datepicker" name="pick_up_date">
	        				</div>
	        			</div>
	        		</div>
	        		<h4>Fees</h4>
	        		<div class="form-group">
	        			<div class="col-md-12">
	            			<div class="col-md-4 no-padding">
	            				<label class="control-label my-label">Duties</label>
	            			</div>
	            			<div class="col-md-8 no-padding">
	            				<input type="text" class="form-control" name="duties" value="">
	            			</div>
	            		</div>
	            		<div class="col-md-12">
	            			<div class="col-md-4 no-padding">
	            				<label class="control-label my-label">Port Fees</label>
	            			</div>
	            			<div class="col-md-8 no-padding">
	            				<input type="text" class="form-control" name="port_fees" value="@if($customer_clearance){{ $customer_clearance->terminal_fees }}@endif">
	            			</div>
	            		</div>
	            		<div class="col-md-12">
	            			<div class="col-md-4 no-padding">
	            				<label class="control-label my-label">CFS Fees</label>
	            			</div>
	            			<div class="col-md-8 no-padding">
	            				<input type="text" class="form-control" name="CFS_fees" value="">
	            			</div>
	            		</div>
	            		<div class="col-md-12">
	            			<div class="col-md-4 no-padding">
	            				<label class="control-label my-label">Pallet Exchange</label>
	            			</div>
	            			<div class="col-md-8 no-padding">
	            				<input type="text" class="form-control" name="pallet_exchange" value="">
	            			</div>
	            		</div>
	        		</div>
	            </div>
	            <div class="form-group">
	            	<div class="col-md-12">
	            		<button type="submit" class="button pull-right">Save</button>
	            		<button class="btn-load pull-right" data-dismiss="modal" aria-label="Close" style="border:2px solid #cdd1d6;margin-right: 10px; no-padding">Close</button>
	            	</div>
	            </div>
	        </form>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
</div>
<div class="modal fade" id="addOrderNoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header col-md-12">
            <h4 class="modal-title col-md-10" id="learnMoreModalLabel">Add Order Note</h4>
            <button type="button" class="close col-md-2 text-right" data-dismiss="modal" aria-label="Close" style="position: absolute;z-index: 2;">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body col-md-12">
            <div class="col-md-12 no-padding">
            	<form method="post" action="{{ url('add-order-note') }}">
           		{{ csrf_field() }}
            		<input type="hidden" name="order_id" value="{{ $order_id }}">
                    <input type="hidden" name="zoho_order_id" value="{{ $order->zoho_order_id }}">
            		<div class="form-group">
            			<div class="col-md-12 no-padding">
            				<div class="col-md-2 no-padding">
            					<label class="control-label">Order Note</label>
            				</div>
            				<div class="col-md-10">
            					<textarea class="form-Control col-md-12" name="note"></textarea>
            				</div>
            			</div>
            		</div>
            		
            </div>
          </div>
          <div class="modal-footer">
                    <div class="form-Control">
                        <div class="col-md-12">
                            <button type="submit" name="submit" class="button">Save</button>
                            <button class="btn-load pull-right" data-dismiss="modal" aria-label="Close" style="border: 2px solid #cdd1d6;">Close</button>
                        </div>
                    </div>
                </form>
          </div>
        </div>
      </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="http://www.datejs.com/build/date.js"></script>
{!! Html::script('assets/dist/js/datatable/jquery.dataTables.min.js') !!}
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach',
                {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
        });
    </script>
<script>
	$(document).ready(function () {
		
        var prefix = 's2id_';
        $("form[id^='validate_edit_document']").validationEngine('attach', {
            promptPosition: "bottomRight", scroll: false,
            prettySelect: true,
            usePrefix: prefix
        });

        $("form[id^='validate_logistics_details']").validationEngine('attach', {
            promptPosition: "bottomRight", scroll: false,
            prettySelect: true,
            usePrefix: prefix
        });

        $('.datepicker').datepicker({
            startDate: new Date(),
		});
        $('#edit_document_section').hide();
        $('#back').click(function(){
        	$('#document_section').show();
        	$('#edit_document_section').hide();
        });
        $('.edit_document_link').click(function(){
        	var value = $(this).attr('value');
        	var split = value.split(',');
			console.log(split);
        	$('#delete_document_id').val(split[0]);
            if(split[0]){
                $('#Delete_Requirement').show();
            }
            else{
                $('#Delete_Requirement').hide();
            }
        	$('#file_name').html(split[1]);
        	$('#order_document_name').val(split[1]);
        	$('#order_document_id').val(split[0]);
        	$('#order_document_due_date').val(split[2]);
            $('#document_id').val(split[3]);
        	$('#document_section').hide();
        	$('#edit_document_section').show();
        });
	});
	$('.check').click(function(){
		var id = $(this).attr('id');
	 	var hold = id.split('_');
	 	if(hold[0] == 2){
	 		if($(this).prop("checked") == true){
	  			$('#note_'+id).removeClass('hide');
	  		}
	  		else{
	  			$('#note_'+id).addClass('hide');
	  		}
	  	}
	});
	$('.view_document').click(function(){
		var id = $(this).attr('id');
        var document_id = $(this).attr('data-document-id');
        console.log(document_id);
        var url = "{{ url('') }}";
        $('#approve_request').attr("href", url+"/logistics/approve-document/"+document_id);
        $('#revision_request').attr("href", url+"/logistics/revision-document/"+document_id);

		$('#iframe').html('<iframe class="col-md-12 no-padding" src="'+id+'" frameborder="0" allowfullscreen style="min-height:400px;"></iframe>');
	});
	$(document).ready(function () {
        $('#order_history').DataTable({
            "bSort" : false,
            "bPaginate": true,
            "bFilter": false,
            "bInfo": true,
            "pageLength": 5,
        });
        $('#order_history_length').addClass('hide');
        $('#order_history_paginate').addClass('hide');
        $('#order_history_info').addClass('hide');
        $('#entries').html($('#order_history_info').text());
    });
    $('#previous').click(function(){
        $('#order_history_previous').click();
        $('#entries').html($('#order_history_info').text());
    });
    $('#next').click(function(){
        $('#order_history_next').click();
        $('#entries').html($('#order_history_info').text());
    });
	function updatelocation(){
	    var location=$("#location").val();
	    var order_id=$("#order_id").val();
        $.ajax({
            headers: {
                'X-CSRF-Token':  "{{ csrf_token() }}"
            },
            method: 'POST', // Type of response and matches what we said in the route
            url: '/logistics/updatelocation', // This is the url we gave in the route
            data: {
                'location': location,
				'order_id': order_id,
			},
            success: function (response) {
				window.location.reload();
            }
        });
	}
</script>
@endsection