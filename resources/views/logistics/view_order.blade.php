@extends('layouts.member.app')
@section('title', 'Confirm Shipping Details')
@section('css')
{!! Html::style('assets/dist/css/style.css') !!}
<style>
.col-md-12 {  padding-left: 35px;  }
.shead {  padding-right: 1px;  }
td {  border-bottom: 0px !important; border-right: 1px solid #f4f4f4 !important;  }
</style>
@endsection
@section('content')
    <section class="content-header">
        <h1>Confirm Bill of lading</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('member/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ url('neworders') }}"><i class="fa fa-shopping-cart"></i> New Orders</a></li>
            <li class="active"><a href="{{ url('neworders/create') }}"> Confirm Bill of lading</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box border-top-0">
            <div class="box-body ">
                <div class="row ">
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                <td class="col-md-4">Order #  {{ $order->order_no }}</td>
                                <td class="col-md-4">Company: {{ $order->company_name }}</td>
                                <td class="col-md-4">
                                    <a href="javascript:void(0)" onclick="revisionsend('{{$order_id}}')">Request Revision</a>
                                    <a class="button" href="{{ url('bill_of_lading/approve?id='.$order_id) }}">Approve</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            @if(count($order_files) != 0)
                <div class="box-body">
                    <div class="col-md-12"><h4>Order Files</h4></div>
                    <div class="col-md-12">
                        <p><a target="_blank" href="{{ url('uploads/bills',$order_files->order_file) }}">{{ $order_files->order_file }}</a></p>
                    </div>
                </div>
                <div class="box-body" id="file_div">
                    <div class="col-md-12">
                        <iframe class="col-md-12 no-padding" id="fileframe" height="500" src="{{ url('/uploads/bills/'.$order_files->order_file) }}" allowfullscreen></iframe>
                    </div>
                </div>
            @endif
        </div>
    </section>
    <div class="modal fade" id="revisionmodal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Request Revision</h4>
                </div>
                <div class="modal-body">
                    <form class="" name="validate" id="validate" action="{{ url('bill_of_lading/revision') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="order" id="order" value="">
                                <div class="form-group">
                                    <label class="control-label">Revision Request<br><span style="color: #7d7f80;font-weight: normal;">Your request will be submitted to the shipper for adjustment.</span></label>
                                    <div class="col-md-12 no-padding">
                                        <textarea name="revision_note" id="revision_note" class="validate[required] col-md-12"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="text-align: right">
                                <input type="submit" name="revision" id="revision" value="  Submit  " class="button">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection
@section('js')
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js') !!}
    {!! Html::script('assets/plugins/validationengine/languages/jquery.validationEngine-en.js') !!}
    {!! Html::script('assets/plugins/validationengine/jquery.validationEngine.js') !!}
    <script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                startDate: new Date(),
            });
            var prefix = 's2id_';
            $("form[id^='validate']").validationEngine('attach', {
                    promptPosition: "bottomRight", scroll: false,
                    prettySelect: true,
                    usePrefix: prefix
                });
            var order_id= '{{$order_id}}';
        });
        function showdiv() {
            $("#quote_div").show();
        }
        $("#edit").click(function (event) {
            event.preventDefault();
            if ($("#validate").validationEngine('validate')) {
                var goods_date = $("#goods_ready_date").val();
                var destination = $("#destination").val();
                var qty = $("#qty").val();
                var size = $("#size").val();
                var order_id = $("#order_id").val();
                $.ajax({
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    method: 'put',
                    url: '/neworders/update',
                    data: {
                        goods_date:goods_date,
                        destination:destination,
                        qty:qty,
                        size:size,
                        order_id:order_id
                    },
                    success: function (response) {
                        console.log(response);
                        window.location="/neworders";
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    }
                });
            }
        });
        function revisionsend(order_id) {
            $("#order").val(order_id);
            $("#revisionmodal").modal('show');
        }
    </script>
@endsection